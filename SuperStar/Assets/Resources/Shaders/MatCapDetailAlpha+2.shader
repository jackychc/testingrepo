Shader "MatCap/Detail Alpha +2" {
	Properties {
		_Color ("Tint Color", Color) = (0.5,0.5,0.5,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_MatCap ("MatCap (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags {"RenderType"="Transparent" "Queue"="Transparent+2" "IgnoreProjector"="True"  "ForceNoShadowCasting"="True"}
        Lighting Off
        Fog { Mode Off }
        LOD 200
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass {
			Name "BASE"
			//Tags { "LightMode" = "Always" }

			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest

				#include "UnityCG.cginc"

				struct v2f { 
					float4 pos : SV_POSITION;
					float2 uv : TEXCOORD0;
					float2 n_v : TEXCOORD1;
				};

				v2f vert(appdata_tan v) {
					v2f o;
					o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					o.uv = v.texcoord;
					o.n_v = mul((float3x3)UNITY_MATRIX_MV, v.normal).xy * 0.5f + 0.5f;

					return o;
				}

				uniform float4 _Color;
				uniform sampler2D _MainTex;
				uniform sampler2D _MatCap;

				float4 frag(v2f i) : COLOR {
					float4 matcapLookup = tex2D(_MatCap, i.n_v);
					float4 detailColor = tex2D(_MainTex, i.uv);
					
					float4 color;
					color.rgb = _Color.rgb * detailColor * matcapLookup * 3;
					color.a = _Color.a * detailColor.a * matcapLookup.a;

					return color;
				}
			ENDCG
		}
	} 
	FallBack "Transparent/Diffuse"
}

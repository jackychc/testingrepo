Shader "MatCap/Basic" {
	Properties {
		_Color ("Tint Color", Color) = (0.5,0.5,0.5,1)
		_MatCap ("MatCap (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags {"RenderType"="Opaque" "Queue"="Geometry" "IgnoreProjector"="True" "ForceNoShadowCasting"="True"}
    	Cull Back
        Lighting Off
        Fog { Mode Off }
        
        LOD 100
		
		Pass {
			Name "BASE"
			//Tags { "LightMode" = "Always" }

			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest

				#include "UnityCG.cginc"

				struct v2f { 
					float4 pos : SV_POSITION;
					float2 n_v : TEXCOORD1;
				};

				v2f vert(appdata_tan v) {
					v2f o;
					o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					o.n_v = mul((float3x3)UNITY_MATRIX_MV, v.normal).xy * 0.5f + 0.5f;

					return o;
				}

				uniform float4 _Color;
				uniform sampler2D _MatCap;

				float4 frag(v2f i) : COLOR {
					float4 matcapLookup = tex2D(_MatCap, i.n_v);
					
					return _Color * matcapLookup * 2;
				}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}

using UnityEngine;
using System.Collections;

public class httprequesttest : MonoBehaviour {
	
	
	int count = 1;
	 void Start ()
	{
 
		string url = Globals.serverAPIURL + "SystemTime.php";

		WWW www = new WWW(url);
		
		
		StartCoroutine(WaitForRequest(www));
	}
		 
	IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
			 
		// check for errors
		if (www.error == null)
		{
			Debug.Log("WWW Ok!: ["+ count+"]" + www.text);
			StartCoroutine(WaitForRequest(www));
		}
		else
		{
			Debug.Log("WWW Error: ["+ count+"]" + www.error);
			StartCoroutine(WaitForRequest(www));
		}
		
		count++;
	} 
	
	// Update is called once per frame
	void Update () {
	
	}
}

using UnityEngine;
using System.Collections;

public class ClothCatagoryHighlighter : MonoBehaviour {
	private bool isShopScrollListInited = false;	
	private GameObject prevCenterdObject = null;
	private GameObject currCenteredObject = null;
	
	
	// put this on a scrollpanel
	
	// Use this for initialization
	void Start ()
	{
		InvokeRepeating("UpdateRoutine", 0f, 0.015f);
	}
	
	// Update is called once per frame
	void UpdateRoutine ()
	{		
		if (isShopScrollListInited)
		{
			GameObject centeredObject = transform.GetComponent<UICenterOnChild>().centeredObject;
			
			if (centeredObject != null)
			{
				if (prevCenterdObject != null && currCenteredObject != null && prevCenterdObject != currCenteredObject)
				{
					string spriteName = prevCenterdObject.GetComponent<UISprite>().spriteName;
					string prefix = spriteName.Substring(0, spriteName.Length-1);				
					prevCenterdObject.transform.GetComponent<UISprite>().spriteName = prefix + "a";
					prevCenterdObject.transform.GetComponent<UISprite>().MakePixelPerfect();
					
					spriteName = currCenteredObject.GetComponent<UISprite>().spriteName;
					prefix = spriteName.Substring(0, spriteName.Length-1);
					currCenteredObject.transform.GetComponent<UISprite>().spriteName = prefix + "b";
					currCenteredObject.transform.GetComponent<UISprite>().MakePixelPerfect();
				}
				if (currCenteredObject == null)
				{
					string spriteName = centeredObject.GetComponent<UISprite>().spriteName;
					string prefix = spriteName.Substring(0, spriteName.Length-1);
					centeredObject.transform.GetComponent<UISprite>().spriteName = prefix + "b";
					centeredObject.transform.GetComponent<UISprite>().MakePixelPerfect();
				}
				else
					prevCenterdObject = currCenteredObject;
				
				currCenteredObject = centeredObject;				
			}
			
			
			/*
			if (prevCenterdObject != null)
			{
				string spriteName = prevCenterdObject.transform.GetComponent<UISprite>().spriteName;
				string prefix = spriteName.Substring(0, spriteName.Length-1);
				
				prevCenterdObject.transform.GetComponent<UISprite>().spriteName = prefix + "a";
			}
			
			foreach (Transform t in transform.GetComponentsInChildren<Transform>())
			{
				if (t == transform) continue;
				if (t.GetComponent<UISprite>() == null) break;
				
				string spriteName = t.GetComponent<UISprite>().spriteName;
				string prefix = spriteName.Substring(0, spriteName.Length-1);	
				
				if (t.gameObject == transform.GetComponent<UICenterOnChild>().centeredObject)
				{
					t.GetComponent<UISprite>().spriteName = prefix + "b";
				}
				else
				{
					t.GetComponent<UISprite>().spriteName = prefix + "a";
				}
				t.GetComponent<UISprite>().MakePixelPerfect();
				
			//	t.localScale = new Vector3(t.GetComponent<UISprite>().GetAtlasSprite().inner.width,
			//								t.GetComponent<UISprite>().GetAtlasSprite().inner.height,
			//								1);
			}
			
			*/
		}
		
	}
	
	public void OnShopScrollListInitialized(bool isDone)
	{
		isShopScrollListInited = isDone;
	}
}

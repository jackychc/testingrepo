using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JobScrollListItem : MonoBehaviour {
	
	public GameObject levelBlock;
	public GameObject runningCover;
	public GameObject mins;
	public GameObject minsBg;
	public GameObject rewardType;
	public GameObject amountCoin;
	public GameObject amountExp;
	
	public GameObject startBtn;
	public GameObject getRewardBtn;
	public GameObject expiredBtn;
	
	public JobScrollListManager scrollListManager;
	
	public Job job;

	bool rewardClicked = false;
	
	//UILabel countDownText;
		
	// Use this for initialization
	void Start () {
		//countDownText = runningCover.transform.FindChild("countdown").GetComponent<UILabel>();
		//DelayedStart();
	}
	
	public void DelayedStart()
	{
		// some random stuff
		
		amountExp.GetComponent<UILabel>().text = "" + Rewards.getReward(job.rewardId).exp;
		amountCoin.GetComponent<UILabel>().text = "" + Rewards.getReward(job.rewardId).coins;
		
		if (Globals.mainPlayer.Level >= job.levelRequired)
		{			
			levelBlock.SetActive(false);

			runningCover.SetActive( isDoingJob());
			
			
			if (runningCover.activeSelf)
			{
				startBtn.SetActive(false);
				getRewardBtn.SetActive(false);
				expiredBtn.SetActive(false);
				
				runningCover.transform.FindChild("countdown").GetComponent<UILabel>().text = secondsToString(job.timeRequired - (job.expireTime - job.timeLeft));
					;
			}			
			// free for select
			else
			{				
				//startBtn.SetActive(true);
				//getRewardBtn.SetActive(false);
				//expiredBtn.SetActive(false);				
				
				
				if (canGetReward())
				{
					startBtn.SetActive(false);
					getRewardBtn.SetActive(true);
					expiredBtn.SetActive(false);
				}				
				else if (isExpired())
				{
					startBtn.SetActive(false);
					getRewardBtn.SetActive(false);
					expiredBtn.SetActive(true);
				}
				else
				{
					startBtn.SetActive(true);
					getRewardBtn.SetActive(false);
					expiredBtn.SetActive(false);
				}
			}
		}
		else
		{
			levelBlock.transform.FindChild("amountLevel").GetComponent<UILabel>().text = "Lv."+ job.levelRequired;
			levelBlock.SetActive(true);

			startBtn.SetActive(false);
			getRewardBtn.SetActive(false);
			expiredBtn.SetActive(false);
		}
		
		mins.GetComponent<UILabel>().text = ""+ (int)(job.timeRequired/60f);
		
		if ((int)(job.timeRequired/60f) >= 60)
		{
			minsBg.GetComponent<UISprite>().spriteName = "jobs_hours";
			mins.GetComponent<UILabel>().text = ""+ (int)((int)(job.timeRequired/60f)/60f);
		}
		
		// set text:
		
		
		//afterOneSecond();
		
		Destroy(transform.GetComponent<UIPanel>());
	}
	
	public void afterOneSecond()
	{		
		if (isDoingJob())
		{			
			runningCover.transform.FindChild("countdown").GetComponent<UILabel>().text = secondsToString(job.timeRequired - (job.expireTime - job.timeLeft));
		}
		else
		{
			
			levelBlock.SetActive(Globals.mainPlayer.Level < job.levelRequired? true:false);
			
			// complete
			runningCover.SetActive(false);
			
			if (canGetReward())
			{
				startBtn.SetActive(false);
				getRewardBtn.SetActive(true);
				expiredBtn.SetActive(false);
			}			
			else if (isExpired())
			{
				startBtn.SetActive(false);
				getRewardBtn.SetActive(false);
				expiredBtn.SetActive(true);
			}
			else
			{
				if (Globals.mainPlayer.Level >= job.levelRequired)
					startBtn.SetActive(true);
				getRewardBtn.SetActive(false);
				expiredBtn.SetActive(false);
			}
		}
	}
	
	void StartJob()
	{
		Hashtable param = new Hashtable();
		param["Start"] = job.title;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Job_State, param);
		
		
		SoundManager.SharedManager.PlaySFX("job_click_start");
		
		if (isDoingJob())
			return;		
		else if (Jobs.isDoingJobOfSkill(job.visibleForSkill) != null)
		{
			PopupManager.ShowInformationPopup(Language.Get("job_samekind"), false);
		}
		
		else if (!isDoingLessThan3Types())
		{
			PopupManager.ShowInformationPopup(Language.Get("job_3types"), false);
		}
		
		// also have other criteria
		else if (!levelBlock.activeSelf)
		{
			
			
			job.startJob();
			
			Log.Debug("starting: "+job.timeRequired+","+job.expireTime+","+ job.timeLeft);
			
			runningCover.SetActive(true);
			runningCover.transform.FindChild("countdown").GetComponent<UILabel>().text = secondsToString(job.timeRequired - (job.expireTime - job.timeLeft));
					;
			
			startBtn.SetActive(false);
			getRewardBtn.SetActive(false);
			expiredBtn.SetActive(false);
			
			JobScene.currentSkillAnimation = Int32.Parse(job.visibleForSkill);
			
			//create local notification of job finish
			//SaveLoadManager.SharedManager.RefreshLocalNotifications();
			//LocalNotification.CancelNotification(Int32.Parse(job.visibleForSkill) + 100);
		}
		
		//scrollListManager.RefreshSFX();
	}
	
	void EndJob()
	{
		if (rewardClicked) return;
		rewardClicked = true;

		Hashtable param = new Hashtable();
		param["End"] = job.title;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Job_State, param);
		
		job.timeLeft = -1;
		
		//Globals.mainPlayer.getSkill().lastSkill = Int32.Parse(job.visibleForSkill);
		
		int skillId = Int32.Parse(job.visibleForSkill);
		
		SkillTier next = SkillTable.GetNextSkillTier(skillId, Globals.mainPlayer.getSkill().skill[skillId]);
		
		SkillTier prevTier = SkillTable.GetSkillTier(Globals.mainPlayer.TitleSkill);
		
		SkillTier afterTier = SkillTable.GetSkillTier(
			skillId,
			Globals.mainPlayer.getSkill().skill[skillId]
				+ Mathf.Min(
					(next == null? 0 : (next.skillAmount - Globals.mainPlayer.getSkill().skill[skillId])),
					Rewards.getReward(job.rewardId).skills.skill[skillId]
				)
			);
		Log.Debug("prevTier:"+prevTier.skillTierId+",afterTier:"+afterTier.skillTierId);
		
		PopupManager.ShowRewardPopup(Rewards.getReward(job.rewardId), false);
		
		if (job.visibleForSkill.Contains("0"))
			QuestFlags.SetFlag(2,true);
			//QuestFlags.var2_tennisFirstJob = true;
		
		if (job.activityId == QuestFlags.GetFlag(11).intValue)
			QuestFlags.SetFlag(12, QuestFlags.GetFlag(12).intValue+1);
		
		if (prevTier.skillTierId != afterTier.skillTierId)
		{
			Globals.mainPlayer.TitleSkill = afterTier.skillTierId;
			
			if (afterTier.Equals(next))	
				PopupManager.ShowNewTitlePopup(false);
			else
			{
				GameObject titleShower = GameObject.Find("TitleShower");
				
				if (titleShower != null)
					titleShower.GetComponent<TitleShower>().Refresh();
			}
			//PopupManager.ShowInformationPopup("new title! " + SkillTable.GetSkillTier(Globals.mainPlayer.TitleSkill).skillTierString);
			
			Hashtable jobStateParam = new Hashtable();
			jobStateParam["Title"] = Skill.skillNames[afterTier.skillType] + " " + Enum.GetName(typeof(SkillTier.STARCOLOR), afterTier.skillColor) + afterTier.skillStars + " ";
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Job_State, jobStateParam);
		}
		
		startBtn.SetActive(true);
		getRewardBtn.SetActive(false);
		expiredBtn.SetActive(false);

		rewardClicked = false;
		//scrollListManager.RefreshSFX();
	}
	
	void ExpireJob()
	{
		Hashtable param = new Hashtable();
		param["Expire"] = job.title;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Job_State, param);
		
		job.timeLeft = -1;
		
		PopupManager.ShowInformationPopup(Language.Get("job_expired"), false);
		
		startBtn.SetActive(true);
		getRewardBtn.SetActive(false);
		expiredBtn.SetActive(false);
	}
	
	public bool isDoingJob()
	{
		return Jobs.isDoingJob(job);
	}
	public bool isDoingLessThan3Types()
	{
		return (Jobs.typesOfJobsDoing() < 3);
	}
	public bool canGetReward()
	{
		return Jobs.isOnReward(job);
	}
	public bool isExpired()
	{
		return (job.timeLeft == 0);
	}
	
	string secondsToString(int seconds)
	{
		return twoDigit(seconds/3600)+":" + twoDigit((seconds%3600)/60) +":" + twoDigit((seconds%3600)%60);
	}
	
	string twoDigit(int i)
	{
		return (i < 10? "0"+i : ""+i);
	}
	
	public void showFinishByGemPopup()
	{
		PopupManager.ShowFinishByGemPopup(this, job, false);
		
	}
	
	
	public void OnFinishByGemClicked()
	{		
		Hashtable param = new Hashtable();
		param["InstantFinish"] = job.title;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Job_State, param);
		
		if (job.activityId == 1)
			MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.FinishFirstJobInJobCenter);
		
		//scrollListManager.RefreshSFX();
	}
}

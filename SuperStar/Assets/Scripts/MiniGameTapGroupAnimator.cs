using UnityEngine;
using System.Collections;

public class MiniGameTapGroupAnimator : MonoBehaviour {
	
	public GameObject[] sprites = new GameObject[2];
	public float period = 0.6f;
	
	private int spriteNo = 0;
	
	// Use this for initialization
	void Start () {
		StartCoroutine(UpdateAtIntervals());
	}
	
	private IEnumerator UpdateAtIntervals()
	{
		while (true)
		{
			Log.Debug(spriteNo);
			//spriteNo = (spriteNo+1) % 2;
			
			sprites[0].SetActive(spriteNo > 0);
			sprites[1].SetActive(spriteNo == 1);
			
			spriteNo = (spriteNo + 1) > 2? 1: spriteNo + 1;
			
			yield return new WaitForSeconds(period);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

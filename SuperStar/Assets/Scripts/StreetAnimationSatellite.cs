using UnityEngine;
using System.Collections;

public class StreetAnimationSatellite : MonoBehaviour
{
	//public StreetAnimationManager sam;
	
	GameObject newCharacter;
	SceneManager sm;
	string targetScene = "HOME";
	int goToShop = 1;
	
	private Scenes nextScene = Scenes.NONE;
	
	//bool animationLocked;
	
	void Start()
	{
		StartCoroutine("Move");
	}
	
	IEnumerator Move()
	{
		if (newCharacter == null)
			newCharacter = GameObject.Find("FEMALE");
		
		if (sm == null)
			sm = transform.GetComponent<SceneManager>();
		
		if (newCharacter == null || sm == null)
		{
			yield return new WaitForSeconds(0.05f);
			StartCoroutine(Move ());
		}
		
		else yield return null;
			
	}
	
	public IEnumerator turnBackAndEnterScene(Scenes scene)
	{
		StreetAnimationManager.breakTheEnd = true;
		StreetAnimationManager.breakTheLoop = true;		
		//while (StreetAnimationManager.animationLock)
		//	yield return null;
		StreetAnimationManager.charRotation = 0f;
		//StreetAnimationManager.currentCharRotation = 0f;
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("street_enter",false);
		newCharacter.GetComponent<CharacterManager>().onAnimEnd = OnCharacterManagerAnimationEnd;		
		
		nextScene = scene;
		yield return null;
		
		//newCharacter.GetComponent<CharacterManager>().InsertAnimation("pose01");	
	}
	
	private void OnCharacterManagerAnimationEnd(string animName)
	{
		if (animName.Equals("street_enter"))
		{
			if (!nextScene.Equals(Scenes.NONE))
			{
				if (nextScene.Equals(Scenes.MINIGAME))
					SceneManager.SharedManager.LoadScene(nextScene, true);
				else
					SceneManager.SharedManager.LoadScene(nextScene, true);
			}
		}
	}
	
	public void GoToBFMap()
	{		
		StartCoroutine(turnBackAndEnterScene(Scenes.BOYFRIENDMAP));
	}
	
	public void GoToHomeScene()
	{		
		StartCoroutine(turnBackAndEnterScene(Scenes.HOME));
	}
	
	public void GoToJobScene()
	{
		StartCoroutine(turnBackAndEnterScene(Scenes.JOB));
	}
	
	public void GoToVotingScene()
	{
		//StartCoroutine(turnBackAndEnterScene(Scenes.VOTING));
	}
	
	public void ChooseGame()
	{
		//PopupManager.ShowChooseMinigamePopup();
		
		StartCoroutine(turnBackAndEnterScene(Scenes.MINIGAME));
	}
	
	
	

	public void GoShop()
	{		
		
		
		VisitShop.Visit(Shops.GetShopById(transform.GetComponent<GoToShop>().goToShop), "STREET");
		StartCoroutine(turnBackAndEnterScene(Scenes.SHOP));
	}
	
	
	
	
}


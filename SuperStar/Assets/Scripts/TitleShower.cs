using UnityEngine;
using System.Collections;

public class TitleShower : MonoBehaviour
{
	public UISysFontLabel titleText;
	public GameObject[] stars = new GameObject[5];
	public UISprite logo;
	
	public int targetPlayerId = -1;
	public Player targetPlayer;
	
	// Use this for initialization
	void Start ()
	{
		//Destroy(transform.GetComponent<UIPanel>());
		
		Refresh(Globals.mainPlayer);
	}
	
	public void Refresh(Player profile)
	{
		targetPlayer = profile;
		Refresh();
	}
	
	public void Refresh()
	{		
		
		SkillTier st = SkillTable.GetSkillTier(targetPlayer.TitleSkill);

		if (st != null)
		{
			//Log.Debug("I AM refreshing with skill tier:" + st.skillTierId);
			
			string starColor = "1";
				
			switch (st.skillColor)
			{
				case SkillTier.STARCOLOR.BRONZE: starColor = "1"; break; 
				case SkillTier.STARCOLOR.SLIVER: starColor = "2"; break; 
				case SkillTier.STARCOLOR.GOLD: starColor = "3"; break;
				case SkillTier.STARCOLOR.DIAMOND: starColor = "4"; break;
				default: starColor = "1"; break; 
			}
			
			string logoName = "";
			
			switch (st.skillType)
			{
				case 0: logoName = "job_title_tennis_" + starColor; break;
				case 1: logoName = "job_title_dance_" + starColor; break;
				case 2:
				case 3:
				case 4: logoName = "job_title_singer_" + starColor; break;
				default: logoName = "job_title_tennis_" + starColor; break;
			}
			
			for (int i =0; i < stars.Length; i++)
			{
				stars[i].SetActive(i <= (st.skillStars-1));
				
				
				if (stars[i].activeSelf)
				{
					stars[i].GetComponent<UISprite>().spriteName = "job_title_name_bar_star_"+starColor;
					stars[i].GetComponent<UISprite>().MakePixelPerfect();
				
					if (i > 0)
					{
						Vector3 prev = stars[i-1].transform.localPosition;
						prev.x += stars[i-1].transform.localScale.x;
						stars[i].transform.localPosition = prev;
					}

				}
			}
			
			titleText.Text = Language.Get(st.skillTierString);
			logo.GetComponent<UISprite>().spriteName = logoName;
			logo.GetComponent<UISprite>().MakePixelPerfect();
		}
		
	}
	
	public void TitleShowerClicked()
	{
		PopupManager.ShowAchievementListPopup(SkillTable.GetSkillTier(targetPlayer.TitleSkill).skillType+1, false);
	}
	
	public void SetStarsVisible(bool v)
	{
		SkillTier st = SkillTable.GetSkillTier(targetPlayer.TitleSkill);
		
		for (int i =0; i < stars.Length; i++)
		{
			if ((i+1) == st.skillStars)
				stars[i].SetActive(v);
		}
	}
	
}


using UnityEngine;
using System.Collections;

public class FriendRequestItem : MonoBehaviour {
	
	public GameObject friendNameLabel;
	public GameObject tick;
	public string gameId;
	public string friendName;
	public bool selected;
	public UISprite bg;
	
	
	// Use this for initialization
	void Start ()
	{
		//a very important line of code
		Destroy(transform.GetComponent<UIPanel>());
		
		friendNameLabel.GetComponent<UISysFontLabel>().Text = friendName;
	}
	
	public void SetBgType(int type)
	{
		if (type == 1)
		{
			bg.spriteName = "frdList_roll_a";
		}
	}

	
	public void Toggle()
	{
		
		Log.Debug("toggle licked");
		selected = !selected;
		
		tick.SetActive(selected);		
	}
	
	public void setTrue()
	{
		selected =true;
		tick.SetActive(true);
	}
	
}

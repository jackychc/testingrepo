using UnityEngine;
using System.Collections;

public class SpringItemToCenter : MonoBehaviour {
	
	public GameObject scrollPanel;
	
	void Start()
	{
		if (scrollPanel == null)
			scrollPanel = transform.parent.gameObject;
	}
	
	public void SpringToCenter()
	{
		UIDraggablePanel dp = scrollPanel.GetComponent<UIDraggablePanel>();
		
		if (dp.scale.x > 0)
			SpringPanel.Begin(scrollPanel, new Vector3(-transform.localPosition.x, scrollPanel.transform.localPosition.y ,0f), 10f).onFinished = springFinished;
		
		if (dp.scale.y > 0)
			SpringPanel.Begin(scrollPanel, new Vector3(scrollPanel.transform.localPosition.x,-transform.localPosition.y ,0f), 10f).onFinished = springFinished;
	}
	
	void springFinished()
	{
		scrollPanel.GetComponent<UIDraggablePanel>().onDragFinished();
	}
}

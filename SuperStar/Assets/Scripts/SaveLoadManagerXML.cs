using System;
using UnityEngine;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Xml;


public class SaveLoadManagerXML : MonoBehaviour
{
	
	//You application needs to have permissions in the AndroidManifest.xml to write to storage.
	//That can be set in Player Settings -> Android Tab -> Other Settings -> Configuration -> Force SD-Card permission.
	
	/*
	// for load in beginning
	void Awake ()
	{
		//LoadData();
	}
   
   	public static void LoadData()
	{ 
		
		// use Playerpref for "have save?)
		
		string loaded = LoadXML();
		
		if (!loaded.ToString().Equals(""))
		{
	 		Globals.Variables data = (Globals.Variables) DeserializeObject(loaded); 
			
			// dunno if this is safe enough...
			Globals.variables = data;
			
			Log.Debug("level? "+Globals.variables.level);
			
			// seems not so work....
			
			// download save/load plugin??
			
		}
	} 

	public static void SaveData()
	{	
	     string data = SerializeObject(Globals.variables); 
	     CreateXML(data); 
	} 
 
	
   static string UTF8ByteArrayToString(byte[] characters) 
   {      
      UTF8Encoding encoding = new UTF8Encoding(); 
      string constructedString = encoding.GetString(characters); 
      return (constructedString); 
   } 
 
   static byte[] StringToUTF8ByteArray(string pXmlString) 
   { 
      UTF8Encoding encoding = new UTF8Encoding(); 
      byte[] byteArray = encoding.GetBytes(pXmlString); 
      return byteArray; 
   } 
 
   static string SerializeObject(object pObject) 
   { 
      string XmlizedString = null; 
      MemoryStream memoryStream = new MemoryStream(); 
      XmlSerializer xs = new XmlSerializer(typeof(Globals.Variables)); 
      XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8); 
      xs.Serialize(xmlTextWriter, pObject); 
      memoryStream = (MemoryStream)xmlTextWriter.BaseStream; 
      XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray()); 
      return XmlizedString; 
   } 

   static object DeserializeObject(string pXmlizedString) 
   { 
      XmlSerializer xs = new XmlSerializer(typeof(Globals.Variables)); 
      MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(pXmlizedString)); 
      XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8); 
      return xs.Deserialize(memoryStream); 
   } 
 
   static void CreateXML(string data) 
   { 
      StreamWriter writer; 
      FileInfo t = new FileInfo(Globals.dbSavePath); 
      if(!t.Exists) 
      { 
         writer = t.CreateText(); 
      } 
      else 
      { 
         t.Delete(); 
         writer = t.CreateText(); 
      } 
      writer.Write(data); 
      writer.Close();
   } 
 
   static string LoadXML() 
   { 
      StreamReader r = File.OpenText(Globals.dbSavePath); 
      string _info = r.ReadToEnd(); 
      r.Close(); 
      return _info;
   } 
	 */
}



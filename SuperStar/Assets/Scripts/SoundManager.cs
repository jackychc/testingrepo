using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
	public enum BGMState
	{
		PLAYING,
		STOP,
		FADEOUT,
		FADEIN
	}
	
	public AudioSource bgmSource;
	public AudioSource sfxSource;
	public AudioSource sfxLoopSource;
	
	
	private BGMState currentBGMState = BGMState.STOP;
	public BGMState CurrBGMState{
		get{
			return currentBGMState;	
		}
	}
	
	
	private static SoundManager instance = null; 
	public static SoundManager SharedManager
	{
		get{
			return instance;	
		}
	}
	
	bool forceCut;
	float mVolume = 1f;
	string nextBGM;
	
	void Awake()
	{
		if (instance != null)
			Destroy(this);
	}

	// Use this for initialization
	void Start ()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(transform.gameObject);
			
			// TITLE PAGE MUSIC
			
			//StartBGM();
		}
	}
	
	public void ChangeMusic(string fileName)
	{
		if (currentBGMState.Equals(BGMState.PLAYING))
		{
			if (nextBGM != null && !nextBGM.Equals(""))
			{
				if (nextBGM.Equals(fileName))
					return;
			}
		}
		
		StartCoroutine(EnumChangeMusic(fileName));
	}
	
	private IEnumerator EnumChangeMusic(string fileName)
	{
		if (bgmSource.isPlaying)
			yield return StartCoroutine(EnumFadeOutBGM());
		
		nextBGM = fileName;
		
		StopCoroutine("ChangeBGM");
		yield return StartCoroutine(ChangeBGM());
	}
	
	private IEnumerator ChangeBGM()
	{
		while(!currentBGMState.Equals(BGMState.STOP))
		{
			yield return new WaitForSeconds(0.05f);
		}
		
		StartBGM();
	}
	
	#region Loop SFX
	public void PlayLoopSFX(string fileName)
	{
		if (sfxLoopSource.isPlaying)
			sfxLoopSource.Stop();
		AudioClip clip = Resources.Load("SFX/" + fileName) as AudioClip;
		sfxLoopSource.volume = 1.0f;
		sfxLoopSource.clip = clip;
		sfxLoopSource.loop = true;
		
		if (Globals.settings_sfx)
			sfxLoopSource.Play();
	}
	
	public void StopLoopSFX()
	{
		if (sfxLoopSource.isPlaying)
			sfxLoopSource.Stop();
	}
	
	public bool IsPlayingLoopSFX()
	{
		return sfxLoopSource.isPlaying;
	}
	
	public string PlayingLoopSFXName()
	{
		return sfxLoopSource.clip.name;	
	}
	#endregion
	
	#region One off SFX
	public void PlaySFX(string fileName)
	{
		PlaySFX(fileName, 1.0f);
	}
	
	public void PlaySFX(string fileName, float volume)
	{
		AudioClip clip = Resources.Load("SFX/" + fileName) as AudioClip;
		sfxSource.volume = volume;
		
		if (Globals.settings_sfx)
			sfxSource.PlayOneShot(clip);
	}
	
	public void StopSFX()
	{
		if (sfxSource.isPlaying)
			sfxSource.Stop();
	}
	
	public void FadeOutLoopSFX()
	{
		forceCut = false;
		StopCoroutine("FadeOutRoutine");
		StartCoroutine(FadeOutRoutine(sfxLoopSource));
	}
	
	public bool isPlayingSFX()
	{
		return sfxSource.isPlaying;	
	}
	#endregion
	
	#region BGM
	public void StartBGM()
	{
		forceCut = true;
		mVolume = 1f;
		bgmSource.Stop();
		bgmSource.clip = Resources.Load("BGM/"+nextBGM) as AudioClip;
		bgmSource.volume = 1.0f;
		//transform.GetComponent<AudioSource>().Stop();
		//transform.GetComponent<AudioSource>().clip = Resources.Load("BGM/"+nextBGM) as AudioClip;
		
		//transform.GetComponent<AudioSource>().volume = 1f;
		
		if (Globals.settings_bgm)
			bgmSource.Play();
			//transform.GetComponent<AudioSource>().Play();
		currentBGMState = BGMState.PLAYING;
	}
	
	public void StopBGM()
	{
		if (bgmSource.isPlaying)
			bgmSource.Stop();
		currentBGMState = BGMState.STOP;
	}
	
	public void FadeInBGM()
	{
		forceCut = false;
		currentBGMState = BGMState.FADEIN;
		StopCoroutine("FadeInRoutine");
		StartCoroutine(FadeInRoutine(bgmSource));
	}
	
	public void FadeOutBGM()
	{
		StartCoroutine(EnumFadeOutBGM());
	}
	
	private IEnumerator EnumFadeOutBGM()
	{
		forceCut = false;
		currentBGMState = BGMState.FADEOUT;
		StopCoroutine("FadeOutRoutine");
		yield return StartCoroutine(FadeOutRoutine(bgmSource));	
	}
	
	public bool IsPlayingBGM()
	{
		return bgmSource.isPlaying;	
	}
	
	public float BGMVolume
	{
		get{
			return mVolume;
		}
		
		set{
			mVolume = value;	
		}
	}
	#endregion
	
	#region FadeInOut
	private IEnumerator FadeInBGMRoutine(AudioSource audioSource)
	{
		yield return FadeInRoutine(audioSource);
	}
	
	private IEnumerator FadeInRoutine(AudioSource audioSource)
	{
		if (!audioSource.isPlaying)
		{
			audioSource.volume = 0.0f;
			audioSource.Play();
			Log.Debug("is not playing lor ?");
		}
		
		Log.Debug("ANVBCADVFDAG");
		while (audioSource.volume < 1.0f && !forceCut)
		{
			audioSource.volume = Mathf.Min(1.0f, audioSource.volume + 0.05f);
			//transform.GetComponent<AudioSource>().volume = mVolume;
			
			
			yield return new WaitForSeconds (0.05f);
		}
		
		currentBGMState = BGMState.PLAYING;
	}
	
	private IEnumerator FadeOutRoutine(AudioSource audioSource)
	{
		while (audioSource.volume > 0.0f && !forceCut)
		{
			audioSource.volume = Mathf.Max(0.0f, audioSource.volume - 0.05f);
			
			//transform.GetComponent<AudioSource>().volume = mVolume;
			//Log.Debug("The bgm volume is: " + audioSource.volume);
			yield return new WaitForSeconds (0.05f);
		}
		
		if (audioSource.isPlaying)
			audioSource.Stop();
		currentBGMState = BGMState.STOP;
	}
	#endregion
}


using UnityEngine;
using System.Collections;

public class RotationTweener : MonoBehaviour {
	
	public enum TWEENMODE
	{
		PASSIVE,
		LOOP,
		BACK_AND_FORTH,
		CONSTANT
	}
	
	public int id = 1;
	float proportion = 0f;
	bool commanded;
	bool isForward = true;
	
	public float speed = 1f;
	 float fromXValue;
	public float xAngle;
	 float fromYValue;
	public float yAngle;
	 float fromZValue;
	public float zAngle;
	
	public TWEENMODE mode = TWEENMODE.PASSIVE;
	
	Transform mTrans;
	public Transform cachedTransform { get { if (mTrans == null) mTrans = transform; return mTrans; } }
	
	
	// Use this for initialization
	void Start () {
		resetStartPosition();
	}
	
	public void resetStartPosition()
	{
		proportion = 0f;
		fromXValue = cachedTransform.localEulerAngles.x;
		fromYValue = cachedTransform.localEulerAngles.y;
		fromZValue = cachedTransform.localEulerAngles.z;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (mode.Equals(TWEENMODE.PASSIVE) && !commanded)
			return;
		
		
		proportion = Mathf.Max(0.0f, Mathf.Min(proportion, 1.0f));
		
		
		Move();
	}
	
	public void setProportion(float v)
	{
		proportion = v;
	}
	
	public void Forward()
	{
		commanded = true;
		isForward = true;
	}
	
	public void Backward()
	{
		commanded = true;
		isForward = false;
	}
	
	void Move()
	{
		
		proportion = proportion + (isForward? 1 : -1) * speed * (1f/30);
		
		
		
		cachedTransform.localEulerAngles = new Vector3( Mathf.Lerp(fromXValue, xAngle, proportion),
												Mathf.Lerp(fromYValue, yAngle, proportion),
												Mathf.Lerp(fromZValue, zAngle, proportion));
		
		if (mode.Equals(TWEENMODE.LOOP) || mode.Equals(TWEENMODE.CONSTANT))
		{
			if (proportion >= 1.0f)
				proportion = 0f;
			
		}
		
		if (mode.Equals(TWEENMODE.BACK_AND_FORTH))
		{
			if (proportion <= 0f || proportion >= 1.0f)
				isForward = !isForward;
		}
	}
}

using UnityEngine;
using System.Collections;

public class ScrollListManager : MonoBehaviour {
	
	
	UIGrid grid;
	
	
	// Use this for initialization
	void Start () {
		grid = transform.GetComponent<UIGrid>();
		
		//SortItems ();
		//addDynamic ();
	}
	
	void addDynamic()
	{
		for (int i =0 ; i < 4; i++)
		{
			GameObject go = (GameObject) Instantiate(Resources.Load ("ClothItem") as GameObject);
			
			go.GetComponent<UISprite>().spriteName = "clothitem"+(i+1);
			go.transform.parent = transform;
			go.transform.localRotation = GameObject.Find("ClothReference").transform.localRotation;
			go.transform.localScale = GameObject.Find("ClothReference").transform.localScale;
		}
		
		transform.GetComponent<UIDraggablePanel>().repositionClipping = true;
		grid.repositionNow = true;
	}
	
	void SortItems()
	{
		
		
		
		Transform[] children = transform.GetComponentsInChildren<Transform>();
		
		for (int i = 0; i < children.Length; i++)
		{
			// i[0] is self
			if (i == 0) continue;
			
			children[i].gameObject.name = "TrainingItem"+i;

		}
		
		grid.repositionNow = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

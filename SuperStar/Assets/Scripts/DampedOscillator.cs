using UnityEngine;
using System.Collections;

public class DampedOscillator : MonoBehaviour
{
	public int periods = 5;
	public float time = 0.8f;
	Vector3 originalPosition, temp;
	
	public float amplitude = 30f;
	float ampFactor = 0f;
	
	float t;
	
	// Use this for initialization
	void Start ()
	{
		originalPosition = transform.localPosition;
		
		InvokeRepeating("Move", 0f, 0.01f);
	}
	
	// Update is called once per frame
	void Move ()
	{
		if (ampFactor <= 0f) return;
		
		temp = transform.localPosition;
		temp.x = originalPosition.x + Mathf.Sin(2f*Mathf.PI * t * periods) * amplitude * ampFactor;
		transform.localPosition = temp;
		
		t += 0.01f/time;
		ampFactor = Mathf.Max(0f, ampFactor - 0.01f/time);
		
	}
	
	public void StartOscillate()
	{
		t= 0f;
		ampFactor = 1f;
	}
	
}


using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class MessageScrollListManager : MonoBehaviour
{
	float yPos = 0f;
	public List<MessageItem> messageItems = new List<MessageItem>();
	
	void Start()
	{
	}
	
	public void LoadMessages()
	{
		messageItems.Clear();
		int childs = transform.childCount;

        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
		SpringPanel.Begin(transform.gameObject, new Vector3(0f,220f,0f), 220f);
		
		yPos = 0f;
		
		
		
		
		List<Message> msgs = Globals.messages;
		
		// get all messages from
		for (int i =0 ; i < msgs.Count; i++)
		{
			
			GameObject go = (GameObject) Instantiate(Resources.Load ("MessageItem") as GameObject);
		
			go.name = "MessageItem"+i;
			go.transform.parent = transform;
			
			go.transform.localPosition = new Vector3(0, yPos,-5f);
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = new Vector3(1f,1f, 1f);
			
			yPos -= 76f;
			
			MessageItem mi = go.GetComponent<MessageItem>();
			
			mi.friendName = msgs[i].oppositePlayerName;
			mi.message = msgs[i];
			mi.SetType(msgs[i].accepted);
			
			messageItems.Add(mi);
			
			
		}
		
		
		
	}
	
	public void RemoveTickedMessages()
	{
		List<MessageItem> returnMe = new List<MessageItem>();
		
		foreach (MessageItem mi in  messageItems)
		{
			if (!mi.selected)
			{
				returnMe.Add(mi);
			}
		}
		
		messageItems.Clear();
		
		int childs = transform.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
		SpringPanel.Begin(transform.gameObject, new Vector3(0f,220f,0f), 220f);
		
		yPos = 0f;
		
		
		List<Message> replaceMe = new List<Message>();
		
		
		for (int i =0 ; i < returnMe.Count; i++)
		{
			
			GameObject go = (GameObject) Instantiate(Resources.Load ("MessageItem") as GameObject);
		
			go.name = "MessageItem"+i;
			go.transform.parent = transform;
			
			go.transform.localPosition = new Vector3(0, yPos,-5f);
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = new Vector3(1f,1f, 1f);
			
			yPos -= 76f;
			
			MessageItem mi = go.GetComponent<MessageItem>();
			
			mi.friendName = returnMe[i].message.oppositePlayerName;
			mi.message = returnMe[i].message;
			mi.SetType(returnMe[i].message.accepted);
			
			messageItems.Add(mi);
			replaceMe.Add(returnMe[i].message);
			
			
			
		}
		
		Globals.messages = replaceMe;
	}

	
	
	
}



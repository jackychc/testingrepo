using UnityEngine;
using System.Collections;

public class LoadingFlower : MonoBehaviour
{
	
	public GameObject flowerDot;
	public GameObject labelTextObj;
	public string description = "";
	float flowerWidth 	= 27f;
	public float speed 	= 1f;
	public bool isActive;
	
	Vector3 flowerVector = new Vector3(0f,0f,0f);
	int[] x = new int[]{0,1,1,1,0,-1,-1,-1}; //start from 12 o'clock
	int[] y = new int[]{1,1,0,-1,-1,-1,0,1}; 
	
	int pos;
	int originalLayer;

	// Use this for initialization
	void Start ()
	{
		flowerVector = flowerDot.transform.localPosition;
		InvokeRepeating("Loop", 0.0f, 0.125f/speed);

		if (description != null && !description.Equals(""))
		{
			UISysFontLabel labelText = labelTextObj.GetComponent<UISysFontLabel>();
			labelText.Text = description;

			labelTextObj.SetActive(true);
		}
	}
	
	public void Show()
	{
		isActive = true;
		transform.localScale = Vector3.one;
	
	}
	
	public void Hide()
	{
		isActive = false;
		transform.localScale = Vector3.zero;
		
		//GameObject.Find("UICamera").GetComponent<UICamera>().eventReceiverMask = originalLayer;
	}
	
	void Destroy()
	{
		Hide();	
	}
	
	private void Loop()
	{
		pos = (pos+1)%8;
		flowerVector.x = x[pos] * flowerWidth;
		flowerVector.y = y[pos] * flowerWidth; 
		flowerDot.transform.localPosition = flowerVector;
		
		//yield return new WaitForSeconds((0.125f/speed));
		
		//StartCoroutine(Loop ());
	}
}


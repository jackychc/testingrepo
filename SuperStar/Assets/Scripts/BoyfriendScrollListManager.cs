using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class BoyfriendScrollListManager : MonoBehaviour
{
	float yPos = 0f;
	public List<BoyfriendListItem> bfItems = new List<BoyfriendListItem>();
	
	void Start()
	{
		
		
		StartCoroutine(LoadBoyfriends());
	}
	
	public void Refresh()
	{
		//StartCoroutine(LoadBoyfriends());
		foreach (BoyfriendListItem bli in bfItems)
		{
			bli.SetInfo();
		}
	}
	
	public IEnumerator LoadBoyfriends()
	{
		//friendRequests.Clear();
		int childs = transform.childCount;

        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
		SpringPanel.Begin(transform.gameObject, new Vector3(0f,142f,0f), 142f);
		
		yPos = 0f;
		// get all fds from server first?
		
		List<Boyfriend> sortedList = new List<Boyfriend>();
		
		for (int i = 0 ; i < Boyfriends.boyfriendsList.Count; i++)
		{
			if (Boyfriends.boyfriendsList[i].rewardTimer != -1)
				sortedList.Add(Boyfriends.boyfriendsList[i]);
		}
		for (int i = 0 ; i < Boyfriends.boyfriendsList.Count; i++)
		{
			if (Boyfriends.boyfriendsList[i].rewardTimer == -1)
				sortedList.Add(Boyfriends.boyfriendsList[i]);
		}
		
		
		for (int i =0 ; i < sortedList.Count; i++)
		{
			
			GameObject go = (GameObject) Instantiate(Resources.Load ("BoyfriendListItem") as GameObject);
		
			go.name = "BoyfriendListItem"+i;
			go.transform.parent = transform;
			
			go.transform.localPosition = new Vector3(0, yPos,0f);
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = new Vector3(1f,1f, 1f);
			
			yPos -= 133f;
			
			BoyfriendListItem bli = go.GetComponent<BoyfriendListItem>();
			bli.boyfriend = sortedList[i];
			bli.SetBgType((i+1)%2);
			bli.bsm = this;
			
			bfItems.Add(bli);
		}
		
		//transform.GetComponent<UIGrid>().repositionNow = true;
		
		yield return null;
	}
	
	
	
}



using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class IAPPopup : Popup
{
	public enum IAPPackageIds
	{
		gemPackage1 = 200,
		gemPackage2 = 525,
		gemPackage3 = 1100,
		gemPackage4	= 2875,
		gemPackage5 = 6000,
		gemPackage6 = 12500,
		
		coinPackage1 = 10000,
		coinPackage2 = 26250,
		coinPackage3 = 55000,
		coinPackage4 = 143750,
		coinPackage5 = 300000,
		coinPackage6 = 625000
	}
	
	private enum State
	{
		Normal,
		Feature,
		IAP2X,
	}
	
	
	public int goToPage = 1;
	
	public GameObject expiryLabel;
	public GameObject offersObj;
	public GameObject[] pages  = new GameObject[3];
	public GameObject[] pageButtons = new GameObject[3];
	public GameObject[] pageTabs = new GameObject[3];
	public GameObject fullPage, notFullPage;
	
	public GameObject IAP2XHintsGroupObj;
	public GameObject IAP2XHintsDescObj;
	public GameObject bgSpriteObj;
	
	int gemsToRecoverEnergy = 80;
	private Dictionary<string, Feature> coinFeatureList = new Dictionary<string, Feature>();
	private Dictionary<string, Feature> gemFeatureList 	= new Dictionary<string, Feature>();
	private Feature closestFeature = null;
	private State currState = State.Normal;
	
	Vector3 normalPosition = new Vector3(-231f, -173.5f, -1f);
	Vector3 twoXPosition = new Vector3(-231f, -391.5f, -1f);
	
	public override void Start()
	{
		bool hasOffers = MunerisManager.SharedManager.HasOffers();
		offersObj.SetActive(hasOffers);
		
		base.Start();
	}	
	
	public override void Show()
	{
		base.Show();
		if (Globals.isIAP)
		{
			//Feature
			CheckFeature();
		}
		else
		{
			CheckIAP2X();
		}
		
		GoToPage(goToPage);
		transform.GetComponent<AnchorTweener>().Forward();		
	}
	
	public override void Hide()
	{
		PopupManager.HideLoadingFlower();
		
		base.Hide();		
		transform.GetComponent<AnchorTweener>().Backward();
		
	}
	
	public override void SilentHide()
	{		
		PopupManager.HideLoadingFlower();
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void OffersButtonClicked()
	{
		//Log.Debug("Offers Button Clicked");
		MunerisManager.SharedManager.ShowOffers();
	}
	
	public void BuyGemProd1()
	{
		BuyGemProd(1);
	}
	public void BuyGemProd2()
	{
		BuyGemProd(2);
	}
	public void BuyGemProd3()
	{
		BuyGemProd(3);
	}
	public void BuyGemProd4()
	{
		BuyGemProd(4);
	}
	public void BuyGemProd5()
	{
		BuyGemProd(5);
	}
	public void BuyGemProd6()
	{
		BuyGemProd(6);
	}
	public void BuyGemProd(int prod)
	{
		BuyProd("gemPackage" + prod);
	}
	
	public void BuyCoinProd1()
	{
		BuyCoinProd(1);
	}
	public void BuyCoinProd2()
	{
		BuyCoinProd(2);
	}
	public void BuyCoinProd3()
	{
		BuyCoinProd(3);
	}
	public void BuyCoinProd4()
	{
		BuyCoinProd(4);
	}
	public void BuyCoinProd5()
	{
		BuyCoinProd(5);
	}
	public void BuyCoinProd6()
	{
		BuyCoinProd(6);
	}
	public void BuyCoinProd(int prod)
	{		
		BuyProd("coinPackage" + prod);
	}
	
	private void BuyProd(string packageId)
	{
		Log.Debug("Buy Product: " + packageId);
		PopupManager.ShowLoadingFlower();
		
		MunerisManager.SharedManager.MakePurchase(packageId);
		
		//Log Flurry Event
		Hashtable iapParam = new Hashtable();
		iapParam["Package"] = packageId;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.In_App_Purchase_Requested, iapParam);
		
		if (!Globals.isIAP)
		{
			MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.In_App_Purchase_Unique_User);
		}
	}
	
	public void GoToPage1()
	{
		GoToPage(1);
	}
	public void GoToPage2()
	{
		GoToPage(2);
	}
	public void GoToPage3()
	{
		GoToPage(3);
	}
	public void GoToPage(int page)
	{
		//Change the size of the buttons and colliders
		float xPos = -250f;
		for (int i = 1; i <= 3; i++)
		{
			pages[i-1].SetActive(i == page);
			pageTabs[i-1].SetActive(i == page);
			
			pageTabs[i-1].transform.localPosition = new Vector3(
					xPos,
					pageTabs[i-1].transform.localPosition.y,
					pageTabs[i-1].transform.localPosition.z
				);
			pageButtons[i-1].transform.localPosition = pageTabs[i-1].transform.localPosition;
			
			BoxCollider bc = pageButtons[i-1].GetComponent<BoxCollider>();
			Vector3 box = bc.size;
			box.x = (i == page? 123 : 81);
			box.y = (i == page? 116 : 54);
			bc.size = box;
			box = bc.center;
			box.x = (i == page? 59: 38);
			bc.center = box;
			
			xPos += (i == page? 123 : 81);
		}
		
		//Stop all the update when switch to other page
		CancelInvoke("UpdateExpireTime");
		closestFeature = null;
		
		Log.Debug("IAPPopup_GoToPage: Globals.isIAP: " + Globals.isIAP);
		if (page != 3)
		{
			if (Globals.isIAP)
			{
				if (bgSpriteObj != null)
					bgSpriteObj.SetActive(true);
				if (IAP2XHintsGroupObj != null)
					IAP2XHintsGroupObj.SetActive(false);
				
				//Check Feature List				
				Log.Debug("IAPPOopup_GoToPage: can show feature");
				GameObject pageObj = pages[page - 1];
				if (pageObj != null)
				{
					Dictionary<string, Feature> featureList = new Dictionary<string, Feature>();
					if (page == 1)
					{
						featureList = gemFeatureList;
					}
					else
					{
						featureList = coinFeatureList;
					}
					
					//Check if features exists
					if (featureList.Count > 0)
					{
						Log.Debug("IAPPOopup_GoToPage: Have Feature in page");
						DateTime closestExpiry = DateTime.MaxValue;
						
						for (int i = 0; i < pageObj.transform.childCount; i++)
						{
							GameObject packageObj 	= pageObj.transform.GetChild(i).gameObject;
							string packageObjName 	= packageObj.name;
							
							if (packageObjName.IndexOf("coinPackage") != -1 || packageObjName.IndexOf("gemPackage") != -1)
							{
								GameObject discountGroupObj 	= packageObj.transform.FindChild("DiscountGroup").gameObject;
								GameObject specialTagGroupObj 	= packageObj.transform.FindChild("SpecialTagGroup").gameObject;
								string packageId = packageObj.name;
							
								List<string> packageIds = new List<string>(featureList.Keys);
								if (packageIds.Contains(packageId))
								{
									Feature f = featureList[packageId];
									
									//Check the earliest Expiry Date
									if (DateTime.Compare(closestExpiry, f.EndDate) > 0)
									{
										closestExpiry = f.EndDate;	
										closestFeature = f;
									}
									
									//Changing the UI
									if (discountGroupObj != null)
									{
										GameObject priceLabelObj = discountGroupObj.transform.FindChild("priceLabel").gameObject;
										if (priceLabelObj != null)
										{
											int increment = (int)((int)Enum.Parse(typeof(IAPPackageIds), packageId) * f.discount * 0.01);
											priceLabelObj.GetComponent<UILabel>().text = "+ " + increment;
										}
										discountGroupObj.SetActive(true);
									}
									
									if (specialTagGroupObj != null)
										specialTagGroupObj.SetActive(false);
								}
								else
								{
									discountGroupObj.SetActive(false);
									specialTagGroupObj.SetActive(false);
								}	
							}						
						}
						
						if (expiryLabel != null)
							expiryLabel.SetActive(true);
						InvokeRepeating("UpdateExpireTime", 0f, 1f);
					}
					else
					{
						if (expiryLabel != null)
							expiryLabel.SetActive(false);	
					}
				}
			}
			else
			{
				if (bgSpriteObj != null)
					bgSpriteObj.SetActive(false);
				
				//2X welcome reward
				if (IAP2XHintsGroupObj != null)
				{
					string spSymbol = Language.Get("msg_specialSymbol1");
					string terms1 	= spSymbol + "  " + Language.Get("atmBubble_term1");
					string terms2 	= spSymbol + "  " + Language.Get("atmBubble_term2");
					string terms3 	= spSymbol + "  " + Language.Get("atmBubble_term3");
					string terms4 	= spSymbol + "  " + Language.Get("atmBubble_term4");
					string terms 	= terms1 + "\r\n" + 
									  terms2 + "\r\n" +
									  terms3 + "\r\n" +
									  terms4;
					
					if (IAP2XHintsDescObj != null)
						IAP2XHintsDescObj.GetComponent<UISysFontLabel>().Text = terms.Replace("[Gem image]", Language.Get("currency_gems")).Replace("[Coin image]", Language.Get("currency_coins"));
					IAP2XHintsGroupObj.SetActive(true);
					
					if (expiryLabel != null)
						expiryLabel.SetActive(true);				
					
					InvokeRepeating("UpdateExpireTime", 0f, 1f);
				}
			}
		}
		else
		{
			//Disable the Expire Time
			if (expiryLabel != null)
				expiryLabel.SetActive(false);
			if (IAP2XHintsGroupObj != null)
				IAP2XHintsGroupObj.SetActive(false);
			if (bgSpriteObj != null)
				bgSpriteObj.SetActive(true);
			
			bool isFull = (Globals.mainPlayer.Energy == Globals.mainPlayer.MaxEnergy);
			
			fullPage.SetActive(isFull);
			notFullPage.SetActive(!isFull);
		}
	}
	
	private void UpdateExpireTime()
	{
		if (currState.Equals(State.Feature))
		{
			if (closestFeature != null)
			{
				TimeSpan remainingTime = closestFeature.CalculateRemainSeconds();
				string dateString = string.Format("{0:00}:{1:00}:{2:00}", remainingTime.Hours, remainingTime.Minutes, remainingTime.Seconds);
				if (remainingTime.Days > 0)
				{
					dateString = remainingTime.Days + " " + Language.Get("deal_days") + " " + dateString;
				}
				
				expiryLabel.GetComponent<UISysFontLabel>().Text = Language.Get("msg_dealexpires") + " " +  dateString;
				expiryLabel.transform.localPosition = normalPosition;
			}
		}
		else if (currState.Equals(State.IAP2X))
		{
			DateTime startPlayTime 	= DateTime.Parse(Globals.startPlayTime);
			DateTime expireTime		= startPlayTime.AddDays(Globals.IAP2XDuration);
			DateTime currTime 		= DateTime.UtcNow;
			TimeSpan remainingTime	= expireTime - currTime;
			
			string dateString = string.Format("{0:00}:{1:00}:{2:00}", remainingTime.Hours, remainingTime.Minutes, remainingTime.Seconds);
			if (remainingTime.Days > 0)
			{
				dateString = remainingTime.Days + " " + Language.Get("deal_days") + " " + dateString;
			}
			
			expiryLabel.GetComponent<UISysFontLabel>().Text = Language.Get("msg_dealexpires") + " " +  dateString;
			expiryLabel.transform.localPosition = twoXPosition;
		}
	}
	
	private void CheckIAP2X()
	{
		string startPlayTimeStr = Globals.startPlayTime;
		if (startPlayTimeStr != null && !startPlayTimeStr.Equals(""))
		{
			DateTime startPlayTime 	= DateTime.Parse(startPlayTimeStr);
			DateTime currTime 		= DateTime.UtcNow;
			TimeSpan timeDiff 		= currTime - startPlayTime;
			
			if (timeDiff.TotalDays > Globals.IAP2XDuration)
			{
				currState = State.Normal;
			}
			else
			{
				currState = State.IAP2X;
			}
		}
		else
			currState = State.Normal;
	}
	
	private void CheckFeature()
	{		
		if (Features.featuresList != null && Features.featuresList.Count > 0)
		{
			for (int i = 0; i < Features.featuresList.Count; i++)
			{
				Feature f = Features.featuresList[i];
				if (f.isRunning() && f.featureType.Equals(Feature.FEATURETYPE.IAP))
				{
					List<string> iapPackageIds = f.iapPackageIds;
					for (int j = 0; j < iapPackageIds.Count; j++)
					{
						string iapPackageId = iapPackageIds[j];
						if (iapPackageId.IndexOf("coinPackage") != -1)
							coinFeatureList.Add(iapPackageId, f);
						else if (iapPackageId.IndexOf("gemPackage") != -1)
							gemFeatureList.Add(iapPackageId, f);	
					}
				}
			}
			
			currState = State.Feature;
		}		
	}
	
	public void BuyEnergy()
	{
		Log.Debug("buy energy");
		
		if (Globals.mainPlayer.Gems >= gemsToRecoverEnergy)
		{
		
			Globals.mainPlayer.GainEnergy(Globals.mainPlayer.MaxEnergy);
			Globals.mainPlayer.GainGems(-gemsToRecoverEnergy);
			GoToPage(3);
			
			SoundManager.SharedManager.PlaySFX("job_click_award");

			Scenes currScene = SceneManager.SharedManager.CurrScene;

			if (currScene.Equals(Scenes.BOYFRIEND))
				MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Recover_Energy_Gem_BF_Scene);
			else if (currScene.Equals(Scenes.MINIGAMECLOTHMATCH))
				MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Recover_Energy_Gem_Runway_Scene);
			else if (currScene.Equals(Scenes.MINIGAMECOINDROP))
				MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Recover_Energy_Gem_DropCoin_Scene);
			else
				MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Recover_Energy_Gem_Other);
		}
		else
		{
			GoToPage(1);
		}
	}


}

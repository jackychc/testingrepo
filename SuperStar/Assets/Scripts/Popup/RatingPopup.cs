using UnityEngine;
using System.Collections;

public class RatingPopup : Popup {

	public override void Show()
	{
		base.Show();
		transform.GetComponent<AnchorTweener>().Forward();

	}

	public override void Hide()
	{
		base.Hide();
		transform.GetComponent<AnchorTweener>().Backward();

	}

	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}

	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}

	public void OnRateOKClicked()
	{
		Globals.canShowRatePopup = false;
		Globals.mainPlayer.Save();
		
#if UNITY_ANDROID
		Application.OpenURL(Globals.googlePlayURL);
#elif UNITY_IPHONE
		Application.OpenURL(Globals.itunesURL);
#endif
		Hide();
	}

	public void OnRateLaterClicked()
	{
		Hide();
	}
}

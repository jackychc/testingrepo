using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AchievementListPopup : Popup
{
	public int initialPage = 1;
	public Player profile;
	
	public GameObject[] jobButtons = new GameObject[5];
	public GameObject[] jobPointers = new GameObject[5];
	
	public UISprite[] icons = new UISprite[4];
	public UISysFontLabel[] texts = new UISysFontLabel[4];
	public UIFilledSprite[] stars = new UIFilledSprite[20];
	
	
	public override void Show()
	{
		base.Show();
		
		
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		StartCoroutine(DelayedStart());
	}
	
	public override void SilentHide()
	{
		base.SilentHide();
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void Hide()
	{
		
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public IEnumerator DelayedStart()
	{
		yield return new WaitForSeconds(0.03f);
		
		GoToPage(initialPage);
	}
	
	public void GoToPage(int p)
	{
		int skillId = p-1;
		
		List<SkillTier> allTiersOfType = SkillTable.GetAllSkillTiersOfType(skillId);
		
		if (allTiersOfType[0].skillAmount == 0)
			allTiersOfType.RemoveAt(0);
		
		for(int i = 0; i < 5; i++)
		{
			if (jobButtons[i] == null) continue;
			
			
			jobButtons[i].SetActive(i != skillId);
			jobPointers[i].SetActive(i == skillId);
			
		}
		
		string logoName = "";
		
		switch (skillId)
		{
			case 0: logoName = "job_title_tennis_"; break;
			case 1: logoName = "job_title_dance_"; break;
			case 2:
			case 3:
			case 4: logoName = "job_title_singer_" ; break;
			default: logoName = "job_title_tennis_" ; break;
		}
		
		for (int i = 0; i < 4; i++)
		{
			icons[i].spriteName = logoName + "" + (i+1);
			icons[i].MakePixelPerfect();
			
			texts[i].Text = Language.Get(allTiersOfType[i*5].skillTierString);
		}
		
		
		
		int currentSkill = profile.getSkill().skill[skillId];
		//SkillTier currentTier = SkillTable.GetSkillTier(skillId, currentSkill);
		//SkillTier nextTier = SkillTable.GetNextSkillTier(skillId, currentSkill);
		
		for (int i = 0; i < allTiersOfType.Count; i++)
		{
			//Log.Debug("i = "+i+","+allTiersOfType[i].skillAmount);
			
			if (currentSkill >= allTiersOfType[i].skillAmount)
			{
				// colorful star
				stars[i].fillAmount = 1f;
			}
			else
			{
				//if (i != allTiersOfType.Count-1)
				//	Log.Debug("values:"+allTiersOfType[i].skillAmount+","+ currentSkill +","+allTiersOfType[i+1].skillAmount);
					
				int leftBound = (i > 0)? allTiersOfType[i-1].skillAmount : 0;
				// here assume i-1 must exist
				if (i > 0 && 
					leftBound <= currentSkill &&  currentSkill < allTiersOfType[i].skillAmount)
				{
					
					stars[i].fillAmount = Mathf.InverseLerp(allTiersOfType[i-1].skillAmount, allTiersOfType[i].skillAmount, currentSkill);
				}
				
				
				
				else
				// hide star
					stars[i].fillAmount = 0f;
			}
		}
		
		
		
		
	}
	
	public void GoToPage1()
	{
		GoToPage(1);
	}
	
	public void GoToPage2()
	{
		GoToPage(2);
	}
	
	public void GoToPage5()
	{
		GoToPage(5);
	}
	
	
}

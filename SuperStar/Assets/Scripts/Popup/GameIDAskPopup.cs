using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class GameIDAskPopup : Popup
{

	public override void Show()
	{
		base.Show();
		transform.GetComponent<AnchorTweener>().Forward();
		
	}
	
	public override void Hide()
	{
		base.Hide();
		transform.GetComponent<AnchorTweener>().Backward();		
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void TickClicked()
	{
		StartCoroutine(GetGameIdRoutine());
	}
	
	IEnumerator GetGameIdRoutine()
	{
		string result = "";
		
		PopupManager.ShowLoadingFlower();
		
		yield return StartCoroutine(GenerateGameId(res => result = res));
		
		PopupManager.HideLoadingFlower();
		
//		Globals.gameIdLock = false;
		
		Globals.mainPlayer.GameId = result;
		
		Hide ();
	}
	
	IEnumerator GenerateGameId(Action<String> gameId)
	{
		NetworkManager.State state	= NetworkManager.State.None;
		string response 			= "";
		string returnMe 			= "";
		string deviceId 			= MunerisManager.SharedManager.GetDeviceId();
		string checkSum 			= MD5Hash.GetServerCheckSumWithString("GenerateID", deviceId);	
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("deviceId", deviceId);
		kvp.Add("isDebugMode", Globals.isDebugMode.ToString());
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "GenerateID.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		response	= NetworkManager.SharedManager.Response;
		state 		= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				// i guess need some parsing
				try
				{
					ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
					
					if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
					{
						returnMe = (String) al[1];
						
						PopupManager.ShowGameIDGetPopup(returnMe, false);
					}
					else
					{
						// some error 
						PopupManager.ShowInformationPopup("There is some error in obtaining Game ID. Please try again later.", false);
					}
				}
				catch (Exception e)
				{
					Log.Debug("GameID can't connect to server " + e);
					PopupManager.ShowInformationPopup("There is some error in obtaining Game ID. Please try again later.", false);
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("GenerateID", null, result => response = result));

		gameId(returnMe);
	}
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DLCBetweenPopup : Popup, DownloadManagerCallbackInterface
{
	//public UISysFontLabel messageLabel;
	public bool isBoundedByLevel;
	public int toDlcId;
	public int maxSize;

	public GameObject descLabel;
	
	public UIFilledSprite filledSpriteProgress;
	public UISysFontLabel labelProgress;
	
	public delegate void OnDLCBetweenPopupClicked();
	public OnDLCBetweenPopupClicked click;
	
	private DownloadManager dlm;

	private bool canCancelQuit = true;
	
	public override void Show()
	{
		base.Show();		
		transform.GetComponent<AnchorTweener>().Forward();
	
		dlm = DownloadManager.SharedManager;
		dlm.SetCallback(this);



		// assumption 1.user is owning non-latest DLC
		// assumption 2.already checked DLC in street
		if (!dlm.isDownloading)
		{
			canCancelQuit = false;

			if (dlm.haveDLC)
			{
				dlm.ResetProgress();
				dlm.StartDLC(isBoundedByLevel, toDlcId, maxSize);

				canCancelQuit = true;
			}
			else //if (!dlm.haveDLC)
			{
				StartCoroutine(CheckDLC());
			}
		}
	}
	
	private IEnumerator CheckDLC()
	{
		bool has = false;
		yield return StartCoroutine(dlm.CheckDLC(isBoundedByLevel, toDlcId, maxSize, result=> has = result));

		if (has)
			dlm.StartDLC(isBoundedByLevel, toDlcId, maxSize);
		else
		{
			//Already most updated
			if (descLabel != null)
				descLabel.GetComponent<UISysFontLabel>().Text = Language.Get("dlc_latestVersion");
			
			labelProgress.gameObject.SetActive(false);
			filledSpriteProgress.transform.parent.gameObject.SetActive(false);
		}

		canCancelQuit = true;
	}
	
	public override void OnDestroy()
	{
		dlm.RemoveCallback(this);
		base.OnDestroy();
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{
		if (!canCancelQuit)
			return;

		//base.OnBackPressed();
		Clicked ();
	}
	
	public void Clicked()
	{
		if (!canCancelQuit)
			return;

		Hide ();
		
		if (click != null)
			click();
	}
	
	public void OnDownloadManagerDLCCompleted()
	{
		Hide();
	}
	
	public override void Update()
	{
		base.Update();
		
		if (dlm != null)
		{
			if (labelProgress != null)
				labelProgress.Text = Mathf.RoundToInt(dlm.GetApparentProgress() * 100f) + "%";
			if (filledSpriteProgress != null)
				filledSpriteProgress.fillAmount = dlm.GetApparentProgress();
		}	
	}
}

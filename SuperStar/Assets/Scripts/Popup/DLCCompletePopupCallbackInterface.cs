using UnityEngine;
using System.Collections;

public interface DLCCompletePopupCallbackInterface{

	void OnDLCCompletePopupCancelClicked();
	void OnDLCCompletePopupOKClicked();
}

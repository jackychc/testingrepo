using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ATMTermsPopup : Popup {
	
	public GameObject descObj;
	
	public override void Start()
	{
		string spSymbol = Language.Get("msg_specialSymbol1");
		string terms1 	= spSymbol + "  " + Language.Get("atmBubble_term1");
		string terms2 	= spSymbol + "  " + Language.Get("atmBubble_term2");
		string terms3 	= spSymbol + "  " + Language.Get("atmBubble_term3");
		string terms4 	= spSymbol + "  " + Language.Get("atmBubble_term4");
		string terms 	= terms1 + "\r\n\r\n" + 
						  terms2 + "\r\n\r\n" +
						  terms3 + "\r\n\r\n" +
						  terms4;
		
		if (descObj != null)
		{
			descObj.GetComponent<UISysFontLabel>().Text = terms.Replace("[Gem image]", Language.Get("currency_gems")).Replace("[Coin image]", Language.Get("currency_coins"));
		}
		base.Start();
	}
	
	public override void Show()
	{
		base.Show();
		transform.GetComponent<AnchorTweener>().Forward();
	}
	
	public override void Hide()
	{
		transform.GetComponent<AnchorTweener>().Backward();
		base.Hide();
	}
	
	public override void SilentHide()
	{
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();	
	}
}

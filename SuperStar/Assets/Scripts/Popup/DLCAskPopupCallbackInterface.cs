using UnityEngine;
using System.Collections;

public interface DLCAskPopupCallbackInterface {
	void OnDLCAskPopupYes();
	void OnDLCAskPopupNo();
}

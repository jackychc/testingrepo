using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class VotingReadyPopup : Popup
{
	//public UISysFontLabel messageLabel;
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		//messageLabel.Text = Language.Get("vote_confirmandjoin");
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void Join()
	{
		Log.Debug("join!");
		base.Hide();
		
		
		transform.GetComponent<AnchorTweener>().Backward();
		
		//SceneManager.SharedManager.LoadScene(Scenes.VOTINGCAPTURE, true);
		//transform.GetComponent<SceneManager>().GoToScene();
		//StartCoroutine(JoinTopicOnServer());
		
		
	}
	
	
}

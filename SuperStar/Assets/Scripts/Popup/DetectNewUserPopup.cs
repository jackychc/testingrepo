using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DetectNewUserPopup : Popup, FacebookManagerCallbackInterface, GooglePlusManagerCallbackInterface, TwitterManagerCallbackInterface
{
	public SocialNetwork.Type type;
	public Hashtable socialInfo;
	private string socialNetworkId;
	
	public UISysFontLabel labelTitle;
	public UISysFontLabel labelDesc;
	
	public UISysFontLabel labelName;	
	public GameObject profilePicSpriteObj;
	public GameObject profilePicTextureObj;
	
	private void Init()
	{
		if (socialInfo != null)
		{
			string profilePicURL = "";
			string displayName = "";
			string socialTypeStr = "";
			
			if (type.Equals(SocialNetwork.Type.Facebook))
			{
				socialNetworkId 	= "" + socialInfo["id"];
				displayName 			= "" + socialInfo["name"];
				profilePicURL 			= FacebookManager.GetProfilePicURL(socialNetworkId, 100, 100);
				socialTypeStr 			= "Facebook";
			}
			else if (type.Equals(SocialNetwork.Type.GooglePlus))
			{
				socialNetworkId = "" + socialInfo["socialNetworkId"];
				displayName 	= "" + socialInfo["displayName"];
				profilePicURL 	= "" + socialInfo["profilePicURL"];
				socialTypeStr 	= "Google+";
			}
			else if (type.Equals(SocialNetwork.Type.Twitter))
			{
				socialNetworkId = "" + socialInfo["id"];
				displayName 	= "" + socialInfo["name"];
				profilePicURL 	= "" + socialInfo["profile_image_url"];
				socialTypeStr 	= "Twitter";
			}
			
			labelName.Text 		= displayName;
			labelTitle.Text		= Language.Get("msg_detectNewSave").Replace("%1", socialTypeStr);
			labelDesc.Text		= Language.Get("msg_detectNewSaveDesc").Replace("%1", socialTypeStr);
			StartCoroutine(GetProfilePic(profilePicURL));
		}		
	}
	
	private IEnumerator GetProfilePic(string url)
	{		
		Log.Debug(url);
		WWW www = new WWW(url);
		yield return www;
		
		if (www.error == null)
		{
			Texture2D texture = new Texture2D(100, 100);
			texture.LoadImage(www.bytes);
			
			if (texture != null)
			{
				Destroy(profilePicSpriteObj);
				
				UITexture uiTextureProfilePic = profilePicTextureObj.GetComponent<UITexture>();
				uiTextureProfilePic.mainTexture = texture;
			}
		}
		else
		{
			Log.Debug("Cannot fetch the image from the facebook url");	
		}
		
	}
	
	public override void Show()
	{
		Init();		
		base.Show();		
		transform.GetComponent<AnchorTweener>().Forward();
	}
	
	public override void OnDestroy()
	{
		FacebookManager.SharedManager.RemoveCallback(this);
		GooglePlusManager.SharedManager.RemoveCallback(this);
		TwitterManager.SharedManager.RemoveCallback(this);
		base.OnDestroy();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void Hide()
	{
		transform.GetComponent<AnchorTweener>().Backward();
		base.Hide();	
	}
	
	public override void OnBackPressed()
	{
		CrossPressed();
		//base.OnBackPressed();
	}
	
	public void TickPressed()
	{		
		Log.Debug("DetectNewUserPopup: Tick Clicked");

		//Update the Quest Status
		QuestFlags.SetFlag(5,true);

		PopupManager.HideAllPopup();
		PopupManager.ShowRestoreProgressPopup(type, socialInfo, true, false);
	}
	
	public void CrossPressed()
	{		
		if (type.Equals(SocialNetwork.Type.Facebook))
		{
			if (FacebookManager.SharedManager.IsLoggedIn())
			{
				FacebookManager.SharedManager.SetCallback(this);
				FacebookManager.SharedManager.Logout();
			}
		}
		else if (type.Equals(SocialNetwork.Type.GooglePlus))
		{
			if (GooglePlusManager.SharedManager.IsSignedIn())
			{
				GooglePlusManager.SharedManager.SetCallback(this);
				GooglePlusManager.SharedManager.SignOut();	
			}
		}
		else if (type.Equals(SocialNetwork.Type.Twitter))
		{
			if (TwitterManager.SharedManager.IsLoggedIn())
			{
				TwitterManager.SharedManager.SetCallback(this);
				TwitterManager.SharedManager.Logout();
			}
		}
	}
	
	#region FacebookManager Callback
	public void OnFacebookManagerLogout()
	{
		Hide();
	}
	
	public void OnFacebookManagerError(Exception e)
	{
		Hide();
	}
	
	public void OnFacebookManagerGetFriends(ArrayList friendList){}
	public void OnFacebookManagerLogin(string userId){}
	public void OnFacebookManagerLoginCancel(){}
	public void OnFacebookManagerUploadPhoto(){}
	public void OnFacebookManagerNewUserError(Hashtable userInfo){}
	public void OnFacebookManagerExistingUserError(Hashtable existingUserInfo){}
	#endregion
	
	#region GooglePlusManager Callback
	public void OnGooglePlusManagerSignOut(Hashtable errorObj)
	{
		Hide();
	}
	
	public void OnGooglePlusManagerError(Exception e)
	{
		Hide();
	}
	
	public void OnGooglePlusManagerSignIn(string userId){}
	public void OnGooglePlusManagerSignInCancelled(){}
	public void OnGooglePlusManagerSignInFailed(Hashtable errorObj){}	
	public void OnGooglePlusManagerGetFriends(ArrayList friendList){}
	public void OnGooglePlusManagerSharePhoto(){}
	public void OnGooglePlusManagerSharePhotoFailed(){}
	public void OnGooglePlusManagerNewUserError(Hashtable socialInfo){}
	public void OnGooglePlusManagerExistingUserError(Hashtable existingUserInfo){}
	#endregion
	
	#region TwitterManager Callback
	public void OnTwitterManagerLogout()
	{
		Hide();
	}
	
	public void OnTwitterManagerError(Exception e)
	{
		Hide();
	}
	
	public void OnTwitterManagerGetUserProfile(ArrayList userInfos){}
	public void OnTwitterManagerGetFriendIds(ArrayList friendIds){}
	public void OnTwitterManagerLoginFailed(){}
	public void OnTwitterManagerLogin(string userId){}
	public void OnTwitterManagerUploadPhoto(){}
	public void OnTwitterManagerUploadPhotoError(string error){}
	public void OnTwitterManagerNewUserError(Hashtable socialInfo){}
	public void OnTwitterManagerExistingUserError(Hashtable existingUserInfo){}
	#endregion	
}

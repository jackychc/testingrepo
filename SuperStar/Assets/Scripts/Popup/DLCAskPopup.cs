using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DLCAskPopup : Popup
{
	//public UISysFontLabel messageLabel;
	public delegate void OnDLCAskPopupYes();
	public delegate void OnDLCAskPopupNo();
	public OnDLCAskPopupYes yes;
	public OnDLCAskPopupNo no;
	public int type = 1;
	public bool canCancelQuit = true;
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
	}
	
	public override void Hide()
	{		
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{
		if (!canCancelQuit)
			return;
		//base.OnBackPressed();
		
		//if (no != null) no();
		
		CrossPressed();
	}
	
	public void CrossPressed()
	{
		if (!canCancelQuit)
			return;
		Hide();
		
		if (no != null) no();
	}
	
	public void TickPressed()
	{		
		Hide ();
		
		if (yes != null) yes();
	}
	
	
}

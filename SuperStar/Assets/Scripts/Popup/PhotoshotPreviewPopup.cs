using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class PhotoshotPreviewPopup : Popup {
	
	public UITexture texturePreview;
	public Texture2D screenshotTexture;
	
	public GameObject buttonControllerObj;
	public GameObject backButtonControllerObj;
	
	public delegate void OnPhotoshotPreviewPopupDiscard();
	public delegate void OnPhotoshotPreviewPopupShare();
	public delegate void OnPhotoshotPreviewPopupSave();
	public OnPhotoshotPreviewPopupDiscard discard;
	public OnPhotoshotPreviewPopupShare share;
	public OnPhotoshotPreviewPopupSave save;
	
	private bool isButtonControllerHide = false;
	
	#region Popup Method
	public override void Show()
	{
		//transform.GetComponent<AnchorTweener>().Forward();
		transform.GetComponent<UIAnchor>().enabled = false;
		transform.localPosition 	= new Vector3(0, 0, 0);
		transform.localEulerAngles 	= new Vector3(0, 0, 0);
		transform.localScale 		= new Vector3(1, 1, 1);
		base.Show();
		
		//Hide();
	}
	
	public override void Hide()
	{
		Log.Debug("Hide");
		//Disable the Anchor Tweener in the Button Controller
		buttonControllerObj.transform.parent.GetComponent<UIAnchor>().enabled = false;
		buttonControllerObj.GetComponent<PositionTweener>().enabled = false;
		
		backButtonControllerObj.transform.parent.GetComponent<UIAnchor>().enabled = false;
		backButtonControllerObj.GetComponent<PositionTweener>().enabled = false;
		
		
		//Destroy(screenshotTexture);
		//transform.GetComponent<AnchorTweener>().Backward();
		base.Hide();
	}
	
	public override void SilentHide()
	{
		//transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		Log.Debug("Back Button Pressed");
		
		if (discard != null)
			discard();
		
		base.OnBackPressed();
	}
	#endregion
		
	#region MonoBehaviour
	// Use this for initialization
	public override void Start () {
		Log.Debug("start");
		//Init the components
		if (screenshotTexture != null)
		{
			Vector2 aspectRatio = UtilsHelper.GetNGUIAspectRatioFromAspectRatio(new Vector2(screenshotTexture.width, screenshotTexture.height));
			
			texturePreview.material 			= new Material(Shader.Find("Unlit/Texture"));
		    texturePreview.material.mainTexture = screenshotTexture;
			texturePreview.transform.localScale = new Vector3(aspectRatio.x, aspectRatio.y, 1);
			
			Log.Debug("Screen shot resolution: " + aspectRatio.x + " " + aspectRatio.y);
		}
		base.Start();
	}
	#endregion
	
	#region Button Click
	public void ButtonDeleteClicked()
	{
		Log.Debug("Button Delete Clicked");
		
		if (discard != null)
			discard();
			
		Hide();
	}
	
	public void ButtonShareClicked()
	{
		Log.Debug("Button Share Clicked");
		if (share != null)
			share();
		
		PopupManager.ShowSharePhotoPopup(screenshotTexture, true);
	}
	
	public void ButtonSaveClicked()
	{		
		Log.Debug("Button Save Clicked");
		//Check if the file is saved
		if (screenshotTexture != null)
		{
			string storagePath 	= UtilsHelper.GetWriteableDirectory();
			string filePath 	= storagePath + Path.DirectorySeparatorChar + Globals.appName + Path.DirectorySeparatorChar;
			string fileNamePath = filePath + System.DateTime.Now.Ticks + ".png";
			
			if (!Directory.Exists(filePath))
				Directory.CreateDirectory(filePath);
			var imageBytes = screenshotTexture.EncodeToPNG();		
			File.WriteAllBytes(fileNamePath, imageBytes);
			UtilsHelper.RefreshDirectoryWithFileName(fileNamePath);
		}
		if (save != null)
			save();		
		
		//Hide();
		PopupManager.ShowInformationPopup(Language.Get("msg_photosaved"), true);
	}
	
	public void PreviewHideButtonClicked()
	{
		Log.Debug("PreviewHideButtonClicked");
		isButtonControllerHide = !isButtonControllerHide;
		
		if (isButtonControllerHide)
		{
			buttonControllerObj.GetComponent<PositionTweener>().Forward();
			backButtonControllerObj.GetComponent<PositionTweener>().Forward();
		}
		else
		{
			buttonControllerObj.GetComponent<PositionTweener>().Backward();
			backButtonControllerObj.GetComponent<PositionTweener>().Backward();
		}
	}
	#endregion
}

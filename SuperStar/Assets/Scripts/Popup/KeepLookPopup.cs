using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class KeepLookPopup : Popup
{
	
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
		
		Globals.mainPlayer.wearingItems = Globals.wearingBackup;
		//SceneManager.SharedManager.LoadScene(Scenes.VOTINGLOBBY, true);
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
		
		Globals.mainPlayer.wearingItems = Globals.wearingBackup;
		//SceneManager.SharedManager.LoadScene(Scenes.VOTINGLOBBY, true);
	}
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void Keep()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
		
		//SceneManager.SharedManager.LoadScene(Scenes.VOTINGLOBBY, true);
		//StartCoroutine(JoinTopicOnServer());
		
		
	}
	
	
}

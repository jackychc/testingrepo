using UnityEngine;
using System.Collections;

public interface TwitterLoginPopupCallbackInterface{
	void OnTwitterLoginPopupLoginSuccess();
	void OnTwitterLoginPopupLoginFailed();
}

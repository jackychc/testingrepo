using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DailyBonusPopup : Popup
{
	public DailyBonusBox[] boxes = new DailyBonusBox[1];
	public bool canClose = false;

	public override void Show()
	{
		base.Show();
		
		for (int i = 0; i < boxes.Length; i++)
		{
			boxes[i].Set(DailyBonus.getRewardOfDay(i+1));
		}
		
		transform.GetComponent<AnchorTweener>().Forward();
		

	}
	
	public override void SilentHide()
	{
		base.SilentHide();
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void Hide()
	{
		if (!canClose)
			return;
		
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}

	
	public override void OnBackPressed()
	{
		Hide();
	}


	
	
}

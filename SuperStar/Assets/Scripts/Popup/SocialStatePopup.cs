using UnityEngine;
using System.Collections;

public class SocialStatePopup : Popup {
	
	public UISprite socialSprite;
	public SocialNetwork.Type type;
	public UISysFontLabel labelDesc;
	
	public bool isErrorPop;
	public string description;
	
	private const string errorSpriteName = "error_icon";
	private const string googlePlusSpriteName = "setting_google+";
	private const string twitterSpriteName = "icon_twitter";
	private const string facebookSpriteName = "setting_facebook";
	
	public override void Start()
	{
		base.Start();
		
		if (!isErrorPop)
		{
			if (type == SocialNetwork.Type.Facebook)
				socialSprite.spriteName = facebookSpriteName;
			else if (type == SocialNetwork.Type.GooglePlus)
				socialSprite.spriteName = googlePlusSpriteName;
			else if (type == SocialNetwork.Type.Twitter)
				socialSprite.spriteName = twitterSpriteName;
		}
		else
		{
			socialSprite.spriteName = errorSpriteName;
		}
		
		labelDesc.Text = description;
	}
	
	public override void Show()
	{
		transform.GetComponent<AnchorTweener>().Forward();
		base.Show();
	}
	
	public override void Hide()
	{
		transform.GetComponent<AnchorTweener>().Backward();	
		base.Hide();			
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
}

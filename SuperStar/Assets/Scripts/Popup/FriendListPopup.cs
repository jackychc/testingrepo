using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FriendListPopup : Popup, FacebookManagerCallbackInterface, GooglePlusManagerCallbackInterface, TwitterManagerCallbackInterface, TwitterLoginPopupCallbackInterface
{
	//public UISysFontLabel searchByIdLabel;
	//public UISysFontLabel yourIdLabel;
	public UISysFontLabel connectLabel;
	public GameObject resultSuccessObj;
	public GameObject resultFailObj;
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		//searchByIdLabel.Text = Language.Get("friend_searchyourfds");
		//yourIdLabel.Text = Language.Get("friend_yourid");
		//connectLabel.Text = Language.Get("friend_connectmsg");
		//	StartCoroutine(GetFriends());

		connectLabel.Text = Language.Get("friend_connectmsg").Replace("%1", Globals.appName);
	}
	
	public override void Start()
	{
		base.Start();
		
		FacebookManager.SharedManager.SetCallback(this);
		TwitterManager.SharedManager.SetCallback(this);
		GooglePlusManager.SharedManager.SetCallback(this);
	}
	
	public override void OnDestroy()
	{
		TwitterManager.SharedManager.RemoveCallback(this);
		FacebookManager.SharedManager.RemoveCallback(this);
		GooglePlusManager.SharedManager.RemoveCallback(this);
		base.OnDestroy();
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
		PopupManager.HideLoadingFlower();
	}
	
	public override void SilentHide()
	{
		PopupManager.HideLoadingFlower();
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void StartTutorial()
	{
		QuestFlags.SetFlag(-103, 1);
		PopupManager.ShowTutorialPopup(3, false);
		PopupManager.ShowFriendListPopup(false);
		PopupManager.HideLatestPopup();
	}
	
	public void Social1Clicked()
	{
		PopupManager.ShowLoadingFlower();
		//resultSuccessObj.SetActive(false);
		//resultFailObj.SetActive(false);
		
		//facebook
		Log.Debug("FACEBOOK CLICKED");
		FacebookManager.SharedManager.GetFriendsOpenGraph();
	}
	
	public void Social2Clicked()
	{
		PopupManager.ShowLoadingFlower();
		//resultSuccessObj.SetActive(false);
		//resultFailObj.SetActive(false);
		
		// google	
		Log.Debug("GOOGLE CLICKED");
		GooglePlusManager.SharedManager.GetFriendsList();
	}
	
	public void Social3Clicked()
	{
		///twitter
		Log.Debug("TWITTER CLICKED");
		
		//resultSuccessObj.SetActive(false);
		//resultFailObj.SetActive(false);
		
		if (!TwitterManager.SharedManager.IsLoggedIn())
		{
			PopupManager.ShowTwitterLoginPopup(this, true);
			
		}
		else
		{		
			PopupManager.ShowLoadingFlower();
			TwitterManager.SharedManager.GetFriendsId();
		}
		//PopupManager.ShowTwitterLoginPopup("FriendListPopup", false);
		//PopupManager.ShowFriendListPopup(false);
		
		
		
		
	}
	
	#region TwitterLoginPopup Callback
	public void OnTwitterLoginPopupLoginSuccess()
	{
		PopupManager.ShowLoadingFlower();
		TwitterManager.SharedManager.GetFriendsId();
	}
	
	public void OnTwitterLoginPopupLoginFailed()
	{
		PopupManager.ShowSocialStatePopup(SocialNetwork.Type.Twitter, Language.Get("login_fail"), true, true);
	}
	#endregion
	
	#region Facebook Callback
	public void OnFacebookManagerGetFriends(ArrayList friendList)
	{
		PopupManager.HideLoadingFlower();
		
		//Checking for the Remaining friends to be added
		if (friendList != null)
		{
			ArrayList remainFdList = new ArrayList();
			for (int i = 0;i < friendList.Count; i++)
			{
				string uid = "" + friendList[i];
				Log.Debug(uid);	
				
				bool hasEqual = false;
				for (int j = 0; j < Friends.friends.Count; j++)
				{
					string fdFBId = Friends.friends[j].facebookElement.socialId;
					if (fdFBId != null && fdFBId.Equals(uid))
					{
						hasEqual = true;
						break;
					}
				}
				
				if (!hasEqual)
					remainFdList.Add(uid);
			}			
			
			if (remainFdList.Count > 0)
			{
				//resultSuccessObj.SetActive(true);
				//Upload the friend list to the server
				StartCoroutine(UploadSocialIdsToServer(remainFdList, "facebook"));
			}
		}
	}
	
	public void OnFacebookManagerError(Exception e)
	{
		PopupManager.HideLoadingFlower();
	}
	
	public void OnFacebookManagerLoginCancel()
	{
		PopupManager.HideLoadingFlower();
	}
	
	public void OnFacebookManagerLogin(string userId)
	{
		if (userId != null && !userId.Equals(""))
		{
			Globals.mainPlayer.facebookElement.socialId = userId;
			
			QuestFlags.SetFlag(5,true);
			//QuestFlags.var5_connectedToSocial = true;
		}
	}
	
	public void OnFacebookManagerNewUserError(Hashtable userInfo)
	{
		Log.Debug("Facebook Manager New User Error");
		PopupManager.HideLoadingFlower();
		PopupManager.HideAllPopup();
		
		PopupManager.ShowDetectNewUserPopup(userInfo, SocialNetwork.Type.Facebook, false);
		
		
		//OnAccountError(SocialNetwork.Type.Facebook);
	}
	
	public void OnFacebookManagerExistingUserError(Hashtable existingUserInfo)
	{
		Log.Debug("Facebook Manager Existing User Error");
		PopupManager.HideLoadingFlower();
		PopupManager.HideAllPopup();
		
		PopupManager.ShowDetectExistingUserPopup(existingUserInfo, SocialNetwork.Type.Facebook, false);
		//OnAccountError(SocialNetwork.Type.Facebook);
	}
	
	public void OnFacebookManagerLogout(){}
	public void OnFacebookManagerUploadPhoto(){}
	#endregion
	
	
	#region GooglePlus Callback
	public void OnGooglePlusManagerGetFriends(ArrayList friendList)
	{
		PopupManager.HideLoadingFlower();
		
		//Checking for the Remaining friends to be added
		if (friendList != null)
		{
			ArrayList remainFdList = new ArrayList();
			for (int i = 0;i < friendList.Count; i++)
			{
				string uid = "" + friendList[i];
				Log.Debug(uid);	
				
				bool hasEqual = false;
				for (int j = 0; j < Friends.friends.Count; j++)
				{
					string fdGoogleId = Friends.friends[j].googlePlusElement.socialId;
					if (fdGoogleId != null && fdGoogleId.Equals(uid))
					{
						hasEqual = true;
						break;
					}
				}
				
				if (!hasEqual)
					remainFdList.Add(uid);
			}			
			
			if (remainFdList.Count > 0)
			{
				//resultSuccessObj.SetActive(true);
				
				//Upload the friend list to the server
				StartCoroutine(UploadSocialIdsToServer(remainFdList, "google"));
				
				
			}
		}
	}
	
	public void OnGooglePlusManagerSignIn(string userId)
	{
		if (userId != null && !userId.Equals(""))
		{
			Globals.mainPlayer.googlePlusElement.socialId = userId;
			
			QuestFlags.SetFlag(5,true);
			//QuestFlags.var5_connectedToSocial = true;
		}
	}
	
	public void OnGooglePlusManagerSignInCancelled()
	{
		PopupManager.HideLoadingFlower();
	}
	
	public void OnGooglePlusManagerSignInFailed(Hashtable errorObj)
	{
		PopupManager.HideLoadingFlower();
	}
	
	public void OnGooglePlusManagerError(Exception e)
	{
		PopupManager.HideLoadingFlower();
		Log.Debug("GooglePlusManager Error " + e.Message.ToString());
	}
	
	public void OnGooglePlusManagerNewUserError(Hashtable socialInfo)
	{
		Log.Debug("GooglePlus Manager New User Error");
		PopupManager.HideLoadingFlower();		
		PopupManager.HideAllPopup();
		
		PopupManager.ShowDetectNewUserPopup(socialInfo, SocialNetwork.Type.GooglePlus, false);
		
		//OnAccountError(SocialNetwork.Type.GooglePlus);
	}
	
	public void OnGooglePlusManagerExistingUserError(Hashtable existingUserInfo)
	{
		Log.Debug("GooglePlus Manager Existing User Error");
		PopupManager.HideLoadingFlower();		
		PopupManager.HideAllPopup();
		
		PopupManager.ShowDetectExistingUserPopup(existingUserInfo, SocialNetwork.Type.GooglePlus, false);
		//OnAccountError(SocialNetwork.Type.GooglePlus);
	}
	
	public void OnGooglePlusManagerSignOut(Hashtable errorObj){}
	public void OnGooglePlusManagerSharePhoto(){}
	public void OnGooglePlusManagerSharePhotoFailed(){}
	#endregion
	
	#region Twitter Callback
	public void OnTwitterManagerGetFriendIds(ArrayList friendList)
	{
		Log.Debug("FriendList GetFriendsIds Twitter");
		PopupManager.HideLoadingFlower();
		
		//Checking for the Remaining friends to be added
		if (friendList != null)
		{
			ArrayList remainFdList = new ArrayList();
			for (int i = 0;i < friendList.Count; i++)
			{
				string uid = "" + friendList[i];
				
				bool hasEqual = false;
				for (int j = 0; j < Friends.friends.Count; j++)
				{
					string fdTwitterId = Friends.friends[j].twitterElement.socialId;
					if (fdTwitterId != null && fdTwitterId.Equals(uid))
					{
						hasEqual = true;
						break;
					}
				}
				
				if (!hasEqual)
					remainFdList.Add(uid);
			}			
			
			if (remainFdList.Count > 0)
			{
				
				//resultSuccessObj.SetActive(true);
				//Upload the friend list to the server
				StartCoroutine(UploadSocialIdsToServer(remainFdList, "twitter"));
			}
		}
	}
	
	public void OnTwitterManagerGetUserProfile(ArrayList userInfos){}	
	public void OnTwitterManagerLogin(string userId){}	
	public void OnTwitterManagerLoginFailed(){}	
	public void OnTwitterManagerError(Exception e){}
	public void OnTwitterManagerNewUserError(Hashtable socialInfo){}	
	public void OnTwitterManagerExistingUserError(Hashtable existingUserInfo){}	
	public void OnTwitterManagerLogout(){}
	public void OnTwitterManagerUploadPhoto(){}
	public void OnTwitterManagerUploadPhotoError(string error){}
	#endregion
	
	private void OnAccountError(SocialNetwork.Type type)
	{
		Log.Debug("Show Account Error Popup");
		string message = "";
		switch(type)
		{
			case SocialNetwork.Type.Facebook:
			{
				if (FacebookManager.SharedManager.IsLoggedIn())
					FacebookManager.SharedManager.SilentLogout();
				message = Language.Get("login_fail_settings").Replace("%1", "Facebook");
				break;
			}
			case SocialNetwork.Type.GooglePlus:
			{
				if (GooglePlusManager.SharedManager.IsSignedIn())
					GooglePlusManager.SharedManager.SilentSignOut();
				message = Language.Get("login_fail_settings").Replace("%1", "Google+");
				break;
			}
			case SocialNetwork.Type.Twitter:
			{
				if (TwitterManager.SharedManager.IsLoggedIn())
					TwitterManager.SharedManager.Logout();
				message = Language.Get("login_fail_settings").Replace("%1", "Twitter");
				break;
			}
		}
		PopupManager.ShowSocialStatePopup(type, message, true, false);
		PopupManager.ShowFriendListPopup(false);
		Hide();
	}
	
	
	#region Server Update
	private IEnumerator UploadSocialIdsToServer(object friendsIds, string socialType)
	{
		Log.Debug("FriendList Uploading the Ids to Server");
		var friendsJson = SuperstarFashionMiniJSON.JsonEncode(friendsIds);		
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + friendsJson;
		Log.Debug("FriendList friend's Json: " + friendsJson);
		
		string apiURL = "";
		if (socialType.Equals("facebook"))
			apiURL = "AddFriendsWithFacebookId";
		else if (socialType.Equals("google"))
			apiURL = "AddFriendsWithGooglePlusId";
		else if (socialType.Equals("twitter"))
			apiURL = "AddFriendsWithTwitterId";
		
		string checkSum = MD5Hash.GetServerCheckSumWithString(apiURL, checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("friendsSocialIds", friendsJson);
		kvp.Add("checkSum", checkSum);
		
		//yield return StartCoroutine(HTTPPost.Post(apiURL, kvp, result => response = result));
		
		string url = Globals.serverAPIURL + apiURL + ".php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				try
				{
					ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				
					if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
					{
						//Add Friends to local list
						Hashtable friendsHT = (Hashtable)al[1];
						if (friendsHT != null)
						{
							for (int i = 0; i < friendsHT.Count; i++)
							{
								Hashtable friendHT = (Hashtable)friendsHT["" + i];
								Friends.AddFriend(friendHT);
							}	
						}
					}
				}
				catch (Exception e)
				{
					Log.Debug(e);
				}
			}
		}
		
		//Add the Friends
		
		PopupManager.HideLoadingFlower();
	}	
	#endregion
	
}

using UnityEngine;
using System.Collections;

public class LevelUpPopup : Popup
{
	public Reward reward;
	public RewardRowsPositioner rrp;
	//public UISysFontLabel titleText;

	public UILabel newLevelLabel;
	
	private object rewardTicket = new object();
	private bool rewardClaimed = false;
	
	GameObject rewardEffect;
	
	public void setReward(Reward r)
	{
		reward = r;
		rrp.reward = r;
		
		//rrp.Position();
	}
	
	
	public override void Show()
	{
		lock(rewardTicket)
		{
			StartCoroutine(EnumRewardClaim());

			base.Show();
		
			// set reward rows
			//titleText.Text = Language.Get("levelup").Replace("%1", ""+Globals.mainPlayer.Level);
			newLevelLabel.text = "Lv "+ Globals.mainPlayer.Level;
		
			rewardEffect = (GameObject) Instantiate(Resources.Load("RewardEffectCamera") as GameObject);
			
			transform.GetComponent<AnchorTweener>().Forward();
			
			SoundManager.SharedManager.PlaySFX("level_up");
		}		
	}
	
	public override void Hide()
	{
		if (rewardClaimed)
			return;
		
		lock(rewardTicket)
		{
			rewardClaimed = true;
			
			//StartCoroutine(EnumRewardClaim());
			transform.GetComponent<AnchorTweener>().Backward();
			
			Destroy(rewardEffect);
			base.Hide();
		}
	}
	
	public override void SilentHide()
	{	
		if (rewardClaimed)
			return;
		
		lock(rewardTicket)
		{		
			rewardClaimed = true;
			
			//StartCoroutine(EnumRewardClaim());
			transform.GetComponent<AnchorTweener>().Backward();
			base.SilentHide();
		}
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}	
	public IEnumerator EnumRewardClaim()
	{
		reward.claim();
		yield return null;
	}
}

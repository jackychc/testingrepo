using UnityEngine;
using System.Collections;

public class InputNamePopup : Popup {
	
	public GameObject inputObj;
	public UIInput inputField;
	public UISysFontLabel labelName;
	
	private Vector3 inputObjOrigPos;	
	private InputNamePopupCallbackInterface callback;
	
	public void SetCallback(InputNamePopupCallbackInterface c)
	{
		this.callback = c;
	}
	
	// Use this for initialization
	public override void Start () {
		
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update();
		if (inputField != null)
		{
			labelName.Text = inputField.text;
		}
	}
	
	public void OnInputSubmit()
	{
		if (inputField != null)
		{
			labelName.Text = inputField.text;
		}		
	}
	
	private void ShowNoNameInputAnimation()
	{
		if (inputObj != null)	
		{
			TweenPosition tp = inputObj.GetComponent<TweenPosition>();	
			if (tp != null)
				Destroy(tp);
			
			tp = inputObj.AddComponent<TweenPosition>();
			inputObjOrigPos = inputObj.transform.localPosition;
			tp.duration	= 0.05f;
			tp.delay	= 0.0f;
			tp.from.x 	= inputObjOrigPos.x;
			tp.from.y 	= inputObjOrigPos.y + 5;
			tp.from.z 	= inputObjOrigPos.z;
			tp.to.x		= inputObjOrigPos.x;
			tp.to.y		= inputObjOrigPos.y - 5;
			tp.to.z		= inputObjOrigPos.z;
			tp.style	= UITweener.Style.Once;
			tp.eventReceiver = this.gameObject;
			tp.callWhenFinished	= "OnShowNoNameInputAnimationFinished";
			tp.enabled 	= true;
		}
	}
	
	public void OnShowNoNameInputAnimationFinished()
	{
		if (inputObj != null)	
		{
			TweenPosition tp = inputObj.GetComponent<TweenPosition>();	
			if (tp != null)
				Destroy(tp);
			
			tp = inputObj.AddComponent<TweenPosition>();
			tp.duration	= 0.05f;
			tp.delay	= 0.0f;
			tp.from.x 	= inputObj.transform.localPosition.x;
			tp.from.y 	= inputObj.transform.localPosition.y;
			tp.from.z 	= inputObj.transform.localPosition.z;
			tp.to.x		= inputObjOrigPos.x;
			tp.to.y		= inputObjOrigPos.y;
			tp.to.z		= inputObjOrigPos.z;
			tp.style	= UITweener.Style.Once;
			tp.enabled 	= true;
		}	
	}
	
	#region Button Handler
	public void TickClicked()
	{
		if (labelName != null)
		{
			string inputName = labelName.Text.Trim();
			if (inputName.Equals(""))
			{
				ShowNoNameInputAnimation();
				return;	
			}
		}
		
		//Record the name entered
		Log.Debug("The input name is: " + labelName.Text);			
		Hide();
		
		//Save the player's name
		Globals.mainPlayer.Name = labelName.Text;
		Globals.mainPlayer.InitializedPlayer = true;
		Globals.mainPlayer.Save();
		
		if (callback != null)
			callback.OnInputNamePopupOKButtonClicked();
	}
	
	public void OnInputFocus()
	{
		Log.Debug("Input focused");
		if (inputField != null)
			inputField.selected = true;
	}
	#endregion
	
	#region Popup
	public override void Show()
	{
		transform.GetComponent<AnchorTweener>().Forward();
		base.Show();
	}
	
	public override void Hide()
	{
		transform.GetComponent<AnchorTweener>().Backward();
		base.Hide();
	}
	
	public override void SilentHide()
	{
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		//base.OnBackPressed();
	}
	#endregion
}

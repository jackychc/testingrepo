using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DLCCompletePopup : Popup
{
	//public UISysFontLabel messageLabel;
	public delegate void OnDLCCompletePopupCancelClicked();
	public delegate void OnDLCCompletePopupOKClicked();
	
	public OnDLCCompletePopupCancelClicked cancel;
	public OnDLCCompletePopupOKClicked ok;
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
	}
	
	public override void Hide()
	{
		base.Hide();		
		transform.GetComponent<AnchorTweener>().Backward();
		
		if (cancel != null)
			cancel();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void TickPressed()
	{
		Hide();	
		if (ok != null)
			ok();
	}
	
	
}

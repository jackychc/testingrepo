using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class QuestSkipPopup : Popup
{
	Quest q;
	
	public void SetQuest(Quest pQuest)
	{
		q = pQuest;
	}
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		//messageLabel.Text = Language.Get("vote_confirmandjoin");
	}
	
	public override void Hide()
	{
		PopupManager.ShowQuestListPopup(false);
		//PopupManager.HideLatestPopup();
		
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{		
		base.OnBackPressed();
	}
	
	public void TickPressed()
	{
		
		
		//check if have $100
		if (Globals.mainPlayer.Coins >= 100)
		{
			Hashtable param = new Hashtable();
			param[q.activityId] = "Skipped";
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Quest_State, param);
			
			Globals.mainPlayer.GainCoins(-100);
			//really skip
			Quests.SkipQuest(q);
		
			// must do after skip quest
			q.StopQuest();
				
			//PopupManager.ShowQuestListPopup();
		}
		else
		{
			PopupManager.ShowNotEnoughCurrencyPopup("Coins", 100-Globals.mainPlayer.Coins, false);
		}
		
		PopupManager.HideLatestPopup();
		
		//SceneManager.SharedManager.LoadScene(targetScene, true);
		
		
	}
	
	
}

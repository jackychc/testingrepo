using UnityEngine;
using System.Collections;

public abstract class Popup : MonoBehaviour {
	
	enum ScaleAction
	{
		NONE,
		UP,
		DOWN
		
	};	
	public static readonly int popupEventMask 			= 8;
	private static object originalLayerTicket 	= new object();
	
	AnchorTweener tweener;
	ScaleAction action 				= ScaleAction.NONE;
	Vector3 originalScale 			= Vector3.one;
	float scaleMultiplier 			= 0.0f;
	bool hiding 					= false;
	bool showingOnScreen			= false;
	
	//Use this for Destructor
	public virtual void OnDestroy()
	{
		
	}
	
	
	public virtual void Awake()
	{
		
	}
	
	// Use this for initialization
	public virtual void Start () 
	{
		
	}
	
	// Update is called once per frame
	public virtual void Update ()
	{		
		if (!action.Equals(ScaleAction.NONE))
		{
			Log.Debug("Popup ScaleAction");
			transform.localScale = originalScale * scaleMultiplier;
			
			if (scaleMultiplier <= 0.0f || scaleMultiplier >= 1.0f)
			{
				if (action.Equals(ScaleAction.DOWN))
				{
					// destroy the popup after finishing scale down
					Destroy(transform.gameObject);
					return;
				}
				
				action = ScaleAction.NONE;
			}
			
			else
			{			
				if (action.Equals(ScaleAction.UP)) ScaleUp();
				if (action.Equals(ScaleAction.DOWN)) ScaleDown();
			}
		}
		else if (hiding)
		{
			if (tweener == null)
				tweener = transform.GetComponent<AnchorTweener>();
			
			if (Mathf.Approximately(tweener.getProportion(), 0.0f))
				Destroy(transform.gameObject);
		}
	}
	
	public virtual void Show()
	{
		lock(originalLayerTicket)
		{		
			//Assign the popup Event Mask
			GameObject uiCamera = GameObject.Find("UICamera");
			if (uiCamera != null)
				uiCamera.GetComponent<UICamera>().eventReceiverMask = 1 << popupEventMask;
			
			GameObject bgCamera = GameObject.Find("BGCamera");
			if (bgCamera != null)
				bgCamera.GetComponent<UICamera>().eventReceiverMask = 1 << popupEventMask;
			
			showingOnScreen = true;
		}
	}
	
	public virtual void SilentHide()
	{
		if (hiding)
			return;
		
		lock(originalLayerTicket)
		{
			hiding 			= true;
			showingOnScreen = false;
			
			//Restore the default Event Mask
			GameObject uiCamera = GameObject.Find("UICamera");
			if (uiCamera != null)
				uiCamera.GetComponent<UICamera>().eventReceiverMask = -1;
			
			GameObject bgCamera = GameObject.Find("BGCamera");
			if (bgCamera != null)
				bgCamera.GetComponent<UICamera>().eventReceiverMask = -1;
			
			
			if (PopupManager.PopupCount() > 0)
			{
				PopupManager.RemovePopup(0);
			}
		}
	}
	
	public virtual void Hide()
	{
		if (hiding)
			return;		
		
		lock(originalLayerTicket)
		{
			hiding 			= true;
			showingOnScreen = false;
			
			GameObject uiCamera = GameObject.Find("UICamera");
			if (uiCamera != null)
				uiCamera.GetComponent<UICamera>().eventReceiverMask = -1;
			
			GameObject bgCamera = GameObject.Find("BGCamera");
			if (bgCamera != null)
				bgCamera.GetComponent<UICamera>().eventReceiverMask = -1;

			//GameObject.Find("UICamera").GetComponent<UICamera>().eventReceiverMask = originalLayer;
			//ScaleDown();
			if (PopupManager.PopupCount() > 0)
			{
				PopupManager.RemovePopup(0);
			}
			if (PopupManager.PopupCount() >= 1)
			{
				Popup p = PopupManager.GetLatestPopup();
				if (!p.isShowingOnScreen())
					PopupManager.ShowLatestPopup();
			}
			
			Log.Debug("Popup Number of popup in popupQueue: " + PopupManager.PopupCount());
		}
	}
	
	public virtual void OnBackPressed()
	{
		Hide();
	}
	
	public virtual bool isShowingOnScreen()
	{
		return showingOnScreen;	
	}
	
	public virtual bool isHiding()
	{
		return (hiding == false);
	}
	
	public void ScaleUp()
	{
		
		
		/*action = ScaleAction.UP;
		
		scaleMultiplier = scaleMultiplier +  0.12f;
		
		scaleMultiplier = Mathf.Min(1.0f, scaleMultiplier);*/
		
		
		
	}
	
	public void ScaleDown()
	{
	
		action = ScaleAction.DOWN;
		
		scaleMultiplier = scaleMultiplier -  0.12f;
		
		scaleMultiplier = Mathf.Max(0.0f, scaleMultiplier);
		
		
	}
	
	public void SaveScale(Vector3 scale)
	{
		originalScale = scale;
	}
}

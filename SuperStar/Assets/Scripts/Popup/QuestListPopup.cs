using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class QuestListPopup : Popup
{
	public QuestScrollListManager qslm;
	
	public override void Show()
	{
		base.Show();
		
		Quests.RefreshQuestList();
		
		qslm.LoadQuests();
		
		
		transform.GetComponent<AnchorTweener>().Forward();
		
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
}

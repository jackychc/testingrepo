using UnityEngine;
using System.Collections;

public interface InputNamePopupCallbackInterface {

	void OnInputNamePopupOKButtonClicked();
}

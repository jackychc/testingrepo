using UnityEngine;
using System.Collections;

public class ChooseMinigamePopup : Popup
{
	string content = "";
	
	
	
	public override void Show()
	{
		base.Show();
		transform.FindChild("Text").gameObject.GetComponent<UISysFontLabel>().Text = content;
		transform.GetComponent<AnchorTweener>().Forward();
		
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
		
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void GoToCoinDropScene()
	{
		Hide ();
		
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAMECOINDROP, true);
	}
	
	public void GoToClothMatchScene()
	{
		Hide ();
		
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAMECLOTHMATCH, true);
	}
}

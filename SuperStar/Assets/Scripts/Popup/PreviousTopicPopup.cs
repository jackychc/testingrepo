using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PreviousTopicPopup : Popup
{
	public GameObject page1, page2;
	public Topic topic;
	public Reward reward;
	public RewardRowsPositioner rrp;
	public LoadingFlower loadingFlower;
	
	public UISysFontLabel buttonText;
	
	List<Topic.Tier> claimableTiers;
	
	public override void Show()
	{
		base.Show();
		
		//transform.GetComponentInChildren<VotingInformationBoard>().votesNumber.text = topic.votes + "";
		//transform.GetComponentInChildren<VotingInformationBoard>().rankNumber.text = topic.rank + "";
		StartCoroutine(delayedUpdate());
		
		claimableTiers = getListOfClaimableTiers();
		
		if (claimableTiers.Count > 0)
		{
			buttonText.Text = "Collect Gift!";
		}
		else
		{
			buttonText.Text = "OK";
		}
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		
		
	}
	
	IEnumerator delayedUpdate()
	{
		yield return new WaitForSeconds(0.05f);
		transform.GetComponentInChildren<VotingInformationBoard>().UpdateProgress();
		transform.GetComponentInChildren<VotingInformationBoard>().UpdateTier();
	}
	
	List<Topic.Tier> getListOfClaimableTiers()
	{
		List<Topic.Tier> returnMe= new List<Topic.Tier>();
		
		foreach (Topic.Tier tier in topic.tiers)
		{
			if (!tier.claimed)
			{
				if (tier.requirement > 0)
				{
					if (topic.votes >= tier.requirement)
						returnMe.Add(tier);
				}
				
				else if (tier.rank > 0)
				{
					if (tier.rank == topic.rank)
						returnMe.Add(tier);
				}
			}
		}
		
		return returnMe;
	}
	
	public void setTopic(Topic t)
	{
		topic = t;
	}
	
	public void setReward(Reward r)
	{
		reward = r;
		rrp.reward = r;
		
		
		
	}
	
	public override void SilentHide()
	{	
		if (!page2.activeSelf) return;
		
		//Log.Debug("!?!?!");
		GameObject.Find("Init").GetComponent<VotingScene>().RefreshTopic();
		reward.claim();
		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void Hide()
	{
		if (!page2.activeSelf) return;
		
		//Log.Debug("!?!?!");
		GameObject.Find("Init").GetComponent<VotingScene>().RefreshTopic();
		reward.claim();
		
		base.Hide();
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void PickGift()
	{
		//StartCoroutine(ClaimRoutine());
		/*Topic.Tier emptyTier = new Topic.Tier();
		emptyTier.tierId = 0;
		
		claimableTiers.Add(emptyTier);
		
		if (claimableTiers.Count > 0)
		{*/
			StartCoroutine(ClaimRoutine());
		//}
		/*else
		{
			//Log.Debug("!?!?!aaaaaaaaaaaa");
			// no gift, hide immediately
			GameObject.Find("Init").GetComponent<VotingScene>().RefreshTopic();
			
			base.Hide();
			transform.GetComponent<AnchorTweener>().Backward();
			
			//GameObject.Find("Init").GetComponent<VotingScene>().RefreshTopic();
		}*/
	}
	
	IEnumerator ClaimRoutine()
	{
		//loadingFlower.Show();
		
		List<Reward> rewardList = new List<Reward>();
		string allTiers = "";
		
		if (claimableTiers.Count > 0)	
		{
			for (int i = 0; i <  claimableTiers.Count; i++)
			{
				allTiers += (allTiers.Equals("")? "" : ",") + claimableTiers[i].tierId;
			}
		}
		
		else
		{
			allTiers = "0";
		}
		
		Reward r = null;
		yield return StartCoroutine(ClaimReward(allTiers, result => r=result));
		
		if (r != null)
		{
			rewardList.Add(r);
		
		
			setReward(Rewards.CombineRewards(rewardList));
			
			//loadingFlower.Hide();
			
			page1.SetActive(false);
			page2.SetActive(true);
			
			rrp.Position();
		}
		
		else
		{
			GameObject.Find("Init").GetComponent<VotingScene>().RefreshTopic();
			base.Hide();
			transform.GetComponent<AnchorTweener>().Backward();
		}
	}
	
	
	
	IEnumerator ClaimReward(string tiers, Action<Reward> returnedReward)
	{
		PopupManager.ShowLoadingFlower();
		
		string topicId 			= Globals.viewingTopic.topicId.ToString();
		string topicTierIds 	= tiers;
		string dlcId 			= Globals.currentDLCVersion.ToString();
		
		//Prepare CheckSum
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + topicId + topicTierIds + dlcId;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("ClaimAllTopicsRewards", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("topicId", topicTierIds);
		kvp.Add("topicTierIds", topicTierIds);
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("dlcId", dlcId);
		kvp.Add("checkSum", checkSum);		
		
		string url = Globals.serverAPIURL + "ClaimAllTopicsRewards.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response 			= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					// have rank already
					if (al[1] != null)
					{
						if (((Hashtable)al[1])["topicTierId"] != null)
						{
							Hashtable rht = ((Hashtable)al[1]);
							
							List<Reward> rewardList = new List<Reward>();
							ArrayList rewardAL =  new ArrayList(rht.Values);
							
							foreach (Hashtable reward in rewardAL)
							{
							
							
								if (Int32.Parse(rht["rewardId"].ToString()) > 0)
								{
								
									// get the reward!!!
									// use server result, don't use local ones
									Reward r = new Reward(
												Int32.Parse(rht["rewardId"].ToString()),
												1,
												Int32.Parse(rht["coins"].ToString()),
												Int32.Parse(rht["gems"].ToString()),
												Int32.Parse(rht["exp"].ToString()),
												Int32.Parse(rht["energy"].ToString()),
												rht["itemIds"] != null? rht["itemIds"].ToString(): "",
												rht["skills"] != null? new Skill(rht["skills"].ToString()):new Skill(new int[]{0,0,0,0,0})
											);
									
									rewardList.Add(r);
									
									
								
								}
								
									
							}
							
							PopupManager.HideLoadingFlower();
							
							foreach (Topic.Tier t in claimableTiers)
							{
								Globals.viewingTopic.getTier(t.tierId).claimed = true;
							}
							
							if (rewardList.Count > 0)
							{
								returnedReward(Rewards.CombineRewards(rewardList));
							}
							else
							{
								returnedReward(null);
							}
							
							
						}
						else
						{
							PopupManager.HideLoadingFlower();
						}
						
					
					}
					else
					{
						PopupManager.HideLoadingFlower();
					}
				}
				else
				{
					PopupManager.HideLoadingFlower();
					if (al[1] != null && ((Hashtable)al[1])["isDLCError"] != null &&  ((Hashtable)al[1])["isDLCError"].ToString().Equals("true") )
					{
						PopupManager.ShowInformationPopup("You must update DLC to the latest version to claim the reward.", false);
						
						// myself actually
						PopupManager.HideLatestPopup();
					}
					else
					{
						PopupManager.ShowInformationPopup("There is some error in collecting the reward.", false);
						
						// myself actually
						PopupManager.HideLatestPopup();
					}
				}
				
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("ClaimAllTopicsRewards", kvp, result => response = result));
	}

}

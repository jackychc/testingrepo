using UnityEngine;
using System.Collections;

public class QuitGamePopup : Popup {
	
	public delegate void OnQuitGamePopupCrossClicked();
	public OnQuitGamePopupCrossClicked crossClicked;	
	
	public override void Show()
	{
		base.Show();
		transform.GetComponent<AnchorTweener>().Forward();		
	}
	
	public override void Hide()
	{
		base.Hide();
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{
		base.SilentHide();
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void OnBackPressed()
	{
		CrossClicked();
	}
	
	#region Button Click
	public void TickClicked()
	{
		Time.timeScale = 0f;
		Application.Quit();
	}
	
	public void CrossClicked()
	{
		Hide();
		if (crossClicked != null)
			crossClicked();
	}
	#endregion
}

using UnityEngine;
using System.Collections;

public class RewardPopup : Popup
{
	public Reward reward;
	public RewardRowsPositioner rrp;
	public GameObject descriptionObj;
	public string description;

	GameObject rewardEffect;
	
	private object rewardLock = new object();
	private volatile bool rewardClaimed = false;
	//public UISysFontLabel titleText;
	
	public void setReward(Reward r, string description)
	{
		if (description != null)
		{
			this.description = description;

			UISysFontLabel descriptionLabel = descriptionObj.GetComponent<UISysFontLabel>();
			if (descriptionLabel != null)
			{
				descriptionLabel.Text = description;
				descriptionObj.SetActive(true);
			}
		}

		reward 		= r;
		rrp.reward 	= r;		
		rrp.Position();
	}
	
	
	public override void Show()
	{
		StartCoroutine(EnumClaimeReward());

		SoundManager.SharedManager.PlaySFX("job_click_award");
		base.Show();
		
		// set reward rows
		
		rewardEffect = (GameObject) Instantiate(Resources.Load("RewardEffectCamera") as GameObject);
		
		
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		//titleText.Text = Language.Get("job_youhavereceived");
		
	}
	
	public override void Hide()
	{
		if (!rewardClaimed)
			return;
		
		lock(rewardLock)
		{
			//rewardClaimed = true;
			
			base.Hide();
			//StartCoroutine(EnumClaimeReward());
			transform.GetComponent<AnchorTweener>().Backward();
			
			//Destroy(rewardEffect);
			rewardEffect.SetActive(false);
		}
	}
	
	public override void SilentHide()
	{
		if (!rewardClaimed)
			return;
		
		lock(rewardLock)
		{
			//rewardClaimed = true;		
			//StartCoroutine(EnumClaimeReward());

			transform.GetComponent<AnchorTweener>().Backward();
			base.SilentHide();

			//Destroy(rewardEffect);
			rewardEffect.SetActive(false);
		}
	}
	
	public override void OnBackPressed()
	{
		if (!rewardClaimed)
			return;

		base.OnBackPressed();
	}

	
	public IEnumerator EnumClaimeReward()
	{
		reward.claim();
		rewardClaimed = true;
		yield return null;
	}


}

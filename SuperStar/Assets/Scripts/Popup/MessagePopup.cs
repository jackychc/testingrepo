using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MessagePopup : Popup
{
	public MessageScrollListManager msm;
	public GameObject acceptButton, clearButton, editButton, selectAllButton;
	UIImageButton editIb;
	UISprite editBg;
	
	bool onEdit = false;
	
	public LoadingFlower loadingFlower;
	public GameObject noMessageLabel;
	
	public override void Show()
	{
		
		//msm.LoadMessages(Globals.messages);
		
		editIb = editButton.GetComponent<UIImageButton>();
		editBg = editButton.transform.FindChild("Background").GetComponent<UISprite>();
		
		//noMessageLabel.GetComponent<UISysFontLabel>().Text = Language.Get("vote_nomessage");
		//acceptButton.GetComponentInChildren<UISysFontLabel>().Text = Language.Get ("vote_accept");
		
		//clearButton.GetComponentInChildren<UISysFontLabel>().Text = Language.Get ("vote_clear");
		//selectAllButton.GetComponentInChildren<UISysFontLabel>().Text = Language.Get ("vote_selectall");
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		
		
		StartCoroutine(GetMessages());
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{	
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public IEnumerator GetMessages()
	{
		//List<Player> list = new List<Player>();
		Hashtable ht = null;
		yield return StartCoroutine(GetMessagesOnServer(result => ht = result));
		Globals.messages = Messages.GetMessagesFromHashtable(ht);
		loadingFlower.Hide();
		if (Globals.messages.Count <= 0)
		{
			noMessageLabel.SetActive(true);
			noMessageLabel.GetComponent<UISysFontLabel>().Text = "No message";
		}
		msm.LoadMessages();
	}

	public void SelectAll()
	{
		Log.Debug("select all clicked");
		foreach (MessageItem mi in msm.messageItems)
		{
			mi.setTrue();
		}
	}
	
	public void EditPressed()
	{
		onEdit = !onEdit;
		
		
		string spriteName = "";
		
		if (onEdit)
		{
			spriteName = "btn_edit_b";
			clearButton.SetActive(true);
			acceptButton.SetActive(false);
			
		}
		else
		{
			spriteName = "btn_edit_a";
			acceptButton.SetActive(true);
			clearButton.SetActive(false);
		}
		
		editIb.normalSprite = spriteName;
		editIb.hoverSprite = spriteName;
		editIb.pressedSprite = spriteName;
		editBg.spriteName = spriteName;
		
	}
	
	public void AcceptRequest()
	{
		SubmitRequest();
	}
	
	public void clearRequest()
	{
		SubmitRequest();
	}
	
	public void SubmitRequest()
	{
		foreach (MessageItem mi in msm.messageItems)
		{
			if (mi.selected)
			{
				string gameId = "", targetGameId = "";
			
				Log.Debug("accepted?"+mi.message.accepted);
			
				if (mi.message.accepted == 0)
				{
					gameId = mi.message.oppositeGameId;
					targetGameId = Globals.mainPlayer.GameId;
				}
			
				if (mi.message.accepted == 1)
				{
					gameId = Globals.mainPlayer.GameId;
					targetGameId = mi.message.oppositeGameId;
				}
			
			
				StartCoroutine(SubmitRequestCoroutine(mi.message.topicId,gameId,targetGameId));
				
				
			}
		}
	}
	
	IEnumerator SubmitRequestCoroutine(int topicId, string gameId, string targetGameId)
	{
		yield return StartCoroutine(SubmitRequestToServer(topicId, gameId, targetGameId, onEdit? "Clear": "Accept"));
	
		msm.RemoveTickedMessages();
		
		if (Globals.messages.Count <= 0)
		{
			noMessageLabel.SetActive(true);
			noMessageLabel.GetComponent<UISysFontLabel>().Text = "No message";
		}
		
	}

	
	IEnumerator GetMessagesOnServer(Action<Hashtable> hash)
	{
		//Prepare CheckSum
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("GetVoteRequest", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		//kvp.Add("topicId", Globals.viewingTopic.topicId.ToString());
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("checkSum", checkSum);
	
		//yield return StartCoroutine(HTTPPost.Post("GetVoteRequest", kvp, result => response = result));
		string url = Globals.serverAPIURL + "GetVoteRequest.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response					= NetworkManager.SharedManager.Response;
		NetworkManager.State state 		= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{			
					hash(((Hashtable)al[1]));
				}
				else
				{
					// ?
				}	
			}
		}
	}
	
	IEnumerator SubmitRequestToServer(int topicId, string gameIdStr, string targetGameId, string mode)
	{
		//Prepare CheckSum
		GameID gameId = new GameID(Globals.mainPlayer.GameId);
		int playerId = gameId.toInt();
		string checkSumData = gameId.ID + playerId + topicId + targetGameId;
		string checkSum = MD5Hash.GetServerCheckSumWithString(mode + "VoteRequest", checkSumData);
		
		List<Player> returnMe = new List<Player>();
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("topicId", topicId.ToString());
		kvp.Add("gameId",  gameIdStr);
		kvp.Add("targetGameIds", targetGameId);
		kvp.Add("checkSum", checkSum);
	
		//yield return StartCoroutine(HTTPPost.Post(mode + "VoteRequest", kvp, result => response = result));
		
		string url = Globals.serverAPIURL + mode + "VoteRequest.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));		
	}
	
}

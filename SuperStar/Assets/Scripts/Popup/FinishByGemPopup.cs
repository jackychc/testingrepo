using UnityEngine;
using System.Collections;
using System;

public class FinishByGemPopup : Popup
{
	public Job job;
	public int proportionGems = 0;
	
	public delegate void OnFinishByGemClicked();
	public OnFinishByGemClicked finishByGemClicked;
	
	//public UISysFontLabel instantLabel;
	//public UISysFontLabel cancelLabel;
	
	public void setJob(Job j)
	{
		// 30*60 = 1800sec = 1 proportion

		job = j;

		int totalProportions = Mathf.CeilToInt(job.timeRequired/1800f);
		int currentProportion = Mathf.CeilToInt((job.timeRequired - (job.expireTime - job.timeLeft))/1800f);

		Log.Debug("total:"+totalProportions+",current:"+currentProportion);

		proportionGems = Mathf.CeilToInt (job.gemsToFinish * (1f* currentProportion/totalProportions));

		// make the last digit be "9"
		if (proportionGems > 10 && proportionGems%10 == 0)
			proportionGems--;

		if (proportionGems <= 0)
		{
			proportionGems = job.gemsToFinish;
		}

		//proportionGems = Mathf.CeilToInt(job.gemsToFinish * (job.timeRequired - (job.expireTime - job.timeLeft))/job.timeRequired);
		setAmount(proportionGems/*job.gemsToFinish*/);
		
		/*if (job.gemsToFinish > Globals.mainPlayer.Gems)
		{
			transform.FindChild("finish").gameObject.SetActive(false);
		}*/
		
		//instantLabel.Text = Language.Get("job_finishinstant");
		//cancelLabel.Text = Language.Get("job_canceljob");
	}
	
	public void setAmount (int a)
	{
		transform.FindChild("Amount").GetComponent<UILabel>().text = "" + a;
	}
	
	public void finishByGem()
	{
		Log.Debug("clicked tick");
		
		//if (job.gemsToFinish > Globals.mainPlayer.Gems)
		if (proportionGems > Globals.mainPlayer.Gems)
		{
			
			PopupManager.ShowNotEnoughCurrencyPopup("Gems", proportionGems - Globals.mainPlayer.Gems, false);
			//PopupManager.ShowNotEnoughCurrencyPopup("Gems", job.gemsToFinish - Globals.mainPlayer.Gems, false);
			Hide ();
			return;
		}
		
		job.timeLeft = -1;
		
		Globals.mainPlayer.GainGems(-proportionGems);
		//Globals.mainPlayer.GainGems(-job.gemsToFinish);
		
		int skillId = Int32.Parse(job.visibleForSkill);
		
		SkillTier next = SkillTable.GetNextSkillTier(skillId, Globals.mainPlayer.getSkill().skill[skillId]);
		
		SkillTier prevTier = SkillTable.GetSkillTier(Globals.mainPlayer.TitleSkill);
		SkillTier afterTier = SkillTable.GetSkillTier(
			skillId,
			Globals.mainPlayer.getSkill().skill[skillId]
				+ Mathf.Min(
					(next == null? 0 : (next.skillAmount - Globals.mainPlayer.getSkill().skill[skillId])),
					Rewards.getReward(job.rewardId).skills.skill[skillId]
				)
			);

		
	
		
		//Rewards.getReward(job.rewardId).claim();
		
		if (job.visibleForSkill.Contains("0"))
			//QuestFlags.var2_tennisFirstJob = true;
			QuestFlags.SetFlag(2, true);
		
		if (job.activityId == QuestFlags.GetFlag(11).intValue)
			QuestFlags.SetFlag(12, QuestFlags.GetFlag(12).intValue+1);
			//QuestFlags.var12_targetJobDoneCount++;
		
		PopupManager.ShowRewardPopup(Rewards.getReward(job.rewardId), false);
		
		if (prevTier.skillTierId != afterTier.skillTierId)
		{
			Globals.mainPlayer.TitleSkill = afterTier.skillTierId;
			
			if (afterTier.Equals(next))
				PopupManager.ShowNewTitlePopup(false);
			else
			{
				GameObject titleShower = GameObject.Find("TitleShower");
				
				if (titleShower != null)
					titleShower.GetComponent<TitleShower>().Refresh();
			}
			
			
			//PopupManager.ShowInformationPopup("new title! " + SkillTable.GetSkillTier(Globals.mainPlayer.TitleSkill).skillTierString);
			
			Hashtable jobStateParam = new Hashtable();
			jobStateParam["Title"] = Skill.skillNames[afterTier.skillType] + " " + Enum.GetName(typeof(SkillTier.STARCOLOR), afterTier.skillColor) + afterTier.skillStars + " ";
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Job_State, jobStateParam);
		}
		
		Hide ();
		
		if (finishByGemClicked != null)
			finishByGemClicked();
		
	}
	
	public void CancelJob()
	{
		SoundManager.SharedManager.PlaySFX("job_cancel_click");
		SoundManager.SharedManager.StopLoopSFX();
		SoundManager.SharedManager.StartBGM();
		
		job.timeLeft = -1;
		Hide ();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
		base.ScaleDown();
	}
	
	
	
	public override void Hide()
	{
		Log.Debug("hide");
		
		base.Hide();
		
		base.ScaleDown();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}

}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopMorePopup : Popup
{
	
	List<Shop> shops;
	
	public UISprite[] shopSprites = new UISprite[4];
	
	public void SetShops(List<Shop> s)
	{
		shops = s;
		
		// todo: some extra settings
		for (int i = 0; i < 4; i++)
		{
			if (i < shops.Count)
			{
				shopSprites[i].spriteName = shops[i].iconName;
				shopSprites[i].MakePixelPerfect();
				shopSprites[i].transform.parent.localPosition = new Vector3(-70f*(shops.Count-1) + i*140f,0f,-1f);
			}
			
			else
				shopSprites[i].gameObject.SetActive(false);
		}
	}
	
	public override void Show()
	{
		base.Show();
		
		
		
		transform.GetComponent<AnchorTweener>().Forward();
		
	}
	
	public override void SilentHide()
	{
		base.SilentHide();
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void Hide()
	{
		
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	void Place1Clicked()
	{
		PlaceClicked(0);
	}
	
	void Place2Clicked()
	{
		PlaceClicked(1);
	}
	
	void Place3Clicked()
	{
		PlaceClicked(2);
	}
	
	void Place4Clicked()
	{
		PlaceClicked(3);
	}
	
	void PlaceClicked(int index)
	{
		Log.Debug("place clicked: "+index);
		
		if (shops.Count > index)
		{
			VisitShop.shop = shops[index];
			VisitShop.fromScene = "CLOSET";
			
			Hide ();
			PopupManager.HideAllPopup();
			
			
			
			SceneManager.SharedManager.LoadScene(Scenes.SHOP, true);
		}
	}
	
	
	
}

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DetectDiffDevicePopup : Popup {
	
	public Hashtable serverData;	
	
	public UISysFontLabel labelTitle;
	public UISysFontLabel labelDesc;
	
	public UISysFontLabel labelLastSyncA;
	public UISysFontLabel labelAccNameA;
	public UILabel labelLevelA;
	public UILabel labelEnergyA;
	public UILabel labelGemsA;
	public UILabel labelCoinsA;
	public UISprite spriteToggleA;
	
	public UISysFontLabel labelLastSyncB;
	public UISysFontLabel labelAccNameB;
	public UILabel labelLevelB;
	public UILabel labelEnergyB;
	public UILabel labelGemsB;
	public UILabel labelCoinsB;
	public UISprite spriteToggleB;
	
	private bool isKeepCurrentGameSave = false;
	
	private string toogleOnSpriteName = "setting_on";
	private string toogleOffSpriteName = "setting_off";
	
	private void Init()
	{
		InitAccountA();
		InitAccountB();
	}	
	
	private void InitAccountA()
	{
		Hashtable playerInfo = (Hashtable)serverData["playerInfo"];
		Log.Debug(playerInfo.toJson());
		
		int exp = 0;
		int level = 0;
		if (playerInfo["exp"] != null)
		{
			exp = int.Parse("" + playerInfo["exp"]);
			level = Globals.mainPlayer.getLevelByExp(exp);	
		}
		
		labelAccNameA.Text 		= "" + playerInfo["playerName"];		
		labelLevelA.text 		= "" + level;
		labelEnergyA.text 		= "" + playerInfo["energy"] + "/" + playerInfo["maxEnergy"];
		labelGemsA.text 		= "" + playerInfo["gems"];
		labelCoinsA.text 		= "" + playerInfo["coins"];
		
		System.DateTime timestamp;
		bool parseResult = System.DateTime.TryParse("" + playerInfo["lastSyncDate"], out timestamp);
		if (parseResult)
		{
			System.DateTime localTime 	= timestamp.ToLocalTime();
			
			string syncDate = string.Format("{0:0000}-{1:00}-{2:00} {3:00}:{4:00}:{5:00}", localTime.Year, localTime.Month, localTime.Day, localTime.Hour, localTime.Minute, localTime.Second);
			
			//string syncDate				= localTime.Year + "-" + localTime.Month + "-" + localTime.Day + " " + localTime.Hour + ":" + localTime.Minute + ":" + localTime.Second;
			labelLastSyncA.Text			= Language.Get("setting_lastsave") + " " + syncDate;
		}
	}
	
	private void InitAccountB()
	{
		labelAccNameB.Text 	= Globals.mainPlayer.Name;
		labelLevelB.text 	= "" + Globals.mainPlayer.Level;
		labelEnergyB.text 	= Globals.mainPlayer.Energy +"/" +Globals.mainPlayer.MaxEnergy;
		labelGemsB.text 	= "" + Globals.mainPlayer.Gems;
		labelCoinsB.text 	= "" + Globals.mainPlayer.Coins;
		labelLastSyncB.Text = Language.Get("setting_currentsave") + " " + Globals.mainPlayer.LastSyncDate;
	}
	
	public override void Show()
	{
		Init();
		base.Show();
		transform.GetComponent<AnchorTweener>().Forward();
	}
	
	public override void Hide()
	{
		base.Hide();		
		transform.GetComponent<AnchorTweener>().Backward();		
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		//CrossPressed();
		base.OnBackPressed();
	}
	
	// Use this for initialization
	public override void Start() {
		base.Start();
	}
	
	#region button Event
	public void TickPressed()
	{
		if (spriteToggleA.spriteName.Equals(toogleOnSpriteName))
		{
			//Restore needed
			PopupManager.HideAllPopup();
			PopupManager.ShowRestoreProgressPopup(SocialNetwork.Type.NONE, serverData, false, false);
			UpdateGameSaveDeviceId();
		}
		else if (spriteToggleB.spriteName.Equals(toogleOnSpriteName))
		{
			//UpdateGameSave With forceSync
			SaveLoadManager.SharedManager.ForceSaveAndSyncPlayerData();
			Hide();
		}
	}
	
	public void CrossPressed()
	{
		
		
		
		
		Hide();
	}
	
	public void AccountAToggled()
	{
		spriteToggleA.spriteName = toogleOnSpriteName;
		spriteToggleB.spriteName = toogleOffSpriteName;
	}
	
	public void AccountBToggled()
	{
		spriteToggleB.spriteName = toogleOnSpriteName;
		spriteToggleA.spriteName = toogleOffSpriteName;
	}
	#endregion
	
	
	#region Server Update
	private void UpdateGameSaveDeviceId()
	{
		StartCoroutine(EnumUpdateGameSaveDeviceId());
	}
	
	private IEnumerator EnumUpdateGameSaveDeviceId()
	{
		string deviceId = MunerisManager.SharedManager.GetDeviceId();
		
		GameID gameId = new GameID(Globals.mainPlayer.GameId);
		int playerId = gameId.toInt();
		string checkSumData = gameId.ID + playerId + deviceId;
		string checkSum = MD5Hash.GetServerCheckSumWithString("UpdateGameSaveDeviceId", checkSumData);
		
		//Prepare the parameters
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", gameId.ID);
		kvp.Add("deviceId", deviceId);
		kvp.Add("checkSum", checkSum);			
	
		//yield return StartCoroutine(HTTPPost.Post("UpdateGameSaveDeviceId", kvp, result => response = result));
		string url = Globals.serverAPIURL + "UpdateGameSaveDeviceId.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				Hashtable respHT = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(response);
				Hashtable serverResp = (Hashtable)respHT["0"];
				if (serverResp != null)
				{
					bool hasResponse = Boolean.Parse("" + serverResp["response"]);
					if (hasResponse)
						Log.Debug("DifferentDeviceDetected Update game device Successfully!!");
					else
						Log.Debug("DifferentDeviceDetected Cannot update game Device");
				}
			}
		}
	}
	#endregion
}

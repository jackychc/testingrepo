using UnityEngine;
using System.Collections;

public class FlirtCompletePopup : Popup
{
	public Boyfriend boyfriend;
	
	//public UISysFontLabel titleLabel;
	//public UISysFontLabel buttonLabel;
	
	public void setBoyfriend(Boyfriend f)
	{
		boyfriend = f;		
	}
	
	public override void Start()
	{
		base.Start();
	}	
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		//titleLabel.Text = Language.Get("bf_complete");
		//buttonLabel.Text = Language.Get("bf_invitetohome");
		
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		//BringHomePressed();
	}
	
	public void BringHomePressed()
	{
		Globals.mainPlayer.CurrentBoyfriendId = boyfriend.boyfriendId;
		Hide ();
		PopupManager.HideAllPopup();
		
		
		
		SceneManager.SharedManager.LoadScene(Scenes.HOME, true);
	}
	



}

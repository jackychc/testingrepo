using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NotEnoughCurrencyPopup : Popup
{
	int currencyType = 1;
	public string currency = "Gems";
	public int amount = 10;
	
	public UISysFontLabel label;
	
	public void Set(string pCurrency, int pAmount)
	{
		if (pCurrency.Equals("Coins"))
			currencyType = 2;
			
		if (pCurrency.Equals("Energy"))
			currencyType = 3;
		
		currency = Language.Get("currency_"+pCurrency.ToLowerInvariant());
		amount = pAmount;
	}
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		string construct = Language.Get("iap_preask").Replace("%1",(""+amount)).Replace("%2", (amount == 1? currency.Trim('s') : currency)) ;
		
		label.Text = construct;
		//label.Text = "You still need "+amount + " " + (amount == 1? currency.Trim('s') : currency) + " to go. Do you want to buy some " + currency +" now?";
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{	
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void Yes()
	{
		Log.Debug("yes");
		
		int gotopage = currencyType;
		
		PopupManager.ShowIAPPopup(gotopage, false);
		PopupManager.HideLatestPopup();
		
		
	}
	
	
}

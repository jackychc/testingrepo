using UnityEngine;
using System.Collections;
using Muneris.AppVersionCheck;


public class NewAppVersionPopup : Popup {
	public NewAppVersion newAppVersion;
	
	
	// Use this for initialization
	
	public override void Show()
	{
		base.Show();
		transform.GetComponent<AnchorTweener>().Forward();
	}	
	
	
	public override void Hide()
	{
		base.Hide();
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void OnBackPressed()
	{
		//base.OnBackPressed();
	}
	
	public void TickPressed()
	{
		//if (newAppVersion != null)
			//newAppVersion.GoToAppStore();

#if UNITY_ANDROID
		Application.OpenURL(Globals.googlePlayURL);
#elif UNITY_IPHONE
		Application.OpenURL(Globals.itunesURL);
#endif
	}
}

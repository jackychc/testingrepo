using UnityEngine;
using System.Collections;

public class NewTitlePopup : Popup
{
	string content = "";
	public GameObject[] stars = new GameObject[5];
	
	bool hideClicked = true;
	
	GameObject titleShower;
	
	public GameObject topTitle, logo, diamond;
	
	public override void Show()
	{
		base.Show();
		SoundManager.SharedManager.PlaySFX("job_star_added");
		//transform.GetComponent<AnchorTweener>().Forward();
		
		transform.GetComponentInChildren<ScaleTweener>().Forward();
		
		titleShower = GameObject.Find("TitleShower");
		
		if (titleShower != null)
		{
			StartCoroutine(ShowRoutine());
		}
		
		StartCoroutine(BeginningLock());
	}
	
	IEnumerator BeginningLock()
	{
		//hideClicked = true;
		ParticleSystem ps = GameObject.Find("PSCamera").GetComponentInChildren<ParticleSystem>();
		//ps.Stop();
		ps.Play();
		
		yield return new WaitForSeconds(2.0f);
		
		hideClicked = false;
		
		Hide ();
		
	}
	
	public override void Hide()
	{
		
		if (hideClicked) return;
		
		hideClicked = true;
		SoundManager.SharedManager.PlaySFX("job_star_fade");
		
		StartCoroutine(HideRoutine());
		
	}
	
	public override void OnBackPressed()
	{
		//lock
		
		//base.OnBackPressed();
	}
	
	IEnumerator ShowRoutine()
	{		
		//yield return new WaitForSeconds(0.03f);		
		titleShower.GetComponent<TitleShower>().Refresh();
		titleShower.GetComponent<TitleShower>().SetStarsVisible(false);
		
		for (int i = 0; i < 5; i++)
		{
			stars[i].SetActive(false);			
		}
		
		yield return null;
	}
	
	IEnumerator HideRoutine()
	{
		
		topTitle.SetActive(false);
		logo.SetActive(false);
		diamond.SetActive(false);
		
		//Play Sound
		
		// usually is not null ge...
		if (titleShower != null)
		{
			
			Transform target = titleShower.transform.FindChild("Star"+SkillTable.GetSkillTier(Globals.mainPlayer.TitleSkill).skillStars);
			Transform originalParent = target.parent.transform;
			target.parent = transform;
			
			transform.GetComponentInChildren<PositionTweener>().xDisplacement = target.localPosition.x - transform.FindChild("Mother").localPosition.x;
			transform.GetComponentInChildren<PositionTweener>().yDisplacement = target.localPosition.y - transform.FindChild("Mother").localPosition.y;
			
			target.parent = originalParent;
			transform.GetComponentInChildren<PositionTweener>().Forward();
			
			ParticleSystem ps = GameObject.Find("PSCamera").GetComponentInChildren<ParticleSystem>();
			//ps.Stop();
			ps.Stop();
		}
		transform.GetComponentInChildren<ScaleTweener>().Backward();

		// wait until the star animation finish	
		yield return new WaitForSeconds(0.8f);			
		
		titleShower.GetComponent<TitleShower>().SetStarsVisible(true);
		
		yield return new WaitForSeconds(0.1f);
		
		titleShower.GetComponent<TitleShower>().Refresh();
		
		base.Hide();
		transform.GetComponent<AnchorTweener>().Backward();
		
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
}

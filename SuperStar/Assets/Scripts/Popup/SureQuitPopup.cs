using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SureQuitPopup : Popup
{
	Scenes targetScene;
	
	public void SetTargetScene(Scenes s)
	{
		targetScene = s;
	}
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		//messageLabel.Text = Language.Get("vote_confirmandjoin");
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void TickPressed()
	{
		PopupManager.HideAllPopup();
		SoundManager.SharedManager.StopLoopSFX();
		SceneManager.SharedManager.LoadScene(targetScene, false);		
	}
}

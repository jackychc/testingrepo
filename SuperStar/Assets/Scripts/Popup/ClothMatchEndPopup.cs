using UnityEngine;
using System.Collections;

public class ClothMatchEndPopup : Popup
{
	int score, apparentScore, combo;
	
	public UILabel scoreValue, comboValue, percentValue;
	public UIFilledSprite progressSprite;
	public GameObject highScoreGO;
	
	public GameObject againButton,exitButton, rewardButton;
	
	public ScaleTweener barScaleTweener;
	
	public void SetParameters(int pScore, int pCombo)
	{
		score = pScore;//Globals.mainPlayer.ClothMatchTotalScore;//pScore;
		apparentScore = Globals.mainPlayer.ClothMatchTotalScore-score;
		combo = pCombo;
	}
	
	public override void Show()
	{
		base.Show();
		scoreValue.text = "" + score;
		comboValue.text = "" + combo;
		
		
		StartCoroutine(DelayedShow());
		
		if (Globals.mainPlayer.ClothMatchHighScore < score)
		{
			highScoreGO.SetActive(true);
			Globals.mainPlayer.ClothMatchHighScore = score;
		}
		else
		{
			highScoreGO.SetActive(false);
		}
		
		
		
		if (Globals.mainPlayer.ClothMatchTotalScore >= Globals.miniGame_clothMatch_scoreForGift)
		{
			againButton.SetActive(false);
			exitButton.SetActive(false);
			rewardButton.SetActive(true);
		}
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		
		//Log Flurry Event
		Hashtable param = new Hashtable();
		param["Score"] = score;
		param["Combo"] = combo;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Runway_Result, param);		
	}
	
	IEnumerator DelayedShow()
	{
		bool keepShow = true;
		percentValue.color = Color.white;
		
		float delta = (Globals.mainPlayer.ClothMatchTotalScore - apparentScore) / 50f;
		float percentage = 0f;
		SoundManager.SharedManager.PlaySFX("changeClothes_giftbar_grow");
		while(keepShow)
			
		{		
			yield return new WaitForSeconds(0.018f);
			apparentScore = Mathf.Min(Globals.mainPlayer.ClothMatchTotalScore, (int)(apparentScore+delta));
			
			progressSprite.fillAmount = Mathf.Min(1f, 1f*apparentScore/Globals.miniGame_clothMatch_scoreForGift);
			
			percentage = apparentScore*100f/Globals.miniGame_clothMatch_scoreForGift;
			percentValue.text = (int)(percentage) + "%";
			
			if (percentage >= 100f)
			{
				percentValue.text = "100%";
				percentValue.color = Color.yellow;
			}
			
			if (apparentScore >= Globals.miniGame_clothMatch_scoreForGift)
			{
				barScaleTweener.speed = 2f;
				SoundManager.SharedManager.PlayLoopSFX("changeClothes_gift_flashing_loop");
			}
			
			if (apparentScore >= Globals.mainPlayer.ClothMatchTotalScore)
			{
				keepShow = false;
				//SoundManager.SharedManager.StopLoopSFX();
			}
		}
	}

	public override void OnDestroy()
	{
		if (SoundManager.SharedManager.IsPlayingLoopSFX())
			SoundManager.SharedManager.StopLoopSFX();
		base.OnDestroy();
	}
	
	public override void Hide()
	{
		Log.Debug("i am hiding");
		
		base.Hide();		
		transform.GetComponent<AnchorTweener>().Backward();
		
	}
	
	public override void SilentHide()
	{		
		//SoundManager.SharedManager.StopLoopSFX();
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		if (Globals.mainPlayer.ClothMatchTotalScore >= Globals.miniGame_clothMatch_scoreForGift)
			RewardClicked();
		else 
			ExitClicked();
	}
	
	public void StartClicked()
	{
		Log.Debug("Start Clicked");
		
		GameObject.Find("Init").GetComponent<MiniGameClothMatchScene>().CheckRoutine();
	}
	
	public void ExitClicked()
	{
		PopupManager.HideAllPopup();
		GameObject.Find("Init").GetComponent<MiniGameClothMatchScene>().DirectExit();
	}
	
	public void RewardClicked()
	{
		GameObject.Find("Init").GetComponent<MiniGameClothMatchScene>().RewardRoutine();
	}
}

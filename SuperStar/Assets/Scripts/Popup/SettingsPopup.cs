using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SettingsPopup : Popup, FacebookManagerCallbackInterface, GooglePlusManagerCallbackInterface, TwitterManagerCallbackInterface, TwitterLoginPopupCallbackInterface
{
	public GameObject itemsRoot;
	
	public GameObject syncLabel;
	
	public UISprite musicLamp, soundLamp, notificationLamp;
	public UISprite facebookLamp, googlePlusLamp, twitterLamp;
	public UISysFontLabel facebookTxt, googlePlusTxt, twitterTxt;
	
	//public Transform languageScrollListMother;
	public GameObject languageScrollList;
	public UISysFontLabel currentLangLabel;
	
	public override void Start()
	{
		base.Start();
	}
	
	public override void OnDestroy()
	{
		//Log.Debug("SettingsPopup: OnDestroy");

		
		//Disable the callback for the Social
		FacebookManager.SharedManager.RemoveCallback(this);
		GooglePlusManager.SharedManager.RemoveCallback(this);
		TwitterManager.SharedManager.RemoveCallback(this);

		base.OnDestroy();
	}
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		foreach(Transform t in itemsRoot.GetComponentsInChildren<Transform>())
		{
			if (t.Equals(itemsRoot.transform))
				continue;
			if (t.Equals(languageScrollList.transform))
				continue;
			
			
			Destroy(t.GetComponent<UIPanel>());
		}
		
		/*foreach(Transform t in languageScrollList.GetComponentsInChildren<Transform>())
		{
			if (t.Equals(languageScrollList.transform))
				continue;
			
			Destroy(t.GetComponent<UIPanel>());
		}
		*/
		languageScrollList.SetActive(false);
		
		UpdateSettings();
	}
	
	public override void Hide()
	{
		PopupManager.HideLoadingFlower();
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{	
		PopupManager.HideLoadingFlower();
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	
	public void Item1Clicked()
	{
		Log.Debug("item 1!");
		
		Globals.settings_bgm = !Globals.settings_bgm;
		
		if (Globals.settings_bgm)
		{
			//musicLamp.spriteName = "setting_on";
			//GameObject.Find("SoundManager").GetComponent<AudioSource>().Play();
			SoundManager.SharedManager.bgmSource.Play();
		}
		else
		{
			//musicLamp.spriteName = "setting_off";
			//GameObject.Find("SoundManager").GetComponent<AudioSource>().Stop();
			SoundManager.SharedManager.bgmSource.Stop();
		}
		
		UpdateSettings();
		
	}
	
	public void Item2Clicked()
	{
		Log.Debug("item 2!");
		
		Globals.settings_sfx = !Globals.settings_sfx;
		
		if (Globals.settings_sfx)
		{
			//soundLamp.spriteName = "setting_on";
			SoundManager.SharedManager.sfxSource.Play();
			SoundManager.SharedManager.sfxLoopSource.Play();
		}
		else
		{
			//soundLamp.spriteName = "setting_off";
			SoundManager.SharedManager.sfxSource.Stop();
			SoundManager.SharedManager.sfxLoopSource.Stop();
		}
		
		UpdateSettings();
	}
	
	public void Item3Clicked()
	{
		Log.Debug("item 3!");
		
		Globals.settings_notification = !Globals.settings_notification;
		
		if (!Globals.settings_notification)
		{
			//notificationLamp.spriteName = "setting_off";
			LocalNotification.CancelAllNotification();
		}
		
		UpdateSettings();
	}
	
	public void Item4Clicked()
	{
		Log.Debug("item 4!");
		languageScrollList.SetActive(true);
		//languageScrollList.transform.position = languageScrollListMother.position; // NOT loaclPosition
		UpdateSettings();
	}
	
	public void Item5Clicked()
	{
		Log.Debug("item 5!");
		PopupManager.ShowLoadingFlower();
		// FACEBOOK
		//PopupManager.ShowDetectExistingUserPopup(null, SocialNetwork.Type.Facebook);
		//PopupManager.ShowSettingsPopup();
		//Hide();
		
		FacebookManager.SharedManager.SetCallback(this);
		if (!FacebookManager.SharedManager.IsLoggedIn())
		{
			// login action
			FacebookManager.SharedManager.Login();
		}
		else
		{
			// logout action
			FacebookManager.SharedManager.Logout();
		}
		
	}
	
	public void Item6Clicked()
	{
		Log.Debug("item 6!");
		PopupManager.ShowLoadingFlower();
		// GG+
		
		GooglePlusManager.SharedManager.SetCallback(this);
		if (!GooglePlusManager.SharedManager.IsSignedIn())
		{
			// login action
			GooglePlusManager.SharedManager.SignIn();
		}
		else
		{
			// logout action
			GooglePlusManager.SharedManager.SignOut();
		}
	}
	
	public void Item7Clicked()
	{
		Log.Debug("item 7!");
		
		// TWITTER
		if (!TwitterManager.SharedManager.IsLoggedIn())
		{
			// login action
			//PopupManager.ShowTwitterLoginPopup("SettingsPopup", false);
			//PopupManager.ShowSettingsPopup(false);
			//Hide();
			PopupManager.ShowTwitterLoginPopup(this, true);
			
			//TwitterManager.SharedManager.PostStatusWithImage("HIHI", Application.persistentDataPath + "/2.png");
		}
		else
		{
			// logout action
			PopupManager.ShowLoadingFlower();
			TwitterManager.SharedManager.SetCallback(this);
			TwitterManager.SharedManager.Logout();
		}
	}
	
	public void Item8Clicked()
	{
		Log.Debug("item 8!");
		PopupManager.ShowLoadingFlower();
		MunerisManager.SharedManager.ShowCustomerSupport();
		UpdateSettings();
	}
	
	
	
	public void Item9Clicked()
	{
		Log.Debug("item 8!");
		// reset game!
		//UpdateSettings();
		
		PopupManager.ShowResetConfirmPopup(0, false);
		Hide ();
	}
	
	void UpdateSettings()
	{
		string musicState 			= "setting_off";
		string soundState 			= "setting_off";
		string notificationState	= "setting_off";
		string facebookState 		= "setting_login";
		string googlePlusState 		= "setting_login";
		string twitterState			= "setting_login";
		
		//BGM + Sound + Notification
		if (Globals.settings_bgm)
			musicState 	= "setting_on";
		if (Globals.settings_sfx)
			soundState = "setting_on";
		if (Globals.settings_notification)
			notificationState = "setting_on";
		if (FacebookManager.SharedManager.IsLoggedIn())
			facebookState = "setting_logout";
		if (GooglePlusManager.SharedManager.IsSignedIn())
			googlePlusState = "setting_logout";
		if (TwitterManager.SharedManager.IsLoggedIn())
			twitterState = "setting_logout";
		
		musicLamp.spriteName 		= musicState;
		soundLamp.spriteName 		= soundState;
		notificationLamp.spriteName = notificationState;
		
		facebookLamp.spriteName 	= facebookState;
		facebookTxt.Text 			= Language.Get(facebookState);
		googlePlusLamp.spriteName 	= googlePlusState;
		googlePlusTxt.Text 			= Language.Get(googlePlusState);
		twitterLamp.spriteName 		= twitterState;
		twitterTxt.Text 			= Language.Get(twitterState);
		
		currentLangLabel.Text = Language.Get("lang_name");
		syncLabel.GetComponent<UISysFontLabel>().Text = Language.Get("setting_lastsave")+" " + Globals.mainPlayer.LastSyncDate;
	}
	
	public void Lang1Clicked()
	{	
		LanguageManager.SwitchLanguage(LanguageCode.EN);
		//Language.SwitchLanguage(LanguageCode.EN);
		Globals.langCode = "EN";
		HideLanguageList();
		currentLangLabel.Text = Language.Get("lang_name");
		
		UpdateSettings();
		RelocalizeAllTextInScreen();
		
	}
	public void Lang2Clicked()
	{
		LanguageManager.SwitchLanguage(LanguageCode.ZH);
		//Language.SwitchLanguage(LanguageCode.ZH);
		Globals.langCode = "ZH";
		HideLanguageList();
		currentLangLabel.Text = Language.Get("lang_name");
		
		UpdateSettings();
		RelocalizeAllTextInScreen();
		
	}
	public void Lang3Clicked()
	{
		LanguageManager.SwitchLanguage(LanguageCode.CN);
		//Language.SwitchLanguage(LanguageCode.CN);
		Globals.langCode = "CN";
		HideLanguageList();
		currentLangLabel.Text = Language.Get("lang_name");
		
		UpdateSettings();
		RelocalizeAllTextInScreen();
		
	}
	public void Lang4Clicked()
	{
		LanguageManager.SwitchLanguage(LanguageCode.JA);
		//Language.SwitchLanguage(LanguageCode.JA);
		Globals.langCode = "JA";
		HideLanguageList();
		currentLangLabel.Text = Language.Get("lang_name");
		
		UpdateSettings();
		RelocalizeAllTextInScreen();
		
	}
	public void Lang5Clicked()
	{
		LanguageManager.SwitchLanguage(LanguageCode.KO);
		//Language.SwitchLanguage(LanguageCode.KO);
		Globals.langCode = "KO";
		HideLanguageList();
		currentLangLabel.Text = Language.Get("lang_name");	
		
		UpdateSettings();
		RelocalizeAllTextInScreen();
	}
	public void Lang6Clicked()
	{	
		LanguageManager.SwitchLanguage(LanguageCode.IT);
		//Language.SwitchLanguage(LanguageCode.EN);
		Globals.langCode = "IT";
		HideLanguageList();
		currentLangLabel.Text = Language.Get("lang_name");
		
		UpdateSettings();
		RelocalizeAllTextInScreen();
		
	}
	public void Lang7Clicked()
	{
		LanguageManager.SwitchLanguage(LanguageCode.FR);
		//Language.SwitchLanguage(LanguageCode.ZH);
		Globals.langCode = "FR";
		HideLanguageList();
		currentLangLabel.Text = Language.Get("lang_name");
		
		UpdateSettings();
		RelocalizeAllTextInScreen();
		
	}
	public void Lang8Clicked()
	{
		LanguageManager.SwitchLanguage(LanguageCode.PT);
		//Language.SwitchLanguage(LanguageCode.CN);
		Globals.langCode = "PT";
		HideLanguageList();
		currentLangLabel.Text = Language.Get("lang_name");
		
		UpdateSettings();
		RelocalizeAllTextInScreen();
		
	}
	public void Lang9Clicked()
	{
		LanguageManager.SwitchLanguage(LanguageCode.DE);
		//Language.SwitchLanguage(LanguageCode.JA);
		Globals.langCode = "DE";
		HideLanguageList();
		currentLangLabel.Text = Language.Get("lang_name");
		
		UpdateSettings();
		RelocalizeAllTextInScreen();
		
	}
	public void Lang10Clicked()
	{
		LanguageManager.SwitchLanguage(LanguageCode.ES);
		//Language.SwitchLanguage(LanguageCode.KO);
		Globals.langCode = "ES";
		HideLanguageList();
		currentLangLabel.Text = Language.Get("lang_name");	
		
		UpdateSettings();
		RelocalizeAllTextInScreen();
	}
	
	
	public void ToggleLangList()
	{
		if (languageScrollList.activeSelf)
		{
			languageScrollList.SetActive(false);
		}
		else
		{
			languageScrollList.SetActive(true);
		}
	}
	
	
	public void HideLanguageList()
	{
		languageScrollList.SetActive(false);
	}
	
	public void RelocalizeAllTextInScreen()
	{
		object[] allObjects = Resources.FindObjectsOfTypeAll(typeof(SysFontLocalizer)) ;
   	 	foreach(object thisObject in allObjects)
		{
    		((SysFontLocalizer)thisObject).ReloadText();
		}
		
		allObjects = Resources.FindObjectsOfTypeAll(typeof(HybridFont)) ;
   	 	foreach(object thisObject in allObjects)
		{
    		((HybridFont)thisObject).Refresh();
		}		
		
		allObjects = Resources.FindObjectsOfTypeAll(typeof(TitleShower)) ;
   	 	foreach(object thisObject in allObjects)
		{
    		((TitleShower)thisObject).Refresh();
		}
	}
	
	
	#region Social Callbacks
	
	#region Facebook
	public void OnFacebookManagerLogin(string userId)
	{
		PopupManager.HideLoadingFlower();
		
		//Show Login Success
		Globals.mainPlayer.facebookElement.socialId = userId;
		
		string msg = Language.Get("login_success").Replace("%1", "Facebook");
		ShowSocialLoginStatusPopup(SocialNetwork.Type.Facebook, msg, false);
		
		QuestFlags.SetFlag(5,true);
		//QuestFlags.var5_connectedToSocial = true;
	}
	
	public void OnFacebookManagerLogout()
	{
		PopupManager.HideLoadingFlower();
		
		string msg = Language.Get("logout_success").Replace("%1", "Facebook");
		ShowSocialLoginStatusPopup(SocialNetwork.Type.Facebook, msg, false);
	}
	
	public void OnFacebookManagerLoginCancel()
	{
		PopupManager.HideLoadingFlower();
		
		string msg = Language.Get("login_fail");
		ShowSocialLoginStatusPopup(SocialNetwork.Type.Facebook, msg, true);
	}
	
	public void OnFacebookManagerError(Exception e)
	{
		PopupManager.HideLoadingFlower();
		ShowSocialLoginStatusPopup(SocialNetwork.Type.Facebook, "Facebook Error!!! " + e.Message.ToString(), true);
		
		FacebookManager.SharedManager.SilentLogout();
	}
	
	public void OnFacebookManagerNewUserError(Hashtable socialInfo)
	{
		Log.Debug("Different Facebook Account detected!!! New User?");
		PopupManager.HideLoadingFlower();	
		PopupManager.HideAllPopup();
		
		PopupManager.ShowDetectNewUserPopup(socialInfo, SocialNetwork.Type.Facebook, false);
		PopupManager.ShowSettingsPopup(false);
		//Hide();	
	}
	
	public void OnFacebookManagerExistingUserError(Hashtable existingUserInfo)
	{		
		Log.Debug("Different Facebook Account detected!!! Switch to new User?");
		PopupManager.HideLoadingFlower();
		PopupManager.HideAllPopup();
		
		PopupManager.ShowDetectExistingUserPopup(existingUserInfo, SocialNetwork.Type.Facebook, false);
		PopupManager.ShowSettingsPopup(false);
		//Hide();	
	}
	
	public void OnFacebookManagerUploadPhoto(){}
	public void OnFacebookManagerGetFriends(ArrayList friendList){}
	#endregion
	
	
	#region GooglePlus
	public void OnGooglePlusManagerSignIn(string userId)
	{
		PopupManager.HideLoadingFlower();
		
		Globals.mainPlayer.googlePlusElement.socialId = userId;
		
		string msg = Language.Get("login_success").Replace("%1", "Google+");
		ShowSocialLoginStatusPopup(SocialNetwork.Type.GooglePlus, msg, false);
		
		QuestFlags.SetFlag(5,true);
		//QuestFlags.var5_connectedToSocial = true;
	}
	
	public void OnGooglePlusManagerSignInCancelled()
	{
		PopupManager.HideLoadingFlower();
	}
	
	public void OnGooglePlusManagerSignInFailed(Hashtable errorObj)
	{
		PopupManager.HideLoadingFlower();
		
		string msg = Language.Get("login_fail");
		ShowSocialLoginStatusPopup(SocialNetwork.Type.GooglePlus, msg, true);
		
		GooglePlusManager.SharedManager.SilentSignOut();
	}
	
	public void OnGooglePlusManagerSignOut(Hashtable errorObj)
	{
		PopupManager.HideLoadingFlower();
		
		string msg = Language.Get("logout_success").Replace("%1", "Google+");
		ShowSocialLoginStatusPopup(SocialNetwork.Type.GooglePlus, msg, false);
	}
	
	public void OnGooglePlusManagerError(Exception e)
	{
		Log.Debug("OnGooglePlusManager Error " + e.Message.ToString());
		PopupManager.HideLoadingFlower();
	}
	
	public void OnGooglePlusManagerNewUserError(Hashtable socialInfo)
	{
		Log.Debug("The user is clean so as to create a new user profile and bound with this account");
		PopupManager.HideLoadingFlower();
		PopupManager.HideAllPopup();
		
		PopupManager.ShowDetectNewUserPopup(socialInfo, SocialNetwork.Type.GooglePlus, false);
		PopupManager.ShowSettingsPopup(false);
		//Hide();
	}
	
	public void OnGooglePlusManagerExistingUserError(Hashtable existingUserInfo)
	{
		Log.Debug("There is an existing game save!! Wanna load it?");
		PopupManager.HideLoadingFlower();
		PopupManager.HideAllPopup();
		
		PopupManager.ShowDetectExistingUserPopup(existingUserInfo, SocialNetwork.Type.GooglePlus, false);
		PopupManager.ShowSettingsPopup(false);
		//Hide();
	}
	
	public void OnGooglePlusManagerGetFriends(ArrayList friendList){}
	public void OnGooglePlusManagerSharePhoto(){}
	public void OnGooglePlusManagerSharePhotoFailed(){}
	#endregion
	
	#region TwitterLoginPopup Callback
	public void OnTwitterLoginPopupLoginFailed()
	{
		ShowSocialLoginStatusPopup(SocialNetwork.Type.Twitter, Language.Get("login_fail"), true);
	}
	public void OnTwitterLoginPopupLoginSuccess()
	{
		ShowSocialLoginStatusPopup(SocialNetwork.Type.Twitter, Language.Get("login_success").Replace("%1", "Twitter"), false);
		UpdateSettings();
	}
	#endregion
	
	#region Twitter	
	public void OnTwitterManagerLogout()
	{		
		Log.Debug("On Twitter Manager Logout");
		PopupManager.HideLoadingFlower();
		
		string msg = Language.Get("logout_success").Replace("%1", "Twitter");
		ShowSocialLoginStatusPopup(SocialNetwork.Type.Twitter, msg, false);
		
		
		//QuestFlags.var5_connectedToSocial = true;
	}
	
	public void OnTwitterManagerError(Exception e)
	{
		Log.Debug("OnTwitterManager Error " + e.Message.ToString());		
		PopupManager.HideLoadingFlower();
		
		TwitterManager.SharedManager.SilentLogout();
		
		string msg = Language.Get("logout_fail").Replace("%1", "Twitter");
		ShowSocialLoginStatusPopup(SocialNetwork.Type.Twitter, msg, false);
	}
	
	public void OnTwitterManagerLogin(string userId){}
	public void OnTwitterManagerLoginFailed(){}
	public void OnTwitterManagerNewUserError(Hashtable socialInfo){}	
	public void OnTwitterManagerExistingUserError(Hashtable existingUserInfo){}
	public void OnTwitterManagerGetUserProfile(ArrayList userInfos){}
	public void OnTwitterManagerGetFriendIds(ArrayList friendIds){}
	public void OnTwitterManagerUploadPhoto(){}
	public void OnTwitterManagerUploadPhotoError(string error){}
	#endregion
	
	private void ShowSocialLoginStatusPopup(SocialNetwork.Type type, string status, bool isError)
	{
		Hide();
		PopupManager.ShowSocialStatePopup(type, status, isError, false);
		PopupManager.ShowSettingsPopup(false);
	}
	#endregion
}

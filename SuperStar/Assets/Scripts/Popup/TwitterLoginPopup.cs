using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class TwitterLoginPopup : Popup, TwitterManagerCallbackInterface {
	
	public UIInput inputField;
	public UISysFontLabel labelUserName;
	public UISysFontLabel labelPassword;
	public string parentPopup;
	
	private string username = "";
	private string password = "";
	
	private volatile bool allowUpdate 	= true;
	private bool isUserNameOnFocus 		= false;
	private bool isLoading 				= false;
	
	public delegate void OnTwitterLoginPopupLoginSuccess();
	public delegate void OnTwitterLoginPopupLoginFailed();
	public OnTwitterLoginPopupLoginSuccess success;
	public OnTwitterLoginPopupLoginFailed failed;
	
	
	private void Init()
	{
		inputField.text = "";
		labelUserName.Text = "";
		labelPassword.Text = "";
		
		username = "";
		password = "";
	}
	
	public override void OnDestroy()
	{
		TwitterManager.SharedManager.RemoveCallback(this);	

		base.OnDestroy();
	}
	
	public override void Start()
	{
		base.Start();
		TwitterManager.SharedManager.SetCallback(this);
	}
	
	public override void Show()
	{
		Init();
		base.Show();
		transform.GetComponent<AnchorTweener>().Forward();
	}
	
	public override void Hide()
	{
		transform.GetComponent<AnchorTweener>().Backward();
		base.Hide();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	public override void OnBackPressed()
	{
		if (!isLoading)
			CrossPressed();
	}
	
	public override void Update()
	{
		base.Update();
		if (allowUpdate)
		{
			if (isUserNameOnFocus)
			{
				labelUserName.Text = inputField.text;
			}
			else
			{
				labelPassword.Text = "";
				for (int i = 0; i < inputField.text.Length; i++)
					labelPassword.Text += "*";
			}
		}
	}
	
	#region UIInput onFocus
	public void PasswordInputOnFocus()
	{
		getUserInput();
		
		inputField.text = labelPassword.Text;		
		isUserNameOnFocus = false;
		Log.Debug("Password Input Pressed");
		inputField.selected = true;
	}
	
	public void UsernameInputOnFocus()
	{
		getUserInput();
		
		inputField.text = labelUserName.Text;		
		isUserNameOnFocus = true;
		Log.Debug("Username Input Pressed");
		inputField.selected = true;
		
	}
	
	public void OnInputSubmit()
	{
		allowUpdate = false;
		getUserInput();
		allowUpdate = true;
		
		//inputField.text = "";
	}
	
	private void getUserInput()
	{
		if (inputField.text != null && !inputField.text.Equals(""))
		{
			if (isUserNameOnFocus)
				username = inputField.text;
			else
				password = inputField.text;
		}
	}
	
	private void ShowSocialLoginStatusPopup(string status, bool isError)
	{
		PopupManager.HideLoadingFlower();
		PopupManager.HideAllPopup();
		PopupManager.ShowSocialStatePopup(SocialNetwork.Type.Twitter, status, isError, false);
		
		if (parentPopup.Equals("SettingsPopup"))
			PopupManager.ShowSettingsPopup(false);
		else if (parentPopup.Equals("FriendListPopup"))
			PopupManager.ShowFriendListPopup(false);

		isLoading = false;
	}
	#endregion
	
	
	#region Button Clicked
	public void TickPressed()
	{
		isLoading = true;
		getUserInput();
		
		Log.Debug("Tick Pressed!!");
		//Log.Debug(username);
		//Log.Debug(password);
		
		PopupManager.ShowLoadingFlower();
		
		if (username == null || username.Equals("") || password == null || password.Equals(""))
		{
			ShowSocialLoginStatusPopup("Login error! Please Check ids and PW", true);
			return;
		}
		
		TwitterManager.SharedManager.UserName = username;
		TwitterManager.SharedManager.Password = password;
		TwitterManager.SharedManager.Login();
	}
	
	public void CrossPressed()
	{
		Hide();	
	}
	#endregion
	
	
	#region Twitter
	public void OnTwitterManagerLogin(string userId)
	{
		Log.Debug("Twitter OnTwitterManagerLogin");
		PopupManager.HideLoadingFlower();
		Globals.mainPlayer.twitterElement.socialId = userId;		
		QuestFlags.SetFlag(5,true);
		Hide();
		if (success != null)
			success();
		
		isLoading = false;
		/*
		if (parentPopup.Equals("SettingsPopup"))
		{
			PopupManager.HideLoadingFlower();
			ShowSocialLoginStatusPopup(Language.Get("login_success").Replace("%1", "Twitter"), false);
		}
		else
			Hide();
			*/
	}	
	
	public void OnTwitterManagerLoginFailed()
	{
		Log.Debug("Twitter Login Popup OnTwitterManagerLoginFailed");
		PopupManager.HideLoadingFlower();
		Hide();
		if (failed != null)
			failed();
		
		isLoading = false;
		/*
		if (parentPopup.Equals("SettingsPopup"))
			ShowSocialLoginStatusPopup(Language.Get("login_fail"), true);
		else
			Hide();
		*/
	}
	
	public void OnTwitterManagerError(Exception e)
	{
		Log.Debug("OnTwitterManager Error " + e.Message.ToString());		
		PopupManager.HideLoadingFlower();
		Hide();
		if (failed != null)
			failed();
		
		isLoading = false;
		/*
		if (parentPopup.Equals("SettingsPopup"))
			ShowSocialLoginStatusPopup("Twitter Error", true);
		else
			Hide();
		*/
	}
	
	public void OnTwitterManagerNewUserError(Hashtable socialInfo)
	{
		Log.Debug("The user is clean so as to create a new user profile and bound with this account");
		PopupManager.HideLoadingFlower();
		
		PopupManager.HideAllPopup();
		PopupManager.ShowDetectNewUserPopup(socialInfo, SocialNetwork.Type.Twitter, false);

		isLoading = false;
	}
	
	public void OnTwitterManagerExistingUserError(Hashtable existingUserInfo)
	{
		Log.Debug("There is an existing game save!! Wanna load it?");
		PopupManager.HideLoadingFlower();
		
		PopupManager.HideAllPopup();
		PopupManager.ShowDetectExistingUserPopup(existingUserInfo, SocialNetwork.Type.Twitter, false);

		isLoading = false;
	}
	
	public void OnTwitterManagerLogout(){}
	public void OnTwitterManagerGetUserProfile(ArrayList userInfos){}
	public void OnTwitterManagerGetFriendIds(ArrayList friendIds){}
	public void OnTwitterManagerUploadPhoto(){}
	public void OnTwitterManagerUploadPhotoError(string error){}
	#endregion
}

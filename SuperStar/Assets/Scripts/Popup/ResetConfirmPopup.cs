using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ResetConfirmPopup : Popup
{
	public UISysFontLabel msgText;
	public int floor;
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		msgText.Text = Language.Get("setting_reset"+floor);
		
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void Hide()
	{
		
		
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void OnBackPressed()
	{
		CrossClicked();
	}
	
	public void TickClicked()	
	{
		if (floor == 0)
		{
			//StartCoroutine(TickClickedRoutine());
			TickClickedRoutine();
		}
		else
		{
			PopupManager.ShowResetConfirmPopup(floor-1, false);

			Hide ();
		}
		

	}
	
	public void CrossClicked()
	{
		PopupManager.ShowSettingsPopup(false);
		Hide();
	}


	
	
	
	void TickClickedRoutine ()
	{
		//Log Flurry Event
		MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Reset_Game);

		//reset progress here
		Globals.mainPlayer.Exp 			= 0;
		Globals.mainPlayer.Energy 		= Globals.mainPlayer.MaxEnergy;

		DatabaseManager.SharedManager.cacher.setExp(0);
		DatabaseManager.SharedManager.cacher.setEnergy(Globals.mainPlayer.MaxEnergy);

		Globals.mainPlayer.owningItems 	= Items.stringToItemList(Globals.DEFAULT_purchasedItems);
		Globals.mainPlayer.wearingItems = Items.stringToItemList(CharacterManager.GenerateRandomDefaultWearingItemString());

		PosterManager.downloadedPosters = false;
		for (int i = 0; i < 3; i++)
		{
			PosterManager.posterClicked[i] = false;
		}
		//Globals.mainPlayer.wearingItems = Items.stringToItemList(Globals.DEFAULT_wearingItems);

		Globals.mainPlayer.ClothMatchHighScore 	= 0;
		Globals.mainPlayer.ClothMatchTotalScore = 0;

		Globals.mainPlayer.CurrentBoyfriendId 	= -1;
		foreach (Boyfriend b in Boyfriends.boyfriendsList)
		{
			b.flirtExp = 0;
			b.rewardTimer = -1;
			b.rewardClaimed = new List<int>(){0,0,0};
		}

		foreach (Job j in Jobs.jobsList)
		{
			j.timeLeft = -1;
		}

		foreach (Quest q in Quests.questsList)
		{
			q.StopQuest();
			q.isCompleted = false;
			q.isNew = true;
			q.completedOnce = false;
		}

		QuestFlags.listOfFlags = QuestFlags.initialListOfFlags;

		Globals.mainPlayer.InitializedPlayer = false;
		Globals.mainPlayer.Save();

		//yield return StartCoroutine(Globals.globalCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
		SceneManager.SharedManager.LoadScene(Scenes.TITLE, true);

		Hide();
	}
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TutorialPopup : Popup
{
	
	
	public override void Show()
	{
		base.Show();
		
		
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		StartCoroutine(SpecialTreatment());
	}
	
	IEnumerator SpecialTreatment()
	{
		yield return new WaitForEndOfFrame();
		foreach (SysFontLocalizer sfl in GetComponentsInChildren<SysFontLocalizer>())
		{
			
			
			if (sfl.identifier.Equals("tutorial_fd2"))
			{
				sfl.transform.GetComponent<UISysFontLabel>().Text = sfl.transform.GetComponent<UISysFontLabel>().Text.Replace("[Gem image]", Language.Get("currency_gems"));
			}
			
			else if (sfl.identifier.Contains("tutorial_livelysexy"))
			{
				sfl.transform.GetComponent<UISysFontLabel>().Text = sfl.transform.GetComponent<UISysFontLabel>().Text
					.Replace("[Lively]", "Lively")
					.Replace("[Sexy]", "Sexy")	;
			
			}
		}
	}
	
	public override void SilentHide()
	{
		base.SilentHide();
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void Hide()
	{		
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	
	
	
}

using UnityEngine;
using System.Collections;

public class DealPopup : Popup
{
	public Shop shop;
	
	public void setShop(Shop s)
	{
		shop = s;
		
		VisitShop.Visit(s, "Street");
	}
	
	public override void Show()
	{
		base.Show();
		
		
		transform.GetComponent<AnchorTweener>().Forward();		
	}
	
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
		
		//go back to minigame scene
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	
}

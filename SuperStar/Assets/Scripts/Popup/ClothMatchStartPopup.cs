using UnityEngine;
using System.Collections;

public class ClothMatchStartPopup : Popup
{
	public UIFilledSprite progressSprite;
	public UILabel percentValue;
	
	public override void Show()
	{
		base.Show();
		
		StartCoroutine(DelayedShow());
		
		transform.GetComponent<AnchorTweener>().Forward();
		
	}
	
	IEnumerator DelayedShow()
	{
		yield return new WaitForSeconds(0.05f);
		progressSprite.fillAmount = Mathf.Min(1f, 1f*Globals.mainPlayer.ClothMatchTotalScore/Globals.miniGame_clothMatch_scoreForGift);
		percentValue.text = (int)(progressSprite.fillAmount*100f) + "%";
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
		
		//go back to minigame scene
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	
	public override void OnBackPressed()
	{
		ExitClicked();
		//base.OnBackPressed();
		
		//Overrride the method but empty to prevent Button pressed
	}
	
	
	public void StartClicked()
	{
		Log.Debug("Start Clicked");
		
		GameObject.Find("Init").GetComponent<MiniGameClothMatchScene>().CheckRoutine();
		
		MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.PlaySuperstarRunway);
	}
	
	public void ExitClicked()
	{
		PopupManager.HideAllPopup();
		GameObject.Find("Init").GetComponent<MiniGameClothMatchScene>().DirectExit();
	}
}

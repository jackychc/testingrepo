using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class VotingRequestPopup : Popup
{
	public FriendRequestScrollListManager frsm;
	public UILabel votesLabel;
	
	public LoadingFlower loadingFlower;
	public GameObject noFriendsLabel;
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		setVoteNumber();
		
		
		StartCoroutine(GetFriends());
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public IEnumerator GetFriends()
	{
		List<Player> list = new List<Player>();
		yield return StartCoroutine(GetFriendsOnServer(result => list = result));
		
		loadingFlower.Hide();
		
		if (list != null)
		{
			if (list.Count <= 0)
			{
				noFriendsLabel.SetActive(true);
				noFriendsLabel.GetComponent<UISysFontLabel>().Text = "No friends available.";
			}
			
			frsm.LoadFriends(list);
		}
	}
	
	public void setVoteNumber()
	{
		votesLabel.text = "" + Globals.viewingTopic.votes;
	}
	
	public void SelectAll()
	{
		Log.Debug("select all clicked");
		foreach (FriendRequestItem fri in frsm.friendRequests)
		{
			fri.setTrue();
		}
	}
	
	public void SubmitRequest()
	{
		Log.Debug("submit clicked");
		List<string> listOfGameIds = new List<string>();
		
		foreach (FriendRequestItem fri in frsm.friendRequests)
		{
			if (fri.selected)
				listOfGameIds.Add(fri.gameId);
		}
		
		if (listOfGameIds.Count > 0)
		{
			Log.Debug("abcd");
			// actually fire the request
			StartCoroutine(SubmitRequestCoroutine(listOfGameIds));
			
		}
		
		else
		{
			Hide();
		}
	}
	
	IEnumerator SubmitRequestCoroutine(List<string> input)
	{
		yield return StartCoroutine(SubmitRequestToServer(input));
		
		Hide();
	}
	
	public void Join()
	{
		Log.Debug("join!");
		
		
		//transform.GetComponent<SceneManager>().GoToScene();
		//StartCoroutine(JoinTopicOnServer());
		
		
	}
	
	// here we won't use local friend list
	IEnumerator GetFriendsOnServer(Action<List<Player>> list)
	{
		List<Player> returnMe 	= new List<Player>();		
		string topicId			= Globals.viewingTopic.topicId.ToString();
		
		//Prepare CheckSum
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + topicId;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("GetFriendsForVoteRequests", checkSumData);		
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("topicId", topicId);
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("checkSum", checkSum);
	
		string url = Globals.serverAPIURL + "GetFriendsForVoteRequests.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response 			= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			//yield return StartCoroutine(HTTPPost.Post("GetFriends", kvp, result => response = result));
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					if (al[1] != null)
					{
						ArrayList alt = new ArrayList(((Hashtable) al[1]).Values);
							
						for (int i = 0; i < alt.Count; i++)
						{
							Hashtable innerHt = (Hashtable) alt[i];
							
							Player p = new Player();
							p.playerId = Int32.Parse(innerHt["playerId"].ToString());
							p.GameId = innerHt["gameId"].ToString();
							p.Name = innerHt["playerName"] != null? innerHt["playerName"].ToString() : innerHt["gameId"].ToString();
							
							returnMe.Add(p);
						}
					}
					
					list(returnMe);
				}
				else
				{
					// ?
					list(null);
				}
			}
			else
				list(null);
		}
		
		//yield return StartCoroutine(HTTPPost.Post("GetFriendsForVoteRequests", kvp, result => response = result));
	}
	
	IEnumerator SubmitRequestToServer(List<string> input)
	{
		List<Player> returnMe = new List<Player>();
		
		string topicId 			= Globals.viewingTopic.topicId.ToString();
		string targetGameIds 	= string.Join(",", input.ToArray());
		
		//Prepare CheckSum
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + topicId + targetGameIds;
		string checkSum		= MD5Hash.GetServerCheckSumWithString("SendVoteRequest", checkSumData);
			
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("topicId", topicId);
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("targetGameIds", targetGameIds);
		kvp.Add("checkSum", checkSum);
	
		string url = Globals.serverAPIURL + "SendVoteRequest.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response 			= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			//yield return StartCoroutine(HTTPPost.Post("GetFriends", kvp, result => response = result));
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					PopupManager.ShowInformationPopup("Vote request sent successfully.", false);
				}
				else
				{
					// ?
					PopupManager.ShowInformationPopup("Vote request failed. Please try again later.", false);
					
				}
			}
		}	
	}
}

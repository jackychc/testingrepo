using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BoyfriendRewardPopup : Popup
{
	public Boyfriend boyfriend;
	int gemsForInstant = 40;
	
	public GameObject[] ticks = new GameObject[3];
	public UISprite[] giftBox = new UISprite[3];
	public UILabel[] times = new UILabel[3];
	public GameObject claimButton, instantButton;
	public UILabel gemsForInstantLabel;
	
	
	public GameObject[] objectsToHide = new GameObject[3];
	
	int claimedRewards = 0;

	int interval, totalProportions, currentProportion, proportionGems;
	
	public void setBoyfriend(Boyfriend f)
	{
		boyfriend = f;
	}
	
	
	public override void Show()
	{
		base.Show();
		
		if (HaveClaimableRewards())
		{
			ClaimRewards();
		}
		
		else
		{
			transform.GetComponent<AnchorTweener>().Forward();
		
			StartCoroutine(UpdateEverySecond());
		}
		
	}
	
	public override void SilentHide()
	{
		base.SilentHide();
		transform.GetComponent<AnchorTweener>().Backward();
		
		GameObject init  = GameObject.Find("Init");
		if (init != null)
		{
			if (init.GetComponent<HomeScene>() != null)
			{
				init.GetComponent<HomeScene>().CheckBoyfriendReward();
			}
		}
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
		
		GameObject init  = GameObject.Find("Init");
		if (init != null)
		{
			if (init.GetComponent<HomeScene>() != null)
			{
				init.GetComponent<HomeScene>().CheckBoyfriendReward();
			}
		}
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	IEnumerator UpdateEverySecond()
	{
		CheckBoyfriendReward();
		
		yield return new WaitForSeconds(1.0f);
		
		StartCoroutine(UpdateEverySecond());
	}
	
	bool HaveClaimableRewards()
	{
		if (boyfriend.rewardTimer >= 0)
		{	
			
			for (int i = 0 ; i < Boyfriends.checkPoints.Count; i++)
			{
				if (boyfriend.rewardTimer <= Boyfriends.checkPoints[i])
				{
					if (boyfriend.rewardClaimed[i] == 0)
					{
						// not claimed
						return true;
					}
					
				}
			}
		}
			
		return false;
	}
	
	void CheckBoyfriendReward()
	{
		Boyfriend myBf = boyfriend;
		int nearestCheckpoint = 0;
		int claimableRewards = 0;
		claimedRewards = 0;
		
		if (myBf.rewardTimer >= 0)
		{	
			
			for (int i = 0 ; i < Boyfriends.checkPoints.Count; i++)
			{
				if (myBf.rewardTimer <= Boyfriends.checkPoints[i])
				{
					giftBox[i].color = Color.white;
					
					if (myBf.rewardClaimed[i] == 0)
					{
						// not claimed
						claimableRewards++;
						
						
						ticks[i].SetActive(false);
					}
					else
					{
						// claimed
						claimedRewards++;
						ticks[i].SetActive(true);
					}
					
				}
				else
				{
					giftBox[i].color = Color.gray;
					
					if (nearestCheckpoint == 0)
						nearestCheckpoint = Boyfriends.checkPoints[i];
				}
			}
		}

		int displayTimer = myBf.rewardTimer - nearestCheckpoint;
		
		if (claimableRewards > 0)
		{
			// claim button
			claimButton.SetActive(true);
			instantButton.SetActive(false);
			
			for (int i = 0; i < 3; i++)
			{
				objectsToHide[i].SetActive(false);
			}
			
			for (int i = 0; i < 3; i++)
			{
			
				Vector3 v = giftBox[i].transform.localPosition;
				v.y = 0f;
				giftBox[i].transform.localPosition = v;
				
				v = ticks[i].transform.localPosition;
				v.y = 0f;
				ticks[i].transform.localPosition = v;

			}
		}
		
		else
		{
			// instantly get gift
			claimButton.SetActive(false);
			instantButton.SetActive(true);
			//Log.Debug("instant cost length:"+boyfriend.instantGiftCost.Count+", where claimed rewards:"+claimedRewards);

			if (claimedRewards > 0)
			{
				interval = Boyfriends.checkPoints[claimedRewards-1] - Boyfriends.checkPoints[claimedRewards];

				totalProportions = Mathf.CeilToInt(interval/1800f);
				currentProportion = Mathf.CeilToInt(displayTimer/1800f);

				proportionGems = Mathf.CeilToInt (boyfriend.instantGiftCost[claimedRewards] * (1f* currentProportion/totalProportions));
			}
			else
			{
				// unreachable code
				proportionGems = boyfriend.instantGiftCost[0];
			}
			// make the last digit be "9"
			if (proportionGems > 10 && proportionGems%10 == 0)
				proportionGems--;

			gemsForInstant = proportionGems;
			//gemsForInstant = boyfriend.instantGiftCost[claimedRewards];
			gemsForInstantLabel.text = "x" + gemsForInstant;
		}
		
		
		

			
		times[0].text = twoDigit(displayTimer/3600);
		times[1].text = twoDigit((displayTimer%3600)/60);
		times[2].text = twoDigit(displayTimer%60);
	}
	
	string twoDigit(int i)
	{
		return i < 10? ("0" + i) : (i + "");
	}
	
	void ClaimRewards()
	{
		Boyfriend myBf = boyfriend;
		List<Reward> rewardList = new List<Reward>();
		
		if (myBf.rewardTimer >= 0)
		{	
			
			for (int i = 0 ; i < Boyfriends.checkPoints.Count; i++)
			{
				if (myBf.rewardTimer <= Boyfriends.checkPoints[i])
				{
					if (myBf.rewardClaimed[i] == 0)
					{
						myBf.rewardClaimed[i] = 1;
						rewardList.Add(Rewards.getReward(myBf.rewardIds[i]));
						
						// one reward each time!
						break;
					}
				}
			}
		}

		//Log FlurryEvent
		Hashtable param = new Hashtable();
		param["receiveMethod"] = "ReceiveByTime";
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.BF_Gift_Receive_Method, param);
		
		PopupManager.ShowRewardPopup(Rewards.CombineRewards(rewardList), false);		
		
		Hide();
	}
	
	void ClaimAllRewards()
	{
		boyfriend.rewardTimer = Boyfriends.checkPoints[claimedRewards];
		ClaimRewards();
	}
	
	void InstantClicked()
	{
		if (Globals.mainPlayer.Gems >= gemsForInstant)
		{
			//Log FlurryEvent
			Hashtable param = new Hashtable();
			param["receiveMethod"] = "InstantFinish";
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.BF_Gift_Receive_Method, param);			
			
			StartCoroutine(InstantClickedRoutine());			
		}
		else
		{
			// iap popup.....
			PopupManager.ShowNotEnoughCurrencyPopup("Gems", gemsForInstant-Globals.mainPlayer.Gems, false);
			Hide ();
		}
	}
	
	public IEnumerator InstantClickedRoutine()
	{
		transform.GetComponent<AnchorTweener>().Backward();
		
		GameObject init  = GameObject.Find("Init");
		if (init != null)
		{
			if (init.GetComponent<HomeScene>() != null)
			{
				yield return StartCoroutine(init.GetComponent<HomeScene>().GetGiftAnimation());
				
			}
		}
		
		Globals.mainPlayer.GainGems(-gemsForInstant);
		ClaimAllRewards();
		
		yield return null;
	}
	

}

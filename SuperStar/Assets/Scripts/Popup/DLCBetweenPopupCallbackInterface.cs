using UnityEngine;
using System.Collections;

public interface DLCBetweenPopupCallbackInterface {
	void OnDLCBetweenPopupClicked();
}

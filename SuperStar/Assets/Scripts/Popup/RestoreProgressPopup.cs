using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RestoreProgressPopup : Popup, DatabaseManagerCallback {
	
	public SocialNetwork.Type socialType;
	public GameObject bgButtonObj;
	public UIFilledSprite filledSpriteProgress;
	public UISysFontLabel labelProgressTitle;
	public UISysFontLabel labelProgress;
	public UISysFontLabel labelFinish;
	
	private float restoreProgress = 0.0f;
	
	public Hashtable userInfo;
	public bool isNewUser = false;
	
	private bool restoreCompleted = false;
	
	public override void Start()
	{
		base.Start();
		DatabaseManager.SharedManager.SetCallback(this);
		
	}
	
	public override void OnDestroy()
	{
		DatabaseManager.SharedManager.RemoveCallback(this);
		base.OnDestroy();
	}
	
	public override void Show()
	{
		base.Show();		
		transform.GetComponent<AnchorTweener>().Forward();
		
		filledSpriteProgress.fillAmount 	= 0.0f;
		labelProgressTitle.Text 			= Language.Get("msg_restoring");
		
		StartCoroutine(PrepareToRestore());		
	}
	
	public IEnumerator PrepareToRestore()
	{
		yield return 1.0f;
		
		StartCoroutine(StartRestoreProgress());
	}
	
	private IEnumerator StartRestoreProgress()
	{
		Log.Debug("Start Restore Progress");
		
		if (userInfo != null)	
		{
			if (!isNewUser)
			{
				Hashtable playerMeta = (Hashtable)userInfo["playerMeta"];
				Hashtable playerInfo = (Hashtable)userInfo["playerInfo"];
				Hashtable playerQuest = (Hashtable)userInfo["playerQuest"];
				Log.Debug(playerMeta.toJson());
				Log.Debug(playerInfo.toJson());
				Log.Debug(playerQuest.toJson());
				//Checking for the 
				if (playerInfo != null && playerMeta != null && playerQuest != null)
				{
					if (playerMeta.ContainsKey("dlcVersion"))
					{
						int dlcVersion = (int)playerMeta["dlcVersion"];
						if (Globals.currentDLCVersion < dlcVersion)
						{
							DownloadManager dm = DownloadManager.SharedManager;
							if (dm != null)
							{
								yield return StartCoroutine(dm.EnumStartDLC(false, dlcVersion, 0));
							}
						}
					}
					
					//Initiate the dlc
					DatabaseManager.SharedManager.RestoreGameSave(userInfo);
				}
				else
				{
					OnDatabaseManagerRestoreGameSaveFailed();	
				}
			}
			else
			{
				DatabaseManager.SharedManager.RestoreNewGameSave(userInfo);
			}
		}
		yield return null;
	}
	
	public override void Hide()
	{		
		if (restoreCompleted)
		{			
			base.Hide();		
			transform.GetComponent<AnchorTweener>().Backward();

			SceneManager.SharedManager.LoadScene(Scenes.TITLE, true);
		}
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}

	public override void OnBackPressed()
	{
		//base.OnBackPressed();
	}
	
	public override void Update()
	{
		base.Update();
		
		if (labelProgress != null)
			labelProgress.Text = Mathf.RoundToInt(restoreProgress * 100f) + "%";
		if (filledSpriteProgress != null)
			filledSpriteProgress.fillAmount = restoreProgress;
			
	}
	
	private IEnumerator ShowRestoreProgressAnimation()
	{
		float loadStart = Time.time;
		float loadEnd = loadStart + 2;
		restoreProgress = 0.0f;
		
		while(filledSpriteProgress.fillAmount < 1.0f)
		{
			restoreProgress = Mathf.Clamp(Mathf.InverseLerp(loadStart,loadEnd,Time.time),0 ,1f);
			yield return new WaitForEndOfFrame();
		}
		
		filledSpriteProgress.fillAmount = 1.0f;
		labelProgress.Text = DatabaseManager.SharedManager.restoreProgress * 100  + "%";
		labelProgressTitle.Text = Language.Get("msg_restoresuccess");
		
		if (labelFinish != null)
		{
			labelFinish.Text = Language.Get("msg_restoresuccessDesc");
			labelFinish.gameObject.SetActive(true);	
		}
		
		restoreCompleted = true;
	}
	
	#region Database Callback
	public void OnDatabaseManagerRestoreGameSaveCompleted()
	{
		//Read the current DLC Version
		//Download the DLC to keep 
		//labelProgressTitle.Text = Language.Get("msg_restoresuccess");
		//filledSpriteProgress.fillAmount = 1.0f;
		//restoreCompleted = true;
		
		StartCoroutine(ShowRestoreProgressAnimation());
		
		//Reload userIds
		FacebookManager.SharedManager.ReloadUserId();
		GooglePlusManager.SharedManager.ReloadUserId();
		TwitterManager.SharedManager.ReloadUserId();
		
		if (socialType.Equals(SocialNetwork.Type.Facebook))
		{
			GooglePlusManager.SharedManager.SilentSignOut();
			TwitterManager.SharedManager.SilentLogout();
		}
		else if (socialType.Equals(SocialNetwork.Type.Twitter))
		{
			FacebookManager.SharedManager.SilentLogout();
			GooglePlusManager.SharedManager.SilentSignOut();
		}
		else if (socialType.Equals(SocialNetwork.Type.GooglePlus))
		{
			FacebookManager.SharedManager.SilentLogout();
			TwitterManager.SharedManager.SilentLogout();
		}
			
		//Enable the button to press
		//bgButtonObj.GetComponent<UIButtonMessage>().enabled = true;
		
		
		//Clear the the corresponding tables		
		//Hashtable userInfo = (Hashtable)MiniJSON.JsonDecode("{\"playerInfo\":{\"playerId\":\"1000506\",\"gameId\":\"00LFZU\",\"playerName\":\"P00LFZU\",\"exp\":\"0\",\"energy\":\"30\",\"maxEnergy\":\"30\",\"coins\":\"9799\",\"gems\":\"9999\",\"profilePicURL\":null,\"skills\":\"{0:1|1:0|2:0|3:0|4:0}\",\"skillTitle\":null,\"boyfriendId\":\"-1\",\"purchasedItems\":\"1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,19,20,52,21,22,23,24,25,26,27,28,29,3054,55,56,57,58,59,60,61,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,71,72,73,74,75,78,79,80,95,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147148,149,150,151,152,153,154,155156,157,158,159,160,161,162,163,106,101\",\"wearingItems\":\"34,1,21,71,107,114,119,127,137,148,156,101\",\"munerisInstallId\":\"123123\",\"lastSyncDate\":\"2013-08-09 06:56:07\"},\"playerMeta\":{\"0\":{\"metaKey\":\"dlcVersion\",\"playerId\":\"1000506\",\"intValue\":\"0\",\"strValue\":null},\"1\":{\"metaKey\":\"energyInterval\",\"playerId\":\"1000506\",\"intValue\":\"360\",\"strValue\":null},\"2\":{\"metaKey\":\"energyTimer\",\"playerId\":\"1000506\",\"intValue\":\"341\",\"strValue\":null},\"3\":{\"metaKey\":\"minimapPosition\",\"playerId\":\"1000506\",\"intValue\":null,\"strValue\":\"0|0\"},\"4\":{\"metaKey\":\"streetPosition\",\"playerId\":\"1000506\",\"intValue\":\"606\",\"strValue\":null}},\"playerBoyfriends\":{\"0\":{\"boyfriendId\":\"1\",\"playerId\":\"1000506\",\"boyfriendName\":null,\"flirtExp\":\"0\",\"flirtExpMax\":\"10000\",\"charismaRequired\":\"1\",\"location\":\"30,20,15\",\"rewardIds\":\"0\",\"flirtCost\":\"1,2,3,4,5,6\",\"flirtRepeat\":\"33,34,35\",\"rewardClaimed\":\"0,0,0\",\"rewardTimer\":\"-1\"},\"1\":{\"boyfriendId\":\"2\",\"playerId\":\"1000506\",\"boyfriendName\":null,\"flirtExp\":\"0\",\"flirtExpMax\":\"10000\",\"charismaRequired\":\"1\",\"location\":\"30,20,15\",\"rewardIds\":\"10\",\"flirtCost\":\"1,2,3,4,5,6\",\"flirtRepeat\":\"33,34,35\",\"rewardClaimed\":\"0,0,0\",\"rewardTimer\":\"-1\"},\"2\":{\"boyfriendId\":\"3\",\"playerId\":\"1000506\",\"boyfriendName\":null,\"flirtExp\":\"0\",\"flirtExpMax\":\"10000\",\"charismaRequired\":\"1\",\"location\":\"30,20,15\",\"rewardIds\":\"10\",\"flirtCost\":\"1,2,3,4,5,6\",\"flirtRepeat\":\"33,34,35\",\"rewardClaimed\":\"0,0,0\",\"rewardTimer\":\"-1\"},\"3\":{\"boyfriendId\":\"4\",\"playerId\":\"1000506\",\"boyfriendName\":null,\"flirtExp\":\"0\",\"flirtExpMax\":\"10000\",\"charismaRequired\":\"1\",\"location\":\"30,20,15\",\"rewardIds\":\"10\",\"flirtCost\":\"1,2,3,4,5,6\",\"flirtRepeat\":\"33,34,35\",\"rewardClaimed\":\"0,0,0\",\"rewardTimer\":\"-1\"},\"4\":{\"boyfriendId\":\"5\",\"playerId\":\"1000506\",\"boyfriendName\":null,\"flirtExp\":\"0\",\"flirtExpMax\":\"10000\",\"charismaRequired\":\"1\",\"location\":\"30,20,15\",\"rewardIds\":\"20\",\"flirtCost\":\"1,2,3,4,5,6\",\"flirtRepeat\":\"33,34,35\",\"rewardClaimed\":\"0,0,0\",\"rewardTimer\":\"-1\"},\"5\":{\"boyfriendId\":\"6\",\"playerId\":\"1000506\",\"boyfriendName\":null,\"flirtExp\":\"0\",\"flirtExpMax\":\"10000\",\"charismaRequired\":\"1\",\"location\":\"30,20,15\",\"rewardIds\":\"40\",\"flirtCost\":\"1,2,3,4,5,6\",\"flirtRepeat\":\"33,34,35\",\"rewardClaimed\":\"0,0,0\",\"rewardTimer\":\"-1\"}},\"playerSocial\":{\"0\":{\"playerId\":\"1000506\",\"socialNetworkType\":\"facebook\",\"socialNetworkId\":\"100004120415001\"}}}");
		//Log.Debug("Restore DonE");
	}
	
	public void OnDatabaseManagerRestoreGameSaveFailed()
	{
		Log.Debug("Restore failed!");
		//Show Restore Error Popup
		PopupManager.ShowInformationPopup(Language.Get("msg_restorefailed"), false);
		Hide();
	}
	
	public void OnDatabaseManagerRestoreNewSaveCompleted()
	{
		StartCoroutine(ShowRestoreProgressAnimation());
		
		if (socialType.Equals(SocialNetwork.Type.Facebook))
		{
			FacebookManager.SharedManager.ReloadUserId();

			GooglePlusManager.SharedManager.SilentSignOut();
			TwitterManager.SharedManager.SilentLogout();
		}
		else if (socialType.Equals(SocialNetwork.Type.Twitter))
		{
			TwitterManager.SharedManager.ReloadUserId();	

			GooglePlusManager.SharedManager.SilentSignOut();
			FacebookManager.SharedManager.SilentLogout();
		}
		else if (socialType.Equals(SocialNetwork.Type.GooglePlus))
		{
			GooglePlusManager.SharedManager.ReloadUserId();	

			FacebookManager.SharedManager.SilentLogout();
			TwitterManager.SharedManager.SilentLogout();
		}
	}
	#endregion
}

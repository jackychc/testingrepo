using UnityEngine;
using System.Collections;

public class FriendInformationPopup : Popup
{
	public Player friend;
	public UISysFontLabel friendName;
	public UILabel friendId;
	
	public void setFriend(Player f)
	{
		friend = f;
		friendName.Text = ""+friend.Name;
		friendId.text = ""+friend.GameId;
	}
	
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		
		
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void Visit()
	{
		Hide ();
		VisitFriend.Visit(friend, "FRIENDS");
		
		//FriendsHome
		SceneManager.SharedManager.LoadScene(Scenes.FRIENDSHOME, true);
		//transform.GetComponent<SceneManager>().GoToScene();
	}


}

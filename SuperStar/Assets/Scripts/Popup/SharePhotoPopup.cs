using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class SharePhotoPopup : Popup, FacebookManagerCallbackInterface, TwitterManagerCallbackInterface, TwitterLoginPopupCallbackInterface {
	
	public UISysFontLabel labelTitle;
	public Texture2D screenshotTexture;
	private string tempFileNamePath;
	
	#region Popup
	public override void OnDestroy()
	{
		FacebookManager.SharedManager.RemoveCallback(this);
		TwitterManager.SharedManager.RemoveCallback(this);

		base.OnDestroy();
	}
	
	public override void Start()
	{
		base.Start();
		TwitterManager.SharedManager.SetCallback(this);
		FacebookManager.SharedManager.SetCallback(this);
	}
	
	public override void Show()
	{
		labelTitle.Text	= Language.Get("share_title");
		
		base.Show();
		transform.GetComponent<AnchorTweener>().Forward();		
	}
	
	public override void Hide()
	{
		base.Hide();		
		transform.GetComponent<AnchorTweener>().Backward();		
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	#endregion
	
	private IEnumerator LogFlurryEvent(string shareMethod)
	{
		Hashtable param = new Hashtable();
		
		Scenes currentScene = SceneManager.SharedManager.CurrScene;
		if (currentScene.Equals(Scenes.PHOTOSHOT))
		{
			GameObject boyfriendModel = GameObject.Find("BOYFRIEND");
			if (boyfriendModel != null)
				param["Share"] = "Boyfriend " + shareMethod;
			else
				param["Share"] = "Single " + shareMethod;				
		}
		else if (currentScene.Equals(Scenes.FRIENDSHOME))
		{
			param["Share"] = "Friends " + shareMethod;
		}
		
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Camera_TakePhoto, param);
		
		yield return null;
	}
	
	#region Manage Photo Texture
	private string SaveTempPhoto()
	{
		string storagePath 	= UtilsHelper.GetWriteableDirectory();
		string filePath 	= storagePath + Path.DirectorySeparatorChar + Globals.appName + Path.DirectorySeparatorChar;
		string fileNamePath = filePath + System.DateTime.Now.Ticks + ".png";
		
		if (!Directory.Exists(filePath))
			Directory.CreateDirectory(filePath);
		var imageBytes = screenshotTexture.EncodeToPNG();		
		File.WriteAllBytes(fileNamePath, imageBytes);
		UtilsHelper.RefreshDirectoryWithFileName(fileNamePath);
		
		return fileNamePath;
	}
	
	public void DeleteTempPhoto(string fileNamePath)
	{
		FileInfo fileInfo = new FileInfo(fileNamePath);
		if (fileInfo != null && fileInfo.Exists)
			File.Delete(fileNamePath);
	}
	#endregion
	
	#region Button Click
	public void ButtonTwitterClicked()
	{
		if (!TwitterManager.SharedManager.IsLoggedIn())
		{
			//PopupManager.ShowTwitterLoginPopup("SharePhotoPopup", true);
			//string errorMsg = Language.Get("login_fail").Replace("%1", "Twitter");
			//PopupManager.ShowSocialStatePopup(SocialNetwork.Type.Twitter, errorMsg, true, true);
			
			PopupManager.ShowTwitterLoginPopup(this, true);
		}
		else
		{
			PopupManager.ShowLoadingFlower();
			
			tempFileNamePath = SaveTempPhoto();
			TwitterManager.SharedManager.PostStatusWithImage("", tempFileNamePath);
			
			/*
			string storagePath 	= UtilsHelper.GetWriteableDirectory();
			string filePath 	= storagePath + Path.DirectorySeparatorChar + Globals.appName + Path.DirectorySeparatorChar;
			string fileName 	= filePath + "635137209978324630.png";
			TwitterManager.SharedManager.PostStatusWithImage("hihi", fileName);*/
		}
	}
	
	public void ButtonFacebookClicked()
	{
		//if (!FacebookManager.SharedManager.IsLoggedIn())
		//{
		//	Hide();
		//	FacebookManager.SharedManager.Login();
		//}
		//else
		//{
			PopupManager.ShowLoadingFlower();
			
			tempFileNamePath = SaveTempPhoto();
			FacebookManager.SharedManager.UploadPhoto(" ", tempFileNamePath);
		//}
	}
	#endregion
	
	#region TwitterLoginPopup Callback
	public void OnTwitterLoginPopupLoginSuccess()
	{
		ButtonTwitterClicked();
	}
	
	public void OnTwitterLoginPopupLoginFailed()
	{
		OnAccountError(SocialNetwork.Type.Twitter);
	}
	#endregion
	
	
	#region TwitterManagerCallback
	public void OnTwitterManagerGetUserProfile(ArrayList userInfos){}
	public void OnTwitterManagerGetFriendIds(ArrayList friendIds){}
	public void OnTwitterManagerLogout(){}
	public void OnTwitterManagerError(Exception e){}
	public void OnTwitterManagerLoginFailed(){}
	public void OnTwitterManagerLogin(string userId){}
	public void OnTwitterManagerNewUserError(Hashtable socialInfo){}	
	public void OnTwitterManagerExistingUserError(Hashtable existingUserInfo){}
	
	public void OnTwitterManagerUploadPhoto()
	{
		StartCoroutine(LogFlurryEvent("Twitter"));
		
		if (tempFileNamePath != null)
			DeleteTempPhoto(tempFileNamePath);		
		
		Hide();
		PopupManager.HideLoadingFlower();
		
		string msg = Language.Get("share_success").Replace("%1", "Twitter");
		PopupManager.ShowSocialStatePopup(SocialNetwork.Type.Twitter, msg, false, true);
		
		QuestFlags.SetFlag(9,true);
	}
	
	public void OnTwitterManagerUploadPhotoError(string error)
	{
		if (tempFileNamePath != null)
			DeleteTempPhoto(tempFileNamePath);
		
		Hide();
		PopupManager.HideLoadingFlower();
		
		string msg = Language.Get("share_fail");
		PopupManager.ShowSocialStatePopup(SocialNetwork.Type.Twitter, msg, true, true);	
	}	
	#endregion
	
	#region FacebookManagerCallback
	public void OnFacebookManagerGetFriends(ArrayList friendList){}
	public void OnFacebookManagerLogin(string userId){}
	public void OnFacebookManagerLogout(){}

	public void OnFacebookManagerLoginCancel()
	{
		PopupManager.HideLoadingFlower();
	}
		
	public void OnFacebookManagerError(Exception e)
	{
		if (tempFileNamePath != null)
			DeleteTempPhoto(tempFileNamePath);
		
		Hide();
		PopupManager.HideLoadingFlower();
		
		string msg = Language.Get("share_fail");
		PopupManager.ShowSocialStatePopup(SocialNetwork.Type.Facebook, msg, true, true);
	}
	
	public void OnFacebookManagerUploadPhoto()
	{
		StartCoroutine(LogFlurryEvent("Facebook"));
		
		if (tempFileNamePath != null)
			DeleteTempPhoto(tempFileNamePath);		
		
		Hide();
		PopupManager.HideLoadingFlower();
		
		string msg = Language.Get("share_success").Replace("%1", "Facebook");
		PopupManager.ShowSocialStatePopup(SocialNetwork.Type.Facebook, msg, false, true);
		
		QuestFlags.SetFlag(9,true);
	}
	
	
	//Checking for the differet facebook account logged in
	public void OnFacebookManagerNewUserError(Hashtable userInfo)
	{
		PopupManager.HideLoadingFlower();
		PopupManager.ShowDetectNewUserPopup(userInfo, SocialNetwork.Type.Facebook, true);
		//OnAccountError(SocialNetwork.Type.Facebook);
	}
	
	public void OnFacebookManagerExistingUserError(Hashtable existingUserInfo)
	{
		PopupManager.HideLoadingFlower();
		PopupManager.ShowDetectExistingUserPopup(existingUserInfo, SocialNetwork.Type.Facebook, true);
		//OnAccountError(SocialNetwork.Type.Facebook);
	}
	
	
	private void OnAccountError(SocialNetwork.Type type)
	{
		Log.Debug("Show Account Error Popup");
		string message = "";
		switch(type)
		{
			case SocialNetwork.Type.Facebook:
				message = Language.Get("login_fail_settings").Replace("%1", "Facebook");
				break;
			case SocialNetwork.Type.GooglePlus:
				message = Language.Get("login_fail_settings").Replace("%1", "Google+");
				break;
			case SocialNetwork.Type.Twitter:
				message = Language.Get("login_fail_settings").Replace("%1", "Twitter");
				break;
		}
		PopupManager.ShowInformationPopup(message, true);
		//PopupManager.ShowSharePhotoPopup(screenshotTexture, false);
	}
	#endregion
}

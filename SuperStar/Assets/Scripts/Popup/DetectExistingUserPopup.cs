using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

public class DetectExistingUserPopup : Popup, FacebookManagerCallbackInterface, GooglePlusManagerCallbackInterface, TwitterManagerCallbackInterface
{
	public enum ButtonState
	{
		None,
		Cross,
		Tick,
	}
	
	public SocialNetwork.Type type;
	public Hashtable userInfo;
	private string socialNetworkId = "";
	
	public UISysFontLabel labelTitle;
	public UISysFontLabel labelDesc;
	
	public UISysFontLabel labelName;
	public UISysFontLabel labelID;
	public GameObject profilePicSpriteObj;
	public GameObject profilePicTextureObj;
	
	private ButtonState buttonClicked 	= ButtonState.None;
	private bool isValidToRestore		= false;
	private int gsDLCVersion			= -1;
	
	private void Init()
	{
		buttonClicked = ButtonState.None;

		if (userInfo != null)
		{
			string playerName = "";
			string gameId = "";
			string profilePicURL = "";
			
			if (type.Equals(SocialNetwork.Type.Facebook))
			{
				Hashtable socialFBInfo = (Hashtable)userInfo["PlayerSocialFB"];
				Hashtable playerInfo = (Hashtable)userInfo["playerInfo"];
				
				socialNetworkId = "" + socialFBInfo["id"];
				
				profilePicURL = FacebookManager.GetProfilePicURL(socialNetworkId, 100, 100);
				gameId = "" + playerInfo["gameId"];
				playerName = "" + playerInfo["playerName"];
				/*if (playerName == null || playerName.Equals(""))
				{
					playerName = "" +socialFBInfo["id"];
				}*/
			}
			else if (type.Equals(SocialNetwork.Type.GooglePlus))
			{
				Hashtable playerInfo = (Hashtable)userInfo["playerInfo"];
				Hashtable googlePlusInfo = (Hashtable)userInfo["PlayerSocialGP"];
				
				socialNetworkId = "" + googlePlusInfo["socialNetworkId"];
				gameId = "" + playerInfo["gameId"];
				playerName = "" + playerInfo["playerName"];
				profilePicURL = "" + googlePlusInfo["profilePicURL"];				
			}
			else if (type.Equals(SocialNetwork.Type.Twitter))
			{
				Hashtable playerInfo = (Hashtable)userInfo["playerInfo"];
				Hashtable twitterInfo = (Hashtable)userInfo["PlayerSocialTW"];
				
				Log.Debug(userInfo.toJson().ToString());
				
				socialNetworkId = "" + twitterInfo["id"];
				gameId = "" + playerInfo["gameId"];
				playerName = "" + playerInfo["playerName"];
				profilePicURL = "" + twitterInfo["profile_image_url"];
			}
			
			labelName.Text 	= playerName;				
			labelID.Text 	= gameId;
			labelTitle.Text = Language.Get("msg_detectExistingSave");
			labelDesc.Text	= Language.Get("msd_detectExistingSaveDesc");
			
			StartCoroutine(GetFBProfilePic(profilePicURL));
		}
	}
	
	private IEnumerator GetFBProfilePic(string url)
	{
		Log.Debug(url);
		WWW www = new WWW(url);
		yield return www;
		
		if (www.error == null)
		{
			Texture2D texture = new Texture2D(100, 100);
			texture.LoadImage(www.bytes);
			
			if (texture != null)
			{
				Destroy(profilePicSpriteObj);
				
				UITexture uiTextureProfilePic = profilePicTextureObj.GetComponent<UITexture>();
				uiTextureProfilePic.mainTexture = texture;
			}
		}
		else
		{
			Log.Debug("Cannot fetch the image from the url");	
		}		
	}
	
	private bool IsLocalDLCValid()
	{
		if (userInfo != null)
		{
			Hashtable playerInfoHT 	= (Hashtable)userInfo["playerMeta"];
			for (int i = 0; i < playerInfoHT.Count; i++)
			{
				Hashtable metaHT	= (Hashtable)playerInfoHT["" + i];
				string metaKey		= (string)metaHT["metaKey"];
				if (metaKey.Equals("dlcVersion"))
				{
					gsDLCVersion = int.Parse("" + metaHT["intValue"]);
					if (gsDLCVersion <= Globals.currentDLCVersion)
						return true;
					return false;
				}
			}
		}
		return false;
	}
	
	public override void Show()
	{
		Init();		
		base.Show();		
		transform.GetComponent<AnchorTweener>().Forward();
		
		//messageLabel.Text = Language.Get("vote_confirmandjoin");
	}
	
	public override void OnDestroy()
	{
		buttonClicked = ButtonState.None;
		FacebookManager.SharedManager.RemoveCallback(this);
		GooglePlusManager.SharedManager.RemoveCallback(this);
		TwitterManager.SharedManager.RemoveCallback(this);
		base.OnDestroy();
	}
	
	public override void SilentHide()
	{
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void Hide()
	{
		transform.GetComponent<AnchorTweener>().Backward();
		base.Hide();
	}
	
	public override void OnBackPressed()
	{
		//CrossPressed();
		//base.OnBackPressed();
		
		buttonClicked = ButtonState.Cross;
		ResetSocialState();
	}
	
	public void CrossPressed()
	{		
		buttonClicked = ButtonState.Cross;
		ResetSocialState();
	}
	
	public void TickPressed()
	{
		Log.Debug("DetectExistingUserPopup: Tick clicked");
		buttonClicked = ButtonState.Tick;
		
		isValidToRestore = IsLocalDLCValid();
		if (isValidToRestore)
		{
			//Update the quest status
			QuestFlags.SetFlag(5,true);

			PopupManager.HideAllPopup();
			PopupManager.ShowRestoreProgressPopup(type, userInfo, false, false);
		}
		else
		{
			ResetSocialState();
		}
		//Hide the popup
		//base.Hide();
		//transform.GetComponent<AnchorTweener>().Backward();
	}
	
	private void ResetSocialState()
	{
		if (type.Equals(SocialNetwork.Type.Facebook))
		{
			if (FacebookManager.SharedManager.IsLoggedIn())
			{
				FacebookManager.SharedManager.SetCallback(this);
				FacebookManager.SharedManager.Logout();
			}
		}
		else if (type.Equals(SocialNetwork.Type.GooglePlus))
		{
			if (GooglePlusManager.SharedManager.IsSignedIn())
			{
				GooglePlusManager.SharedManager.SetCallback(this);
				GooglePlusManager.SharedManager.SignOut();
			}
		}
		else if (type.Equals(SocialNetwork.Type.Twitter))
		{
			if (TwitterManager.SharedManager.IsLoggedIn())
			{
				TwitterManager.SharedManager.SetCallback(this);
				TwitterManager.SharedManager.Logout();
			}
		}
	}
	
	#region FacebookManager Callback
	public void OnFacebookManagerLogout()
	{
		
		if (buttonClicked.Equals(ButtonState.Cross))
		{
			Hide();
		}
		else if (buttonClicked.Equals(ButtonState.Tick))
		{
			//The DLC version is not valid
			PopupManager.HideAllPopup();
			if (!isValidToRestore)
			{
				//DownloadManager dlm = DownloadManager.SharedManager;
				//if (dlm.isDownloading)
					//DownloadManager.SharedManager.AskForDLCProgress();
				//else
					DownloadManager.SharedManager.AskForDLCUpgrade(false, gsDLCVersion, 0);
			}
		}
		
		buttonClicked = ButtonState.None;
	}
	
	public void OnFacebookManagerError(Exception e)
	{
		Hide();
		
		string errorMsg = Language.Get("login_fail_settings").Replace("%1", "Facebook");
		PopupManager.ShowSocialStatePopup(SocialNetwork.Type.Facebook, errorMsg, true, false);
	}
	
	public void OnFacebookManagerGetFriends(ArrayList friendList){}
	public void OnFacebookManagerLogin(string userId){}
	public void OnFacebookManagerLoginCancel(){}
	public void OnFacebookManagerUploadPhoto(){}
	public void OnFacebookManagerNewUserError(Hashtable userInfo){}
	public void OnFacebookManagerExistingUserError(Hashtable existingUserInfo){}
	#endregion
	
	#region GooglePlusManager Callback
	public void OnGooglePlusManagerSignOut(Hashtable errorObj)
	{		
		if (buttonClicked.Equals(ButtonState.Cross))
		{
			Hide();	
		}
		else if (buttonClicked.Equals(ButtonState.Tick))
		{

			//The DLC Version is not valid
			PopupManager.HideAllPopup();
			if (!isValidToRestore)
				DownloadManager.SharedManager.AskForDLCUpgrade(false, gsDLCVersion, 0);
		}
		
		buttonClicked = ButtonState.None;
	}
	
	public void OnGooglePlusManagerError(Exception e)
	{		
		string errorMsg = Language.Get("login_fail_settings").Replace("%1", "Google+");
		PopupManager.ShowSocialStatePopup(SocialNetwork.Type.GooglePlus, errorMsg, true, false);
	}
	
	public void OnGooglePlusManagerSignIn(string userId){}
	public void OnGooglePlusManagerSignInCancelled(){}
	public void OnGooglePlusManagerSignInFailed(Hashtable errorObj){}	
	public void OnGooglePlusManagerGetFriends(ArrayList friendList){}
	public void OnGooglePlusManagerSharePhoto(){}
	public void OnGooglePlusManagerSharePhotoFailed(){}
	public void OnGooglePlusManagerNewUserError(Hashtable socialInfo){}
	public void OnGooglePlusManagerExistingUserError(Hashtable existingUserInfo){}
	#endregion
	
	#region TwitterManager Callback
	public void OnTwitterManagerLogout()
	{
		Log.Debug("DetectEcistingUserPopip: TwitterManager Logout callback");
		Log.Debug("BUtton clicked: " + buttonClicked);
		if (buttonClicked.Equals(ButtonState.Cross))
		{	
			Hide();
		}
		else if (buttonClicked.Equals(ButtonState.Tick))
		{
			//The DLC Version is not valid
			PopupManager.HideAllPopup();
			if (!isValidToRestore)
				DownloadManager.SharedManager.AskForDLCUpgrade(false, gsDLCVersion, 0);
		}
		
		buttonClicked = ButtonState.None;
	}
	
	public void OnTwitterManagerError(Exception e)
	{		
		string errorMsg = Language.Get("login_fail_settings").Replace("%1", "Twitter");
		PopupManager.ShowSocialStatePopup(SocialNetwork.Type.Twitter, errorMsg, true, false);
	}
	
	public void OnTwitterManagerGetUserProfile(ArrayList userInfos){}
	public void OnTwitterManagerGetFriendIds(ArrayList friendIds){}
	public void OnTwitterManagerLoginFailed(){}
	public void OnTwitterManagerLogin(string userId){}
	public void OnTwitterManagerUploadPhoto(){}
	public void OnTwitterManagerUploadPhotoError(string error){}
	public void OnTwitterManagerNewUserError(Hashtable socialInfo){}
	public void OnTwitterManagerExistingUserError(Hashtable existingUserInfo){}
	#endregion	
}

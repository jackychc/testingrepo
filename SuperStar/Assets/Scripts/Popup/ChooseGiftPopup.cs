using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChooseGiftPopup : Popup
{
	int type = 1, position = -1;
	public GameObject page1, page2;
	public Reward reward;
	public List<Reward> rewards;
	public RewardRowsPositioner rrp;
	
	public RewardRowsPositioner[] rubbishRRP = new RewardRowsPositioner[2];
	
	int[] sequence = new int[3];
	
	public GameObject flower;
	

	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
	}
	public void setType(int pType)
	{
		type = pType;
	}
	
	public void setReward(Reward r)
	{
		reward = r;
		rrp.reward = r;
	}
	public void setRewards(List<Reward> r)
	{
		rewards = r;
	}
	
	public override void SilentHide()
	{
		base.SilentHide();
		transform.GetComponent<AnchorTweener>().Backward();
		//reward.claim();
	}
	
	public override void Hide()
	{
		if (!page2.activeSelf) return;
		
		base.Hide();
		transform.GetComponent<AnchorTweener>().Backward();
		//reward.claim();
	}
	
	public override void OnBackPressed()
	{		
		if (!page2.activeSelf) return;

		base.OnBackPressed();
	}
	
	public void PickGift()
	{
		if (page2.activeSelf)
		{
			Hide();
			return;
		}
		
		page1.SetActive(false);
		page2.SetActive(true);
		
		if (type == 1)
		{
			rrp.Position();
			rrp.GetComponent<ScaleTweener>().Forward();
		}
		
		if (type == 2)
		{
			SoundManager.SharedManager.PlaySFX("changeClothes_opengift");
			StartCoroutine (Type2PopRoutine());
		}
	}
	
	public IEnumerator Type2PopRoutine()
	{
		int tRubbishCount = 0;
		
		Vector3 temp = flower.transform.localPosition;
		temp.x = temp.x + 200f*sequence[0];
		flower.transform.localPosition = temp;

		for (int i = 0; i < 3; i++)
		{
			if (sequence[i] == position)
			{
				reward = rewards[i];
				reward.claim();
			}
		}
		
		for (int i = 0; i < 3; i++)
		{
			RewardRowsPositioner targetRRP;
		
			if (sequence[i] == position)
			{
				targetRRP = rrp;
				reward = rewards[i];
				rrp.reward = rewards[i];


			}
			else
			{
				targetRRP = rubbishRRP[tRubbishCount];
				tRubbishCount++;
				
			}
			
			targetRRP.reward = rewards[i];

			Vector3 cachedPos = targetRRP.transform.localPosition;
			cachedPos.x = cachedPos.x + 200f*sequence[i] + (sequence[i] == position? -30f: -30f);
			targetRRP.transform.localPosition = cachedPos;
			Transform lid = transform.FindChild("Cover "+(sequence[i]+2));
			lid.GetComponent<PositionTweener>().Forward();
			lid.GetComponent<RotationTweener>().Forward();
			
			yield return new WaitForSeconds(0.3f);
			
			targetRRP.Position();
			
			targetRRP.GetComponent<ScaleTweener>().Forward();
		}
	}
	
	public void PickGift1()
	{
	 	position = -1;
		sequence = new int [3]{-1,0,1};
		PickGift();
	}
	public void PickGift2()
	{
		position = 0;
		sequence = new int [3]{0,-1,1};
		PickGift();
	}
	public void PickGift3()
	{
		position = 1;
		sequence = new int [3]{1,-1,0};
		PickGift();
	}
}

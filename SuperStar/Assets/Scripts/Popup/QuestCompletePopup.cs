using UnityEngine;
using System.Collections;

public class QuestCompletePopup : Popup
{
	public Quest quest;
	public UISysFontLabel questContent;
	public RewardRowsPositioner rrp;
	
	public void setQuest(Quest q)
	{
		quest = q;
	}
	
	public override void Show()
	{
		base.Show();
		
		questContent.Text = quest.action.GetQuestDescription();
		
		rrp.reward = quest.action.GetQuestReward();
		rrp.Position();
		
		
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		SoundManager.SharedManager.PlaySFX("job_click_award");
		
	}
	
	public override void Hide()
	{		
		quest.action.GetQuestReward().claim();		
		quest.action.OnFinishRequirementMeet();
		
		transform.GetComponent<AnchorTweener>().Backward();	
		base.Hide();
		
		PopupManager.ShowQuestListPopup(false);
	}
	
	public override void SilentHide()
	{		
		quest.action.GetQuestReward().claim();		
		quest.action.OnFinishRequirementMeet();	
		
		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
}

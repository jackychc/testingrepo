using UnityEngine;
using System.Collections;

public class SystemErrorPopup : Popup {
	
	private string desc = "";
	
	public void SetText(string text)
	{
		desc = text;
	}
	
	public override void Show()
	{
		transform.FindChild("Text").gameObject.GetComponent<UISysFontLabel>().Text = desc;
		transform.GetComponent<AnchorTweener>().Forward();
		base.Show();
	}
	
	public override void Hide()
	{
		//transform.GetComponent<AnchorTweener>().Backward();
		//base.Hide();
		Time.timeScale = 0f;
		Application.Quit();
	}
	
	public override void SilentHide()
	{
		Time.timeScale = 0f;
		Application.Quit();
	}
	
	public override void OnBackPressed()
	{
		//Disable the back button
		//base.OnBackPressed();
		base.OnBackPressed();
	}
	
	public void OnButtonOKClicked()
	{
		Time.timeScale = 0f;
		Application.Quit();	
	}
}

using UnityEngine;
using System.Collections;
using System;

public class BoyfriendOptionsPopup : Popup
{
	public Boyfriend boyfriend;
	
	public delegate void OnBoyfriendOptionClicked();
	public OnBoyfriendOptionClicked boyfriendOptionClicked;
	
	public UISysFontLabel questionLabel;
	public GameObject dateButton, inviteButton;
	public UITexture icon;
	public Texture2D iconTexture;
	
	//public UISysFontLabel instantLabel;
	//public UISysFontLabel cancelLabel;
	
	public void setBoyfriend(Boyfriend b)
	{
		boyfriend = b;
		
		if (boyfriend.flirtExp >= boyfriend.maxFlirtExp)
		{
			// invite
			questionLabel.Text = Language.Get("bf_invitemsg").Replace("%1", boyfriend.boyfriendName);
			dateButton.SetActive(false);
			inviteButton.SetActive(true);
		}
		else
		{
			//date
			questionLabel.Text = Language.Get("bf_datemsg").Replace("%1", boyfriend.boyfriendName);
			inviteButton.SetActive(false);
			dateButton.SetActive(true);
		}
		
	}
	
	public void SetTexture(Texture2D t)
	{
		icon.mainTexture= t;
	}
	
	public void ButtonClicked()
	{
		Log.Debug("clicked tick");
		
		
		
		Hide ();
		
		if (boyfriendOptionClicked != null)
			boyfriendOptionClicked();
		
	}
	
	public override void Show()
	{
		transform.GetComponent<AnchorTweener>().Forward();
		base.Show();
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	
	
	public override void Hide()
	{
		Log.Debug("hide");
		transform.GetComponent<AnchorTweener>().Backward();
		
		base.Hide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}

}

using UnityEngine;
using System.Collections;

public class GameIDGetPopup : Popup
{
	string content = "";
	public GameObject gameIdLabel;
	
	public void setText(string text)
	{
		content = text;
		//gameIdLabel.GetComponent<UISysFontLabel>().Text = "";
	}
	
	public override void Show()
	{
		base.Show();
		//gameIdLabel.GetComponent<UISysFontLabel>().Text = content;
		gameIdLabel.GetComponent<UILabel>().text = content;
		transform.GetComponent<AnchorTweener>().Forward();
		
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
		
	}
	
	
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
}

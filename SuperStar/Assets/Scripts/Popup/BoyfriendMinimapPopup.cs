using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoyfriendMinimapPopup :MonoBehaviour
{
	
	public UISysFontLabel labelDesc;
	public GameObject[] positions 		= new GameObject[4];
	public GameObject[] subPositions 	= new GameObject[12];
	public GameObject[] touchPositions 	= new GameObject[4];
	public ScaleTweener[] scalers 		= new ScaleTweener[4];
	public bool[] isSexyPlace 			= new bool[4]{false, false, true, true};
	public List<BoyfriendMinimapItem> allBFItems = new List<BoyfriendMinimapItem>();
	public UITexture mapBase;
	
	int currentPos = 0;
	
	void Start()
	{
		if (allBFItems.Count == 0)
		{
			GenerateBFMapItems();
			
		}
		labelDesc.Text = Language.Get("bf_minimap_desc");
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.BOYFRIENDMAP);		
	}
	
	/*
	public override void Show()
	{
		if (allBFItems.Count == 0)
		{
			GenerateBFMapItems();
			Vector4 clipRange = dragPanel.transform.GetComponent<UIPanel>().clipRange;
			float menuHeight = 50f;
			clipRange.y = clipRange.y - menuHeight;
			if (1f*Screen.height/Screen.width >= 1.5f)
			{
				
				clipRange.z = 640f +3;
				clipRange.w = 640f * Screen.height/Screen.width - menuHeight/2;
			}
			else
			{
				clipRange.z = 960f * Screen.width/Screen.height +3;
				clipRange.w = 960f - menuHeight/2;
			}
			
			dragPanel.transform.GetComponent<UIPanel>().clipRange = clipRange;
		}
		labelDesc.Text = Language.Get("bf_minimap_desc");
		
		base.Show();
		
		// need some nasty calculation
		
		
		// set scroll position
		
		/*SpringPanel.Begin(dragPanel, new Vector3(Globals.mainPlayer.MinimapPosition.x,
			Globals.mainPlayer.MinimapPosition.y,
			dragPanel.transform.localPosition.z), 10000f);
		
		//if (StreetScene.minimapBackground == null)
		//	StreetScene.minimapBackground = Resources.Load("mini map") as Texture2D;
		
		//mapBase.mainTexture = StreetScene.minimapBackground;
		
		transform.GetComponent<AnchorTweener>().Forward();
		//InvokeRepeating("Move", 0f, 0.02f);
		
	}*/
	
	/*public override void SilentHide()
	{
		base.SilentHide();
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
		
	}*/
	
	/*public override void OnBackPressed()
	{
		base.OnBackPressed();
	}*/
	
	public void GoBack()
	{
		SceneManager.SharedManager.LoadScene(Scenes.STREET, true);
	}
	
	public void GenerateBFMapItems()
	{
		int livelyCount = 0, sexyCount = 0;	
		Vector2 charismaVector = Items.sumCharismaForList(Globals.mainPlayer.wearingItems);
		List<Boyfriend> livelyexclude = new List<Boyfriend>();
		List<Boyfriend> sexyexclude = new List<Boyfriend>();
		
		for (int i = 0; i < 4; i++)
		{
			// random all
			
			for (int j = 0; j < 3; j++)
			{
				Boyfriend b = Boyfriends.GetBoyfriendByCriteria(i+1, 100, (i+1<=2? livelyexclude:sexyexclude));
				
				if (b != null)
				{
					if (i+1 <=2)
					{
						livelyexclude.Add(b);
						livelyCount++;
					}
					else
					{
						sexyexclude.Add(b);
						sexyCount++;
					}
					
				}
			}
			
		}
		
		Log.Warning("livelycount:"+livelyexclude.Count+",sexycount:"+sexyexclude.Count);
		
		for (int i = 0; i < 4; i++)
		{
			// random all
			
			for (int j = 0; j < (Mathf.Min(3, i <= 1? livelyCount/2 : sexyCount/2)); j++)
			{
				Boyfriend b = i+1<=2? livelyexclude[0] : sexyexclude[0];//exclude[i*3+j];
				
				if (b != null)
				{
					GameObject bfItem = (GameObject) Instantiate(Resources.Load("BFMapItem") as GameObject);
				
					bfItem.transform.parent = transform;
					
					bfItem.transform.position = subPositions[i*3 + j].transform.position;
					bfItem.transform.localEulerAngles = new Vector3(0f, 0f,0f);
					bfItem.transform.localScale = new Vector3(0.7f,0.7f,0.7f);
					
					bfItem.GetComponent<BoyfriendMinimapItem>().boyfriend = b;
					bfItem.GetComponent<BoyfriendMinimapItem>().location = i+1;
					Vector3 t = bfItem.GetComponent<BoyfriendMinimapItem>().bfIcon.transform.localPosition;
					t.x = (int) t.x;
					t.y = (int) t.y;
					bfItem.GetComponent<BoyfriendMinimapItem>().bfIcon.transform.localPosition = t;
					
					allBFItems.Add(bfItem.GetComponent<BoyfriendMinimapItem>());
					bfItem.GetComponent<BoyfriendMinimapItem>().map = this;
					bfItem.GetComponent<ScaleTweener>().Forward();
					
					if (i+1<=2)
						livelyexclude.RemoveAt(0);
					else
						sexyexclude.RemoveAt(0);
				}
			}
			
		}
		
		
		
		
		HideAllMenus();
	}
	
	
	
	
	public void HideAllMenus()
	{
		foreach (BoyfriendMinimapItem bfItem in allBFItems)
		{
			bfItem.HideMenu();
		}
	}
	
	public void PositionClicked(int pos)
	{
		currentPos = pos;
		StartCoroutine(PositionClickedRoutine(pos));
	}
	
	IEnumerator PositionClickedRoutine(int pos)
	{
		Log.Debug("position "+pos+"clicked");
		List<BoyfriendMinimapItem> tweenTargets = new List<BoyfriendMinimapItem>();
		foreach (BoyfriendMinimapItem bfItem in allBFItems)
		{
			if (bfItem.location == pos)
			{
				tweenTargets.Add(bfItem);
				//bfItem.GetComponent<ScaleTweener>().Backward();
			}
			else
			{
				//bfItem.GetComponent<ScaleTweener>().Forward();
				bfItem.gameObject.SetActive(false);
				bfItem.GetComponent<ScaleTweener>().setProportion(1f);
				
			}
		}
		
		for (int i = 0; i < 4; i++)
		{
			//touchPositions[i].SetActive(!((i+1) == pos));
			
			if ((i+1) == pos)
			{
				touchPositions[i].SetActive(false);
				scalers[i].mode = ScaleTweener.TWEENMODE.BACK_AND_FORTH;
				scalers[i].Forward();
			}
			else
			{
				touchPositions[i].SetActive(true);
				scalers[i].mode = ScaleTweener.TWEENMODE.PASSIVE;
				scalers[i].Backward();
			}
		}
		
		foreach (BoyfriendMinimapItem i in tweenTargets)
		{
			if (currentPos == pos)
			{			
				i.gameObject.SetActive(true);
				i.GetComponent<ScaleTweener>().Backward();
				yield return new WaitForSeconds(0.18f);
			}
			//else
			//{
				
				//i.GetComponent<ScaleTweener>().Forward();
			//}
		}
		
		
	}	
	
	public void Position1Clicked()
	{
		PositionClicked(1);
	}
	public void Position2Clicked()
	{
		PositionClicked(2);
	}
	public void Position3Clicked()
	{
		PositionClicked(3);
	}
	public void Position4Clicked()
	{
		PositionClicked(4);
	}
	
	public void SaveMinimapPosition()
	{
//		Globals.mainPlayer.MinimapPosition = new Vector2(dragPanel.transform.localPosition.x, dragPanel.transform.localPosition.y);
	}
	



}

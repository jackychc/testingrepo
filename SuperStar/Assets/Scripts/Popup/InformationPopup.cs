using UnityEngine;
using System.Collections;

public class InformationPopup : Popup
{
	string content = "";
	
	public void setText(string text)
	{
		content = text;
		transform.FindChild("Text").gameObject.GetComponent<UISysFontLabel>().Text = "";
	}
	
	public override void Show()
	{
		base.Show();
		transform.FindChild("Text").gameObject.GetComponent<UISysFontLabel>().Text = content;
		transform.GetComponent<AnchorTweener>().Forward();
		
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
		
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
}

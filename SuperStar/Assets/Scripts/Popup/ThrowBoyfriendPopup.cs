using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ThrowBoyfriendPopup : Popup
{
	
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		
	}
	
	public override void Hide()
	{	
		base.Hide();		
		transform.GetComponent<AnchorTweener>().Backward();
		
		PopupManager.ShowBoyfriendListPopup(false);
	}
	
	public override void SilentHide()
	{		
		transform.GetComponent<AnchorTweener>().Backward();
		base.SilentHide();		
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void TickPressed()
	{
		/*PopupManager.HideAllPopup();
		
		SceneManager.SharedManager.LoadScene(targetScene, true);
		
		*/
		
		Globals.mainPlayer.CurrentBoyfriendId = -1; // !?
		
		//GameObject.Destroy(GameObject.Find("BOYFRIEND"));
		
		GameObject homeInit = GameObject.Find("Init");
		if (homeInit != null)
		{
			HomeScene homeScene = homeInit.GetComponent<HomeScene>();
			if (homeScene != null)
			{
				homeScene.giftThinkingObj.SetActive(false);
				homeScene.RegenCharacters();
			}
		}
		
		PopupManager.HideLatestPopup();
	}
}

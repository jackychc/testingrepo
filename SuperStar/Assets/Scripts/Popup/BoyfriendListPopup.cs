using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BoyfriendListPopup : Popup
{
	public override void Show()
	{
		base.Show();
		
		transform.GetComponent<AnchorTweener>().Forward();
		
		
	//	StartCoroutine(GetFriends());
	}
	
	public override void SilentHide()
	{
		base.SilentHide();
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void Hide()
	{
		base.Hide();
		
		transform.GetComponent<AnchorTweener>().Backward();
	}
	
	public override void OnBackPressed()
	{
		base.OnBackPressed();
	}
	
	public void StartTutorial()
	{
		QuestFlags.SetFlag(-102, 1);
		PopupManager.ShowTutorialPopup(2, false);
		PopupManager.ShowBoyfriendListPopup(false);
		
		PopupManager.HideLatestPopup();
	}
	
	//public IEnumerator GetFriends()
	//{

	//}
}

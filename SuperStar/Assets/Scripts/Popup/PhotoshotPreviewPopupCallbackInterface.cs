using UnityEngine;
using System.Collections;

public interface PhotoshotPreviewPopupCallbackInterface {
	void OnPhotoshotPreviewPopupSave();
	void OnPhotoshotPreviewPopupDiscard();
	void OnPhotoshotPreviewPopupShare();
}

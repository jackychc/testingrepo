using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MessageDownloader : MonoBehaviour
{
	public GameObject holder;
	int updateInterval = 30; //seconds
	
	// Use this for initialization
	void Start ()
	{
		UpdateEnvelope();
		
		StartCoroutine(Get ());
	}

	IEnumerator Get()
	{
		
		StartCoroutine(ManualGetRoutine());
		
		yield return new WaitForSeconds (updateInterval * 1.0f);
		
		StartCoroutine(Get ());
	}
	
	public void ManualGet()
	{
		StartCoroutine(ManualGetRoutine());
	}
	
	public IEnumerator ManualGetRoutine()
	{
		Hashtable response = null;
		yield return StartCoroutine(GetMessagesOnServer(result => response = result));
		
		if (response != null)
		{
			Globals.messages = Messages.GetMessagesFromHashtable(response);
		
			UpdateEnvelope();
		}
	}
	
	void UpdateEnvelope()
	{
		if (Messages.GetNoOfMessages() > 0)
		{
			if (holder != null)
			{
				// should have a envelop in the scene
				GameObject envelope = GameObject.Find("Envelope(Clone)");
			
				if (envelope == null)	
				{
					envelope = (GameObject) Instantiate(Resources.Load("Envelope") as GameObject);
					envelope.transform.parent = holder.transform;
					envelope.transform.localPosition = Vector3.zero;
					envelope.transform.localRotation = Quaternion.identity;
					envelope.transform.localScale = Vector3.one;
				}
				
				envelope.GetComponent<Envelope>().updateNumber(Messages.GetNoOfMessages());
				
			}
		}
		else
		{
			// no message, destroy the envelope
			GameObject envelope = GameObject.Find("Envelope(Clone)");
			
			if (envelope != null)
				Destroy(envelope);
		}
	}
	
	IEnumerator GetMessagesOnServer(Action<Hashtable> hash)
	{
		//Prepare CheckSum
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("GetVoteRequest", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("checkSum", checkSum);
	
		//yield return StartCoroutine(HTTPPost.Post("GetVoteRequest", kvp, result => response = result));
		
		string url = Globals.serverAPIURL + "GetVoteRequest.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					
					hash(((Hashtable)al[1]));
		
				}
				else		
				{
					hash(null);
				}				
			}
		}	
	}
	
	
}


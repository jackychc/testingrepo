using UnityEngine;
using System.Collections;

public class DebugButtonGroupController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.gameObject.SetActive(Globals.isDebugMode);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OnDebugGemButtonClicked()
	{
		Globals.mainPlayer.GainGems(100000);
	}
	
	public void OnDebugMoneyButtonClicked()
	{
		Globals.mainPlayer.GainCoins(100000);
	}
	
	public void OnDebugEXPButtonClicked()
	{
		Globals.mainPlayer.GainExp(100000);	
	}
}

using UnityEngine;
using System.Collections;

public class AnchorTweener : MonoBehaviour {
	
	public enum TWEENMODE
	{
		PASSIVE,
		LOOP,
		BACK_AND_FORTH
	}
	
	float proportion = 0f;
	float sineProportion = 0f;
	bool commanded;
	bool isForward = true;
	
	public float speed = 1f;
	public float fromXValue;
	public float toXValue;
	public float fromYValue;
	public float toYValue;
	float forwardBounce = 0.02f;
	bool passed;
	
	public TWEENMODE mode = TWEENMODE.PASSIVE;
	
	Vector2 fromVector = Vector2.zero, toVector = Vector2.zero;
	
	UIAnchor theAnchor;
	
	// Use this for initialization
	void Start () {
		
		theAnchor = transform.GetComponent<UIAnchor>();
		
		if (fromXValue != toXValue)
			proportion = Mathf.InverseLerp(fromXValue, toXValue, theAnchor.relativeOffset.x);
		else
			proportion = Mathf.InverseLerp(fromYValue, toYValue, theAnchor.relativeOffset.y);
		
		fromVector = new Vector2(fromXValue, fromYValue);
		toVector = new Vector2(toXValue, toYValue);
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (mode.Equals(TWEENMODE.PASSIVE) && !commanded)
			return;
		
		
		//proportion = Mathf.Max(0.0f, Mathf.Min(proportion, 1.02f));
		
		
		Move();
	}
	
	
	public float getProportion()
	{
		return proportion;
	}
	
	public void setProportion(float v)
	{
		proportion = v;
	}
	
	public void Forward()
	{	
		//if (Mathf.Approximately(0.0f, proportion)) return;
		
		passed = false;
		commanded = true;
		isForward = true;
	}
	
	public void Backward()	
	{
		//if (Mathf.Approximately(1.0f, proportion)) return;
		
		passed = false;
		commanded = true;
		isForward = false;
	}
	
	void Move()
	{
		
		proportion = proportion + (isForward? 1 : -1) *  (passed? -1: 1) * speed * (1f/30);
		proportion = Mathf.Max(0.0f, Mathf.Min(proportion, (1f + forwardBounce)));
		
		
		
		sineProportion = 1 - (proportion-1)*(proportion-1);
		
		theAnchor.relativeOffset = Vector2.Lerp(fromVector, toVector, sineProportion);
		//transform.GetComponent<UIAnchor>().relativeOffset.x = fromXValue +  (toXValue - fromXValue) * sineProportion;
		//transform.GetComponent<UIAnchor>().relativeOffset.y = fromYValue +  (toYValue - fromYValue) * sineProportion;
		
		
		if (mode.Equals(TWEENMODE.LOOP))
		{
			if (proportion >= 1.0f)
				proportion = 0f;
			
		}
		
		if (mode.Equals(TWEENMODE.BACK_AND_FORTH))
		{
			if (proportion <= 0f || proportion >= 1.0f)
				isForward = !isForward;
		}
		
		else if (mode.Equals(TWEENMODE.PASSIVE))
		{
			if (proportion <= 0f || proportion >= 1.0f)
				commanded = false;
		}
	}
}

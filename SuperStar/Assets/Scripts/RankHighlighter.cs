using UnityEngine;
using System.Collections;

public class RankHighlighter : MonoBehaviour {
	
	
	// put this on a scrollpanel
	Vector3 normalScale, biggerScale;
	
	
	
	// Use this for initialization
	void Start ()
	{
		normalScale = new Vector3(30f,30f,1f);
		biggerScale = new Vector3(50f,50f,1f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		foreach (RankItem t in transform.GetComponentsInChildren<RankItem>())
		{
			

			
			if (t.gameObject == transform.GetComponent<UICenterOnChild>().centeredObject)
			{
				t.rankLabel.transform.localScale = biggerScale;
				t.rankLabel.effectStyle = UILabel.Effect.Outline;
				t.rankLabel.color = Color.white;
			}
			else
			{
				t.rankLabel.transform.localScale = normalScale;
				t.rankLabel.effectStyle = UILabel.Effect.None;
				t.rankLabel.color = Color.black;
			}
			
		
		}

	}
}

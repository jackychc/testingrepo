using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

public class ClosetItem : MonoBehaviour {

	public Item item;
	public GameObject tick;
	public ScaleTweener scaleTweener;
	public int sortOrder;
	
	public GameObject icon;
	
	public GameObject sexyIcon, livelyIcon;
	public UILabel indexLabel;
	
	GameObject  scrollPanelT;
	ClosetScrollListManager cslm;
	GameObject girl;
	Texture2D tex;
	
	public void Start()
	{
		StartCoroutine(ReallyStart());
	}
	
	public IEnumerator ReallyStart()
	{
		UITexture iconTexture = icon.GetComponent<UITexture>();
		tex = Items.GetIconOfItem(item);
		if(tex == null)
		{
			yield return StartCoroutine(LoadIconFromPersistent(item, result => tex = result));
		}
		
		string shaderName 	= "UnlitTransparentColored";
		Shader shader 		= Resources.Load("Shaders/" + shaderName) as Shader;
		if (shader != null)
			iconTexture.material = new Material(shader);
		else
			iconTexture.material = new Material(Shader.Find ("Unlit/Transparent Colored"));
		
        iconTexture.material.mainTexture = tex;
        icon.transform.localScale = new Vector3(128f,128f,1f);
		
		SetIndex();
		
		Destroy(transform.GetComponent<UIPanel>());
		
		
	}
	
	public void AssignFindings(ClosetScrollListManager pCslm, GameObject pScrollPanelT, GameObject pGirl)
	{
		cslm = pCslm;
		scrollPanelT = pScrollPanelT;
		girl = pGirl;
	}
	
	
	public IEnumerator LoadIconFromPersistent(Item i, Action<Texture2D> returnMe)
	{
		WWW loader = new WWW("file://" + Application.persistentDataPath + Path.DirectorySeparatorChar + i.itemId + "_icon.png");		
		yield return loader;
		
		if (loader.error != null)
		{
			Log.Debug("Closet Item Cannot load item icon with itemId: " + i.itemId);
		}
		else
		{
			if (loader.texture != null)
			{
				returnMe((Texture2D)loader.texture);
			}
			else
			{
				Log.Debug("Closet Item Cannot load texture with itemId: " + i.itemId);	
			}
		}
	}
	
	public IEnumerator ClickItem()
	{
		//Log.Debug("clicku!");
		
		
		

		// how to wear? ....
		if (!Globals.mainPlayer.isWearingItem(item))
		{
			Globals.mainPlayer.wearItem(item);
			
			SpringPanel.Begin(scrollPanelT, new Vector3(0f,-transform.localPosition.y ,0f), 5f);
			
			//ClosetScrollListManager cslm = scrollPanelGroup.GetComponent<ClosetScrollListManager>();
				
			if (cslm != null)
			{
				cslm.refreshTicks();
				cslm.showItemName(item);
			}
		}
		else
		{
			Item noneItem = Items.getNoneItemOfSubcategory(item.subCategoryId);
			
			if (noneItem != null)
			{
				Globals.mainPlayer.wearItem(noneItem);
				
				// jump the scroll list to the none item
				// so lazy method...
				//ClosetScrollListManager cslm = scrollPanelGroup.GetComponent<ClosetScrollListManager>();
				
				if (cslm != null)
				{
					cslm.refreshTicks();
					//cslm.focusToWearingItem();
					cslm.showItemName(noneItem);
				}
			}
		}
		
		// just set wearing item id, and call generate character!?
		//girl.GetComponent<CharacterManager>().PartialGenerateCharacter(Globals.mainPlayer, new List<ItemSubcategory>(){ItemSubcategories.getSubcategory(item.subCategoryId)});
		//girl.GetComponent<CharacterManager>().GenerateCharacter(Globals.mainPlayer);
		yield return StartCoroutine(girl.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
		
		string animationStr = girl.GetComponent<CharacterManager>().GetClosetAnimationString(ItemSubcategories.getSubcategory(item.subCategoryId));
		girl.GetComponent<CharacterManager>().InsertAnimation(animationStr, false); // test line
		girl.GetComponent<CharacterManager>().QueueAnimation(RandomIdleAnimationString(), true); // test line
		
		if(Application.loadedLevel == (int) Scenes.CLOSET)
		{
			Globals.hasChanagedClothesInCloset = true;

			QuestFlags.SetFlag(1,true);
			//QuestFlags.var1_changedCloth = true;
		}
		
		
	}

	private string RandomIdleAnimationString()
	{
		int ran = UnityEngine.Random.Range(1,2+1);

		switch(ran)
		{
			case 1: return "pose01";
			case 2: return "pose02";
		}

		return "pose01";
	}
	
	public void SetIndex()
	{
		if (item.sexy > 0)
		{
			sexyIcon.SetActive(true);
			livelyIcon.SetActive(false);
			indexLabel.text = "" + item.sexy;
		}
		else if (item.lively > 0)
		{
			sexyIcon.SetActive(false);
			livelyIcon.SetActive(true);
			indexLabel.text = "" + item.lively;
		}
		else
		{
			sexyIcon.SetActive(false);
			livelyIcon.SetActive(false);
			indexLabel.text = "";
		}
	}
	
	void OnDestroy()
	{
		tex = null;
	}
	
	
}

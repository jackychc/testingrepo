using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VotingLobbyScene: MonoBehaviour
{
	GameObject newCharacter;
	
	public UISprite[] buttons = new UISprite[4];
	public Color[] buttonTextOriginalColor = new Color[4];
	public UISysFontLabel[] buttonTexts = new UISysFontLabel[4];
	public RankScrollListManager rankScorllList;
	public UILabel votesText, rankText, gameIdText;
	public UISysFontLabel playerNameText;
	public GameObject voteButton, getVoteButton;
	public GameObject scrollListMother;
	
	void Start()
	{
		
		
		if (GameObject.Find("FEMALE") == null)
		{
			newCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			newCharacter.name = "FEMALE";
			newCharacter.AddComponent<CharacterManager>();
			//DontDestroyOnLoad(newCharacter);
		}
		
		else
		{
			newCharacter = GameObject.Find("FEMALE");
		}
		
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
		newCharacter.GetComponent<CharacterManager>().GenerateCharacter(Globals.mainPlayer);
		
		for (int i = 0 ; i < 4; i++)
		{
			buttonTextOriginalColor[i] = buttonTexts[i].color;
		}
		//buttonTexts[0].Text = Language.Get("vote_top");
		//buttonTexts[1].Text = Language.Get("vote_latest");
		//buttonTexts[2].Text = Language.Get("vote_friends");
		//buttonTexts[3].Text = Language.Get("vote_you");
		
		YouClicked();
		
		//SoundManager.SharedManager.ChangeMusic("SuperstarStory_fashioncontest");
	}
	
	void Top20Clicked()
	{
		highlightButtons(0);
		//scrollListMother.GetComponent<AnchorTweener>().Forward();
		StartCoroutine(NonYouClickedRoutine("Top100VotingsRank", null));
	}

	void RandomClicked()
	{
		highlightButtons(1);
		//scrollListMother.GetComponent<AnchorTweener>().Forward();
		StartCoroutine(NonYouClickedRoutine("LatestVotingsRank", null));
	}

	void FriendsClicked()
	{
		highlightButtons(2);
		
		StartCoroutine(NonYouClickedRoutine("FriendsVotingsRank", Globals.mainPlayer.GameId));
	}
	
	IEnumerator NonYouClickedRoutine(string apiName, string gameId)
	{
		getVoteButton.SetActive(false);
		voteButton.SetActive(false);
		PopupManager.ShowLoadingFlower();
		
		scrollListMother.GetComponent<AnchorTweener>().Backward();
		
		ClearBoardData();
		
		string topicId 		= Globals.viewingTopic.topicId.ToString();
		string topicOffset 	= "0";
		string topicRange 	= "100";		
		string checkSumData = "";
		if (gameId != null)
		{
			GameID gameID 	= new GameID(Globals.mainPlayer.GameId);
			int playerId 	= gameID.toInt();
			checkSumData 	= gameID.ID + playerId + topicId + topicOffset + topicRange;
		}
		else
		{
			checkSumData = topicId + topicOffset + topicRange;
		}
		string checkSum = MD5Hash.GetServerCheckSumWithString(apiName, checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("topicId", topicId);
		kvp.Add("topicOffset", topicOffset);
		kvp.Add("topicRange", topicRange);
		if (gameId != null)
			kvp.Add("gameId", gameId);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + apiName + ".php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					// have rank already
					if (al[1] != null)
					{
						ArrayList playerList =  new ArrayList(((Hashtable) al[1]).Values);
						
						List<Player> setMe = new List<Player>();
						//Log.Debug("count:"+playerList.Count);
						for (int i = 0; i < playerList.Count; i++)
						{
							Player p = new Player();
							p.playerId = -2;
							p.GameId = ((Hashtable)playerList[i])["gameId"].ToString();
							p.Name = ((Hashtable)playerList[i])["playerName"] == null? "":((Hashtable)playerList[i])["playerName"].ToString();
							p.votingRank = ((Hashtable)playerList[i])["rank"] != null? Int32.Parse(((Hashtable)playerList[i])["rank"].ToString()) : 0;
							p.votingVotes = ((Hashtable)playerList[i])["votes"] != null ? Int32.Parse(((Hashtable)playerList[i])["votes"].ToString()) : 0;
							p.wearingItems = ((Hashtable)playerList[i])["voteItems"] != null? Items.stringToItemList(((Hashtable)playerList[i])["voteItems"].ToString()) : Items.stringToItemList(UnityEngine.Random.Range(32,51) +",52,53");//((Hashtable)playerList[i])["voteItems"].ToString();
							
							setMe.Add(p);
							
						}
						
						if (setMe.Count > 0)
						{
							PopupManager.HideLoadingFlower();
							setMe.Sort((x,y) => {return x.votingRank.CompareTo(y.votingRank);});
							rankScorllList.RefreshWithPlayers(setMe);
						}
						else
						{
							PopupManager.HideLoadingFlower();
							PopupManager.ShowInformationPopup(Language.Get("vote_noinfoavailable"), false);
						}
					}
					
					else
					{
						PopupManager.HideLoadingFlower();
						PopupManager.ShowInformationPopup(Language.Get("vote_noinfoavailable"), false);
					}
				}
				else
				{
					PopupManager.HideLoadingFlower();
					PopupManager.ShowInformationPopup(Language.Get("vote_noinfoavailable"), false);
				}
			}
		}		
		
		//yield return StartCoroutine(HTTPPost.Post(apiName, kvp, result => response = result));
	}
	
	void YouClicked()
	{
		highlightButtons(3);
		
		scrollListMother.GetComponent<AnchorTweener>().Backward();
		
		StartCoroutine(YouClickedRoutine());
	}
	
	IEnumerator YouClickedRoutine()
	{
		getVoteButton.SetActive(false);
		voteButton.SetActive(false);
		PopupManager.ShowLoadingFlower();
		
		ClearBoardData();
		setPlayerName(Globals.mainPlayer.Name);		
		
		string topicId 		= Globals.viewingTopic.topicId.ToString();
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + topicId;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("PlayerVotingsRank", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("topicId", topicId);
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "PlayerVotingsRank.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					// have rank already
					if (al[1] != null)
					{
						
						Player p = new Player();
						p.playerId = -2;
						setRankText(((Hashtable)al[1])["rank"] != null ? ((Hashtable)al[1])["rank"].ToString() : "--");
						setVotesText(((Hashtable)al[1])["votes"] != null ?((Hashtable)al[1])["votes"].ToString() : "--");
						p.GameId = Globals.mainPlayer.GameId;
						p.Name = Globals.mainPlayer.Name;
						
						p.wearingItems = Items.stringToItemList(((Hashtable)al[1])["voteItems"].ToString());
						
						GameObject.Find("FEMALE").GetComponent<CharacterManager>().GenerateCharacter(p);
						
						MattersOfGameId(Globals.mainPlayer.GameId);
						
						PopupManager.HideLoadingFlower();
						//rankScorllList.RefreshWithRank(Int32.Parse(((Hashtable)al[1])["rank"].ToString()));
					}
					else
					{
						// don't have "you" actually
						// maybe generate a popup to tell join?
						
						//Top20Clicked();
						PopupManager.HideLoadingFlower();
						PopupManager.ShowInformationPopup(Language.Get("vote_noinfoavailable"), false);
					}
				}
				else
				{
					PopupManager.HideLoadingFlower();
					PopupManager.ShowInformationPopup(Language.Get("vote_noinfoavailable"), false);
				}
			}
		}
		
		
		//yield return StartCoroutine(HTTPPost.Post("PlayerVotingsRank", kvp, result => response = result));	
	}
	
	
	public void ClearBoardData()
	{
		//rankScorllList.ClearText();
		gameIdText.text = "--";
		votesText.text = "--";
		rankText.text = "--";
		playerNameText.Text = "--";
		
	}
	
	public void setVotesText(string text)
	{
		votesText.text = text;
	}
	public void setRankText(string text)
	{
		rankText.text = text;
	}
	public void setPlayerName(string text)
	{
		playerNameText.Text = "Justin Bigger";
		//		playerNameText.Text = text;
	}
	
	public void MattersOfGameId(string gameId)
	{
		gameIdText.text = gameId;
		if (gameId.Equals(Globals.mainPlayer.GameId))
		{
			//self
			getVoteButton.SetActive(true);
			voteButton.SetActive(false);
			
			
		}
		else
		{
			//not self
			// also set the button's target!?
			getVoteButton.SetActive(false);
			voteButton.SetActive(true);
			voteButton.GetComponent<VoteButton>().targetGameId = gameId;
		}
	}
	
	
	void highlightButtons(int index)
	{
		for (int i = 0; i < 4 ; i++)
		{
			buttons[i].spriteName = buttons[i].spriteName.TrimEnd('a').TrimEnd('b') + (index == i? "b":"a");
			
			buttonTexts[i].color = (index == i? Color.white : buttonTextOriginalColor[i]);
		}
	}
	
	public void ShowGetVotePopup()
	{
		PopupManager.ShowVotingRequestPopup(false);
	}
	
	public void GoToVotingScene()
	{
		//SceneManager.SharedManager.LoadScene(Scenes.VOTING, true);
	}
}


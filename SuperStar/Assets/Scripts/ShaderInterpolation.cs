using UnityEngine;
using System.Collections;

public class ShaderInterpolation : MonoBehaviour {
	
	public UIScrollBar scrollBar;
	public int[] fromRGB = {255,255,255};
	public int[] toRGB = {128,94,94};
	
	
	// Use this for initialization
	void Start () {
		if (scrollBar == null)
			scrollBar = GameObject.Find("Scroll Bar").GetComponent<UIScrollBar>();
		
		//Log.Debug ("color?" + transform.GetComponent<SkinnedMeshRenderer>().material.GetColor("_Color").r * 255f);
		//scrollBar.scrollValue = Mathf.InverseLerp(fromRGB[0], toRGB[0],transform.GetComponent<SkinnedMeshRenderer>().material.GetColor("_Color").r * 255f);
	
		scrollBar.scrollValue = 0.5f;
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		
		
		transform.GetComponent<SkinnedMeshRenderer>().material.SetColor("_Color",
			new Color(Mathf.Lerp(fromRGB[0], toRGB[0], scrollBar.scrollValue)/255f,
			Mathf.Lerp(fromRGB[1], toRGB[1], scrollBar.scrollValue)/255f,
			Mathf.Lerp(fromRGB[2], toRGB[2], scrollBar.scrollValue)/255f,
			transform.GetComponent<SkinnedMeshRenderer>().material.GetColor("_Color").a));
	
	}
}

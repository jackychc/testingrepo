using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ClosetScrollListManager : MonoBehaviour {

	public GameObject scrollPanelC;
	public GameObject scrollPanelT;
	UICenterOnChild categoryCenter, itemCenter;
	
	public UIPanelAlphaTweener itemNamePanel;
	public UILabel itemNameLabel;
	
	public HairColorScrollListManager hcsl;
	public PositionTweener hairColorPanel;
	
	public PositionTweener alphaPanel;
	public AlphaSetter alphaSetter;
	
	public PositionTweener bgPosTweener, bkPosTweener;
	
	public Transform tDown, tUp, tBG;
	
	List<ItemCategory> categoryList = new List<ItemCategory>();
	
	string categoryName = "";
	GameObject girl;
	bool isZoomingIn;
	
	
	int previousSort = 0;
	float yPos = 0f;
	
	bool cancelHide;
	
	
	// Use this for initialization
	void Start () {
		
		StartCoroutine(DelayedStart());
	}
	
	IEnumerator DelayedStart()
	{
		yield return new WaitForEndOfFrame();
		
		girl = GameObject.Find("FEMALE");
		
		categoryCenter = scrollPanelC.GetComponent<UICenterOnChild>();
		itemCenter = scrollPanelT.GetComponent<UICenterOnChild>();
		scrollPanelC.GetComponent<UIDraggablePanel>().onDragFinished += DragCategoryFinished;
		scrollPanelT.GetComponent<UIDraggablePanel>().onDragFinished += DragItemFinished;
		
		AdjustForHigherScreens();
		LoadCategories();
		LoadItems();
		focusToWearingItem();
		
		scrollPanelC.GetComponent<ClothCatagoryHighlighter>().SendMessage("OnShopScrollListInitialized", true);
		
		
	}
	
	public void LoadCategories()
	{
		
		foreach (ItemCategory category in ItemCategories.allCategories)
		{
			foreach (Item item in Globals.mainPlayer.owningItems)
			{
				
				if (item == null) Log.Error("!?!?!?!?");
				if (item.itemName == null)
					
				{	
					Log.Error("item id !?!?!?!? " + item.itemId);
					Log.Error("!?!?!?!? X2");
				}
				
				if (item.itemName.Equals("None"))
					continue;
				
				if (category.listOfSubcategories.Contains(ItemSubcategories.getSubcategory(item.subCategoryId)))
				{
					if (!categoryList.Contains(category))
					{
						categoryList.Add(category);
						break;
					}
				}
			}
		}
		//categoryList.Sort((x, y) => x.categoryId.CompareTo(y.categoryId));
		
		for (int i = 0; i < categoryList.Count; i++)
		{
			GameObject go = (GameObject) Instantiate(Resources.Load ("CategoryScrollListItem") as GameObject);
			
				go.name = i + " - " + categoryList[i].categoryName;
				go.transform.parent = scrollPanelC.transform;
				
				go.GetComponent<CategoryScrollListItem>().category = categoryList[i];
				
				go.transform.localPosition = new Vector3(0f, i * -99f,0f);
				go.transform.localRotation = Quaternion.identity;
				go.transform.localScale = new Vector3(74f,73f, 1f);
			
			Log.Debug("category["+i+"] : "+ categoryList[i].categoryName);
		}	
	}
	
	public void LoadItems()
	{

		ItemCategory category;
		if (categoryName.Equals(""))
		{
			category = categoryList[0];
			categoryName = category.categoryName;
		}
		else
		{
			category = categoryCenter.centeredObject.GetComponent<CategoryScrollListItem>().category;
			categoryName = categoryCenter.centeredObject.name; //!
		}
		LoadZoom();
		LoadRotate();
		
		
		Transform itemPanel = scrollPanelT.transform;
		

		// remove previous category items
		
		int childs = itemPanel.childCount;

        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(itemPanel.GetChild(i).gameObject);
        }
		
		// add this category items
		
		List<ItemSubcategory> subcats = ItemCategories.getListOfSubcategoriesByCategory(category.categoryId);
	
		yPos = 0f;
		
		// sort by level......
		List<Item> sieve = new List<Item>();
		
		// loop through all player items, if contains the subcat, add!
		for (int i =0 ; i < Globals.mainPlayer.owningItems.Count; i++)
		{
			Item item = Globals.mainPlayer.owningItems[i];
			
			if (subcats.Contains(ItemSubcategories.getSubcategory(item.subCategoryId)))
			{
			
				sieve.Add(item);
			
			}
		}
		
		//sieve.Sort((x, y) => (x.subCategoryId != y.subCategoryId)? x.subCategoryId.CompareTo(y.subCategoryId): x.requiredLevel.CompareTo(y.requiredLevel));
		
		for (int i =0 ; i < sieve.Count; i++)
		{
			Item item = sieve[i];
			
			//if (subcats.Contains(ItemSubcategories.getSubcategory(item.subCategoryId)))
			{
			
				GameObject go = (GameObject) Instantiate(Resources.Load ("ClosetItem") as GameObject);
			
				go.name = item.itemName;
				go.transform.parent = itemPanel;
				
				go.GetComponent<ClosetItem>().item = item;
				go.GetComponent<ClosetItem>().sortOrder = i+1;
				go.GetComponent<ClosetItem>().AssignFindings(this,scrollPanelT,girl);
				go.transform.localPosition = new Vector3(0, yPos,0f);
				go.transform.localRotation = Quaternion.identity;
				go.transform.localScale = new Vector3(1f,1f, 1f);
			
				
				yPos -= 159f;
			
			}
		}
		
		List<Shop> shops = Shops.GetShopsWithSubcategories(subcats);
		// also a cart object
		
		if (shops.Count > 0)
		{
			GameObject cart = (GameObject) Instantiate(Resources.Load ("ClosetCart") as GameObject);	
			cart.name = "ClosetZCart";
			cart.transform.parent = itemPanel;
			cart.GetComponent<ClosetCartItem>().SetShops(shops);
			Destroy(cart.GetComponent<UIPanel>());
			cart.transform.localPosition = new Vector3(0, yPos,0f);
			cart.transform.localRotation = Quaternion.identity;
			cart.transform.localScale = new Vector3(1f,1f, 1f);
		}
		
		refreshTicks();
		LoadPanels();
		
		StartCoroutine(DelayedUnload());
		
	}
	
	IEnumerator DelayedUnload()
	{
		yield return new WaitForSeconds(1.0f);
		Resources.UnloadUnusedAssets();
	}
	public void LoadZoom()
	{
		
		if (categoryCenter.centeredObject == null) return;
		
		ItemCategory category = categoryCenter.centeredObject.GetComponent<CategoryScrollListItem>().category;
		
		// testing zoom in
		if (category.willZoom)
			zoomIn();
		
		else
			zoomOut();
	}
	
	public void LoadRotate()
	{
		
	}
	
	public void LoadPanels()
	{
		LoadColorPanel();
		LoadAlphaPanel();
	}
	
	void LoadColorPanel()
	{
		if (categoryName.Contains("Front Hair") ||
			categoryName.Contains("Back Hair") ||
			categoryName.Contains("Lips") ||
			categoryName.Contains("Eye Makeup")
			) 
		{
			hairColorPanel.Forward();
			
			ItemSubcategory targetSubcat = null;
			
			if (categoryName.Contains("Front Hair") ||
				categoryName.Contains("Back Hair"))
					targetSubcat = ItemSubcategories.getSubcategory("HAIRCOLOR");
				
			if (categoryName.Contains("Lips"))
					targetSubcat = ItemSubcategories.getSubcategory("LIPSCOLOR");
			
			if (categoryName.Contains("Eye Makeup"))
					targetSubcat = ItemSubcategories.getSubcategory("EYESHAPECOLOR");
					
			
			
			hcsl.SetSubcategory(targetSubcat);
		}
		else
		{
			hairColorPanel.Backward();
		}
	}
	
	void LoadAlphaPanel()
	{
		if (
			categoryName.Contains("Rouge") ||
			categoryName.Contains("Eye Makeup")
			) 
		{
			alphaPanel.Forward();
			
			string mode = "";
			
			if (categoryName.Contains("Rouge"))
					mode = "ROUGE";
			
			if (categoryName.Contains("Eye Makeup"))
					mode = "EYESHADOW";
					
			
			// todo: some action here
			alphaSetter.SetMode(mode);
		}
		else
		{
			alphaPanel.Backward();
		}
	}
	
	public void HideAllPanels()
	{
		hairColorPanel.Backward();
		alphaPanel.Backward();
	}
	
	public void OnSliderChange()
	{
		//Log.Debug("test how slider changes");
	}
	
	
	void DragCategoryFinished()
	{
		
		if (categoryName.Equals(categoryCenter.centeredObject.name)) return;
		
		LoadItems();
			//SpringPanel.Begin(scrollPanelT, new Vector3(0f,0f ,0f), 5f);	
		focusToWearingItem();
		
		//should focus the item list to the "wearing" item
		
		// but impossible to be "wear item" ar?
		//WearItem();
	}
	
	void DragItemFinished()
	{

	}
	


	public void WearItem()
	{
		
		
		
		
	}

	public void zoomIn()
	{
		bgPosTweener.Forward();
		bkPosTweener.Forward();
		isZoomingIn = true;
	}
	
	public void zoomOut()
	{
		bgPosTweener.Backward();
		bkPosTweener.Backward();
		isZoomingIn = false;
	}
	
	public bool isZooming()
	{
		return isZoomingIn;
	}
	
	public void goUp()
	{
		Log.Debug("going up");
		relativeItem(-5);
	}
	public void goDown()
	{
		Log.Debug("going down");
		relativeItem(5);
	}
	
	public void relativeItem(int position)
	{
		itemCenter.Recenter(); // get the center
		
	
		float springAmount = -itemCenter.centeredObject.transform.localPosition.y + (159f* position);
		Log.Debug("spring? = "+springAmount);
		springAmount = Mathf.Min(Mathf.Max(0f, springAmount), -yPos);
		Log.Debug("spring after? = "+springAmount);
		SpringPanel.Begin(scrollPanelT, new Vector3(0f,springAmount,0f), 5f);
		 
	}
	
	public void focusToWearingItem()
	{
		
		StartCoroutine("focus");
	}
	
	public IEnumerator focus()
	{
		yield return new WaitForSeconds(0.05f);
		
		float springAmount = 0f;
		float topMostY = Mathf.NegativeInfinity;
		
		// because the items are sorted by name, so we need to determine the actual first element by position 
		foreach (ClosetItem ci in scrollPanelT.GetComponentsInChildren<ClosetItem>())
		{
			//Log.Debug("checking item: "+ci.item.itemName);
			
			if (Globals.mainPlayer.isWearingItem(ci.item))
			{
				if (ci.transform.localPosition.y > topMostY)
				{
					topMostY = ci.transform.localPosition.y;
					springAmount = -ci.transform.localPosition.y;
				}
				
				
			}
		}
		
//		Log.Debug("sp amount: "+springAmount);
		
		SpringPanel.Begin(scrollPanelT, new Vector3(0f,springAmount ,0f), 5f);		
	}
	
	public void refreshTicks()
	{
		foreach (ClosetItem ci in scrollPanelT.GetComponentsInChildren<ClosetItem>())
		{
			if (Globals.mainPlayer.isWearingItem(ci.item))
			{
				ci.tick.SetActive(true);
				//ci.GetComponent<RotationTweener>().Forward();
				//ci.scaleTweener.Forward();
			}
			
			else
			{
				ci.tick.SetActive(false);
				//ci.GetComponent<RotationTweener>().Backward();
				 //ci.scaleTweener.Backward();
			}
		}
		
	}
	
	public void showItemName(Item item)
	{
		StartCoroutine(showItemNameRoutine(item));
	}
	
	IEnumerator showItemNameRoutine(Item item)
	{
#if !UNITY_EDITOR
		if (ItemSubcategories.getSubcategory(item.subCategoryId).shouldShowItemName)
#endif
		{
			
			itemNamePanel.setProportion(0f);
			itemNamePanel.Forward();
			
			yield return new WaitForEndOfFrame();
			itemNameLabel.text = item.itemName;
		}
	}
	
	void AdjustForHigherScreens()
	{
		if (1f*Screen.height / Screen.width > 1.5f)
		{
			int diff = (int) (640* (1f*Screen.height / Screen.width)) - 960;
			
			Vector3 t3 = Vector3.zero;
			Vector4 t4 = Vector4.zero;
			
			
			// Category Part
			t4 = scrollPanelC.GetComponent<UIPanel>().clipRange;
			t4.w = t4.w + diff - 20;
			scrollPanelC.GetComponent<UIPanel>().clipRange = t4;
			
			// Item Part
			
			t4 = scrollPanelT.GetComponent<UIPanel>().clipRange;
			t4.w = t4.w + diff - 20;
			scrollPanelT.GetComponent<UIPanel>().clipRange = t4;
			
			Transform tParent 	= scrollPanelT.transform.parent;
			
			t3 = tDown.localPosition;
			t3.y -= (diff/2 - 5);
			tDown.localPosition = t3;
			
			t3 = tUp.localPosition;
			t3.y += (diff/2 - 5);
			tUp.localPosition = t3;
			
			t3 = tBG.localScale;
			t3.y += diff;
			tBG.localScale = t3;
			
			
		}
	}
}

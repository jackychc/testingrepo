using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class MiniGameClothMatchScene : MonoBehaviour
{
	public static bool hasShownTutorial = false;
	
	UICamera cam;
	
	GameObject newCharacter, girlParent;
	Player fakePlayer;
	public GameObject timerGO, progressBarGO;
	public UITexture[] itemIconTex = new UITexture[3];
	public UIFilledSprite progressSprite;
	public UISysFontLabel scoreLabel;
	public GameObject comboObject;
	public UISysFontLabel comboLabel;
	public GameObject[] correctSymbol = new GameObject[3];
	public GameObject[] wrongSymbol = new GameObject[3];
	public AnchorTweener slotTweener;
	public DampedOscillator oscillator;
	public bool playedOnce;

	
	MegaMorph morpher;
	
	public ParticleSystem charEffect;
	public ParticleSystem []itemEffect = new ParticleSystem[3];
	
	public GameObject HintsGroupObj;
	
	
	public ScaleTweener startTweener;
	
	public PositionTweener[] curtainTweener = new PositionTweener[3];
	
	float progress, apparentProgress;
	
	public static float gameTime = 0f;
	int score = 0, combo = 0, maxCombo = 0;
	int gTicket;
	
	List<Item> targetSet;
	int targetIndex;
	int correctCount;
	Item targetItem;
	Item judgeTargetItem;
	
	bool stopPressed = false;
	bool onFailWalk = false;
	bool isPlaying = true;
	bool rewardClicked = false;
	string defaultItems = "34,180,1,21,71,107,114,119,127,137,150,156,201,190,215";
	
	float charPosition, currentCharPosition, originalCharPosition;
	float charRotation, currentCharRotation;
	Vector3 charPosVector = Vector3.zero, charRotVector = Vector3.zero;
	
	
	List<Shop> predefinedShops = new List<Shop>()
	{
		Shops.GetShopById(2),
		Shops.GetShopById(4)
	};
	
	List<Item> excludeShoeItems = new List<Item>()
	{
		Items.getItem(235),
		Items.getItem(237),
		Items.getItem(238),
		Items.getItem(239),
		Items.getItem(240),
		Items.getItem(241),
		Items.getItem(242),
		Items.getItem(243),
		Items.getItem(246),
		Items.getItem(247),
		Items.getItem(248),
		Items.getItem(491),
		Items.getItem(493),
		Items.getItem(496),
		Items.getItem(497),
		Items.getItem(499)
	};
	
	/*List<List<Item>> predefinedSets = new List<List<Item>>()
	{
		Items.stringToItemList("1,95,351"),
		Items.stringToItemList("2,96,352"),
		Items.stringToItemList("3,97,353"),
		Items.stringToItemList("4,98,354"),
		Items.stringToItemList("5,99,355"),
		Items.stringToItemList("6,100,356"),
		Items.stringToItemList("7,101,357"),
		Items.stringToItemList("1,102,358"),
		Items.stringToItemList("2,103,359"),
		Items.stringToItemList("10,104,360"),
	};*/

	// Use this for initialization
	IEnumerator Start()
	{
		
		fakePlayer = new Player();
		fakePlayer.playerId = -2;
		fakePlayer.wearingItems = Items.stringToItemList(Items.itemListToString(Globals.mainPlayer.wearingItems));
		RemoveAllRemovableItems();		
		/*GameObject femaleObj = GameObject.Find("FEMALE");
		if (femaleObj == null)
		{
			newCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			newCharacter.name = "FEMALE";
			newCharacter.AddComponent<CharacterManager>();
		}		
		else
		{
			newCharacter = femaleObj;
		}*/
		
		newCharacter = Globals.globalCharacter;
		girlParent = GameObject.Find("GirlParent");		
		//newCharacter.transform.parent = girlParent.transform;
		girlParent.transform.localPosition = Vector3.zero;
		girlParent.transform.localRotation = Quaternion.identity;
		girlParent.transform.localScale = Vector3.one;
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
		yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(fakePlayer));
		
		
		morpher = newCharacter.transform.Find("Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head/head").GetComponent<MegaMorph>();
		
		cam = GameObject.Find("UICamera").GetComponent<UICamera>();
		StartCoroutine(LoadRules());		
		
		//Disable the un-necessary UI
		GameObject topRightArea = slotTweener.gameObject;
		//topRightArea.SetActive(false);
		
		
		girlParent.SetActive(false);
		UIAnchor topRightAreaUIAnchor = slotTweener.GetComponent<UIAnchor>();
		
		topRightAreaUIAnchor.relativeOffset.x = slotTweener.fromXValue;
		
	
		SoundManager.SharedManager.ChangeMusic("SuperstarStory_FashionMinigame");
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.MINIGAMECLOTHMATCH);
	}
	
	IEnumerator LoadRules()
	{
		charPosition = 180f;
		currentCharPosition = 180f;
		originalCharPosition = 180f;
		charRotation = 60f;
		currentCharRotation = 60f;
		
		StartCoroutine(Move());
		
		yield return new WaitForSeconds(1.0f);
		PopupManager.ShowClothMatchStartPopup(false);
	}
	
	
	public void CheckRoutine()
	{
		charPosition = 180f;
		currentCharPosition = 180f;
		originalCharPosition = 180f;
		charRotation = 60f;
		currentCharRotation = 60f;
		
		
		
		if (Globals.mainPlayer.Energy >= 4)
		{
			Globals.mainPlayer.GainEnergy(-4);
			PopupManager.HideAllPopup();
			StartCoroutine(StartPressed());
			
			if (Globals.mainPlayer.NumOfSessions <= 1 && !hasShownTutorial)
			{
				HintsGroupObj.SetActive(true);
				hasShownTutorial = true;
			}
			
			//Log Flurry Event
			MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Runway_Played_times);
		}
		
		else
		{
			PopupManager.ShowNotEnoughCurrencyPopup("Energy", 4-Globals.mainPlayer.Energy, false);
			PopupManager.HideLatestPopup();
			StartCoroutine("WaitPopupEndRoutine");
		}
	}
	
	
	public IEnumerator WaitPopupEndRoutine()
	{
		yield return new WaitForSeconds(0.1f);
		
		if (cam.eventReceiverMask != -1)
		{
			StartCoroutine("WaitPopupEndRoutine");
		}
		else
		{
			PopupManager.ShowClothMatchStartPopup(false);
		}
	}
	
	public void RewardRoutine()
	{
		if (rewardClicked) return;

		rewardClicked = true;

		QuestFlags.SetFlag(17,true);
		originalCharPosition = -180f;
		charPosition = -180f;
		currentCharPosition = -180f;
		
		List<Reward> realPool = new List<Reward>();
		List<Reward> normalPool = new List<Reward>()
		{
			new Reward(9999, 1, 500, 0, 0, 0, "", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 1000, 0, 0, 0, "", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 1500, 0, 0, 0, "", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 2000, 0, 0, 0, "", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 100, 0, "", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 200, 0, "", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 300, 0, "", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 400, 0, "", new Skill(new int[]{0,0,0,0,0})),
		};
		List<Reward> itemPool = new List<Reward>()
		{
			//TODO:change to desginated items
			new Reward(9999, 1, 0, 0, 0, 0, "424", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "300", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "336", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "320", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "464", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "231", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "313", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "230", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "399", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "288", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "393", new Skill(new int[]{0,0,0,0,0})),
			new Reward(9999, 1, 0, 0, 0, 0, "268", new Skill(new int[]{0,0,0,0,0})),
			
		};
		
		int itemAt = UnityEngine.Random.Range(0,2+1);
		
		for (int i = 0; i < 3; i++)
		{
			if (i == itemAt)
				realPool.Add(itemPool[UnityEngine.Random.Range(0, itemPool.Count)]);
			else
				realPool.Add(normalPool[UnityEngine.Random.Range(0, normalPool.Count)]);
		}
		
		PopupManager.ShowChooseGiftPopup(realPool, 2, false);
		PopupManager.HideLatestPopup();
		StartCoroutine(WaitRewardEndRoutine());
		StartCoroutine(LogAcquiredClothRewardEvent(itemPool));
	}
	
	private IEnumerator LogAcquiredClothRewardEvent(List<Reward> itemPool)
	{
		//Log Flurry Event
		int totalItems = 0;
		int acquiredItems = 0;
		for (int i = 0; i < itemPool.Count; i++)
		{	
			Reward r = itemPool[i];
			string[] itemsStr = r.itemIds.Split(',');
			foreach(string itemStr in itemsStr)
			{
				Item item = Items.getItem(int.Parse(itemStr));
				if (item != null)
				{
					if (item.subCategoryId != (int)ItemSubcategories.Id.PHOTOSHOTBG)
					{
						totalItems++;
						
						for (int k = 0; k < Globals.mainPlayer.owningItems.Count; k++)
						{
							Item pItem = Globals.mainPlayer.owningItems[k];
							if (pItem.itemId == item.itemId)
								acquiredItems++;
						}
					}
				}
			}
		}
		
		Hashtable param = new Hashtable();
		param["OwnedRewards"] = acquiredItems;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Runway_Result, param);
		
		
		yield return null;
	}
	
	public IEnumerator WaitRewardEndRoutine()
	{
		yield return new WaitForSeconds(0.1f);
		
		if (cam.eventReceiverMask != -1)
		{
			StartCoroutine(WaitRewardEndRoutine());
		}
		else
		{
			rewardClicked = false;
			Globals.mainPlayer.ClothMatchTotalScore = 0;
			PopupManager.ShowClothMatchStartPopup(false);
		}
	}
	
	
	public IEnumerator StartPressed()
	{
		isPlaying = true;
		
		timerGO.SetActive(true);
		progressBarGO.SetActive(true);
		scoreLabel.gameObject.SetActive(true);
		gameTime = 40;
		correctCount = 0;
		progress = 1f;
		score =0 ;
		combo = 0;
		maxCombo = 0;
		
		// for ui lock
		stopPressed = true;
		
		slotTweener.Backward();
		
		yield return new WaitForSeconds(0.5f);
		
		startTweener.Forward();
		
		yield return new WaitForSeconds(1.5f);
		
		startTweener.Backward();
		
		StartCoroutine(BeginSet());
		
		StartCoroutine(GameTimer());
		
	}
	
	public IEnumerator LoadIconFromPersistent(Item i, Action<Texture2D> returnMe)
	{
		WWW loader = new WWW("file://" + Application.persistentDataPath + Path.DirectorySeparatorChar + i.itemId + "_icon.png");
		yield return loader;
		
		if (loader.error != null)
		{
			Log.Debug("ClothMatch MiniGame Cannot load the icon with itemId: " + i.itemId);
		}
		else
		{
			if (loader.texture != null)
			{
				returnMe((Texture2D)loader.texture);
			}
			else
			{
				Log.Debug("ClothMatch MiniGame Cannot read the texture with itemId: " + i.itemId);
			}
		}
	}	
	
	public IEnumerator BeginSet()
	{
		if (!isPlaying)
		{
			
			slotTweener.Backward();
			charPosition = 180f;
			currentCharPosition = 180f;
			originalCharPosition = 180f;
			charRotation = 60f;
			currentCharRotation = 60f;
			yield break;
		
		}
		
		if (girlParent != null)
			girlParent.SetActive(true);
		if (slotTweener != null)
			slotTweener.gameObject.SetActive(true);
		
		onFailWalk = false;
		
		targetIndex = 0;
		correctCount = 0;
		
		
		Shop targetShop = null;
		//while (targetShop.items.Count <= 0)
			
		List<Item> pool1 = new List<Item>();
		List<Item> pool2 = new List<Item>();
		List<Item> pool3 = new List<Item>();
		bool shouldRepeat = true;
		do
		{
			shouldRepeat = true;
			pool1.Clear();
			pool2.Clear();
			pool3.Clear();
			targetShop = predefinedShops[UnityEngine.Random.Range(0, predefinedShops.Count)];
			
			
			// 1.dress
			foreach (Item i in targetShop.items)
			{
				//Log.Debug("dlcid of item:"+i.itemId+"="+i.dlcId);
				if (i.dlcId <= Globals.currentDLCVersion) // check if item requires DLC
				{
					if (i.subCategoryId == (int)ItemSubcategories.Id.DRESS)
					{
						pool1.Add(i);
					}
					if (i.subCategoryId == (int)ItemSubcategories.Id.HAT)
					{
						pool2.Add(i);
					}
					if (i.subCategoryId == (int)ItemSubcategories.Id.SHOES)
					{
						pool3.Add(i);
					}
				}
			}		
			
			targetSet = new List<Item>();
			
			
			if (pool1.Count > 0)
			{
				Item target1 = pool1[UnityEngine.Random.Range(0, pool1.Count)];
			
				targetSet.Add(target1);
				
				if (pool2.Count > 0)
				{
					targetSet.Add(pool2[UnityEngine.Random.Range(0, pool2.Count)]);
					if (excludeShoeItems != null && excludeShoeItems.Count != 0)
					{
						bool isExcluded = false;
						for (int i = 0; i < excludeShoeItems.Count; i++)
						{
							Item excludeItem = excludeShoeItems[i];

							if (excludeItem != null)
							{
								if (excludeItem.itemId == target1.itemId)
								{
									isExcluded = true;
									break;
								}
							}
						}

						if (!isExcluded)
						{
							if (pool3.Count > 0)
							{
								targetSet.Add(pool3[UnityEngine.Random.Range(0, pool3.Count)]);
								shouldRepeat = false;
							}
						}
						else
						{
							//the dress is excluded items
							shouldRepeat = false;
						}
					}
				}
			}			
			
		}
		while (shouldRepeat);
		
		//ensure the character is active
		yield return new WaitForEndOfFrame();
		
		fakePlayer = new Player();
		fakePlayer.playerId = -2;
		fakePlayer.wearingItems = Items.stringToItemList(Items.itemListToString(Globals.mainPlayer.wearingItems));
		RemoveAllRemovableItems();
		yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(fakePlayer, true));
		
		yield return new WaitForSeconds(0.125f);
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("street_walk");
		yield return new WaitForSeconds(0.125f);
		
		
		if (!isPlaying)
		{
			slotTweener.Backward();
			charPosition = 180f;
			currentCharPosition = 180f;
			originalCharPosition = 180f;
			charRotation = 60f;
			currentCharRotation = 60f;
			yield break;
		}
		
		morpher.GetChannel("head_laugh").SetTarget(0f, 100f);
		morpher.GetChannel("head_sad").SetTarget(0f, 100f);
			
		charRotation = 60f;
		currentCharRotation = 60f;
		
		originalCharPosition = -180f;
		charPosition = -40f;
		currentCharPosition = -180f;
		
		
		yield return new WaitForSeconds(0.50f);
		
		
		foreach (PositionTweener pt in curtainTweener)
		{
			pt.Backward();
		}
		
		foreach (GameObject go in correctSymbol)
		{
			go.transform.localScale = new Vector3(0.0001f, 0.0001f, 1f);
		}
		foreach (GameObject go in wrongSymbol)
		{
			go.SetActive(false);
		}
		
		for (int i = 0; i < targetSet.Count; i++)
		{
			Texture2D targetIcon = Items.GetIconOfItem(targetSet[i]);
			if(targetIcon == null)
			{
				yield return StartCoroutine(LoadIconFromPersistent(targetSet[i], result => targetIcon = result));
			}
			
			string shaderName 	= "UnlitTransparentColored";
			Shader shader 		= Resources.Load("Shaders/" + shaderName) as Shader;
			if (shader != null)
				itemIconTex[i].material = new Material(shader);
			else
				itemIconTex[i].material = new Material(Shader.Find ("Unlit/Transparent Colored"));
	        itemIconTex[i].material.mainTexture = targetIcon;
	        itemIconTex[i].transform.localScale = new Vector3(128f,128f,1f);
		}
		
		yield return new WaitForSeconds(0.50f);
		
		
		if (!isPlaying)
		{
			slotTweener.Backward();
			charPosition = 180f;
			currentCharPosition = 180f;
			originalCharPosition = 180f;
			charRotation = 60f;
			currentCharRotation = 60f;
			yield break;
		}
		
		slotTweener.Forward();
		
		
		
		while (currentCharPosition < -45f)
			yield return new WaitForSeconds(0.02f);
		
		
		charRotation = 0f;
		
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("pose01");
		
		yield return new WaitForSeconds(0.5f);
		
		if (!isPlaying)
		{
			slotTweener.Backward();
			charPosition = 180f;
			currentCharPosition = 180f;
			originalCharPosition = 180f;
			charRotation = 60f;
			currentCharRotation = 60f;
			yield break;
		}
		
		StartCoroutine(BeginRound());
		
		yield return null;
		
	}

	
	public IEnumerator BeginRound()
	{
		int ticket = UnityEngine.Random.Range(1,1000000000);
		gTicket = ticket;

		judgeTargetItem = null;
		targetItem = targetSet[targetIndex];
		
		curtainTweener[targetIndex].Forward();
		
		stopPressed = false;
		
		if (!isPlaying)
		{
			slotTweener.Backward();
			charPosition = 180f;
			currentCharPosition = 180f;
			originalCharPosition = 180f;
			charRotation = 60f;
			currentCharRotation = 60f;
			yield break;
		}
		
		StartCoroutine (ChangeClothRepeatedly(ticket));
		yield return null;
	}
	
	public IEnumerator ChangeClothRepeatedly(int ticket)
	{
		if (stopPressed) yield break;

		List<int> duplicateIds = new List<int>();
		duplicateIds.Add(targetItem.itemId);

		List<Item> poolOfItems = new List<Item>();		
		poolOfItems.Add(targetItem);

		int targetSetLength = UnityEngine.Random.Range(3,5);

		while(poolOfItems.Count < targetSetLength)
		{
			Item it = Items.getRandomItemOfSubcategory(targetItem.subCategoryId);

			if (duplicateIds.Contains(it.itemId))
			{
				continue;
			}
			else if (it != null && !Items.requireDLC(it))
			{
				bool needsContinue = false;
				for (int i = 0; i < excludeShoeItems.Count; i++)
				{
					Item excludeItem = excludeShoeItems[i];

					if (excludeItem != null)
					{
						if (excludeItem.itemId.Equals(it.itemId))
						{
							needsContinue = true;
							break;
						}
					}
				}

				if (needsContinue)
					continue;

				poolOfItems.Add(it);
				duplicateIds.Add(it.itemId);
			}
		}


		/*
		for (int i = 0; i < ; i++)
		{
			Item it = Items.getRandomItemOfSubcategory(targetItem.subCategoryId);
			
			if (it != null && !Items.requireDLC(it))
				poolOfItems.Add(it);

			//if (poolOfItems.Count >= targetSetLength)
			//	break;
		}
		*/

		//todo: shuffle
		poolOfItems = ShuffleList<Item>(poolOfItems);

		while (ticket == gTicket)
		{
			//int loopIndex = UnityEngine.Random.Range(0, poolOfItems.Count);

			for (int l = 0; l < poolOfItems.Count; l++)
			{
				if (ticket != gTicket) break;
				
				fakePlayer.wearItem(poolOfItems[l]);
				stopPressed = true;

				//newCharacter.GetComponent<CharacterManager>().PartialGenerateCharacter(fakePlayer, new List<ItemSubcategory>(){ItemSubcategories.getSubcategory(poolOfItems[l].subCategoryId)});
				yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(fakePlayer, false));

				stopPressed = false;
				judgeTargetItem = poolOfItems[l];
				yield return new WaitForSeconds(0.759f);
			}
		}
	}
	
	public IEnumerator StopPressed()
	{
		StopCoroutine("ChangeClothRepeatedly");

		if (HintsGroupObj.activeSelf)
		{
			TweenAlpha ta = HintsGroupObj.GetComponent<TweenAlpha>();
			ta.enabled = true;
		}		
		
		if (stopPressed) yield break;
		
		if (gameTime <= 0) yield break;
		
		stopPressed = true;

		
		gTicket = UnityEngine.Random.Range(1,1000000000);
		
		
		if (targetItem.Equals(judgeTargetItem))
		{
			// correct answer!
			Log.Debug("correct answer");
			
			charEffect.Play();
			itemEffect[targetIndex].Play();
			
			correctSymbol[targetIndex].transform.localScale = Vector3.one;
			correctCount++;
			
			
			combo++;
			score+= 100 + (combo-1)*10 ;
			
			//extra score for correct whole set
			if (correctCount == targetSet.Count)
				score+= 100 ;
			
			if (combo > maxCombo)
				maxCombo = combo;
			
			SoundManager.SharedManager.PlaySFX("changeClothes_combo"+Mathf.Min(9, combo));
			
			comboLabel.Text = "" + combo;
			comboObject.SetActive(true);
			comboObject.GetComponent<ScaleTweener>().speed = combo;
			
		
			yield return new WaitForSeconds(0.3f);
		}
		else
		{
			// wrong answer!
			Log.Debug("wrong answer");
			combo = 0;
			
			SoundManager.SharedManager.PlaySFX("changeClothes_click_wrong");
			
			comboObject.SetActive(false);
			oscillator.StartOscillate();
			wrongSymbol[targetIndex].SetActive(true);
			
			yield return new WaitForSeconds(1.3f);
		}
		
		if (gameTime > 0)
		{
			targetIndex++;
			if (targetIndex < targetSet.Count)
			{

				// little delay for performance
				yield return new WaitForSeconds(0.5f);

				StartCoroutine(BeginRound());
			}
				
			else
			{
				
				
				if(correctCount == targetSet.Count)
				{
					
					// end set actions
					
					morpher.GetChannel("head_laugh").SetTarget(100f, 100f);
					
					charRotation = 60f;
					yield return new WaitForSeconds(0.2f);
					
					originalCharPosition = -40f;
					charPosition = 100f;
					
					newCharacter.GetComponent<CharacterManager>().InsertAnimation("street_walk");
					
					SoundManager.SharedManager.PlaySFX("changeClothes_walkin");
					
					yield return new WaitForSeconds(1.1f);
					
					slotTweener.Backward();
				}
				
				else
				{
					onFailWalk = true;
					
					newCharacter.GetComponent<CharacterManager>().InsertAnimation("minigame_fail");
					newCharacter.GetComponent<CharacterManager>().QueueAnimation("minigame_fail_walk");
					
					SoundManager.SharedManager.PlaySFX("changeClothes_lose");
					
					morpher.GetChannel("head_sad").SetTarget(100f, 100f);
					
					yield return new WaitForSeconds(1.25f);
					
					charRotation = 60f;
					yield return new WaitForSeconds(0.2f);
					
					originalCharPosition = -40f;
					charPosition = 100f;
					
					yield return new WaitForSeconds(3.0f);
					
					slotTweener.Backward();
				}
				
				
				// a double check is really required...
				if (isPlaying)
				{
					StartCoroutine(BeginSet());
				}
			}
		}
	}

	
	
	public IEnumerator GameTimer()
	{
		
		
		yield return new WaitForSeconds(0.05f);
		
		gameTime = gameTime - 0.05f;
		progress = gameTime*1f/40f;
		
		
		//timerText.text = (int)(gameTime) + "";
		
		if (gameTime > 0f)
			StartCoroutine(GameTimer());
		else
			StartCoroutine(EndGame());
	}
	
	public IEnumerator EndGame()
	{
		isPlaying = false;
		stopPressed = true;
		
		gTicket = UnityEngine.Random.Range(1,1000000000);
		
		originalCharPosition = -180f;
		charPosition = -180f;
		currentCharPosition = -180f;
		
		slotTweener.Backward();
		
		Globals.mainPlayer.ClothMatchTotalScore += score;
		
		//playedOnce = true;
		
		PopupManager.ShowClothMatchEndPopup(score,maxCombo, false);
		
		comboObject.SetActive(false);
		
		
		QuestFlags.SetFlag(8, QuestFlags.GetFlag(8).intValue +1);
		//QuestFlags.var8_playedRunwayTimes++;
		
		yield return null;
	}
	
	
	// Update is called once per frame
	IEnumerator Move()
	{
		apparentProgress = Mathf.Max(progress, apparentProgress-0.002f);
		
		progressSprite.fillAmount = apparentProgress;
		
		scoreLabel.Text = Language.Get("minigame_yourscore") +" "+score;
		
		// PRS update
		currentCharPosition = Mathf.Min(charPosition, currentCharPosition+ (charPosition - originalCharPosition) * (onFailWalk? 0.006f:0.012f));
		charPosVector.x = currentCharPosition;
		girlParent.transform.localPosition = charPosVector;
		
		currentCharRotation += (charRotation - currentCharRotation) * 0.1f;
		charRotVector.y= currentCharRotation;
		girlParent.transform.localEulerAngles = charRotVector;
		
		yield return new WaitForSeconds(0.015f);
		
		StartCoroutine(Move());
	}
	
	void RemoveAllRemovableItems()
	{
		foreach (ItemSubcategory subcat in Characters.FEMALE.getCharacterParts())
		{
			Item noneItem = Items.getNoneItemOfSubcategory(subcat.subcategoryid);
			if (noneItem != null)
			{
				fakePlayer.wearItem(noneItem);
			}
		}
	}
	
	public void DirectExit()
	{
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAME, false);	
	}
	
	
	public void GoToStreetScene()
	{
		if (!Mathf.Approximately(0f, MiniGameClothMatchScene.gameTime))
			PopupManager.ShowSureQuitPopup(Scenes.MINIGAME, false);
		else
			SceneManager.SharedManager.LoadScene(Scenes.MINIGAME, false);	
	}
	
	#region Hints	
	public void OnHitsGroupTweenAlphaFinished()
	{
		HintsGroupObj.SetActive(false);	
	}
	#endregion
	
	private static List<E> ShuffleList<E>(List<E> inputList)
	{
	     List<E> randomList = new List<E>();
		 List<E> copyList = new List<E>();
		foreach (E item in inputList)
		{
			copyList.Add(item);
		}
		
		
	
	     System.Random r = new System.Random();
	     int randomIndex = 0;
	     while (copyList.Count > 0)
	     {
	          randomIndex = r.Next(0, copyList.Count); //Choose a random object in the list
			
			  if (!randomList.Contains(copyList[randomIndex]))
	         	 randomList.Add(copyList[randomIndex]); //add it to the new, random list
	          copyList.RemoveAt(randomIndex); //remove to avoid duplicates
	     }
		
	
	     return randomList; //return the new random list
	}
	
	void OnDestroy()
	{
		//morpher = Globals.globalCharacter.transform.Find("Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head/head").GetComponent<MegaMorph>();
		morpher.GetChannel("head_laugh").SetTarget(0f, 100f);
		morpher.GetChannel("head_sad").SetTarget(0f, 100f);
	}
}


using UnityEngine;
using System.Collections;
using System;

public class Reward
{
	public int rewardId;
	public int remainder;
	public int coins;
	public int gems;
	public int exp;
	public int energy;
	public string itemIds;
	public Skill skills = new Skill(new int[] {0,0,0,0,0});
	
	// for voting
	//public int probability;
	//public int tierId;
	
	/*public Reward(int pRewardId, int pRemainder, int pCoins, int pGems, int pExp, int pEnergy,
		string pItemIds, Skill pSkill) :
		this(pRewardId, pRemainder, pCoins, pGems, pExp, pEnergy, pItemIds, pSkill, 1, -1) {}
	 */
	
	public Reward(int pRewardId, int pRemainder, int pCoins, int pGems, int pExp, int pEnergy,
		string pItemIds, Skill pSkill/*, int pProbability, int pTierId*/)
	{
		this.rewardId 	= pRewardId;
		this.remainder 	= pRemainder;
		this.coins 		= pCoins;
		this.gems 		= pGems;
		this.exp 		= pExp;
		this.energy 	= pEnergy;
		this.itemIds 	= pItemIds;
		this.skills 	= new Skill(Skill.GetSkillAsString(pSkill)); // let's make array copying safer
		//this.probability = pProbability;
		//this.tierId = pTierId;
	}
	
	public Reward(int pRewardId)
	{
		this.rewardId = pRewardId;
	}
	
	public void claim()
	{
		Log.Debug("just claiming");
		
		if (remainder != 0) // able to claim
		{
			if (remainder > 0)
				remainder--;
			
			// find 

			Globals.mainPlayer.GainCoins(coins);
			Globals.mainPlayer.GainGems(gems);
			Globals.mainPlayer.GainExp(exp);
			Globals.mainPlayer.GainEnergy(energy);
			
			if (!itemIds.Equals(""))
			{
				// TODO
				
				string[] sub = itemIds.TrimStart('{').TrimEnd('}').Split('|');
		
				foreach (string ss in sub)
				{
					string[] sss = ss.Split(':');
					
					int itemId = Int32.Parse(sss[0]);
					//int qty = Int32.Parse(sss[1]);
					
					if (Items.getItem(itemId) != null)
					{
						if (!Globals.mainPlayer.owningItems.Contains(Items.getItem(itemId)))
							Globals.mainPlayer.owningItems.Add(Items.getItem(itemId));
					}
					else
						Log.Error("no such item");
					
				}
			}
			
			Skill s = Globals.mainPlayer.getSkill();		
			for (int i = 0; i < 5; i++)
			{
				int amendedAmount = 0;
				SkillTier next = SkillTable.GetNextSkillTier(i, Globals.mainPlayer.getSkill().skill[i]);
				if (next != null)
				{
					amendedAmount = Mathf.Min(next.skillAmount - Globals.mainPlayer.getSkill().skill[i], skills.skill[i]);
				}
				
				
				s.skill[i] += amendedAmount;
			}
			
			Globals.mainPlayer.setSkill(s);
			/*
			GameObject g = GameObject.Find("TitleShower");
			if (g != null)
				g.GetComponent<TitleShower>().Refresh();
			*/
		}
		
		//Rewards.Save();
	}
}


using UnityEngine;
using System.Collections;

public class JobIconsInJobList : MonoBehaviour {
	
	public GameObject[] icons = new GameObject[5];
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void setIcon0()
	{
		setIcon(0);
	}
	public void setIcon1()
	{
		setIcon(1);
	}
	public void setIcon2()
	{
		setIcon(2);
	}
	public void setIcon3()
	{
		setIcon(3);
	}
	public void setIcon4()
	{
		setIcon(4);
	}
	
	public void setIcon(int i)
	{
		for (int p = 0; p < 5; p++)
			icons[p].SetActive(false);
		
		icons[i].SetActive(true);
	}
}

using UnityEngine;
using System.Collections;

public class MenuToggler : MonoBehaviour {
	
	public enum MenuState
	{
		OPEN,
		CLOSED
	}
	
	public PositionTweener positionTweener1, positionTweener2;
	public MenuState defaultState = MenuState.CLOSED;
	public GameObject arrow;
	Vector3 arrowRotation;
	MenuState state;
	
	// Use this for initialization
	void Start () {
		state = defaultState;
		arrowRotation = arrow.transform.localEulerAngles;
		
		if (state.Equals(MenuState.OPEN))
		{
			positionTweener1.setProportion(1.0f);
			positionTweener1.Forward();
			positionTweener2.setProportion(1.0f);
			positionTweener2.Forward();
		}
		
		
		else
		{
			
			positionTweener1.Backward();
			positionTweener2.Backward();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Toggle()
	{
		if (state.Equals(MenuState.OPEN))
		{
			// close it
			positionTweener1.Backward();
			positionTweener2.Backward();
			state = MenuState.CLOSED;
			arrowRotation.y = 0f;
			arrow.transform.localEulerAngles = arrowRotation;
		}
		
		else
		{
			// open it
			positionTweener1.Forward();
			positionTweener2.Forward();
			state = MenuState.OPEN;
			arrowRotation.y = 180f;
			arrow.transform.localEulerAngles = arrowRotation;
			
			if (QuestFlags.GetFlag(-103).intValue == 0)
			{
				QuestFlags.SetFlag(-103, 1);
				PopupManager.ShowTutorialPopup(3, false);
			}
		}
		
		
	}
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AddFriendChecking : MonoBehaviour {

	public UIInput input;
	string targetGameId = "";
	
	public GameObject resultSuccessObj;
	public GameObject resultFailObj;
	
	public IEnumerator Check()
	{
		//PopupManager.ShowInformationPopup("Connecting to server...");
		PopupManager.ShowLoadingFlower();
		
		resultFailObj.SetActive(false);
		resultSuccessObj.SetActive(false);
		
		targetGameId = input.text.ToUpperInvariant();
		string response = "";
		Hashtable fh = null;		
		
		if (Friends.GetFriend(targetGameId) != null)
		{
			PopupManager.HideLoadingFlower();
			
			resultFailObj.SetActive(true);
			resultFailObj.GetComponentInChildren<UISysFontLabel>().Text = Language.Get("friend_doublefd");
			
			yield break;
		}	
		
		yield return StartCoroutine(CheckPlayerExist(result => response = result, result => fh = result));
		if (response.Equals("Exist"))
		{		
			// reuse the response
			yield return StartCoroutine(AddFriendOnServer(result => response = result));
			
			PopupManager.HideLoadingFlower();
			
			if (response.Equals("OK"))
			{
				yield return StartCoroutine(LinearlyAddFriend(fh));
							
				//PopupManager.ShowFriendAddedPopup();
				resultSuccessObj.SetActive(true);
				QuestFlags.SetFlag(18, QuestFlags.GetFlag(18).intValue+1);
				
				MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Add_Friends);
			}
			
			else
			{
				resultFailObj.SetActive(true);
				resultFailObj.GetComponentInChildren<UISysFontLabel>().Text = Language.Get("friend_unable");
			}			
			//PopupManager.HideAllPopup();			
		}
		else
		{
			PopupManager.HideLoadingFlower();
			
			//PopupManager.HideAllPopup();
			if (response.Equals("Self"))
			{
				resultFailObj.SetActive(true);
				resultFailObj.GetComponentInChildren<UISysFontLabel>().Text = Language.Get("friend_ownid");
			}
			else if (response.Equals("NotExist"))
			{
				resultFailObj.SetActive(true);
				resultFailObj.GetComponentInChildren<UISysFontLabel>().Text = Language.Get("friend_notexist");
				
			}
			else if (response.Equals("InvalidLetter"))
			{
				resultFailObj.SetActive(true);
				resultFailObj.GetComponentInChildren<UISysFontLabel>().Text = Language.Get("friend_notexist");
				
			}
			else
			{
				resultFailObj.SetActive(true);
				resultFailObj.GetComponentInChildren<UISysFontLabel>().Text = Language.Get("friend_unable");
				
			}
			
			//PopupManager.HideAllPopup();
		}
	}
	
	IEnumerator LinearlyAddFriend(Hashtable fh)
	{
		//Friends.InsertRow(fh);
		Friends.AddFriend(fh);
		yield return null;
	}
		
	IEnumerator CheckPlayerExist(System.Action<string> msg, System.Action<Hashtable> data)
	{
		//Prepare CheckSum
		GameID gameId = new GameID(targetGameId);
		int playerId = gameId.toInt();
		string checkSumData = gameId.ID + playerId;
		string checkSum = MD5Hash.GetServerCheckSumWithString("SearchPlayer", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", targetGameId);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "SearchPlayer.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null)
			{
				try
				{
					ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				
					if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
					{
						if (al[1] != null)
						{
							if (!targetGameId.Equals(Globals.mainPlayer.GameId))
							{
								// to do : check if already added...
								
								msg("Exist");
								data((Hashtable)al[1]);
							}
							else
							{
								msg("Self");
							}
						}
						else
							msg("NotExist");
					}
					else
					{
						msg("InvalidLetter");
					}
				}
				catch (Exception e)
				{
					Log.Debug(e);
					msg("Error");
				}
			}
			else
				msg("Error");
		}
		else
			msg("Error");
	}
	
	IEnumerator AddFriendOnServer(System.Action<string> msg)
	{
		//Prepare CheckSum
		GameID gameId = new GameID(Globals.mainPlayer.GameId);
		int playerId = gameId.toInt();
		string checkSumData = gameId.ID + playerId + targetGameId;
		string checkSum = MD5Hash.GetServerCheckSumWithString("AddFriend", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("friendGameId", targetGameId);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "AddFriend.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null)
			{
				try
				{
					ArrayList al = new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				
					if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
					{
						msg("OK");
					}
					else
					{
						msg("Error");
					}
				}
				catch (Exception e)
				{
					Log.Debug(e);
					msg("Error");
				}
			}
			else
				msg("Error");
		}
		
		//yield return StartCoroutine(HTTPPost.Post("AddFriend", kvp, result => response = result));
		//Log.Debug("check add friend response "+response);
		
	}
	
	
}

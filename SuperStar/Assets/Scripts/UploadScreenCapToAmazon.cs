using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;

public class UploadScreenCapToAmazon : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(uploadPic());		
	}
	
	IEnumerator uploadPic()
	{
		//To prevent the system is not ready yet
		yield return new WaitForEndOfFrame();
		
		//Make a screen capture
		int width = Screen.width;
		int height = Screen.height;
		var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
		tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		tex.Apply();
		var imageBytes = tex.EncodeToPNG();
		Destroy(tex);
		
		//Variables to be prepared
		/*
		string AWSKey = "AKIAJSQSEMWZ5H4GVMYQ";
		string AWSSecret = "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";	
		string topicId = "6";
		string gameId = "000001";		
		string url = "http://superstarstory.s3.amazonaws.com/Topics"+topicId+"/"+gameId+".png";
		
		//*Amazon StringToSign
		string method = "PUT";
		string contentMD5 = "";
		string contentType = "image/png";
		string date = System.DateTime.UtcNow.ToUniversalTime().ToString("r");
		string canonicalizedAmzHeaders = "";
		string canonicalizedResource = "/superstarstory/Topics6/000001.png";
		string stringToSign = method + System.Environment.NewLine + 
							  contentMD5 + System.Environment.NewLine + 
							  contentType + System.Environment.NewLine + 
							  date + System.Environment.NewLine + 
							  canonicalizedAmzHeaders +
							  canonicalizedResource;
		
		//Authorization Headers
		System.Text.Encoding ae = new System.Text.UTF8Encoding();
		HMACSHA1 hmac = new HMACSHA1();
		hmac.Key = System.Text.Encoding.ASCII.GetBytes(AWSSecret);
		byte[] bytes = ae.GetBytes(stringToSign);
		byte[] moreBytes = hmac.ComputeHash(bytes);
		string signature = System.Convert.ToBase64String(moreBytes);
		string authorization = "AWS " + AWSKey + ":" + signature;
		*/
		
		
		string topicId = "6";
		string gameId = "000001";
		string AWSKey = "AKIAJSQSEMWZ5H4GVMYQ";
		string AWS_URL = "http://superstarstory.s3.amazonaws.com/Topics"+topicId+"/"+gameId+".png";
		string SERVER_URL = Globals.serverAPIURL + "CreateS3Authorization.php";
		string date = System.DateTime.UtcNow.ToUniversalTime().ToString("r");
		
		WWWForm form = new WWWForm();
		form.AddField("date", date);
		form.AddField("topicId", topicId);
		form.AddField("gameId", gameId);
		
		WWW www = new WWW(SERVER_URL, form);		
		System.DateTime startTime = System.DateTime.Now;
		yield return www;
		
		string authorization = null;
		if (www.error == null)
		{
			authorization = www.text;
			Log.Debug("THe authorization: "+authorization);
			
			//Make a put request on Amazon
			var request = new HTTP.Request("PUT", AWS_URL);
			request.headers.Set("Authorization", authorization);
			request.headers.Set("Date", date);
			request.headers.Set("AWSAccessKeyId", AWSKey);
			request.headers.Set("Content-Type", "image/png");
			request.headers.Set("Content-Length", imageBytes.Length.ToString());
			request.bytes = imageBytes;
			
			
			request.Send();
			while(!request.isDone)
				yield return null;
			
			System.DateTime endTime = System.DateTime.Now;
			System.TimeSpan duration = endTime.Subtract(startTime);
			
			int statusCode = request.response.status;
			bool isOk = request.response.message.Equals("OK");		
			if (statusCode == 200 && isOk)
			{
				Log.Debug("Upload Successfully!");	
				Log.Debug("Time used: "+ duration.TotalSeconds);
			}
			else 
			{
				Log.Debug("Failed");
				Log.Debug(statusCode);
				Log.Debug(request.response.message);
			}
			
		}
		else
		{
			Log.Debug("cannot make the authorization header");	
			Log.Debug(www.error);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

using UnityEngine;
using System.Collections;

public class TopMenuManager : MonoBehaviour {
	
	public GameObject levelText;
	public GameObject levelBar;
	public GameObject energyBar;
	public GameObject energyText;
	public GameObject gemText;
	public GameObject coinText;
	public GameObject energyTimeText;
	
	public float[] apparentValues = new float[4];
	public float[] animationFraction = new float[4]{1f,1f,1f,1f};
	public bool[] changing = new bool[4];
	
	public GameObject energyIncrementPoint, gemIncrementPoint, coinIncrementPoint;
	
	// Use this for initialization
	void Start () {
		
		
		Camera uicam = GameObject.Find("UICamera").GetComponent<Camera>();		
		foreach (UIAnchor anchor in transform.GetComponentsInChildren<UIAnchor>())
		{
			anchor.uiCamera = (Camera) uicam;
		}
		
	
		changing[0] = true;
		changing[1] = true;
		changing[2] = true;
		changing[3] = true;
		
		
		InvokeRepeating("Move", 0f, 0.03f);
		
		Quests.RefreshQuestList();
	}
	
	
	public void GainExpAnimation(int increment)
	{
		// leave a space for gain animation
		//if (Globals.mainPlayer.Exp >= ExpTable.expTable[Globals.mainPlayer.Level+1])
		//	Globals.mainPlayer.Level++;
		
		//Update();
		apparentValues[0] = Globals.mainPlayer.Exp - increment;
		animationFraction[0] = 0f;
		changing[0] = true;
	}
	
	public void GainEnergyAnimation(int increment)
	{
		apparentValues[1] = energyBar.GetComponent<UIFilledSprite>().fillAmount; //1.0f * (Globals.mainPlayer.Energy - increment) / Globals.mainPlayer.MaxEnergy;
		animationFraction[1] = 0f;
		changing[1] = true;
		
		GameObject t = (GameObject) Instantiate(Resources.Load("EnergyIncrement") as GameObject);
		t.transform.parent = energyIncrementPoint.transform;
		t.transform.localPosition = Vector3.zero;
		t.transform.localRotation = Quaternion.identity;
		t.transform.localScale = new Vector3(30f,30f,1f);
		t.GetComponent<IncrementLabel>().text = (increment>0? "+":"")+increment;
	}
	
	public void GainGemsAnimation(int increment)
	{
		// leave a space for gain animation
		apparentValues[2] = Globals.mainPlayer.Gems - increment;
		animationFraction[2] = 0f;
		changing[2] = true;
		
		GameObject t = (GameObject) Instantiate(Resources.Load("GemIncrement") as GameObject);
		t.transform.parent = gemIncrementPoint.transform;
		t.transform.localPosition = Vector3.zero;
		t.transform.localRotation = Quaternion.identity;
		t.transform.localScale = new Vector3(30f,30f,1f);
		t.GetComponent<IncrementLabel>().text = (increment>0? "+":"")+increment;
	}
	
	public void GainCoinsAnimation(int increment)
	{
		// leave a space for gain animation
		apparentValues[3] = Globals.mainPlayer.Coins - increment;
		animationFraction[3] = 0f;
		changing[3] = true;
		
		GameObject t = (GameObject) Instantiate(Resources.Load("CoinIncrement") as GameObject);
		t.transform.parent = coinIncrementPoint.transform;
		t.transform.localPosition = Vector3.zero;
		t.transform.localRotation = Quaternion.identity;
		t.transform.localScale = new Vector3(30f,30f,1f);
		t.GetComponent<IncrementLabel>().text = (increment>0? "+":"")+increment;
	}
	
	
	
	public void ShowIAPPopup1()
	{
		PopupManager.ShowIAPPopup(1, false);
	}
	public void ShowIAPPopup2()
	{
		PopupManager.ShowIAPPopup(2, false);
	}
	public void ShowIAPPopup3()
	{
		PopupManager.ShowIAPPopup(3, false);
	}
	
	public void Move()
	{
		if (Globals.mainPlayer.MaxEnergy == 0) return;
		
		if (changing[0])
		{
			if (Globals.mainPlayer.Level >= ExpTable.expTable.Count-1)
				levelText.GetComponent<UILabel>().text = "MAX";
			else
				levelText.GetComponent<UILabel>().text =  (Globals.mainPlayer.getLevelByExp((int)Mathf.Lerp((int)apparentValues[0], Globals.mainPlayer.Exp, animationFraction[0])) < 10? "0" : "") + Globals.mainPlayer.getLevelByExp((int)Mathf.Lerp((int)apparentValues[0], Globals.mainPlayer.Exp, animationFraction[0]));
		
			levelBar.GetComponent<UIFilledSprite>().fillAmount = ExpTable.InverseLerp((int)Mathf.Lerp((int)apparentValues[0], Globals.mainPlayer.Exp, animationFraction[0]));
		}
		if (changing[1])
		{
			energyBar.GetComponent<UIFilledSprite>().fillAmount = Mathf.Lerp(apparentValues[1], 1.0f*Globals.mainPlayer.Energy/Globals.mainPlayer.MaxEnergy, animationFraction[1]);
			energyText.GetComponent<UILabel>().text = Globals.mainPlayer.Energy +"/"+Globals.mainPlayer.MaxEnergy;
		}
		if (changing[2])
			gemText.GetComponent<UILabel>().text = (int)(Mathf.Lerp(apparentValues[2], Globals.mainPlayer.Gems, animationFraction[2])) + "";
		if (changing[3])
			coinText.GetComponent<UILabel>().text = (int)(Mathf.Lerp(apparentValues[3], Globals.mainPlayer.Coins, animationFraction[3])) + "";
		
		for (int i = 0;i < 4; i++)
		{
			
			
			if (animationFraction[i] >= 1.0f)
			{
				changing[i] = false;
			}
			
			else
				
			{
				animationFraction[i] = Mathf.Min(1.0f, animationFraction[i] + 0.025f);
			}
		}
		
		energyTimeText.GetComponent<UILabel>().text = GetEnergyTimeString();
		
	}
	
	string GetEnergyTimeString()
	{
		if (Globals.mainPlayer.Energy >= Globals.mainPlayer.MaxEnergy) return "";
		
		if (Globals.mainPlayer.EnergyTimer == 0)
			return "06:00";
		
		int m = Globals.mainPlayer.EnergyTimer/60;
		int s = Globals.mainPlayer.EnergyTimer%60;
		
		return "0" + m + ":" + (s<10? "0":"") + s;
	}
	
	public void SettingClicked()
	{
		PopupManager.ShowSettingsPopup(false);
	}
	
	public void GoToInitDBScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.INITDB, true);
	}
}

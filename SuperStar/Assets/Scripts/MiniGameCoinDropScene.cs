using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MiniGameCoinDropScene : MonoBehaviour, PopupManagerCallbackInterface, InputManagerCallback
{
	UICamera cam;
	
	public static bool hasShownTutorial = false;
	
	public GameObject HintsGroupObj;
	public GameObject DescriptionGroupObj;
	public GameObject TapGroupObj;
	
	public GameObject panelObj;
	public GameObject uiCameraObj;
	public GameObject tigerSlotLampAreaObj;
	public GameObject allCoins;
	public GameObject supportBase;
	public List<GameObject> droppedCoins = new List<GameObject>();
	public int[] occupied = new int[8];
	
	public static int tigerSlotQuota;
	public GameObject[] tigerSlots = new GameObject[3];
	
	bool canPayout, canDrop;
	bool showLuckyPriceRewardPopup = false;
	
	public PositionTweener handTweener;
	
	public int payoutLockTimer = 0;
	
	public static Vector2 highestChain = new Vector2(0,0);

	private int totalCoinDropped = 0;

	// Use this for initialization
	void Start()
	{
		Globals.globalCharacter.transform.localPosition = new Vector3(100000f,100000f,100000f);
		
		//InvokeRepeating("TestDropCoins", 0f, 1f);
		canDrop = true;
		handTweener.speed = 0f;
		
		showLuckyPriceRewardPopup=false;
		tigerSlotQuota = 0;
		
		StartCoroutine(LoadPins());
		InvokeRepeating("handLoop", 0f, 0.1f);
		InvokeRepeating("FlashCoins", 0.0f, 1.0f);
		InputManager.SharedManager.SetCallback(this);
		SoundManager.SharedManager.ChangeMusic("SuperstarStory_CoinMinigame");
		
		cam = GameObject.Find("UICamera").GetComponent<UICamera>();
		
		if (Globals.canShowAds)
			MunerisManager.SharedManager.ShowBannerAds();
		
		if (Globals.mainPlayer.NumOfSessions <= 1 && !hasShownTutorial)
		{
			HintsGroupObj.SetActive(true);
			DescriptionGroupObj.SetActive(false);
			hasShownTutorial = true;
		}

		PopupManager.SetCallback(this);		
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.MINIGAMECOINDROP);
	}
	
	void OnDestroy()
	{
		Hashtable param = new Hashtable();
		param["Total"] = totalCoinDropped;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.DropCoin_Total_Count, param);

		totalCoinDropped = 0;


		MunerisManager.SharedManager.RemoveBannerAds();
		MunerisManager.SharedManager.HideBannerAds();
		///MunerisManager.SharedManager.RemoveBannerAds();
		PopupManager.RemoveCallback(this);
		InputManager.SharedManager.RemoveCallback(this);
	}
	
	IEnumerator LoadPins()
	{
		yield return new WaitForSeconds(0.03f);
		GameObject pin = (GameObject) Instantiate(Resources.Load("Pins") as GameObject);
		pin.transform.parent = GameObject.Find("MachineRealBody").transform;
		pin.transform.localPosition = new Vector3(0f, 680f, 0f);
		pin.transform.localRotation = Quaternion.identity;
		pin.transform.localScale = new Vector3(1f,1f,1f);	
		
		
		yield return new WaitForSeconds(0.20f);
		PopupManager.ShowCoinDropRewardPopup(false);
	}
	
	private void FlashCoins()
	{
		if (droppedCoins != null)
		{
			for (int i = 0; i < droppedCoins.Count; i++)
			{
				GameObject coinObj = (GameObject)droppedCoins[i];
				MiniGameCoinDropCoin dropCoin = coinObj.GetComponent<MiniGameCoinDropCoin>();
				dropCoin.FlashCoin();
			}
		}
	}
	
	void handLoop()
	{
		//yield return new WaitForSeconds(0.1f);
		
		payoutLockTimer = Mathf.Max(payoutLockTimer-1,0);
		
		if (canPayout && highestChain.x >= 2 && payoutLockTimer <= 0)
		{
			handTweener.speed = 3f;
		}
		else
		{
			handTweener.speed = 0f;
		}
	}
	
	void DropClicked(int area)
	{
		if (!canDrop)
			return;
		
		if (TapGroupObj.activeSelf)
		{
			TapGroupObj.SetActive(false);
			DescriptionGroupObj.SetActive(true);
		}
		
		
		if (Globals.mainPlayer.Energy < 1)
		{
			PopupManager.ShowNotEnoughCurrencyPopup("Energy", 1, false);
			return;
		}

		totalCoinDropped++;
		
		Globals.mainPlayer.GainEnergy(-1);
		
		SoundManager.SharedManager.PlaySFX("pinball_releaseball");
		
		QuestFlags.SetFlag(4, true);
		QuestFlags.SetFlag(7, QuestFlags.GetFlag(7).intValue+1);
		
		// a timer that avoids cheating (payout before lose)
		payoutLockTimer = 30;
		
		//canPayout = false;
		canDrop = false;
		
		GameObject coin = (GameObject) Instantiate(Resources.Load("coindrop_coin") as GameObject);
		coin.transform.parent = allCoins.transform;
		coin.transform.localPosition = new Vector3(UnityEngine.Random.Range((area-5)*70f-35f, (area-5)*70f+35f), 300f, -2f);
		coin.transform.localRotation = Quaternion.identity;
		coin.transform.localScale = new Vector3(50f,50f,1f);
		
		// add some randomness
		
		coin.rigidbody.velocity = new Vector3((UnityEngine.Random.Range(1,3) == 1? 1:-1) * UnityEngine.Random.Range(0.05f,0.1f) ,0f,0f);
		droppedCoins.Add(coin);
	
		StartCoroutine("StuckTimeout");
	}
	
	public void SensorPassed(int sensorId)
	{
		Log.Debug("sensor " + sensorId +" passed");
		
		//StopCoroutine("StuckTimeout");
		
		if (sensorId == 0)
		{
			// lucky draw
			
			
			tigerSlotQuota++;
			
			if (tigerSlotQuota == 1)
				StartCoroutine(TigerSlotRoutine());
			
			//canDrop = true;
			//RefreshHighestChain();
		}
		else
		{
			occupied[sensorId]++;
			if (occupied[sensorId] >= 2)
			{
				float chain = highestChain.x;
				if (highestChain.y > chain)
					chain = highestChain.y;

				Hashtable param = new Hashtable();
				param["ChainLength"] = chain;
				MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.DropCoin_Fail_ChainLength, param);


				SoundManager.SharedManager.PlaySFX("pinball_wronghole");
				
				StartCoroutine(ClearCoins());
				
				/*for (int i = 1; i <= 7; i++)
				{
					occupied[i] = false;
				}*/
				//RefreshHighestChain();
			}
			else
			{
				SoundManager.SharedManager.PlaySFX("pinball_righthole");
				//occupied[sensorId]++;
				
				//canDrop = true;
				
				//RefreshHighestChain();
			}
		}
		
		
		RefreshHighestChain();
	}
	
	public void RefreshHighestChain()
	{
		highestChain = Vector2.zero;
		int chain = 0;
		for (int i = 1; i <= 7+1; i++)
		{
			if (i <= 7 && occupied[i] >= 1)
			{
				if (occupied[i] == 1)
					chain++;
				else
				{
					highestChain = Vector2.zero;
					return;
				}
			}
			else
			{
				// chain break
				if (chain >= highestChain.x)
				{
					highestChain.y = highestChain.x;
					highestChain.x = chain;
				}
				else if (chain >= highestChain.y)
				{
					highestChain.y = chain;
				}
				
				chain = 0;
			}
		}
		
		/*
		for (int i = 1; i <= 7; i++)
		{
			if (occupied[i] >= 2)
			{
				
			}
		}
		*/
		
		Log.Debug("refreshing highest chain: " + highestChain.x +","+highestChain.y);
		
		if (highestChain.x >= 2 || highestChain.y >= 2)
		{
			canPayout = true;
		}
		/*
		if (highestChain.x >= 7)
		{
			PayoutPressed();
		}*/
		
	}
	
	IEnumerator StuckTimeout()
	{
		yield return new WaitForSeconds(0.89f);
		
		canDrop = true;
	}
	
	public IEnumerator ClearCoins()
	{
		yield return new WaitForSeconds (0.2f);
		
		
		supportBase.rigidbody.collider.isTrigger = true;
		
	
		
		
		yield return new WaitForSeconds (1.3f);
		
		/*for (int i = 1; i <= 7; i++)
		{
			occupied[i] = false;
		}*/
		
		//highestChain = new Vector2(0f,0f);
		
		supportBase.rigidbody.collider.isTrigger = false;
		
		
		
		//canDrop = true;
	}
	
	IEnumerator GetPayout()
	{
		QuestFlags.SetFlag(16, (int)highestChain.x);
		
		PopupManager.ShowRewardPopup(GetRewardByChain((int)highestChain.x),false);
		
		// callback: load takeover
		//StartCoroutine(WaitRewardEndRoutine());
	
		yield return null;
	}	
	
	Reward GetRewardByChain(int chain)
	{
		switch (chain)
		{
			case 2: return new Reward(999, 1, 10, 0, 0, 0, "", new Skill(new int[]{0,0,0,0,0}));
			case 3: return new Reward(999, 1, 0, 0, 0, 2, "", new Skill(new int[]{0,0,0,0,0}));
			case 4: return new Reward(999, 1, 0, 0, 50, 0, "", new Skill(new int[]{0,0,0,0,0}));
			case 5: return new Reward(999, 1, 1000, 0, 0, 0, "", new Skill(new int[]{0,0,0,0,0}));
			case 6: return new Reward(999, 1, 0, 5, 0, 0, "", new Skill(new int[]{0,0,0,0,0}));
			case 7: return new Reward(999, 1, 0, 50, 0, 0, "", new Skill(new int[]{0,0,0,0,0}));
			default: return new Reward(999, 1, 0, 0, 0, 0, "", new Skill(new int[]{0,0,0,0,0}));
		}
	}
	
	void PayoutPressed()
	{
		if (!canPayout || payoutLockTimer > 0) 
			return;
		else if (highestChain.x < 2 && highestChain.y < 2)
			return;

		float chain = highestChain.x;
		if (highestChain.y > chain)
			chain = highestChain.y;

		//PPA
		if (chain >= 4)
			MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.Match4CoinsInSuperstarCoins);

		Hashtable param = new Hashtable();
		param["ChainLength"] = chain;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.DropCoin_Payout_ChainLength, param);
		
		canPayout = false;
		
		Log.Debug("Payout! highest chains:"+highestChain.x+","+highestChain.y);
		
		StartCoroutine(GetPayout());
		
		
		/*for (int i = 1; i <= 7; i++)
		{
			occupied[i] = false;
		}*/
		//occupied = new int[8];
		highestChain = Vector2.zero;
		StartCoroutine(ClearCoins());
		//RefreshHighestChain();
	}
	
	
	IEnumerator TigerSlotRoutine()
	{
		TigerSlotLampController controller = tigerSlotLampAreaObj.GetComponent<TigerSlotLampController>();
		
		while(!showLuckyPriceRewardPopup && tigerSlotQuota > 0)
		{
			//Switch on the lamp effect			
			if (!controller.IsSwitchOnLampEffect())
				controller.SwitchLampEffect(true);
			
			PositionTweener[] slotTweeners = new PositionTweener[3];
		
			for (int i = 0; i < 3; i++)
			{
				slotTweeners[i] = tigerSlots[i].GetComponent<PositionTweener>();
			}
			
			SoundManager.SharedManager.PlayLoopSFX("pinball_slot_spinning_loop");
			
			// accelerate
			while (slotTweeners[0].speed < 1.0f)
			{
				for (int i = 0; i < 3; i++)
				{
					slotTweeners[i].speed = Mathf.Min(1f, slotTweeners[i].speed+0.01f);
				}
				
				yield return new WaitForSeconds(0.01f);
			}
			
			yield return new WaitForSeconds(UnityEngine.Random.Range(1f, 2f));
			
			
			yield return StartCoroutine(Decelerate(slotTweeners[0]));
			
			//yield return new WaitForSeconds(UnityEngine.Random.Range(0.2f, 0.6f));
			
			yield return StartCoroutine(Decelerate(slotTweeners[1]));
			
			//yield return new WaitForSeconds(UnityEngine.Random.Range(0.2f, 0.6f));
			
			yield return StartCoroutine(Decelerate(slotTweeners[2]));
			
			SoundManager.SharedManager.StopLoopSFX();
			
			int p1,p2,p3;
			
			p1 = rounding(Mathf.Abs((int) tigerSlots[0].transform.localPosition.y));
			p2 = rounding(Mathf.Abs((int) tigerSlots[1].transform.localPosition.y));
			p3 = rounding(Mathf.Abs((int) tigerSlots[2].transform.localPosition.y));
			
			if (p1 == p2 && p1 == p3)
			{
				// win a prize!
				
				SoundManager.SharedManager.PlaySFX("pinball_slot_win");
				
				//Switch on teh getPrize effect
				controller.SwitchGetPrizeLampEffect(true);
				yield return StartCoroutine(LuckyPrizeRoutine(p1));
			}			
			else
			{
				tigerSlotQuota--;
				if (tigerSlotQuota > 0)
				{
					yield return new WaitForSeconds(0.5f);
					//StartCoroutine(TigerSlotRoutine());
				}
			}
		}
		
		if (controller.IsSwitchOnLampEffect())
			controller.SwitchLampEffect(false);
	}
	
	int rounding(int input)
	{
		Log.Warning("input?"+input);
		
		if (-3 <= input && input <= 3)
			return 0;
		
		else if (97 <= input && input <= 103)
			return 1;
		
		else if (197 <= input && input <= 203)
			return 2;
		
		else if (297 <= input && input <= 303)
			return 0;
		
		return 0;
	}
	
	IEnumerator Decelerate(PositionTweener pt)
	{
		
		while (pt.speed > 0.05f)
		{
			pt.speed = Mathf.Max(0.05f, pt.speed - 0.01f);
			
			yield return new WaitForSeconds(0.01f);
		}
		
		while (pt.speed > 0f)
		{
			int  p = ((int) pt.transform.localPosition.y)%100;
			if (p <= 2 || p >= 98)
			{
				pt.transform.localPosition = new Vector3(pt.transform.localPosition.x,
														(int) pt.transform.localPosition.y,
														pt.transform.localPosition.z);
				pt.speed = 0f;
				break;
			}
			yield return new WaitForSeconds(0.01f);
		}
		
		yield return null;
	}
	
	IEnumerator LuckyPrizeRoutine(int p)
	{
		yield return new WaitForSeconds(0.5f);
		
		Reward r = null;
		switch (p)
		{
			// star
			case 0:
			case 3:
				r = new Reward(999, 1, 0,0,50,0,"",new Skill(new int[]{0,0,0,0,0}));
			break;
			
			// energy
			case 1:
				r = new Reward(999, 1, 0,0,0,2,"",new Skill(new int[]{0,0,0,0,0}));
			break;
			
			// coin
			case 2:
				r = new Reward(999, 1, 10 ,0, 0, 0, "",new Skill(new int[]{0,0,0,0,0}));
			break;			
			
		}

		PopupManager.ShowRewardPopup(r,false);
		showLuckyPriceRewardPopup = true;
	}
	
	void BackToStreetScene()
	{
		if (MiniGameCoinDropScene.highestChain.x >= 1 || MiniGameCoinDropScene.tigerSlotQuota > 0)
			PopupManager.ShowSureQuitPopup(Scenes.MINIGAME, false);
		else
		{
			SoundManager.SharedManager.StopLoopSFX();
			SceneManager.SharedManager.LoadScene(Scenes.MINIGAME, false);
		}
	}
	
	void DropClicked1()
	{
		DropClicked(1);
	}
	void DropClicked2()
	{
		DropClicked(2);
	}
	void DropClicked3()
	{
		DropClicked(3);
	}
	void DropClicked4()
	{
		DropClicked(4);
	}
	void DropClicked5()
	{
		DropClicked(5);
	}
	void DropClicked6()
	{
		DropClicked(6);
	}
	void DropClicked7()
	{
		DropClicked(7);
	}
	void DropClicked8()
	{
		DropClicked(8);
	}
	void DropClicked9()
	{
		DropClicked(9);
	}
	
	void QuestionClicked()
	{
		PopupManager.ShowCoinDropRewardPopup(false);
	}
	
	#region Tap + Description Group	
	public void OnDescriptionTweenAlphaFinished()
	{
		DescriptionGroupObj.SetActive(false);
		TapGroupObj.SetActive(false);
		HintsGroupObj.SetActive(false);
	}
	#endregion
	
	#region PopupManager Callback
	public void OnPopupManagerHidePopup(GameObject popup)
	{
		Log.Debug("On PopupManager Hide Popup");
		if (popup.name.Equals("RewardPopup"))
		{		
			Log.Debug("On PopupManager RewardPopup");
			if (showLuckyPriceRewardPopup)
			{
				//Unblock the spinning of the remaining lucky price
				showLuckyPriceRewardPopup = false;
				
				//Switch off the luckyPrize effect
				TigerSlotLampController controller = tigerSlotLampAreaObj.GetComponent<TigerSlotLampController>();
				controller.SwitchGetPrizeLampEffect(false);
				
				tigerSlotQuota--;
				if (tigerSlotQuota > 0)
				{
					StartCoroutine(TigerSlotRoutine());
				}
			}
		}
	}
	
	public void OnPopupManagerShowPopup(GameObject popup){}
	#endregion
	
	#region InputManager Callback
#if UNITY_EDITOR
	public void OnInputManagerMouseClicked(Vector3 clickedPos)
	{
		if (uiCameraObj != null)
		{
			Vector3 touchPos = uiCameraObj.camera.ScreenToWorldPoint(clickedPos);
			
			//Create the touch effect
			GameObject touchEffectObj = Instantiate(Resources.Load("Prefab/MinigameDropCoinTouchEffect")) as GameObject;
			if (touchEffectObj != null)
			{
				touchEffectObj.transform.parent 			= panelObj.transform;
				touchEffectObj.transform.position 			= touchPos;
				touchEffectObj.transform.localEulerAngles	= Vector3.zero;
				touchEffectObj.transform.localScale			= new Vector3(1, 1, 1);
				touchEffectObj.SetActive(true);
			}
		}
	}
#endif
	
	public void OnInputManagerTouchBegan(Touch touch)
	{
		if (uiCameraObj != null)
		{
			Vector3 touchPos = uiCameraObj.camera.ScreenToWorldPoint(touch.position);
			
			//Create the touch effect
			GameObject touchEffectObj = Instantiate(Resources.Load("Prefab/MinigameDropCoinTouchEffect")) as GameObject;
			if (touchEffectObj != null)
			{
				touchEffectObj.transform.parent 			= panelObj.transform;
				touchEffectObj.transform.position	 		= touchPos;
				touchEffectObj.transform.localEulerAngles	= Vector3.zero;
				touchEffectObj.transform.localScale			= new Vector3(1, 1, 1);
				touchEffectObj.SetActive(true);
			}
		}
	}
	
	public void OnInputManagerTouchEnded(Touch touch){}	
	public void OnInputManagerFemaleCharacterTouched(){}
	public void OnInputManagerBoyfriendChacaterTouched(){}
	public void OnInputManagerBackPressed(){}
	#endregion
}


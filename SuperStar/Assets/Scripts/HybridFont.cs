using UnityEngine;
using System.Collections;

public class HybridFont : MonoBehaviour
{
	public string originalString;
	public string localizationKey;
	
	public GameObject originalObject;
	public GameObject sysFontObject;

	// Use this for initialization
	void Start ()
	{
		Refresh();
	}
	
	public void Refresh()
	{
		if (originalString.Equals(Language.Get(localizationKey)))
		{
			originalObject.SetActive(true);
			sysFontObject.SetActive(false);
		}
		else
		{
			originalObject.SetActive(false);
			sysFontObject.SetActive(true);
		}
	}
}


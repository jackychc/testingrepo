using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;

public class PosterManager : MonoBehaviour
{
	public static bool downloadedPosters = false;
	public static bool[] posterClicked = new bool [3];
	public GameObject[] posterLocations = new GameObject[3];
	public UITexture[] sprite = new UITexture[3]; 
	
	private bool isCheckingDLC = false;
	
	Poster[] targetPoster = new Poster[3];
	
	void Start()
	{
		if (!downloadedPosters)
		{
			Log.Debug("Start Download Posters");
			StartCoroutine(ReallyStart());
		}
		else
		{
			Log.Debug("Start Alternate Posters");
			StartCoroutine(AlternateStart());
		}
	}
	IEnumerator AlternateStart()
	{	
		// skipped download from server
			yield return StartCoroutine(LoadPosters());
		
			downloadedPosters = true;
	}
	
	IEnumerator ReallyStart()
	{	
		//yield return StartCoroutine(LoadPosters()); // load some local posters first
		
		yield return StartCoroutine(GetPosterFromServer());
		
		yield return StartCoroutine(LoadPosters());
		
		downloadedPosters = true;
	}
	
	public IEnumerator GetPosterFromServer()
	{
		Dictionary<string, string> kvp = null;
		if (Globals.isDebugMode)
		{
			kvp = new Dictionary<string, string>();
			kvp.Add("isDebugMode", "1");
		}

		string url = Globals.serverAPIURL + "GetPosters.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response 			= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			//yield return StartCoroutine(HTTPPost.Post("GetFriends", kvp, result => response = result));
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					if (al[1] != null)
					{
						ArrayList posterList =  new ArrayList(((Hashtable) al[1]).Values);
						
						List<Poster> setMe = new List<Poster>();
						//Log.Debug("count:"+playerList.Count);
						for (int i = 0; i < posterList.Count; i++)
						{
							Hashtable posterHT = (Hashtable)posterList[i];
							Poster p = new Poster(
								Int32.Parse(posterHT["posterId"].ToString()),
								posterHT["posterPeriod"].ToString(),
								Int32.Parse(posterHT["posterProbability"].ToString()),
								posterHT["posterRedirectScene"] != null? posterHT["posterRedirectScene"].ToString() : "",
							    posterHT["rewardId"] != null? Int32.Parse(posterHT["rewardId"].ToString()) : -1,
								posterHT["posterURL"] != null? posterHT["posterURL"].ToString().Replace("\\", ""): "",
								Int32.Parse(posterHT["posterLocation"].ToString()),
								posterHT["dlcId"] != null? Int32.Parse(posterHT["dlcId"].ToString()) : 0
							);
		
							setMe.Add(p);



							// save to local
							
							WWW imagewww = new WWW(p.posterURL);
							yield return imagewww;
							if (imagewww.error != null)
							{
								Log.Debug("Poster Cannot open the file with posterId: " + p.posterId);
							}
							else
							{
								if (imagewww.bytes != null)
								{
									File.WriteAllBytes(getPosterPath(p.posterId), imagewww.bytes);
								}
								else
								{
									Log.Debug("Poster Cannot fetch the poster texture from file with posterId: " + p.posterId);
								}
							}
							//if (!Directory.Exists(getPosterDirectory()))
								//Directory.CreateDirectory(getPosterDirectory());
							imagewww.Dispose();
							imagewww = null;
						}
						
						//setMe.Sort((x,y) => {return x.votingRank.CompareTo(y.votingRank);});
						
						Posters.postersList = setMe;
						Posters.Save();
					}
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("GetPosters", null, result => response = result));
	}
							
	public string getPosterDirectory()
	{
		return Application.persistentDataPath + Path.DirectorySeparatorChar + "poster" + Path.DirectorySeparatorChar;
	}
	
	public string getPosterPath(int id)
	{
		//return UtilsHelper.GetWriteableDirectory() + Path.DirectorySeparatorChar + "poster" + /*Path.DirectorySeparatorChar +*/ id +".png";
		return Application.persistentDataPath + Path.DirectorySeparatorChar + /*"poster" + Path.DirectorySeparatorChar +*/ id +".png";
	}
	
	public Poster getOnePosterOfLocation(int loc)
	{
		List<Poster> pool = new List<Poster>();
		//Poster returnMe = null;

		Log.Debug("Num of posters: " + Posters.postersList.Count);
		foreach (Poster p in Posters.postersList)
		{
			if (p.posterLocation == loc)
			{
				// check date
				if (p.period == null || p.period.Equals(""))
					continue;
				
				string[] dates = p.period.Split('|');
				
				if (dates.Length != 2)
					continue;
				Log.Debug("Check period");
				
				//Log.Debug("[PosterManager] startTime: " + dates[0]);
				//Log.Debug("[PosterManager] endTime: " + dates[1]);
				DateTime now 		= DateTime.UtcNow;
				DateTime startTime 	= DateTime.Parse(dates[0]);
				DateTime endTime	= DateTime.Parse(dates[1]);
				
				startTime	= new DateTime(startTime.Year, startTime.Month, startTime.Day, 23, 59, 59);
				endTime		= new DateTime(endTime.Year, endTime.Month, endTime.Day, 23, 59, 59);

				Log.Debug("Posters location: " + loc);
				Log.Debug("Posters start time: " + startTime.ToString());
				Log.Debug("Posters end Time: " + endTime.ToString());
				Log.Debug("Current time: " + now.ToString());
				
				if ((now - startTime).TotalSeconds > 0 && (endTime - now).TotalSeconds >= 0)
				{
					for (int i = 0; i < p.probability; i++)
						pool.Add(p);
				}
			}
		}
		
		if (pool.Count > 0)
		{
			Log.Debug("Have Posters");
			return pool[UnityEngine.Random.Range(0, pool.Count)];
		}

		Log.Debug("NO Posters");
		return null;
	}
	
	public IEnumerator LoadPosters()
	{
		for (int i = 0 ; i < 3; i++)
		{
			sprite[i].gameObject.SetActive(false);
		}
		
		for (int i = 0; i < 3; i++)
		{
			targetPoster[i] = getOnePosterOfLocation(i+1);
			
			if (targetPoster[i] != null)
			{
				Poster p 			= targetPoster[i];
				bool canShowPoster	= true;

				if (p.redirectScene.Trim().StartsWith("dlc"))
				{
					DownloadManager dlm = DownloadManager.SharedManager;
					if (dlm != null)
					{
						if (!dlm.isDownloading)
						{
							bool hasDLC = false;
							yield return StartCoroutine(dlm.CheckDLC(true, -1, 0, result=> hasDLC = result));

							if (!hasDLC)
								canShowPoster = false;
						}
					}
				}

				if (canShowPoster)
				{
					yield return StartCoroutine(LoadImage(p));

					sprite[i].gameObject.SetActive(true);
					posterLocations[i].SetActive(true);

					//Disable the Default Posters
					Transform demo = posterLocations[i].transform.FindChild("Panel/DemoObject");				
					if (demo != null)
						demo.gameObject.SetActive(false);

					Transform effectBox = posterLocations[i].transform.FindChild("Panel/EffectBox");				
					if (effectBox != null)
						effectBox.GetComponent<UISpriteAlphaTweener>().mode = UISpriteAlphaTweener.TWEENMODE.BACK_AND_FORTH;
				}
			}
		}
	}
		
	public IEnumerator LoadImage(Poster p)
	{
		WWW loader = new WWW("file://" + getPosterPath(p.posterId));
		yield return loader;
		
		if (loader.error != null)
		{
			Log.Debug("Poster Cannot fetch open the image from file with posterId: " + p.posterId);
		}
		else
		{
			if (loader.texture != null)
			{
				Texture2D texture = loader.texture;
				Shader shader = Shader.Find("Unlit/Texture");
				if(shader != null)
				{
					 Material material = new Material(shader);
					 material.mainTexture = texture;
					 sprite[p.posterLocation-1].material = material;
					 sprite[p.posterLocation-1].color = Color.white;
					 sprite[p.posterLocation-1].MakePixelPerfect();
				}
			}
			else
			{
				Log.Debug("Poster Cannot fetch the poster texture from file with posterId: " + p.posterId);	
			}
		}

		loader.Dispose();
		loader = null;
	}
	
	public void Billboard1Clicked()
	{
		BillboardNClicked(0);
	}
	public void Billboard2Clicked()
	{
		BillboardNClicked(1);
	}
	public void Billboard3Clicked()
	{
		BillboardNClicked(2);
	}
	
	public void BillboardNClicked(int n)
	{
		if (targetPoster[n] != null)
		{
			if (!posterClicked[n])
			{
				if (Rewards.getReward(targetPoster[n].rewardId) != null)
				{
					posterClicked[n] = true;
				}
				BillboardClicked(targetPoster[n]);
			}
		}
		else
		{
			//Log.Warning("poster null! " + n);

			//!!!
			HardAction(n);
		}
	}

	void HardAction(int n)
	{
		switch(n)
		{
			case 0: VisitShop.shop = Shops.GetShopById(2); SceneManager.SharedManager.LoadScene(Scenes.SHOP, true); break;
			case 1: VisitShop.shop = Shops.GetShopById(3); SceneManager.SharedManager.LoadScene(Scenes.SHOP, true); break;
			case 2: VisitShop.shop = Shops.GetShopById(4); SceneManager.SharedManager.LoadScene(Scenes.SHOP, true); break;
			default: break;
		}
	}
	
	public void BillboardClicked(Poster p)
	{
		//Log Flurry Event
		Hashtable param = new Hashtable();
		param["Location"] = p.posterLocation;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Street_Click_Posters, param);

		/*
		if (p.dlcId > Globals.currentDLCVersion)
		{
			PopupManager.ShowInformationPopup("Please update and enjoy the latest contents!", false);
		}
		*/
		if (p.dlcId <= Globals.currentDLCVersion)
		{
			// poster reward is DEPRECIATED
			/*if (Rewards.getReward(p.rewardId) != null)
			{
				PopupManager.ShowRewardPopup(Rewards.getReward(p.rewardId), false);
			}*/
			
			if (p.redirectScene.Trim().StartsWith("Shop"))
			{
				int shopNo = Int32.Parse(p.redirectScene.Trim().Remove(0,4));
				
				if (Shops.GetShopById(shopNo) != null)
				{
					if (shopNo <= 4)
					{
						VisitShop.shop = Shops.GetShopById(shopNo);
						SceneManager.SharedManager.LoadScene(Scenes.SHOP, true);
					}
					else
					{
						Shop shop = Shops.GetShopById(shopNo);
						PopupManager.ShowDealPopup(shop, false);
					}
				}
				else
				{
					Log.Warning("Posters the shop id is not valid!!");
				}
			}
			else if (p.redirectScene.Trim().StartsWith("dlc"))
			{
				if (!isCheckingDLC)
					StartCoroutine(CheckDLC());
			}
			else if (!p.redirectScene.Trim().Equals(""))
			{
				Scenes sceneIndex = (Scenes)Enum.Parse(typeof(Scenes), p.redirectScene.ToUpperInvariant());
				SceneManager.SharedManager.LoadScene(sceneIndex, true);
			}
		}
		else
		{
			DownloadManager.SharedManager.AskForDLCUpgrade(false, p.dlcId, 0, 2, true);
		}
	}
	
	private IEnumerator CheckDLC()
	{
		isCheckingDLC = true;
		DownloadManager dlm = DownloadManager.SharedManager;
		if (!dlm.isDownloading)
		{
			bool has = false;
			yield return StartCoroutine(dlm.CheckDLC(true, -1, 0, result=> has = result));
			
			/*if (!dlm.haveDLC)
			{
				PopupManager.ShowInformationPopup(Language.Get("dlc_latestVersion"), false);
				isCheckingDLC = false;
				yield break;
			}
			else*/

			if (dlm.haveDLC)
			{
				DownloadManager.SharedManager.AskForDLCUpgrade(true, -1, 0);
			}
		}
		else
		{
			DownloadManager.SharedManager.AskForDLCProgress(true, -1, 0);
		}

		isCheckingDLC = false;
	}
}


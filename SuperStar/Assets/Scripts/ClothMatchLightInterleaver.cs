using UnityEngine;
using System.Collections;

public class ClothMatchLightInterleaver : MonoBehaviour {
	
	
	//public string[] spriteNames;
	int current;
	public float updateInterval = 0.3f;
	public UISprite sprite;
	
	// Use this for initialization
	void Start () {
		//sprite = transform.GetComponent<UISprite>();
		current = 0;
		
		StartCoroutine(NextSprite());
	}
	
	IEnumerator NextSprite()
	{
		//sprite.spriteName = spriteNames[current];
		
		sprite.gameObject.SetActive(current == 0);
		
		//if (sprite.spriteName.Equals("glow_a")
		
		yield return new WaitForSeconds(updateInterval);
		
		current = (current+1)%(2);
		
		StartCoroutine(NextSprite());
	}
}

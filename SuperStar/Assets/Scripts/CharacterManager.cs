using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Object=UnityEngine.Object;
using Random=UnityEngine.Random;

public class CharacterManager : MonoBehaviour {
	
	Player profile;
	Character character;
	public List<AnimationQueueItem> animationQueue = new List<AnimationQueueItem>();
	
	private Dictionary<string , string> previousData = new Dictionary<string, string>(); // eg: [shoes, ["1", shoeTransform]];
	
	
	private Transform[] hips;

	private Dictionary<string , SkinnedMeshRenderer> targetSmr = new Dictionary<string, SkinnedMeshRenderer>();
	
	public delegate void OnCharacterManagerAnimationEnd(string animationName);
	public OnCharacterManagerAnimationEnd onAnimEnd;
	
	// Use this for initialization
	void Start ()
	{

	}
	
	//Check for Facial Items + bra + panties
	public static List<ItemSubcategory> CheckForMissingStandardSubcategory(List<Item> itemLists)
	{
		List<ItemSubcategory> missingSubcats = new List<ItemSubcategory>();
		foreach (ItemSubcategories.Id subcatId in Enum.GetValues(typeof(ItemSubcategories.Id)))
		{
			if (!subcatId.Equals(ItemSubcategories.Id.ACCESSORIES) &&
				!subcatId.Equals(ItemSubcategories.Id.COAT) &&
				!subcatId.Equals(ItemSubcategories.Id.SOCKS) &&
				!subcatId.Equals(ItemSubcategories.Id.LEGGINGS) &&
				!subcatId.Equals(ItemSubcategories.Id.HANDBAGS) &&
				!subcatId.Equals(ItemSubcategories.Id.HAT) &&
				!subcatId.Equals(ItemSubcategories.Id.GLASSES) &&
				!subcatId.Equals(ItemSubcategories.Id.NECKLACE) &&
				!subcatId.Equals(ItemSubcategories.Id.PHOTOSHOTBG) &&
				!subcatId.Equals(ItemSubcategories.Id.SHOES) &&
				!subcatId.Equals(ItemSubcategories.Id.DRESS) &&
				!subcatId.Equals(ItemSubcategories.Id.SKIRT) &&
				!subcatId.Equals(ItemSubcategories.Id.TROUSERS) &&
				!subcatId.Equals(ItemSubcategories.Id.TOP))
			{
				bool hasSubcat = false;
				for (int i = 0; i < itemLists.Count; i++)
				{
					Item item = itemLists[i];
					if (item.subCategoryId == (int)subcatId)
						hasSubcat = true;
				}
				
				if (!hasSubcat)
					missingSubcats.Add(ItemSubcategories.getSubcategory((int)subcatId));
			}
		}
		
		return missingSubcats;
	}
	
	public static Item GenerateRandomDefaultWearingItemForSubcategory(ItemSubcategory subcat)
	{
		Item item = null;
		if (subcat != null)
		{
			List<Item> items = Items.getItemsFromSubcategory(subcat.subcategoryid);
			int randIndex = Random.Range(0, items.Count - 1);
			return items[randIndex];
		}
		
		return item;
	}	
	
	public static string GenerateRandomDefaultFacialItemString()
	{
		string wearingItemStr = "";
		if (Items.allItems != null && Items.allItems.Count > 0)
		{
			foreach (ItemSubcategories.Id subcatId in Enum.GetValues(typeof(ItemSubcategories.Id)))
			{
				if (!subcatId.Equals(ItemSubcategories.Id.ACCESSORIES) &&
					!subcatId.Equals(ItemSubcategories.Id.COAT) &&
					!subcatId.Equals(ItemSubcategories.Id.SOCKS) &&
					!subcatId.Equals(ItemSubcategories.Id.LEGGINGS) &&
					!subcatId.Equals(ItemSubcategories.Id.HANDBAGS) &&
					!subcatId.Equals(ItemSubcategories.Id.HAT) &&
					!subcatId.Equals(ItemSubcategories.Id.GLASSES) &&
					!subcatId.Equals(ItemSubcategories.Id.NECKLACE) &&
					!subcatId.Equals(ItemSubcategories.Id.PANTIES) &&
					!subcatId.Equals(ItemSubcategories.Id.BRA) &&
					!subcatId.Equals(ItemSubcategories.Id.PHOTOSHOTBG) &&
					!subcatId.Equals(ItemSubcategories.Id.SHOES) &&
					!subcatId.Equals(ItemSubcategories.Id.DRESS) &&
					!subcatId.Equals(ItemSubcategories.Id.SKIRT) &&
					!subcatId.Equals(ItemSubcategories.Id.TROUSERS) &&
					!subcatId.Equals(ItemSubcategories.Id.TOP))
				{
					List<Item> items = Items.getPurchasedItemsFromSubcategory((int)subcatId, Globals.mainPlayer);

					int randIndex = Random.Range(0, items.Count - 1);
					wearingItemStr += items[randIndex].itemId + ",";
				}
			}
			
			wearingItemStr = wearingItemStr.TrimEnd(',');
		}
		
		return wearingItemStr;
	}
	
	public static List<Item> GenerateRandomDefaultWearingItem()
	{
		string randomStr = GenerateRandomDefaultWearingItemString();
		List<Item> items = Items.stringToItemList(randomStr);
		
		return items;
	}
	
	public static string GenerateRandomDefaultWearingItemString()
	{
		string wearingItemStr = GenerateRandomDefaultFacialItemString();
		if (!wearingItemStr.Equals(""))
		{
			wearingItemStr += ",";
		}
		
		wearingItemStr 	+= 	  "201," + //bra
							  "215," + //panties
							  "186," + //handbag
							  "188," + //hat
							  "339," + //Shoes
							  "374," + //socks
							  "381," + //Leggings
							  "414," + //necklace
							  "665," + //glasses
			(UnityEngine.Random.Range(1,3) == 1? "180,278,319" //Top + skirts 
											   : "192,190,105" //Dress);
						);			
				
		return wearingItemStr;
	}	
	
	/*public void GenerateCharacter(Player player)
	{
		GenerateCharacter(player,false);
		
	}*/
	
	public void PartialGenerateCharacter(Player player, List<ItemSubcategory> pSubcat)
	{
		GenerateCharacter(player);
	}
	
	/*public void dum()
	{
		profile = player;
		//data = new Dictionary<string, Dictionary<string,Transform>>(); // eg: [shoes, ["1", shoeTransform]];
		
		//assume the character is generated before
		
		//if (targetSmr == null)
		//	targetSmr = new Dictionary<string, SkinnedMeshRenderer>();

		List<ItemSubcategory> partsList = Characters.FEMALE.getCharacterParts();
		
		
		foreach (ItemSubcategory part in partsList)
		{	
			string partName = part.subcategoryName;
			SkinnedMeshRenderer smr = null;
			
			if (pSubcat.Contains(part))
			{
				if (transform.FindChild(part.subcategoryName) != null)
				{
					Destroy(transform.FindChild(part.subcategoryName).gameObject);
				}	
			
				GameObject partObj = new GameObject();
				partObj.name = partName;
				partObj.transform.parent = transform;
				
				if (targetSmr.ContainsKey(partName))
					targetSmr[partName] = partObj.AddComponent<SkinnedMeshRenderer>();
				else
					targetSmr.Add(partName , partObj.AddComponent<SkinnedMeshRenderer>());
			}
			else if (Items.isNoneItem(profile.wearingItemOfSubcategory(part)))
			{
				if (transform.FindChild(part.subcategoryName) != null)
				{
					Destroy(transform.FindChild(part.subcategoryName).gameObject);
				}
				
				if (targetSmr.ContainsKey(partName))
					targetSmr.Remove(partName);
					
			}
			else
			{
				if (!targetSmr.ContainsKey(partName))
					targetSmr.Add(partName , transform.FindChild(part.subcategoryName).GetComponent<SkinnedMeshRenderer>());
			}
			
			
		}

		hips = transform.GetComponentsInChildren<Transform>();
		
		// *********************************** start constructing model************************************

		foreach (ItemSubcategory subcat in partsList)
		{
			
			if (profile.wearingItemOfSubcategory(subcat) != null)
			{
				Item it =profile.wearingItemOfSubcategory(subcat);
				
				if (pSubcat.Contains(subcat))
				{
					if (ItemConflicts.NeedToAlternative(it, profile))
					{
						ChangePart(subcat.subcategoryName, it.meta+"_b", it.texMeta, it.colorMeta );
					}
					else
					{
						ChangePart(subcat.subcategoryName, it.meta, it.texMeta, it.colorMeta );
					}
				}
				
				else if (ItemConflicts.NeedToAlternative(it, profile))
				{
					ChangePart(subcat.subcategoryName, it.meta+"_b", it.texMeta, it.colorMeta );
				}
			}
			
			
		}
		
		transform.animation.playAutomatically = false;
		transform.animation.wrapMode = WrapMode.Once;

		InsertAnimation("pose01");
	}*/
	
	
	public void GenerateCharacter(Player player)
	{
		StartCoroutine(EnumGenerateCharacter(player));
	}
	
	//public void GenerateCharacter(Player player)
	public IEnumerator EnumGenerateCharacter(Player player)
	{
		yield return StartCoroutine(EnumGenerateCharacter(player, true));
	}
	
	public IEnumerator EnumGenerateCharacter(Player player, bool WithUnload)
	{
		profile = player;
		//data = new Dictionary<string, Dictionary<string,Transform>>(); // eg: [shoes, ["1", shoeTransform]];
		

		List<ItemSubcategory> partsList = Characters.FEMALE.getCharacterParts();
		
	
		
		foreach (ItemSubcategory part in partsList)
		{
			string partName = part.subcategoryName;
			
			if (!targetSmr.ContainsKey(partName))
			{
				GameObject partObj = new GameObject();
				partObj.name = partName;
				partObj.transform.parent = transform;
			
				targetSmr.Add(partName , partObj.AddComponent<SkinnedMeshRenderer>());
				
			}
			
			/*if (transform.FindChild(part.subcategoryName) != null)
			{
				Destroy(transform.FindChild(part.subcategoryName).gameObject);
			}*/
		}	
		hips = transform.GetComponentsInChildren<Transform>();
		//Transform[] hips = transform.GetComponentsInChildren<Transform>();
		
		// *********************************** start constructing model************************************

		foreach (ItemSubcategory subcat in partsList)
		{
			string partName = subcat.subcategoryName;
			Item i = profile.wearingItemOfSubcategory(subcat);
			
			//if (previousData.ContainsKey(partName) && !previousData[partName].Equals(i.itemId+""))
			{
			
				if (i != null && !Items.isNoneItem(i) )
				{
					Item it =profile.wearingItemOfSubcategory(subcat);
					
					if (ItemConflicts.NeedToAlternative(it, profile))
					{
						//Log.Debug("need to alternative: " + it.itemId);
						// !?
						
						if (!previousData.ContainsKey(partName) || !previousData[partName].Equals(i.itemId+"_b"))
							yield return StartCoroutine(ChangePart(subcat.subcategoryName, it.meta+"_b", it.texMeta, it.colorMeta ));
						
						if (!previousData.ContainsKey(partName))
							previousData.Add(partName, i.itemId+"_b");
						else
							previousData[partName] = i.itemId+"_b";
					}
					else
					{
						//Log.Debug("NO need to alternative: " + it.itemId);
						if (!previousData.ContainsKey(partName) || !previousData[partName].Equals(i.itemId+""))
							yield return StartCoroutine(ChangePart(subcat.subcategoryName, it.meta, it.texMeta, it.colorMeta ));
						
						
						if (!previousData.ContainsKey(partName))
							previousData.Add(partName, i.itemId+"");
						else
							previousData[partName] = i.itemId+"";
					}
				}
				else if (i != null && Items.isNoneItem(i))
				{

					if (!previousData.ContainsKey(partName) || !previousData[partName].Equals(i.itemId+""))
						yield return StartCoroutine(ChangePart(subcat.subcategoryName, "", "" ,""));
					
					if (!previousData.ContainsKey(partName))
					{
						if (i != null)
							previousData.Add(partName, i.itemId+"");
					}
					else
					{
						if (i != null)
							previousData[partName] = i.itemId+"";
					}
				}
			}
			
			

		}
		
		FacialStuff();
		
	
		
		transform.animation.playAutomatically = false;
		transform.animation.wrapMode = WrapMode.Once;
		
		InsertAnimation("pose01");
		
		if (WithUnload)	
			StartCoroutine(DelayedUnload());
		
	}
	
	IEnumerator DelayedUnload()
	{
		yield return new WaitForSeconds(1.0f);
		Resources.UnloadUnusedAssets();
	}
	
	void ChangeCharacter(Character character)
	{
	}
	
	public string CurrAnimationName()
	{
		return this.transform.animation.name;
	}
	
	// play immediately, without crossfade
	public void PlayAnimation(string a)
	{
		animationQueue.Clear();
		QueueAnimation(a);
		
		foreach (Transform t in transform.GetComponentsInChildren<Transform>())
		{
			if (t.GetComponent<Animation>() != null)
			{
				
				if (t.animation.GetClip(a) == null)
				{
					AnimationClip ac = Resources.Load("CharacterAssets/Animation/"+a) as AnimationClip;
					t.animation.AddClip(ac, a);
				}
				
				t.animation.Play(a);
					
				
			}
		}
		
	}
	
	public void QueueAnimation(string a)
	{
		QueueAnimation(a, true);
	}
	
	public void QueueAnimation(string a, bool loop)
	{		
		AnimationQueueItem item = new AnimationQueueItem();
		item.name = a;
		item.isLoop = loop;
		animationQueue.Add(item);
		
		//Log.Warning("After adding "+a+",animation queue count:"+animationQueue.Count);
	}
	
	public void InsertAnimation(string a)
	{
		InsertAnimation(a, true);
	}
	
	public void InsertAnimation(string a, bool loop)
	{
		
		animationQueue.Clear();
		
		QueueAnimation(a,loop);
		
		DequeueAnimation();
	}
	// Update is called once per frame
	void Update ()
	{
		if (animationQueue.Count > 0)
		{
			if (!transform.GetComponentInChildren<Transform>().animation.isPlaying)
			{
				string animName = animationQueue[0].name;
				if (animationQueue.Count > 1)
				{
					// some clip is waiting
					animationQueue.RemoveAt(0);
				}
				else
				{
					if (!animationQueue[0].isLoop)
						animationQueue.RemoveAt(0);
				}
				
				DequeueAnimation();
				
				if (onAnimEnd != null)
					onAnimEnd(animName);
			}
			
			
			
		}
	
		
		
	}
	
	void DequeueAnimation()
	{
		foreach (Transform t in transform.GetComponentsInChildren<Transform>())
		{
			if (t.GetComponent<Animation>() != null)
			{
				if (animationQueue.Count > 0)
				{

					string path = t.gameObject.name.Equals("head")? "AnimationHead/":"";

					string a = animationQueue[0].name;
					if (t.animation.GetClip(a) == null)
					{
						AnimationClip ac = Resources.Load("CharacterAssets/Animation/"+path+a) as AnimationClip;
						t.animation.AddClip(ac, a);
					}
					
					//if (!t.animation.IsPlaying(a))
					{
						t.animation.CrossFade(a, 0.618f);
					}
				}
			}
		}
	}
	
	public IEnumerator ChangePart(string part , string item, string pTex, string pColor)
	{
		
		// NEED DEBUG
		if (item.Equals(""))
		{
			targetSmr[part].quality 		= SkinQuality.Bone4;			
			targetSmr[part].sharedMesh 		= null;
			targetSmr[part].bones 			= null;
			targetSmr[part].sharedMaterials = new Material[0];
			
			yield break;
		}
		
		/*
		 *     
		 *    A. Parts that only need to change texture
		 *    this part has moved to FacialStuff();
		 * 
		 */
		
		// eyeball color
		if (   part.Equals("EYEBALL")
			|| part.Equals("ROUGE")
			|| part.Equals("LIPS")
			|| part.Equals("EYEBROWS")
			|| part.Equals("ROUGE")
			|| part.Equals("LIPS")
			|| part.Equals("EYEBROWS")
			|| part.Equals("EYESHAPE")
			|| part.Equals("SKINCOLOR")
			)
		{
			//EyeballCode();
			
			yield break;
		}		
		
		
		
		/*
		 *     
		 *    B. Parts that only need to change mesh
		 * 
		 */
		
		string[] meta = item.Split('|');
		
		
		GameObject t = null;
		SkinnedMeshRenderer smr;
		
		if (!meta[0].Equals("Null")) // "Null" here is really a string
		{
			Object obj = Resources.Load("CharacterAssets/Parts/"+part+"/"+meta[0]);

			if (obj == null)
			{
				// try load from streaming assets folder instead
				string filePath 	= Application.persistentDataPath + "/"  + meta[0] +".dlc";
				FileInfo fileInfo 	= new FileInfo(filePath);
			    if (fileInfo != null && fileInfo.Exists)
			    {			   
					WWW bundle = new WWW("file://" + filePath);
					//bundle.threadPriority = ThreadPriority.High;
					yield return bundle;					
					
					if (bundle.error != null)
					{
						Log.Debug("CM Fail to load the asset bundle for " + meta[0]);
					}
					else
					{
						if (bundle.assetBundle != null)
						{
							Log.Debug("CM Loaded " + meta[0] + " asset bundle successfully!");
							t = (GameObject) Instantiate(bundle.assetBundle.mainAsset as GameObject);
							bundle.assetBundle.Unload(false);
						}
						else
							Log.Debug("CM The " + meta[0] + " asset bundle is null");	
					}		
			    }
				else
				{
					Log.Warning("PATH FAIL:"+meta[0]);
				}
				
			}
			else
			{
	//			Log.Debug("or i here?" +item);
				t = (GameObject) Instantiate(obj as GameObject);
			}
		}
		
		
		if (t != null) // actual object exist
		{
		
			
			smr = t.GetComponentInChildren<SkinnedMeshRenderer>();
			Mesh tempMesh = smr.sharedMesh;
			Material[] tempMaterialArray = smr.materials;
			smr.castShadows = false;
			smr.receiveShadows = false;
			Destroy(t);	
	
			//Transform[] hips = transform.GetComponentsInChildren<Transform>();
			
			List<Transform> bones = new List<Transform>();			
			foreach(Transform bone in smr.bones)   // item's bones
			{
				if (hips == null)
					Log.Debug("CM hips is null");
				else if (hips.Length == 0)
					Log.Debug("CM hips count is 0");
			
				foreach(Transform hip in hips)
				{
					if (hip == null)
						Log.Debug("CM hip is null");
					if (bone == null)
						Log.Debug("CM bone is null");
					if(hip.name != bone.name)
						continue;
					
					bones.Add(hip);
					break;
				}
			}
			
			//Log.Debug("shared mesh name = "+smr.sharedMesh.name);
			
			// if you don't do
			foreach (Material m in smr.materials)
			{
				string shaderName 	= m.shader.name.Replace(" ", "").Replace("/", "").Replace("-", "");
				Texture2D shade = (Texture2D) m.GetTexture("_MatCap");
				// test code
				/*Log.Debug("shader name?"+shaderName);
				
				bool changed = false;
				if (shaderName.Equals("MatCapDetail"))
				{
					changed = true;
					shaderName = "ToonDetail";
				}
				if (shaderName.Equals("MatCapBasic"))
				{
					changed = true;
					shaderName = "ToonBasic";
				}*/
				
				Shader shader		= Resources.Load("Shaders/" + shaderName) as Shader;
				if (shader != null)
					m.shader = shader;
				else
					m.shader = Shader.Find(m.shader.name);
				
				//if (changed)
				//	m.SetTexture("_ToonShade", shade);
			}
			
			/*
			 *     
			 *    C. Parts that have both mesh and texture
			 * 
			 */
			
			if (!pTex.Equals(""))
			{
				Material[] mList = tempMaterialArray;
				
	//			Log.Debug("name? "+mList[0].name+",tex?"+pTex);
				
				Texture2D texture= (Texture2D) Resources.Load("CharacterAssets/Parts/"+part+"/tex/" + pTex);
				
				if (texture == null)
				{
					//try get from dlc
					WWW loader = new WWW("file://" + Application.persistentDataPath + Path.DirectorySeparatorChar + (pTex.Replace(" ","")) + ".png");
					yield return loader;
					
					if (loader.error != null)
					{
						Log.Debug("CM Fail to load the asset bundle");
					}
					else
					{
						if (loader.texture != null)
						{
							texture = (Texture2D) loader.texture;
							
							
						}
						else
							Log.Debug("CM The " + part + " asset bundle is null " + pTex);	
					}
				}
				
				mList[0].SetTexture("_MainTex", texture);
				//targetSmr[part].quality 		= SkinQuality.Bone4;
				targetSmr[part].bones 			= bones.ToArray();
				targetSmr[part].sharedMaterials = mList;
				targetSmr[part].sharedMesh 		= tempMesh;
				
					
			}
			
			else
			{
				//targetSmr[part].quality 		= SkinQuality.Bone4;	
				targetSmr[part].bones 			= bones.ToArray();
				targetSmr[part].sharedMaterials = smr.materials;
				targetSmr[part].sharedMesh 		= tempMesh;
				
			}
		}
		
		
		/*
		 *     
		 *    D. Special parts
		 * 
		 */
		
		// hair color
		if (part.Equals("FRONTHAIR") || part.Equals("BACKHAIR"))
		{
			// i suppose this is not null (bandit)
			string colorStr = "1:1:1:1";
			colorStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("HAIRCOLOR")).meta;
			string[] p = colorStr.Split('_');
			targetSmr[part].materials[0].color = new Color(
				float.Parse(p[0])/255f,
				float.Parse(p[1])/255f,
				float.Parse(p[2])/255f,
				float.Parse(p[3])/255f);
		}
		
	}
	
	void FacialStuff()
	{
		string commonStr = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head";
		Transform commonTrans = transform.Find(commonStr);
		Transform head = commonTrans.Find("head");
		
		// hair colorlocal
		
		// i suppose this is not null (bandit)
		string colorStrB = "1:1:1:1";
		colorStrB = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("HAIRCOLOR")).meta;
		string[] pB = colorStrB.Split('_');
		Color c = new Color(
			float.Parse(pB[0])/255f,
			float.Parse(pB[1])/255f,
			float.Parse(pB[2])/255f,
			float.Parse(pB[3])/255f);
		targetSmr["FRONTHAIR"].materials[0].color = c;
		targetSmr["BACKHAIR"].materials[0].color = c;
		
		//EYEBALL
		
		Texture2D tex = (Texture2D) Resources.Load("CharacterAssets/Parts/EYEBALL/" + profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYEBALL")).meta);
		MeshRenderer mesh = commonTrans.Find("DM_Leyeball/eyeballL").GetComponent<MeshRenderer>();
		Material m = mesh.materials[0];
		m.SetTexture("_MainTex", tex );
		mesh.materials = new Material[]{m};
		
		mesh = commonTrans.Find("DM_Reyeball/eyeballR").GetComponent<MeshRenderer>();
		m = mesh.materials[0];
		m.SetTexture("_MainTex",  tex);
		mesh.materials = new Material[]{m};
		
		//SKIN COLOR
		SkinnedMeshRenderer bodymesh = transform.Find("body").GetComponent<SkinnedMeshRenderer>();
	 	m = bodymesh.materials[0];
		colorStrB = "1_1_1_1";
		colorStrB = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("SKINCOLOR")).meta;
			
		pB = colorStrB.Split('_');
		m.SetColor("_Color", new Color(
					float.Parse(pB[0])/255f,
					float.Parse(pB[1])/255f,
					float.Parse(pB[2])/255f,
					float.Parse(pB[3])/255f));
		
		// OTHERS
		
		
		MegaMorph morpher = head.GetComponent<MegaMorph>();
			
		// LIPS (shape)
		string perStr = "0_0_0_0";
				perStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("LIPS")).meta;

		pB = perStr.Split('_');

		morpher.GetComponent<HeadMorphUpdater>().setLip03Amend(Int32.Parse(pB[2]) == 1);

		for (int i = 1; i <= 4; i++)
		{
			morpher.GetChannel("head_lip_" + (i<10?("0"+i):(""+i))).SetTarget(float.Parse(pB[i-1])*100f,10000.0f);
		}
		
		// EYEBROWS
		perStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYEBROWS")).meta;

		pB = perStr.Split('_');
		for (int i = 1; i <= 7; i++)
			morpher.GetChannel("head_brow" + (i<10?("0"+i):(""+i))).SetTarget(float.Parse(pB[i-1])*100f,10000.0f);
		
		//EYESHAPE
		perStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYESHAPE")).meta;

		pB = perStr.Split('_');
		string[] channelNames = new string[8]{"head_eye01", "head_eye02","head_eye03","head_eye05","head_eye06","head_eye07","head_eye01_0","head_eye02_0"};

		if (perStr.Equals("0_0_0_0_0_0_0_0"))
			morpher.GetComponent<HeadMorphUpdater>().cancelTargetEyeChannel();
			
		for (int i = 1; i <= 8; i++)
		{
			//if (i == 4) continue;

			if (Int32.Parse(pB[i-1]) == 1)
				morpher.GetComponent<HeadMorphUpdater>().setTargetEyeChannel(channelNames[i-1]);
							
			morpher.GetChannel(channelNames[i-1]).SetTarget(float.Parse(pB[i-1])*99.9f,10000.0f);
		}
		
		SkinnedMeshRenderer headMesh = head.GetComponent<SkinnedMeshRenderer>();
//		mesh = head.GetComponent<SMeshRenderer>();
		Material[] ms = new Material[ headMesh.materials.Length];
		
		for (int i = 0; i < ms.Length; i++)
		{
			
			// ROUGE
			if (headMesh.materials[i].name.Contains("face_red_mat"))
			{
				
				
				string colorStr = "1_1_1_1";
				colorStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("ROUGE")).meta;

				string[] p = colorStr.Split('_');
				headMesh.materials[i].SetColor("_Color", new Color(
					float.Parse(p[0])/255f,
					float.Parse(p[1])/255f,
					float.Parse(p[2])/255f,
					(colorStr.Equals("0_0_0_0")? 0 : profile.RougeAlpha)/255f));
				
			}
			
			// lips color
			else if (headMesh.materials[i].name.Contains("lips_mat"))
			{
				
				
				string colorStr = "1_1_1_1";
				colorStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("LIPSCOLOR")).meta;

				string[] p2 = colorStr.Split('_');
				headMesh.materials[i].SetColor("_Color", new Color(
					float.Parse(p2[0])/255f,
					float.Parse(p2[1])/255f,
					float.Parse(p2[2])/255f,
					float.Parse(p2[3])/255f));
				
			}
			
			else if (headMesh.materials[i].name.Contains("eyeshadow_mat"))
			{
				string texStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYESHAPE")).texMeta;
			
				
				headMesh.materials[i].mainTexture=  Resources.Load("CharacterAssets/Parts/EYESHAPE/tex/"+texStr) as Texture2D;
				
			
				string colorStr = "1_1_1_1";
				colorStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYESHAPECOLOR")).meta;

				string[] p2 = colorStr.Split('_');
				headMesh.materials[i].SetColor("_Color", new Color(
					float.Parse(p2[0])/255f,
					float.Parse(p2[1])/255f,
					float.Parse(p2[2])/255f,
					profile.EyeShadowAlpha/255f));
				
			}
			
			else if (headMesh.materials[i].name.Contains("eyelash_mat"))
			{
				string colStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYESHAPE")).colorMeta;
			
				headMesh.materials[i].mainTexture=  Resources.Load("CharacterAssets/Parts/EYESHAPE/tex/"+colStr) as Texture2D;
				//test code
				//mesh.materials[i].renderQueue = 99;
			}
			
			else if (headMesh.materials[i].name.Contains("head_uv_col"))
			{
				
				
				string texStr = "head_uv_col";
				texStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("SKINCOLOR")).texMeta;

				
				headMesh.materials[i].SetTexture("_MainTex", Resources.Load("CharacterAssets/Parts/SKINCOLOR/tex/"+texStr) as Texture2D);
				
			}
			
			ms[i] = headMesh.materials[i];
		}
		
		
		
		headMesh.materials = ms;
	}
	/*
	void EyeballCode()
	{
		//transform leftBall, rightBall;
			
		string commonStr = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head";
		Transform commonTrans = transform.Find(commonStr);
		
		MeshRenderer mesh = commonTrans.Find("DM_Leyeball/eyeballL").GetComponent<MeshRenderer>();
		Material m = mesh.materials[0];
		m.SetTexture("_MainTex",  (Texture2D) Resources.Load("CharacterAssets/Parts/"+part+"/" + item));
		mesh.materials = new Material[]{m};
		
		mesh = commonTrans.Find("DM_Reyeball/eyeballR").GetComponent<MeshRenderer>();
		m = mesh.materials[0];
		m.SetTexture("_MainTex",  (Texture2D) Resources.Load("CharacterAssets/Parts/"+part+"/" + item));
		mesh.materials = new Material[]{m};
	}
	
	void RougeCode()
	{
		//transform leftBall, rightBall;
			
			string commonStr = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head";
			Transform commonTrans = transform.Find(commonStr);
			
			MeshRenderer mesh = commonTrans.Find("head").GetComponent<MeshRenderer>();
			Material[] ms = new Material[ mesh.materials.Length];
			
			for (int i = 0; i < ms.Length; i++)
			{
				
				
				if (mesh.materials[i].name.Contains("face_red_mat"))
				{
					
					
					string colorStr = "1_1_1_1";
					colorStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("ROUGE")).meta;

					string[] p = colorStr.Split('_');
					mesh.materials[i].SetColor("_Color", new Color(
						float.Parse(p[0])/255f,
						float.Parse(p[1])/255f,
						float.Parse(p[2])/255f,
						(colorStr.Equals("0_0_0_0")? 0 : profile.RougeAlpha)/255f));
					
				}
				
				ms[i] = mesh.materials[i];
			}
			
			
			
			mesh.materials = ms;
	}
	
	void LipsCode()
	{
		//transform leftBall, rightBall;
			
			string commonStr = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head";
			Transform commonTrans = transform.Find(commonStr);
			
			MegaMorph morpher = commonTrans.Find("head").GetComponent<MegaMorph>();
			
			string perStr = "0_0_0_0";
					perStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("LIPS")).meta;

			string[] p = perStr.Split('_');
			for (int i = 1; i <= 4; i++)
				morpher.GetChannel("head_lip_" + (i<10?("0"+i):(""+i))).SetTarget(float.Parse(p[i-1])*100f,10000.0f);
			
			
			// lips color
			MeshRenderer mesh = commonTrans.Find("head").GetComponent<MeshRenderer>();
			Material[] ms = new Material[ mesh.materials.Length];
			
			for (int i = 0; i < ms.Length; i++)
			{
				
				
				if (mesh.materials[i].name.Contains("lips_mat"))
				{
					
					
					string colorStr = "1_1_1_1";
					colorStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("LIPSCOLOR")).meta;

					string[] p2 = colorStr.Split('_');
					mesh.materials[i].SetColor("_Color", new Color(
						float.Parse(p2[0])/255f,
						float.Parse(p2[1])/255f,
						float.Parse(p2[2])/255f,
						float.Parse(p2[3])/255f));
					
				}
				
				ms[i] = mesh.materials[i];
			}
			
			
			
			mesh.materials = ms;
	}
	
	void EyebrowsCode()
	{
		//transform leftBall, rightBall;
			
			string commonStr = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head";
			Transform commonTrans = transform.Find(commonStr);
			
			MegaMorph morpher = commonTrans.Find("head").GetComponent<MegaMorph>();
			
			string perStr = "0_0_0_0_0_0_0";
					perStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYEBROWS")).meta;

			string[] p = perStr.Split('_');
			for (int i = 1; i <= 7; i++)
				morpher.GetChannel("head_brow" + (i<10?("0"+i):(""+i))).SetTarget(float.Parse(p[i-1])*100f,10000.0f);
	}
	
	void EyeshapeCode()
	{
		//transform leftBall, rightBall;
			
			string commonStr = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head";
			Transform commonTrans = transform.Find(commonStr);
			
			MegaMorph morpher = commonTrans.Find("head").GetComponent<MegaMorph>();
			
			string perStr = "0_0_0_0_0_0_0_0";
					perStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYESHAPE")).meta;

			string[] p = perStr.Split('_');
			string[] channelNames = new string[8]{"head_eye01", "head_eye02","head_eye03","head_eye05","head_eye06","head_eye07","head_eye01_0","head_eye02_0"};
			
			for (int i = 1; i <= 8; i++)
			{
				//if (i == 4) continue;				
				morpher.GetChannel(channelNames[i-1]).SetTarget(float.Parse(p[i-1])*99.9f,10000.0f);
			}			
			
			// eyeshape color and texture
			MeshRenderer mesh = commonTrans.Find("head").GetComponent<MeshRenderer>();
			Material[] ms = new Material[ mesh.materials.Length];
			
			for (int i = 0; i < ms.Length; i++)
			{
				
				if (mesh.materials[i].name.Contains("eyeshadow_mat"))
				{
					string texStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYESHAPE")).texMeta;
				
					
					mesh.materials[i].mainTexture=  Resources.Load("CharacterAssets/Parts/EYESHAPE/tex/"+texStr) as Texture2D;
					
				
					string colorStr = "1_1_1_1";
					colorStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYESHAPECOLOR")).meta;

					string[] p2 = colorStr.Split('_');
					mesh.materials[i].SetColor("_Color", new Color(
						float.Parse(p2[0])/255f,
						float.Parse(p2[1])/255f,
						float.Parse(p2[2])/255f,
						profile.EyeShadowAlpha/255f));
					
				}
				
				else if (mesh.materials[i].name.Contains("eyelash_mat"))
				{
					string colStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("EYESHAPE")).colorMeta;
				
					mesh.materials[i].mainTexture=  Resources.Load("CharacterAssets/Parts/EYESHAPE/tex/"+colStr) as Texture2D;
					//test code
					//mesh.materials[i].renderQueue = 99;
				}
				
				ms[i] = mesh.materials[i];
			}
			
			
			
			mesh.materials = ms;
	}
	
	void SkinColorCode()
	{
		//transform leftBall, rightBall;
			
			string commonStr = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head";
			Transform commonTrans = transform.Find(commonStr);
			
			MeshRenderer mesh = commonTrans.Find("head").GetComponent<MeshRenderer>();
			Material[] ms = new Material[ mesh.materials.Length];
			
			for (int i = 0; i < ms.Length; i++)
			{
				if (mesh.materials[i].name.Contains("head_uv_col"))
				{
					
					
					string texStr = "head_uv_col";
					texStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("SKINCOLOR")).texMeta;

					
					mesh.materials[i].SetTexture("_MainTex", Resources.Load("CharacterAssets/Parts/SKINCOLOR/tex/"+texStr) as Texture2D);
					
				}
				ms[i] = mesh.materials[i];
			}
			
			
			
			mesh.materials = ms;
			
			
			SkinnedMeshRenderer bodymesh = transform.Find("body").GetComponent<SkinnedMeshRenderer>();
			Material m = bodymesh.materials[0];
			string colorStr = "1_1_1_1";
			colorStr = profile.wearingItemOfSubcategory(ItemSubcategories.getSubcategory("SKINCOLOR")).meta;
			
			string[] p = colorStr.Split('_');
			m.SetColor("_Color", new Color(
						float.Parse(p[0])/255f,
						float.Parse(p[1])/255f,
						float.Parse(p[2])/255f,
						float.Parse(p[3])/255f));
	}
	*/
	public string GetClosetAnimationString(ItemSubcategory subcat)
	{
		int subcatId = subcat.subcategoryid;
		
		switch (subcatId)
		{
			case (int)ItemSubcategories.Id.TOP: 
			case (int)ItemSubcategories.Id.DRESS:
			case (int)ItemSubcategories.Id.COAT:
			case (int)ItemSubcategories.Id.BRA:
				return "closet_upper";
			
			case (int)ItemSubcategories.Id.SKIRT: 
			case (int)ItemSubcategories.Id.TROUSERS:
			case (int)ItemSubcategories.Id.PANTIES:
			case (int)ItemSubcategories.Id.SOCKS:
			case (int)ItemSubcategories.Id.SHOES:
			case (int)ItemSubcategories.Id.LEGGINGS:
				return "closet_lower";
			
		}
		
		return "pose01";
	}
		
	public void ResetPRS()
	{
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;
		transform.localScale = Vector3.one;
	}
	
	// set Position, Rotation, Scale using values in reference object
	public void SetPRSbyReference(GameObject reference)
	{
		transform.localPosition = reference.transform.localPosition;
		transform.localRotation = reference.transform.localRotation;
		transform.localScale = reference.transform.localScale;
	}
	
	public void InsertShadow()
	{
		GameObject shadow = (GameObject) Instantiate(Resources.Load("shadow_plane") as GameObject);
		
		shadow.transform.parent = transform;
		shadow.transform.localPosition = Vector3.zero;
		shadow.transform.localRotation = Quaternion.identity;
		shadow.transform.localScale = Vector3.one;
		
		
	}
	
	
}

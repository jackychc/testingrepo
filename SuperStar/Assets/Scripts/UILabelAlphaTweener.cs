using UnityEngine;
using System.Collections;

public class UILabelAlphaTweener : MonoBehaviour {
	
	public enum TWEENMODE
	{
		PASSIVE,
		LOOP,
		BACK_AND_FORTH
	}
	
	float proportion = 0f;
	bool commanded;
	bool isForward = true;
	
	public float speed = 1f;
	public float fromValue;
	public float toValue;
	
	public TWEENMODE mode = TWEENMODE.PASSIVE;
	
	UILabel theLabel;
	Color initialColor;
	
	public float CountDown = 0;
	int countDown;
	
	// Use this for initialization
	void Start () {
		theLabel = transform.GetComponent<UILabel>();
		initialColor = theLabel.color;
		countDown = (int) (CountDown * 60);
	}
	
	// Update is called once per frame
	void Update () {
		
		if (mode.Equals(TWEENMODE.PASSIVE) && !commanded)
			return;
		
		
		proportion = Mathf.Max(0.0f, Mathf.Min(proportion, 1.0f));
		
		
		Move();
		CountDownTick();
	}
	
	public void setProportion(float v)
	{
		proportion = v;
	}
	
	public void Forward()
	{
		if (CountDown > 0)
		{
			proportion = 0f;
			countDown =  (int) (CountDown * 60);
		}
		commanded = true;
		isForward = true;
	}
	
	public void Backward()
	{
		commanded = true;
		isForward = false;
	}
	
	void Move()
	{
		proportion = proportion + (isForward? 1 : -1) * speed * (1f/30);
		
		theLabel.color = new Color(initialColor.r, initialColor.g, initialColor.b, Mathf.Lerp(fromValue, toValue, proportion));
		//transform.GetComponent<UILabel>().relativeOffset.y = Mathf.Lerp(fromYValue, toYValue, proportion);
		
		if (mode.Equals(TWEENMODE.LOOP))
		{
			if (proportion >= 1.0f)
				proportion = 0f;
			
		}
		
		if (mode.Equals(TWEENMODE.BACK_AND_FORTH))
		{
			if (proportion <= 0f || proportion >= 1.0f)
				isForward = !isForward;
		}
	}
	
	void CountDownTick()
	{
		if (countDown > 0)
		{
			countDown = Mathf.Max(0, countDown - 1);
			
			if (countDown <= 0)
			{
				Backward();
			}
		}
	}
}

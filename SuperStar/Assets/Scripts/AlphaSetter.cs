using UnityEngine;
using System.Collections;
using System;

public class AlphaSetter : MonoBehaviour
{
	public UISlider slider;
	
	Material rougeMaterial;
	Material eyeShadowMaterial;
	
	MODE mode = MODE.ROUGE;
	bool inited = false;
	Color c;
	
	
	enum MODE
	{
		ROUGE,
		EYESHADOW
	}
	
	
	// Use this for initialization
	void Start ()
	{
		inited = false;		
		slider.onValueChange += ValueChanged;
		
		StartCoroutine(DelayedStart());
	}
	
	IEnumerator DelayedStart()
	{
		yield return new WaitForEndOfFrame();
		
		GameObject girl = GameObject.Find("FEMALE");
		
		string commonStr = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 Head/head";
		Transform commonTrans = girl.transform.Find(commonStr);
		SkinnedMeshRenderer mesh = commonTrans.GetComponent<SkinnedMeshRenderer>();
		Material[] ms = new Material[ mesh.materials.Length];
			
		for (int i = 0; i < ms.Length; i++)
		{
			
			
			if (mesh.materials[i].name.Contains("face_red_mat"))
			{
				rougeMaterial = mesh.materials[i];				
			}
			
			if (mesh.materials[i].name.Contains("eyeshadow_mat"))
			{
				eyeShadowMaterial = mesh.materials[i];				
			}
			
		}
		
		inited = true;
	}
	
	
	public void SetMode(string m)
	{
		mode = (MODE) Enum.Parse(typeof(MODE), m );
		
		float targetValue = 0f;
		
		if (mode.Equals(MODE.ROUGE))
		{
			targetValue = Globals.mainPlayer.RougeAlpha *1f/255f;
		}
		
		
		if (mode.Equals(MODE.EYESHADOW))
		{
			targetValue = Globals.mainPlayer.EyeShadowAlpha *1f/255f;
		}
		
		slider.sliderValue = targetValue;
		//slider.
	}
	
	void ValueChanged(float val)
	{
		if (!inited) return;
		
		Log.Debug("value changed:"+val);
		if (mode.Equals(MODE.ROUGE))
		{
			//targetValue = Globals.mainPlayer.RougeAlpha *1f/255f;
			Globals.mainPlayer.RougeAlpha = (int) (val*255);
			c =  rougeMaterial.color;
			
			// none rouge
			if (!Globals.mainPlayer.isWearingItem(Items.getItem(179)))
				c.a = val;
			else
				c.a = 0;
			
			rougeMaterial.color = c;
		}
		
		
		if (mode.Equals(MODE.EYESHADOW))
		{
			//targetValue = Globals.mainPlayer.EyeShadowAlpha *1f/255f;
			Globals.mainPlayer.EyeShadowAlpha = (int) (val*255);
			c =  eyeShadowMaterial.color;
			c.a = val;
			eyeShadowMaterial.color = c;
		}
		
	}
}


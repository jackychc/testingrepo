using UnityEngine;
using System.Collections;

public class CloudMovement : MonoBehaviour
{
	Vector3 pos;
	int dir = 1;
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(DelayedStart());
	}
	
	IEnumerator DelayedStart()
	{
		yield return new WaitForSeconds(0.03f);
		
		pos = transform.localPosition;
		dir = UnityEngine.Random.Range(-2,3);
		InvokeRepeating("Move", 0f, 0.015f);
	}
	
	void Move()
	{
		pos.x = pos.x + (dir * 0.2f);
		
		if (pos.x < - 500f && dir < 0f)
		{
			pos.x = 7000f;
		}
		else if (pos.x > 7000f && dir > 0f)
		{
			pos.x = -500f;
		}
		
		transform.localPosition = pos;
		
	}
	
}


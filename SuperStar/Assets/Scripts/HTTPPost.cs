using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HTTPPost 
{
	//bool error = false;
	//string response  = "";
	
	public static IEnumerator Post(string functionName, Dictionary<string,string> parameters, System.Action<string> response)
	{
		Log.Debug("function name:"+functionName);
		
		WWWForm form = new WWWForm();		
		if (parameters != null)
		{
			foreach (KeyValuePair<string,string> k in parameters)
			{
				form.AddField(k.Key, k.Value);
			}
		}
		else
			form.AddField("dummy", "dummy");
	
		string url = "http://prod-ac-superstarstory.outblaze.net/API/"+functionName+".php";/* + (parameters == null? "":"?"+parameters)*/		
		
		WWW www = new WWW(url, form);
		
//		Log.Debug("bbbb");
		
		yield return www;
		
		//Log.Debug("!@*$*!*$@!");
			 
		// check for errors
		if (www.error == null)
		{
			Log.Debug("["+functionName+"] WWW Ok!: " + www.text);
			response(www.text);
		}
		else
		{
			Log.Debug("["+functionName+"] WWW Error: " + www.error);
			//error = true;
			response(null);
		}
		
		
		
		//yield return StartCoroutine(WaitForRequest(www));
	}

	
	/*public string getResponse()
	{
		return response;
	}*/
}

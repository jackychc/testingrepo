using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VotingScene: MonoBehaviour
{
	public UISysFontLabel[] topicNames = new UISysFontLabel[2];
	public GameObject[] topicBoards = new GameObject[2];
	public GameObject joinButton;
	public GameObject informationBoard;
	public GameObject specialEventButton, weeklyEventButton;
	public GameObject voteButton;
	
	public GameObject backgroundPicture, backgroundPictureSpecial, goldBall;
	
	public LoadingFlower loadingFlower;
	
	bool done1, done2;
	Texture2D normalBg, specialBg;
	
	
	void Awake()
	{
		//normalBg = (Texture2D) Resources.Load("Backgrounds/voting_Bg");
		//specialBg = (Texture2D) Resources.Load("Backgrounds/votingSp_Bg");
		StartCoroutine(ReallyAwake());
	}
	
	IEnumerator ReallyAwake()
	{
		
		
		informationBoard.GetComponent<VotingInformationBoard>().reinitialize();
		
		if (Globals.normalTopic != null)
		{
			Globals.viewingTopic = Globals.normalTopic;
			topicNames[0].Text = Globals.viewingTopic.topicName;
			
			yield return StartCoroutine(SimutaneousGet(Globals.normalTopic));

			
		}
		
		
		if (Globals.specialTopic != null)
		{
			if (Globals.normalTopic == null)
			{
				Globals.viewingTopic = Globals.specialTopic;
				topicNames[1].Text = Globals.viewingTopic.topicName;
				yield return StartCoroutine(SimutaneousGet(Globals.specialTopic));
			}
			
			else
			{
				specialEventButton.GetComponent<AnchorTweener>().Forward();
			}
		}
		
		//SoundManager.SharedManager.ChangeMusic("SuperstarStory_fashioncontest");
		
		LocalizeButtonText();
		
	}
	
	IEnumerator SimutaneousGet(Topic topic)
	{
		yield return StartCoroutine(SimutaneousGet(topic, true));
	}
	IEnumerator SimutaneousGet(Topic topic, bool workAtForeground)
	{
		
		if (workAtForeground)
			loadingFlower.Show();
		
		done1 = false;
		done2 = false;
		
		StartCoroutine(GetInformation(topic, workAtForeground));
		StartCoroutine(GetRewardData(topic, workAtForeground));
			
		while (!done1 || !done2)
			yield return null;

		loadingFlower.Hide();
		
		if (topic.isEnded)
		{
			PopupManager.ShowPreviousTopicPopup(topic, false);
			// Shows that weird popup
		}
		else
		{
			voteButton.GetComponent<AnchorTweener>().Forward();
		}
	}
	
	void hideAll()
	{
		joinButton.GetComponent<AnchorTweener>().Backward();
		informationBoard.GetComponent<AnchorTweener>().Backward();
		weeklyEventButton.GetComponent<AnchorTweener>().Backward();
		specialEventButton.GetComponent<AnchorTweener>().Backward();
		voteButton.GetComponent<AnchorTweener>().Backward();
		informationBoard.GetComponent<VotingInformationBoard>().reinitialize();
	}
	
	public IEnumerator SpecialClicked()
	{
		if (!loadingFlower.isActive)
		{
			//backgroundPicture.GetComponent<UITexture>().mainTexture =specialBg;
			backgroundPicture.GetComponent<AnchorTweener>().Backward();
			backgroundPictureSpecial.GetComponent<AnchorTweener>().Forward();
			goldBall.GetComponent<AnchorTweener>().Forward();
			
			hideAll();
			Globals.viewingTopic = Globals.specialTopic;
			
			
			topicNames[1].Text = Globals.viewingTopic.topicName;
			
			if (Globals.normalTopic != null)
				weeklyEventButton.GetComponent<AnchorTweener>().Forward();
			else
				weeklyEventButton.GetComponent<AnchorTweener>().Backward();
			specialEventButton.GetComponent<AnchorTweener>().Backward();
			
			topicBoards[1].GetComponent<AnchorTweener>().Forward();
			topicBoards[0].GetComponent<AnchorTweener>().Backward();
			
			
			
			yield return StartCoroutine(SimutaneousGet(Globals.specialTopic));
			
			//PopupManager.ShowPreviousTopicPopup(Globals.specialTopic);
		}
		else
			yield return null;
	}
	
	public IEnumerator WeeklyClicked()
	{
		if (!loadingFlower.isActive)
		{
			//backgroundPicture.GetComponent<UITexture>().mainTexture = normalBg;
			backgroundPictureSpecial.GetComponent<AnchorTweener>().Backward();
			backgroundPicture.GetComponent<AnchorTweener>().Forward();
			goldBall.GetComponent<AnchorTweener>().Backward();
			
			hideAll();
			Globals.viewingTopic = Globals.normalTopic;
			
		
			
			topicNames[0].Text = Globals.viewingTopic.topicName;
	
			if (Globals.specialTopic != null)
				specialEventButton.GetComponent<AnchorTweener>().Forward();
			else
				specialEventButton.GetComponent<AnchorTweener>().Backward();
			weeklyEventButton.GetComponent<AnchorTweener>().Backward();
			
			
			
			topicBoards[0].GetComponent<AnchorTweener>().Forward();
			topicBoards[1].GetComponent<AnchorTweener>().Backward();
			
			yield return StartCoroutine(SimutaneousGet(Globals.normalTopic));
		}
		else
			yield return null;
	}
	
	
	IEnumerator GetInformation(Topic topic, bool workAtForeground)
	{
		done1 = false;
		int tp = topic.Equals(Globals.normalTopic)? 0:1;
		
		string topicId = topic.topicId.ToString();
		GameID gameId = new GameID(Globals.mainPlayer.GameId);
		int playerId = gameId.toInt();
		string checkSumData = gameId.ID + playerId + topicId;
		string checkSum = MD5Hash.GetServerCheckSumWithString("PlayerVotingsRank", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("topicId", topicId);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "PlayerVotingsRank.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					
					if (al[1] != null)
					{
						// have rank already
						int secondsRemain = 0;
						yield return StartCoroutine(CalculateNumberOfSecondsRemain(topic.endDate, result => secondsRemain = result ));
							
						((Hashtable)al[1]).Add("secondsRemain", secondsRemain);
						
						
						if (workAtForeground)
						{
							joinButton.GetComponent<AnchorTweener>().Backward();
							informationBoard.GetComponent<AnchorTweener>().Forward();
						}
						
						informationBoard.GetComponent<VotingInformationBoard>().UpdateInformation((Hashtable)al[1]);
						informationBoard.GetComponent<VotingInformationBoard>().secondsRemain = secondsRemain;
						informationBoard.GetComponent<VotingInformationBoard>().UpdateTimeRemain();
					}
					
					else
					{
						
						if (workAtForeground)
						{
							joinButton.GetComponent<AnchorTweener>().Forward();
							informationBoard.GetComponent<AnchorTweener>().Backward();
						}
					}
				}
				else
				{
					// didn't join the topic
					
					int secsRemain = 0;
					yield return StartCoroutine(CalculateNumberOfSecondsRemain(topic.endDate, result => secsRemain = result ));
					informationBoard.GetComponent<VotingInformationBoard>().secondsRemain = secsRemain;
					informationBoard.GetComponent<VotingInformationBoard>().UpdateTimeRemain();
					
					if (workAtForeground)
					{
						
						joinButton.GetComponent<AnchorTweener>().Forward();
						informationBoard.GetComponent<AnchorTweener>().Backward();
					}
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("PlayerVotingsRank", kvp, result => response = result));
		done1 = true;
	}

	
	IEnumerator GetRewardData(Topic topic, bool workAtForeground)
	{
		done2 = false;
		int tp = topic.Equals(Globals.normalTopic)? 0:1;
		
		string topicId 		= topic.topicId.ToString();
		GameID gameId		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + topicId;
		string checkSum		= MD5Hash.GetServerCheckSumWithString("GetTopicsRewards", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("topicId", topicId);
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "GetTopicsRewards.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					
					if (al[1] != null)
					{
						topic.tiers.Clear();
						
						ArrayList alt = new ArrayList(((Hashtable) al[1]).Values);
		
						for (int i = 0; i < alt.Count; i++)
						{
							Hashtable ht = (Hashtable) alt[i];
		
							
							Topic.Tier t = new Topic.Tier();
							
							t.tierId = Int32.Parse(ht["topicTierId"].ToString());
							
							if (ht["votes"] != null)
								t.requirement = (Int32.Parse(ht["votes"].ToString()));
							
							else if (ht["rank"] != null)
								t.rank = (Int32.Parse(ht["rank"].ToString()));
							
							
							ArrayList alrOuter = new ArrayList(((Hashtable) ht["rewards"]).Values);
							
							
							for (int k = 0; k < alrOuter.Count; k++)
							{
								Hashtable rht = (Hashtable) alrOuter[k];
								
								
								//testing codes
								Reward r = new Reward(
									Int32.Parse(rht["rewardId"].ToString()),
									-1,
									Int32.Parse(rht["coins"].ToString()),
									Int32.Parse(rht["gems"].ToString()),
									Int32.Parse(rht["exp"].ToString()),
									Int32.Parse(rht["energy"].ToString()),
									"",
									new Skill(new int[]{0,0,0,0,0})
								);
								
								t.rewards.Add(r);
							}
							
							t.claimed = (ht["isClaimed"].ToString() == "1");
							
							topic.tiers.Add(t);
						}
						
						informationBoard.GetComponent<VotingInformationBoard>().UpdateTier();
					}
					
					else
					{
						// no reward!?
					}
				}
			}
		}	
		//yield return StartCoroutine(HTTPPost.Post("GetTopicsRewards", kvp, result => response = result));
		
		done2 = true;
	}

	
	public void claimRewardAtTier1()
	{
		// i guess we should do this at same class
		if (Globals.viewingTopic.votes >= Globals.viewingTopic.getTierAt(0).requirement)
			StartCoroutine(ClaimReward(Globals.viewingTopic.getTierAt(0).tierId));
	}
	public void claimRewardAtTier2()
	{
		if (Globals.viewingTopic.votes >= Globals.viewingTopic.getTierAt(1).requirement)
			StartCoroutine(ClaimReward(Globals.viewingTopic.getTierAt(1).tierId));
	}
	public void claimRewardAtTier3()
	{
		if (Globals.viewingTopic.votes >= Globals.viewingTopic.getTierAt(2).requirement)
			StartCoroutine(ClaimReward(Globals.viewingTopic.getTierAt(2).tierId));
	}
	
	IEnumerator ClaimReward(int tier)
	{
		PopupManager.ShowLoadingFlower();
		
		int tp = Globals.viewingTopic.Equals(Globals.normalTopic)? 0:1;
		
		string topicId 		= Globals.viewingTopic.topicId.ToString();
		string topicTierId 	= tier.ToString();
		string dlcId 		= Globals.currentDLCVersion.ToString();
		
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + topicId + topicTierId + dlcId;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("ClaimTopicsRewards", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("topicId", topicId);
		kvp.Add("topicTierId", topicTierId);
		kvp.Add("dlcId", dlcId);
		kvp.Add("checkSum", checkSum);	
		
		string url = Globals.serverAPIURL + "ClaimTopicsRewards.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					// have rank already
					if (al[1] != null)
					{
						if (((Hashtable)al[1])["topicTierId"] != null)
						{
							Hashtable rht = ((Hashtable)al[1]);
							
							// get the reward!!!
							// use server result, don't use local ones
							Reward r = new Reward(
										Int32.Parse(rht["rewardId"].ToString()),
										1,
										Int32.Parse(rht["coins"].ToString()),
										Int32.Parse(rht["gems"].ToString()),
										Int32.Parse(rht["exp"].ToString()),
										Int32.Parse(rht["energy"].ToString()),
										rht["itemIds"] != null? rht["itemIds"].ToString(): "",
										rht["skills"] != null? new Skill(rht["skills"].ToString()):new Skill(new int[]{0,0,0,0,0})
									);
							
							PopupManager.HideLoadingFlower();
							
							// if last reward, show a popup
							if (tier != Globals.viewingTopic.getTierAt(Globals.viewingTopic.getCountTiers().Count-1).tierId)
							{
								r.claim();
								PopupManager.ShowRewardPopup(r, false);
							}
							else
							{
								PopupManager.ShowChooseGiftPopup(r, false);
							}
							
							
							Globals.viewingTopic.getTier(tier).claimed = true;
							
							
							yield return new WaitForSeconds(1.0f);
							
							informationBoard.GetComponent<VotingInformationBoard>().UpdateTier();
							
							
						}
						
						else
						{
							// claimed? or what? (bandit)
							// suppose we won't reach here lor.
							PopupManager.HideLoadingFlower();
						}
						
					
					}
					else
					{
						PopupManager.HideLoadingFlower();
					}
				}
				else
				{
					PopupManager.HideLoadingFlower();
					
					if (al[1] != null && ((Hashtable)al[1])["isDLCError"] != null &&  ((Hashtable)al[1])["isDLCError"].ToString().Equals("true") )
					{
						PopupManager.ShowInformationPopup("You must update DLC to the latest version to claim the reward.", false);
					}
					else
					{
						PopupManager.ShowInformationPopup("There is some error in collecting the reward.", false);
					}
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("ClaimTopicsRewards", kvp, result => response = result));
	}
	
	IEnumerator CalculateNumberOfSecondsRemain(string input, Action<int> output)
	{
		DateTime endDate;
		string[] time = null;
		string[] date = null;
		string[] inputDateTime = input.Split(' ');
		if (inputDateTime != null && input.Length == 2)
		{
			string inputDate = inputDateTime[0];
			string inputTime = inputDateTime[1];
			
			time = inputTime.Split(':');
			date = inputDate.Split('-');
			endDate = new DateTime(Int32.Parse(date[0]), Int32.Parse(date[1]), Int32.Parse(date[2]), Int32.Parse(time[0]), Int32.Parse(time[1]), Int32.Parse(time[2]));
		}
		else
		{
			string inputDate = input;
			date = inputDate.Split('-');
			endDate = new DateTime(Int32.Parse(date[0]), Int32.Parse(date[1]), Int32.Parse(date[2]), 0,0,0);
		}
			
		DateTime now = new DateTime();
		yield return StartCoroutine(GetSystemTime(result => now = result));
		TimeSpan span = endDate - now;
		output((int) (span.TotalSeconds));
		
	}
	
	IEnumerator GetSystemTime(Action<DateTime> date)
	{		
		string checkSum = MD5Hash.GetServerCheckSumWithString("SystemTime", null);	
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("checkSum", checkSum);		
		
		string url = Globals.serverAPIURL + "SystemTime.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					// have rank already
					if (al[1] != null)
					{
						date(new DateTime(
							Int32.Parse(((Hashtable)al[1])["year"].ToString()),
							Int32.Parse(((Hashtable)al[1])["month"].ToString()),
							Int32.Parse(((Hashtable)al[1])["day"].ToString()),
							Int32.Parse(((Hashtable)al[1])["hour"].ToString()),
							Int32.Parse(((Hashtable)al[1])["minute"].ToString()),
							Int32.Parse(((Hashtable)al[1])["second"].ToString())
							));
					}
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("SystemTime", kvp, result => response = result));		
	}
	
	void ShowMessagePopup()
	{
		PopupManager.ShowMessagePopup(false);
	}
	
	
	public void RefreshTopic()
	{
		StartCoroutine(RefreshTopicRoutine());
	}
	public IEnumerator RefreshTopicRoutine()
	{
		Log.Debug("start refresh topic");
		Globals.normalTopic 	= null;
		Globals.specialTopic 	= null;		
		
		loadingFlower.Show();
		topicNames[0].Text = "";
		topicNames[1].Text = "";
		hideAll();
		informationBoard.GetComponent<VotingInformationBoard>().reinitialize();
		
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("GetLatestTopics", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId",Globals.mainPlayer.GameId);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "GetLatestTopics.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					
					ArrayList alt = new ArrayList(((Hashtable) al[1]).Values);
					
					for (int i = 0; i < alt.Count; i++)
					{
						Hashtable innerHt = (Hashtable) alt[i];
						
						// have topic
						if (innerHt["topicIsEnd"] != null)
						{
							
							if (Int32.Parse(innerHt["topicType"].ToString()) == 0)
							{	
								Globals.normalTopic = new Topic(false);
								Globals.normalTopic.topicId = Int32.Parse(innerHt["topicId"].ToString());
								Globals.normalTopic.topicName = innerHt["topicName"].ToString();
								Globals.normalTopic.endDate = innerHt["topicPeriod"].ToString().Split('|')[1];
								Globals.normalTopic.isEnded = (Int32.Parse(innerHt["topicIsEnd"].ToString()) == 1);
							}
							else if (Int32.Parse(innerHt["topicType"].ToString()) == 1)
							{
								Globals.specialTopic = new Topic(true);
								Globals.specialTopic.topicId = Int32.Parse(innerHt["topicId"].ToString());
								Globals.specialTopic.topicName = innerHt["topicName"].ToString();
								Globals.specialTopic.endDate = innerHt["topicPeriod"].ToString().Split('|')[1];
								Globals.specialTopic.isEnded = (Int32.Parse(innerHt["topicIsEnd"].ToString()) == 1);
							}	
						}
					}
		
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("GetLatestTopics",kvp, result => response = result));
		
		// to avoid bugs, always switch to normal topic
		
		StartCoroutine(ReallyAwake());
		topicBoards[0].GetComponent<AnchorTweener>().Forward();
		topicBoards[1].GetComponent<AnchorTweener>().Backward();

	}
	
	public void GoToStreetScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.STREET, true);	
	}
	
	public void GoToVotingClosetScene()
	{
		//SceneManager.SharedManager.LoadScene(Scenes.VOTINGCLOSET, true);	
	}
	
	public void GoToVotingLobbyScene()
	{
		//SceneManager.SharedManager.LoadScene(Scenes.VOTINGLOBBY, true);	
	}
	
	public void LocalizeButtonText()
	{
	}

}


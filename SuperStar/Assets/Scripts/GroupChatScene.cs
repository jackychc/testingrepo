using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class GroupChatScene : MonoBehaviour
{
	GameObject newCharacter, friendFemale;




	IEnumerator Start()
	{


		newCharacter = Globals.globalCharacter;
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference3A"));
		yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
	
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("visitL_idle");


		if (Friends.friends.Count >= 1)
		{
			friendFemale = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			friendFemale.name = "FRIENDFEMALE1";
			friendFemale.AddComponent<CharacterManager>();
			friendFemale.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference3B"));
			yield return StartCoroutine(friendFemale.GetComponent<CharacterManager>().EnumGenerateCharacter(Friends.friends[UnityEngine.Random.Range(0,Friends.friends.Count)]));

		}
		if (Friends.friends.Count >= 2)
		{
			friendFemale = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			friendFemale.name = "FRIENDFEMALE2";
			friendFemale.AddComponent<CharacterManager>();
			friendFemale.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference3C"));
			yield return StartCoroutine(friendFemale.GetComponent<CharacterManager>().EnumGenerateCharacter(Friends.friends[UnityEngine.Random.Range(0,Friends.friends.Count)]));

		

		}
		
		
	
		SoundManager.SharedManager.ChangeMusic("SuperstarStory_home_remix0");

	}
	
	void OnDestroy()
	{

	}

	void GoToHomeScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.HOME, false);
	}


	string RandomIdleAnimationString(string t)
	{
		int ran = UnityEngine.Random.Range(1,5+1);
		
		if (t.Equals("b") && VisitFriend.friend.CurrentBoyfriendId == 7) return (ran%2 == 0? "BF_pose02":"BF_idle");
		
		switch(ran)
		{
			//abcde
			
			case 1: return "BF_pose01";
			case 2: return "BF_pose02";
			default: return "BF_idle"; 
			
		}
		
		return "BF_idle";
	}
}



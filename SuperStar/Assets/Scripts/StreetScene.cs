using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StreetScene : MonoBehaviour, PopupManagerCallbackInterface{
	
	public GameObject offersButtonObj;
	public GameObject votingButton;
	public static bool hasShownRatingPopup = false;
	public static bool firstEnter;	
	StreetAnimationManager sam;
	GameObject newCharacter;
	float t 	= 0;	
	int testAni = 0;
	
	void Awake()
	{		
		votingButton.SetActive(false);		
	}

	//update please
	// Use this for initialization
	IEnumerator Start ()
	{
		
		StartCoroutine(LoopUntilHaveTopic());
		
		/*if (GameObject.Find("FEMALE") == null)
		{
			//newCharacter = (GameObject) Instantiate((Resources.Load("CharacterAssets/test/test_fullcloth/girl3_fullcloth4@test_full") as GameObject));
			newCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			newCharacter.name = "FEMALE";
			newCharacter.transform.parent = GameObject.Find("GirlParent").transform;
			newCharacter.AddComponent<CharacterManager>();
			yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));			
		}		*/
		//GameObject.Find("GirlParent").transform.localPosition = Vector3.zero;
		Transform gp = Globals.globalCharacter.transform.parent;
		gp.gameObject.SetActive(true);
		gp.localPosition = Vector3.zero;
		gp.localRotation = Quaternion.identity;
		gp.localScale = Vector3.one;
		
		Globals.globalCharacter.GetComponent<CharacterManager>().ResetPRS();
		yield return StartCoroutine(Globals.globalCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
			
		// set scroll position
		GameObject sp = GameObject.Find("ScrollPanel");
		SpringPanel.Begin(GameObject.Find("ScrollPanel"), new Vector3(Globals.mainPlayer.StreetPosition,
			sp.transform.localPosition.y,
			sp.transform.localPosition.z), 10000f);
		
		SoundManager.SharedManager.ChangeMusic("SuperstarStory_home_remix0");
		
		if (!firstEnter)
		{
			// ....... GET FDS
			firstEnter = true;
			
			StartCoroutine(GetFriendsFromServer());
		}
		
		//PopupManager.ShowChooseGiftPopup(new List<Reward>(){Rewards.getReward(2),Rewards.getReward(3),Rewards.getReward(4)}, 2);
		PopupManager.SetCallback(this);
		
		//Items.AllQuery();

		CheckIfShowRatePopup();
		CheckDailyBonus();		
		Checkinterstitial();		
		CheckOfferButtons();
	}
	
	void OnDestroy()
	{		
		PopupManager.RemoveCallback(this);	
	}
	
	private void Checkinterstitial()
	{
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.STREET);
	}
	
	private void CheckOfferButtons()
	{
		if (offersButtonObj != null)
		{
			bool hasOffers = MunerisManager.SharedManager.HasOffers();
			offersButtonObj.SetActive(hasOffers);	
		}
	}

	private void CheckIfShowRatePopup()
	{
		if (!hasShownRatingPopup)
		{
			if (Globals.canShowRatePopup)
			{
				if (Globals.mainPlayer.NumOfSessions == 5 || Globals.mainPlayer.NumOfSessions % 10 == 0)
				{
					hasShownRatingPopup = true;
					PopupManager.ShowRatingPopup(false);
				}
			}
		}
	}

	void CheckDailyBonus ()
	{
		if (!Globals.currentTimeStr.Equals(""))
		{
			DateTime serverTime = DateTime.Parse(Globals.currentTimeStr);
			//Log.Debug("current time?"+ serverTime);
			//Log.Debug("stamp?"+ Globals.mainPlayer.DailyBonusStamp);
			TimeSpan span = serverTime - Globals.mainPlayer.DailyBonusStamp;
			if (span.TotalHours >= 20)
			{
				PopupManager.ShowDailyBonusPopup(false);
			}
		}


	}
	
	void SaveStreetPosition()
	{
		Globals.mainPlayer.StreetPosition = (int) (GameObject.Find("ScrollPanel").transform.localPosition.x);
		//Log.Debug("save"+ Globals.mainPlayer.StreetPosition);
	
	}
	
	IEnumerator LoopUntilHaveTopic()
	{
		if (TopicManager.haveTopic)
		{
			votingButton.SetActive(true);
		}			
		else
		{
			yield return new WaitForSeconds (0.5f);
			StartCoroutine(LoopUntilHaveTopic());
		}
	}
	
	public void ShowBFMinimap()
	{
		//PopupManager.ShowBoyfriendMinimapPopup(false);
		SceneManager.SharedManager.LoadScene(Scenes.BOYFRIENDMAP, false);
	}
	
	public void HideHuman()
	{
		StartCoroutine(DelayedHide());
	}
	
	IEnumerator DelayedHide()
	{
		yield return new WaitForSeconds(0.1f);
		newCharacter.SetActive(false);
	}
	public void ShowHumanAgain()
	{
		newCharacter.SetActive(true);
	}	
	
	public void ShowATM()
	{
		PopupManager.ShowIAPPopup(1, false);
	}
	
	public void GoToHomeScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.HOME, true);	
	}
	
	public void GoToJobScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.JOB, true);
	}
	
	
	IEnumerator GetFriendsFromServer()
	{		
		//Prepare CheckSum
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("GetFriends", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("checkSum", checkSum);		
		
		string url = Globals.serverAPIURL + "GetFriends.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response 			= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			//yield return StartCoroutine(HTTPPost.Post("GetFriends", kvp, result => response = result));
			if (response != null && !response.Equals(""))
			{
				ParseFriends(response);
			}
		}
		//yield return StartCoroutine(HTTPPost.Post("GetFriends", kvp, result => response = result));
		
		PopupManager.HideLoadingFlower();
	}
	
	void ParseFriends(string input)
	{
		Log.Debug("input?" + input);
		//add players to friends.friends
		ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(input)).Values);
		if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
		{
			// have friend
			if (al[1] != null)
			{
				ArrayList fl =  new ArrayList(((Hashtable) al[1]).Values);
				
				foreach (Hashtable fh in fl)
				{
					Friends.AddFriend(fh);
				}
			}
		}
	}
	
	public void GirlClicked()
	{
		Log.Debug("Girl Clicked");	
	}
	
	#region Button Clicked
	public void OffersButtonClicked()
	{
		MunerisManager.SharedManager.ShowOffers();	
	}
	#endregion
	
	#region Popup Delegate
	public void OnPopupManagerHidePopup(GameObject popup)
	{
		if (popup.name.Equals("IAPPopup"))
		{
			Log.Debug("StreetScene: callback from IAPPopup Hide");
			GameObject streetAnimManagerObj = GameObject.Find("Street Animation Manager");
			streetAnimManagerObj.GetComponent<StreetAnimationManager>().SetATMHintsObjEnable(!Globals.isIAP);
		}
		else if (popup.name.Equals("BoyfriendMinimapPopup"))
		{
			ShowHumanAgain();
		}
	}
	
	public void OnPopupManagerShowPopup(GameObject popup)
	{
		if (popup.name.Equals("BoyfriendMinimapPopup"))
		{
			HideHuman();
		}
	}
	#endregion
}

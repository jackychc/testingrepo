using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClosetDLCItem : MonoBehaviour, DLCAskPopupCallbackInterface
{

	// Use this for initialization
	void Start ()
	{

	}
	

	void ItemClicked()
	{
		Log.Debug("Clicked lor!");
		

		PopupManager.ShowDLCAskPopup(this, true, 1, false);
	}

	#region DLCAskPopup Callback
	public void OnDLCAskPopupYes()
	{
		DownloadManager.SharedManager.AskForDLCUpgrade(false, -1, 0);
	}

	public void OnDLCAskPopupNo()
	{

	}
	#endregion
}


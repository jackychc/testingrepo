using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class VotingInformationBoard : MonoBehaviour {
	
	float startCoor = -215f;
	float endCoor = 220f;
	
	public Topic topic;
	
	public UISysFontLabel votesTag, rankTag;
	public UILabel votesNumber, rankNumber, messageNumber;
	public UILabel[] tierNumbers = new UILabel[3];
	public GameObject[] tierSprites = new GameObject[3];
	public UILabel[] timeRemain = new UILabel[4];
	public UIFilledSprite progressSprite;
	public int secondsRemain;
	
	public int votes, rank;
	
	void Start()
	{

		reinitialize();
		
		StartCoroutine(UpdateEverySecond());
	}
	
	public void UpdateInformation(Hashtable ht)
	{
		StartCoroutine(delayedUpdate(ht));
	}
	
	public void UpdateCachedInformation()
	{
		
		votesNumber.text = Globals.viewingTopic.votes +"";
		rankNumber.text = Globals.viewingTopic.rank +"";
		UpdateProgress();
		UpdateTier();
	}
	
	IEnumerator delayedUpdate(Hashtable ht)
	{
		
		// 0.05s
		
		
		votes = ht["votes"] != null? Int32.Parse(ht["votes"].ToString()) : 0;
		rank = ht["rank"] != null? Int32.Parse(ht["rank"].ToString()) : 0;
		secondsRemain = Int32.Parse(ht["secondsRemain"].ToString());
		
		
		
		Globals.viewingTopic.votes = votes;
		Globals.viewingTopic.rank = rank;
		
		
		
		
		UpdateProgress();
		UpdateTier();
		//StartCoroutine(UpdateEverySecond());
		yield return null;
	}

	
	public void UpdateTimeRemain()
	{
		int sec = secondsRemain;
		
		timeRemain[0].text = twoDigit(sec/86400);
		timeRemain[1].text = twoDigit((sec%86400)/3600);
		timeRemain[2].text = twoDigit((sec%3600)/60);
		timeRemain[3].text = twoDigit((sec%3600)%60);
	}
	string twoDigit(int i)
	{
		return (i < 10)? ("0"+i):(i+"");
	}
	
	public void UpdateProgress()
	{
		votesNumber.text = Globals.viewingTopic.votes +"";
		rankNumber.text = Globals.viewingTopic.rank+"";
		
		Log.Debug ("????"+Globals.viewingTopic.votes+","+Globals.viewingTopic.rank);
		List<int> tiers = Globals.viewingTopic.getCountTiers();

		float progress = Mathf.Min(1.0f, 1.0f*Globals.viewingTopic.votes/tiers[tiers.Count -1]);
		progressSprite.fillAmount = progress;
		
		for (int i = 0; i < tiers.Count ;i++)
		{
			tierSprites[i].SetActive(true);
			if (Globals.viewingTopic.votes >= tiers[i])
			{
				// not enough votes
				Colorize(i);
			}
			
			//tierSprites[i].SetActive(true);
		}
		
		
	}
	
	public void UpdateTier()
	{
		List<int> tiers = Globals.viewingTopic.getCountTiers();
		
		//Log.Debug("tiers:"+tiers[0]+","+tiers[1]+","+tiers[2]);
		

		for (int i = 0; i < tiers.Count; i++)
		{
			tierNumbers[i].text = tiers[i] + "";
			
			Vector3 temp = tierNumbers[i].transform.localPosition;
			temp.x = Mathf.Lerp(startCoor, endCoor, Mathf.InverseLerp(0, tiers[tiers.Count-1], tiers[i]));
			tierNumbers[i].transform.localPosition = temp;
			
			//if (i <= 1)
			tierSprites[i].transform.localPosition = temp;
			
			if (i == tiers.Count-1)
			{
				string spriteName = "vote_bigGiftG";
				UIImageButton ib = tierSprites[i].GetComponent<UIImageButton>();
				ib.normalSprite = spriteName;
				ib.hoverSprite = spriteName;
				ib.pressedSprite = spriteName;
				
				Transform bg = tierSprites[i].transform.FindChild("Background");
				bg.GetComponent<UISprite>().spriteName = spriteName;
				bg.localScale = new Vector3(81f,89f,1f);
				
			}
			else
			{
				string spriteName = "vote_smallGiftG";
				UIImageButton ib = tierSprites[i].GetComponent<UIImageButton>();
				ib.normalSprite = spriteName;
				ib.hoverSprite = spriteName;
				ib.pressedSprite = spriteName;
				
				Transform bg = tierSprites[i].transform.FindChild("Background");
				bg.GetComponent<UISprite>().spriteName = spriteName;
				bg.localScale = new Vector3(57f,62f,1f);
			}
			
			
			tierSprites[i].SetActive(true);
			if (!Globals.viewingTopic.getTierAt(i).claimed && Globals.viewingTopic.votes >= tiers[i] )
			{
				// claimed, make it invisble? or grey? dunno
				Colorize(i);
			}
		}
	}
	
	public void Colorize(int tier)
	{
		UIImageButton ib = tierSprites[tier].GetComponent<UIImageButton>();
		UISprite isp = tierSprites[tier].transform.FindChild("Background").GetComponent<UISprite>();
		string spriteName = ib.normalSprite.TrimEnd('G');
		ib.hoverSprite = spriteName;
		ib.normalSprite = spriteName;
		ib.pressedSprite = spriteName;
		isp.spriteName = spriteName;
	}
	
	public void reinitialize()
	{
		secondsRemain = -1;
		if (timeRemain[0] != null)
		{
			timeRemain[0].text = "--";
			timeRemain[1].text = "--";
			timeRemain[2].text = "--";
			timeRemain[3].text = "--";
		}
		votesNumber.text = "--";
		rankNumber.text = "--";
		tierSprites[0].SetActive(false);
		tierSprites[1].SetActive(false);
		tierSprites[2].SetActive(false);
		tierNumbers[0].text = "";
		tierNumbers[1].text = "";
		tierNumbers[2].text = "";
		progressSprite.fillAmount = 0f;
	}
	
	IEnumerator UpdateEverySecond()
	{
		if (secondsRemain > 0)
		{
			secondsRemain--;
			UpdateTimeRemain();
		}
		
		yield return new WaitForSeconds(1.0f);	
		StartCoroutine(UpdateEverySecond());
		
	}
}

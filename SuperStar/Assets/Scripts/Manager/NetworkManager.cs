using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System;

public class NetworkManager : MonoBehaviour {
	public enum State
	{
		None,
		Connecting,
		Success,
		Fail
	}
	
	private string url				= "";
	private string error			= "";
	private string response 		= "";
	private byte[] bytes			= null;
	private AssetBundle bundle		= null;
	private Texture2D texture		= null;
	
	private State currState 		= State.None;
	private ArrayList callbackList 	= new ArrayList();
	
	private static volatile NetworkManager instance = null;
	public static NetworkManager SharedManager
	{
		get{
			return instance;	
		}
	}
	
	public Texture2D Texture
	{
		get
		{
			return texture;	
		}
	}
	
	public byte[] Bytes
	{
		get
		{
			return bytes;	
		}
	}
	
	public string Response
	{
		get
		{
			return response;	
		}
	}
	
	public string Error
	{
		get
		{
			return error;	
		}
	}
	
	public State NetworkState
	{
		get
		{
			return currState;	
		}
	}
	
	public string URL
	{
		get	
		{
			return url;	
		}
	}
	
	public void SetCallback(NetworkManagerCallback c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
			{
				callbackList.Remove(c);
			}
		}
		
		callbackList.Add(c);
	}
	
	public void RemoveCallback(NetworkManagerCallback c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
				callbackList.Remove(c);
		}
	}
	
	public void Reset()
	{
		currState 	= State.None;
		response 	= "";
		error 		= "";
		url			= "";
	}
	
	public IEnumerator ConnectURL(string url, Dictionary<string, string> parameters)
	{	
		WWW www 			= null;
		this.url			= url;
		currState 			= State.Connecting;
		response 			= "";
		
		
		if (parameters != null)
		{
			WWWForm form = new WWWForm();		
			foreach (KeyValuePair<string,string> kvp in parameters)
			{
				form.AddField(kvp.Key, kvp.Value);
			}
			www = new WWW(url, form);
		}
		else
		{
			www = new WWW(url);
		}		
		yield return www;
		
		Uri uri 			= new Uri(www.url);		
		string fileName		= Path.GetFileNameWithoutExtension(uri.LocalPath);
		if (www.error != null)
		{
			currState = State.Fail;
			error	  = www.error;
			
			Log.Debug("[NetworkManager]["+fileName+"] WWW Error: " + error);
		}
		else
		{
			currState 	= State.Success;
			response 	= www.text;
			
			if (www.bytes != null)
				bytes		= www.bytes;
			if (www.texture != null)
				texture		= www.texture;
			
			Log.Debug("[NetworkManager]["+fileName+"] WWW Ok!: " + response);
		}
		
		SendCallbackMessage(fileName);
	}
	
	private void SendCallbackMessage(string fileName)
	{
		if (callbackList != null)
		{
			if (currState.Equals(State.Fail))
			{
				for (int i = 0;i < callbackList.Count; i++)
				{
					NetworkManagerCallback c = (NetworkManagerCallback)callbackList[i];
					c.OnNetworkManagerConnectionFailed();
				}
			}
			else if (currState.Equals(State.Success))
			{
				for (int i = 0;i < callbackList.Count; i++)
				{
					NetworkManagerCallback c = (NetworkManagerCallback)callbackList[i];
					c.OnNetworkManagerConnectionSuccess();
				}
			}
		}		
	}
	
	// Use this for initialization
	void Start () 
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
			Destroy(this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class DatabaseManager : MonoBehaviour {

	public GameObject initLabel;

	public SQLiteAsync asyncDB 	= null;
	public SQLiteDB db 			= null;	
	private bool onInit 		= false;

	public float restoreProgress = 0.0f;
	public bool isRestoring = false;

	private List<string> asyncQueue = new List<string>();
	private bool isRunningAsync 	= false, saveRegularCheck = false;

	private bool isStartUp									= false;
	private static readonly object unzipTicket			 	= new object(); 
	private Dictionary<string, int> totalDLCList			= new Dictionary<string, int>();
	private Dictionary<int, int> successDLCList				= new Dictionary<int, int>();
	private Dictionary<int, int> failDLCList			 	= new Dictionary<int, int>();

	private ArrayList callbackList = new ArrayList();

	private static volatile DatabaseManager instance = null;

	public ValueCacher cacher = new ValueCacher();

	public static DatabaseManager SharedManager
	{
		get{
			return instance;
		}
	}

	public void SetCallback(DatabaseManagerCallback c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
			{
				callbackList.Remove(c);
			}

			callbackList.Add(c);
		}
	}

	public void RemoveCallback(DatabaseManagerCallback c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
				callbackList.Remove(c);
		}
	}

	public bool IsInitialized
	{
		get{
			return onInit;	
		}

		private set{}
	}

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.transform.gameObject);
		}
		else
			Destroy(this);
	}

	// Use this for initialization
	void Start () {
		//Open ();	
		CheckInitialized(true);	

	}

	#region Initialization
	public void CheckInitialized(bool isStartUp)
	{
		this.isStartUp = isStartUp;
		StartCoroutine(EnumCheckInitialized(isStartUp));
	}

	private IEnumerator EnumCheckInitialized(bool isStartUp)
	{
		Log.Debug("Start DatabaseManager Init");

		bool dbMoved = false;
		bool noDBInited = false;

		//Check Moved Database
		//Log.Debug(Globals.dbSavePath);

#if !UNITY_EDITOR
		FileInfo fileInfo = new FileInfo(Globals.dbSavePath);
		if (fileInfo == null || !fileInfo.Exists)
		{
			yield return StartCoroutine(MoveDatabase());
			dbMoved = true;
		}
#endif

		Log.Debug("Init Data");
		db = new SQLiteDB();
		/*
		db.Open(Globals.dbSavePath);*/
		#if !UNITY_WEBPLAYER
		db.Open(Globals.dbSavePath);
		#else
		MemoryStream memStream = new MemoryStream();
		db.OpenStream("dbStream", memStream);
		#endif

		if (db == null)
			Log.Debug("DB is still NULL!!!!!");
		db.Key(Globals.dbKey);

		Log.Debug("Create table Variables");
		// Table1. General (system level things)
		// playerId: points to self's profile
		string query = "CREATE TABLE IF NOT EXISTS Variables (Id INTEGER PRIMARY KEY, appVersion TEXT, dbVersion INTEGER, playerId INTEGER);";
		SQLiteQuery qr = new SQLiteQuery(db , query);
		qr.Step();
		qr.Release();

		Log.Debug("Select Rows From Variables");
		// test if the above table has any rows
		query = "SELECT * FROM Variables WHERE Id > 0";
		qr = new SQLiteQuery(db, query);
		if (!qr.Step())
		{
			noDBInited 	= true;
		}
		qr.Release();

		//#if !UNITY_EDITOR
		//db.Key(Globals.dbKey);
		//#endif		
		//asyncDB = new SQLiteAsync();
		//asyncDB.Open(Globals.dbSavePath,null,null);

		//if (asyncDB == null)
			//Log.Debug("async DB is still NULL!!!!!");
		//#if !UNITY_EDITOR	
		//asyncDB.Query("PRAGMA key='"+Globals.dbKey+"';", AsyncQueryCreated, null);
		//#endif	


		Log.Debug("Check DB Inited");
		if (noDBInited)
		{
			Log.Debug("Start Create Tables");
			yield return StartCoroutine(CreateTables());
			Log.Debug("Done Create Tables");

			Log.Debug("Start Init Tables");
			yield return StartCoroutine(InitTables());
			Log.Debug("Done Init Tables");

			/*//Used to Genereate an encrypted db
			query = "DELETE FROM PlayerMeta";
			qr = new SQLiteQuery(db, query);
			if (qr.ExecuteUpdate())
				Log.Debug("DELETED playerMeta");

			query = "DELETE FROM Players";
			qr = new SQLiteQuery(db, query);
			if (qr.ExecuteUpdate())
				Log.Debug("DELETED Players");

			yield break;*/

		}
		else if (dbMoved)
		{
			Log.Debug("Start Post-Init Tables");
			yield return StartCoroutine(PostInitTables());
			Log.Debug("Done Post-Init Tables");
		}

		UpdateDatabase();

		onInit = true;
		Log.Debug("Done Database Init");

		Log.Debug("Load Values");
		LoadValues();

		if (noDBInited || dbMoved)
		{
			//Generate Random Character
			string randomStr = CharacterManager.GenerateRandomDefaultWearingItemString();
			Log.Debug("Random Wearing items: " + randomStr);

			Globals.mainPlayer.wearingItems = Items.stringToItemList(randomStr);
			Globals.mainPlayer.Save();

			//Check Languages
			Log.Debug("Switch to Default Languages");
			LanguageCode langCode 	= UtilsHelper.GetDeviceLanguage();
			string langCodeStr		= Enum.GetName(typeof(LanguageCode), langCode);
			Globals.langCode 		= langCodeStr;

			//Globals.shouldShowDLCPopup = true;
		}
		LanguageManager.SwitchLanguage(Globals.langCode);


		if (Globals.globalCharacter == null)
		{
			Globals.globalCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			Globals.globalCharacter.name = "FEMALE";
			//Globals.globalCharacter.transform.parent = GameObject.Find("GirlParent").transform;
			Globals.globalCharacter.transform.localPosition = Vector3.zero;
			Globals.globalCharacter.AddComponent<CharacterManager>();
			yield return StartCoroutine(Globals.globalCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));

			DontDestroyOnLoad(Globals.globalCharacter);
		}

		Log.Debug("DB Check and Execute DLCs");
		Log.Debug("Num of DLCConfigs: " + DLCConfigs.allDlcConfigs.Count);

		int numOfPackagesUnzip = 0;
		for (int i = 0; i < DLCConfigs.allDlcConfigs.Count; i++)
		{
			DLCConfig dlcc = DLCConfigs.allDlcConfigs[i];
			dlcc.ConvertToListInside();

			if (dlcc.dlcId < Globals.currentDLCVersion)
				continue;

			Log.Debug("DB DLCPackages Count: " + dlcc.packages.Count + " in dlc: " + dlcc.dlcId);

			List<DLCPackage> packages = dlcc.packages;
			if (packages != null && packages.Count > 0)
			{
				numOfPackagesUnzip += packages.Count;

				//failDLCList[dlcc.dlcId] = dlcc.packages.Count;
				for (int j = 0; j < packages.Count; j++)
				{
					DLCPackage package 	= packages[j];
					string zipFileName	= Path.GetFileName(package.url);
					string zipFilePath	= Application.persistentDataPath + "/" + zipFileName;
					string savePath		= Application.persistentDataPath;

					totalDLCList[zipFilePath] = dlcc.dlcId;
					string callbackObj = this.gameObject.name;
					UtilsHelper.UnzipFile(zipFilePath, savePath, callbackObj);
				}

				SysFontLocalizer localizer = initLabel.GetComponent<SysFontLocalizer>();
				localizer.identifier = "dlc_committing";
				localizer.ReloadText();
			}
			else
			{
				//For no item packages dlc or App Update

				string zipFilePath = "AppUpdateFilePath" + dlcc.dlcId;
				successDLCList[dlcc.dlcId] = 1;
				totalDLCList[zipFilePath] = dlcc.dlcId;
			}
		}

#if !UNITY_EDITOR
		if (numOfPackagesUnzip == 0)
		{
			Log.Debug("There is no DLC Packages needed to unzip, now execute DLC");
			try
			{
				DLCConfigs.ExecuteDLC();
			}
			catch(Exception e)
			{
				Log.Debug("Execute DLC with dlcId failed: " + e.Message);

				Hashtable param = new Hashtable();
				param["Failed"] = "ExecuteALL";
				MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.DLC_State, param);
			}
			InitializedCompleted();
		}
#else
		try
		{
			DLCConfigs.ExecuteDLC();
		}
		catch(Exception e)
		{
			Log.Debug("Execute DLC with dlcId failed: " + e.Message);

			Hashtable param = new Hashtable();
			param["Failed"] = "ExecuteALL";
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.DLC_State, param);
		}
		InitializedCompleted();
#endif
	}


	private void InitializedCompleted()
	{
		if (isStartUp)
		{
			//Auto redirect to TitleScene
			Application.LoadLevel("Title");
		}
	}

	private void processUnzippedDLC(string zipFilePath, bool isSuccess)
	{
		lock(unzipTicket)
		{
			//Record the success Package
			if (isSuccess)
			{
				int dlcId = totalDLCList[zipFilePath];

				if (!successDLCList.ContainsKey(dlcId))
					successDLCList[dlcId] = 1;
				else
					successDLCList[dlcId]++;

				File.Delete(zipFilePath);
			}
			else
			{
				int dlcId = totalDLCList[zipFilePath];
				if (!failDLCList.ContainsKey(dlcId))
					failDLCList[dlcId] = 1;
				else
					failDLCList[dlcId]++;

				File.Delete(zipFilePath);
			}

			List<int> totalDLCIds 	= new List<int>();
			int successCount		= 0;
			int failCount			= 0;

			//Check how many Success case
			if (successDLCList != null && successDLCList.Count > 0)
			{
				foreach(int successDLCId in successDLCList.Keys)
				{
					//if (!totalDLCIds.Contains(successDLCId))
					//totalDLCIds.Add(successDLCId);
					successCount++;
				}
			}

			//Check how many fail case
			if (failDLCList != null && failDLCList.Count > 0)
			{
				foreach(int failDLCId in failDLCList.Keys)
				{
					//if (!totalDLCIds.Contains(failDLCId))
					//totalDLCIds.Add(failDLCId);
					failCount++;
				}
			}

			if (successCount + failCount == totalDLCList.Count)
			{
				//Add the dlcId to totalDLCIds
				foreach(string zipFilePathKey in totalDLCList.Keys)
				{
					int dlcId = totalDLCList[zipFilePathKey];
					totalDLCIds.Add(dlcId);
				}

				//Remove the fail case in Success List
				foreach(int failDLCId in failDLCList.Keys)
				{
					if (successDLCList.ContainsKey(failDLCId))
						successDLCList.Remove(failDLCId);
				}

				//Execute the DLC by each success case
				totalDLCIds.Sort();
				for (int i = 0; i < totalDLCIds.Count; i++)
				{
					int dlcId = totalDLCIds[i];

					if (Globals.currentDLCVersion >= dlcId)
						continue;

					//Check if one of the success package is failed
					int numOfPackages = 0;
					if (successDLCList.ContainsKey(dlcId))
					{
						numOfPackages = successDLCList[dlcId];
					}
					if (numOfPackages == 0)
						break;

					try
					{
						DLCConfigs.ExecuteDLC(dlcId);
					}
					catch(Exception e)
					{
						Log.Debug("Execute DLC with dlcId failed: " + e.Message);

						Hashtable param = new Hashtable();
						param["Failed"] = "Execute_" + dlcId;
						MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.DLC_State, param);
					}
				}

				InitializedCompleted();
			}	
		}
	}
	#endregion

	#region Native callback From UtilsHelper
	void onZipHelperUnzipSuccess(string zipFilePath)
	{
		Log.Debug("DB Unzip file success!! The zipFilePath is: " + zipFilePath);
		processUnzippedDLC(zipFilePath, true);
	}

	void onZipHelperUnzipFailed(string zipFilePath)
	{
		Log.Debug("DB Unzip file Failed!! The zipFilePath is: " + zipFilePath);
		processUnzippedDLC(zipFilePath, false);
	}
	#endregion



	#region Database Movement
	private IEnumerator MoveDatabase()
	{

		byte[] bytes = null;	
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
		string dbpath = "file://" + Application.streamingAssetsPath + "/" + Globals.dbName;
		WWW www = new WWW(dbpath);
		yield return www;
		if (www.error != null)
		{
			Log.Debug("DB Cannot read database from path: " + dbpath);
		}
		else
		{
			if (www.bytes != null)
			{
				bytes = www.bytes;
			}
			else
			{
				Log.Debug("DB Cannot load the database file from path: " + dbpath);
			}
		}

		#elif UNITY_IPHONE
		string dbpath = Application.dataPath + "/Raw/" + Globals.dbName;		
		try{	
			using ( FileStream fs = new FileStream(dbpath, FileMode.Open, FileAccess.Read, FileShare.Read) ){
				bytes = new byte[fs.Length];
				fs.Read(bytes,0,(int)fs.Length);
			}			
		} catch (Exception e){
			Log.Debug("Test Fail with Exception " + e.ToString());
		}
		yield return null;
		#elif UNITY_ANDROID
		string dbpath = Application.streamingAssetsPath + "/" + Globals.dbName;
		WWW www = new WWW(dbpath);
		yield return www;
		if (www.error != null)
		{
			Log.Debug("DB Cannot read database from path: " + dbpath);
		}
		else
		{
			if (www.bytes != null)
			{
				bytes = www.bytes;
			}
			else
			{
				Log.Debug("DB Cannot load the database file from path: " + dbpath);
			}
		}
		#endif
		if ( bytes != null )
		{
			try
			{
				// copy database to real file into cache folder
				using( FileStream fs = new FileStream(Globals.dbSavePath, FileMode.Create, FileAccess.Write) )
				{
					fs.Write(bytes,0,bytes.Length);
					Log.Debug("Copy database from streaminAssets to persistentDataPath: " + Globals.dbSavePath);
				}

			} catch (Exception e){
				Log.Debug("Copy Error: " + e.ToString());
			}
		}	
	}	
	#endregion

	#region Create + Init Tables + Load Values
	private void UpdateDatabase()
	{
		SQLiteQuery qr 		= null;
		int oldDBVersion 	= -1;

		string variableSql = "SELECT * FROM Variables WHERE playerId = -1";
		qr = new SQLiteQuery(db, variableSql);
		if (qr.Step())
		{
			//DB Exists 
			oldDBVersion 	= qr.GetInteger("dbVersion");
		}
		qr.Release();

		//Execute the query file if any
		if (oldDBVersion < Globals.dbVersion)
		{
			for (int i = oldDBVersion; i < Globals.dbVersion; i++)
			{
				string fileName 		= "db_queryfile_" + i;
				TextAsset queryFile 	= Resources.Load("DB Query/"+fileName) as TextAsset;
				bool isSuccess			= true;

				if (queryFile != null)
				{
					string text 		= queryFile.text;											
					string[] queries 	= text.Split(';');

					if (queries != null && queries.Length > 0)
					{
						try
						{
							BeginTransaction();

							for(int j = 0; j < queries.Length; j++)
							{
								string query = queries[j];
								Log.Debug("Execute the DB query: " + query);

								qr = new SQLiteQuery(db, query.Replace("\n", ""));
								qr.Step();
								qr.Release();
							}

							Commit();

						}
						catch
						{
							Log.Debug("ROLLBACKED!");
							isSuccess = false;
							Rollback();
						}
					}
				}

				if (isSuccess)
				{
					//Update the dbVersion in Variables
					string sql = "UPDATE Variables SET dbVersion = " + (i + 1) + " WHERE playerId = -1";
					qr = new SQLiteQuery(db, sql);
					qr.Step();
					qr.Release();

					Log.Debug("Update DB Version to: " + (i + 1));
				}
			}
		}
		else
		{
			Log.Debug("DB already in latest version!");
		}
	}

	private void LoadSaveData()
	{
		QuestFlags.Load();
		Boyfriends.Load();
		Globals.mainPlayer.Load();
	}

	private void LoadValues()
	{
		Items.Load();
		ItemConflicts.Load();
		ExpTable.Load();
		Rewards.Load();
		Jobs.Load();
		Quests.Load();
		QuestFlags.Load();
		Posters.Load();
		Shops.Load();
		Boyfriends.Load();
		Features.Load();
		SkillTable.Load();
		Globals.mainPlayer.Load();
		Friends.Load();

		// this must be at last i think
		DLCConfigs.Load();	
	}


	public IEnumerator CreateTables()
	{
		string queryCreate = "";
		SQLiteQuery qr = null;

		queryCreate = "PRAGMA encoding=\"UTF-8\"";
		qr = new SQLiteQuery(db, queryCreate);
		qr.Step();
		qr.Release();

		// Table2. Player
		// player serial = 6-digit code for identity
		// 'level' is probably dummy, as we want to calculate it by exp table
		queryCreate = "CREATE TABLE IF NOT EXISTS Players (playerId INTEGER, playerName TEXT, level INTEGER, exp INTEGER, energy INTEGER, maxEnergy INTEGER, coins INTEGER, gems INTEGER, gameId TEXT, profilePicURL TEXT, boyfriendId INTEGER, skills TEXT, titleSkill INTEGER);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		queryCreate = "CREATE TABLE IF NOT EXISTS PlayerMeta (playerId INTEGER, metaKey TEXT, intValue INTEGER, strValue TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();				

		// Table2a. ExpTable
		// records TOTAL exp that reach the level
		// eg. Lv1 - 0, Lv2 = 100 (+100), Lv3 = 500 (+400), Lv4 = 1000 (+500)
		queryCreate = "CREATE TABLE IF NOT EXISTS ExpTable (level INTEGER, totalExp INTEGER);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();	

		// Table3. Activity (supertable for job/training/quest)
		// The main purpose of supertable is making countdown easier
		// sqlite don't have time format, so use INTEGER to store times (seconds)
		queryCreate = "CREATE TABLE IF NOT EXISTS Activities (activityId INTEGER, title TEXT, description TEXT, timeRequired INTEGER, timeLeft INTEGER, gemsToFinish INTEGER, rewardId INTEGER);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();	

		// then why need to separate 3 tables? seems structure is the same o.o"
		// ^ for easier management/update
		// Table3a. Job
		queryCreate = "CREATE TABLE IF NOT EXISTS Jobs (jobId INTEGER, sortOrder INTEGER, activityId INTEGER,levelRequired INTEGER, expireTime INTEGER,  visibleForSkill TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		// Table3c. Quest
		queryCreate = "CREATE TABLE IF NOT EXISTS Quests (questId INTEGER, sortOrder INTEGER, activityId INTEGER, isRunning TEXT, isCompleted TEXT, completedOnce TEXT, isNew TEXT, canRepeat TEXT, canSkip TEXT, actionClass TEXT, goToSceneName TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		queryCreate = "CREATE TABLE IF NOT EXISTS QuestFlags (flagId INTEGER, description TEXT, intValue INTEGER, strValue TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		// Table4. Reward
		// remainder: times that the reward can be claimed, -1 = infinity
		// possible to use better names rather than ABCDE?
		queryCreate = "CREATE TABLE IF NOT EXISTS Rewards (rewardId INTEGER, remainder INTEGER, coins INTEGER, gems INTEGER, exp INTEGER, energy INTEGER, itemIds TEXT, skills TEXT );";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		// Table5. Voting
		// also for history of past votings
		queryCreate = "CREATE TABLE IF NOT EXISTS Votings (votingId INTEGER, topic TEXT, playerId INTEGER, rank INTEGER, votes INTEGER);";
		;qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();


		// Table6. DLC
		queryCreate = "CREATE TABLE IF NOT EXISTS Dlcs (dlcId TEXT, dlcVersion TEXT, dlcContent TEXT, dlcLevel INTEGER);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		// Table7. Boyfriend
		queryCreate = "CREATE TABLE IF NOT EXISTS Boyfriends (boyfriendId INTEGER, boyfriendName TEXT, title TEXT, meta TEXT, meshName TEXT, flirtExp INTEGER, flirtExpMax INTEGER, charismaRequired INTEGER, location TEXT, rewardIds TEXT, flirtCost TEXT, flirtRepeat TEXT, instantGiftCost TEXT, rewardClaimed TEXT, rewardTimer INTEGER, dlcId INTEGER);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		// Table8. Item
		// visibility = 'true' or 'false'
		queryCreate = "CREATE TABLE IF NOT EXISTS Items (itemId INTEGER, itemName TEXT, visibility TEXT, subcategoryId INTEGER, brandId INTEGER, price INTEGER, gems INTEGER, requiredLevel INTEGER, featureId INTEGER, charisma TEXT, rewardId INTEGER, dlcId INTEGER, meta TEXT, texMeta TEXT, colorMeta TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();	

		// Table8x. Items Conflict
		queryCreate = "CREATE TABLE IF NOT EXISTS ItemConflicts (item1Id INTEGER, item2Id INTEGER, conflictType TEXT, conflictAction TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		// Table8a. Item Category
		// eg. top, head, bottom
		queryCreate = "CREATE TABLE IF NOT EXISTS ItemCategory (categoryId INTEGER, categoryName TEXT, categoryDescription TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();	

		// Table8b. Item Category - Subcategory
		queryCreate = "CREATE TABLE IF NOT EXISTS ItemCategoriesToSubcategories (categoryId INTEGER, subcategoryId INTEGER);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();	

		// Table8c. Item Subategory
		// eg. eyelash, shirt, shoes, socks
		queryCreate = "CREATE TABLE IF NOT EXISTS ItemSubcategories (subcategoryId INTEGER, subcategoryName TEXT, subcategoryDescription TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();	

		// Table8d. Item Brand
		// eg. LV, H&M, CK
		queryCreate = "CREATE TABLE IF NOT EXISTS ItemBrands (brandId INTEGER, brandName TEXT, brandDescription TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();		

		// Table11. Feature
		queryCreate = "CREATE TABLE IF NOT EXISTS Features (featureId INTEGER, featureName TEXT, featureType TEXT, itemIds TEXT, shopIds TEXT, subcategoryIds TEXT, iapPackageIds TEXT, coins INTEGER, gems INTEGER, discount INTEGER, period TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();		

		// Table9. Shop
		queryCreate = "CREATE TABLE IF NOT EXISTS Shops (shopId INTEGER, shopName TEXT, itemId TEXT, bgmFilename TEXT, iconName TEXT, hideSubcats TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		// Table10. Player - Item
		// isWearing = 'true' or 'false'
		queryCreate = "CREATE TABLE IF NOT EXISTS PlayersToItems (playerId INTEGER, purchasedItems TEXT, wearingItems TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		// Player to Social
		queryCreate = "CREATE TABLE IF NOT EXISTS PlayersToSocials (playerId INTEGER, socialType TEXT, socialId TEXT, appearName TEXT, loginName TEXT, loginPassword TEXT);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();			

		// Table12. Posters
		// isWearing = 'true' or 'false'
		queryCreate = "CREATE TABLE IF NOT EXISTS Posters (posterId INTEGER, posterPeriod TEXT, posterProbability INTEGER, posterRedirectScene TEXT, rewardId INTEGER, posterURL TEXT, posterLocation INTEGER, dlcId INTEGER);";
		qr = new SQLiteQuery(db, queryCreate); 
		qr.Step();
		qr.Release();

		yield return null;
	}

	public IEnumerator InitTables()
	{
		Log.Debug("Init Tables");
		//Check GameId
		string gameIdGen = Globals.mainPlayer.GameId;
		if (Globals.mainPlayer.GameId.Equals(""))
			yield return StartCoroutine(GenerateGameId(result => gameIdGen = result));
			//yield return null;

		Log.Debug("The GameId is: " + gameIdGen);

		InitTablePlayer(gameIdGen);
		InitTablePlayerMeta();
		InitTableExp();
		InitTableJobs();
		InitTableQuest();
		InitTableQuestFlags();
		InitTableRewards();
		InitTableItems();
		InitTableItemConflicts();
		InitTablePlayersToItems();
		InitTablePlayersToSocials();
		InitTableShops();
		InitTableBoyfriends();
		InitTableVariables();

		yield return null;
	}

	public IEnumerator PostInitTables()
	{
		//!
		// refreshing data
		string gameIdGen = "";
		//if (Globals.mainPlayer.GameId.Equals(""))
		yield return StartCoroutine(GenerateGameId(result => gameIdGen = result));

		InitTablePlayer(gameIdGen);
		InitTablePlayerMeta();
	}

	private void InitTableVariables()
	{
		Log.Debug("Init Table Variables");

		string queryInsert = "";
		SQLiteQuery qr = null;

		queryInsert = "INSERT INTO Variables (appVersion, dbVersion, playerId) VALUES(?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.bundleVersion);
		qr.Bind(Globals.dbVersion);
		qr.Bind(Globals.mainPlayer.playerId); // always -1
		qr.Step();
		qr.Release();	
	}

	private void InitTablePlayer(string gameIdGen)
	{
		Log.Debug("Init Table Player");

		string queryInsert;
		SQLiteQuery qr = null;

		queryInsert = "INSERT INTO Players (playerId, playerName,level,exp, energy, maxEnergy, coins,gems, gameId, profilePicURL, boyfriendId, skills, titleSkill) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.DEFAULT_playerId);
		qr.Bind(gameIdGen.Equals("")? "Player" : "P"+gameIdGen); // player name
		qr.Bind(Globals.DEFAULT_level);
		qr.Bind(0/*Globals.DEFAULT_exp*/);
		qr.Bind(19+1/*Globals.DEFAULT_energy*/);
		qr.Bind(18+2/*Globals.DEFAULT_maxEnergy*/);
		qr.Bind(14998+2/*Globals.DEFAULT_coins*/);
		qr.Bind(96+4/*Globals.DEFAULT_gems*/);
		qr.Bind(gameIdGen);
		qr.Bind(Globals.DEFAULT_profilePicURL);
		qr.Bind(Globals.DEFAULT_boyfriendId);
		qr.Bind(Skill.GetSkillAsString(Globals.DEFAULT_skills));
		qr.Bind(Globals.DEFAULT_titleSkill);
		qr.Step();
		qr.Release();

		cacher.setCoins(13998+1002);
		cacher.setGems(97+3);
		cacher.setExp(0);
		cacher.setEnergy(19+1);
		cacher.setMaxEnergy(18+2);
	}

	public void InitTablePlayerMeta()
	{
		Log.Debug("Init Table Player Meta");

		string queryInsert = "";
		SQLiteQuery qr = null;

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("initializedPlayer");
		qr.Bind(Globals.DEFAULT_initializedPlayer);
		qr.Bind(" ");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("numOfSessions");
		qr.Bind(Globals.DEFAULT_numOfSessions);
		qr.Bind(" ");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("energyTimer");
		qr.Bind(Globals.DEFAULT_energyTimer);
		qr.Bind(" ");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("energyInterval");
		qr.Bind(Globals.DEFAULT_energyInterval);
		qr.Bind(" ");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("streetPosition");
		qr.Bind(Globals.DEFAULT_streetPosition);
		qr.Bind(" ");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("minimapPosition");
		qr.Bind(0);
		qr.Bind(Globals.DEFAULT_minimapPosition);
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("dlcVersion");
		qr.Bind(Globals.DEFAULT_dlcVersion);
		qr.Bind(" ");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("dlcProgress");
		qr.Bind(0);
		qr.Bind(" ");
		qr.Step();
		qr.Release();

		DateTime now = DateTime.UtcNow;
		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("timeStamp");
		qr.Bind(0);
		qr.Bind((now.Year+"-"+now.Month+"-"+now.Day+"-"+now.Hour+"-"+now.Minute+"-"+now.Second));
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("visitedGameIds");
		qr.Bind(0);
		qr.Bind(Globals.DEFAULT_visitedGameIds);
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("currentQuestIds");
		qr.Bind(0);
		qr.Bind(Globals.DEFAULT_currentQuests);
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("clothMatchTotalScore");
		qr.Bind(Globals.DEFAULT_clothMatchTotalScore);
		qr.Bind(" ");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("clothMatchHighScore");
		qr.Bind(Globals.DEFAULT_clothMatchHighScore);
		qr.Bind(" ");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("lastSyncDate");
		qr.Bind(0);
		qr.Bind((now.Year+"-"+now.Month+"-"+now.Day+"-"+now.Hour+"-"+now.Minute+"-"+now.Second));
		qr.Step();
		qr.Release();

		DateTime d = Globals.DEFAULT_dailyBonusStamp;
		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("dailyBonusStamp");
		qr.Bind(0);
		qr.Bind((d.Year+"-"+d.Month+"-"+d.Day+"-"+d.Hour+"-"+d.Minute+"-"+d.Second));
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("dailyBonusDay");
		qr.Bind(Globals.DEFAULT_dailyBonusDay);
		qr.Bind("");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("settings_bgm");
		qr.Bind(0);
		qr.Bind("true");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("startPlayTime");
		qr.Bind(0);
		qr.Bind(Globals.DEFAULT_startPlayTime);
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("isCracked");
		qr.Bind(0);
		qr.Bind(Globals.DEFAULT_isCracked);
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert);
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("isIAP");
		qr.Bind(0);
		qr.Bind(Globals.DEFAULT_isIAP);
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("hasFirstDLC");
		qr.Bind(0);
		qr.Bind(Globals.DEFAULT_hasFirstDLC);
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("scrolledQuest");
		qr.Bind(0);
		qr.Bind(Globals.DEFAULT_scrolledQuest.ToString());
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("iapRecord");
		qr.Bind(0);
		qr.Bind(Globals.DEFAULT_iapRecord);
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("bundleVersion");
		qr.Bind(0);
		qr.Bind(Globals.bundleVersion);
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("settings_sfx");
		qr.Bind(0);
		qr.Bind("true");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("settings_notification");
		qr.Bind(0);
		qr.Bind("true");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("settings_langCode");
		qr.Bind(0);
		qr.Bind("EN");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("rougeAlpha");
		qr.Bind(Globals.DEFAULT_rougeAlpha);
		qr.Bind("");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("eyeShadowAlpha");
		qr.Bind(Globals.DEFAULT_eyeShadowAlpha);
		qr.Bind("");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES(?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.playerId);
		qr.Bind("playerDLCVersion");
		qr.Bind(Globals.DEFAULT_dlcVersion);
		qr.Bind("");
		qr.Step();
		qr.Release();
	}

	public void InitTableExp()
	{
		Log.Debug("Init Table Exp");

		string queryInsert = "";
		SQLiteQuery qr = null;

		for (int i = 0; i <  ExpTable.initialExpTable.Length; i++)
		{
			queryInsert = "INSERT INTO ExpTable (level, totalExp) VALUES(?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(i+1);
			qr.Bind(ExpTable.initialExpTable[i]);
			qr.Step();
			qr.Release();
		}	
	}

	public void InitTableJobs()
	{
		Log.Debug("Init Table Jobs");

		string queryInsert = "";
		SQLiteQuery qr = null;

		for (int i = 0; i <  Jobs.initialJobsList.Length; i++)
		{
			queryInsert = "INSERT INTO Jobs (jobId, sortOrder, activityId, levelRequired, expireTime, visibleForSkill) VALUES(?,?,?,?,?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(Jobs.initialJobsList[i].activityId);
			qr.Bind(Jobs.initialJobsList[i].sortOrder);
			qr.Bind(Jobs.initialJobsList[i].activityId);
			qr.Bind(Jobs.initialJobsList[i].levelRequired);
			qr.Bind(Jobs.initialJobsList[i].expireTime);
			qr.Bind(Jobs.initialJobsList[i].visibleForSkill);
			qr.Step();
			qr.Release();

			queryInsert = "INSERT INTO Activities (activityId, title, description, timeRequired, timeLeft, gemsToFinish, rewardId) VALUES(?,?,?,?,?,?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(Jobs.initialJobsList[i].activityId);
			qr.Bind(Jobs.initialJobsList[i].title);
			qr.Bind(Jobs.initialJobsList[i].description);
			qr.Bind(Jobs.initialJobsList[i].timeRequired);
			qr.Bind(Jobs.initialJobsList[i].timeLeft);
			qr.Bind(Jobs.initialJobsList[i].gemsToFinish);
			qr.Bind(Jobs.initialJobsList[i].rewardId);
			qr.Step();
			qr.Release();
		}
	}

	public void InitTableQuest()
	{
		Log.Debug("Init Table Quest");

		string queryInsert = "";
		SQLiteQuery qr = null;

		for (int i = 0; i <  Quests.initialQuestsList.Length; i++)
		{
			queryInsert = "INSERT INTO Quests (questId, sortOrder, activityId, isRunning, isCompleted, completedOnce, isNew,canRepeat, canSkip, actionClass, goToSceneName) VALUES(?,?,?,?,?,?,?,?,?,?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(Quests.initialQuestsList[i].activityId);
			qr.Bind(Quests.initialQuestsList[i].sortOrder);
			qr.Bind(Quests.initialQuestsList[i].activityId);
			qr.Bind(Quests.initialQuestsList[i].isRunning.ToString());
			qr.Bind(Quests.initialQuestsList[i].isCompleted.ToString());
			qr.Bind(Quests.initialQuestsList[i].completedOnce.ToString());
			qr.Bind(Quests.initialQuestsList[i].isNew.ToString());
			qr.Bind(Quests.initialQuestsList[i].canRepeat.ToString());
			qr.Bind(Quests.initialQuestsList[i].canSkip.ToString());
			qr.Bind(Quests.initialQuestsList[i].stringAction);
			qr.Bind(Quests.initialQuestsList[i].goToSceneName);
			qr.Step();
			qr.Release();

			queryInsert = "INSERT INTO Activities (activityId, title, description, timeRequired, timeLeft, gemsToFinish, rewardId) VALUES(?,?,?,?,?,?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(Quests.initialQuestsList[i].activityId);
			qr.Bind(Quests.initialQuestsList[i].title);
			qr.Bind(Quests.initialQuestsList[i].description);
			qr.Bind(Quests.initialQuestsList[i].timeRequired);
			qr.Bind(Quests.initialQuestsList[i].timeLeft);
			qr.Bind(Quests.initialQuestsList[i].gemsToFinish);
			qr.Bind(Quests.initialQuestsList[i].rewardId);

			qr.Step();
			qr.Release();
		}
	}

	public void InitTableQuestFlags()
	{
		Log.Debug("Init Table Quest Flags");

		string queryInsert = "";
		SQLiteQuery qr = null;

		for (int i = 0; i <  QuestFlags.initialListOfFlags.Count; i++)
		{
			queryInsert = "INSERT INTO QuestFlags (flagId, description, intValue, strValue) VALUES(?,?,?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(QuestFlags.initialListOfFlags[i].flagId);
			qr.Bind(QuestFlags.initialListOfFlags[i].description);
			qr.Bind(QuestFlags.initialListOfFlags[i].intValue);
			qr.Bind(QuestFlags.initialListOfFlags[i].strValue);
			qr.Step();
			qr.Release();
		}
	}

	public void InitTableRewards()
	{
		Log.Debug("Init Table Rewards");

		string queryInsert = "";
		SQLiteQuery qr = null;

		for (int i = 0; i <  Rewards.initialRewardsList.Length; i++)
		{
			queryInsert = "INSERT INTO Rewards (rewardId, remainder, coins, gems, exp, energy, itemIds, skills) VALUES(?,?,?,?,?,?,?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(Rewards.initialRewardsList[i].rewardId);
			qr.Bind(Rewards.initialRewardsList[i].remainder);
			qr.Bind(Rewards.initialRewardsList[i].coins);
			qr.Bind(Rewards.initialRewardsList[i].gems);
			qr.Bind(Rewards.initialRewardsList[i].exp);
			qr.Bind(Rewards.initialRewardsList[i].energy);
			qr.Bind(Rewards.initialRewardsList[i].itemIds);
			qr.Bind(Skill.GetSkillAsString(Rewards.initialRewardsList[i].skills));

			qr.Step();
			qr.Release();
		}
	}

	public void InitTableItems()
	{
		Log.Debug("Init Table Items");

		string queryInsert = "";
		SQLiteQuery qr = null;

		for (int i = 0; i <  Items.initialItemsList.Count; i++)
		{
			//Log.Warning("inserting items:"+Items.initialItemsList[i].itemId);

			queryInsert = "INSERT INTO Items (itemId, itemName, visibility, subcategoryId, brandId, price, gems, requiredLevel, featureId, charisma, rewardId, dlcId, meta, texMeta, colorMeta) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(Items.initialItemsList[i].itemId);
			qr.Bind(Items.initialItemsList[i].itemName);
			qr.Bind(Items.initialItemsList[i].visibility.ToString());
			qr.Bind(Items.initialItemsList[i].subCategoryId);
			qr.Bind(Items.initialItemsList[i].brandId);
			qr.Bind(Items.initialItemsList[i].price);
			qr.Bind(Items.initialItemsList[i].gems);
			qr.Bind(Items.initialItemsList[i].requiredLevel);
			qr.Bind(Items.initialItemsList[i].featureId);
			qr.Bind(Items.initialItemsList[i].sexy +"," +Items.initialItemsList[i].lively );
			qr.Bind(-1/*Items.initialItemsList[i].rewardId*/);
			qr.Bind(Items.initialItemsList[i].dlcId);
			qr.Bind(Items.initialItemsList[i].meta);
			qr.Bind(Items.initialItemsList[i].texMeta);
			qr.Bind(Items.initialItemsList[i].colorMeta);
			qr.Step();
			qr.Release();
		}
	}

	public void InitTableItemConflicts()
	{

		Log.Debug("Init Table ItemConflicts");

		string queryInsert = "";
		SQLiteQuery qr = null;

		for (int i = 0; i <  ItemConflicts.initialConflictDBs.Count; i++)
		{
			queryInsert = "INSERT INTO ItemConflicts (item1Id, item2Id, conflictType, conflictAction) VALUES(?,?,?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(ItemConflicts.initialConflictDBs[i].attacker);
			qr.Bind(ItemConflicts.initialConflictDBs[i].defenser);
			qr.Bind(ItemConflicts.initialConflictDBs[i].conflictType.ToString());
			qr.Bind(ItemConflicts.initialConflictDBs[i].action.ToString());
			qr.Step();
			qr.Release();
		}
	}

	public void InitTablePlayersToItems()
	{
		Log.Debug("Init Table PlayersToItems");

		string queryInsert = "";
		SQLiteQuery qr = null;

		queryInsert = "INSERT INTO PlayersToItems (playerId, purchasedItems, wearingItems) VALUES(?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.PlayerId);
		qr.Bind(Globals.DEFAULT_purchasedItems);
		qr.Bind(Globals.DEFAULT_wearingItems);
		qr.Step();
		qr.Release();	
	}

	public void InitTablePlayersToSocials()
	{
		Log.Debug("Init Table PlayersToSocials");

		string queryInsert = "";
		SQLiteQuery qr = null;

		queryInsert = "INSERT INTO PlayersToSocials (playerId, socialType, socialId, appearName, loginName, loginPassword) VALUES(?,?,?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.PlayerId);
		qr.Bind("FACEBOOK");
		qr.Bind("");
		qr.Bind("");
		qr.Bind("");
		qr.Bind("");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayersToSocials (playerId, socialType, socialId, appearName, loginName, loginPassword) VALUES(?,?,?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.PlayerId);
		qr.Bind("GOOGLEPLUS");
		qr.Bind("");
		qr.Bind("");
		qr.Bind("");
		qr.Bind("");
		qr.Step();
		qr.Release();

		queryInsert = "INSERT INTO PlayersToSocials (playerId, socialType, socialId, appearName, loginName, loginPassword) VALUES(?,?,?,?,?,?);";
		qr = new SQLiteQuery(db, queryInsert); 
		qr.Bind(Globals.mainPlayer.PlayerId);
		qr.Bind("TWITTER");
		qr.Bind("");
		qr.Bind("");
		qr.Bind("");
		qr.Bind("");
		qr.Step();
		qr.Release();
	}

	public void InitTableShops()
	{
		Log.Debug("Init Table Shops");

		string queryInsert = "";
		SQLiteQuery qr = null;

		for (int i = 0; i <  Shops.initialShopsList.Count; i++)
		{
			queryInsert = "INSERT INTO Shops (shopId, shopName, itemId, bgmFilename, iconName, hideSubcats) VALUES(?,?,?,?,?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(Shops.initialShopsList[i].shopId);
			qr.Bind(Shops.initialShopsList[i].shopName);
			qr.Bind(Shops.itemListToString(Shops.initialShopsList[i].itemIds));
			qr.Bind(Shops.initialShopsList[i].bgmFilename);
			qr.Bind(Shops.initialShopsList[i].iconName);
			qr.Bind(Shops.initialShopsList[i].hideSubcats);
			qr.Step();
			qr.Release();
		}	
	}

	public void InitTableBoyfriends()
	{
		Log.Debug("Init Table Boyfriends");

		string queryInsert = "";
		SQLiteQuery qr = null;

		for (int i = 0; i <  Boyfriends.initialBoyfriendsList.Count; i++)
		{
			queryInsert = "INSERT INTO Boyfriends (boyfriendId, boyfriendName, title, meta, meshName, flirtExp, flirtExpMax, charismaRequired, location, rewardIds, flirtCost, flirtRepeat, instantGiftCost, rewardClaimed, rewardTimer, dlcId) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
			qr = new SQLiteQuery(db, queryInsert); 
			qr.Bind(Boyfriends.initialBoyfriendsList[i].boyfriendId);
			qr.Bind(Boyfriends.initialBoyfriendsList[i].boyfriendName);
			qr.Bind(Boyfriends.initialBoyfriendsList[i].title);
			qr.Bind(Boyfriends.initialBoyfriendsList[i].meta);
			qr.Bind(Boyfriends.initialBoyfriendsList[i].meshName);
			qr.Bind(Boyfriends.initialBoyfriendsList[i].flirtExp);
			qr.Bind(Boyfriends.initialBoyfriendsList[i].maxFlirtExp);
			qr.Bind(Boyfriends.initialBoyfriendsList[i].charismaRequired);
			qr.Bind(Boyfriends.intListToString(Boyfriends.initialBoyfriendsList[i].location));
			qr.Bind(Boyfriends.intListToString(Boyfriends.initialBoyfriendsList[i].rewardIds));
			qr.Bind(Boyfriends.intListToString(Boyfriends.initialBoyfriendsList[i].flirtCost));
			qr.Bind(Boyfriends.intListToString(Boyfriends.initialBoyfriendsList[i].flirtRepeat));
			qr.Bind(Boyfriends.intListToString(Boyfriends.initialBoyfriendsList[i].instantGiftCost));
			qr.Bind(Boyfriends.intListToString(Boyfriends.initialBoyfriendsList[i].rewardClaimed));
			qr.Bind(Boyfriends.initialBoyfriendsList[i].rewardTimer);
			qr.Bind(Boyfriends.initialBoyfriendsList[i].dlcId);
			qr.Step();
			qr.Release();
		}	
	}

	#endregion

	class UpdateOrInsertCallbackData
	{
		public List<string> updateQueries = new List<string>();
		public List<string> insertQueries = new List<string>();

		public UpdateOrInsertCallbackData(List<string> pUpdateQ, List<string> pInsertQ)
		{
			updateQueries = pUpdateQ;
			insertQueries = pInsertQ;
		}
	}

	public void BatchUpdateOrInsert(UpdateOrInsertBatch batch)
	{
		List<string> reallyUpdateQueries = new List<string>();
		List<string> reallyInsertQueries = new List<string>();

		foreach (UpdateOrInsertElement element in batch.elements)
		{
			SQLiteQuery qr = new SQLiteQuery(DatabaseManager.SharedManager.db, batch.judgeQuery);
			bool hasRecord = qr.Step();
			qr.Release();

			if (hasRecord)
			{
				foreach(string s in element.updateQueries)
				{
					reallyUpdateQueries.Add(s);
				}
			}

			else
			{
				foreach(string s in element.insertQueries)
				{
					reallyInsertQueries.Add(s);
				}
			}
		}

		string concatUpdate = batch.updateHeader + String.Join(",", reallyUpdateQueries.ToArray());

		SQLiteQuery qrOut = new SQLiteQuery(DatabaseManager.SharedManager.db, concatUpdate);
		qrOut.Step();
		qrOut.Release();

		string concatInsert = batch.insertHeader + String.Join(",", reallyInsertQueries.ToArray());

		qrOut = new SQLiteQuery(DatabaseManager.SharedManager.db, concatInsert);
		qrOut.Step();
		qrOut.Release();
	}

	public void UpdateOrInsert(string judgeQuery, List<string> updateQueries, List<string> insertQueries)
	{
		//UpdateOrInsertCallbackData data = new UpdateOrInsertCallbackData(updateQueries, insertQueries);
		//asyncDB.Query(judgeQuery, UpdateOrInsertQueryCreated, data);

		SQLiteQuery qr = new SQLiteQuery(DatabaseManager.SharedManager.db, judgeQuery);
		bool hasRecord = qr.Step();
		qr.Release();

		if (hasRecord)
		{
			//Log.Debug("[DatabaseManager][UpdateOrInsert] hasRecord, judge:"+judgeQuery);
			foreach(string s in updateQueries)
			{
				SaveSync(s);
			}
		}
		else
		{
			//Log.Debug("[DatabaseManager][UpdateOrInsert] no Record found!, judge:"+judgeQuery);
			foreach(string s in insertQueries)
			{
				SaveSync(s);
			}
		}
	}

	public void Save(string query)
	{
		SaveSync(query);
	}

	public void SaveAsync(string query)
	{
		if (!onInit) 
			return;

		/*asyncQueue.Add(query);
		
		if (!isRunningAsync)
		{
			
			isRunningAsync = true;*/
		asyncDB.Query(query, AsyncQueryCreated, null);

		/*if (!saveRegularCheck)
			{
				saveRegularCheck = true;
				StartCoroutine(SaveAsyncRegularCheck());
			}
		}*/
	}

	// a thing to prevent timing bug
	IEnumerator SaveAsyncRegularCheck()
	{
		if (!isRunningAsync && asyncQueue.Count > 0)
		{
			isRunningAsync = true;
			asyncDB.Query(asyncQueue[0], AsyncQueryCreated, null);
		}

		yield return new WaitForSeconds(1.0f);

		StartCoroutine(SaveAsyncRegularCheck());
	}

	public void SaveSync(string query)
	{
		if (!onInit) 
			return;		

		SQLiteQuery qr;
		string querySelect = query;

		qr = new SQLiteQuery(db, querySelect);
		while(qr.Step()){}
		qr.Release();
	}

	#region Restore GameSave
	public void RestoreNewGameSave(Hashtable socialInfo)
	{
		Globals.mainPlayer.GameId = "";
		StartCoroutine(WashDB());
	}	

	public void RestoreGameSave(Hashtable userInfo)
	{
		StartCoroutine(EnumRestoreGameSave(userInfo));
	}

	private IEnumerator EnumRestoreGameSave(Hashtable userInfo)
	{
		bool restoreSuccess = false;
		restoreProgress = 0.0f;
		isRestoring = true;
		Log.Debug("Prepare for restore");
		if (PrepareForRestore(ref userInfo))
		{
			restoreProgress = 0.3f;
			Log.Debug("Restore from Server Data");
			if (RestoreFromServerData(userInfo))
			{
				restoreProgress = 0.6f;
				Log.Debug("Re load values");
				LoadSaveData();
				Log.Debug("Load done");

				yield return StartCoroutine(UpdateGameSaveDeviceId());

				restoreProgress = 1.0f;
				restoreSuccess = true;
			}
		}

		isRestoring = false;

		if (restoreSuccess)
		{
			if (callbackList != null)
			{
				for (int i = 0;i < callbackList.Count; i++)
				{
					DatabaseManagerCallback c = (DatabaseManagerCallback)callbackList[i];
					c.OnDatabaseManagerRestoreGameSaveCompleted();
				}
			}
		}
		else
		{
			for (int i = 0;i < callbackList.Count; i++)
			{
				DatabaseManagerCallback c = (DatabaseManagerCallback)callbackList[i];
				c.OnDatabaseManagerRestoreGameSaveFailed();
			}
		}

		yield return null;
	}

	private string ValidateWearingItems(string itemStr)
	{
		List<Item> defaultWearingItemList 	= Items.stringToItemList(Globals.DEFAULT_wearingItems);
		Item defaultBraItem 				= null;
		Item defaultPantiesItem 			= null;

		//Prepare for the default bra and the panties
		Log.Debug("Default Wearing Items Count: " + defaultWearingItemList.Count);
		for (int i = 0; i < defaultWearingItemList.Count; i++)
		{
			Item item = defaultWearingItemList[i];
			if (item != null)
			{
				int braSubCatId 	= (int)ItemSubcategories.Id.BRA;
				int pantiesSubCatId = (int)ItemSubcategories.Id.PANTIES;

				if (item.subCategoryId == braSubCatId)
				{
					defaultBraItem = item;

					Log.Debug("Validate Wearing Items: The default Bra is: " + defaultBraItem.itemId);
				}
				else if (item.subCategoryId == pantiesSubCatId)
				{
					defaultPantiesItem = item;

					Log.Debug("Validate Wearing Items: The default Panties is: " + defaultPantiesItem.itemId);
				}

				if (defaultBraItem != null && defaultPantiesItem != null)
					break;
			}
		}

		//Check if the bra and panties exists
		if (itemStr != null && !itemStr.Equals(""))
		{
			List<Item> wearingItemList = Items.stringToItemList(itemStr);

			if (wearingItemList != null && wearingItemList.Count > 0)
			{
				bool hasBra = false, hasPanties = false;
				for (int i = 0; i < wearingItemList.Count; i++)
				{
					Item item = wearingItemList[i];
					if (item != null)
					{
						int braSubCatId 	= (int)ItemSubcategories.Id.BRA;
						int pantiesSubCatId = (int)ItemSubcategories.Id.PANTIES;

						if (item.subCategoryId == braSubCatId)
						{
							hasBra = true;
						}
						else if (item.subCategoryId == pantiesSubCatId)
						{
							hasPanties = true;
						}

						if (hasBra && hasPanties)
							break;
					}
				}

				if (wearingItemList != null && wearingItemList.Count > 0)
					itemStr = Items.itemListToString(wearingItemList);
				else
					itemStr = "";

				if (!hasPanties)
				{
					Log.Debug("Validate Wearing Items: The Panties is missed!!");

					if (itemStr != null && !itemStr.Equals(""))
						itemStr += "," + defaultPantiesItem.itemId;
					else
						itemStr = "" + defaultPantiesItem.itemId;
				}

				if (!hasBra)
				{
					Log.Debug("Validate Wearing Items: The Bra is missed!!");

					itemStr += "," + defaultBraItem.itemId;
				}
			}
			else
			{
				itemStr = Globals.DEFAULT_wearingItems;
			}
		}
		else
		{
			itemStr = Globals.DEFAULT_wearingItems;
		}

		Log.Debug("Validate Wearing Items: The final item string is: " + itemStr);

		return itemStr;
	}

	private string ValidatePurchasedItems(string itemStr)
	{
		if (itemStr != null && !itemStr.Equals(""))
		{
			List<string> purchasedItemIds				= new List<string>(itemStr.Split(','));
			List<string> defaultPurchasedItemIds		= new List<string>(Globals.DEFAULT_purchasedItems.Split(','));

			for (int i = 0; i < defaultPurchasedItemIds.Count; i++)
			{
				string defaultItemId = defaultPurchasedItemIds[i];

				if (!purchasedItemIds.Contains(defaultItemId))
				{
					purchasedItemIds.Add(defaultItemId);
				}
			}

			if (purchasedItemIds != null && purchasedItemIds.Count > 0)
			{
				itemStr = string.Join(",", purchasedItemIds.ToArray());
			}
			else
			{
				itemStr = Globals.DEFAULT_purchasedItems;
			}
		}
		else
		{
			itemStr = Globals.DEFAULT_purchasedItems;
		}

		return itemStr;
	}	

	private bool PrepareForRestore(ref Hashtable serverData)
	{
		if (!onInit)
		{
			Log.Debug("Database not yet init");
			return false;
		}

		SQLiteQuery qr = null;
		string query = "";
		bool sqlResult = true;

		List<string> tablesToClear = new List<string>(){"Players", "PlayerMeta", "PlayersToItems", "PlayersToSocials"};
		foreach (string tableName in tablesToClear)
		{
			query = "DELETE FROM " + tableName;
			qr = new SQLiteQuery(db, query);
			sqlResult = qr.ExecuteUpdate();
			qr.Release();
			if (!sqlResult)
			{
				Log.Debug("Cannot Delete Players table");
				return false;
			}
		}

		//Reset Activities
		query = "UPDATE Activities SET timeLeft = -1";
		qr = new SQLiteQuery(db, query);
		sqlResult = qr.ExecuteUpdate();
		qr.Release();
		if (!sqlResult)
		{
			Log.Debug("Cannot reset Activities table");
			return false;
		}

		//Reset Boyfriends
		query = "UPDATE Boyfriends SET flirtExp = 0, rewardClaimed = '0, 0, 0', rewardTimer = -1";
		qr = new SQLiteQuery(db, query);
		sqlResult = qr.ExecuteUpdate();
		qr.Release();
		if (!sqlResult)
		{
			Log.Debug("Cannot rest Boyfriends table");
			return false;
		}

		//Check userInfo

		Hashtable playerInfo = (Hashtable)serverData["playerInfo"];
		if (playerInfo != null)
		{
			string purchasedItemStr = ValidatePurchasedItems("" + playerInfo["purchasedItems"]);
			string wearingItemStr	= ValidateWearingItems("" + playerInfo["wearingItems"]);

			playerInfo["purchasedItems"] 	= purchasedItemStr;
			playerInfo["wearingItems"] 		= wearingItemStr;
		}


		return true;
	}

	private bool RestoreFromServerData(Hashtable serverData)
	{
		bool isSuccess = true;
		SQLiteQuery qr = null;
		Hashtable playerInfo = (Hashtable)serverData["playerInfo"];
		Hashtable playerMeta = (Hashtable)serverData["playerMeta"];
		Hashtable playerBoyfriends = (Hashtable)serverData["playerBoyfriends"];
		Hashtable playerSocial = (Hashtable)serverData["playerSocial"];
		Hashtable playerQuest = (Hashtable)serverData["playerQuest"];

		//Restore playerInfo
		//Prepare playerInfo for binding
		if (playerInfo != null)
		{
			BeginTransaction();

			string query = "INSERT INTO Players VALUES(-1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			qr = new SQLiteQuery(db, query);
			if (playerInfo["playerName"] == null) 			qr.Bind(Globals.DEFAULT_playerName); 						else qr.Bind("" + playerInfo["playerName"]);
			if (playerInfo["level"] == null)      			qr.Bind(Globals.DEFAULT_level); 							else qr.Bind(int.Parse("" + playerInfo["level"]));

			if (playerInfo["exp"] == null)		  			
				{qr.Bind(0/*Globals.DEFAULT_exp*/); 					cacher.setExp(-1+1);}
			else
				{qr.Bind(int.Parse("" + playerInfo["exp"]));			cacher.setExp(int.Parse("" + playerInfo["exp"]));}

			if (playerInfo["energy"] == null)
				{qr.Bind(19+1/*Globals.DEFAULT_energy*/); 				cacher.setEnergy(18+2);}
			else
				{qr.Bind(int.Parse("" + playerInfo["energy"])); 		cacher.setEnergy(int.Parse("" + playerInfo["energy"]));}

			if (playerInfo["maxEnergy"] == null)
				{qr.Bind(19+1/*Globals.DEFAULT_maxEnergy*/);			cacher.setMaxEnergy(16+4);}
			else
				{qr.Bind(int.Parse("" + playerInfo["maxEnergy"]));		cacher.setMaxEnergy(int.Parse("" + playerInfo["maxEnergy"]));}

			if (playerInfo["coins"] == null)
				{qr.Bind(13999+1001/*Globals.DEFAULT_coins*/);			cacher.setCoins(13998+1002);}
			else
				{qr.Bind(int.Parse("" + playerInfo["coins"]));			cacher.setCoins(int.Parse("" + playerInfo["coins"]));}

			if (playerInfo["gems"] == null)
				{qr.Bind(98+2/*Globals.DEFAULT_gems*/);					cacher.setGems(97+3);}
			else
				{qr.Bind(int.Parse("" + playerInfo["gems"]));			cacher.setGems(int.Parse("" + playerInfo["gems"]));}



			if (playerInfo["gameId"] == null)	  			qr.Bind(""); 												else qr.Bind("" + playerInfo["gameId"]);
			if (playerInfo["profilePicURL"] == null)	  	qr.Bind(Globals.DEFAULT_profilePicURL); 					else qr.Bind("" + playerInfo["profilePicURL"]);
			if (playerInfo["boyfriendId"] == null)	  		qr.Bind(Globals.DEFAULT_boyfriendId); 						else qr.Bind("" + playerInfo["boyfriendId"]);
			if (playerInfo["skills"] == null)	  			qr.Bind(Skill.GetSkillAsString(Globals.DEFAULT_skills)); 	else qr.Bind("" + playerInfo["skills"]);
			if (playerInfo["skillTitle"] == null)	  		qr.Bind(Globals.DEFAULT_titleSkill);						else qr.Bind("" + playerInfo["skillTitle"]);
			isSuccess = qr.ExecuteUpdate();
			qr.Release();
			if (!isSuccess)
			{
				Log.Debug("Cannot Restore Players");
				//Rollback();
				//return false;
			}	

			//Restore PlayersToItems

			query = "INSERT INTO PlayersToItems VALUES(-1, ?, ?)";
			qr = new SQLiteQuery(db, query);

			if (playerInfo["purchasedItems"] == null)
				qr.Bind(Globals.DEFAULT_purchasedItems); 	
			else
				qr.Bind("" + playerInfo["purchasedItems"]);

			if (playerInfo["wearingItems"] == null)			
				qr.Bind(CharacterManager.GenerateRandomDefaultWearingItemString()); 		
			else 
				qr.Bind("" + playerInfo["wearingItems"]);

			isSuccess = qr.ExecuteUpdate();
			qr.Release();
			if (!isSuccess)
			{
				Log.Debug("Cannot Restore PlayersToItems");
				//Rollback();
				//return false;
			}
		}

		//Restore PlayerMeta
		if (playerMeta != null)
		{
			for (int i = 0; i < playerMeta.Keys.Count; i++)
			{
				Hashtable meta 		= (Hashtable)playerMeta["" + i];
				string metaKey 		= "" + meta["metaKey"];
				string metaIntValue = "" + meta["intValue"];
				string metaStrValue = "" + meta["strValue"];				

				//Checking for the skipped data
				if (metaKey.Equals("dlcVersion"))
				{
					metaIntValue = "" + Globals.currentDLCVersion;
					/*
					int saveDLCVersion = int.Parse(metaIntValue);
					if (Globals.currentDLCVersion > saveDLCVersion)
						continue;*/
				}

				//Prepare for the query
				string query = "INSERT INTO PlayerMeta VALUES (-1, ?, ?, ?)";
				qr = new SQLiteQuery(db, query);
				qr.Bind(metaKey);
				qr.Bind((metaIntValue == null || metaIntValue.Equals(""))? 0: int.Parse(metaIntValue));
				qr.Bind((metaStrValue == null || metaStrValue.Equals(""))? " ": metaStrValue);


				//Check isNumeric
				/*
				int intValue;
				bool isNumeric = int.TryParse(metaIntValue, out intValue);
				if (isNumeric)
				{
					qr.Bind(intValue);
					qr.Bind("");
				}
				else
				{
					qr.Bind(0);
					qr.Bind(metaStrValue);
				}
				*/

				isSuccess = qr.ExecuteUpdate();
				qr.Release();
				if (!isSuccess)
				{
					Log.Debug("Cannot Restore PlayerMeta");
					//Rollback();
					//return false;
				}
			}
		}

		//Restore Boyfriends
		if (playerBoyfriends != null)
		{

			for (int i = 0; i < playerBoyfriends.Count; i++)
			{
				Hashtable boyfriend = (Hashtable)playerBoyfriends["" + i];
				if (boyfriend != null)
				{
					string query = "UPDATE Boyfriends SET flirtExp = ?, rewardClaimed = ?, rewardTimer = ? WHERE boyfriendId = ?";
					qr = new SQLiteQuery(db, query);
					if (boyfriend["flirtExp"] == null) 		qr.Bind(Globals.DEFAULT_flirtExp); 		else qr.Bind(int.Parse("" + boyfriend["flirtExp"]));
					if (boyfriend["rewardClaimed"] == null) qr.Bind(Globals.DEFAULT_rewardClaimed); else qr.Bind("" + boyfriend["rewardClaimed"]);
					if (boyfriend["rewardTimer"] == null) 	qr.Bind(Globals.DEFAULT_rewardTimer); 	else qr.Bind(int.Parse("" + boyfriend["rewardTimer"]));
					if (boyfriend["boyfriendId"] == null) 	qr.Bind(0); 							else qr.Bind(int.Parse("" + boyfriend["boyfriendId"]));

					isSuccess = qr.ExecuteUpdate();
					if (!isSuccess)
					{
						Log.Debug("Cannot Restore Boyfriends");
						//Rollback();
						//return false;
					}
				}			
			}
		}


		//Restore PlayerSocial
		if (playerSocial != null)
		{

			for (int i = 0; i < playerSocial.Count; i++)
			{
				Hashtable social = (Hashtable)playerSocial["" + i];
				if (social != null)
				{
					string serverSocialId 	= "" + social["socialNetworkId"];
					string serverSocialType = "" + social["socialNetworkType"];					

					string query = "INSERT INTO PlayersToSocials(playerId, socialType, socialId, appearName) VALUES (-1, ?, ?, ?)";
					qr = new SQLiteQuery(db, query);
					if (serverSocialType.Equals("facebook"))
						qr.Bind("FACEBOOK");
					else if (serverSocialType.Equals("google"))
						qr.Bind("GOOGLEPLUS");
					else if (serverSocialType.Equals("twitter"))
						qr.Bind("TWITTER");
					qr.Bind(serverSocialId);
					qr.Bind("");
					isSuccess = qr.ExecuteUpdate();
					qr.Release();
					if (!isSuccess)
					{
						Log.Debug("Cannot Restore PlayersToSocials");
						//Rollback();
						//return false;					
					}
				}
			}
		}

		//Restore playerQuest
		if (playerQuest != null)
		{

			for (int i = 0; i < playerQuest.Count; i++)
			{
				Hashtable metaRow = (Hashtable)playerQuest["" + i];
				if (metaRow != null)
				{
					string query = "UPDATE QuestFlags SET description = ?, intValue = ?, strValue = ? WHERE flagId = ?";
					qr = new SQLiteQuery(db, query);
					if (metaRow["description"] == null) 		qr.Bind(""); 	else qr.Bind("" + metaRow["description"]);
					if (metaRow["intValue"] == null)     		qr.Bind(0); 	else qr.Bind(int.Parse("" + metaRow["intValue"]));
					if (metaRow["strValue"] == null) 			qr.Bind(""); 	else qr.Bind("" + metaRow["strValue"]);
					if (metaRow["flagId"] == null) 				qr.Bind(-1); 	else qr.Bind("" + metaRow["flagId"]);
					//!?
					isSuccess = qr.ExecuteUpdate();
					if (!isSuccess)
					{
						Log.Debug("Cannot Restore PlayerQuest");
						//Rollback();
						//return false;
					}
				}			
			}
		}

		isSuccess = ValidateDBStructure();

		if (isSuccess)
		{
			Commit();

			cacher.setCoins(15000);
			cacher.setGems(100);
			cacher.setExp(0);
			cacher.setEnergy(20);
		}
		else
			Rollback();

		return isSuccess;
	}

	private bool ValidateDBStructure()
	{	
		SQLiteQuery qr = null;
		string query = "";	
		bool isSuccess = true;

		DateTime now = DateTime.UtcNow;
		string timeStampStr = (now.Year+"-"+now.Month+"-"+now.Day+"-"+now.Hour+"-"+now.Minute+"-"+now.Second);

		//Check PlayerMeta
		Dictionary<string, object> playerMetas = new Dictionary<string, object>(){
			{"dlcVersion", Globals.DEFAULT_dlcVersion},
			{"energyInterval", Globals.DEFAULT_energyInterval},
			{"energyTimer", Globals.DEFAULT_energyTimer},
			{"minimapPosition", Globals.DEFAULT_minimapPosition},
			{"streetPosition", Globals.DEFAULT_streetPosition},
			{"timeStamp", timeStampStr},
			{"visitedGameIds", Globals.DEFAULT_visitedGameIds},
			{"lastSyncDate", Globals.DEFAULT_lastSyncDate},
			{"settings_bgm", Globals.DEFAULT_settings_bgm},
			{"settings_sfx", Globals.DEFAULT_settings_sfx},
			{"settings_notification", Globals.DEFAULT_settings_notification},
			{"scrolledQuest", Globals.DEFAULT_scrolledQuest},
			{"settings_langCode", Globals.DEFAULT_settings_langCode},
			{"initializedPlayer", Globals.DEFAULT_initializedPlayer},
			{"currentQuestIds", Globals.DEFAULT_currentQuests},
			{"clothMatchTotalScore", Globals.DEFAULT_clothMatchTotalScore},
			{"clothMatchHighScore", Globals.DEFAULT_clothMatchHighScore},
			{"rougeAlpha", Globals.DEFAULT_rougeAlpha},
			{"eyeShadowAlpha", Globals.DEFAULT_eyeShadowAlpha},
			{"numOfSessions", Globals.DEFAULT_numOfSessions},
			{"startPlayTime", Globals.DEFAULT_startPlayTime},
			{"isIAP", Globals.DEFAULT_isIAP},
			{"isCracked", Globals.DEFAULT_isCracked},
			{"hasFirstDLC", Globals.DEFAULT_hasFirstDLC},
			{"iapRecord", Globals.DEFAULT_iapRecord},
			{"bundleVersion", Globals.bundleVersion}
		};
		query = "SELECT metaKey FROM PlayerMeta WHERE playerId = " + Globals.mainPlayer.playerId;
		qr = new SQLiteQuery(db, query);
		while(qr.Step())
		{
			string dbMetaKey = qr.GetString("metaKey");
			foreach(string metaKey in playerMetas.Keys)
			{
				if (dbMetaKey.Equals(metaKey))
				{
					playerMetas.Remove(metaKey);
					break;
				}
			}
		}
		qr.Release();

		if (playerMetas.Count > 0)
		{
			foreach(string metaKey in playerMetas.Keys)
			{
				int metaValue;
				query = "INSERT INTO PlayerMeta (playerId, metaKey, intValue, strValue) VALUES (?, ?, ?, ?)";
				qr = new SQLiteQuery(db, query);
				qr.Bind(Globals.mainPlayer.playerId);
				qr.Bind(metaKey);
				if (int.TryParse("" + playerMetas[metaKey], out metaValue))
				{
					qr.Bind(metaValue);
					qr.Bind("");
				}
				else
				{
					qr.Bind(0);
					qr.Bind("" + playerMetas[metaKey]);
				}
				isSuccess = qr.ExecuteUpdate();
				qr.Release();

				if (!isSuccess)
				{
					Log.Debug("error when re-insert the data into PlayerMeta");
					return false;
				}
			}			
		}


		//Check PlayersToSocials
		List<string> socialTypes = new List<string>(){"FACEBOOK", "TWITTER", "GOOGLEPLUS"};
		query = "SELECT socialType From PlayersToSocials WHERE playerId = " + Globals.mainPlayer.playerId;
		qr = new SQLiteQuery(db, query);
		while(qr.Step())
		{
			string dbSocialType = qr.GetString("socialType");
			foreach(string socialType in socialTypes)
			{
				if (dbSocialType.Equals(socialType))
				{
					socialTypes.Remove(socialType);
					break;
				}				
			}
		}
		qr.Release();

		//Re-insert the default values to keep the consistency
		if (socialTypes.Count > 0)
		{
			foreach(string socialType in socialTypes)
			{
				query = "INSERT INTO PlayersToSocials (playerId, socialType, socialId, appearName, loginName, loginPassword) VALUES(?,?,?,?,?,?);";
				qr = new SQLiteQuery(db, query); 
				qr.Bind(Globals.mainPlayer.PlayerId);
				qr.Bind(socialType);
				qr.Bind("");
				qr.Bind("");
				qr.Bind("");
				qr.Bind("");
				isSuccess = qr.ExecuteUpdate();
				qr.Release();

				if (!isSuccess)
				{
					Log.Debug("error when re-insert the data into PlayersToSocials");
					return false;
				}
			}		
		}		

		return isSuccess;
	}

	private IEnumerator UpdateGameSaveDeviceId()
	{
		string deviceId 	= MunerisManager.SharedManager.GetDeviceId();		
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + deviceId;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("UpdateGameSaveDeviceId", checkSumData);



		//Prepare the parameters
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", gameId.ID);
		kvp.Add("deviceId", deviceId);
		kvp.Add("checkSum", checkSum);			

		//yield return StartCoroutine(HTTPPost.Post("UpdateGameSaveDeviceId", kvp, result => response = result));
		string url = Globals.serverAPIURL + "UpdateGameSaveDeviceId.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));

		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;

		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				Hashtable respHT = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(response);
				Hashtable serverResp = (Hashtable)respHT["0"];
				if (serverResp != null)
				{
					bool hasResponse = Boolean.Parse("" + serverResp["response"]);
					if (hasResponse)
						Log.Debug("DB Update game device Successfully!!");
					else
						Log.Debug("DB Cannot update game Device");
				}
			}
		}
	}

	#endregion

	#region Database Transaction Function
	private bool BeginTransaction()
	{
		bool result 	= false;
		string query 	= "BEGIN TRANSACTION";

		SQLiteQuery qr 	= new SQLiteQuery(db, query);
		result 			= qr.ExecuteUpdate();
		qr.Release();

		return result;
	}

	private bool Commit()
	{
		bool result 	= false;
		string query 	= "COMMIT";

		SQLiteQuery qr 	= new SQLiteQuery(db, query);
		result 			= qr.ExecuteUpdate();
		qr.Release();

		return result;
	}

	private bool Rollback()
	{
		bool result 	= false;
		string query 	= "ROLLBACK";

		SQLiteQuery qr 	= new SQLiteQuery(db, query);
		result 			= qr.ExecuteUpdate();
		qr.Release();

		return result;
	}
	#endregion

	public IEnumerator WashDB()
	{
		// for debug only, never call this function in release unless intended
		db = new SQLiteDB();
		db.Open(Globals.dbSavePath);
		#if !UNITY_EDITOR
		db.Key(Globals.dbKey);
		#endif

		SQLiteQuery qr;
		string queryDelete = "";

		List<string> tableNames = new List<string>(new string[]{"Variables", "Players", "PlayerMeta",  "ExpTable",
			"Activities", "Jobs", "Trainings", "Quests", "QuestFlags", "Rewards", "Votings", "Dlcs", "Boyfriends",
			"Items", "ItemConflicts", "ItemCategory", "ItemCategoriesToSubcategories", "ItemSubcategories",
			"ItemBrands", "Features", "Shops", "PlayersToItems", "PlayersToSocials", "Posters"});

		foreach (string s in tableNames)
		{
			queryDelete= "DROP TABLE IF EXISTS " + s;
			qr = new SQLiteQuery(db, queryDelete); 
			qr.Step();
			qr.Release();
		}


		//PopupManager.ShowLoadingFlower();
		yield return StartCoroutine(EnumCheckInitialized(false));
		yield return "[DatabaseManager][WashDB] Success!";

		for (int i = 0; i < callbackList.Count; i++)
		{
			DatabaseManagerCallback c = (DatabaseManagerCallback)callbackList[i];
			c.OnDatabaseManagerRestoreNewSaveCompleted();
		}
	}



	#region GenerateGameId
	private IEnumerator GenerateGameId(Action<String> gameId)
	{
		Log.Debug("Start Generate GameId");

		string returnMe = "";
		//string deviceId = Muneris.Muneris.Instance.GetDeviceId().InstallId;
		string deviceId = MunerisManager.SharedManager.GetDeviceId();

		//Prepare CheckSum
		string checkSumData = deviceId;
		string checkSum = MD5Hash.GetServerCheckSumWithString("GenerateID", checkSumData);	

		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("deviceId", deviceId);
		kvp.Add("isDebugMode", Globals.isDebugMode.ToString());
		kvp.Add("checkSum", checkSum);

		string url = Globals.serverAPIURL + "GenerateID.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));

		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;

		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				// i guess need some parsing
				try
				{
					ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);

					if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
					{
						Log.Debug("DB GameID received");
						returnMe = (String) al[1];
						Globals.shouldShowGameIdPopup = true;
						//PopupManager.ShowGameIDGetPopup(returnMe);
					}
					else
					{
						// some error 
						Log.Debug("DB can't get game id!!");
					}
				}
				catch (Exception e)
				{
					Log.Debug("DB can't connect to server " + e);
				}
			}
		}

		//yield return StartCoroutine(HTTPPost.Post("GenerateID", kvp, result => response = result));

		gameId(returnMe);
	}
	#endregion

	#region asyncDB callback
	void AsyncQueryCreated(SQLiteQuery qr, object state)
	{
		// we don't want modify query here, so do read.
		asyncDB.Step(qr, AsyncStepCallback, state);
	}


	void AsyncStepCallback(SQLiteQuery qr, bool step, object state)
	{	
		if(step) 
		{
			// do next read.
			asyncDB.Step(qr, AsyncStepCallback, state);
		}
		else
		{
			// we reach limit or finish reading.
			asyncDB.Release(qr, AsyncQueryReleased, null);
		}
	}


	void AsyncQueryReleased(object state)
	{
		// do next query
		/*asyncQueue.RemoveAt(0);
		
		if (asyncQueue.Count >= 1)
		{
			asyncDB.Query(asyncQueue[0], AsyncQueryCreated, null);
		}
		else
		{
			isRunningAsync = false;
		}*/
	}




	void UpdateOrInsertQueryCreated(SQLiteQuery qr, object state)
	{

		asyncDB.Step(qr, UpdateOrInsertStepCallback, state);
	}


	void UpdateOrInsertStepCallback(SQLiteQuery qr, bool step, object state)
	{
		UpdateOrInsertCallbackData data = state as UpdateOrInsertCallbackData;

		if(step) 
		{
			// update
			foreach(string q in data.updateQueries)
				SaveAsync(q);

		}
		else
		{
			// insert
			foreach(string q in data.insertQueries)
				SaveAsync(q);
		}

		asyncDB.Release(qr, UpdateOrInsertQueryReleased, null);
	}


	void UpdateOrInsertQueryReleased(object state)
	{
		state = null;
	}
	#endregion
}

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class InputManager : MonoBehaviour, SceneManagerCallback{
    
    private Dictionary<string, DateTime> buttonStartTime = new Dictionary<string, DateTime>();
    private bool showQuitGamePopup = false;
	private ArrayList callbackList = new ArrayList();
    private int touchFingerId = -1;
	
    private static InputManager instance = null;
    public static InputManager SharedManager
    {
        get
        {
            return instance;    
        }
    }
    
    void Awake()
    {
        
    }
    
    // Use this for initialization
    void Start () {
        if (instance == null)
        {
            instance = this;
            ScenesMap.Build();
            SceneManager.SharedManager.SetCallback(this);
            DontDestroyOnLoad(this.transform.gameObject);
        }
    }
    
    // Update is called once per frame
    void Update () {
        if (Input.GetKey(KeyCode.Escape))
        {
            if (buttonStartTime.ContainsKey("" + KeyCode.Escape))
            {
                DateTime startTime = buttonStartTime["" + KeyCode.Escape];
                DateTime endTime = DateTime.Now;
                
                TimeSpan timeDiff = endTime - startTime;
                //Log.Debug(timeDiff.TotalSeconds);
                
                if (timeDiff.TotalSeconds > 0.5 && !showQuitGamePopup)
                {
                    if (PopupManager.PopupCount() == 0)
                    {
                        showQuitGamePopup = true;
                        PopupManager.HideAllPopup();
                        PopupManager.ShowQuitGamePopup(this,false);
                    }
                }
            }
            else
            {
                buttonStartTime["" + KeyCode.Escape] = DateTime.Now;
            }
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {
			if (callbackList != null)
			{
				for (int i = 0;i < callbackList.Count; i++)	
				{
					InputManagerCallback c = (InputManagerCallback)callbackList[i];
					c.OnInputManagerBackPressed();
				}
			}
            //Log.Debug("Key Up");
            if (!showQuitGamePopup)
            {
                CheckBackButtonPressed();
            }
            buttonStartTime.Remove("" + KeyCode.Escape);
        }
        
        /*
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (buttonStartTime.ContainsKey("" + KeyCode.Escape))
            {
                DateTime startTime = buttonStartTime["" + KeyCode.Escape];
                DateTime endTime = DateTime.Now;
                
                TimeSpan timeDiff = endTime - startTime;
                if (timeDiff.TotalSeconds > 0.5 && !showQuitGamePopup)
                {
                    showQuitGamePopup = true;
                    PopupManager.ShowQuitGamePopup();
                }
            }
            else
                buttonStartTime["" + KeyCode.Escape] = DateTime.Now;
        }
        else if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (showQuitGamePopup)
            {
                showQuitGamePopup = false;
                buttonStartTime.Remove("" + KeyCode.Escape);
            }
            else
            {
                Log.Debug("Check Button Pressed");
                CheckBackButtonPressed();
            }
        }
        */
    }
	
	void FixedUpdate()
	{
		#if UNITY_EDITOR
		if (Input.GetMouseButtonDown(0))
		{
			for (int j = 0; j < callbackList.Count; j++)
			{
				InputManagerCallback c = (InputManagerCallback)callbackList[j];
				c.OnInputManagerMouseClicked(Input.mousePosition);
			}
		}
		#endif
		
		if (Input.touchCount == 1)
		{
			for (int i = 0; i < Input.touches.Length; i++)
			{
				Touch touch = Input.touches[i];
				if (touch.phase == TouchPhase.Began)
				{
					for (int j = 0; j < callbackList.Count; j++)
					{
						InputManagerCallback c = (InputManagerCallback)callbackList[j];
						c.OnInputManagerTouchBegan(touch);
					}
				}
				else if (touch.phase == TouchPhase.Ended)
				{
					if (i == 0)
					{
						CheckTouchGirlBoyfriend(touch);
					}
					
					for (int j = 0; j < callbackList.Count; j++)
					{
						InputManagerCallback c = (InputManagerCallback)callbackList[j];
						c.OnInputManagerTouchEnded(touch);
					}
				}
			}
		}
	}
	
	private void CheckTouchGirlBoyfriend(Touch touch)
	{
		//Log.Debug("Touch Count 1");
		GameObject bkCamera = GameObject.Find("BKCamera");
		if (bkCamera != null)
		{
			Ray ray = bkCamera.camera.ScreenPointToRay(touch.position);
			
			RaycastHit hit;
			if (Physics.Raycast(ray.origin, ray.direction * 10, out hit))
			{
				string objectName = hit.transform.name;
				if (objectName.Equals("FEMALE"))
				{
					Log.Debug("Girl touched!");
					if (callbackList != null)
					{
						for (int i = 0; i < callbackList.Count; i++)
						{
							Log.Debug("Send Girl touch event to callback");
							InputManagerCallback c = (InputManagerCallback)callbackList[i];	
							c.OnInputManagerFemaleCharacterTouched();
						}
					}
				}
				else if (objectName.Equals("BOYFRIEND"))
				{
					Log.Debug("boyfriend touched!");
					if (callbackList != null)
					{
						for (int i = 0; i < callbackList.Count; i++)
						{
							Log.Debug("Send boyfriend touch event to callback");
							InputManagerCallback c = (InputManagerCallback)callbackList[i];	
							c.OnInputManagerBoyfriendChacaterTouched();
						}							
					}
				}					
			}
		}
	}
    
    private void CheckBackButtonPressed()
    {
        if (CheckPopup())
            return;
        if (CheckScene())
            return;
        
        PopupManager.ShowQuitGamePopup(this,false);
    }
    
    private bool CheckPopup()
    {
        bool resolved = false;
        
        int numOfPopup = PopupManager.PopupCount();
        if (numOfPopup > 0)
        {
            PopupManager.GetLatestPopup().OnBackPressed();
            //PopupManager.HideLatestPopup();
            resolved = true;
        }
        else
            resolved = false;
        
        return resolved;
    }
    
    private bool CheckScene()
    {
        bool resolved = false;
        if (SceneManager.SharedManager != null)
		{
			if (SceneManager.SharedManager.IsLoading())
				return false;

            Scenes currentScene = SceneManager.SharedManager.CurrScene;
            if (!currentScene.Equals(Scenes.STREET) && !currentScene.Equals(Scenes.TITLE) && !currentScene.Equals(Scenes.INITDB))
            {
                if (ScenesMap.isBuilt())
                    ScenesMap.Build();


                Scenes parentScene = ScenesMap.FindParentScenesWithScenes(currentScene);
                if (currentScene.Equals(Scenes.MINIGAMECOINDROP) && (MiniGameCoinDropScene.highestChain.x >= 1 || MiniGameCoinDropScene.tigerSlotQuota > 0))
				{
                    PopupManager.ShowSureQuitPopup(parentScene,false);
					resolved = true;
				}
                else if (currentScene.Equals(Scenes.MINIGAMECLOTHMATCH) && (MiniGameClothMatchScene.gameTime > 0f))
				{
					PopupManager.ShowSureQuitPopup(parentScene,false);
					resolved = true;
					
				}
				else if (currentScene.Equals(Scenes.BOYFRIEND))
				{
					Boyfriend currBoyfriend = Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId);
					if (currBoyfriend != null)
					{
						if (currBoyfriend.flirtExp >= currBoyfriend.maxFlirtExp)
						{
							resolved = true;
						}
					}
					//nth, just lock it
				}
				else if (currentScene.Equals(Scenes.FRIENDSHOME))
				{
					GameObject initObj = GameObject.Find("Init");
					if (initObj != null)
					{
						FriendsHomeScene friendsHomeScene = initObj.GetComponent<FriendsHomeScene>();
						if (friendsHomeScene.page2.activeSelf)
						{
							friendsHomeScene.GoToPage1();
							resolved = true;
						}
					}
				}		
                
				if (!resolved)
				{
                    SceneManager.SharedManager.LoadScene(parentScene, true);
                	resolved = true;
				}
            }
        }
        
        return resolved;    
    }	
	
	public void SetCallback(InputManagerCallback c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
			{
				callbackList.Remove(c);
			}
			
			callbackList.Add(c);
		}
	}
	
	public void RemoveCallback(InputManagerCallback c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
				callbackList.Remove(c);	
		}
	}
    
    #region Button Callback
    public void OnQuitGamePopupCrossClicked()
    {
        showQuitGamePopup = false;
    }
    #endregion
    
    #region SceneManager Callback
    public void OnSceneChanged(bool withFakeLoading)
    {
        showQuitGamePopup = false;
    }
    #endregion
}
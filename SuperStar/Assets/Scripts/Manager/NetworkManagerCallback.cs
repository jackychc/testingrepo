using UnityEngine;
using System.Collections;

public interface NetworkManagerCallback{
	void OnNetworkManagerConnectionFailed();
	void OnNetworkManagerConnectionSuccess();
}

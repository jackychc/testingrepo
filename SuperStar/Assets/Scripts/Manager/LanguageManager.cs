using UnityEngine;
using System;
using System.Collections;

public class LanguageManager {
	public static void SwitchLanguage(string langCode)
	{
		Language.SwitchLanguage(langCode);
		LogFlurryEvent(langCode);
	}
	
	public static void SwitchLanguage(LanguageCode langCode)
	{
		Language.SwitchLanguage(langCode);
		LogFlurryEvent(langCode);
	}
	
	private static void LogFlurryEvent(LanguageCode langCode)
	{
		string langCodeStr = Enum.GetName(typeof(LanguageCode), langCode);
		LogFlurryEvent(langCodeStr);
	}
	
	private static void LogFlurryEvent(string langCode)
	{
		if (MunerisManager.SharedManager != null)
		{
			Hashtable param = new Hashtable();
			param["Language"] = langCode;
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Language_Used, param);
		}
	}
}

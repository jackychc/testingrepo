using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NotificationManager : MonoBehaviour
{
	public GameObject notificationItem;
	
	public enum TYPE
	{
		JOB_FINISHED,
		BOYFRIEND_REWARD,
		DLC_FINISHED
	}
	
	public GameObject bg;
	public UISprite notiIcon;
	public UISysFontLabel notiText;
	TYPE currentType = TYPE.JOB_FINISHED;
	
	public static List<TYPE> notificationQueue = new List<TYPE>();
	

	
	void Start()
	{
		//Log.Debug("starting noti");
		
		transform.parent.GetComponent<UIAnchor>().uiCamera = Camera.mainCamera;
		transform.parent.localPosition = new Vector3(-320f,480f,-150f);
		StartCoroutine(LoopRoutine());
	}
	
	public static void AddNotificationOfType(TYPE t)
	{
		//if (notificationQueue.Count > 0)
		//{
			notificationQueue.Add(t);
		//}
		
		//notifi
	}
	
	public IEnumerator LoopRoutine()
	{
		while(true)
		{
			if (notificationQueue.Count > 0)
			{
				Log.Debug("q > 0");
				
				// only show notifications for scenes applicable
				if (GameObject.Find("NotificationMother") != null && !SceneManager.SharedManager.loadingUI.activeSelf)
				{
					Log.Debug("start showing 2");
				
					
					currentType = notificationQueue[0];
					
					UIImageButton imageBtn = bg.GetComponent<UIImageButton>();
					UISprite imageBtnBg = bg.GetComponentInChildren<UISprite>();
					string targetSpriteName = "";
					
					if (notificationQueue[0].Equals(TYPE.JOB_FINISHED))
					{
						targetSpriteName = "notification_bar";
						notiIcon.spriteName = "notification_jobFinish";
						notiIcon.gameObject.SetActive(true);
						notiText.Text = Language.Get("notification_jobfinish");
					}
					
					else if (notificationQueue[0].Equals(TYPE.BOYFRIEND_REWARD))
					{
						targetSpriteName = "notification_bar";
						notiIcon.spriteName = "notification_bfGift";
						notiIcon.gameObject.SetActive(true);
						notiText.Text = Language.Get("notification_giftready");
					}
						
					else if (notificationQueue[0].Equals(TYPE.DLC_FINISHED))
					{
						targetSpriteName = "DLC_notice_dlComplete";
						notiIcon.spriteName = "";
						notiIcon.gameObject.SetActive(false);
						notiText.Text = Language.Get("notification_dlcomplete");						
					}
					
					imageBtn.normalSprite = targetSpriteName;
					imageBtn.hoverSprite = targetSpriteName;
					imageBtn.pressedSprite = targetSpriteName;
					imageBtnBg.spriteName = targetSpriteName;
					
					
					if (Globals.settings_notification)
						notificationItem.GetComponent<PositionTweener>().Forward();
					
					notificationQueue.RemoveAt(0);
					
					yield return new WaitForSeconds(4.5f);
					
					notificationItem.GetComponent<PositionTweener>().Backward();
				}
			}
			
			yield return new WaitForSeconds(0.5f);
		}
		//StartCoroutine(LoopRoutine());
	}
	
	public void ItemClicked()
	{
		//Log.Debug("iiiiiiiiiiii clicked");
		
		if (currentType.Equals(TYPE.JOB_FINISHED))
		{
			Scenes currentScene = SceneManager.SharedManager.CurrScene;
			if (!currentScene.Equals(Scenes.JOB))
				SceneManager.SharedManager.LoadScene(Scenes.JOB, true);
		}		
		else if (currentType.Equals(TYPE.BOYFRIEND_REWARD))
		{
			Scenes currentScene = SceneManager.SharedManager.CurrScene;
			if (!currentScene.Equals(Scenes.HOME))
				SceneManager.SharedManager.LoadScene(Scenes.HOME, true);
		}
	}
	
	
}


using UnityEngine;
using System.Collections;

public interface DatabaseManagerCallback {

	void OnDatabaseManagerRestoreNewSaveCompleted();
	
	void OnDatabaseManagerRestoreGameSaveCompleted();
	void OnDatabaseManagerRestoreGameSaveFailed();
}

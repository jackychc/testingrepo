using UnityEngine;
using System.Collections;

public interface InputManagerCallback {

	void OnInputManagerFemaleCharacterTouched();
	void OnInputManagerBoyfriendChacaterTouched();
	
	void OnInputManagerTouchBegan(Touch touch);
	void OnInputManagerTouchEnded(Touch touch);
	void OnInputManagerBackPressed();
	
	#if UNITY_EDITOR
	void OnInputManagerMouseClicked(Vector3 clickedPos);
	#endif
	
}

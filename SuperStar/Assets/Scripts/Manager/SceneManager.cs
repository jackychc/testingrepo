using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SceneManager : MonoBehaviour {
	
	public UISprite titleSprite;
	public UIFilledSprite progressBar;
	public UILabel progressNumber;
	public UISprite progressBarBg;
	public UISysFontLabel labelTutorial;
	
	private bool canRefreshLabel = false;	
	private List<string> tutorialTextList 		= new List<string>();
	private readonly string  tutorialTextPrefix = "loading_tips_";
	private readonly int tutorialTextStIndex	= 1;
	private readonly int tutorialTextEndIndex 	= 11;	
	
	//public AsyncOperation loadOp;
	
	public GameObject loadingPanelObj;
	public GameObject loadingUI;
	public float LoadProgress;
	private float loadStart;
	private float loadEnd;
	private bool isLoading;
	
	private int currSceneIndex = -1;
	private int nextSceneIndex = -1;
	TweenAlpha ta;
	UIPanel loadingPanel;
	public UISysFontLabel loadingSysLabel;
	private bool fakeLoading;
	
	private ArrayList callbackQueue = new ArrayList();
	private static SceneManager instance = null; 
	public static SceneManager SharedManager
	{
		get{
			return instance;
		}
	}
	
	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}
	
	void Start () {
		isLoading = false;
	}
	
	
	private void InitLoadingUI()
	{
		if (titleSprite != null)
		{
			LanguageCode currLangCode = (LanguageCode)Enum.Parse(typeof(LanguageCode), Globals.langCode);		
			
#if FGP
			if (currLangCode.Equals(LanguageCode.JA))
			{
				titleSprite.spriteName = "FGP_titleScreen_title_ja";
			}
			else if (currLangCode.Equals(LanguageCode.KO))
			{
				titleSprite.spriteName = "FGP_titleScreen_title_ko";
			}
			else if (currLangCode.Equals(LanguageCode.ES))
			{
				titleSprite.spriteName = "FGP_titleScreen_title_es";
			}
			else
			{
				titleSprite.spriteName = "FGP_titleScreen_title_en";
			}
#elif SFG
			titleSprite.spriteName = "SFG_titleScreen_title";
#endif
			
			titleSprite.MakePixelPerfect();
		}
	}
	
	private void PrepareTutorialText()
	{
		if (tutorialTextList != null || tutorialTextList.Count > 0)
			tutorialTextList.Clear();
		for (int i = tutorialTextStIndex; i <= tutorialTextEndIndex; i++)
		{
			string textId 	= tutorialTextPrefix + string.Format("{0:000}", i);
			string text 	= Language.Get(textId);
			tutorialTextList.Add(text);
		}
	}
	
	public void SetCallback(SceneManagerCallback c)
	{
		if (callbackQueue != null)
		{
			if (callbackQueue.Contains(c))
			{
				callbackQueue.Remove(c);
			}

			callbackQueue.Add(c);
		}
	}
	
	public void RemoveCallback(SceneManagerCallback c)
	{
		if (callbackQueue != null)
		{
			if (callbackQueue.Contains(c))
				callbackQueue.Remove(c);
		}
	}
	
	public IEnumerator LoadToNextScene()
	{
		if (nextSceneIndex != -1)
		{		
			//SoundManager.SharedManager.FadeOutBGM();
			
			AsyncOperation loadOp = null;
			
			if (!fakeLoading)
			{
				
				ShowProgressBar(true);
				
				loadOp = Application.LoadLevelAsync(nextSceneIndex);
				
				ResetProgressBar();
				
				loadStart 		= Time.time;
				loadEnd 		= loadStart + 5;
				LoadProgress 	= 0.1f;
				
				isLoading = true;
				while(!loadOp.isDone)
				{
					LoadProgress = Mathf.Clamp(Mathf.InverseLerp(loadStart,loadEnd,Time.time),0 ,0.9f);
				    yield return new WaitForEndOfFrame();
				}
				Resources.UnloadUnusedAssets();
			}
			else
			{
				
				Application.LoadLevel(nextSceneIndex);
				isLoading = true;
				
				LoadProgress = 1f;
				
				Resources.UnloadUnusedAssets();
			}
			
			//Clear the popup queue
			PopupManager.ClearPopupQueue();

			//Reset character rotation
			if (Globals.globalCharacter != null && Globals.globalCharacter.transform.parent != null)
			{
				Globals.globalCharacter.transform.parent.localEulerAngles 	= new Vector3(0, 0, 0);
				Globals.globalCharacter.transform.parent.localPosition		= new Vector3(0, 0, 0);
			}
			
			
			//Disable the Touch event
			GameObject uiCamera = GameObject.Find("UICamera");
			GameObject bgCamera = GameObject.Find("BGCamera");
			
			if (uiCamera != null)
				uiCamera.GetComponent<UICamera>().eventReceiverMask = 1 << 9;
			if (bgCamera != null)
				bgCamera.GetComponent<UICamera>().eventReceiverMask = 1 << 9;
			
			while (LoadProgress < 1)
			{
				LoadProgress += 1 * Time.deltaTime;
				LoadProgress = Mathf.Clamp(LoadProgress,0,1);
				yield return loadOp;
			}
		}
		
		isLoading = false;
		nextSceneIndex = -1;		
		
		//Fire the callback event
		for (int i = 0; i < callbackQueue.Count; i++)
		{
			SceneManagerCallback c = (SceneManagerCallback)callbackQueue[i];
			if (c != null)
				c.OnSceneChanged(fakeLoading);
		}
		
		yield return new WaitForSeconds(0.258f);
		
		ta = loadingPanelObj.GetComponent<TweenAlpha>();
		ta.enabled = true;
		
		
		//yield return new WaitForSeconds(0.25f);
	}
	
	public void OnLoadingPanelFadedIn()
	{
		loadingPanel = loadingPanelObj.GetComponent<UIPanel>();
		
		ta = loadingPanelObj.GetComponent<TweenAlpha>();
		Destroy(ta);
		ta= null;
		
		ta = loadingPanelObj.AddComponent<TweenAlpha>();
		ta.from 			= 1.0f;
		ta.to 				= 0.0f;
		ta.duration			= 0.3f;
		ta.eventReceiver 	= this.gameObject;
		ta.callWhenFinished = "OnLoadingPanelFadedOut";
		ta.enabled 			= false;
		
		StartCoroutine(LoadToNextScene());
	}
	
	public void OnLoadingPanelFadedOut()
	{
		ta = loadingPanelObj.GetComponent<TweenAlpha>();
		Destroy(ta);
		ta = null;
		
		//Reset the Tween Alpha
		ta = loadingPanelObj.AddComponent<TweenAlpha>();
		ta.from 			= 0.0f;
		ta.to 				= 1.0f;
		ta.duration			= 0.3f;
		ta.eventReceiver 	= this.gameObject;
		ta.callWhenFinished = "OnLoadingPanelFadedIn";
		ta.enabled 			= false;
		
		loadingPanel 	= loadingPanelObj.GetComponent<UIPanel>();
		loadingPanel.alpha 		= 1.0f;		
		
		loadingUI.SetActive(false);
		
		canRefreshLabel = false;
		//SoundManager.SharedManager.StartBGM();
		ResetProgressBar();
		
		//Reset the Camera's eventReceiveMask
		GameObject uiCamera = GameObject.Find("UICamera");
		GameObject bgCamera = GameObject.Find("BGCamera");
	
		if (uiCamera != null)
		{
			if (uiCamera.GetComponent<UICamera>().eventReceiverMask == 1 << 9)
				uiCamera.GetComponent<UICamera>().eventReceiverMask = -1;
		}
		if (bgCamera != null)
		{
			if (bgCamera.GetComponent<UICamera>().eventReceiverMask == 1 << 9)
				bgCamera.GetComponent<UICamera>().eventReceiverMask = -1;
		}

		PopupManager.RestorePendingPopup();
	}
	
	private void ResetProgressBar()
	{
		LoadProgress = 0.0f;
		progressBar.fillAmount = 0.0f;
		progressNumber.text = "0%";	
	}
	
	private void ShowProgressBar(bool show)
	{
		progressBarBg.gameObject.SetActive(show);
		progressBar.gameObject.SetActive(show);
		progressNumber.gameObject.SetActive(show);
		labelTutorial.gameObject.SetActive(show);
	}
	
	private void LoadSceneWithLoading(bool fake)
	{
		Vector3 t = loadingSysLabel.transform.parent.localPosition;
		if (!fake)
		{
			t.x = -40f;
		 	loadingSysLabel.transform.parent.localPosition = t;
			loadingSysLabel.transform.localPosition = new Vector3(40f,0f,-1f);
			loadingSysLabel.Alignment = SysFont.Alignment.Right;
			loadingSysLabel.pivot = UIWidget.Pivot.Right;
			
			InitLoadingUI();
			PrepareTutorialText();
			canRefreshLabel = true;
			StopCoroutine("ShowTutorials");
			StartCoroutine(ShowTutorials());	
		}
		else
		{
			t.x = 0f;
			loadingSysLabel.transform.localPosition = new Vector3(0f,0f,-1f);
		 	loadingSysLabel.transform.parent.localPosition = t;
			loadingSysLabel.Alignment = SysFont.Alignment.Center;
			loadingSysLabel.pivot = UIWidget.Pivot.Center;
		}
		
		loadingSysLabel.transform.parent.GetComponent<HybridFont>().Refresh();
		ShowProgressBar(!fake);
		
		fakeLoading = fake;
		
		ResetProgressBar();
		
		
		
		loadingUI.SetActive(true);
		ta = loadingPanelObj.GetComponent<TweenAlpha>();
		ta.enabled = true;
		
		//StartCoroutine(LoadToNextScene());
	}
	
	public void LoadScene(Scenes scene, bool withLoading)
	{		
		//Disable the Touch event
		GameObject uiCamera = GameObject.Find("UICamera");
		GameObject bgCamera = GameObject.Find("BGCamera");
		
		if (uiCamera != null)
			uiCamera.GetComponent<UICamera>().eventReceiverMask = 1 << 9;
		if (bgCamera != null)
			bgCamera.GetComponent<UICamera>().eventReceiverMask = 1 << 9;
		
		currSceneIndex = Application.loadedLevel;
		nextSceneIndex = (int)scene;
		
		LoadSceneWithLoading(!withLoading);
		
	}
	
	private IEnumerator ShowTutorials()
	{
		if (labelTutorial != null)
		{
			labelTutorial.gameObject.SetActive(true);
			while(canRefreshLabel)
			{
				if (tutorialTextList.Count > 0)
				{
					int randNum = UnityEngine.Random.Range(0, tutorialTextList.Count - 1);
					string text = tutorialTextList[randNum];
					labelTutorial.Text = text;
				}
				
				yield return new WaitForSeconds(4.0f);
				
				labelTutorial.Text = "";
			}
			
			labelTutorial.gameObject.SetActive(false);
		}
	}
	
	public Scenes PrevScene
	{
		get
		{
			return (Scenes)currSceneIndex;
		}
	}
	
	public Scenes CurrScene
	{
		get
		{
			return (Scenes)Application.loadedLevel;
		}
	}
	
	void Update()
	{
		if (isLoading && nextSceneIndex != -1)
		{			
			progressBar.fillAmount = LoadProgress;
			progressNumber.text = Mathf.RoundToInt(LoadProgress * 100f) + "%";
		}
		
		if (loadingPanel != null)
			loadingSysLabel.alpha = loadingPanel.alpha;
	}	

	public bool IsLoading()
	{
		return isLoading;
	}
	
	/*
	public void GoToScene()
	{
		GoToScene(goToScene);
	}
	
	void GoToScene (string sceneName)
	{
		//abcde = sceneName;
		nextScene = goToScene;	
		
		if (withLoading)
		{
			Application.LoadLevelAsync("Loading");	
		}
		else
		{
			// also need add a loading flower on screen to prevent ANR?
			Application.LoadLevelAsync((int) Enum.Parse(typeof(Scenes), sceneName.ToUpperInvariant()));
		}
	}
	
	bool isLoadingScene()
	{
		return Application.loadedLevelName.Contains("Loading");
	}
	*/
}

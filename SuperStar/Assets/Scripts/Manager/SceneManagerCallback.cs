using UnityEngine;
using System.Collections;

public interface SceneManagerCallback {	
	void OnSceneChanged(bool withFakeLoading);
}

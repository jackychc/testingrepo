using System;


public class ValueCacher
{


	string coins = "";
	string gems = "";
	string exp = "";
	string energy = "";
	string maxEnergy = "";

	int coinSeed = -1;
	int gemSeed = -1;
	int expSeed = -1;
	int energySeed = -1;
	int maxEnergySeed = -1;

	public bool verifyCoins (int v)
	{
		return coins.Equals(MD5Hash.GetWithString((v*v + coinSeed^1).ToString()));
	}

	public bool verifyGems (int v)
	{
		return gems.Equals(MD5Hash.GetWithString((v*v + gemSeed^2).ToString()));
	}

	public bool verifyExp (int v)
	{
		return exp.Equals(MD5Hash.GetWithString((v*v + expSeed^3).ToString()));
	}

	public bool verifyEnergy (int v)
	{
		return energy.Equals(MD5Hash.GetWithString((v*v + energySeed^4).ToString()));
	}

	public bool verifyMaxEnergy (int v)
	{
		return maxEnergy.Equals(MD5Hash.GetWithString((v*v + maxEnergySeed^4).ToString()));
	}

	public void setCoins(int v)
	{
		if (coinSeed == -1) coinSeed =  UnityEngine.Random.Range(1,296);

		coins = MD5Hash.GetWithString(""+(v*v + coinSeed^1));
	}

	public void setGems(int v)
	{
		if (gemSeed == -1) gemSeed =  UnityEngine.Random.Range(1,296);

		gems = MD5Hash.GetWithString(""+(v*v + gemSeed^2));
	}

	public void setExp(int v)
	{
		if (expSeed == -1) expSeed =  UnityEngine.Random.Range(1,296);

		exp = MD5Hash.GetWithString(""+(v*v + expSeed^3));
	}

	public void setEnergy(int v)
	{
		if (energySeed == -1) energySeed =  UnityEngine.Random.Range(1,296);

		energy = MD5Hash.GetWithString(""+(v*v + energySeed^4));
	}

	public void setMaxEnergy(int v)
	{
		if (maxEnergySeed == -1) maxEnergySeed =  UnityEngine.Random.Range(1,296);

		maxEnergy = MD5Hash.GetWithString(""+(v*v + maxEnergySeed^4));
	}
}



using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SaveLoadManager : MonoBehaviour, SceneManagerCallback
{
	//int autosaveInterval = 5; //seconds
	//int autosaveCounter = 0;	
	
	private volatile bool loadingPlayer = false;
	private volatile bool syncingPlayer = false;
	private static SaveLoadManager instance = null; 
	public static SaveLoadManager SharedManager
	{
		get{
			return instance;
		}
	}
	
	void Awake()
	{
		if (instance != null)
			Destroy(this);


	}
	
	// Use this for initialization
	void Start ()
	{
		if (instance == null)
		{
			instance = this;
			SceneManager.SharedManager.SetCallback(this);
			DontDestroyOnLoad(this.transform.gameObject);
			
			//Calculate the remain time diff
			CompansateTimerOff();
			//InvokeRepeating("UpdateTimers", 0.0f, 1.0f);
			StartCoroutine("UpdateTimersControl", 0.1f);
			//LocalNotification.CancelAllNotification();
			//autosaveCounter = autosaveInterval;
			//StartCoroutine(UpdatePerSecond());
		}	
	}

	IEnumerator UpdateTimersControl()
	{
		while (true)
		{
			yield return new WaitForSeconds(1.0f);

			UpdateTimers();
		}
	}
	
	/*
	IEnumerator UpdatePerSecond()
	{
		yield return new WaitForSeconds(1f);
		
		OneSecondPassed();
		
		// auto save progress regularly
		//autosaveCounter--;
		//if (autosaveCounter == 0)
		//{
			//Jobs.Save();
			//Trainings.Save();
			//Quests.Save();
			//Boyfriends.Save();
			//autosaveCounter = autosaveInterval;
		//}	
		StartCoroutine(UpdatePerSecond());
	}
	*/
	
	public void CompansateTimerOff()
	{
		DateTime now = DateTime.UtcNow;
		
		TimeSpan span = now - Globals.mainPlayer.timeStamp;
		
		for (int i = 0; i < span.TotalSeconds; i++)
		{
			UpdateTimers(false);
		}
		
		/*if (Globals.mainPlayer.timeStamp.DayOfYear != now.DayOfYear)
		{
			// reset daily bouns etc
			Globals.visitedGameIds.Clear();
		}*/
	}
	
	public void RefreshLocalNotifications()
	{
		if (Globals.settings_notification)
		{
			foreach (Job a in Jobs.jobsList)
			{
				int secondsToFinish = a.timeRequired - (a.expireTime - a.timeLeft);
				if (secondsToFinish > 0)
				{
					//Log.Debug("[SaveLoadManager][RefreshLoacalNotification] The job has time left: " + secondsToFinish);
					DateTime fireDate 	= DateTime.Now.AddSeconds(secondsToFinish); 
					LocalNotification.SetFireDate(fireDate);
					LocalNotification.SetId(Int32.Parse(a.visibleForSkill) + 100);
					LocalNotification.SetTitle(Globals.appName);
					LocalNotification.SetMessage(Language.Get("notification_jobfinish"));
					LocalNotification.ScheduleNotification();

					//Log.Debug("[SaveLoadManager][RefreshLocalNotification] The fire Date is: " + fireDate.ToString());
					//Log.Debug("[SaveLoadManager][RefreshLocalNotification] Job Notification Scheduled!");
					
				}
			}
			
			if (Globals.mainPlayer.CurrentBoyfriendId > 0)
			{
				if (Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId).rewardTimer > 0)
				{
					Boyfriend myBf = Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId);
						
					for (int i = 0 ; i < Boyfriends.checkPoints.Count; i++)
					{
						int secondsToFinish = myBf.rewardTimer - Boyfriends.checkPoints[i];
						if (secondsToFinish > 0)
						{

							//Log.Debug("[SaveLoadManager][RefreshLoacalNotification] The boyfriend has time left: " + Boyfriends.checkPoints[i]);
							DateTime fireDate = DateTime.Now.AddSeconds(secondsToFinish);
							LocalNotification.SetFireDate(fireDate);
							LocalNotification.SetId(1);
							LocalNotification.SetTitle(Globals.appName);
							LocalNotification.SetMessage(Language.Get("notification_giftready"));
							LocalNotification.ScheduleNotification();

							//Log.Debug("[SaveLoadManager][RefreshLocalNotification] The fire Date is: " + fireDate.ToString());
							//Log.Debug("[SaveLoadManager][RefreshLocalNotification] Boyfriend Gift Notification Scheduled!");

							break;
						}
					}
				}
			}
		}
	}

	private void UpdateTimers()
	{
		UpdateTimers(true);
	}
	private void UpdateTimers(bool withEffect)
	{
		
		// don't set any notification if setting off
		//if (!Globals.settings_notification)
		//	Globals.refreshLocalNotification = false;
		
		//if (Globals.refreshLocalNotification)
		//	Log.Warning("START REFRESHING LOCAL NOTIFICATION");
		
		foreach (Job a in Jobs.jobsList)
		{
			if (a.timeLeft > 0)
			{
				
				/*if (Globals.refreshLocalNotification)
				{
					
					LocalNotification.CancelNotification(Int32.Parse(a.visibleForSkill) + 100);
					
					DateTime fireDate = DateTime.Now.AddSeconds(a.timeRequired - (a.expireTime - a.timeLeft));
					LocalNotification.SetFireDate(fireDate);
					LocalNotification.SetId(Int32.Parse(a.visibleForSkill) + 100);
					LocalNotification.SetTitle(Globals.appName);
					LocalNotification.SetMessage(Language.Get("notification_jobfinish"));
					LocalNotification.ScheduleNotification();
				}*/
				
				a.timeLeft --;
				
				if (a.timeLeft == a.expireTime - a.timeRequired)
				{
					Log.Debug("start showing 1");
					NotificationManager.AddNotificationOfType(NotificationManager.TYPE.JOB_FINISHED);
				}
				
			}
		}
		
		// todo : if current bf timer > 0, deduct it
		if (Globals.mainPlayer.CurrentBoyfriendId > 0)
		{
			if (Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId).rewardTimer > 0)
			{
				
				bool matchTier = false;
				
				for (int i = 0; i <  Boyfriends.checkPoints.Count; i++)
				{
					if (Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId).rewardTimer == Boyfriends.checkPoints[i])
					{
						//if (i > 0)
						{
							if (Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId).rewardClaimed[i] == 0)
							{
								matchTier = true;
								break;
							}
						}
					}
				}
				
				if (!matchTier)
				{
					Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId).rewardTimer--;
					
					Boyfriend myBf = Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId);
					
					for (int i = 0 ; i < Boyfriends.checkPoints.Count; i++)
					{
						if (myBf.rewardTimer == Boyfriends.checkPoints[i])
						{
							NotificationManager.AddNotificationOfType(NotificationManager.TYPE.BOYFRIEND_REWARD);
						}
						
						/*else
						{
							if (Globals.refreshLocalNotification)
							{
								//LocalNotification.CancelNotification(1);
					
								DateTime fireDate = DateTime.Now.AddSeconds(myBf.rewardTimer - Boyfriends.checkPoints[i]);
								LocalNotification.SetFireDate(fireDate);
								LocalNotification.SetId(1);
								LocalNotification.SetTitle(Globals.appName);
								LocalNotification.SetMessage(Language.Get("notification_giftready"));
								LocalNotification.ScheduleNotification();
							}
						}*/
					}
				}
			}
		}
		
		Globals.mainPlayer.EnergyTimer--;
		
		if (Globals.mainPlayer.EnergyTimer <= 0)
		{
			Globals.mainPlayer.GainEnergy(1, withEffect);


			Globals.mainPlayer.EnergyTimer = Globals.mainPlayer.EnergyInterval;
		}
		
		
		foreach (Quest q in Quests.currentQuests)
		{
			if (q.isRunning)
			{
				q.CheckFinish();
				
				
			}
		}
		
		
		//Globals.refreshLocalNotification = false;
		
		
	}
	
	/*IEnumerator EnergyGainer()
	{
		// 6 min = 1 energy
		yield return new WaitForSeconds(60 * 6f);
		
		Globals.mainPlayer.GainEnergy(1);
		
		StartCoroutine(EnergyGainer());
	}*/
	
	/*void OnApplicationQuit()
	{
		Globals.mainPlayer.Save();
	}*/

#if UNITY_ANDROID
	void OnApplicationFocus(bool focus) 
	{
		if (focus)
		{
			CompansateTimerOff();
			//StartCoroutine("UpdateTimersControl", 0.1f);
			//InvokeRepeating("UpdateTimers", 0.0f, 1.0f);
			LocalNotification.CancelAllNotification();
		}
		else
		{
			RefreshLocalNotifications();
			//StopCoroutine("UpdateTimersControl");
			//CancelInvoke("UpdateTimers");			
			Globals.mainPlayer.timeStamp = DateTime.UtcNow;
			StartCoroutine(EnumSaveAll());
		}
        
    }
#endif
#if UNITY_IPHONE
	void OnApplicationPause(bool pause)
	{
		if (!pause)
		{
			CompansateTimerOff();
			//InvokeRepeating("UpdateTimers", 0.0f, 1.0f);
			LocalNotification.CancelAllNotification();
		}
		else
		{
			RefreshLocalNotifications();
			//CancelInvoke("UpdateTimers");
			Globals.mainPlayer.timeStamp = DateTime.UtcNow;
			StartCoroutine(EnumSaveAll());
		}
	}
#endif

	void OnApplicationQuit()
	{
		Log.Debug("SaveLoad App Quit");

		//CancelInvoke("UpdateTimers");
		Globals.mainPlayer.timeStamp = DateTime.UtcNow;
		SaveAll();

		LocalNotification.CancelAllNotification();
		RefreshLocalNotifications();
	}
	
	public void LoadAll()
	{
		if (!syncingPlayer)
		{
			loadingPlayer = true;
			
			Items.Load();
			ItemConflicts.Load();
			ExpTable.Load();
			Jobs.Load();
			Quests.Load();
			QuestFlags.Load();
			Rewards.Load();
			Posters.Load();
			Shops.Load();
			Boyfriends.Load();
			Globals.mainPlayer.Load();
			Friends.Load();
			Features.Load();			
			SkillTable.Load();
			
			loadingPlayer = false;
		}
	}
	
	private IEnumerator EnumSaveAll()
	{
		
		if (!loadingPlayer)
		{
			SaveAll();
			//Items.AllQuery();
		}

		yield return null;
	}

	private void SaveAll()
	{
		Jobs.Save();
		//Trainings.Save();
		Quests.Save();
		QuestFlags.Save();
		Boyfriends.Save();
		Friends.Save();
		Features.Save();
		ItemConflicts.Save();
		Globals.mainPlayer.Save();
	}
	
	public void OnSceneChanged(bool withFakeLoading)
	{
		//SceneChanged
		if (!syncingPlayer && !loadingPlayer && !withFakeLoading)
		{
			if (!SceneManager.SharedManager.CurrScene.Equals(Scenes.INITDB) || !SceneManager.SharedManager.CurrScene.Equals(Scenes.TITLE))
			{
				//Save Data	
				StartCoroutine(SaveAndSyncPlayerData());
			}
		}
	}
	
	public void ForceSaveAndSyncPlayerData()
	{
		StartCoroutine(EnumForceSaveAndSyncPlayerData());
	}
	
	private IEnumerator EnumForceSaveAndSyncPlayerData()
	{
		syncingPlayer = true;
		yield return StartCoroutine(EnumSaveAll());
		yield return StartCoroutine(SyncPlayerInfo(true));
		syncingPlayer = false;	
	}
	
	private IEnumerator SaveAndSyncPlayerData()
	{
		syncingPlayer = true;
		yield return StartCoroutine(EnumSaveAll());		
		yield return StartCoroutine(SyncPlayerInfo(false));
		syncingPlayer = false;
	}
	
	private IEnumerator SyncPlayerInfo(bool isForcedSync)
	{
		string gameIdStr = Globals.mainPlayer.GameId;			
		if (gameIdStr != null && !gameIdStr.Equals(""))
		{
			string deviceId = MunerisManager.SharedManager.GetDeviceId();
			
			//Prepare Player's info to Sync
			//Player's info
			Dictionary<string, object> playerInfo = new Dictionary<string, object>();
			playerInfo.Add("playerName", Globals.mainPlayer.Name);
			playerInfo.Add("exp", Globals.mainPlayer.Exp);
			playerInfo.Add("energy", Globals.mainPlayer.Energy);
			playerInfo.Add("maxEnergy", Globals.mainPlayer.MaxEnergy);
			playerInfo.Add("coins", Globals.mainPlayer.Coins);
			playerInfo.Add("gems", Globals.mainPlayer.Gems);
			playerInfo.Add("skills", Skill.GetSkillAsString(Globals.mainPlayer.getSkill()));
			playerInfo.Add("skillTitle", "" + Globals.mainPlayer.TitleSkill);
			playerInfo.Add("boyfriendId", Globals.mainPlayer.CurrentBoyfriendId);
			playerInfo.Add("purchasedItems", Items.itemListToString(Globals.mainPlayer.owningItems));
			playerInfo.Add("munerisInstallId", deviceId);
			playerInfo.Add("wearingItems", Items.itemListToString(Globals.mainPlayer.wearingItems));
			
			Log.Debug("playerInfo : "  + SuperstarFashionMiniJSON.JsonEncode(playerInfo));
			
			//PlayerMeta
			Dictionary<string, object> playerMeta = new Dictionary<string, object>();
			playerMeta.Add("initializedPlayer", Globals.mainPlayer.InitializedPlayer? "1":"0");
			playerMeta.Add("dlcVersion", Globals.currentDLCVersion);
			playerMeta.Add("energyTimer", Globals.mainPlayer.EnergyTimer);
			playerMeta.Add("energyInterval", Globals.mainPlayer.EnergyInterval);
			playerMeta.Add("streetPosition", Globals.mainPlayer.StreetPosition);
			playerMeta.Add("minimapPosition", "" + (int)Globals.mainPlayer.MinimapPosition.x + "|" + (int)Globals.mainPlayer.MinimapPosition.y);
			playerMeta.Add("settings_bgm", Globals.settings_bgm.ToString().ToLowerInvariant());
			playerMeta.Add("settings_sfx", Globals.settings_sfx.ToString().ToLowerInvariant());
			playerMeta.Add("settings_notification", Globals.settings_notification.ToString().ToLowerInvariant());
			playerMeta.Add("settings_langCode", Globals.langCode);
			playerMeta.Add("visitedGameIds",VisitRecord.GetRecordsAsString()); // below added on 13/9
			string concat = "";
			foreach (Quest q in Quests.currentQuests)
			{
				if (!concat.Equals("")) concat += ",";
				
				concat += ""+q.activityId;
			}
			playerMeta.Add("currentQuestIds", concat);
			playerMeta.Add("clothMatchTotalScore", Globals.mainPlayer.ClothMatchTotalScore);
			playerMeta.Add("clothMatchHighScore", Globals.mainPlayer.ClothMatchHighScore);
			playerMeta.Add("rougeAlpha", Globals.mainPlayer.RougeAlpha);
			playerMeta.Add("eyeShadowAlpha", Globals.mainPlayer.EyeShadowAlpha);
			playerMeta.Add("numOfSessions", Globals.mainPlayer.NumOfSessions);
			playerMeta.Add("startPlayTime", Globals.startPlayTime);
			playerMeta.Add("isIAP", Globals.isIAP.ToString().ToLowerInvariant());
			playerMeta.Add("isCracked", Globals.isCracked.ToString().ToLowerInvariant());
			playerMeta.Add("bundleVersion", Globals.bundleVersion.ToLowerInvariant());
			playerMeta.Add("hasFirstDLC", Globals.hasFirstDLC.ToString().ToLowerInvariant());
			playerMeta.Add("iapRecord", Globals.iapRecord);

			DateTime st = Globals.mainPlayer.DailyBonusStamp;
			playerMeta.Add("dailyBonusDay", Globals.mainPlayer.DailyBonusDay);
			playerMeta.Add("dailyBonusStamp", (	st.Year+"-"+
			                                   st.Month+"-"+
			                                   st.Day+"-"+
			                                   st.Hour+"-"+
			                                   st.Minute+"-"+
			                                   st.Second));
			
			//Quests
			ArrayList playerQuests = new ArrayList();
			
			foreach (QuestFlag q in QuestFlags.listOfFlags)
			{
				Dictionary<string, object> playerQuest = new Dictionary<string, object>();
				playerQuest.Add("flagId", q.flagId);
				playerQuest.Add("description", q.description);
				playerQuest.Add("intValue", q.intValue);
				playerQuest.Add("strValue", q.strValue);
				playerQuests.Add(playerQuest);
			}
			
			//Boyfriends
			ArrayList playerBoyfriends = new ArrayList();
			foreach (Boyfriend b in Boyfriends.boyfriendsList)
			{
				Dictionary<string, object> boyfriend = new Dictionary<string, object>();
				boyfriend.Add("boyfriendId", b.boyfriendId);
				boyfriend.Add("boyfriendName", b.boyfriendName);
				boyfriend.Add("flirtExp", b.flirtExp);
				boyfriend.Add("flirtExpMax", b.maxFlirtExp);
				boyfriend.Add("flirtCost", "" + Boyfriends.intListToString(b.flirtCost));
				boyfriend.Add("flirtRepeat", "" + Boyfriends.intListToString(b.flirtRepeat));
				boyfriend.Add("instantGiftCost", "" + Boyfriends.intListToString(b.instantGiftCost));
				boyfriend.Add("charismaRequired", b.charismaRequired);
				boyfriend.Add("location", Boyfriends.intListToString(b.location));
				boyfriend.Add("rewardIds", Boyfriends.intListToString(b.rewardIds));
				boyfriend.Add("rewardClaimed", Boyfriends.intListToString(b.rewardClaimed));
				boyfriend.Add("rewardTimer", b.rewardTimer);
				boyfriend.Add("meshName", b.meshName);
				
				playerBoyfriends.Add(boyfriend);
			}
			
			//Create the player json
			
			var playerInfoJson 			= SuperstarFashionMiniJSON.JsonEncode(playerInfo);
			var playerMetaJson 			= SuperstarFashionMiniJSON.JsonEncode(playerMeta);
			var playerBoyfriendsJson 	= SuperstarFashionMiniJSON.JsonEncode(playerBoyfriends);
			var playerQuestJson			= SuperstarFashionMiniJSON.JsonEncode(playerQuests);
			
			Log.Debug(playerInfoJson);
			Log.Debug(playerMetaJson);
			Log.Debug(playerBoyfriendsJson);
			Log.Debug(playerQuestJson);
			
			GameID gameId 		= new GameID(gameIdStr);
			int playerId 		= gameId.toInt();
			string checkSumData = gameId.ID + playerId + playerInfoJson + playerMetaJson + playerBoyfriendsJson + playerQuestJson + deviceId + isForcedSync.ToString().ToLowerInvariant();
			string checkSum 	= MD5Hash.GetServerCheckSumWithString("UpdateGameSave", checkSumData);			


			//Log.Debug("The GameID is: " + gameId.ID);
			//Log.Debug("The GameId in int is: " + gameId.toInt());

			//Prepare the parameters
			Dictionary<string, string> kvp = new Dictionary<string, string>();
			kvp.Add("gameId", gameId.ID);
			kvp.Add("playerInfo", playerInfoJson);
			kvp.Add("playerMeta", playerMetaJson);
			kvp.Add("playerBoyfriends", playerBoyfriendsJson);
			kvp.Add("playerQuest", playerQuestJson);
			kvp.Add("deviceId", deviceId);
			kvp.Add("isForcedSync", isForcedSync.ToString().ToLowerInvariant());
			kvp.Add("checkSum", checkSum);
			
			string url = Globals.serverAPIURL + "UpdateGameSave.php";
			yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
			
			string response				= NetworkManager.SharedManager.Response;
			NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
			
			if (state.Equals(NetworkManager.State.Success))
			{
				if (response != null && !response.Equals(""))
				{
					try
					{
						Log.Debug("check response "+response);
						ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
						bool resp = Boolean.Parse(((Hashtable)al[0])["response"].ToString());
					
						if (resp)
						{
							Log.Debug("Response From Server");
							Hashtable serverResp = (Hashtable)al[1];
							
							bool isDeviceIdError = Boolean.Parse(serverResp["isDeviceIdError"].ToString());
							if (!isDeviceIdError)
							{
								Log.Debug("Sync Data successfully");
								Hashtable syncTime = (Hashtable)((Hashtable)al[1])["syncTime"];						
								string fullDateResponse = (syncTime["full"]).ToString();
							
								System.DateTime timestamp;
								bool parseResult = System.DateTime.TryParse(fullDateResponse, out timestamp);
								if (parseResult)
								{
									System.DateTime localTime = timestamp.ToLocalTime();
									string syncDate = localTime.Year + "-" + twoDigit(localTime.Month) + "-" + twoDigit(localTime.Day) + " " + twoDigit(localTime.Hour) + ":" + twoDigit(localTime.Minute) + ":" + twoDigit(localTime.Second);
									Log.Debug(syncDate);
									
									Globals.mainPlayer.LastSyncDate = syncDate;
									Globals.mainPlayer.Save();
									Log.Debug("Updated the lastSyncDate");
								}
							}
							else
							{
								Log.Debug("There is another record found!!");
								
								Hashtable serverData = (Hashtable)serverResp["playerData"];
								
								PopupManager.HideLoadingFlower();
								PopupManager.HideAllPopup();
								PopupManager.ShowDetectDiffDevice(serverData, false);
							}
						}
						else
						{
							Log.Debug("Cannot sync the data to the server");
						}
					}
					catch (Exception e)
					{
						Log.Debug(e.ToString());
					}
				}
			}
			
			
			//yield return StartCoroutine(HTTPPost.Post("UpdateGameSave", kvp, result => response = result));
		}
	}
	
	string twoDigit(int i)
	{
		return (i < 10)? ("0"+i):(i+"");
	}
}


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpdateOrInsertBatch
{
	
	
	public string judgeQuery;
	public string updateHeader;
	public string insertHeader;
	public List<UpdateOrInsertElement> elements = new List<UpdateOrInsertElement>();
	
	public void SetJudge(string q)
	{
		judgeQuery = q;
	}
	
	public void SetUpdateHeader(string q)
	{
		updateHeader = q;
	}
	
	public void SetInsertHeader(string q)
	{
		insertHeader = q;
	}
	
	public void AddElement(List<string> pUpdates, List<string> pInserts)
	{
		elements.Add(new UpdateOrInsertElement(pUpdates, pInserts));
	}
}

public class UpdateOrInsertElement
{
	public List<string> updateQueries;
	public List<string> insertQueries;
	
	public UpdateOrInsertElement(List<string> pUpdates, List<string> pInserts)
	{
		updateQueries = pUpdates;
		insertQueries = pInserts;
	}
}


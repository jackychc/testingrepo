using UnityEngine;
using System.Collections;

public class MiniGameCoinDropSensor : MonoBehaviour
{
	public MiniGameCoinDropScene mainBody;
	public int slotId;
	

	void OnTriggerEnter (Collider visitor)
	{
		if (slotId == -1)
		{
			Debug.Log("FOUND YOU!" +visitor.gameObject.name);
			
			mainBody.occupied[visitor.GetComponent<MiniGameCoinDropCoin>().slotId]--;
			mainBody.droppedCoins.Remove(visitor.gameObject);
			Destroy(visitor.gameObject);
			//mainBody.RefreshHighestChain();
		}
		else
		{
			visitor.rigidbody.velocity = new Vector3(0f, 0f, 0f);
			
			visitor.GetComponent<MiniGameCoinDropCoin>().slotId = slotId;
			//mainBody.occupied[slotId]++;
			//mainBody.RefreshHighestChain();
			
			/*if (mainBody.occupied[slotId])
			{
				//StartCoroutine(DestroyRoutine(visitor));
			}*/
			mainBody.SensorPassed(slotId);
			
			
		}
	}
	
	IEnumerator DestroyRoutine(Collider visitor)
	{
		yield return new WaitForSeconds(2.0f);
		
		Destroy(visitor.gameObject);
	}
}


using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class JobScrollListManager : MonoBehaviour {
	
	JobScrollListItem[] jobs;
	
	GameObject newCharacter;
	GameObject peripheralItem;
	
	int currentFocusSkill;
	private int currentAni, previousAni =-2;
	
	
	// Use this for initialization
	void Start () {
		
		newCharacter = GameObject.Find("FEMALE");
		

		//InvokeRepeating("SFXDetermination", 0f, 0.5f);
	}

	public void StartLoadAnimations()
	{
		reloadDynamic(0);

		updateEverySecond();
		InvokeRepeating("AnimationDetermination", 0f, 0.5f);
	}
		
	public void reloadDynamic(int skillType)
	{
		currentFocusSkill = skillType;
		
		int childs = transform.childCount;
		
        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }



		GameObject runningObj = null;
		for (int i =0 ; i < Jobs.jobsOfSkill(skillType).Count ; i++)
		{
			GameObject go = (GameObject) Instantiate(Resources.Load ("JobItem") as GameObject);
			
			go.name = "jobItem"+(i+1);
			go.transform.parent = transform;
			
			go.transform.localPosition 	= new Vector3(430f*i,0f,0f);
			go.transform.localRotation 	= Quaternion.identity;
			go.transform.localScale 	= Vector3.one;


			List<Job> jobs 	= Jobs.jobsOfSkill(skillType);
			Job job			= jobs[i];
			if (Jobs.isDoingJob(job) || Jobs.isOnReward(job) || Jobs.isExpired(job))
			{
				runningObj = go;
			}

			go.GetComponent<JobScrollListItem>().job = job;
			go.GetComponent<JobScrollListItem>().DelayedStart();
			go.GetComponent<JobScrollListItem>().scrollListManager = this;
		}
		
		AnimationDetermination();
		//RefreshSFX();

		if (runningObj == null)
		{
			SpringPanel.Begin(transform.gameObject, new Vector3(0f,0f,0f), 10f);
		}
		else
		{
			SpringPanel.Begin(this.gameObject, new Vector3(-runningObj.transform.localPosition.x, 0f, 0f), 5f);
		}

	
		//jobs = null;
	}

	void AnimationDetermination()
	{
		StartCoroutine(AnimationDeterminationRoutine());
	}
	
	
	private IEnumerator AnimationDeterminationRoutine()
	{
		if (Jobs.isDoingJobOfSkill(""+currentFocusSkill) == null)
		{
			currentAni = -1;
		}
		else
		{
			currentAni = currentFocusSkill;
		}
		
		if (currentAni != previousAni)
		{
			previousAni = currentAni;
		
			if (peripheralItem!= null) {Destroy(peripheralItem); peripheralItem=null;}
			
			yield return null;
				
			switch(currentAni)
			{
				
				case 0:
				{
					newCharacter.animation.Stop();
					peripheralItem = (GameObject) Instantiate((Resources.Load("CharacterAssets/Peripheral/job_tennis_obj") as GameObject));
					peripheralItem.transform.parent = newCharacter.transform;
					
					peripheralItem.animation.AddClip(Resources.Load("CharacterAssets/Animation/job_tennis") as AnimationClip,"job_tennis");
					//peripheralItem.animation.CrossFade("job_tennis", 0.618f);
					//newCharacter.animation.Stop();
					newCharacter.GetComponent<CharacterManager>().InsertAnimation("job_tennis");
					
					peripheralItem.transform.localScale = Vector3.zero;
					//yield return new WaitForSeconds(0.618f);
				
					if (peripheralItem != null)
						peripheralItem.transform.localScale = Vector3.one;
					break;
				}
				case 1:
				{
					newCharacter.GetComponent<CharacterManager>().InsertAnimation("job_dance");
					break;
				}
				case 4:
				{
					newCharacter.animation.Stop();
					peripheralItem = (GameObject) Instantiate((Resources.Load("CharacterAssets/Peripheral/job_sing_obj") as GameObject));
					peripheralItem.transform.parent = newCharacter.transform;
					peripheralItem.animation.AddClip(Resources.Load("CharacterAssets/Animation/job_sing") as AnimationClip,"job_sing");
					
					//peripheralItem.animation.CrossFade("job_sing", 0.618f);
					newCharacter.GetComponent<CharacterManager>().InsertAnimation("job_sing");
					
					peripheralItem.transform.localScale = Vector3.zero;
					//yield return new WaitForSeconds(0.618f);
					if (peripheralItem != null)
						peripheralItem.transform.localScale = Vector3.one;
					break;
				}
				default:
				{
					newCharacter.GetComponent<CharacterManager>().InsertAnimation("pose01");
					break;
				}
			}
		}
	}
	
	public void RefreshSFX()
	{
		if (Jobs.isDoingJobOfSkill(""+currentFocusSkill) == null)
		{
			//Log.Debug("Current Focus Skill is Null");
			if (SoundManager.SharedManager.IsPlayingLoopSFX())
				SoundManager.SharedManager.StopLoopSFX();
			//Log.Debug("BGMVolume: " + SoundManager.SharedManager.BGMVolume);
			//Log.Debug("Current State: " + SoundManager.SharedManager.CurrBGMState);
			SoundManager.SharedManager.ChangeMusic("SuperstarStory_JobCenter");
		}
		else
		{
			//Log.Debug(currentFocusSkill + " current focus skills");
			switch(currentFocusSkill)
			{
				case 0:
				{
					//Log.Debug("Tennis SFX");
					bool isPlay = false;
					if (SoundManager.SharedManager.IsPlayingLoopSFX())
					{
						//Log.Debug(SoundManager.SharedManager.PlayingLoopSFXName());
						if (!SoundManager.SharedManager.PlayingLoopSFXName().Equals("job_tennis_loop"))
						{
							isPlay = true;
						}
					}
					else
						isPlay = true;
				
					if (isPlay)
					{
						//Log.Debug("Play the loop source");
						SoundManager.SharedManager.FadeOutBGM();
					
						if (Globals.settings_sfx)
							SoundManager.SharedManager.PlayLoopSFX("job_tennis_loop");	
					}
				
					break;	
				}
				case 1:
				{
					//Log.Debug("Dance SFX");
					bool isPlay = false;
					if (SoundManager.SharedManager.IsPlayingLoopSFX())
					{
						if (!SoundManager.SharedManager.PlayingLoopSFXName().Equals("job_dance_loop"))
						{
							isPlay = true;
						}
					}
					else
						isPlay = true;
				
					if (isPlay)
					{
						SoundManager.SharedManager.FadeOutBGM();
					
						if (Globals.settings_sfx)
							SoundManager.SharedManager.PlayLoopSFX("job_dance_loop");	
					}
					break;	
				}
				case 4:
				{
					//Log.Debug("Sing SFX");
					bool isPlay = false;
					if (SoundManager.SharedManager.IsPlayingLoopSFX())
					{
						if (!SoundManager.SharedManager.PlayingLoopSFXName().Equals("job_sing_loop"))
						{
							isPlay = true;
						}
					}
					else
						isPlay = true;
				
					if (isPlay)
					{
						SoundManager.SharedManager.FadeOutBGM();
						
						if (Globals.settings_sfx)
							SoundManager.SharedManager.PlayLoopSFX("job_sing_loop");	
					}
					break;
				}
				default:
					break;
			}
		}
	}
	
	public void updateEverySecond()
	{		
		jobs = transform.gameObject.GetComponentsInChildren<JobScrollListItem>();
		for (int i = 0; i < jobs.Length; i++)
		{
			if (jobs[i].job != null)
			{
				bool isDoingJob = jobs[i].isDoingJob();
				jobs[i].afterOneSecond();
			}		
		}
		
		RefreshSFX();
		//StartCoroutine(updateEverySecond());
	}
	

	public void reloadDynamic0()
	{
		reloadDynamic(0);
	}
	public void reloadDynamic1()
	{
		reloadDynamic(1);
	}
	public void reloadDynamic2()
	{
		reloadDynamic(2);
	}
	public void reloadDynamic3()
	{
		reloadDynamic(3);
	}
	public void reloadDynamic4()
	{
		reloadDynamic(4);
	}
	
	void OnDestroy()
	{
		Destroy(peripheralItem);
		peripheralItem = null;
	}
	
}

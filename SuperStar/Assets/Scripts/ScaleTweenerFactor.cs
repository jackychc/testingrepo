using UnityEngine;
using System.Collections;

public class ScaleTweenerFactor : MonoBehaviour {
	
	public enum TWEENMODE
	{
		PASSIVE,
		LOOP,
		BACK_AND_FORTH,
		CONSTANT
	}
	
	public enum CURVETYPE
	{
		STRAIGHT,
		ASCEND,
		DESCEND
	}
	
	public int id = 1;
	float proportion = 0f;
	float sineProportion = 0f;
	bool commanded;
	bool isForward = true;
	
	public float speed = 1f;
	 float fromXValue,toXValue;
	 float fromYValue,toYValue;
	 float fromZValue,toZValue;
	
	public float factorX,factorY,factorZ;
	
	public TWEENMODE mode = TWEENMODE.PASSIVE;
	public CURVETYPE curveType = CURVETYPE.DESCEND;
	
	Vector3 t = Vector3.zero;
	Vector3 fromVector = Vector3.zero, toVector = Vector3.zero;
	
	Transform mTrans;
	public Transform cachedTransform { get { if (mTrans == null) mTrans = transform; return mTrans; } }
	
	
	// Use this for initialization
	void Start () {
		StartCoroutine(resetStartPosition());
	}
	
	public IEnumerator resetStartPosition()
	{
		
		
		proportion = 0f;
		
		
		
		yield return new WaitForSeconds(0.05f);
		t = cachedTransform.localScale;
		commanded = true;
		
		
		//Log.Debug("from values: = "+t.x+","+ t.y+","+ t.z);
		//Log.Debug("t0 values: = "+t.x* factorX+","+ t.y* factorY+","+ t.z* factorZ);
		fromVector = new Vector3(t.x, t.y, t.z);
		toVector = new Vector3(t.x * factorX, t.y * factorY, t.z * factorZ);
	}
	
	// Update is called once per frame
	void Update () {
		
		
		if (!commanded)
			return;
		
		
		proportion = Mathf.Clamp(proportion, 0.0f,1.0f);
		
		
		Move();
	}
	
	public float getProportion()
	{
		return proportion;
	}
	
	public void setProportion(float v)
	{
		proportion = v;
	}
	
	public void Forward()
	{
		commanded = true;
		isForward = true;
	}
	
	public void Backward()
	{
		commanded = true;
		isForward = false;
	}
	
	void Move()
	{
		
		proportion = proportion + (isForward? 1 : -1) * speed * (1f/30);
		
		sineProportion = proportion;
		
		if (curveType == CURVETYPE.DESCEND) sineProportion = 1 - (proportion-1)*(proportion-1);
		if (curveType == CURVETYPE.ASCEND) sineProportion = (proportion)*(proportion);
		
		cachedTransform.localScale = Vector3.Lerp(fromVector, toVector, sineProportion);
		
		if (mode.Equals(TWEENMODE.LOOP) || mode.Equals(TWEENMODE.CONSTANT))
		{
			if (proportion >= 1.0f)
				proportion = 0f;
			
		}
		
		else if (mode.Equals(TWEENMODE.BACK_AND_FORTH))
		{
			if (proportion <= 0f || proportion >= 1.0f)
				isForward = !isForward;
		}
		
		else if (mode.Equals(TWEENMODE.PASSIVE))
		{
			if (proportion <= 0f || proportion >= 1.0f)
				commanded = false;
		}
	}
}

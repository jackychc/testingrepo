using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;

public class FeatureManager : MonoBehaviour
{
	public static bool downloadedFeatures = false;
	
	void Start()
	{
		if (!downloadedFeatures)
			StartCoroutine(ReallyStart());
	}
	
	IEnumerator ReallyStart()
	{
		yield return StartCoroutine(GetFeatureFromServer());
		downloadedFeatures = true;
	}
	
	public IEnumerator GetFeatureFromServer()
	{
		string url = Globals.serverAPIURL + "GetFeatures.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, null));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					// have rank already
					if (al[1] != null)
					{
						ArrayList featureList =  new ArrayList(((Hashtable) al[1]).Values);
						
						List<Feature> setMe = new List<Feature>();
						//Log.Debug("count:"+playerList.Count);
						for (int i = 0; i < featureList.Count; i++)
						{
							List<int> itemsList = new List<int>();
							if (((Hashtable)featureList[i])["featureItems"] != null)
							{
								string featureItemsStr 		= "" + ((Hashtable) featureList[i])["featureItems"];
								if (!featureItemsStr.Equals(""))
								{
									string[] featureItemsArr 	= featureItemsStr.Split(',');
									//ArrayList itemsAl 			=  new ArrayList(featureItemsArr);

									foreach (string featureItemId in featureItemsArr)
									{
										Log.Debug("Feature gain a item id: " + featureItemId);
										itemsList.Add(Int32.Parse(featureItemId));
									}
								}
							}
							
							List<int> shopList = new List<int>();
							if (((Hashtable)featureList[i])["featureShops"] != null)
							{
								string featureShopsStr		= "" + ((Hashtable)featureList[i])["featureShops"];

								if (!featureShopsStr.Equals(""))
								{
									string[] featureShopsArr	= featureShopsStr.Split(',');
									//ArrayList itemsAl 			=  new ArrayList(featureShopsArr);

									foreach (string featureShopId in featureShopsArr)
									{
										Log.Debug("Feature gain a shop id: " + featureShopId);
										shopList.Add(Int32.Parse(featureShopId));
									}
								}
							}
							
							List<int> subcatList = new List<int>();
							if (((Hashtable)featureList[i])["featureSubcategories"] != null)
							{
								string featureSubcatsStr	= "" + ((Hashtable)featureList[i])["featureSubcategories"];

								if (!featureSubcatsStr.Equals(""))
								{
									string[] featureSubcatsArr	= featureSubcatsStr.Split(',');
									//ArrayList itemsAl =  new ArrayList(featureSubcatsArr);

									foreach (string featureSubcatId in featureSubcatsArr)
									{
										Log.Debug("Feature gain a subcat id: " + featureSubcatId);
										subcatList.Add(Int32.Parse(featureSubcatId));
									}
								}
							}
							
							List<string> iapList = new List<string>();
							if (((Hashtable)featureList[i])["featureIAPs"] != null)
							{
								string featureIAPsStr	= "" + ((Hashtable)featureList[i])["featureIAPs"];

								if(!featureIAPsStr.Equals(""))
								{
									string[] featureIAPsArr	= featureIAPsStr.Split(',');
									//ArrayList itemsAl 		=  new ArrayList(featureIAPsArr);

									foreach (string featureIAPsId in featureIAPsArr)
									{
										//string iapPackageKey = ht["iapPackageKey"].ToString();
										Log.Debug("Feature gain a IAP key: " + featureIAPsId);
										iapList.Add(featureIAPsId);
									}
								}
							}						
							
							Feature p = new Feature(
									Int32.Parse(((Hashtable)featureList[i])["featureId"].ToString()),
									((Hashtable)featureList[i])["featureName"].ToString(),
									((Hashtable)featureList[i])["featureType"].ToString(),
									Int32.Parse(((Hashtable)featureList[i])["coins"].ToString()),
									Int32.Parse(((Hashtable)featureList[i])["gems"].ToString()),
									Int32.Parse(((Hashtable)featureList[i])["discountOff"].ToString()),
									((Hashtable)featureList[i])["period"].ToString(),
									itemsList,
									shopList,
									subcatList,
									iapList
								);
		
							setMe.Add(p);
						}
												
						Features.featuresList = setMe;
						Features.Save();
					}
				}
			}
		}
	}

}


using UnityEngine;
using System.Collections;

public class MiniGameCoinDropPin : MonoBehaviour
{
	

	void OnCollisionEnter (Collision c)
	{
		//Log.Debug("impact force:" +c.impactForceSum.magnitude);
		
		//if (c.impactForceSum.magnitude > 0.618f)
		SoundManager.SharedManager.PlaySFX("pinball_hit_barriers", Mathf.Min(1f,c.impactForceSum.magnitude));	
	}
}


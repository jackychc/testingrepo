using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;

public class ShopItem : MonoBehaviour {

	public Item item;
	public int sortOrder;
	public UISprite currencyCoin, currencyGem;
	public UILabel amount;
	public GameObject buyButton;
	public GameObject priceBlock;
	public GameObject block;
	public GameObject background;
	public GameObject lockSprite;
	public GameObject levelLabel;
	public GameObject newBlock;
	public GameObject discountBlock;
	public GameObject discountCross;
	public GameObject icon;
	public UILabel discountLabel;
	public GameObject dlcIcon;
	
	public GameObject sexyIcon, livelyIcon;
	public UILabel indexLabel;
	
	public int discount;
	public int offerCoins;
	public int offerGems;
	
	public ScaleTweener buyScaleTweener;
	
	static Color yellowColor = new Color(225f/255f, 224f/255f, 102f/255f, 1f);
	static Color pinkColor = new Color(244f/255f, 91f/255f, 235f/255f, 1f);
	static Color blueColor = new Color(100f/255f, 100f/255f, 200f/255f, 1f);
	
	GameObject girl, scrollPanelT;
	ShopScrollListManager sslm;
	Texture2D tex;
	
	public void Start()
	{
		StartCoroutine(ReallyStart());
	}
	
	public IEnumerator ReallyStart()
	{
		//Log.Debug("item name:"+item.itemName);
		UITexture iconTexture = icon.GetComponent<UITexture>();
		tex = Items.GetIconOfItem(item);
		if(tex == null)
		{
			yield return StartCoroutine(LoadIconFromPersistent(item, result => tex = result));
		}
		
		string shaderName 	= "UnlitTransparentColored";
		Shader shader 		= Resources.Load("Shaders/" + shaderName) as Shader;
		if (shader != null)
			iconTexture.material = new Material(shader);
		else
			iconTexture.material = new Material(Shader.Find ("Unlit/Transparent Colored"));
        iconTexture.material.mainTexture = tex;
        icon.transform.localScale = new Vector3(128f,128f,1f);
		
		SetCurrencyAndAmount();
		SetIndex();
		SetLevelLock();
		SetDLCLock();
		
		Destroy(transform.GetComponent<UIPanel>());
		
	}
	
	public void AssignFindings(ShopScrollListManager pSslm, GameObject pScrollPanelT, GameObject pGirl)
	{
		sslm = pSslm;
		scrollPanelT = pScrollPanelT;
		girl = pGirl;
	}
	
	public IEnumerator LoadIconFromPersistent(Item i, Action<Texture2D> returnMe)
	{
		WWW loader = new WWW("file://" + Application.persistentDataPath + Path.DirectorySeparatorChar + i.itemId + "_icon.png");
		yield return loader;
		
		if (loader.error != null)
		{
			Log.Debug("ShopItem Cannot open the file with itemId: " + i.itemId);
		}
		else
		{
			if (loader.texture != null)
			{
				returnMe((Texture2D)loader.texture);
				
				loader.Dispose();
			}
			else
			{
				Log.Debug("ShopItem Cannot fetch the texture from file with itemId: " + i.itemId);
			}
		}	
	}
	
	public void SetCurrencyAndAmount()
	{
		if (item.gems > 0)
		{
			currencyGem.gameObject.SetActive(true);
			currencyCoin.gameObject.SetActive(false);
			amount.text = /*offerGems + ""; //*/item.gems + "";
			amount.effectColor = pinkColor;
			
			if (offerGems != item.gems)
			{
				discountBlock.SetActive(true);
				discountCross.SetActive(true);
				
				if (discount > 0)
				{
					discountLabel.text = discount + "% Off";
				}
				else
				{
					discountLabel.text = offerGems + " Gems";
				}
			}
		}
		else
		{
			amount.text = /*offerCoins + "";//*/item.price + "";
			amount.effectColor = yellowColor;
			
			if (offerCoins != item.price)
			{
				discountBlock.SetActive(true);
				discountCross.SetActive(true);
				
				if (discount > 0)
				{
					discountLabel.text = discount + "% Off";
				}
				else
				{
					discountLabel.text = offerCoins + " Coins";
				}
			}
		}
	}
	
	public void SetLevelLock()
	{
		if (item.requiredLevel > Globals.mainPlayer.Level)
		{
			block.SetActive(true);
			lockSprite.SetActive(true);
			levelLabel.GetComponent<UILabel>().text = "Lv"+item.requiredLevel;
			levelLabel.SetActive(true);
			
			discountCross.SetActive(false);
			
			currencyGem.gameObject.SetActive(false);
			currencyCoin.gameObject.SetActive(false);
			amount.text = "";
			
			sexyIcon.SetActive(false);
			livelyIcon.SetActive(false);
			indexLabel.text = "";
		}
	}
	
	public void SetIndex()
	{
		if (item.sexy > 0)
		{
			sexyIcon.SetActive(true);
			livelyIcon.SetActive(false);
			indexLabel.text = "" + item.sexy;
		}
		else if (item.lively > 0)
		{
			sexyIcon.SetActive(false);
			livelyIcon.SetActive(true);
			indexLabel.text = "" + item.lively;
		}
		else
		{
			sexyIcon.SetActive(false);
			livelyIcon.SetActive(false);
			indexLabel.text = "";
		}
	}

	public void SetDLCLock()
	{
		if (!checkEnoughDLCVersion())
		{
			if (item.requiredLevel <= Globals.mainPlayer.Level)
			{
				block.SetActive(true);
				dlcIcon.SetActive(true);
				amount.text = " ";
				currencyGem.gameObject.SetActive(false);
				currencyCoin.gameObject.SetActive(false);
				sexyIcon.SetActive(false);
				livelyIcon.SetActive(false);
				indexLabel.text = "";
			}
			else
			{
				dlcIcon.SetActive(false);
			}

		}
		else
			dlcIcon.SetActive(false);
	}

	public void ClickItem()
	{
		ClickItem(false);
	}
	
	public void ClickItem(bool isByDrag)
	{
		StartCoroutine(ClickItemRoutine(isByDrag));
	}
	
	public IEnumerator ClickItemRoutine(bool isByDrag)
	{
//		Log.Debug("this item is clicked:" + item.itemName);
		
		ShopScene.callCount++;
		int myCount = ShopScene.callCount;
		
		yield return new WaitForSeconds(0.03f);
		
		if (myCount != ShopScene.callCount) yield break;
		
		ShopScene.callCount = 0;
		
		// restore all item's "buy" status
		foreach (ShopItem si in transform.parent.GetComponentsInChildren<ShopItem>())
		{
			si.buyButton.SetActive(false);
			//si.buyScaleTweener.Backward();
			si.background.GetComponent<UISprite>().spriteName = "items_bg";
		}
		background.GetComponent<UISprite>().spriteName = "item_choose";
		
		
		
		if (Globals.mainPlayer.owningItems.Contains(item))
		{
			//if (item.requiredLevel <= Globals.mainPlayer.Level)
			{
				Globals.mainPlayer.wearItem(item);
			}
		}
		else
		{
	
			if (item.requiredLevel > Globals.mainPlayer.Level)
			{
				// not enoguh level popup?
			}
			else
			{
				//buyScaleTweener.Forward();
				if (checkEnoughDLCVersion())
					buyButton.SetActive(true);
			}
				
			
		}
		
		
		if (checkEnoughDLCVersion())
		{
			// how to wear? ....
			// i guess the fake player wear only....
			if (!ShopScene.fakePlayer.isWearingItem(item) && item.requiredLevel <= Globals.mainPlayer.Level)
			{
				ShopScene.fakePlayer.wearItem(item);
				Log.Debug("wearing item"+item.itemName);
				
				// hide some items
				List<ItemSubcategory> hideList= new List<ItemSubcategory>();
				
				if (!ItemSubcategories.getSubcategory(item.subCategoryId).subcatsToHide.Equals(""))
				{
					foreach (String s in ItemSubcategories.getSubcategory(item.subCategoryId).subcatsToHide.Split(','))
					{
						hideList.Add(ItemSubcategories.getSubcategory(Int32.Parse(s)));
					}
				}
				
				
				foreach(Item t in ShopScene.fakePlayer.wearingItems)
				{
					if (hideList.Contains(ItemSubcategories.getSubcategory(t.subCategoryId)))
					{
						Item noneItem = Items.getNoneItemOfSubcategory(t.subCategoryId);
						
						if (noneItem != null)
							ShopScene.fakePlayer.wearItem(noneItem);
					}
				}
				// end of hide some items
				
				hideList.Add(ItemSubcategories.getSubcategory(item.subCategoryId));
				
				//girl.GetComponent<CharacterManager>().GenerateCharacter(ShopScene.fakePlayer);
				yield return StartCoroutine(girl.GetComponent<CharacterManager>().EnumGenerateCharacter(ShopScene.fakePlayer));
			
				string animationStr = girl.GetComponent<CharacterManager>().GetClosetAnimationString(ItemSubcategories.getSubcategory(item.subCategoryId));
				girl.GetComponent<CharacterManager>().InsertAnimation(animationStr, false); // test line
				girl.GetComponent<CharacterManager>().QueueAnimation("pose01", true); // test line
				
				// update index please
			}
		}
		else
		{
			// not enough DLC version
			if (!isByDrag && item.requiredLevel <= Globals.mainPlayer.Level)
			{
				if (!DownloadManager.SharedManager.isDownloading)
					DownloadManager.SharedManager.AskForDLCUpgrade(false, item.dlcId, -1);
				else
					DownloadManager.SharedManager.AskForDLCProgress(false, item.dlcId, -1);
			}
		}
	
		// the fake character gen?
		
		
		SpringPanel.Begin(scrollPanelT, new Vector3(0f,-transform.localPosition.y ,0f), 5f);
		
		if (item.requiredLevel <= Globals.mainPlayer.Level && checkEnoughDLCVersion())
			sslm.showItemName(item);
		
		
		
		
	}
	
	public void BuyItem()
	{
		
		
		// check if enough $$
		if (!checkEnoughCurrency())
		{
			// TODO: link to ATM
			/*if (item.gems > 0)
				PopupManager.ShowInformationPopup("You don't have enough gems to buy...");
			else
				PopupManager.ShowInformationPopup("You don't have enough coins to buy...");*/
			
			//PopupManager.ShowIAPPopup();
			
			if (offerGems/*item.gems*/ > 0)
			{
				PopupManager.ShowNotEnoughCurrencyPopup("Gems", offerGems - Globals.mainPlayer.Gems, false);
				
					
			}
			else
			{
				PopupManager.ShowNotEnoughCurrencyPopup("Coins", offerCoins - Globals.mainPlayer.Coins, false);
					
			}
			
			return;
		}
		
		
		
		Globals.mainPlayer.owningItems.Add(item);
		Globals.mainPlayer.wearItem(item);
		
		buyButton.SetActive(false);
		priceBlock.SetActive(false);
		discountBlock.SetActive(false);
		newBlock.SetActive(false);
		block.SetActive(true);
		background.GetComponent<UISprite>().spriteName = "items_bg";
		
		if (item.gems > 0)
		{
			Globals.mainPlayer.GainGems(-offerGems/*-item.gems*/);
		}
		else
		{
			Globals.mainPlayer.GainCoins(-offerCoins/*-item.price*/);
		}
		
		if (VisitShop.shop.shopId == QuestFlags.GetFlag(13).intValue &&
			item.subCategoryId == QuestFlags.GetFlag(14).intValue)
		{
			QuestFlags.SetFlag(15, QuestFlags.GetFlag(15).intValue+1);
		}

		if (VisitShop.shop.shopId == 2)
		{
			QuestFlags.SetFlag(19, true);
		}
		
		PopupManager.ShowItemBroughtPopup(false);
		
		//PPA
		MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.BuyAnItemAnyShop);
		
		//Flurry Event
		Hashtable param = new Hashtable();
		param["itemName"] 	= item.itemName;
		param["price"] 		= item.price;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Purchased_Item, param);
	}
	
	public bool checkEnoughCurrency()
	{
		if (offerGems/*item.gems*/ > 0)
		{
			if (Globals.mainPlayer.Gems < offerGems/*item.gems*/)
				return false;
		}
		else
		{
			if (Globals.mainPlayer.Coins < offerCoins/*item.price*/)
				return false;
		}
		
		return true;
	}

	public bool checkEnoughDLCVersion()
	{
		return !Items.requireDLC(item);
	}

	void OnDestroy()
	{
		tex = null;
		
	}
}

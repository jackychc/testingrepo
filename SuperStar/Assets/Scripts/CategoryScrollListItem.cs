using System;
using UnityEngine;
using System.Collections.Generic;

public class CategoryScrollListItem : MonoBehaviour {
	
	
	
	public ItemCategory category;
		
	// Use this for initialization
	void Start () {
		transform.GetComponent<UISprite>().spriteName = category.iconSpriteName;
	}
	
	public void DelayedStart()
	{
		
	}
	

}

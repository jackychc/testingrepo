using System;
using System.Collections.Generic;


public class Character
{
	CharacterType type;
	List<ItemSubcategory> parts;
	string defaultAnimation;
	
	public Character(CharacterType pType, List<ItemSubcategory> pParts, string pAnimation)
	{
		this.type = pType;
		this.parts = pParts;
		this.defaultAnimation = pAnimation;
	}
	
	public CharacterType getCharacterType()
	{
		return this.type;
	}
	
	public List<ItemSubcategory> getCharacterParts()
	{
		return this.parts;
	}
	
	public string getDefaultAnimation()
	{
		return this.defaultAnimation;
	}
	
}



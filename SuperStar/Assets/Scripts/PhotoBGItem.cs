using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class PhotoBGItem : MonoBehaviour {
	
	//Listener
	public delegate void OnPhotoBGItemClicked(GameObject photoBGItem);
	
	public UITexture icon;
	public UISprite spriteTick;
	public Item item;
	public OnPhotoBGItemClicked photoBGItemClickHandler;
	public GameObject dlcIcon;
	
	// Use this for initialization
	void Start () 
	{
		StartCoroutine(ReallyStart());
	}
	
	IEnumerator ReallyStart()
	{
		//initialize the item
		if (item != null)
		{
			Destroy(this.transform.GetComponent<UIPanel>());
			
			Texture2D tex;
			tex = (Texture2D) Resources.Load("PhotoshotBackgrounds/" + item.itemId + "_icon");
			
			if (tex == null)
			{
				yield return StartCoroutine(LoadIconFromPersistent(item, result => tex = result));
			}
			
			string shaderName 	= "UnlitTransparentColored";
			Shader shader 		= Resources.Load("Shaders/" + shaderName) as Shader;
			if (shader != null)
				icon.material = new Material(shader);
			else
				icon.material = new Material(Shader.Find ("Unlit/Transparent Colored"));
	        icon.mainTexture = tex;
	        icon.transform.localScale = new Vector3(97f,145f,1f);




			if (Items.requireDLC(item))
			{
				dlcIcon.SetActive(true);
			}
			else
			{
				dlcIcon.SetActive(false);
			}

			
			//string[] meta 		= item.meta.Split(';');
			//string itemName 	= meta[0];			
			//icon.spriteName = itemName;
			
			//UITexture iconTexture = icon.GetComponent<UITexture>();
			//Texture2D tex = (Texture2D) Resources.Load("PhotoshotBackgrounds/" + itemName);		
			//iconTexture.material = new Material (Shader.Find ("Unlit/Transparent Colored"));
	        //iconTexture.material.mainTexture = tex;
	        //icon.transform.localScale = new Vector3(120f,120f,1f);
		}
		else
		{
			Log.Debug("Error The Item is null");
			
		}
	}
	
	public IEnumerator LoadIconFromPersistent(Item i, Action<Texture2D> returnMe)
	{
		WWW loader = new WWW("file://" + Application.persistentDataPath + Path.DirectorySeparatorChar + i.itemId + "_icon.png");
		yield return loader;
		
		if (loader.error != null)
		{
			Log.Debug("Photoshot Item Cannot load the icon file with itemId: " + i.itemId);
		}
		else
		{
			if (loader.texture != null)
			{
				returnMe((Texture2D)loader.texture);
			}
			else
			{
				Log.Debug("Photoshot Item Cannot load the texture with itemId: " + i.itemId);
			}
		}
	}
	
	public void ItemClicked()
	{
		Log.Debug("Item " + item.itemName + " clicked!!");	


		if (!Items.requireDLC(item))
		{	
			if (photoBGItemClickHandler != null)
				photoBGItemClickHandler(this.gameObject);
		}
		else
		{
			if (!DownloadManager.SharedManager.isDownloading)
				DownloadManager.SharedManager.AskForDLCUpgrade(false, item.dlcId, -1);
			else
				DownloadManager.SharedManager.AskForDLCProgress(false, item.dlcId, -1);
		}
	}
}

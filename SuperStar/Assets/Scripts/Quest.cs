using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Quest: Activity
{
	public bool isRunning; //!
	public bool isCompleted;
	public bool completedOnce;
	public bool isNew;
	public bool canRepeat;
	public bool canSkip;
	public string stringAction;
	public QuestAction action;
	
	public int sortOrder;
	public string title = "";
	public string description = "";
	public int timeRequired = 0;
	public int timeLeft = 0;
	public int gemsToFinish = 2;
	public Reward reward;
	public int rewardId = -1;
	
	public string goToSceneName;
	
	public Quest(int pQuestid, int pSortOrder, string pTitle,
		string pDescription, bool pRunning, bool pCompleted, bool pCompletedOnce, bool pNew, bool pRepeat, bool pSkip, string pActionClass, string pGoTo, int pTimeRequired, int pTimeLeft,
		int pGemsToFinish, int pRewardId)
	{
		this.activityId = pQuestid;
		this.sortOrder = pSortOrder;
		this.title = pTitle;
		this.description = pDescription;
		this.isRunning = pRunning;
		this.isCompleted = pCompleted;
		this.completedOnce = pCompletedOnce;
		this.isNew = pNew;
		this.timeRequired = pTimeRequired;
		this.timeLeft = pTimeLeft;
		this.canRepeat = pRepeat;
		this.canSkip = pSkip;
		this.stringAction = pActionClass;
		this.action = Quests.getAction(pActionClass);
		this.action.quest = this;
		this.goToSceneName = pGoTo;
		this.gemsToFinish = pGemsToFinish;
		this.rewardId = pRewardId;
		this.reward = Rewards.getReward(pRewardId);
		
	}
	
	public void StartQuest()
	{
		isRunning = true;
		action.OnQuestStart();
	}
	
	public void ContinueQuest()
	{
		if (!isRunning)
		{
			StartQuest();
		}
	}
	
	public void StopQuest()
	{
		isRunning = false;
		action.OnQuestStop();
	}
	
	public void CheckFinish()
	{
		/*if (action.FinishRequirement())
		{
			isRunning = false;
			isCompleted = true;
			
			action.OnFinishRequirementMeet();
			
			
			PopupManager.ShowQuestCompletePopup(this);
			
			
		}*/
	}
	
}


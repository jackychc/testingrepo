using UnityEngine;
using System.Collections;

public class FPSShowerBehaviour : MonoBehaviour {
	public  float updateInterval = 0.5F;
	private float accum   = 0; // FPS accumulated over the interval
	private int   frames  = 0; // Frames drawn over the interval
	private float timeleft; // Left time for current interval
	
	bool show = false;
	
	// Use this for initialization
	void Start () {
		if (!show)
			Destroy(transform.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
		//if (!show) {transform.GetComponent<UILabel>().text = ""; return; }
		
	    timeleft -= Time.deltaTime;
	    accum += Time.timeScale/Time.deltaTime;
	    ++frames;
	 
	    // Interval ended - update GUI text and start new interval
	    if( timeleft <= 0.0 )
	    {
	        // display two fractional digits (f2 format)
			float fps = accum/frames;
			
			
			
			string format = System.String.Format("FPS:{0:F2}",fps);
			//Log.Debug(format);
			transform.GetComponent<UILabel>().text = format;
	 
		
	        timeleft = updateInterval;
	        accum = 0.0F;
	        frames = 0;
	    }
	}
}

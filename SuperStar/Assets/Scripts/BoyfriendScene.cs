using UnityEngine;
using System.Collections;
using System.IO;

public class BoyfriendScene : MonoBehaviour
{
	GameObject newCharacter;
	GameObject boyfriendModel;
	public GameObject backButton;
	
	Boyfriend currentBf;
	
	public UITexture background;
	public UILabel nameTag;
	public GameObject flirtTrigger, flirtMenu, heart;
	public UILabel flirtPercentage;
	public UIFilledSprite flirtFilling;
	public UISprite flirtHeart;
	public ScaleTweener heartScaleTweener;
	public ParticleSystem heartParticleSystem;
	public ParticleSystem heartSubParticleSystem;
	public GameObject toggler;
	
	public ParticleSystem flirtCompleteSystem;
	
	public ParticleSystem[] flirtOngoingSystem = new ParticleSystem[3];
	int systemIndex;
	
	public UILabel[] flirtCostLabels = new UILabel[3];
	
	bool flirting, onDelay, menuOn;
	
	int apparentExp;
	
	Vector3 t;
	
	IEnumerator Start()
	{
		currentBf = VisitBoyfriend.boyfriend;
		
		background.mainTexture = (Texture2D) Resources.Load("Backgrounds/boyfriendLocation"+VisitBoyfriend.location);
		
		// load YOU
		/*if (GameObject.Find("FEMALE") == null)
		{
			newCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			newCharacter.name = "FEMALE";
			newCharacter.AddComponent<CharacterManager>();
			newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference2"));
			newCharacter.GetComponent<CharacterManager>().GenerateCharacter(Globals.mainPlayer);
			//DontDestroyOnLoad(newCharacter);
		}
		
		else
		{
			newCharacter = GameObject.Find("FEMALE");
			newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference2"));*/
			newCharacter = Globals.globalCharacter;
			newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference2"));
			yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
		//}
		
		newCharacter.GetComponent<CharacterManager>().InsertAnimation(RandomIdleAnimationString("g")+"_g");
		
		
		yield return StartCoroutine(GetBoyfriendMesh(VisitBoyfriend.boyfriend));
		if (boyfriendModel == null)
		{
			DownloadManager.SharedManager.AskForDLCUpgrade(false, VisitBoyfriend.boyfriend.dlcId, 0);
			yield break;
		}
		else
		{
			boyfriendModel.name = "BOYFRIEND";
			boyfriendModel.AddComponent<CharacterManager>();
			boyfriendModel.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
			boyfriendModel.GetComponent<CharacterManager>().InsertShadow();
			boyfriendModel.GetComponent<CharacterManager>().InsertAnimation(RandomIdleAnimationString("b")+"_b");
			/*boyfriendModel.animation.AddClip(Resources.Load("CharacterAssets/Animation/BF_idle_b") as AnimationClip,"BF_idle_b");
			boyfriendModel.animation.wrapMode = WrapMode.Loop;
			boyfriendModel.animation.Play("BF_idle_b");*/
				
			apparentExp = currentBf.flirtExp;
			UpdateFlirtBar();
			
			//backButton.GetComponent<SceneManager>().goToScene = VisitBoyfriend.fromScene;
			
			
			nameTag.text = currentBf.boyfriendName;
			
			
			for (int i = 0; i < flirtCostLabels.Length; i++)	
			{
				flirtCostLabels[i].text = currentBf.flirtCost[i] + "";
			}
		}
		
		SoundManager.SharedManager.ChangeMusic("SuperstarStory_" + (VisitBoyfriend.isSexy? "CarRomantic":"CarHappy"));
		
		MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.DateFirstBF);
		
		StartCoroutine(LoopAnimations());
		StartCoroutine(DelayedStart());
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.BOYFRIEND);
	}
	
	IEnumerator DelayedStart()
	{
		yield return new WaitForSeconds(0.5f);
		
		if (QuestFlags.GetFlag(-104).intValue == 0)
		{
			StartTutorial();
		}
	}
	
	void StartTutorial()
	{
		QuestFlags.SetFlag(-104, 1);
		PopupManager.ShowTutorialPopup(4, false);
	}
	
	
	
	public void UpdateFlirtBar()
	{
		StartCoroutine(UpdateFlirtBarRoutine());
	}
	
	public IEnumerator UpdateFlirtBarRoutine()
	{
		float proportion = Mathf.Min(1.0f, (apparentExp*1.0f/currentBf.maxFlirtExp));
		flirtPercentage.text = ((int) (proportion *100f))+"%";
		flirtFilling.fillAmount = proportion;
		
		string heartNo = "01";
		
		if (proportion >= 0.25f)
			heartNo = "02";
		if (proportion >= 0.5f)
			heartNo = "03";
		if (proportion >= 0.75f)
			heartNo = "04";
		
		float scale = 30f + Mathf.Lerp(0f,20f,proportion);
		flirtPercentage.transform.localScale = new Vector3(scale,scale,1f);
		
		
		flirtHeart.spriteName = "loveBar_heart_"+heartNo;
		flirtHeart.MakePixelPerfect();
		
		t = heartScaleTweener.transform.localPosition;
		t.y = Mathf.Lerp(-380f, 0f, proportion);
		heartScaleTweener.transform.localPosition = t;
		
		if (apparentExp >= currentBf.maxFlirtExp)
		{
			// 100%, bring him home!
			Globals.mainPlayer.CurrentBoyfriendId = currentBf.boyfriendId;
			
			// start counting
			StartCoroutine(FlirtCompleteRoutine());
			
		}
		else if (apparentExp < currentBf.flirtExp)
		{
			apparentExp = Mathf.Min(currentBf.maxFlirtExp, apparentExp+10);
			
			yield return new WaitForSeconds(0.01f);
			StartCoroutine(UpdateFlirtBarRoutine());
		}
		
	}
	
	IEnumerator FlirtCompleteRoutine()
	{
		SoundManager.SharedManager.FadeOutBGM();
		
		currentBf.rewardTimer = Boyfriends.checkPoints[0];
		
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("BF_kiss_g",false);
		newCharacter.GetComponent<CharacterManager>().QueueAnimation(RandomIdleAnimationString("g")+"_g");
		
		boyfriendModel.animation.Stop();
		boyfriendModel.GetComponent<CharacterManager>().InsertAnimation("BF_kiss_b",false);
		boyfriendModel.GetComponent<CharacterManager>().QueueAnimation(RandomIdleAnimationString("b")+"_b");
		
		heartParticleSystem.Play();
		
		SoundManager.SharedManager.PlaySFX("boyfriend_flirt_complete");	
		
		yield return new WaitForSeconds(2.667f);
		
		
		//SoundManager.SharedManager.PlaySFX("boyfriend_flirt_complete");	
		
		flirtCompleteSystem.Play();
		
		yield return new WaitForSeconds(2.0f);
		
		//SoundManager.SharedManager.ChangeMusic("SuperstarStory_" + (VisitBoyfriend.isSexy? "CarRomantic":"CarHappy"));
		
		
		PopupManager.ShowFlirtCompletePopup(VisitBoyfriend.boyfriend, false);
		
		yield return new WaitForSeconds(1.0f);
		
		if (Globals.settings_bgm)
			SoundManager.SharedManager.FadeInBGM();
	}
	
	public void ShowFlirtMenu()
	{
		toggler.SetActive(true);
		flirtTrigger.SetActive(false);
		menuOn = true;
		flirtMenu.GetComponent<AnchorTweener>().Forward();
	}
	
	public void ToggleFlirtMenu()
	{
		//flirtTrigger.SetActive(false);
		menuOn = !menuOn;
		if (menuOn)
			flirtMenu.GetComponent<AnchorTweener>().Forward();
		else
			flirtMenu.GetComponent<AnchorTweener>().Backward();
	}
	
	public void Flirt1Clicked()
	{	
		if (onDelay) return;
		
		if (currentBf.flirtExp >= currentBf.maxFlirtExp) return;
		
		if (Globals.mainPlayer.Energy < currentBf.flirtCost[0])
		{
			PopupManager.ShowNotEnoughCurrencyPopup("Energy", currentBf.flirtCost[0] - Globals.mainPlayer.Energy, false);
			return;
		}
		
		StartCoroutine(FlirtClicked(1));
	}
	public void Flirt2Clicked()
	{
		if (onDelay) return;
		
		if (currentBf.flirtExp >= currentBf.maxFlirtExp) return;
		
		if (Globals.mainPlayer.Coins < currentBf.flirtCost[1])
		{
			PopupManager.ShowNotEnoughCurrencyPopup("Coins", currentBf.flirtCost[1] - Globals.mainPlayer.Coins, false);
			return;
		}
		
		StartCoroutine(FlirtClicked(2));
	}
	public void Flirt3Clicked()
	{
		if (onDelay) return;
		
		if (currentBf.flirtExp >= currentBf.maxFlirtExp) return;
		
		if (Globals.mainPlayer.Gems < currentBf.flirtCost[2])
		{
			PopupManager.ShowNotEnoughCurrencyPopup("Gems", currentBf.flirtCost[2] - Globals.mainPlayer.Gems, false);
			return;
		}
		
		StartCoroutine(FlirtClicked(3));
	}
	
	public IEnumerator FlirtClicked(int flirtType)
	{	
		//onDelay = true;	
		if (!flirting)
		{
			StartCoroutine(KissAnimation());
		}
		
		
		
		flirtOngoingSystem[systemIndex].Play();
		systemIndex = (systemIndex+1)%3;
		
		onDelay = true;
		yield return new WaitForSeconds(0.218f);
		
		if (currentBf.flirtExp == 0)
		{
			//LogFlurryEvent
			Hashtable flirtStStateParam = new Hashtable();
			flirtStStateParam["Start"] = currentBf.boyfriendName;
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.BF_Flirt_State, flirtStStateParam);
		}
		
		
		switch (flirtType)
		{
			
			case 1:
				currentBf.flirtExp += Mathf.CeilToInt(currentBf.maxFlirtExp / currentBf.flirtRepeat[0]);
				Globals.mainPlayer.GainEnergy(-currentBf.flirtCost[0]);
				break;
			case 2:
				currentBf.flirtExp += Mathf.CeilToInt(currentBf.maxFlirtExp / currentBf.flirtRepeat[1]);
				Globals.mainPlayer.GainCoins(-currentBf.flirtCost[1]);
				break;
			case 3:
				currentBf.flirtExp += Mathf.CeilToInt(currentBf.maxFlirtExp / currentBf.flirtRepeat[2]);
				Globals.mainPlayer.GainGems(-currentBf.flirtCost[2]);
				break;
		}
		
		if (currentBf.flirtExp == 0)
		{
			//LogFlurryEvent
			Hashtable flirtEndStateParam = new Hashtable();
			flirtEndStateParam["End"] = currentBf.boyfriendName;
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.BF_Flirt_State, flirtEndStateParam);
		}
		
		//LogFlurryEvent
		Hashtable giftMethodParam = new Hashtable();
		giftMethodParam[currentBf.boyfriendName] = (flirtType == 1)? "Energy":((flirtType == 2)? "Coins": "Gems");
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.BF_Flirt_Method, giftMethodParam);
		
		UpdateFlirtBar();
		
		//flirting = false;	
		
		onDelay = false;
	}
	
	IEnumerator KissAnimation()
	{
		QuestFlags.SetFlag(3,true);
		flirting = true;
		
		// todo: replace with another animation
		
		
		
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("BF_progress_g",false);
		newCharacter.GetComponent<CharacterManager>().QueueAnimation(RandomIdleAnimationString("g")+"_g");
		/*
		boyfriendModel.animation.Stop();
		boyfriendModel.GetComponent<CharacterManager>().InsertAnimation("BF_kiss_b",false);
		boyfriendModel.GetComponent<CharacterManager>().QueueAnimation("BF_idle_b");
		*/
		
		
		heartScaleTweener.speed = 3f;
		yield return new WaitForSeconds(2.433f);
		heartScaleTweener.speed = 1f;
		
		flirting = false;
	}
	
	IEnumerator SuicideAfterSeconds(GameObject go, float sec)
	{
		yield return new WaitForSeconds(sec);
		Destroy(go);
	}
	
	public void GoToStreetScene()
	{	
		if (currentBf.flirtExp >= currentBf.maxFlirtExp) return;
		
		SceneManager.SharedManager.LoadScene(Scenes.STREET, true);
	}
	
	public IEnumerator LoopAnimations()
	{
		while(true)
		{			
			if (newCharacter != null && newCharacter.GetComponent<CharacterManager>().animationQueue.Count <= 3)
			{
				newCharacter.GetComponent<CharacterManager>().QueueAnimation(RandomIdleAnimationString("g")+"_g", true);	
			}	
			if (boyfriendModel != null && boyfriendModel.GetComponent<CharacterManager>().animationQueue.Count <= 3)
			{
				boyfriendModel.GetComponent<CharacterManager>().QueueAnimation(RandomIdleAnimationString("b")+"_b", true);	
				
			}
			yield return new WaitForSeconds(1.0f);			
		}
	}
	
	string RandomIdleAnimationString(string t)
	{
		int ran = UnityEngine.Random.Range(1,5+1);
		
		if (t.Equals("b") && currentBf.boyfriendId == 7) return (ran%2 == 0? "BF_pose02":"BF_idle");
		
		switch(ran)
		{
			//abcde
			
			case 1: return "BF_pose01";
			case 2: return "BF_pose02";
			default: return "BF_idle"; 
			
		}
	}
	
	private IEnumerator GetBoyfriendMesh(Boyfriend bf)
	{
		GameObject returnMe = null;
		
		Object obj = Resources.Load("Boyfriends/"+bf.meshName);			
		if (obj == null)
		{			
			// try load from streaming assets folder instead
			string filePath 	= Application.persistentDataPath + "/"  + bf.meshName +".dlc";
			FileInfo fileInfo 	= new FileInfo(filePath);
		    if (fileInfo != null && fileInfo.Exists)
		    {
				WWW bundle = new WWW("file://" + filePath);
				yield return bundle;
				
				//Log.Debug("am i here?" +item);
				if (bundle.error != null)
				{
					Log.Debug("Boyfriend Scene read the boyfriend bundle error");
				}
				else
				{
					if (bundle.assetBundle != null)
					{
						returnMe = (GameObject) Instantiate(bundle.assetBundle.mainAsset as GameObject);				
						bundle.assetBundle.Unload(false);
						
						//Re-assign the shaders to the boyfriend model						
						SkinnedMeshRenderer[] smrs = returnMe.GetComponentsInChildren<SkinnedMeshRenderer>();
						for (int i = 0; i < smrs.Length; i++)
						{
							SkinnedMeshRenderer smr = smrs[i];
							Material[] materials 		= smr.materials;
							for (int j = 0; j < materials.Length; j++)
							{
								Material material 	= materials[j];
								string shaderName	= material.shader.name.Replace(" ", "").Replace("/", "").Replace("-", "");
								Shader shader 		= Resources.Load("Shaders/" + shaderName) as Shader;
								if (shader != null)
									material.shader	= shader;
								else
									material.shader = Shader.Find(material.shader.name);
							}						
						}
					}
					else
					{
						Log.Debug("Boyfriend Scene Cannot read the boyfriend asset bundle");	
					}
				}
		    }
			else
			{
				Log.Warning("PATH FAIL:"+ bf.meshName);
			}
			
		}
		else
		{
			returnMe = (GameObject) Instantiate(obj as GameObject);
		}
		
		boyfriendModel = returnMe;
	}
}



﻿using UnityEngine;
using System.Collections;

public class HeadMorphUpdater : MonoBehaviour {

	bool assigned = false;
	bool lip03Amend = false;
	MegaMorph morpher;

	MegaMorphChan targetEyeChannel, lip03Channel;


	MegaMorphChan eyecloseL, eyecloseR, mouthOpen, blaugh, smile, laugh, sad, cry, confuse, angry;


	// Use this for initialization
	void Start () {
		morpher = GetComponent<MegaMorph>();
		eyecloseL = morpher.GetChannel("head_eyecloseL");
		eyecloseR = morpher.GetChannel("head_eyecloseR");
		mouthOpen = morpher.GetChannel("head_mouth_open");
		blaugh = morpher.GetChannel("head_Blaugh");
		smile = morpher.GetChannel("head_smile_02");
		laugh = morpher.GetChannel("head_laugh");
		sad = morpher.GetChannel("head_sad");
		cry = morpher.GetChannel("head_cry");
		confuse = morpher.GetChannel("head_confuse");
		angry = morpher.GetChannel("head_angry");

		targetEyeChannel = morpher.GetChannel("head_eye01");
		lip03Channel = morpher.GetChannel("head_lip_03");
	}
	
	// Update is called once per frame
	void Update () {


		if (lip03Amend)
		{
			// lip03's update
			lip03Channel.SetTarget(100f - (    blaugh.Percent * 0.2f
			                                 + laugh.Percent * 0.2f
			                                 + cry.Percent * 0.5f
			                                   ) ,10000f);
		}


		if (assigned)
		{
			//eye's update
			targetEyeChannel.SetTarget(100f - (  eyecloseL.Percent*0.5f
			                                  + eyecloseR.Percent*0.5f
			                                  + blaugh.Percent
			                                  + smile.Percent * 0.05f
			                                  + laugh.Percent * 0.2f
			                                  + sad.Percent * 0.2f
			                                  + cry.Percent
			                                  + confuse.Percent
			                                  + angry.Percent * 0.2f
			                                  ) ,10000f);
		}
	}

	public void cancelTargetEyeChannel()
	{
		assigned = false;
	}

	public void setTargetEyeChannel(string name)
	{
		targetEyeChannel = morpher.GetChannel(name);
		assigned = true;
	}

	public void setLip03Amend(bool v)
	{
		lip03Amend = v;
	}
}

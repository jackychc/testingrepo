using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class StreetMenuManager : MonoBehaviour, DownloadManagerCallbackInterface, DLCCompletePopupCallbackInterface
{
	//public static int numEnterStreet = 0;
	public PositionTweener menuTweener;	
	DownloadManager dlm;

	public GameObject			dlcButtonHintsGroupObj;
	public UIFilledSprite 		dlcProgressBar;
	public GameObject   		dlcButton;
	public UILabel 				percentLabel;
	public GameObject 			downloadButtonLabel;
	
	public GameObject 			exclaimation;
	public GameObject 			exclaimationA, exclaimationB;
	
	private bool checkingDLC 		= false;

	public static bool hasShownDLCButtonHints		= false;
	public static bool questExclaimation;
	public static bool hasShowDLCPopup				= false;
	
	// Use this for initialization
	void OnDestroy()
	{
		DownloadManager.SharedManager.RemoveCallback(this);
	}
	
	void Start()
	{
		//numEnterStreet++;
		StartCoroutine(DelayedStart());
		DownloadManager.SharedManager.SetCallback(this);
	}
	
	IEnumerator DelayedStart ()
	{
		dlm = DownloadManager.SharedManager;		
		dlcButton.gameObject.SetActive(false);
		dlcButtonHintsGroupObj.SetActive(false);

		if (!dlm.isDownloading)
		{
			if (!checkingDLC)
				yield return StartCoroutine(CheckDLC());

			if (!Globals.IsDLCCompleted)
			{
				if (!dlm.haveDLC)
				{
					dlcProgressBar.fillAmount 	= 0f;
					percentLabel.text 			= "";	
				}
				else
				{
					dlcProgressBar.fillAmount 	= 0f;
					percentLabel.text 			= "";

					dlcButton.gameObject.SetActive(true);
					exclaimationB.SetActive(true);

					dlcButtonHintsGroupObj.SetActive(!hasShownDLCButtonHints && !Globals.hasFirstDLC);

					if (!hasShowDLCPopup)
					{
						DownloadManager.SharedManager.AskForDLCUpgrade(true, -1, 0, 2, true);
						hasShowDLCPopup = true;
					}
				}
			}
			else
			{
				dlcProgressBar.fillAmount 	= 1f;
				percentLabel.text 			= "100%";
				dlcButton.gameObject.SetActive(true);
			}
		}
		else
		{
			dlcButton.gameObject.SetActive(true);
			
			// already donwloading...
			Log.Debug("already DLing");	
			
			exclaimationB.SetActive(false);
			
			SysFontLocalizer localizer 	= downloadButtonLabel.GetComponent<SysFontLocalizer>();
			localizer.identifier		= "dlc_downloadingDesc";
			localizer.ReloadText();			
				
			dlcProgressBar.fillAmount 	= dlm.GetApparentProgress();
			percentLabel.text 			= (int)(dlcProgressBar.fillAmount * 100f) + "%";
		}
		
		CheckQuest();
		//InvokeRepeating("Move", 0f, 0.03f);
		
		yield return null;
		
	}
	
	void CheckQuest()
	{
		Quests.RefreshQuestList();

		if (Globals.mainPlayer.NumOfSessions == 1)
		{
			exclaimationA.SetActive(true);
			questExclaimation = true;
		}
		else
		{
			foreach (Quest q in Quests.currentQuests)
			{
				if (q.action.FinishRequirement())
				{
					//exclaimation.SetActive(true);

					exclaimationA.SetActive(true);
					questExclaimation = true;
				}
			}
		}
	}
	
	IEnumerator CheckDLC()
	{
		
		checkingDLC = true;
		bool has = false;
		yield return StartCoroutine(dlm.CheckDLC(true, -1, 0, result=> has = result));

		/*
		NetworkManager.State state = NetworkManager.SharedManager.NetworkState;
		if (state.Equals(NetworkManager.State.Success))
		{
			if (has)
			{
				//exclaimation.SetActive(true);
				exclaimationB.SetActive(true);
			}
		}
		*/
		
		
		checkingDLC= false;
	}
	
	void Update()
	{
		if (!Globals.IsDLCCompleted)
		{
			if (dlm.isDownloading)
			{
				dlcButton.gameObject.SetActive(true);
			}

			float progress = dlm.GetApparentProgress();
			if (progress > 0f)
			{			
				if (progress < 1.0f)
				{
					SysFontLocalizer localizer 	= downloadButtonLabel.GetComponent<SysFontLocalizer>();			
					if (!localizer.identifier.Equals("dlc_downloadingDesc"))
					{
						localizer.identifier		= "dlc_downloadingDesc";
						localizer.ReloadText();
					}
					
					dlcProgressBar.fillAmount 	= progress;
					percentLabel.text 			= Mathf.RoundToInt(dlcProgressBar.fillAmount * 100f) + "%";
				}
				else
				{
					SysFontLocalizer localizer 	= downloadButtonLabel.GetComponent<SysFontLocalizer>();			
					if (!localizer.identifier.Equals("menu_dlc"))
					{
						localizer.identifier		= "menu_dlc";
						localizer.ReloadText();
					}
					
					dlcProgressBar.fillAmount 	= 0f;
					percentLabel.text 			= "";
				}
			}
			else
			{
				dlcProgressBar.fillAmount 	= 0f;
				percentLabel.text 			= "";
			}
		}
		/*else
		{
			dlcProgressBar.fillAmount 	= 0f;
			percentLabel.text 			= "";
		}*/
		
	}
	
	//public IEnumerator UpdatePerSecond ()
	/*
	void Move()
	{
		dlcProgressBar.fillAmount = dlm.GetApparentProgress();
		
		if (dlcProgressBar.fillAmount > 0f)
		{
			percentLabel.text = Mathf.RoundToInt(dlcProgressBar.fillAmount * 100f) + "%";
			
			if (dlcProgressBar.fillAmount < 1.0f)
			{
				SysFontLocalizer localizer 	= downloadButtonLabel.GetComponent<SysFontLocalizer>();			
				if (!localizer.identifier.Equals("dlc_downloadingDesc"))
				{
					localizer.identifier		= "dlc_downloadingDesc";
					localizer.ReloadText();
				}
			}
			else
			{
				CancelInvoke("Move");
			}
		}
	}
	*/
	
	public void ToggleMenu()
	{
		if (menuTweener.proportion < 1f)
		{
			exclaimation.SetActive(false);
			menuTweener.Forward();
		}
		else
		{
			StartCoroutine(DelayedHide());	
			menuTweener.Backward();
		}
	}
	
	IEnumerator DelayedHide()
	{
		yield return new WaitForSeconds(0.5f);
		if (exclaimationA.activeSelf || exclaimationB.activeSelf)
				exclaimation.SetActive(true);
	}
	
	int clickedTimes;
	public void QuestClicked()
	{
		MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Street_Click_Quest);

		questExclaimation = false;
		exclaimationA.SetActive(false);
		
		PopupManager.ShowQuestListPopup(false);
	}
	
	public void DLCClicked()
	{
		if (!Globals.hasFirstDLC)
		{
			dlcButtonHintsGroupObj.SetActive(false);
			hasShownDLCButtonHints = true;
			Globals.hasFirstDLC = true;
			Globals.mainPlayer.Save();
		}

		StartCoroutine(DLCClickedRoutine());
	}
	
	IEnumerator DLCClickedRoutine()
	{
		MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Street_Click_DLC);
		exclaimationB.SetActive(false);
		
		if (dlm.isDownloading)
		{
			DownloadManager.SharedManager.AskForDLCProgress(true, -1, 0);
		}
		else
		{
			if (!Globals.IsDLCCompleted)
			{
				exclaimationB.SetActive(false);


				yield return StartCoroutine(CheckDLC());

				NetworkManager.State state = NetworkManager.SharedManager.NetworkState;
				if (state.Equals(NetworkManager.State.Success))
				{
					if (!dlm.haveDLC)
					{
						PopupManager.ShowInformationPopup(Language.Get("dlc_latestVersion"), false);
					}
					else
					{
						if (Globals.dlcProgress >= 100)
						{
							Globals.dlcProgress = 0;
						}
						
						percentLabel.text 			= Globals.dlcProgress+"%";
						dlcProgressBar.fillAmount 	= Globals.dlcProgress * 1f/100f;
	
						DownloadManager.SharedManager.AskForDLCProgress(true, -1, 0);
					}
				}
				else
				{
					PopupManager.ShowInformationPopup(Language.Get("msg_network_error"), true);	
				}
			}
			else
			{
				PopupManager.ShowDLCCompletePopup(this, true);
			}
		}
	}
	
	public void SettingsClicked()
	{
		PopupManager.ShowSettingsPopup(false);
	}
	
	#region DownloadManagerCallback
	public void OnDownloadManagerDLCCompleted()
	{	
		//Globals.IsDLCCompleted = true;
		//dlcButton.SetActive(false);
		
		
		
		/*SysFontLocalizer localizer 	= downloadButtonLabel.GetComponent<SysFontLocalizer>();			
		localizer.identifier		= "menu_dlc";
		localizer.ReloadText();
		
		
		
		percentLabel.text 			= "";
		dlcProgressBar.fillAmount 	= 0f;*/
	}
	#endregion

	#region DLCCompletePopup Callback
	public void OnDLCCompletePopupOKClicked()
	{
		Time.timeScale = 0f;
		Application.Quit();
	}

	public void OnDLCCompletePopupCancelClicked(){}
	#endregion
}


using UnityEngine;
using System.Collections;

public class IndexBlock : MonoBehaviour
{
	public Player target;
	public UILabel sexyLabel, livelyLabel;
	Vector2 sum, apparentSum;
	// Use this for initialization
	void Start ()
	{
		target = Globals.mainPlayer;
		apparentSum = Items.sumCharismaForList(target.wearingItems);
		
		InvokeRepeating("Move", 0.018f, 0.03f);
	}
	
	// Update is called once per frame
	void Move ()
	{
		sum = Items.sumCharismaForList(target.wearingItems);
		
		if (sum.x != apparentSum.x)
		{
			apparentSum.x = apparentSum.x + 1 * (int) (Mathf.Sign(sum.x - apparentSum.x));
		}
		if (sum.y != apparentSum.y)
		{
			apparentSum.y = apparentSum.y + 1 * (int) (Mathf.Sign(sum.y - apparentSum.y));
		}
		
		sexyLabel.text = "" + (int)apparentSum.x;
		livelyLabel.text = "" + (int)apparentSum.y;
	}
	
	public void StartTutorial()
	{
		QuestFlags.SetFlag(-105, 1);
		PopupManager.ShowTutorialPopup(5, false);
	}
}


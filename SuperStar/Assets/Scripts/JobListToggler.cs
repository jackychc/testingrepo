using UnityEngine;
using System.Collections;

public class JobListToggler : MonoBehaviour {
	public AnchorTweener anchorTweener;
	bool isForward = false;
	// Use this for initialization
	void Start () {
	
	}
	
	void Backward()
	{
		isForward = false;
		anchorTweener.Backward();
	}
	
	void Toggle()
	{
		isForward = !isForward;
		
		if (isForward)
		{
			anchorTweener.Forward();
		}
		else
		{
			anchorTweener.Backward();
		}
	}
}

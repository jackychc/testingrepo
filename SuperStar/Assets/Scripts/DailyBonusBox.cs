using UnityEngine;
using System.Collections;
using System;

public class DailyBonusBox : MonoBehaviour
{
	public int day;

	public UISysFontLabel dayLabel;
	public UISysFontLabel collectLabel;
	public UISprite triangleSprite;
	public UISprite frameSprite;
	public UISprite contentSprite;
	public UILabel amountLabel;
	public PositionTweener posTweener;

	public UISprite glowBg;
	public GameObject tick;

	int playerDay = (Globals.mainPlayer.DailyBonusDay+1);
	bool clicked;
	Reward reward;

	static Color yellowColor = new Color (204/255f, 190/255f, 29/255f, 1f);
	static Color pinkColor = new Color (210/255f, 17/255f, 205/255f, 1f);

	// Use this for initialization
	void Start ()
	{
		Set(DailyBonus.getRewardOfDay(day));

	}

	public void Set(Reward r)
	{
		reward = r;

		if (reward.gems > 0)
		{
			triangleSprite.spriteName = "daily_bonus_day5";
			frameSprite.spriteName = "daily_bonus_bg_a";
			glowBg.spriteName = "daily_bonus_bg_a_glow";
			contentSprite.spriteName = "daily_bonus_coin_5";
			collectLabel.color = pinkColor;
			amountLabel.text = reward.gems + "";
		}

		else if (reward.coins > 0)
		{
			triangleSprite.spriteName = "daily_bonus_day1";
			frameSprite.spriteName = "daily_bonus_bg_b";
			glowBg.spriteName = "daily_bonus_bg_b_glow";
			collectLabel.color = yellowColor;
			amountLabel.text = reward.coins + "";

			if (reward.coins <= 1999)
				contentSprite.spriteName = "daily_bonus_coin_1";
			else if (reward.coins >= 2000 && reward.coins <= 2999)
				contentSprite.spriteName = "daily_bonus_coin_2";
			else if (reward.coins >= 3000 && reward.coins <= 3999)
				contentSprite.spriteName = "daily_bonus_coin_3";
			else if (reward.coins >= 4000)
				contentSprite.spriteName = "daily_bonus_coin_4";

		}

		contentSprite.MakePixelPerfect();


		if (day == playerDay)
		{
			// is today
			triangleSprite.spriteName = "daily_bonus_today";
			dayLabel.Text = "TODAY";
			collectLabel.gameObject.SetActive(true);
		}
		else
		{
			// is not today



			collectLabel.gameObject.SetActive(false);
			//GetComponent<BoxCollider>().size = Vector3.zero;
			dayLabel.Text = "Day " + day;
			GetComponent<UIButtonScale>().pressed = Vector3.one;
			GetComponent<UIButtonSound>().audioClip = null;

			posTweener.mode = PositionTweener.TWEENMODE.PASSIVE;
			posTweener.Backward();

			if (day < playerDay)
			{

				tick.SetActive(true);
				ScaleTweener st = tick.GetComponent<ScaleTweener>();
				st.speed = 10000f;
				st.setProportion(1f);
				st.Forward();
			}
		}
	}

	public void BoxClicked()
	{
		if (clicked) return;

		clicked = true;

		if (day != playerDay) return;

		StartCoroutine(BoxClickedRoutine());
	}

	IEnumerator BoxClickedRoutine()
	{
		posTweener.mode = PositionTweener.TWEENMODE.PASSIVE;
		posTweener.setProportion(0f);
		posTweener.Backward();

		collectLabel.gameObject.SetActive(false);

		glowBg.gameObject.SetActive(true);
		tick.SetActive(true);
		tick.GetComponent<ScaleTweener>().Forward();




		yield return new WaitForSeconds(0.2f);

		glowBg.gameObject.SetActive(true);
		SoundManager.SharedManager.PlaySFX("stamp");

		if (reward.gems > 0)
			Globals.mainPlayer.GainGems(reward.gems);
		else if (reward.coins > 0)
			Globals.mainPlayer.GainCoins(reward.coins);

		yield return new WaitForSeconds(0.75f);

		transform.parent.GetComponent<DailyBonusPopup>().canClose = true;

		GetComponent<UIButtonScale>().pressed = Vector3.one;
		GetComponent<UIButtonSound>().audioClip = null;

		DateTime serverTime = DateTime.Parse(Globals.currentTimeStr);
		Globals.mainPlayer.DailyBonusStamp = serverTime;
		Globals.mainPlayer.DailyBonusDay = (Globals.mainPlayer.DailyBonusDay+1) % DailyBonus.cycleLength;
		Globals.mainPlayer.Save();

		yield return null;
	}

}


using UnityEngine;
using System.Collections;

public class RainbowText : MonoBehaviour {

	UILabel lb;
	UISysFontLabel slb;
	float mV = 0;
	
	// Use this for initialization
	void Start () {
		lb = transform.GetComponent<UILabel>();
		slb = transform.GetComponent<UISysFontLabel>();
	}
	
	// Update is called once per frame
	void Update () {
		
		mV += 0.2f;
		
		if (mV > 360.0f) mV = 0.0f;
		
		if (lb != null)
			lb.color = ColorFromHSV(mV, 1.0f, 1.0f, 1.0f);	
		if (slb != null)
			slb.color = ColorFromHSV(mV, 1.0f, 1.0f, 1.0f);	
	}
	
	public static Color ColorFromHSV(float h, float s, float v, float a = 1)
	{
	// no saturation, we can return the value across the board (grayscale)
        if (s == 0)
            return new Color(v, v, v, a);

        // which chunk of the rainbow are we in?
        float sector = h / 60;

        // split across the decimal (ie 3.87 into 3 and 0.87)
        int i = (int)sector;
        float f = sector - i;

        float p = v * (1 - s);
        float q = v * (1 - s * f);
        float t = v * (1 - s * (1 - f));

        // build our rgb color
        Color color = new Color(0, 0, 0, a);

        switch(i)
        {
            case 0:
                color.r = v;
                color.g = t;
                color.b = p;
                break;

            case 1:
                color.r = q;
                color.g = v;
                color.b = p;
                break;

            case 2:
                color.r  = p;
                color.g  = v;
                color.b  = t;
                break;

            case 3:
                color.r  = p;
                color.g  = q;
                color.b  = v;
                break;

            case 4:
                color.r  = t;
                color.g  = p;
                color.b  = v;
                break;

            default:
                color.r  = v;
                color.g  = p;
                color.b  = q;
                break;
        }

        return color;
}
}


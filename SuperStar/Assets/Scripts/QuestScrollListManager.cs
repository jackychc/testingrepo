using UnityEngine;
using System.Collections;

public class QuestScrollListManager : MonoBehaviour
{
	
	//QuestItem[]  quests;
	float xPos;
	UICenterOnChild itemCenter;

	Transform cachedTrans;
	public GameObject leftArrow,rightArrow;
	float rightThreshold = -845f;
	
	// Use this for initialization
	void Start ()
	{
		//LoadQuests();
		cachedTrans = transform;
	}	
	
	public void LoadQuests()
	{
		// create some questItems
		int childs = transform.childCount;

        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
		

		//SpringPanel.Begin(transform.gameObject, new Vector3(0f,0f,0f), 10f);
		
		xPos = 430f;
		
		for (int i =0 ; i < Quests.currentQuests.Count; i++)
		{
			GameObject go = (GameObject) Instantiate(Resources.Load ("QuestItem") as GameObject);
			
			go.name = "QuestItem"+(i+1);
			go.transform.parent = transform;
			
			go.GetComponent<QuestItem>().quest = Quests.currentQuests[i];
			go.GetComponent<QuestItem>().quest.ContinueQuest();
			go.GetComponent<QuestItem>().Refresh();
			
			go.transform.localPosition = new Vector3(430f*i,0f,0f);
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = Vector3.one;
			
			xPos -= 430f;
		}

		rightThreshold = -430f * (Quests.currentQuests.Count-1) + 20f;


		if (!Globals.mainPlayer.ScrolledQuest)
		{
			Globals.mainPlayer.ScrolledQuest = true;
			StartCoroutine(DelayedStart());
		}
	}

	IEnumerator DelayedStart()
	{
		yield return new WaitForSeconds(0.5f);

		SpringPanel.Begin(transform.gameObject, new Vector3(-430f * (Quests.currentQuests.Count-1), 0f, 0f), 2.5f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (cachedTrans.localPosition.x > -20f)
		{
			if (leftArrow.activeSelf)
				leftArrow.SetActive(false);
		}
		else if (!leftArrow.activeSelf)
			leftArrow.SetActive(true);

		if (cachedTrans.localPosition.x < rightThreshold)
		{
			if (rightArrow.activeSelf)
				rightArrow.SetActive(false);
		}
		else
			if (!rightArrow.activeSelf)
				rightArrow.SetActive(true);

	}
	
	void LeftPressed()
	{
		//SpringPanel.Begin(transform.gameObject, new Vector3(0f,0f,0f), 10f);
		relativeItem(-1);
	}
	
	void RightPressed()
	{
		//SpringPanel.Begin(transform.gameObject, new Vector3(0f,0f,0f), 10f);
		relativeItem(1);
	}
	
	public void relativeItem(int position)
	{
		itemCenter = transform.GetComponent<UICenterOnChild>();
		itemCenter.Recenter();
		
		float springAmount = (-itemCenter.centeredObject.transform.localPosition.x) - (430f* position);
		
		springAmount = Mathf.Max(Mathf.Min(0f, springAmount), xPos);
		SpringPanel.Begin(transform.gameObject, new Vector3(springAmount, 0f,0f), 5f);
		 
	}
}


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RadarChartDrawer : MonoBehaviour
{
	Color[] occupationColor;
	Vector3[] nodePositions;
	float[] proportion;
	
	//int[] sort = {2,1,4,3,0};
	
	void Start()
	{
	}
	Color colorRGBA(int r, int g, int b, int a)
	{
		return new Color(r/255f, g/255f, b/255f, a/255f);
	}
	
	int findMax()
	{
		int index = 0;
		float max = 0f;
		for (int i = 0; i < 5; i++)
		{
			if (Globals.mainPlayer.getSkill().skill[i] > max)
			{
				max = Globals.mainPlayer.getSkill().skill[i];
				index = i;
			}
		}
		
		return index;
	}
	
    public void updatePolygon()
    {
        occupationColor = new Color[]{colorRGBA(247,143,0,255),									
									colorRGBA(125,62,238,255),
									colorRGBA(239,50,143,255),
									colorRGBA(38,232,50,255),
									colorRGBA(0,205,255,255)
									};
		nodePositions = new Vector3[5];
		
		proportion = new float[5];
		
		for (int i = 0; i < 5; i++)
		{
			proportion[i] = Mathf.Min(Globals.mainPlayer.getSkill().skill[i] / 100f, 1f);
			
		
			
			nodePositions[i] = new Vector3(proportion[i]*  Mathf.Cos( -0.4f*Mathf.PI * i + Mathf.PI/2f),
											proportion[i] * Mathf.Sin( -0.4f*Mathf.PI * i + Mathf.PI/2f),
											0f);
		}
		
        transform.gameObject.GetComponent<MeshFilter>().mesh.Clear();
 
        
        Mesh mesh = new Mesh();
       
        //Components
        
        
       
        //Create mesh
        mesh = CreateMesh(0);
       
        //Assign materials
        //MR.material = myMaterial;
       
        //Assign mesh to game object
        transform.gameObject.GetComponent<MeshFilter>().mesh = mesh;
			
		// Assign color
		Material[] tempMArray = new Material[1];
		Material tempM = transform.gameObject.GetComponent<MeshRenderer>().materials[0];
		tempM.color = occupationColor[findMax()];
		tempM.SetColor("_OutlineColor", tempM.color);
		tempMArray[0] = tempM;
		transform.gameObject.GetComponent<MeshRenderer>().materials = tempMArray;
        
    }
	
	Mesh CreateMesh(int num)
    {
        int x;
     
        //Create a new mesh
        Mesh mesh = new Mesh();
       
        //Vertices
        Vector3[] vertex = new Vector3[nodePositions.Length];
       
        for(x = 0; x < nodePositions.Length; x++)
        {
            vertex[x] = nodePositions[x];
        }
       
        //UVs
        Vector2[] uvs = new Vector2[vertex.Length];
       
        for (x = 0; x < vertex.Length; x++)
        {
            if((x%2) == 0)
            {
                uvs[x] = new Vector2(0,0);
            }
            else
            {
                uvs[x] = new Vector2(1,1);
            }
        }
       
        //Triangles
        int[] tris = new int[3 * (vertex.Length - 2)];    //3 verts per triangle * num triangles
        int C1,C2,C3;
       
        if(num == 0)
        {
            C1 = 0;
            C2 = 1;
            C3 = 2;
           
            for(x = 0; x < tris.Length; x+=3)
            {
                tris[x] = C1;
                tris[x+1] = C2;
                tris[x+2] = C3;
               
                C2++;
                C3++;
            }
        }
        else
        {
            C1 = 0;
            C2 = vertex.Length - 1;
            C3 = vertex.Length - 2;
           
            for(x = 0; x < tris.Length; x+=3)
            {
                tris[x] = C1;
                tris[x+1] = C2;
                tris[x+2] = C3;
               
                C2--;
                C3--;
            }   
        }
       
        //Assign data to mesh
        mesh.vertices = vertex;
        mesh.uv = uvs;
        mesh.triangles = tris;
       
        //Recalculations
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();   
        mesh.Optimize();
       
        //Name the mesh
        mesh.name = "MyMesh";
       
        //Return the mesh
        return mesh;
    }
}

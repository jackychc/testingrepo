using UnityEngine;
using System.Collections;

public class TigerSlotLampController : MonoBehaviour {
	public GameObject lampControlObj;
	public GameObject currentLampObj;
	
	public int numOfLamps = 42;
	public int currLampIndex = 1;
	
	public float rotatingSpeed = 0.01f;
	public float getPriceSpeed = 0.2f;

	private bool switchOnLampEffect = false;
	private bool switchOnGetPrizeEffect = false;
	
	// Use this for initialization
	void Start () {
		if (currentLampObj == null)
		{
			currentLampObj = lampControlObj.transform.FindChild("Lamp1").gameObject;
			currLampIndex = 1;
		}
		
		//StartCoroutine(TurnGetPriceLamp());
	}
	
	// Update is called once per frame
	void Update () {
		//if (!switchingLamp && switchOnLampEffect)
			//StartCoroutine(TurnOnOffLamp());
	}
	
	private IEnumerator TurnGetPriceLamp()
	{
		bool isOn = false;
		while(switchOnGetPrizeEffect)
		{
			for (int i = 1; i <= numOfLamps; i++)
			{
				GameObject lampObj = lampControlObj.transform.FindChild("Lamp" + i).gameObject;
				if (lampObj != null)
				{
					UISprite lampObjSprite = lampObj.GetComponent<UISprite>();
					
					if (isOn)
					{
						lampObjSprite.spriteName = "title_board_glowless";
					}
					else
					{
						lampObjSprite.spriteName = "title_board_glow";
					}
					lampObjSprite.MakePixelPerfect();
				}
			}		
			
			isOn = !isOn;
			
			yield return new WaitForSeconds(getPriceSpeed);
		}		
	}
	
	private IEnumerator TurnOnOffLamp()
	{
		while(switchOnLampEffect)
		{
			if (currentLampObj != null)
			{
				currentLampObj.GetComponent<UISprite>().spriteName = "title_board_glowless";
				currentLampObj.GetComponent<UISprite>().MakePixelPerfect();
				//currentLampObj.SetActive(false);
				
				currLampIndex = ((currLampIndex + 1) > numOfLamps)? 1: currLampIndex + 1;			
				currentLampObj = lampControlObj.transform.FindChild("Lamp" + currLampIndex).gameObject;
				if (currentLampObj != null)
				{
					currentLampObj.GetComponent<UISprite>().spriteName = "title_board_glow";
					currentLampObj.GetComponent<UISprite>().MakePixelPerfect();
				}
			}
			
			yield return new WaitForSeconds(rotatingSpeed);
		}
	}
	
	private void SwitchOffAllEffect()
	{
		switchOnLampEffect = false;
		switchOnGetPrizeEffect = false;
		StopCoroutine("TurnOnOffLamp");
		StopCoroutine("TurnGetPriceLamp");
		
		//Switch off the all the lamp
		for (int i = 1; i <= numOfLamps; i++)
		{
			GameObject lampObj = lampControlObj.transform.FindChild("Lamp" + i).gameObject;
			if (lampObj != null)
			{
				UISprite lampObjSprite = lampObj.GetComponent<UISprite>();
				lampObjSprite.spriteName = "title_board_glowless";
				lampObjSprite.MakePixelPerfect();
			}
		}
	}
	
	public bool IsSwitchOnLampEffect()
	{
		return switchOnLampEffect;
	}
	
	public bool IsSwitchOnGetPrizeEffect()
	{
		return switchOnGetPrizeEffect;
	}
	
	public void SwitchLampEffect(bool turnOn)
	{
		SwitchOffAllEffect();
		switchOnLampEffect = turnOn;
		
		if (switchOnLampEffect)
		{
			StartCoroutine(TurnOnOffLamp());
		}	
	}
	
	public void SwitchGetPrizeLampEffect(bool turnOn)
	{
		SwitchOffAllEffect();
		switchOnGetPrizeEffect = turnOn;
		
		if (switchOnGetPrizeEffect)
		{
			StartCoroutine(TurnGetPriceLamp());
		}		
	}
}

using UnityEngine;
using System.Collections;

public class MessageItem : MonoBehaviour {
	
	public GameObject friendNameLabel;
	public GameObject msgLabel;
	public GameObject tick;
	public GameObject getVoteIcon;
	public GameObject sendVoteIcon;
	public GameObject spIcon;
	public UISprite bg;
	
	public Message message;
	public string friendName;
	public bool selected;
	
	
	public int type = 0;
	
	// Use this for initialization
	void Start ()
	{
		//a very important line of code
		Destroy(transform.GetComponent<UIPanel>());
		
		friendNameLabel.GetComponent<UISysFontLabel>().Text = friendName;
	
	}
	
	public void SetType(int t)
	{
		type = t;
		
		// the sprite name is swapped
		
		if (type == 0)
		{
			msgLabel.GetComponent<UISysFontLabel>().Text = Language.Get("vote_askingforyourvote");
			bg.spriteName = "msg_roll_getVote";  // purple
			getVoteIcon.SetActive(false);
			sendVoteIcon.SetActive(true);
		}
		if (type == 1)
		{
			msgLabel.GetComponent<UISysFontLabel>().Text = Language.Get("vote_gotavote");
			bg.spriteName = "msg_roll_giveVote"; // yellow
		}
		
	}

	
	public void Toggle()
	{
		selected = !selected;
		
		tick.SetActive(selected);		
	}
	
	public void setTrue()
	{
		selected =true;
		tick.SetActive(true);
	}
	
}

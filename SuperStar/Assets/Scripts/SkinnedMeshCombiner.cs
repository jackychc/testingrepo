using UnityEngine;
using System.Collections.Generic;
using System.Collections;
 
public class SkinnedMeshCombiner : MonoBehaviour {
 
    void Start()
	{
	}
	
	public void Combine()
	{
		StartCoroutine(CombineStage(1));
		//CombineStage(2);
	}
	
	public IEnumerator CombineStage(int stage)
	{
		yield return null;
		
		Texture2D shade = null;
		Color color = new Color(150f/255f,150f/255f,150f/255f,1f);
		
        SkinnedMeshRenderer[] tempRenderers= GetComponentsInChildren<SkinnedMeshRenderer>();
		List<SkinnedMeshRenderer> tempList = new List<SkinnedMeshRenderer>();
		
		
		if (stage == 1)
		{
			foreach(SkinnedMeshRenderer smr in tempRenderers)
			{
				if (smr.sharedMesh != null
					&& !smr.gameObject.name.Equals("body")
					&& !smr.gameObject.name.Equals("FRONTHAIR")
					&& !smr.gameObject.name.Equals("BACKHAIR")
					&& !smr.gameObject.name.Contains("ZCOMMON"))
				tempList.Add(smr);
			}
		}
		else if (stage == 2)
		{
			foreach(SkinnedMeshRenderer smr in tempRenderers)
			{
				if (!smr.gameObject.name.Contains("ZCOMMON")
					&& (smr.gameObject.name.Equals("FRONTHAIR")
					|| smr.gameObject.name.Equals("BACKHAIR")))
				tempList.Add(smr);
			}
		}
			
		
		
		SkinnedMeshRenderer[] smRenderers = tempList.ToArray();
		
		
		//if (stage == 1)
		shade = (Texture2D) smRenderers[0].sharedMaterial.GetTexture("_MatCap");
		color = smRenderers[0].sharedMaterial.color;
		
        List<Transform> bones = new List<Transform>();        
        List<BoneWeight> boneWeights = new List<BoneWeight>();        
        List<CombineInstance> combineInstances = new List<CombineInstance>();
        List<Texture2D> textures = new List<Texture2D>();
		List<Texture2D> shades = new List<Texture2D>();
        int numSubs = 0;
 
        foreach(SkinnedMeshRenderer smr in smRenderers)
		{
			if (smr.sharedMesh == null) Log.Warning("is null lor!");
			
           	 numSubs += smr.sharedMesh.subMeshCount;
		}
 
        int[] meshIndex = new int[numSubs];
        int boneOffset = 0;
        for( int s = 0; s < smRenderers.Length; s++ ) 
		{
            SkinnedMeshRenderer smr = smRenderers[s];  
			
            BoneWeight[] meshBoneweight = smr.sharedMesh.boneWeights;
 
            // May want to modify this if the renderer shares bones as unnecessary bones will get added.
            foreach( BoneWeight bw in meshBoneweight )
			{
                BoneWeight bWeight = bw;
 
                bWeight.boneIndex0 += boneOffset;
                bWeight.boneIndex1 += boneOffset;
                bWeight.boneIndex2 += boneOffset;
                bWeight.boneIndex3 += boneOffset;                
 
                boneWeights.Add( bWeight );
            }
            boneOffset += smr.bones.Length;
 
            Transform[] meshBones = smr.bones;
            foreach( Transform bone in meshBones )
                bones.Add( bone );
 
            if( smr.material.mainTexture != null )
                textures.Add( smr.renderer.material.mainTexture as Texture2D );
			
			// if( smr.material.GetTexture("_ToonShade") != null )
            //    shades.Add( smr.renderer.material.GetTexture("_ToonShade") as Texture2D );
			
 
            CombineInstance ci = new CombineInstance();
            ci.mesh = smr.sharedMesh;
            meshIndex[s] = ci.mesh.vertexCount;
            ci.transform = smr.transform.localToWorldMatrix;
            combineInstances.Add( ci );
 
            Object.Destroy( smr.gameObject );
        }
 
        //List<Matrix4x4> bindposes = new List<Matrix4x4>();
 
       // for( int b = 0; b < bones.Count; b++ ) {
       //     bindposes.Add( bones[b].worldToLocalMatrix * transform.localToWorldMatrix );
       // }
		
		Destroy(transform.FindChild("ZCOMMON"+stage).gameObject);
 		GameObject zcommon = new GameObject();
		zcommon.name = "ZCOMMON"+stage;
		zcommon.transform.parent = transform;
		zcommon.transform.localPosition = Vector3.zero;
		zcommon.transform.localRotation = Quaternion.identity;
		zcommon.transform.localScale = Vector3.one;
		
        SkinnedMeshRenderer r = zcommon.AddComponent<SkinnedMeshRenderer>();
        r.sharedMesh = new Mesh();
        r.sharedMesh.CombineMeshes( combineInstances.ToArray(), true, true );
		r.sharedMesh.name = "Zcommon"+stage;
 
        Texture2D skinnedMeshAtlas = new Texture2D(4096,4096);
		//Texture2D shadesAtlas = new Texture2D( 512, 512);
		
        Rect[] packingResult = skinnedMeshAtlas.PackTextures( textures.ToArray(), 0 );
		//shadesAtlas.PackTextures( shades.ToArray(), 0 );
        Vector2[] originalUVs = r.sharedMesh.uv;
        Vector2[] atlasUVs = new Vector2[originalUVs.Length];
 
        int rectIndex = 0;
        int vertTracker = 0;
        for( int i = 0; i < atlasUVs.Length; i++ ) {
			
			//Log.Debug("atlasUV.melgth:"+atlasUVs.Length+",packingResult.length="+packingResult.Length+",i="+i);
			
            atlasUVs[i].x = Mathf.Lerp( packingResult[rectIndex].xMin, packingResult[rectIndex].xMax, originalUVs[i].x );
            atlasUVs[i].y = Mathf.Lerp( packingResult[rectIndex].yMin, packingResult[rectIndex].yMax, originalUVs[i].y );            
 
            if( i >= meshIndex[rectIndex] + vertTracker ) {                
                vertTracker += meshIndex[rectIndex];
                rectIndex++;                
            }
        }
 
        Material combinedMat = new Material( Shader.Find( "MatCap/Detail" ) );
		
        combinedMat.mainTexture = skinnedMeshAtlas;
		combinedMat.color = color;
		combinedMat.SetTexture("_MatCap", shade);
//		combinedMat.SetTexture("_ToonShade", shadesAtlas); //!!!
        r.sharedMesh.uv = atlasUVs;
        r.sharedMaterial = combinedMat;
		
		
		Log.Debug("r.sharedMaterial:"+r.sharedMaterial.name);
 
        r.bones = bones.ToArray();
        r.sharedMesh.boneWeights = boneWeights.ToArray();
       // r.sharedMesh.bindposes = bindposes.ToArray();
        r.sharedMesh.RecalculateBounds();
    }
}
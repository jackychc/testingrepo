using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ShopScrollListManager : MonoBehaviour {
	
	public GameObject scrollPanelC;
	public GameObject scrollPanelT;
	
	UICenterOnChild categoryCenter, itemCenter;
	
	public UIPanelAlphaTweener itemNamePanel;
	public UILabel itemNameLabel;
	
	public HairColorScrollListManager hcsl;
	public PositionTweener hairColorPanel;
	
	public PositionTweener alphaPanel;
	public AlphaSetter alphaSetter;
	
	public PositionTweener bgPosTweener, bkPosTweener;
	
	public Transform tDown, tUp, tBG;
	
	List<ItemCategory> categoryList = new List<ItemCategory>();
	
	List<Item> wearingCache = null;
	Item itemCache = null;
	
	string categoryName = "";
	GameObject girl;
	bool isZoomingIn;
	
	
	int previousSort = 0;
	float yPos = 0f;
	
	bool cancelHide;
	
	Feature shopFeature = null;
	
	
	// Use this for initialization
	void Start () {
		StartCoroutine(DelayedStart());
	}
	
	IEnumerator DelayedStart()
	{
		yield return new WaitForEndOfFrame();
		girl = GameObject.Find("FEMALE");
		
		categoryCenter = scrollPanelC.GetComponent<UICenterOnChild>();
		itemCenter = scrollPanelT.GetComponent<UICenterOnChild>();
		scrollPanelC.GetComponent<UIDraggablePanel>().onDragFinished += DragCategoryFinished;
		scrollPanelT.GetComponent<UIDraggablePanel>().onDragFinished += DragItemFinished;
		
		AdjustForHigherScreens();
		
		GetShopFeatureId();
		LoadCategories();
		LoadItems();
		
		GameObject.Find("IndexBlock").GetComponent<IndexBlock>().target = ShopScene.fakePlayer;
		
		scrollPanelC.GetComponent<ClothCatagoryHighlighter>().SendMessage("OnShopScrollListInitialized", true);
	}
	
	public void GetShopFeatureId()
	{
		//VisitShop.shop.shopId;
		// ....
		shopFeature = Features.GetFeatureOfShop(VisitShop.shop);
		//Log.Debug("Shop Feature is: " + shopFeature.featureName);
	}
	
	
	public void LoadCategories()
	{		
		foreach (ItemCategory category in ItemCategories.allCategories)
		{
			foreach (Item item in VisitShop.shop.items)
			{				
				if (category.listOfSubcategories.Contains(ItemSubcategories.getSubcategory(item.subCategoryId)))
				{
					if (!categoryList.Contains(category))
					{
						categoryList.Add(category);
						break;
					}
				}
			}
		}
		
		for (int i = 0; i < categoryList.Count; i++)
		{
			GameObject go = (GameObject) Instantiate(Resources.Load ("CategoryScrollListItem") as GameObject);
			
				go.name = i + " - " + categoryList[i].categoryName;
				go.transform.parent = scrollPanelC.transform;
				
				go.GetComponent<CategoryScrollListItem>().category = categoryList[i];
				
				go.transform.localPosition = new Vector3(0f, i * -99f,0f);
				go.transform.localRotation = Quaternion.identity;
				go.transform.localScale = new Vector3(74f,73f, 1f);
			
			Log.Debug("category["+i+"] : "+ categoryList[i].categoryName);
		}	
	}

	
	public void LoadItems()
	{
		if (categoryList == null || categoryList.Count == 0)
			return;
		
		ItemCategory category;
		if (categoryName.Equals(""))
		{
			category = categoryList[0];
			categoryName = category.categoryName;
		}
		else
		{
			category = categoryCenter.centeredObject.GetComponent<CategoryScrollListItem>().category;
			categoryName = categoryCenter.centeredObject.name; //!
		}
		
		// for previous category, if player did't own the item, restore to cache
		if (itemCache != null)
		{
			ItemSubcategory cacheSubcat = ItemSubcategories.getSubcategory(itemCache.subCategoryId);
			
			if (!Globals.mainPlayer.owningItems.Contains(ShopScene.fakePlayer.wearingItemOfSubcategory(cacheSubcat)))
			{
				//ShopScene.fakePlayer.wearItem(itemCache);
				ShopScene.fakePlayer.wearingItems = wearingCache;
				girl.GetComponent<CharacterManager>().GenerateCharacter(ShopScene.fakePlayer);
			}
		}
		
		// assign new cache
		List<ItemSubcategory> t = ItemCategories.getListOfSubcategoriesByCategory(category.categoryId);
		itemCache = Globals.mainPlayer.wearingItemOfSubcategory(t[0]); //assume the list is not null, assume 1to1 mapping
		wearingCache = ShopScene.fakePlayer.wearingItems;
		
		LoadZoom();
		LoadRotate();
		
//		Log.Debug("loading items! " + categoryName);
		
		Transform itemPanel = scrollPanelT.transform;
		

		// remove previous category items
		
		int childs = itemPanel.childCount;

        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(itemPanel.GetChild(i).gameObject);
        }
		
		// add this category items
		
		List<ItemSubcategory> subcats = ItemCategories.getListOfSubcategoriesByCategory(category.categoryId);
	
		yPos = 0f;
		
		// sort by level......
		List<Item> sieveA = new List<Item>();
		List<Item> sieveB = new List<Item>();
		List<Item> sieve = new List<Item>();
		
		// loop through all player items, if contains the subcat, add!
		for (int i =0 ; i < VisitShop.shop.items.Count; i++)
		{
			Item item = VisitShop.shop.items[i];
			
			if (Globals.mainPlayer.owningItems.Contains(item)) continue;
			
			if (subcats.Contains(ItemSubcategories.getSubcategory(item.subCategoryId)))
			{
				if (Globals.mainPlayer.Level >= item.requiredLevel)
					sieveA.Add(item);
				else
					sieveB.Add(item);
				//sieve.Add(item);
			}
		}
		
		sieveA = sortUnlocked(sieveA);
		sieveB = sortLocked(sieveB);
		
		foreach (Item i in sieveA) sieve.Add(i);
		foreach (Item i in sieveB) sieve.Add(i);
		//sieve = sortLocked(sieve);
		
		
		for (int i =0 ; i < sieve.Count; i++)
		{
			Item item = sieve[i];
			
			//if (Globals.mainPlayer.owningItems.Contains(item)) continue;
			
			//if (subcats.Contains(ItemSubcategories.getSubcategory(item.subCategoryId)))
			{
			
				GameObject go = (GameObject) Instantiate(Resources.Load ("ShopItem") as GameObject);
			
				go.name = item.itemName;
				go.transform.parent = itemPanel;
				
				go.GetComponent<ShopItem>().item = item;
				go.GetComponent<ShopItem>().sortOrder = i+1;
				go.GetComponent<ShopItem>().AssignFindings(this, scrollPanelT, girl);
				go.transform.localPosition = new Vector3(0, yPos,0f);
				go.transform.localRotation = Quaternion.identity;
				go.transform.localScale = new Vector3(1f,1f, 1f);
				
				go.GetComponent<ShopItem>().offerCoins = item.price;
				go.GetComponent<ShopItem>().offerGems = item.gems;
				
				Feature f = null;
				// do the feature stuff
				
				Feature subcatFeature 	= Features.GetFeatureOfShopCat(VisitShop.shop, ItemSubcategories.getSubcategory(item.subCategoryId));
				Feature itemFeature		= Features.GetFeatureOfItem(item);
				
				if (itemFeature != null)
				{
					f = itemFeature;	
				}
				else if (subcatFeature != null)
				{
					f = subcatFeature;
				}
				else if (shopFeature != null)
				{
					f = shopFeature;	
				}
				
				if (f != null)
				{
					if (f.discount > 0)
					{
						go.GetComponent<ShopItem>().discount = f.discount;
						go.GetComponent<ShopItem>().offerCoins = (int) (item.price * ((100-f.discount)/100f));
						go.GetComponent<ShopItem>().offerGems = (int) (item.gems * ((100-f.discount)/100f));
					}
					else if (f.gems > 0)
					{
						go.GetComponent<ShopItem>().offerGems = f.gems;
					}
					else if (f.coins > 0)
					{
						go.GetComponent<ShopItem>().offerCoins = f.coins;
					}
				}

				if (i != VisitShop.shop.items.Count)
					yPos -= 214f;
				
				// TODO : load item icon
				//go.GetComponent<UISprite>().spriteName = "closetitem" + UnityEngine.Random.Range(1,4);
			}
		}
		
		//itemPanel.GetComponent<UIGrid>().repositionNow = true;

		//also a dlc button
		/*DownloadManager dlm = DownloadManager.SharedManager;		

		if (!dlm.isDownloading)
		{

			if (!Globals.IsDLCCompleted)
			{
				if (dlm.haveDLC)
				{
					GameObject dlcButton = (GameObject) Instantiate(Resources.Load ("ClosetDLC") as GameObject);	
					dlcButton.name = "ClosetZDLC";
					dlcButton.transform.parent = itemPanel;
					Destroy(dlcButton.GetComponent<UIPanel>());
					dlcButton.transform.localPosition = new Vector3(0, yPos,0f);
					dlcButton.transform.localRotation = Quaternion.identity;
					dlcButton.transform.localScale = new Vector3(1f,1f, 1f);

					yPos -= 214f;
				}
			}
		}*/
		
		refreshTicks();
		LoadPanels();
		
		StartCoroutine(DelayedUnload());
		
	}
	
	IEnumerator DelayedUnload()
	{
		yield return new WaitForSeconds(1.0f);
		Resources.UnloadUnusedAssets();
	}
	
	public bool isZooming()
	{
		return isZoomingIn;
	}
	
	public void LoadZoom()
	{
		if (categoryCenter.centeredObject == null) return;
		
		ItemCategory category = categoryCenter.centeredObject.GetComponent<CategoryScrollListItem>().category;
		// testing zoom in
		
		
		if (category.willZoom)
		{
			zoomIn();
			
		}
		
		else
		{
			zoomOut();
		}
	}
	
	public void LoadRotate()
	{
	}
	
	public void LoadPanels()
	{
		LoadColorPanel();
		LoadAlphaPanel();
	}
	
	void LoadColorPanel()
	{
		Log.Warning("category NAME?"+categoryName);
		
		if (categoryName.Contains("Front Hair") ||
			categoryName.Contains("Back Hair") ||
			categoryName.Contains("Lips") ||
			categoryName.Contains("Eye Makeup")
			) 
		{
			hairColorPanel.Forward();
			
			ItemSubcategory targetSubcat = null;
			
			if (categoryName.Contains("Front Hair") ||
				categoryName.Contains("Back Hair"))
					targetSubcat = ItemSubcategories.getSubcategory("HAIRCOLOR");
				
			if (categoryName.Contains("Lips"))
					targetSubcat = ItemSubcategories.getSubcategory("LIPSCOLOR");
			
			if (categoryName.Contains("Eye Makeup"))
					targetSubcat = ItemSubcategories.getSubcategory("EYESHAPECOLOR");
					
			
			
			hcsl.SetSubcategory(targetSubcat);
		}
		else
		{
			hairColorPanel.Backward();
		}
	}
	
	void LoadAlphaPanel()
	{
		if (
			categoryName.Contains("Rouge") ||
			categoryName.Contains("Eye Makeup")
			) 
		{
			alphaPanel.Forward();
			
			string mode = "";
			
			if (categoryName.Contains("Rouge"))
					mode = "ROUGE";
			
			if (categoryName.Contains("Eye Makeup"))
					mode = "EYESHADOW";
					
			
			// todo: some action here
			alphaSetter.SetMode(mode);
		}
		else
		{
			alphaPanel.Backward();
		}
	}
	
	public void HideAllPanels()
	{
		hairColorPanel.Backward();
		alphaPanel.Backward();
	}
	
	void DragCategoryFinished()
	{
		
		if (categoryName.Equals(categoryCenter.centeredObject.name)) return;
		
		LoadItems();	
		focusToWearingItem();
	}
	void DragItemFinished()
	{

		itemCenter.centeredObject.GetComponent<ShopItem>().ClickItem(true);
		//Log.Debug("!?!?!");
	}
	
	

	public void zoomIn()
	{
		bgPosTweener.Forward();
		bkPosTweener.Forward();
		isZoomingIn = true;
	}
	
	public void zoomOut()
	{
		bgPosTweener.Backward();
		bkPosTweener.Backward();
		isZoomingIn = false;
		
	}
	
	public void goUp()
	{
		Log.Debug("going up");
		relativeItem(-5);
	}
	public void goDown()
	{
		Log.Debug("going down");
		relativeItem(5);
	}
	
	public void relativeItem(int position)
	{
		itemCenter.Recenter(); // get the center

		float springAmount = -itemCenter.centeredObject.transform.localPosition.y + (214f* position);
		Log.Debug("spring? = "+springAmount);
		springAmount = Mathf.Min(Mathf.Max(0f, springAmount), -yPos - 214f);
		Log.Debug("spring after? = "+springAmount);
		SpringPanel.Begin(scrollPanelT, new Vector3(0f,springAmount,0f), 5f);
		 
	}
	
	public void focusToWearingItem()
	{
		
		StartCoroutine("focus");
	}
	
	public IEnumerator focus()
	{
		yield return new WaitForSeconds(0.05f);
		
		float springAmount = 0f;
		float topMostY = Mathf.NegativeInfinity;
		
		// because the items are sorted by name, so we need to determine the actual first element by position 
		foreach (ClosetItem ci in scrollPanelT.GetComponentsInChildren<ClosetItem>())
		{
			//Log.Debug("checking item: "+ci.item.itemName);
			
			if (Globals.mainPlayer.isWearingItem(ci.item))
			{
				if (ci.transform.localPosition.y > topMostY)
				{
					topMostY = ci.transform.localPosition.y;
					springAmount = -ci.transform.localPosition.y;
				}
				
				
			}
		}
		
		Log.Debug("sp amount: "+springAmount);
		
		SpringPanel.Begin(scrollPanelT, new Vector3(0f,springAmount ,0f), 5f);		
	}
	
	public void refreshTicks()
	{
		foreach (ClosetItem ci in scrollPanelT.GetComponentsInChildren<ClosetItem>())
		{
			if (Globals.mainPlayer.isWearingItem(ci.item))
				ci.tick.SetActive(true);
			
			else
				ci.tick.SetActive(false);
		}
		
	}
	
	public void showItemName(Item item)
	{
		StartCoroutine(showItemNameRoutine(item));
	}
	
	IEnumerator showItemNameRoutine(Item item)
	{
#if !UNITY_EDITOR
		if (ItemSubcategories.getSubcategory(item.subCategoryId).shouldShowItemName)
#endif
		{
		
			itemNamePanel.setProportion(0f);
			itemNamePanel.Forward();
			
			yield return new WaitForEndOfFrame();
			itemNameLabel.text = item.itemName;
		}
	}
	
	void AdjustForHigherScreens()
	{
		if (1f*Screen.height / Screen.width > 1.5f)
		{
			int diff = (int) (640* (1f*Screen.height / Screen.width)) - 960;
			
			Vector3 t3 = Vector3.zero;
			Vector4 t4 = Vector4.zero;
			
			
			// Category Part
			t4 = scrollPanelC.GetComponent<UIPanel>().clipRange;
			t4.w = t4.w + diff - 20;
			scrollPanelC.GetComponent<UIPanel>().clipRange = t4;
			
			// Item Part
			
			t4 = scrollPanelT.GetComponent<UIPanel>().clipRange;
			t4.w = t4.w + diff - 20;
			scrollPanelT.GetComponent<UIPanel>().clipRange = t4;
			
			Transform tParent = scrollPanelT.transform.parent;
			
			t3 = tDown.localPosition;
			t3.y -= (diff/2 - 5);
			tDown.localPosition = t3;
			
			t3 = tUp.localPosition;
			t3.y += (diff/2 - 5);
			tUp.localPosition = t3;
			
			t3 = tBG.localScale;
			t3.y += diff;
			tBG.localScale = t3;
			
			
		}
	}
	
	List<Item> sortUnlocked(List<Item> input)
	{
		input.Sort((x, y) => 
			/*x.subCategoryId != y.subCategoryId?
			(
				x.subCategoryId.CompareTo(y.subCategoryId)
			):*/
			x.requiredLevel != y.requiredLevel?
			(
				x.requiredLevel.CompareTo(y.requiredLevel)
			)
			:
			(
				x.price > 0 && y.price == 0?
				(
					-1
				)
				:
				(
					x.price == 0 && y.price > 0?
					(
						1
					)
					:
					(
						// same currency
						x.price > 0?
						(
							x.price.CompareTo(y.price)
						)
						:
						(
							x.gems.CompareTo(y.gems)
						)
					)
				)
				
			)
		);
		
		return input;
	}
	
	List<Item> sortLocked(List<Item> input)
	{
		input.Sort((x, y) => 
			/*x.subCategoryId != y.subCategoryId?
			(
				x.subCategoryId.CompareTo(y.subCategoryId)
			):*/
			(
				//x.requiredLevel != y.requiredLevel?
				(
					x.requiredLevel.CompareTo(y.requiredLevel)
				)
				/*:
				(
					x.price > 0 && y.price == 0?
					(
						-1
					)
					:
					(
						x.price == 0 && y.price > 0?
						(
							1
						)
						:
						(
							// same currency
							x.price > 0?
							(
								x.price.CompareTo(y.price)
							)
							:
							(
								x.gems.CompareTo(y.gems)
							)
						)
					)
				)*/
			)
		);
		
		return input;
	}

}

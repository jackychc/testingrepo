using UnityEngine;
using System.Collections;

public class Quest3: QuestAction
{
	public override bool AppearRequirement()
	{	
		return true;
	}
	
	public override bool FinishRequirement()
	{
		return bool.Parse(QuestFlags.GetFlag(3).strValue);
	}
	
	public override void OnQuestGo()
	{
		//SceneManager.SharedManager.LoadScene(Scenes.CLOSET, true);
		//PopupManager.HideAllPopup();
		//PopupManager.ShowBoyfriendMinimapPopup(false);
		SceneManager.SharedManager.LoadScene(Scenes.BOYFRIENDMAP, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(3, false);
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
	}
}


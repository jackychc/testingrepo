using UnityEngine;
using System.Collections;

public class Quest5: QuestAction
{
	public override bool AppearRequirement()
	{	
		return true;
	}
	
	public override bool FinishRequirement()
	{
		return QuestFlags.GetFlag(8).intValue >= 1;
	}
	
	public override void OnQuestGo()
	{
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAMECLOTHMATCH, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(8,0);
	}
	
	public override void OnQuestStop()
	{
		QuestFlags.SetFlag(8,0);
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(8,0);
	}
}


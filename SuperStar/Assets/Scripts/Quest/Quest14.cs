using UnityEngine;
using System.Collections;

public class Quest14: QuestAction
{
	public override bool AppearRequirement()
	{	
		if (Quests.currentQuests.Contains(Quests.GetQuest(115))) return false;
		if (Quests.currentQuests.Contains(Quests.GetQuest(116))) return false;
		
		return true;
	}
	
	public override bool FinishRequirement()
	{
		return QuestFlags.GetFlag(8).intValue >= 2;
	}
	
	public override void OnQuestGo()
	{	
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAMECLOTHMATCH, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(8,0);
	}
	
	public override void OnQuestStop()
	{
		QuestFlags.SetFlag(8,0);
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(8,0);
	}
	
	public override string GetQuestDescription()
	{
		return Language.Get(quest.description).Replace("%1", "2");
	}
	
	public override string GetQuestProgress()
	{
		return QuestFlags.GetFlag(8).intValue + "/2";
	}
}


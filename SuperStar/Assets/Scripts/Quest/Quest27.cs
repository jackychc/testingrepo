using UnityEngine;
using System.Collections;

public class Quest27: QuestAction
{
	
	public override bool AppearRequirement()
	{	
		if (QuestFlags.GetFlag(10).intValue == 0)
		{
			Vector2 charismaVector = Items.sumCharismaForList(Globals.mainPlayer.wearingItems);
			Boyfriend target = Boyfriends.GetBoyfriendByCriteria(Mathf.Max((int)charismaVector.x , (int)charismaVector.y));
			
			if (target!=null)
			{
				if (target.rewardTimer == -1)
					QuestFlags.SetFlag(10,target.boyfriendId);
				
				
			}
		}	
		
		
		return (QuestFlags.GetFlag(10).intValue != 0);
	}
	
	public override bool FinishRequirement()
	{
		Boyfriend target = Boyfriends.GetBoyfriend(QuestFlags.GetFlag(10).intValue);
		
		return (target.flirtExp >= target.maxFlirtExp);
	}
	
	public override void OnQuestGo()
	{	
		//PopupManager.HideAllPopup();
		//PopupManager.ShowBoyfriendMinimapPopup(false);
		SceneManager.SharedManager.LoadScene(Scenes.BOYFRIENDMAP, true);
	}
	
	public override void OnQuestStart()
	{
	}
	
	public override void OnQuestStop()
	{
		QuestFlags.SetFlag(10,0);
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(10,0);
	}
	
	public override string GetQuestDescription()
	{
		return Language.Get(quest.description).Replace("%1", Boyfriends.GetBoyfriend(QuestFlags.GetFlag(10).intValue).boyfriendName);
	}
}


using UnityEngine;
using System.Collections;

public class Quest2: QuestAction
{
	// done
	
	public override bool AppearRequirement()
	{	
		return true;
	}
	
	public override bool FinishRequirement()
	{
		return bool.Parse(QuestFlags.GetFlag(2).strValue);
	}
	
	public override void OnQuestGo()
	{
		SceneManager.SharedManager.LoadScene(Scenes.JOB, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(2, false);
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
	}
}


using UnityEngine;
using System.Collections;

public class Quest17: QuestAction
{
	// done
	
	public override bool AppearRequirement()
	{
		return Quests.GetQuest(105).completedOnce;
	}
	
	public override bool FinishRequirement()
	{
		return bool.Parse(QuestFlags.GetFlag(17).strValue);
	}
	
	public override void OnQuestGo()
	{
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAMECLOTHMATCH, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(17, false);
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
	}
}


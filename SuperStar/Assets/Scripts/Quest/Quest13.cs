using UnityEngine;
using System.Collections;

public class Quest13: QuestAction
{
	public override bool AppearRequirement()
	{	
		return Friends.friends.Count >= 1;
	}
	
	public override bool FinishRequirement()
	{
		return bool.Parse(QuestFlags.GetFlag(6).strValue);
	}
	
	public override void OnQuestGo()
	{	
		//VisitFriend.friend = Friends.friends[UnityEngine.Random.Range(0,Friends.friends.Count)];
		//SceneManager.SharedManager.LoadScene(Scenes.FRIENDSHOME, true);
		
		//PopupManager.HideAllPopup();
		//PopupManager.ShowFriendListPopup();
		
		SceneManager.SharedManager.LoadScene(Scenes.HOME, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(6, false);
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(6, false);
	}
}


using UnityEngine;
using System.Collections;

public class Quest6: QuestAction
{
	public override bool AppearRequirement()
	{	
		if (Quests.GetQuest(106).completedOnce) return false;
		
		return true;
	}
	
	public override bool FinishRequirement()
	{
		return bool.Parse(QuestFlags.GetFlag(5).strValue);
	}
	
	public override void OnQuestGo()
	{
		PopupManager.HideAllPopup();
		PopupManager.ShowSettingsPopup(false);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(5, false);
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
	}
}


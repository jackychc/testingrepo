using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestFlags
{
	public static List<QuestFlag> listOfFlags = new List<QuestFlag>();
	
	public static List<QuestFlag> initialListOfFlags = new List<QuestFlag>()
	{	
		new QuestFlag(-101, "var-1_tutorialjob", 0, ""),
		new QuestFlag(-102, "var-2_tutorialgift", 0, ""),
		new QuestFlag(-103, "var-3_tutorialfriend", 0, ""),
		new QuestFlag(-104, "var-4_tutorialbf", 0, ""),
		new QuestFlag(-105, "var-5_tutorialindex", 0, ""),
		
		new QuestFlag(1, "var1_changedCloth", 0, "false"),
		new QuestFlag(2, "var2_tennisFirstJob", 0, "false"),
		new QuestFlag(3, "var3_flirtedBf", 0, "false"),
		new QuestFlag(4, "var4_droppedCoin", 0, "false"),
		new QuestFlag(5, "var5_connectedToSocial", 0, "false"),
		new QuestFlag(6, "var6_shootPhoto", 0, "false"),
		new QuestFlag(7, "var7_coinsDropped", 0, ""),
		new QuestFlag(8, "var8_playedRunwayTimes", 0, ""),
		new QuestFlag(9, "var9_sharedPhoto", 0, "false"),
		new QuestFlag(10, "var10_targetBfId", 0, ""),
		new QuestFlag(11, "var11_targetJobId", 0, ""),
		new QuestFlag(12, "var12_targetJobDoneCount", 0, ""),
		new QuestFlag(13, "var13_targetShopId", 0, ""),
		new QuestFlag(14, "var14_targetSubcategoryId", 0, ""),
		new QuestFlag(15, "var15_targetItemDoneCount", 0, ""),
		new QuestFlag(16, "var16_highestChain", 0, ""),
		new QuestFlag(17, "var17_winnedGift", 0, "false"),
		new QuestFlag(18, "var18_friendsIncrement", 0, ""),
		new QuestFlag(19, "var19_broughtCelebrityPiece", 0, "false"),
	};
	
	public static void Save()
	{
		// TODO :REWRITE
		
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;		
		
		foreach (QuestFlag a in listOfFlags)
		{
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM QuestFlags WHERE flagId = '" + a.flagId + "'",
				
				//update
				new List<string>(){
				"UPDATE QuestFlags SET "+
				"  description = '" + a.description +"'" +
				" ,intValue = " + a.intValue  +
				" ,strValue = '" + a.strValue +"'" +
				" WHERE flagId = " + a.flagId
				},
			
				//insert
				new List<string>(){
					"INSERT INTO QuestFlags (flagId, description, intValue, strValue) VALUES( "+
				       + a.flagId + 
				"  , '" + a.description + "'" +
				"  , " + a.intValue +
				"  , '" + a.strValue + "'" +
			    ")"
				}
			);
			
			
			/*SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM QuestFlags WHERE flagId = '" + a.flagId + "'" ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			if (hasRecord)
			{
				// update
				DatabaseManager.SharedManager.Save("UPDATE QuestFlags SET "+
				"  description = '" + a.description +"'" +
				" ,intValue = " + a.intValue  +
				" ,strValue = '" + a.strValue +"'" +
				" WHERE flagId = " + a.flagId);
				
			}
			else
			{
				// insert
				DatabaseManager.SharedManager.Save("INSERT INTO QuestFlags (flagId, description, intValue, strValue) VALUES( "+
				       + a.flagId + 
				"  , '" + a.description + "'" +
				"  , " + a.intValue +
				"  , '" + a.strValue + "'" +
			    ")");
			}*/
		}
	}
	
	public static void Load()
	{
		// TODO :REWRITE
		
		listOfFlags.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM QuestFlags";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
		 	listOfFlags.Add(new QuestFlag(
				qr.GetInteger("flagId"),
				qr.GetString("description"),
				qr.GetInteger("intValue"),
				qr.GetString("strValue")
				));
			
		}
	
		
		qr.Release();
	}
	
	public static QuestFlag GetFlag(int id)
	{
		foreach(QuestFlag f in listOfFlags)
		{
			if (f.flagId == id)
				return f;
		}
		
		return null;
	}
	
	public static void SetFlag(int id, int v)
	{
		foreach(QuestFlag f in listOfFlags)
		{
			if (f.flagId == id)
			{
				f.intValue = v;
			}
		}
	}
	
	public static void SetFlag(int id, string v)
	{
		foreach(QuestFlag f in listOfFlags)
		{
			if (f.flagId == id)
			{
				f.strValue = v;
			}
		}
	}
	
	public static void SetFlag(int id, bool v)
	{
		foreach(QuestFlag f in listOfFlags)
		{
			if (f.flagId == id)
			{
				f.strValue = v.ToString();
			}
		}
	}
	
	
}

public class QuestFlag
{
	public int flagId;
	public string description;
	public int intValue;
	public string strValue;
	
	public QuestFlag(int pId, string pDesc, int pIntValue, string pStringValue)
	{
		this.flagId = pId;
		this.description = pDesc;
		this.intValue = pIntValue;
		this.strValue = pStringValue;
	}
}


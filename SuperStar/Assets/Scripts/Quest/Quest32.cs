using UnityEngine;
using System.Collections;
using System;

public class Quest32: QuestAction
{
	
	public override bool AppearRequirement()
	{	
		return !Boolean.Parse(QuestFlags.GetFlag(19).strValue);
	}
	
	public override bool FinishRequirement()
	{
		return Boolean.Parse(QuestFlags.GetFlag(19).strValue);
	}
	
	public override void OnQuestGo()
	{	
		VisitShop.shop = Shops.GetShopById(2);

		SceneManager.SharedManager.LoadScene(Scenes.SHOP, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(19, false);
	}
	
	public override void OnQuestStop()
	{

	}
	
	public override void OnFinishRequirementMeet()
	{

	}
}


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Quest8: QuestAction
{
	public override bool AppearRequirement()
	{	
		if (Quests.currentQuests.Contains(Quests.GetQuest(107))) return false;
		if (Quests.currentQuests.Contains(Quests.GetQuest(109))) return false;
		
		if (QuestFlags.GetFlag(13).intValue == 0)
		{
			Shop s = Shops.getRandomShop();
			
			if (s != null)
			{
				List<int> poolOfSubcats = new List<int>();
				Dictionary<int,int> subcatToQty = new Dictionary<int, int>();

				foreach (Item item in s.items)
				{
					if (!Globals.mainPlayer.owningItems.Contains(item))
					{
						if (Globals.mainPlayer.Level >= item.requiredLevel)
						{
							if (!poolOfSubcats.Contains(item.subCategoryId))
							{
								if (!subcatToQty.ContainsKey(item.subCategoryId))
									subcatToQty.Add(item.subCategoryId, 1);
								else
									subcatToQty[item.subCategoryId] = subcatToQty[item.subCategoryId]+1;



								if (subcatToQty[item.subCategoryId] >= 2)
									poolOfSubcats.Add(item.subCategoryId);
							}
						}
					}
				}
				
				if (poolOfSubcats.Count > 0)
				{
					QuestFlags.SetFlag(13, s.shopId);
					
					QuestFlags.SetFlag(14, poolOfSubcats[UnityEngine.Random.Range(0, poolOfSubcats.Count)]);
					
					
				}
			}
		}
		
		return QuestFlags.GetFlag(13).intValue != 0;
	}
	
	public override bool FinishRequirement()
	{
		return QuestFlags.GetFlag(15).intValue >= 2;
	}
	
	public override void OnQuestGo()
	{
		VisitShop.shop = Shops.GetShopById(QuestFlags.GetFlag(13).intValue);
		
		SceneManager.SharedManager.LoadScene(Scenes.SHOP, true);
	}
	
	public override void OnQuestStart()
	{
	}
	
	public override void OnQuestStop()
	{
		QuestFlags.SetFlag(13,0);
		QuestFlags.SetFlag(14,0);
		QuestFlags.SetFlag(15,0);
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(13,0);
		QuestFlags.SetFlag(14,0);
		QuestFlags.SetFlag(15,0);
	}
	
	public override string GetQuestDescription()
	{
		return Language.Get(quest.description).Replace("%1", "2")
						.Replace("%2", ItemSubcategories.getSubcategory(QuestFlags.GetFlag(14).intValue).subcategoryDescription)
						.Replace("%3", Shops.GetShopById(QuestFlags.GetFlag(13).intValue).shopName+"");
	}
		
	public override string GetQuestProgress()
	{
		return QuestFlags.GetFlag(15).intValue + "/2";
	}	
}
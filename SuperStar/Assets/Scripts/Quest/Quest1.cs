using UnityEngine;
using System.Collections;

public class Quest1: QuestAction
{
	// done
	
	public override bool AppearRequirement()
	{
		return true;
	}
	
	public override bool FinishRequirement()
	{
		return bool.Parse(QuestFlags.GetFlag(1).strValue);
	}
	
	public override void OnQuestGo()
	{
		SceneManager.SharedManager.LoadScene(Scenes.CLOSET, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(1, false);
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
	}
	
	
}


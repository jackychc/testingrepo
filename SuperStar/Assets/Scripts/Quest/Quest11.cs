using UnityEngine;
using System.Collections;

public class Quest11: QuestAction
{
	public override bool AppearRequirement()
	{	
		return true;
	}
	
	public override bool FinishRequirement()
	{
		return QuestFlags.GetFlag(18).intValue >= 5;
	}
	
	public override void OnQuestGo()
	{
		SceneManager.SharedManager.LoadScene(Scenes.HOME, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(18, 0);
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(18, 0);
	}
	
	public override string GetQuestDescription()
	{
		return Language.Get(quest.description).Replace("%1", "5");
	}
	
	public override string GetQuestProgress()
	{
		return QuestFlags.GetFlag(18).intValue + "/5";
	}
}


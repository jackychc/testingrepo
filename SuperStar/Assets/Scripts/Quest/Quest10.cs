using UnityEngine;
using System.Collections;

public class Quest10: QuestAction
{
	// done
	
	public override bool AppearRequirement()
	{
		return Quests.GetQuest(106).completedOnce;
	}
	
	public override bool FinishRequirement()
	{
		return bool.Parse(QuestFlags.GetFlag(9).strValue);
	}
	
	public override void OnQuestGo()
	{
		SceneManager.SharedManager.LoadScene(Scenes.HOME, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(9, false);
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
	}
}


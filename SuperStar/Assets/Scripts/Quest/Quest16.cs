using UnityEngine;
using System.Collections;

public class Quest16: QuestAction
{
	public override bool AppearRequirement()
	{	
		if (Quests.currentQuests.Contains(Quests.GetQuest(114))) return false;
		if (Quests.currentQuests.Contains(Quests.GetQuest(115))) return false;
		
		return Quests.GetQuest(115).completedOnce;
	}
	
	public override bool FinishRequirement()
	{
		return QuestFlags.GetFlag(8).intValue >= 4;
	}
	
	public override void OnQuestGo()
	{
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAMECLOTHMATCH, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(8,0);
	}
	
	public override void OnQuestStop()
	{
		QuestFlags.SetFlag(8,0);
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(8,0);
	}
	
	public override string GetQuestDescription()
	{
		return Language.Get(quest.description).Replace("%1", "4");
	}
	
	public override string GetQuestProgress()
	{
		return QuestFlags.GetFlag(8).intValue + "/4";
	}
}


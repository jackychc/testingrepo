using UnityEngine;
using System.Collections;

public class Quest18: QuestAction
{
	public override bool AppearRequirement()
	{	
		if (Quests.currentQuests.Contains(Quests.GetQuest(119))) return false;
		if (Quests.currentQuests.Contains(Quests.GetQuest(120))) return false;
		
		return true;
	}
	
	public override bool FinishRequirement()
	{
		return QuestFlags.GetFlag(7).intValue >= 10;
	}
	
	public override void OnQuestGo()
	{	
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAMECOINDROP, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(7,0);
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(7,0);
	}
	
	public override string GetQuestDescription()
	{
		return Language.Get(quest.description).Replace("%1", "10");
	}
	
	public override string GetQuestProgress()
	{
		return QuestFlags.GetFlag(7).intValue + "/10";
	}
}


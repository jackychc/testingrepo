using UnityEngine;
using System.Collections;
using System;

public class Quest25: QuestAction
{
	public override bool AppearRequirement()
	{	
		if (Quests.currentQuests.Contains(Quests.GetQuest(124))) return false;
		if (Quests.currentQuests.Contains(Quests.GetQuest(126))) return false;
		
		if (!Quests.GetQuest(124).completedOnce) return false;
		
		if (QuestFlags.GetFlag(11).intValue == 0)
		{
			
			Job target = Jobs.getRandomJob();
			
			if (target!=null)
			{
				QuestFlags.SetFlag(11, target.activityId);
			}
		}
				
		
		
		
		
		
		return (QuestFlags.GetFlag(11).intValue != 0);
	}
	
	public override bool FinishRequirement()
	{
		return (QuestFlags.GetFlag(12).intValue >= 2);
	}
	
	public override void OnQuestGo()
	{	
		SceneManager.SharedManager.LoadScene(Scenes.JOB, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(12,0);
	}
	
	public override void OnQuestStop()
	{
		QuestFlags.SetFlag(11,0);
		QuestFlags.SetFlag(12,0);
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(11,0);
		QuestFlags.SetFlag(12,0);
	}
	
	public override string GetQuestDescription()
	{
		return Language.Get(quest.description).Replace("%1", getTimeStringBySeconds(Jobs.getJob(QuestFlags.GetFlag(11).intValue).timeRequired))
			.Replace("%2", Language.Get("job_job"+Int32.Parse(Jobs.getJob(QuestFlags.GetFlag(11).intValue).visibleForSkill)))
			.Replace("%3", "2");
	}
	
	public override string GetQuestProgress()
	{
		return QuestFlags.GetFlag(12).intValue + "/2";
	}
	
	public override Reward GetQuestReward ()
	{
		float proportion = 1f;

		Reward dynamicReward = new Reward(999,-1,0,0,0,0,"", new Skill(new int[]{0,0,0,0,0}));
		
		Reward jobReward = Rewards.getReward(Jobs.getJob(QuestFlags.GetFlag(11).intValue).rewardId);
		
		dynamicReward.coins = (int) (jobReward.coins * proportion);
		dynamicReward.gems = (int) (jobReward.gems * proportion);
		dynamicReward.exp = (int) (jobReward.exp * proportion);
		dynamicReward.energy = (int) (jobReward.energy * proportion);
		
		return dynamicReward;
	}
	
	
	
	string getTimeStringBySeconds(int sec)
	{
		if (sec < 3600)
		{
			return ((sec/60) + " " + Language.Get("quest_mins"));
		}
		
		return (sec/3600)+" "+Language.Get("quest_hours");
	}
	
}


using UnityEngine;
using System.Collections;

public class Quest33: QuestAction
{
	
	public override bool AppearRequirement()
	{	
		return Globals.currentDLCVersion == 0;
	}
	
	public override bool FinishRequirement()
	{
		return (Globals.currentDLCVersion > 0);
	}
	
	public override void OnQuestGo()
	{	
		PopupManager.HideAllPopup();

		if (!DownloadManager.SharedManager.isDownloading)
			DownloadManager.SharedManager.AskForDLCUpgrade(false, -1, 1, 2, true);
		else
			DownloadManager.SharedManager.AskForDLCProgress(false, -1, 1);

	}
	
	public override void OnQuestStart()
	{
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
	}

}


using UnityEngine;
using System.Collections;

public class Quest12: QuestAction
{
	public override bool AppearRequirement()
	{	
		return Quests.GetQuest(111).completedOnce;
	}
	
	public override bool FinishRequirement()
	{
		return QuestFlags.GetFlag(18).intValue >= 10;
	}
	
	public override void OnQuestGo()
	{
		SceneManager.SharedManager.LoadScene(Scenes.HOME, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(18, 0);
	}
	
	public override void OnQuestStop()
	{
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(18, 0);
	}
	
	public override string GetQuestDescription()
	{
		return Language.Get(quest.description).Replace("%1", "10");
	}
	
	public override string GetQuestProgress()
	{
		return QuestFlags.GetFlag(18).intValue + "/10";
	}
}


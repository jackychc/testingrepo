using UnityEngine;
using System.Collections;

public class Quest21: QuestAction
{
	public override bool AppearRequirement()
	{	
		if (Quests.currentQuests.Contains(Quests.GetQuest(122))) return false;
		if (Quests.currentQuests.Contains(Quests.GetQuest(123))) return false;
		
		return true;
	}
	
	public override bool FinishRequirement()
	{
		return QuestFlags.GetFlag(16).intValue >= 3;
	}
	
	public override void OnQuestGo()
	{	
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAMECOINDROP, true);
	}
	
	public override void OnQuestStart()
	{
		QuestFlags.SetFlag(16,0);
	}
	
	public override void OnQuestStop()
	{
		QuestFlags.SetFlag(16,0);
	}
	
	public override void OnFinishRequirementMeet()
	{
		QuestFlags.SetFlag(16,0);
	}
	
	public override string GetQuestDescription()
	{
		return Language.Get(quest.description).Replace("%1", "3");
	}
}


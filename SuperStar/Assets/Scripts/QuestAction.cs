using UnityEngine;
using System.Collections;

public abstract class QuestAction
{
	public Quest quest;
	
	public virtual bool AppearRequirement()
	{
		return true;
	}
	public virtual bool FinishRequirement()
	{
		return false;
	}
	
	public virtual void OnQuestGo()
	{
		//Scenes sceneIndex = (Scenes)Enum.Parse(typeof(Scenes), quest.goToSceneName.ToUpperInvariant());
		
		//SceneManager.SharedManager.LoadScene(sceneIndex, true);
	}
	public virtual void OnQuestStart()
	{
	}
	public virtual void OnQuestStop()
	{
	}
	public virtual void OnFinishRequirementMeet()
	{
	}
	
	public virtual string GetQuestDescription()
	{
		return Language.Get(quest.description);
	}
	
	public virtual string GetQuestProgress()
	{
		return "";
	}
	
	public virtual Reward GetQuestReward()
	{
		return Rewards.getReward(quest.rewardId);
	}
	
}


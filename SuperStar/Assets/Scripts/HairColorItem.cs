using UnityEngine;
using System.Collections;

public class HairColorItem : MonoBehaviour {

	public Item item;
	public GameObject tick;
	public int sortOrder;
	public UISprite foreground;
	
	public void Start()
	{
		if (item!=null)
		{
			string[] p = item.meta.Split('_');
			foreground.color =  new Color(
							float.Parse(p[0])/255f,
							float.Parse(p[1])/255f,
							float.Parse(p[2])/255f,
							float.Parse(p[3])/255f);
		
		
			Destroy(transform.GetComponent<UIPanel>());
		}
	}
	
	public void ClickItem()
	{
		//Log.Debug("clicku!");
		
		GameObject girl = GameObject.Find("FEMALE");
		

			// how to wear? ....
			Globals.mainPlayer.wearItem(item);
		
			// just set wearing item id, and call generate character!?
			girl.GetComponent<CharacterManager>().GenerateCharacter(Globals.mainPlayer);
		
		
	}
}

using UnityEngine;
using System;
using System.Collections;

public class JobScene : MonoBehaviour, InputManagerCallback {
	
	public enum DefaultWearingTypes
	{
		NONE		= 0,
		TopBottom 	= 1,
		Dress 		= 2
	}
	
	public GameObject occupationBarObj;
	public GameObject jobScrollListManagerObj;
	public GameObject offerButtonObj;
	
	GameObject newCharacter;
	public GameObject girlAndItem;
	
	public static Player fakePlayer;
	//string defaultItems = "278,319";
	
	string defaultItems 	= "278,319";
	string defaultTop 		= "278";
	string defaultBottom	= "319";
	string defaultDress		= "";
	string defaultShoe		= "339";
	
	public static int currentSkillAnimation = -1, previousSkillAnimation = -1;
	GameObject peripheralItem;
	
	// Use this for initialization
	IEnumerator Start ()
	{		
		fakePlayer = new Player();
		fakePlayer.playerId = -2;
		fakePlayer.wearingItems = Items.stringToItemList(Items.itemListToString(Globals.mainPlayer.wearingItems)); // actually made a copy
		WearDefaultItems();	
		
		newCharacter = Globals.globalCharacter;
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
		yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(fakePlayer));

		JobScrollListManager jobScrollListManager = jobScrollListManagerObj.GetComponent<JobScrollListManager>();
		jobScrollListManager.StartLoadAnimations();

		InputManager.SharedManager.SetCallback(this);
		//SoundManager.SharedManager.ChangeMusic("SuperstarStory_JobCenter");
		if (QuestFlags.GetFlag(-101).intValue == 0)
		{
			QuestFlags.SetFlag(-101, 1);
			PopupManager.ShowTutorialPopup(1, false);
		}
		
		StartCoroutine(DelayedStart());
	}
	
	void OnDestroy()
	{
		InputManager.SharedManager.RemoveCallback(this);
	}
	
	IEnumerator DelayedStart()
	{
		yield return new WaitForSeconds(0.5f);
		
		if (QuestFlags.GetFlag(-101).intValue == 0)
		{
			StartTutorial();
		}
		
		#if !UNITY_EDITOR
		if (offerButtonObj != null)
		{
			bool hasOffers = MunerisManager.SharedManager.HasOffers();
			offerButtonObj.SetActive(hasOffers);
		}			
		#endif
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.JOB);
	}
	
	void StartTutorial()
	{
		QuestFlags.SetFlag(-101, 1);
		PopupManager.ShowTutorialPopup(1, false);
	}
	
	
	public void WearDefaultItems()
	{
		RemoveAllRemovableItems();
		
		int[] values 				= (int[])Enum.GetValues(typeof(DefaultWearingTypes));
		int randIndex 				= UnityEngine.Random.Range(0, values.Length);
		DefaultWearingTypes type 	= (DefaultWearingTypes) values[randIndex];
		
		if (defaultTop.Equals(""))
			type = DefaultWearingTypes.Dress;
		else if (defaultDress.Equals(""))
			type = DefaultWearingTypes.TopBottom;
		else if (defaultTop.Equals("") && defaultDress.Equals(""))
			type = DefaultWearingTypes.NONE;
		
		if (type.Equals(DefaultWearingTypes.TopBottom))
		{
			string[] topItems 		= defaultTop.Split(',');
			string[] bottomItems 	= defaultBottom.Split(',');
			
			int randTopIndex	= UnityEngine.Random.Range(0, topItems.Length);
			int randBottomIndex = UnityEngine.Random.Range(0, bottomItems.Length);
			
			defaultItems = topItems[randTopIndex] + "," + bottomItems[randBottomIndex];
		}
		else if (type.Equals(DefaultWearingTypes.Dress))
		{
			string[] dressItems 	= defaultDress.Split(',');			
			int randDressIndex		= UnityEngine.Random.Range(0, dressItems.Length);
			defaultItems 			= dressItems[randDressIndex];
		}

		//Wear Shoes
		string[] shoeItems 	= defaultShoe.Split(',');
		int randShoeIndex 	=  UnityEngine.Random.Range(0, shoeItems.Length);
		defaultItems += "," + shoeItems[randShoeIndex];


		string[] itemStr = defaultItems.Split(',');
		for (int i = 0; i < itemStr.Length; i++)
		{
			Item item = Items.getItem(int.Parse(itemStr[i]));
			if (item != null)
				fakePlayer.wearItem(item);
		}
		
		//fakePlayer.wearItem(Items.getItem(278));
		//fakePlayer.wearItem(Items.getItem(319));
	}
	
	void RemoveAllRemovableItems()
	{
		foreach (ItemSubcategory subcat in Characters.FEMALE.getCharacterParts())
		{
			Item noneItem = Items.getNoneItemOfSubcategory(subcat.subcategoryid);
			if (noneItem != null)
			{
				fakePlayer.wearItem(noneItem);
			}
		}
	}
	
	
	void ShowHelp()
	{
		PopupManager.ShowInformationPopup("The time you spend on each career decides who you are and what gifts you will receive from your boyfriends.", false);
	}
	
	void OffersButtonClicked()
	{
		MunerisManager.SharedManager.ShowOffers();
	}
	
	public void GoToStreetScene()
	{
		OccupationBarManager occBarManager = occupationBarObj.GetComponent<OccupationBarManager>();
		occBarManager.StopUpdateBars();
		
		//Stop all Sound Effects
		SoundManager.SharedManager.StopSFX();
		SoundManager.SharedManager.FadeOutLoopSFX();
		
		SceneManager.SharedManager.LoadScene(Scenes.STREET, true);
	}
	
	#region InputManager Callback
	public void OnInputManagerBackPressed()
	{
		OccupationBarManager occBarManager = occupationBarObj.GetComponent<OccupationBarManager>();
		occBarManager.StopUpdateBars();
		
		//Stop all Sound Effects
		SoundManager.SharedManager.StopSFX();
		SoundManager.SharedManager.FadeOutLoopSFX();
	}
	
	public void OnInputManagerFemaleCharacterTouched(){}
	public void OnInputManagerBoyfriendChacaterTouched(){}
	
	public void OnInputManagerTouchBegan(Touch touch){}
	public void OnInputManagerTouchEnded(Touch touch){}
	#if UNITY_EDITOR
	public void OnInputManagerMouseClicked(Vector3 clickedPos){}
	#endif
	
	#endregion

}

using UnityEngine;
using System.Collections;

public class HairColorScrollListManager : MonoBehaviour {
	
	float xPos = 0f;
	public ItemSubcategory subcat;
	
	// Use this for initialization
	void Start () {
		//LoadColors();
	}
	
	public void SetSubcategory(ItemSubcategory i)
	{
		subcat = i;
		LoadColors();
	}
	

	void LoadColors () {
		
//		Log.Debug("loading colors");
		
		// remove previous items
		Transform itemPanel = transform;
		
		int childs = itemPanel.childCount;

        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(itemPanel.GetChild(i).gameObject);
        }
		
		SpringPanel.Begin(itemPanel.gameObject, new Vector3(0f,0f,-5f), 5f);
		
		xPos = -96f;
		int sort = 0;
		
		// loop through all player items, if contains the subcat, add!
		for (int i =0 ; i < Globals.mainPlayer.owningItems.Count; i++)
		//foreach (Item item in Globals.mainPlayer.owningItems)
		{
			Item item = Globals.mainPlayer.owningItems[i];
			
			if (item.subCategoryId == subcat.subcategoryid/*ItemSubcategories.getSubcategory("HAIRCOLOR").subcategoryid*/)
			{
				if (sort > 0 /*&& sort%2 == 0*/)
					xPos += 61f;
			
				GameObject go = (GameObject) Instantiate(Resources.Load ("HairColorItem") as GameObject);
			
				go.name = "HairColor"+(sort+1);
				go.transform.parent = itemPanel;
				
				go.GetComponent<HairColorItem>().item = item;
				go.GetComponent<HairColorItem>().sortOrder = sort+1;
				go.transform.localPosition = new Vector3(xPos, 0f/*(sort%2 == 0? 31f:-31f)*/, -1f);
				go.transform.localRotation = Quaternion.identity;
				go.transform.localScale = new Vector3(1f,1f, 1f);
				

				/*go.transform.FindChild("Background").GetComponent<UISprite>().spriteName =
					"hairColor_"+item.itemId;*/
				
				
				sort++;
				
			}
		}
	}
	
	public void goLeft()
	{
		Log.Debug("eft");
		relativeItem(-4);
	}
	public void goRight()
	{
		relativeItem(4);
	}
	
	public void relativeItem(int position)
	{
		
		float springAmount = -(-transform.localPosition.x + (61f* position));
		//Log.Debug("spring? = "+springAmount);
		springAmount = Mathf.Max(-xPos+96, Mathf.Min(springAmount, 0f));
		//Log.Debug("spring after? = "+springAmount);
		
		SpringPanel.Begin(transform.gameObject, new Vector3(springAmount,0f,-5f), 5f);
		 
	}
}

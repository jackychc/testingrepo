using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;

public class PhotoshotScrollListManager : MonoBehaviour {
	
	public GameObject sceneBgObj;
	public GameObject uiCameraAnchorObj;	
	
	public GameObject scrollPanelT;
	private GameObject currentSelectedObj;	
	
	private GameObject mainCharacter;
	private GameObject boyfriendModel;
	
	
	bool isZoomingIn;
	int previousSort = 0;	
	bool cancelHide;
	
	
	// Use this for initialization
	void Start () 
	{
		scrollPanelT.GetComponent<UIDraggablePanel>().onDragFinished += OnDragItemFinished;

		LoadItems();
	}
	
	public void LoadItems()
	{	
		
		// remove previous category items
		Transform itemPanel = scrollPanelT.transform;
		int childs = itemPanel.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
           Destroy(itemPanel.GetChild(i).gameObject);
		}
        
		
		//ItemCategory category = ItemCategories.getCategory((int)ItemCategories.Id.PHOTOSHOTBG);
		//List<ItemSubcategory> subcats = ItemCategories.getListOfSubcategoriesByCategory((int)ItemCategories.Id.PHOTOSHOTBG);
		
		ItemSubcategory subcat = ItemSubcategories.getSubcategory((int)ItemSubcategories.Id.PHOTOSHOTBG);
		List<Item> itemList = Items.getItemsFromSubcategory(subcat.subcategoryid);

		List<int> validIndexPool = new List<int>();
		for(int i = 0; i <  itemList.Count; i++)
		{
			Item item = itemList[i];
			if (item != null)
			{
				if (!Items.requireDLC(item))
				{
					validIndexPool.Add(i);
				}
			}
		}


		Log.Debug(itemList.Count);
		
		//Generate the Items in the scroll list
		int startItemIndex = validIndexPool[UnityEngine.Random.Range(0, validIndexPool.Count - 1)];
		float xPos = 0.0f;
		for (int i = 0; i < itemList.Count; i++)
		{
			Item item = itemList[i];
			
			GameObject itemObj 	= (GameObject) Instantiate(Resources.Load("PhotoBGItem"));
			itemObj.name 		= item.itemName;
			itemObj.transform.parent = itemPanel;
			itemObj.GetComponent<PhotoBGItem>().item = item;
			itemObj.GetComponent<PhotoBGItem>().photoBGItemClickHandler += OnItemClicked;
			itemObj.transform.localPosition = new Vector3(xPos, 0.0f, 0.0f);
			itemObj.transform.localRotation = Quaternion.identity;
			itemObj.transform.localScale	= new Vector3(1.0f, 1.0f, 1.0f);
			//NGUITools.AddChild(itemPanel.gameObject, itemObj);	
			
			
			xPos += itemObj.transform.GetChild(0).FindChild("icon").localScale.x;	
			
			if (i == startItemIndex)
			{
				itemObj.GetComponent<PhotoBGItem>().spriteTick.enabled = true;
				
				StartCoroutine(GenerateSceneWithMeta(itemObj, item));
				SpringPanel.Begin(scrollPanelT.gameObject, new Vector3(-itemObj.transform.localPosition.x, scrollPanelT.transform.localPosition.y, 0f), 8f);
				/*
				string[] meta 		= item.meta.Split(';');
				string itemName 	= meta[0];
				int numOfPPL 		= int.Parse(meta[1]);		
				
				//Render the main Character
				string ch1VectorStr = ((string)meta[2]);
				Log.Debug(ch1VectorStr);
				string[] vectorStr = (ch1VectorStr.Substring(1, ch1VectorStr.Length - 2)).Split(',');
				
				GameObject mainCharacter = GameObject.Find("GirlParent");
				mainCharacter.transform.position 			= new Vector3(float.Parse(vectorStr[0]), float.Parse(vectorStr[1]), float.Parse(vectorStr[2]));
				mainCharacter.transform.localEulerAngles	= new Vector3(float.Parse(vectorStr[3]), float.Parse(vectorStr[4]), float.Parse(vectorStr[5]));
				mainCharacter.transform.localScale			= new Vector3(float.Parse(vectorStr[6]), float.Parse(vectorStr[7]), float.Parse(vectorStr[8]));
								
				if (numOfPPL > 1)
				{
					string ch2VectorStr = ((string)meta[3]);
					Log.Debug(ch2VectorStr.Substring(1, ch1VectorStr.Length - 2));
					vectorStr = (ch2VectorStr.Substring(1, ch1VectorStr.Length - 1)).Split(',');
					
					//Render Boyfriend
					boyfriendModel = (GameObject) Instantiate((Resources.Load("Boyfriends/BF01_mesh") as GameObject));
					boyfriendModel.name = "BOYFRIEND";
					boyfriendModel.AddComponent<CharacterManager>();
					boyfriendModel.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReferenceBF"));
					
					mainCharacter.transform.position 			= new Vector3(float.Parse(vectorStr[0]), float.Parse(vectorStr[1]), float.Parse(vectorStr[2]));
					mainCharacter.transform.localEulerAngles	= new Vector3(float.Parse(vectorStr[3]), float.Parse(vectorStr[4]), float.Parse(vectorStr[5]));
					mainCharacter.transform.localScale			= new Vector3(float.Parse(vectorStr[6]), float.Parse(vectorStr[7]), float.Parse(vectorStr[8]));
					
					
					boyfriendModel.animation.AddClip(Resources.Load("CharacterAssets/Animation/BF_idle_b") as AnimationClip,"BF_idle_b");
					boyfriendModel.animation.wrapMode = WrapMode.Loop;
					boyfriendModel.animation.Play("BF_idle_b");
				}
				
				
				//Render the background
				UITexture sceneBgTexture = sceneBgObj.GetComponent<UITexture>();
				Texture2D tex = (Texture2D) Resources.Load("PhotoshotBackgrounds/" + itemName);
				sceneBgTexture.material = new Material (Shader.Find ("Unlit/Texture"));
		        sceneBgTexture.material.mainTexture = tex;
				currentSelectedObj = itemObj;
				
				
				//Render the foreground
				*/
				
				currentSelectedObj = itemObj;
				
			}
		}
		scrollPanelT.GetComponent<UIGrid>().repositionNow = true;
		scrollPanelT.GetComponent<UIGrid>().Reposition();
	}
	
	
	private IEnumerator GenerateSceneWithMeta(GameObject itemObj, Item item)
	{
		/*StartCoroutine(GenerateSceneWithMetaRoutine(itemObj,item));
	}
	
	IEnumerator GenerateSceneWithMetaRoutine(GameObject itemObj, Item item)
	{*/
		//Render the two characters
		string fileName = "";
		int numOfPPL = 1;		
		Vector3 gPos 	= Vector3.zero;
		Vector3 gRot 	= Vector3.zero;
		Vector3 gScale 	= Vector3.zero;
		
		Vector3 bfPos 	= Vector3.zero;
		Vector3 bfRot 	= Vector3.zero;
		Vector3 bfScale = Vector3.zero;
		
		Vector3 BKPos	= Vector3.zero;
		Vector3 BKRot	= Vector3.zero;
		Vector3 BKScale	= Vector3.zero;
		
		Hashtable metaHash = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(item.texMeta);
		if (metaHash != null)
		{
			fileName = "" + metaHash["fileName"];
			numOfPPL = int.Parse("" + metaHash["numOfPerson"]);
			
			if (Globals.mainPlayer.CurrentBoyfriendId != -1 && numOfPPL > 1)
			{					
				Hashtable twoPersonHash = (Hashtable)metaHash["twoPerson"];
				Hashtable gHash = (Hashtable)twoPersonHash["0"];
				Hashtable bfHash = (Hashtable)twoPersonHash["1"];
				
				string[] BKTr = ((string)twoPersonHash["BKCamera"]).Split(',');
				string[] gTr = ((string)gHash["transform"]).Split(',');
				string[] bfTr = ((string)bfHash["transform"]).Split(',');
				
				//Girl Transform
				gPos = new Vector3(float.Parse("" + gTr[0]), float.Parse("" + gTr[1]), float.Parse("" + gTr[2]));
				gRot = new Vector3(float.Parse("" + gTr[3]), float.Parse("" + gTr[4]), float.Parse("" + gTr[5]));
				gScale = new Vector3(float.Parse("" + gTr[6]), float.Parse("" + gTr[7]), float.Parse("" + gTr[8]));
				
				//BF Transform
				bfPos = new Vector3(float.Parse("" + bfTr[0]), float.Parse("" + bfTr[1]), float.Parse("" + bfTr[2]));
				bfRot = new Vector3(float.Parse("" + bfTr[3]), float.Parse("" + bfTr[4]), float.Parse("" + bfTr[5]));
				bfScale = new Vector3(float.Parse("" + bfTr[6]), float.Parse("" + bfTr[7]), float.Parse("" + bfTr[8]));
				
				//BK Camera
				BKPos = new Vector3(float.Parse("" + BKTr[0]), float.Parse("" + BKTr[1]), float.Parse("" + BKTr[2]));
				BKRot = new Vector3(float.Parse("" + BKTr[3]), float.Parse("" + BKTr[4]), float.Parse("" + BKTr[5]));
				BKScale = new Vector3(float.Parse("" + BKTr[6]), float.Parse("" + BKTr[7]), float.Parse("" + BKTr[8]));
				
				if (boyfriendModel == null)
				{
					yield return StartCoroutine(GetBoyfriendMesh(Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId)));
					boyfriendModel.name = "BOYFRIEND";
					boyfriendModel.AddComponent<CharacterManager>();
					boyfriendModel.GetComponent<CharacterManager>().InsertShadow();
					boyfriendModel.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReferenceBF"));
				}
				else
				{
					boyfriendModel.SetActive(true);	
				}
				
				boyfriendModel.transform.position 			= bfPos;
				boyfriendModel.transform.localEulerAngles	= bfRot;
				boyfriendModel.transform.localScale			= bfScale;					
				
				if (boyfriendModel.animation.GetClip("BF_photo") == null)
					boyfriendModel.animation.AddClip(Resources.Load("CharacterAssets/Animation/BF_photo") as AnimationClip,"BF_photo");
				boyfriendModel.animation.wrapMode = WrapMode.Loop;
				boyfriendModel.animation.Play("BF_photo");
			}
			else
			{
				if (boyfriendModel != null)
				{	
					boyfriendModel.SetActive(false);
				}
				
				Hashtable onePersonHash = (Hashtable)metaHash["onePerson"];
				string[] onePersonBK = ((string)onePersonHash["BKCamera"]).Split(',');
				string[] onePersonTr = ((string)onePersonHash["transform"]).Split(',');
				
				//Girl Transform
				gPos 		= new Vector3(float.Parse("" + onePersonTr[0]), float.Parse("" + onePersonTr[1]), float.Parse("" + onePersonTr[2]));
				gRot 		= new Vector3(float.Parse("" + onePersonTr[3]), float.Parse("" + onePersonTr[4]), float.Parse("" + onePersonTr[5]));
				gScale 		= new Vector3(float.Parse("" + onePersonTr[6]), float.Parse("" + onePersonTr[7]), float.Parse("" + onePersonTr[8]));
				
				//BK Transform
				BKPos 		= new Vector3(float.Parse("" + onePersonBK[0]), float.Parse("" + onePersonBK[1]), float.Parse("" + onePersonBK[2]));
				BKRot 		= new Vector3(float.Parse("" + onePersonBK[3]), float.Parse("" + onePersonBK[4]), float.Parse("" + onePersonBK[5]));
				BKScale 	= new Vector3(float.Parse("" + onePersonBK[6]), float.Parse("" + onePersonBK[7]), float.Parse("" + onePersonBK[8]));	
			}
			
			GameObject bkCamera 					= GameObject.Find("BKCamera");
			bkCamera.transform.position 			= BKPos;
			bkCamera.transform.localEulerAngles 	= BKRot;
			bkCamera.transform.localScale 			= BKScale;
			
			GameObject mainCharacter 					= GameObject.Find("GirlParent");
			mainCharacter.transform.position 			= gPos;
			mainCharacter.transform.localEulerAngles 	= gRot;
			mainCharacter.transform.localScale 			= gScale;
			
			//Render the Background
			StartCoroutine(RenderBackground(itemObj, item, fileName));
			StartCoroutine(RenderForeground(fileName));
		}
	}
	
	IEnumerator RenderForeground(String fileName)
	{
		Camera uiCamera = GameObject.Find("UICamera").GetComponent<Camera>();
		
		//Render the Foreground
		UnityEngine.Object prefab = Resources.Load("PhotoshotBackgrounds/" + fileName + "_foreground");
		if (prefab == null)
		{
			WWW www = WWW.LoadFromCacheOrDownload("file://"+Application.persistentDataPath + "/"  + fileName +"_foreground.dlc", 1);			
			yield return www;
			try
			{
				prefab = www.assetBundle.mainAsset as GameObject;
				www.assetBundle.Unload(false);
			}
			catch (Exception e)
			{
				prefab = null;
				Log.Debug(e.Message);
			}
		}
		
		if (prefab != null)
		{
			GameObject foregroundObj 					= Instantiate(prefab) as GameObject;
			foregroundObj.name 							= fileName + "_foreground";
			foregroundObj.transform.parent 				= uiCameraAnchorObj.transform;
			foregroundObj.transform.localPosition 		= Vector3.zero;
			foregroundObj.transform.localEulerAngles 	= Vector3.zero;
			foregroundObj.transform.localScale 			= new Vector3(1, 1, 1);
			
			foreach (UIAnchor anc in foregroundObj.GetComponentsInChildren<UIAnchor>())
			{
				anc.uiCamera = uiCamera;
			}
			
		}
	}
	
	IEnumerator RenderBackground(GameObject itemObj, Item item, string fileName)
	{
		//Render the background
		UITexture sceneBgTexture = sceneBgObj.GetComponent<UITexture>();
		
		Texture2D tex;		
		tex = (Texture2D) Resources.Load("PhotoshotBackgrounds/" + fileName);
		if (tex == null) // get from dlc
		{
			WWW loader = new WWW("file://" + Application.persistentDataPath + Path.DirectorySeparatorChar + item.meta + ".png");
			yield return loader;
			
			if (loader.error != null)
			{
				Log.Debug("Photoshot Download the background error!");
			}
			else
			{
				if (loader.texture != null)
				{
					tex = ((Texture2D)loader.texture);
				}
				else
				{
					Log.Debug("Photoshot Cannot load the texture!");
				}
			}
		}
		
		sceneBgTexture.material = new Material (Shader.Find ("Unlit/Texture"));
        sceneBgTexture.material.mainTexture = tex;
		currentSelectedObj = itemObj;
	}
	
	#region Scroll View
	void OnDragItemFinished()
	{
		
		/*
		Log.Debug("ON Drag Item Finished");
		UICenterOnChild centerOnChildScript = scrollPanelT.GetComponent<UICenterOnChild>();
		centerOnChildScript.enabled = true;
		centerOnChildScript.Recenter(true);
		centerOnChildScript.enabled = false;
		*/
	}
	#endregion
	
	#region ItemClick Handler
	public void OnItemClicked(GameObject photoBGItemObj)
	{
		if (photoBGItemObj != null)
		{
			PhotoBGItem photoBGItemScript = photoBGItemObj.GetComponent<PhotoBGItem>();
			Item item = photoBGItemScript.item;
			
			//Turn off the previous tick
			if (currentSelectedObj != null)
			{
				Item currentItem = currentSelectedObj.GetComponent<PhotoBGItem>().item;
				if (currentItem.itemName.Equals(item.itemName))
					return;
				currentSelectedObj.GetComponent<PhotoBGItem>().spriteTick.enabled = false;
				
				Hashtable metaHash = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(currentItem.texMeta);
				string fileName = "" + metaHash["fileName"];
				
				Transform foregroundTr = uiCameraAnchorObj.transform.FindChild(fileName + "_foreground");
				if (foregroundTr != null)
				{
					Destroy(foregroundTr.gameObject);
				}				
			}
			
			//Switch the tick state
			photoBGItemScript.spriteTick.enabled = !photoBGItemScript.spriteTick.enabled;
			
			//Spring the panel to the position
			//Vector3 newPos = scrollPanelT.transform.worldToLocalMatrix.MultiplyPoint3x4(photoBGItemObj.transform.position);
			//Vector3 newPos = photoBGItemObj.transform.worldToLocalMatrix.MultiplyPoint3x4(scrollPanelT.transform.position);//scrollPanelT.transform.localToWorldMatrix.MultiplyPoint3x4(photoBGItemObj.transform.position);
       	 	SpringPanel.Begin(scrollPanelT.gameObject, new Vector3(-photoBGItemObj.transform.localPosition.x, scrollPanelT.transform.localPosition.y, 0f), 8f);
			
			
			StartCoroutine(GenerateSceneWithMeta(photoBGItemObj, item));
			/*
			//Render the two characters
			string[] meta 		= item.meta.Split(';');
			string itemName 	= meta[0];
			int numOfPPL 		= int.Parse(meta[1]);		
			
			//Render the main Character
			string ch1VectorStr = ((string)meta[2]);
			Log.Debug(ch1VectorStr);
			string[] vectorStr = (ch1VectorStr.Substring(1, ch1VectorStr.Length - 2)).Split(',');
			GameObject mainCharacter = GameObject.Find("GirlParent");
			mainCharacter.transform.position 			= new Vector3(float.Parse(vectorStr[0]), float.Parse(vectorStr[1]), float.Parse(vectorStr[2]));
			mainCharacter.transform.localEulerAngles	= new Vector3(float.Parse(vectorStr[3]), float.Parse(vectorStr[4]), float.Parse(vectorStr[5]));
			mainCharacter.transform.localScale			= new Vector3(float.Parse(vectorStr[6]), float.Parse(vectorStr[7]), float.Parse(vectorStr[8]));
							
			if (numOfPPL > 1)
			{
				string ch2VectorStr = ((string)meta[3]);
				Log.Debug(ch2VectorStr.Substring(1, ch1VectorStr.Length - 2));
				vectorStr = (ch2VectorStr.Substring(1, ch1VectorStr.Length - 2)).Split(',');
				
				//Render Boyfriend
				if (boyfriendModel == null)
				{
					boyfriendModel = (GameObject) Instantiate((Resources.Load("Boyfriends/BF01_mesh") as GameObject));
					boyfriendModel.name = "BOYFRIEND";
					boyfriendModel.AddComponent<CharacterManager>();
					boyfriendModel.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReferenceBF"));
					boyfriendModel.animation.AddClip(Resources.Load("CharacterAssets/Animation/BF_idle_b") as AnimationClip,"BF_idle_b");
					boyfriendModel.animation.wrapMode = WrapMode.Loop;
				}
				else
					boyfriendModel.SetActive(true);
				
				boyfriendModel.transform.position 			= new Vector3(float.Parse(vectorStr[0]), float.Parse(vectorStr[1]), float.Parse(vectorStr[2]));
				boyfriendModel.transform.localEulerAngles	= new Vector3(float.Parse(vectorStr[3]), float.Parse(vectorStr[4]), float.Parse(vectorStr[5]));
				boyfriendModel.transform.localScale			= new Vector3(float.Parse(vectorStr[6]), float.Parse(vectorStr[7]), float.Parse(vectorStr[8]));				
				boyfriendModel.animation.Play("BF_idle_b");
			}
			else
			{
				if (boyfriendModel != null)
				{
					boyfriendModel.SetActive(false);
					boyfriendModel.animation.Play("BF_idle_b");
				}
			}
			
			
			//Render the background
			UITexture sceneBgTexture = sceneBgObj.GetComponent<UITexture>();
			Texture2D tex = (Texture2D) Resources.Load("PhotoshotBackgrounds/" + itemName);
			sceneBgTexture.material = new Material (Shader.Find ("Unlit/Texture"));
	        sceneBgTexture.material.mainTexture = tex;*/
			
			currentSelectedObj = photoBGItemObj;
		}
	}
	
	
	#endregion

	public void zoomIn()
	{
		GameObject.Find("BGCamera").transform.FindChild("Anchor").GetComponent<PositionTweener>().Forward();
		GameObject.Find("BKCamera").GetComponent<PositionTweener>().Forward();
		isZoomingIn = true;
	}
	
	public void zoomOut()
	{
		GameObject.Find("BGCamera").transform.FindChild("Anchor").GetComponent<PositionTweener>().Backward();
		GameObject.Find("BKCamera").GetComponent<PositionTweener>().Backward();
		isZoomingIn = false;	
	}
	
	public bool isZooming()
	{
		return isZoomingIn;
	}
	
	public void goUp()
	{
		Log.Debug("going up");
		relativeItem(-5);
	}
	public void goDown()
	{
		Log.Debug("going down");
		relativeItem(5);
	}
	
	public void relativeItem(int position)
	{
		//float springAmount = -itemCenter.centeredObject.transform.localPosition.x + (150f* position);
		//Log.Debug("spring? = "+springAmount);
		//springAmount = Mathf.Min(Mathf.Max(0f, springAmount), -yPos);
		//Log.Debug("spring after? = "+springAmount);
		//SpringPanel.Begin(scrollPanelT, new Vector3(springAmount,0f,0f), 5f);	
	}
	
	private IEnumerator GetBoyfriendMesh(Boyfriend bf)
	{
		GameObject returnMe = null;
		
		UnityEngine.Object obj = Resources.Load("Boyfriends/"+bf.meshName);			
		if (obj == null)
		{
			// try load from streaming assets folder instead
			string filePath 	= Application.persistentDataPath + "/"  + bf.meshName +".dlc";
			FileInfo fileInfo 	= new FileInfo(filePath);
		    if (fileInfo != null && fileInfo.Exists)
		    {
		   
				WWW bundle = new WWW("file://" + filePath);
				yield return bundle;
				
				//Log.Debug("am i here?" +item);
				if (bundle.error != null)
				{
					Log.Debug("Photoshot Cannot load the boyfriend "+ bf.meshName + " from path: " + filePath);
				}
				else
				{
					if (bundle.assetBundle != null)
					{
						returnMe = (GameObject) Instantiate(bundle.assetBundle.mainAsset as GameObject);				
						bundle.assetBundle.Unload(false);
						
						//Re-assign the shaders to the boyfriend model						
						SkinnedMeshRenderer[] smrs = returnMe.GetComponentsInChildren<SkinnedMeshRenderer>();
						for (int i = 0; i < smrs.Length; i++)
						{
							SkinnedMeshRenderer smr = smrs[i];
							Material[] materials 		= smr.materials;
							for (int j = 0; j < materials.Length; j++)
							{
								Material material 	= materials[j];
								string shaderName	= material.shader.name.Replace(" ", "").Replace("/", "").Replace("-", "");
								Shader shader 		= Resources.Load("Shaders/" + shaderName) as Shader;
								if (shader != null)
									material.shader	= shader;
								else
									material.shader = Shader.Find(material.shader.name);
							}						
						}
					}
					else
					{
						Log.Debug("Photoshot the boyfriend "+ bf.meshName + " asset bundle is null from path: " + filePath);	
					}
				}
		    }
			else
			{
				Log.Warning("PATH FAIL:"+ bf.meshName);
			}
			
		}
		else
		{
			returnMe = (GameObject) Instantiate(obj as GameObject);
		}
		
		boyfriendModel = returnMe;
	}
}

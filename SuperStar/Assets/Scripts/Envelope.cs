using UnityEngine;
using System.Collections;

public class Envelope : MonoBehaviour
{
	public UILabel number;
	
	void Start()
	{
		StartCoroutine(UpdateEverySecond());
	}
	
	IEnumerator UpdateEverySecond()
	{
		if (Globals.messages != null)
			updateNumber(Messages.GetNoOfMessages());
		
		yield return new WaitForSeconds(1.0f);
		StartCoroutine(UpdateEverySecond());
	}
	
	public void updateNumber(int n)
	{
		if (n <= 0)
		{
			Destroy(transform.gameObject);
		}
		
		number.text = n > 99? "99": (n + "");
	}
	
	public void OpenMessagePopup()
	{
		PopupManager.ShowMessagePopup(false);
	}
}


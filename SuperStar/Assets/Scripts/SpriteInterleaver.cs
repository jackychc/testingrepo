using UnityEngine;
using System.Collections;

public class SpriteInterleaver : MonoBehaviour {
	
	
	public string[] spriteNames;
	int current;
	public float updateInterval = 0.3f;
	UISprite sprite;
	
	// Use this for initialization
	void Start () {
		sprite = transform.GetComponent<UISprite>();
		current = 0;
		
		StartCoroutine(NextSprite());
	}
	
	IEnumerator NextSprite()
	{
		sprite.spriteName = spriteNames[current];
		
		yield return new WaitForSeconds(updateInterval);
		
		current = (current+1)%(spriteNames.Length);
		
		StartCoroutine(NextSprite());
	}
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Muneris.AppVersionCheck;

public class PopupManager {
	
	private enum State
	{
		Show,
		Hide
	}

	private static readonly object popupTicket 	= new object();

	public static volatile List<Popup> pendingPopupQueue 	= new List<Popup>();
	public static volatile List<Popup> popupQueue 			= new List<Popup>();
	private static float latestZIndex 						= -500f;	
	private static ArrayList callbackList 					= new ArrayList();

	public static GameObject loadingFlower, smallLoadingFlower;
	private static LayerMask loadingFlowerRestoreEventMask;
	public static int originalLayer;

	public static void ShowDCBannerPopup(string linkURL, Texture2D texture, bool isShowOnTop)
	{
		GameObject go 	= (GameObject) GameObject.Instantiate(Resources.Load("Popup/DCBannerPopup"));
		go.name 		= "DCBannerPopup";
		go.GetComponent<DCBannerPopup>().texture = texture;
		go.GetComponent<DCBannerPopup>().linkURL = linkURL;

		settings(go, 0);		
		queuing(go, isShowOnTop);
	}

	public static void ShowDailyBonusPopup(bool isShowOnTop)
	{
		GameObject go 	= (GameObject) GameObject.Instantiate(Resources.Load("Popup/DailyBonusPopup"));
		go.name 		= "DailyBonusPopup";

		settings(go,0);		
		queuing(go, isShowOnTop);
	}

	public static void ShowRatingPopup(bool isShowOnTop)
	{
		GameObject go 	= (GameObject) GameObject.Instantiate(Resources.Load("Popup/RatingPopup"));
		go.name 		= "RatingPopup";

		settings(go,0);		
		queuing(go, isShowOnTop);
	}
	
	public static void ShowTutorialPopup(int seq, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/TutorialPopup"+seq));
		go.name = "TutorialPopup"+seq;
		
		settings(go,0);		
		queuing(go, isShowOnTop);		
	}
	
	public static void ShowSystemErrorPopup(string text, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/SystemErrorPopup"));
		go.name = "SystemErrorPopup";
		go.GetComponent<SystemErrorPopup>().SetText(text);
		
		settings(go,0);		
		queuing(go, isShowOnTop);		
	}
	
	public static void ShowATMTermsPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/ATMTermsPopup"));
		go.name = "ATMTermsPopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);
	}
	
	public static void ShowDLCCompletePopup(DLCCompletePopupCallbackInterface c, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/DLCCompletePopup"));
		go.name = "DLCCompletePopup";
		
		if (c != null)
		{
			go.GetComponent<DLCCompletePopup>().ok 		= c.OnDLCCompletePopupOKClicked;
			go.GetComponent<DLCCompletePopup>().cancel 	= c.OnDLCCompletePopupCancelClicked;
		}
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowDLCBetweenPopup(DLCBetweenPopupCallbackInterface s, bool isBoundedByLevel, int toDlcId, int maxSize, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/DLCBetweenPopup"));
		go.name = "DLCBetweenPopup";

		go.GetComponent<DLCBetweenPopup>().isBoundedByLevel = isBoundedByLevel;
		go.GetComponent<DLCBetweenPopup>().toDlcId			= toDlcId;
		go.GetComponent<DLCBetweenPopup>().maxSize			= maxSize;

		if (s != null)
			go.GetComponent<DLCBetweenPopup>().click = s.OnDLCBetweenPopupClicked;
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}

	public static void ShowDLCAskPopup(DLCAskPopupCallbackInterface s, bool canCancelQuit, int pType, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/DLCAskPopup"+(pType==2? "Type2":"")));
		go.name = "DLCAskPopup";
		go.GetComponent<DLCAskPopup>().type = pType;
		
		if (s != null)
		{
			go.GetComponent<DLCAskPopup>().yes 	= s.OnDLCAskPopupYes;
			go.GetComponent<DLCAskPopup>().no 	= s.OnDLCAskPopupNo;
		}
		go.GetComponent<DLCAskPopup>().canCancelQuit = canCancelQuit;
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowShopMorePopup(List<Shop> s, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/ShopMorePopup"));
		go.name = "ShopMorePopup";
		// set text here
		go.GetComponent<ShopMorePopup>().SetShops(s);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowDealPopup(Shop s, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/DealPopup"));
		go.name = "DealPopup";
		// set text here
		go.GetComponent<DealPopup>().setShop(s);
		
		settings(go,3);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowBoyfriendOptionsPopup(BoyfriendMinimapItem item, Boyfriend b, Texture2D tex, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/BoyfriendOptionsPopup"));
		go.name = "BoyfriendOptionsPopup";
		// set text here
		go.GetComponent<BoyfriendOptionsPopup>().setBoyfriend(b);
		go.GetComponent<BoyfriendOptionsPopup>().SetTexture(tex);
		go.GetComponent<BoyfriendOptionsPopup>().boyfriendOptionClicked =  item.BoyfriendOptionClicked;
		
		settings(go,2);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowThrowBoyfriendPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/ThrowBoyfriendPopup"));
		go.name = "ThrowBoyfriendPopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}	
	
	public static void ShowQuestSkipPopup(Quest q, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/QuestSkipPopup"));
		go.name = "QuestSkipPopup";
		go.GetComponent<QuestSkipPopup>().SetQuest(q);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}	
	
	public static void ShowSharePhotoPopup(Texture2D screenshotTexture, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/SharePhotoPopup"));
		go.name = "SharePhotoPopup";
		go.GetComponent<SharePhotoPopup>().screenshotTexture = screenshotTexture;
		
		settings(go,0);		
		queuing(go, isShowOnTop);
	}
	
	public static void ShowSureQuitPopup(Scenes target, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/SureQuitPopup"));
		go.name = "SureQuitPopup";
		go.GetComponent<SureQuitPopup>().SetTargetScene(target);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
	}
	
	public static void ShowPhotoshotPreviewPopup(PhotoshotPreviewPopupCallbackInterface s, Texture2D screenshotTexture, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/PhotoshotPreviewPopup"));
		go.name 													= "PhotoshotPreviewPopup";
		go.GetComponent<PhotoshotPreviewPopup>().screenshotTexture 	= screenshotTexture;
		
		if (s != null)
		{
			go.GetComponent<PhotoshotPreviewPopup>().save 				= s.OnPhotoshotPreviewPopupSave;
			go.GetComponent<PhotoshotPreviewPopup>().share 				= s.OnPhotoshotPreviewPopupShare;
			go.GetComponent<PhotoshotPreviewPopup>().discard 			= s.OnPhotoshotPreviewPopupDiscard;
		}
		
		settings(go,0);		
		queuing(go, isShowOnTop);
	}
	
	public static void ShowClothMatchEndPopup(int pScore, int pCombo, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/ClothMatchEndPopup"));
		go.name = "ClothMatchEndPopup";
		go.GetComponent<ClothMatchEndPopup>().SetParameters(pScore, pCombo);
		
		settings(go,0);
		queuing(go, isShowOnTop);
	}
	
	public static void ShowClothMatchStartPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/ClothMatchStartPopup"));
		go.name = "ClothMatchStartPopup";
		
		settings(go,0);
		queuing(go, isShowOnTop);	
	}
	
	public static void ShowCoinDropRewardPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/CoinDropRewardPopup"));
		go.name = "CoinDropRewardPopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);	
	}
	
	public static void ShowInputNamePopup(InputNamePopupCallbackInterface c, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/InputNamePopup"));
		go.name = "InputNamePopup";
		
		if (c != null)
			go.GetComponent<InputNamePopup>().SetCallback(c);
		
		settings(go, 0);		
		queuing(go, isShowOnTop);		
	}
	
	public static void ShowNewAppVersionPopup(NewAppVersion nAppVersion, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/NewAppVersionPopup"));
		go.name = "NewAppVersionPopup";
		go.GetComponent<NewAppVersionPopup>().newAppVersion = nAppVersion;
		
		settings(go, 0);		
		queuing(go, isShowOnTop);		
	}
	
	
	public static void ShowQuestCompletePopup(Quest q, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/QuestCompletePopup"));
		go.name = "QuestCompletePopup";
		go.GetComponent<QuestCompletePopup>().setQuest(q);
		
		settings(go,0);		
		queuing(go, isShowOnTop);	
	}
	
	public static void ShowQuitGamePopup(InputManager im, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/QuitGamePopup"));
		go.name = "QuitGamePopup";
		go.GetComponent<QuitGamePopup>().crossClicked = im.OnQuitGamePopupCrossClicked;
		
		settings(go,0);		
		queuing(go, isShowOnTop);			
	}
	
	public static void ShowChooseMinigamePopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/ChooseMinigamePopup"));
		go.name = "ChooseMinigamePopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);	
	}
	
	public static void ShowQuestListPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/QuestListPopup"));
		go.name = "QuestListPopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);		
	}

	public static void ShowSocialStatePopup(SocialNetwork.Type type, string description, bool isErrorPopup, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/SocialStatePopup"));
		go.name = "SocialStatePopup";
		go.GetComponent<SocialStatePopup>().type = type;
		go.GetComponent<SocialStatePopup>().description = description;
		go.GetComponent<SocialStatePopup>().isErrorPop = isErrorPopup;
		
		settings(go,0);		
		queuing(go, isShowOnTop);
	}
	
	public static void ShowTwitterLoginPopup(TwitterLoginPopupCallbackInterface c, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/TwitterLoginPopup"));
		go.name = "TwitterLoginPopup";
		
		if (c != null)
		{
			go.GetComponent<TwitterLoginPopup>().success 	= c.OnTwitterLoginPopupLoginSuccess;
			go.GetComponent<TwitterLoginPopup>().failed 	= c.OnTwitterLoginPopupLoginFailed;
		}
		
		settings(go,0);		
		queuing(go, isShowOnTop);	
	}

	
	public static void ShowNewTitlePopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/NewTitlePopup"));
		go.name = "NewTitlePopup";
		settings(go,0);
		
		queuing(go, isShowOnTop);		
	}
	
	public static void ShowRestoreProgressPopup(SocialNetwork.Type type, Hashtable userInfo, bool isNewUser, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/RestoreProgressPopup"));
		go.name = "RestoreProgressPopup";
		go.GetComponent<RestoreProgressPopup>().socialType	= type;
		go.GetComponent<RestoreProgressPopup>().userInfo 	= userInfo;
		go.GetComponent<RestoreProgressPopup>().isNewUser 	= isNewUser;
		
		settings(go,0);		
		queuing(go, isShowOnTop);		
	}
	
	public static void ShowDetectDiffDevice(Hashtable serverData, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/DetectDiffDevicePopup"));
		go.name = "DetectDiffDevicePopup";
		go.GetComponent<DetectDiffDevicePopup>().serverData = serverData;
		
		settings(go,0);		
		queuing(go, isShowOnTop);
	}
	
	public static void ShowDetectNewUserPopup(Hashtable socialInfo, SocialNetwork.Type type, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/DetectNewUserPopup"));
		go.name = "DetectNewUserPopup";
		go.GetComponent<DetectNewUserPopup>().socialInfo = socialInfo;
		go.GetComponent<DetectNewUserPopup>().type = type;
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowDetectExistingUserPopup(Hashtable userInfo, SocialNetwork.Type type, bool isShowOnTop)
	{	
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/DetectExistingUserPopup"));
		go.name = "DetectExistingUserPopup";
		go.GetComponent<DetectExistingUserPopup>().userInfo = userInfo;
		go.GetComponent<DetectExistingUserPopup>().type = type;
		
		settings(go,0);		
		queuing(go, isShowOnTop);		
	}
	
	public static void ShowAchievementListPopup(int page, bool isShowOnTop)
	{
		ShowAchievementListPopup(page, Globals.mainPlayer, isShowOnTop);
	}
	
	public static void ShowAchievementListPopup(int page, Player profile, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/AchievementListPopup"));
		go.name = "AchievementListPopup";
		go.GetComponent<AchievementListPopup>().profile = profile;
		go.GetComponent<AchievementListPopup>().initialPage = page;
		
		settings(go,2);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowResetConfirmPopup(int floor, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/ResetConfirmPopup"));
		go.name = "ResetConfirmPopup";
		go.GetComponent<ResetConfirmPopup>().floor = floor;
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowFriendListPopup(bool isShowOnTop)
	{
		string friendListPopupStr = "FriendListPopup";

		#if UNITY_AMAZON
			friendListPopupStr = "FriendListPopup_Amazon";
		#endif

		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/" + friendListPopupStr));
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowSettingsPopup(bool isShowOnTop)
	{
		string settingsPopupStr = "SettingsPopup";

		#if UNITY_AMAZON
			settingsPopupStr = "SettingsPopup_Amazon";
		#endif

		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/" + settingsPopupStr));
		go.name = "SettingsPopup";	
		
		settings(go,0);	
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowKeepLookPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/KeepLookPopup"));
		go.name = "KeepLookPopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowGameIDAskPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/GameIDAskPopup"));
		go.name = "GameIDAskPopup";	
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowGameIDGetPopup(string id, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/GameIDGetPopup"));
		go.name = "GameIDGetPopup";
		go.GetComponent<GameIDGetPopup>().setText(id);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowNotEnoughCurrencyPopup(string pCurrency, int pAmount, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/NotEnoughCurrencyPopup"));
		go.name = "NotEnoughCurrencyPopup";
		go.GetComponent<NotEnoughCurrencyPopup>().Set(pCurrency, pAmount);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowBoyfriendRewardPopup(Boyfriend f, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/BoyfriendRewardPopup"));
		go.name = "BoyfriendRewardPopup";
		go.GetComponent<BoyfriendRewardPopup>().setBoyfriend(f);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowBoyfriendListPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/BoyfriendListPopup"));
		go.name = "BoyfriendListPopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowFlirtCompletePopup(Boyfriend f, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/FlirtCompletePopup"));
		go.name = "FlirtCompletePopup";
		go.GetComponent<FlirtCompletePopup>().setBoyfriend(f);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowBoyfriendMinimapPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/BoyfriendMinimapPopup"));
		go.name = "BoyfriendMinimapPopup";
		
		settings(go,3);		
		queuing(go, isShowOnTop);
		
	}
	
	
	public static void ShowIAPPopup(int page, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/IAPPopup"));
		go.name = "IAPPopup";
		go.GetComponent<IAPPopup>().goToPage = page;
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowLevelUpPopup(Reward reward, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/LevelUpPopup"));
		go.name = "LevelUpPopup";
		// set text here
		go.GetComponent<LevelUpPopup>().setReward(reward);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowPreviousTopicPopup(Topic t, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/PreviousTopicPopup"));
		go.name = "PreviousTopicPopup";
		go.GetComponent<PreviousTopicPopup>().setTopic(t);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowMessagePopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/MessagePopup"));
		go.name = "MessagePopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowVotingRequestPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/VotingRequestPopup"));
		go.name = "VotingRequestPopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowVotingReadyPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/VotingReadyPopup"));
		go.name = "VotingReadyPopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowItemBroughtPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/ItemBroughtPopup"));
		go.name = "ItemBroughtPopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	public static void ShowChooseGiftPopup(List<Reward> rewards, int type, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/ChooseGiftPopupType2"));
		go.name = "ChooseGiftPopupType2";
		go.GetComponent<ChooseGiftPopup>().setRewards(rewards);
		go.GetComponent<ChooseGiftPopup>().setType(type);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowChooseGiftPopup(Reward reward, int type, bool isShowOnTop)
	{
		string gameObjectName = "ChooseGiftPopup" + (type==2?"Type2":"");		
		
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/" + gameObjectName));
		go.name = gameObjectName;
		go.GetComponent<ChooseGiftPopup>().setReward(reward);
		go.GetComponent<ChooseGiftPopup>().setType(type);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowChooseGiftPopup(Reward reward, bool isShowOnTop)
	{
		ShowChooseGiftPopup(reward,1, isShowOnTop);
		
	}
	
	
	public static void ShowFriendAddedPopup(bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/FriendAddedPopup"));
		go.name = "FriendAddedPopup";
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowFriendInformationPopup(Player friend, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/FriendInformationPopup"));
		go.name = "FriendInformationPopup";
		go.GetComponent<FriendInformationPopup>().setFriend(friend);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}

	public static void ShowRewardPopup(Reward reward, string description, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/RewardPopup"));
		go.name = "RewardPopup";

		// set text here
		go.GetComponent<RewardPopup>().setReward(reward, description);

		settings(go,0);		
		queuing(go, isShowOnTop);
	}

	public static void ShowRewardPopup(Reward reward, bool isShowOnTop)
	{
		ShowRewardPopup(reward, null, isShowOnTop);
	}	
	
	public static void ShowFinishByGemPopup(JobScrollListItem scrollListItem, Job job, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/FinishByGemPopup"));
		go.name = "FinishByGemPopup";
		// set text here
		go.GetComponent<FinishByGemPopup>().setJob(job);
		go.GetComponent<FinishByGemPopup>().finishByGemClicked =  scrollListItem.OnFinishByGemClicked;
		
		settings(go,1);		
		queuing(go, isShowOnTop);
		
	}
	
	public static void ShowInformationPopup(string text, bool isShowOnTop)
	{
		GameObject go = (GameObject) GameObject.Instantiate(Resources.Load("Popup/InformationPopup"));
		go.name = "InformationPopup";
		go.GetComponent<InformationPopup>().setText(text);
		
		settings(go,0);		
		queuing(go, isShowOnTop);
		
	}

	public static void ShowLoadingFlower()
	{
		ShowLoadingFlower("");
	}
	
	public static void ShowLoadingFlower(string description)
	{
		if (loadingFlower != null)
		{
			GameObject.Destroy(loadingFlower);
			loadingFlower = null;
		}
		
		GameObject uiCameraObj = GameObject.Find("UICamera");
		
		loadingFlower 											= (GameObject) GameObject.Instantiate(Resources.Load("LoadingFlower"));
		loadingFlower.transform.parent 							= uiCameraObj.transform.FindChild("Anchor").transform;
		loadingFlower.transform.localPosition 					= new Vector3(0f,0f,-950f);
		loadingFlower.transform.localRotation 					= Quaternion.identity;
		loadingFlower.transform.localScale 						= Vector3.one;
		loadingFlower.GetComponent<LoadingFlower>().description = description;
		
		//Set the loading Flower to a unique event mask
		loadingFlowerRestoreEventMask = uiCameraObj.GetComponent<UICamera>().eventReceiverMask;
		uiCameraObj.GetComponent<UICamera>().eventReceiverMask = 1 << 10;
		
		loadingFlower.GetComponent<LoadingFlower>().Show();
		SendCallbackMessage(State.Show, loadingFlower);
	}
	
	public static void HideLoadingFlower()
	{
		
		if (loadingFlower != null)
		{
			GameObject uiCameraObj = GameObject.Find("UICamera");
			
			loadingFlower.GetComponent<LoadingFlower>().Hide();
			SendCallbackMessage(State.Hide, loadingFlower);
			
			GameObject.Destroy(loadingFlower);			
			loadingFlower = null;
			
			uiCameraObj.GetComponent<UICamera>().eventReceiverMask = loadingFlowerRestoreEventMask;
		}
	}
	
	public static void ShowSmallLoadingFlower()
	{
		smallLoadingFlower = (GameObject) GameObject.Instantiate(Resources.Load("LoadingFlowerSmall"));
		smallLoadingFlower.transform.parent = GameObject.Find("UICamera").transform.FindChild("Anchor").transform;
		smallLoadingFlower.transform.localPosition = new Vector3(0f,0f,-950f);
		smallLoadingFlower.transform.localRotation = Quaternion.identity;
		smallLoadingFlower.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
		
		// small loading flower won't take focus of screen
		//loadingFlower.GetComponent<LoadingFlower>().Show();
	}
	
	public static void HideSmallLoadingFlower()
	{
		
		if (smallLoadingFlower != null)
		{
			//loadingFlower.GetComponent<LoadingFlower>().Hide();
			
			GameObject.Destroy(smallLoadingFlower);
			
			smallLoadingFlower = null;
			
			
		}
	}
	
	public static void SetCallback(PopupManagerCallbackInterface callback)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(callback))
				return;
			
			callbackList.Add(callback);
		}
	}
	
	public static void RemoveCallback(PopupManagerCallbackInterface callback)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(callback))
				callbackList.Remove(callback);
		}
	}
	
	private static void SendCallbackMessage(State s, GameObject popup)
	{
		switch(s)
		{
			case State.Show:
			{
				if (callbackList != null)
				{
					for (int i = 0; i < callbackList.Count; i++)
					{
						PopupManagerCallbackInterface c = (PopupManagerCallbackInterface)callbackList[i];
						c.OnPopupManagerShowPopup(popup);
					}
				}			
				break;	
			}
			case State.Hide:
			{
				if (callbackList != null)
				{
					for (int i = 0; i < callbackList.Count; i++)
					{
						PopupManagerCallbackInterface c = (PopupManagerCallbackInterface)callbackList[i];
						c.OnPopupManagerHidePopup(popup);
					}
				}
				break;	
			}
		}
	}
	
	static void settings(GameObject go, int type)
	{
		GameObject uicamerObj = GameObject.Find("UICamera");
		go.transform.parent  = uicamerObj.transform.FindChild("Anchor").transform;	
		//go.transform.parent = GameObject.Find("UICamera").transform.FindChild("Anchor").transform; // this is slow. how to improve?	
		go.transform.localScale = Vector3.one;
		switch (type)
		{
			case 3:
			{
				go.transform.localPosition = new Vector3(0f,0f, -80f);
				break;
			}
			case 2:
			{
				go.transform.localPosition = new Vector3(0f,0f, -120f);
				break;
			}
			case 1:
			{
				go.transform.localPosition = new Vector3(0f,0f, -100f);
				break;
			}
			default:
			{
				//Change all the uicamera to uicamera
				
				foreach(UIAnchor anchor in go.gameObject.GetComponentsInChildren<UIAnchor>())
				{
					anchor.uiCamera = uicamerObj.GetComponent<Camera>();
				}
				//go.GetComponent<UIAnchor>().uiCamera = GameObject.Find("UICamera").GetComponent<Camera>();
				go.transform.localPosition = new Vector3(-1000f,-1000f, -100f);
				break;
			}
		}
		
		go.transform.localRotation = Quaternion.identity;
	}
	
	static void queuing(GameObject go)
	{
		queuing(go, false);
	}
	
	static void queuing(GameObject go, bool isShowAtTop)
	{
		lock(popupTicket)
		{
			Popup popup = go.GetComponent<Popup>();
			if (popup != null)
			{	
				if (SceneManager.SharedManager != null && SceneManager.SharedManager.IsLoading())
				{
					if (isShowAtTop)
					{
						pendingPopupQueue.Insert(0, popup);
					}
					else
					{
						pendingPopupQueue.Add(popup);
					}
				}
				else
				{
					if (isShowAtTop)
					{
						popupQueue.Insert(0, popup);

						for (int i = 1; i < popupQueue.Count; i++)
						{
							Popup p = (Popup)popupQueue[i];

							//Change the whole popup hierarchy to Defauly Layer
							foreach (Transform t in p.GetComponentsInChildren<Transform>(true))
							{
								t.gameObject.layer = 0;	
							}
							p.gameObject.layer = 0;
						}					

						go.transform.localPosition 	= new Vector3(0f,0f, latestZIndex);
						go.transform.localRotation 	= Quaternion.identity;
						go.transform.localScale 	= Vector3.one;
						popup.Show();
						SendCallbackMessage(State.Show, go);

						latestZIndex -= 50f;
					}
					else
					{
						popupQueue.Add(popup);


						if (popupQueue.Count <= 1)
						{
							//Restore the whole hierarchy layer to Popup
							foreach (Transform t in popup.GetComponentsInChildren<Transform>(true))
							{
								t.gameObject.layer = Popup.popupEventMask;	
							}
							popup.gameObject.layer = Popup.popupEventMask;

							popup.Show();
							SendCallbackMessage(State.Show, go);
						}
					}
				}
			}
		}
	}

	public static void RestorePendingPopup()
	{
		if (pendingPopupQueue != null)
		{
			for (int i = 0; i < pendingPopupQueue.Count; i++)
			{
				Popup p 			= pendingPopupQueue[i];
				GameObject popupObj = p.gameObject;

				if (popupObj != null)
				{
					queuing(popupObj);
				}
			}

			pendingPopupQueue.Clear();
		}
	}
	
	public static bool HasPopup(string popupName)
	{
		bool hasPopup = false;
		lock(popupTicket)
		{
			for (int i = 0; i < popupQueue.Count; i++)
			{
				Popup popup = (Popup)popupQueue[i];
				if (popup.gameObject.name.Equals(popupName))
				{
					hasPopup = true;
					break;
				}
			}
		}
		
		return hasPopup;
	}
	
	public static void RemovePopup(int index)
	{
		lock(popupTicket)
		{
			SendCallbackMessage(State.Hide, popupQueue[index].gameObject);
			
			popupQueue.RemoveAt(index);
			if (latestZIndex < -500f)
				latestZIndex += 50f;
			
			if (popupQueue.Count > 0)
			{
				Popup p = (Popup)popupQueue[0];
				
				//Restore the whole hierarchy layer to Popup
				foreach(Transform t in p.GetComponentsInChildren<Transform>(true))
				{
					t.gameObject.layer = Popup.popupEventMask;	
				}
				p.gameObject.layer = Popup.popupEventMask;
			}
		}
	}
	
	public static int PopupCount()
	{
		lock(popupTicket)
		{
			if (popupQueue != null)
				return popupQueue.Count;
			else
				return 0;
		}
	}
	
	public static Popup GetLatestPopup()
	{
		Popup returnMe = null;
		lock(popupTicket)
		{			
			if (popupQueue.Count > 0)			
				returnMe= popupQueue[0];			
		}
		
		return returnMe;
	}
	
	public static void ShowLatestPopup()
	{
		lock(popupTicket)
		{
			Popup popup = (Popup)popupQueue[0];
			popup.Show();
			SendCallbackMessage(State.Show, popup.gameObject);
		}
	}
	
	public static void HideLatestPopup()
	{
		/*foreach (Popup go in popupQueue)
		{
			if (go != null)
			go.Hide();
		}
		popupQueue.Clear();*/
		
		lock(popupTicket)
		{
			Popup popup = (Popup)popupQueue[0];
			popup.Hide();
			SendCallbackMessage(State.Hide, popup.gameObject);
		}		
	}
	
	public static void ClearPopupQueue()
	{
		lock(popupTicket)
		{
			if (popupQueue.Count > 0)
				popupQueue.Clear();
		}
	}
	
	public static void HideAllPopup()
	{
		lock(popupTicket)
		{
			Log.Debug("Popup HideAllPopup");
			while(popupQueue.Count > 0)
			{
				Popup popup = popupQueue[0];
				if (popup != null)
				{
					Log.Debug("Hide Object " + popup.name);
					popup.SilentHide();
					
					SendCallbackMessage(State.Hide, popup.gameObject);
				}
			}
		}
	}
}

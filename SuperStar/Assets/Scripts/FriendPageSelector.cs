using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;


public class FriendPageSelector : MonoBehaviour{
	
	public GameObject bg;
	
	public GameObject friendsList;
	public GameObject friendBtnSprite;
	
	public GameObject friendAddSprite;
	public GameObject RefreshButtonObj;
	
	public GameObject addModule;
	public AddFriendChecking addFriendChecking;
	
	public Transform friendsPanel;
	public GameObject noFriendLabel;
	
	private bool isLoadingFromServer = false;
	
	//public FriendsScene friendsScene;
	
	public GameObject yourID;
	
	// Use this for initialization
	void Start () {
		yourID.GetComponent<UILabel>().text = Globals.mainPlayer.GameId;	
		
		/*
		if (!Globals.mainPlayer.GameId.Equals(""))
		{
			
			//LoadFriends();
			isLoadingFromServer = true;
			StartCoroutine(GetFriendsFromServer());
		}*/
		
		SelectPage(1);
	}

	void SelectPage(int page)
	{
		switch (page)
		{
			case 1:
				bg.GetComponent<UITexture>().mainTexture = (Texture2D) Resources.Load("Backgrounds/friends_searchFriends_bg");
				friendsList.SetActive(true);
				friendBtnSprite.SetActive(true);
				RefreshButtonObj.SetActive(true);
				friendAddSprite.SetActive(false);
				addModule.SetActive(false);
			
				//if (!isLoadingFromServer)
					LoadFriends();
			//TODO: friend pop up . load?
				//friendsScene.LoadFriends();
				//yourID.SetActive(false);
				
				break;			
			case 2:
				bg.GetComponent<UITexture>().mainTexture = (Texture2D) Resources.Load("Backgrounds/friends_addFriends_bg");
				friendsList.SetActive(false);
				friendBtnSprite.SetActive(false);
				RefreshButtonObj.SetActive(false);
				friendAddSprite.SetActive(true);
				addModule.SetActive(true);
				noFriendLabel.SetActive(false);
				//yourID.SetActive(true);
				break;

				
			default:break;
		}
	}
	
	void SelectPage1()
	{
		SelectPage (1);	
	}
	
	void SelectPage2()
	{
		SelectPage (2);	
	}
	
	public void LoadFriends()
	{
		// remove previous items
		noFriendLabel.gameObject.SetActive(false);
		
		
		int childs = friendsPanel.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(friendsPanel.GetChild(i).gameObject);
        }
		
		//add items
		
		for (int i = 0; i < Friends.friends.Count; i++)
		{
			GameObject friendItem = (GameObject) Instantiate(Resources.Load("FriendItem") as GameObject);
			
			friendItem.transform.parent = friendsPanel;
			
			friendItem.transform.localPosition = new Vector3(-160f, 210f - (i) * 100f ,0f);
			friendItem.transform.localRotation = Quaternion.identity;
			friendItem.transform.localScale = Vector3.one;
			
			
			friendItem.GetComponent<FriendItem>().SetFriend(Friends.friends[i]);
			
		}
		
		// TEST AREA
		/*List<Texture2D> combineList = new List<Texture2D>();
		foreach (FriendItem item in friendsList.GetComponentsInChildren<FriendItem>())
		{
			combineList.Add((Texture2D) item.friendName.GetComponent<UISysFontLabel>().mainTexture);
		}
		
		Texture2D bigTexture = new Texture2D(2048,2048);
		Rect[] rects = bigTexture.PackTextures(combineList.ToArray(), 2, 2048);
		
		foreach (FriendItem item in friendsList.GetComponentsInChildren<FriendItem>())
		{
			item.friendName.GetComponent<UISysFontLabel>().mainTexture = bigTexture;
				
		}*/
		
		
		
		
		if (Friends.friends.Count <= 0)
		{
			noFriendLabel.gameObject.SetActive(true);
		}
	}
	
	public static void VisitFriend (Player friend)
	{
		Log.Debug("visit friend: "+ friend.PlayerId);
	}	

	IEnumerator GetFriendsFromServer()
	{
		PopupManager.ShowLoadingFlower();
		
		//Prepare CheckSum
		GameID gameId 			= new GameID(Globals.mainPlayer.GameId);
		int playerId 			= gameId.toInt();
		string checkSumData 	= gameId.ID + playerId;
		Log.Debug("checksum!" + checkSumData);
		string checkSum 		= MD5Hash.GetServerCheckSumWithString("GetFriends", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "GetFriends.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response 			= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			
			if (response != null && !response.Equals(""))
			{
				// TODO: turn the reponse string to friend list
				ParseFriends(response);
				//Friends.Save();
			}
		}			
		//yield return StartCoroutine(HTTPPost.Post("GetFriends", kvp, result => response = result));
		
		isLoadingFromServer = false;
		PopupManager.HideLoadingFlower();
	}
	
	void ParseFriends(string input)
	{
		Log.Debug("input?" + input);
		//add players to friends.friends
		ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(input)).Values);
		if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
		{
			// have friend
			if (al[1] != null)
			{
				ArrayList fl =  new ArrayList(((Hashtable) al[1]).Values);
				
				foreach (Hashtable fh in fl)
				{
					Friends.AddFriend(fh);
				}
				
				// this line should be commented out when move outside this class
				LoadFriends();
			}
		}
	}
	
	public IEnumerator OnRefreshButtonClicked()
	{
		if (!Globals.mainPlayer.GameId.Equals(""))
		{			
			//LoadFriends();
			isLoadingFromServer = true;
			yield return StartCoroutine(GetFriendsFromServer());

			if (NetworkManager.SharedManager.NetworkState.Equals(NetworkManager.State.Fail))
			{
				PopupManager.ShowInformationPopup(Language.Get("msg_network_error"), true);
			}
		}
	}
	
	public void GoToHomeScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.HOME, true);	
	}
	
	public void GoToInitDBScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.INITDB, true);	
	}
}

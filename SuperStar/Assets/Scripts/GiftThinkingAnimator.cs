using UnityEngine;
using System.Collections;

public class GiftThinkingAnimator : MonoBehaviour {
	public GameObject[] sprites = new GameObject[3];
	public float period = 0.6f;
	public int spriteNo = 0;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	void OnEnable()
	{
		Log.Debug("ENABLE");
		StopCoroutine("UpdateAtInterval");
		StartCoroutine(UpdateAtInterval());
	}
	
	void OnDisable()
	{
		StopCoroutine("UpdateAtInterval");	
	}
	
	IEnumerator UpdateAtInterval()
	{		
		while(true)
		{			
			spriteNo = ((spriteNo + 1) >= sprites.Length)? -1: spriteNo + 1;
			
			for (int i = 0; i < sprites.Length; i++)
			{
				sprites[i].SetActive(spriteNo >= i);
			}
			
			
			yield return new WaitForSeconds(period);
		}
	}


}

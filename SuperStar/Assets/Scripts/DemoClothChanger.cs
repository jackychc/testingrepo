using UnityEngine;
using System.Collections;

// this class is for demo use only, should not exist at production LOL

public class DemoClothChanger : MonoBehaviour {
	
	GameObject girl = null;
	UICenterOnChild centerer = null;
	int prevSelection = 0;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (girl == null)
		{
			girl = GameObject.Find("FEMALE");
		}
		if (centerer == null)
		{
			centerer = GetComponent<UICenterOnChild>();
		}
		
		
		
		if (centerer.centeredObject.GetComponent<UISprite>().spriteName.Contains("2"))
		{
			if (prevSelection == 2) return;
			
			girl.transform.FindChild("shirt").gameObject.SetActive(true);
			girl.transform.FindChild("skirtA").gameObject.SetActive(false);	
			
			// change texture test;
			Material[] m = new Material[1];
			m[0] = (Material) Resources.Load("CharacterAssets/test/idle_test/Materials/shirt_mat1");
			girl.transform.FindChild("shirt").GetComponent<SkinnedMeshRenderer>().materials = m;
			
			
			m[0] = (Material) Resources.Load("CharacterAssets/test/idle_test/Materials/trousers_mat1");
			girl.transform.FindChild("trousers").GetComponent<SkinnedMeshRenderer>().materials = m;
			
			prevSelection = 2;
		}
		
		if (centerer.centeredObject.GetComponent<UISprite>().spriteName.Contains("1"))
		{
			if (prevSelection == 1) return;
			
			girl.transform.FindChild("shirt").gameObject.SetActive(true);
			girl.transform.FindChild("skirtA").gameObject.SetActive(false);	
			
			// change texture test;
			Material[] m = new Material[1];
			m[0] = (Material)  Resources.Load("CharacterAssets/test/idle_test/Materials/shirt_mat2");
			girl.transform.FindChild("shirt").GetComponent<SkinnedMeshRenderer>().materials = m;
			
			m[0] = (Material) Resources.Load("CharacterAssets/test/idle_test/Materials/trousers_mat2");
			girl.transform.FindChild("trousers").GetComponent<SkinnedMeshRenderer>().materials = m;
			
			prevSelection = 1;
		}
		
		if (centerer.centeredObject.GetComponent<UISprite>().spriteName.Contains("3"))
		{
			if (prevSelection == 3) return;
			
			girl.transform.FindChild("shirt").gameObject.SetActive(false);
			girl.transform.FindChild("skirtA").gameObject.SetActive(true);
			
			prevSelection = 3;
		}
	}
}

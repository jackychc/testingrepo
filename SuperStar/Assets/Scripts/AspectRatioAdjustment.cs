using System;
using UnityEngine;
using System.Collections;

// The script make UIRoot scale based on manualWidth instead of UIRoot.manualHeight.
// if current aspect ratio > maxAcceptAspectRatio.
[RequireComponent(typeof(UIRoot))]
public class AspectRatioAdjustment : MonoBehaviour {

    UIRoot uiRoot;
    public int manualWidth;
    public float maxAcceptAspectRatio = 1.5f;

    void Awake() {
        uiRoot = GetComponent<UIRoot>();
    }

    void OnEnable() {
        UpdateHeight();
    }

    void UpdateHeight() {
        float aspect = (float)Screen.height / Screen.width;
        if (aspect > maxAcceptAspectRatio) {
            // Change manualHeight to simulate scale based on manualWidth.
            int manualHeight = (int)(manualWidth * aspect);
            if (uiRoot.manualHeight != manualHeight)
                uiRoot.manualHeight = manualHeight;
        }
    }
	
	
	
	
	 
}
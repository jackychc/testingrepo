using UnityEngine;
using System.Collections;

public class ScaleTweener : MonoBehaviour {
	
	public enum TWEENMODE
	{
		PASSIVE,
		LOOP,
		BACK_AND_FORTH,
		CONSTANT
	}
	
	public enum CURVETYPE
	{
		STRAIGHT,
		ASCEND,
		DESCEND
	}
	
	public int id = 1;
	float proportion = 0f;
	bool commanded;
	bool isForward = true;
	
	public float speed = 1f;
	 float fromXValue;
	public float toXValue;
	 float fromYValue;
	public float toYValue;
	 float fromZValue;
	public float toZValue;
	
	float sineProportion = 0f;
	
	public TWEENMODE mode = TWEENMODE.PASSIVE;
	public CURVETYPE curveType = CURVETYPE.DESCEND;
	
	Vector3 t = Vector3.zero;
	Vector3 fromVector = Vector3.zero, toVector = Vector3.zero;
	
	Transform mTrans;
	public Transform cachedTransform { get { if (mTrans == null) mTrans = transform; return mTrans; } }
	
	// Use this for initialization
	void Start () {
		resetStartPosition();
	}
	
	public void resetStartPosition()
	{
		proportion = 0f;
		fromVector = cachedTransform.localScale;
		toVector = new Vector3(toXValue, toYValue, toZValue);
		
		t = cachedTransform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (mode.Equals(TWEENMODE.PASSIVE) && !commanded)
			return;
		
		
		proportion = Mathf.Clamp(proportion, 0.0f,1.0f);
		
		
		Move();
	}
	
	public float getProportion()
	{
		return proportion;
	}
	
	public void setProportion(float v)
	{
		proportion = v;
	}
	
	public void Forward()
	{
		commanded = true;
		isForward = true;
	}
	
	public void Backward()
	{
		commanded = true;
		isForward = false;
	}
	
	void Move()
	{
		
		proportion = proportion + (isForward? 1 : -1) * speed * (1f/30);
		
		sineProportion = proportion;
		
		if (curveType == CURVETYPE.DESCEND) sineProportion = 1 - (proportion-1)*(proportion-1);
		if (curveType == CURVETYPE.ASCEND) sineProportion = (proportion)*(proportion);
		
		cachedTransform.localScale = Vector3.Lerp(fromVector, toVector, sineProportion);
		
		if (mode.Equals(TWEENMODE.LOOP) || mode.Equals(TWEENMODE.CONSTANT))
		{
			if (proportion >= 1.0f)
				proportion = 0f;
			
		}
		
		else if (mode.Equals(TWEENMODE.BACK_AND_FORTH))
		{
			if (proportion <= 0f || proportion >= 1.0f)
				isForward = !isForward;
		}
		
		else if (mode.Equals(TWEENMODE.PASSIVE))
		{
			if (proportion <= 0f || proportion >= 1.0f)
				commanded = false;
		}
	}
}

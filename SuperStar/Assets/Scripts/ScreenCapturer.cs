using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

public class ScreenCapturer : MonoBehaviour
{
	GameObject newCharacter;
	bool captureLock = false;
	
	void Start()
	{
		if (GameObject.Find("FEMALE") == null)
		{
			newCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			newCharacter.name = "FEMALE";
			newCharacter.AddComponent<CharacterManager>();
			//DontDestroyOnLoad(newCharacter);
		}
		
		else
		{
			newCharacter = GameObject.Find("FEMALE");
		}
		
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
		newCharacter.GetComponent<CharacterManager>().GenerateCharacter(Globals.mainPlayer);
		
		StartCoroutine(Capture());
		
		//SoundManager.SharedManager.ChangeMusic("SuperstarStory_fashioncontest");
	}
	
	public IEnumerator Capture()
	{
		if (captureLock) yield break;
		
		captureLock = true;
		
		yield return new WaitForEndOfFrame();
		Vector4 dimensions = getDimensions(0.4f, 0.7f, 0f, 0.05f);
		
		int startX = (int) dimensions.w;
		int startY = (int) dimensions.x;
        int width = (int) dimensions.y;
        int height = (int) dimensions.z;
		
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(startX, startY, width, height), 0, 0);
        tex.Apply();
        byte[] imageBytes = tex.EncodeToPNG();
		
		
		
		//UITexture test = GameObject.Find("TestPic").GetComponent<UITexture>();
		//test.material = new Material (Shader.Find ("Unlit/Transparent Colored"));
        //test.material.mainTexture = tex;
        //test.MakePixelPerfect();
		
		PopupManager.ShowLoadingFlower();
		
	
		string topicId = Globals.viewingTopic.topicId.ToString();
		string gameId = Globals.mainPlayer.GameId;
		string AWSKey = "AKIAJSQSEMWZ5H4GVMYQ";
		string AWS_URL = "http://superstarstory.s3.amazonaws.com/Topics/Topics"+topicId+"/"+gameId+".png";
		string SERVER_URL = Globals.serverAPIURL + "CreateS3Authorization.php";
		string date = System.DateTime.UtcNow.ToUniversalTime().ToString("r");
		
		WWWForm form = new WWWForm();
		form.AddField("date", date);
		form.AddField("topicId", topicId);
		form.AddField("gameId", gameId);
		
		WWW www = new WWW(SERVER_URL, form);		
		System.DateTime startTime = System.DateTime.Now;
		yield return www;
		
		string authorization = null;
		if (www.error == null)
		{
			authorization = www.text;
			
			
			
			//Make a put request on Amazon
			var request = new HTTP.Request("PUT", AWS_URL);
			request.headers.Set("Authorization", authorization);
			request.headers.Set("Date", date);
			request.headers.Set("AWSAccessKeyId", AWSKey);
			request.headers.Set("Content-Type", "image/png");
			request.headers.Set("Content-Length", imageBytes.Length.ToString());
			request.headers.Set("x-amz-acl", "public-read");
			request.bytes = imageBytes;
			
			
			request.Send();
			while(!request.isDone)
				yield return null;
			
			System.DateTime endTime = System.DateTime.Now;
			System.TimeSpan duration = endTime.Subtract(startTime);
			
			int statusCode = request.response.status;
			bool isOk = request.response.message.Equals("OK");		
			if (statusCode == 200 && isOk)
			{
				Log.Debug("Upload Successfully!");	
				Log.Debug("Time used: "+ duration.TotalSeconds);
				
				yield return StartCoroutine(JoinTopicOnServer());
				//transform.GetComponent<SceneManager>().GoToScene();
			}
			else 
			{
				Log.Debug("Failed");
				Log.Debug(statusCode);
				Log.Debug(request.response.message);
				
				captureLock = false;
				PopupManager.HideLoadingFlower();
				//test.material.mainTexture = null;
			}
			
		}
		else
		{
			Log.Debug("cannot make the authorization header");	
			Log.Debug(www.error);
			
			captureLock = false;
			PopupManager.HideLoadingFlower();
			//test.material.mainTexture = null;
		}
		
		
		//Destroy(tex);
		
		//
	}
	
	// use like anchor (start from center)
	public static Vector4 getDimensions(float proportionX, float proportionY, float offsetX, float offsetY )
	{
		Vector4 returnMe = Vector4.zero;
		
		//Log.Debug("screen width height" + Screen.width +","+Screen.height);
		
		float screenRatio = Screen.height * 1f / Screen.width;
		
		// result picture's pixel dimension
		returnMe.y = Screen.width *       proportionX * Mathf.Min(1f, (screenRatio/1.5f));
		returnMe.z = Screen.height *      proportionY * Mathf.Min(1f, (1.5f/screenRatio));
		
		Log.Debug("final dimension: " + returnMe.y +","+ returnMe.z);
			
		
		returnMe.w = (Screen.width/2) - (returnMe.y/2) + (offsetX * Screen.width);
		if (screenRatio < 1.5f) 
			returnMe.w = (Screen.width/2) - (returnMe.y/2) + (offsetX * (Screen.width * screenRatio/1.5f));
		
		returnMe.x = (Screen.height/2) - (returnMe.z/2) + (offsetY * Screen.height);
		if (screenRatio > 1.5f) 
			returnMe.x = (Screen.height/2) - (returnMe.z/2) + (offsetY * (Screen.height * 1.5f/screenRatio));
		
		Log.Debug("final offset: " + returnMe.w + "," + returnMe.x);
		
		
		return returnMe;
	}
	
	IEnumerator JoinTopicOnServer()
	{
		string topicId 		= Globals.viewingTopic.topicId.ToString();
		string voteItems 	= Items.itemListToString(Globals.mainPlayer.wearingItems);
		string isSpecial 	= Globals.viewingTopic.isSpecial.ToString();
		
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + topicId + voteItems + isSpecial;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("JoinTopics", checkSumData);	

		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("topicId", topicId);
		kvp.Add("voteItems", voteItems);
		kvp.Add("isSpecial", isSpecial);
		kvp.Add("checkSum", checkSum);
	
		string url = Globals.serverAPIURL + "JoinTopics.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response 			= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			//yield return StartCoroutine(HTTPPost.Post("GetFriends", kvp, result => response = result));
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					//Log.Debug("nani C0?");
					//Hide();
					//Log.Debug("nani C1?");
					//GameObject.Find("StatusLabel").GetComponent<UISysFontLabel>().Text = "You have successfully joined the contest";
					PopupManager.HideLoadingFlower();
					//transform.GetComponent<SceneManager>().GoToScene();
					PopupManager.ShowKeepLookPopup(false);
					//SceneManager.SharedManager.LoadScene((int)Scenes.VOTINGLOBBY, true);
					//StartCoroutine(GameObject.Find("Init").GetComponent<ScreenCapturer>().Capture());
				}
				else
				{
					//Hide ();
					PopupManager.ShowInformationPopup("Cannot join topic", false);
					
					captureLock = false;
					PopupManager.HideLoadingFlower();
					GameObject.Find("TestPic").GetComponent<UITexture>().material.mainTexture = null;
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("JoinTopics", kvp, result => response = result));	
	}
	
	public void GoToStreetScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.STREET, true);	
	}
}


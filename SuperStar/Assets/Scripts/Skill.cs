using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Skill
{	
	public readonly static int skillTypes = 5;
	
	// better put to globals?
	public static string[] skillNames = new string[5]{"Tennis Player", "Dancer", "","", "Singer"};
	
	public int[] skill = new int[skillTypes];
	
	
	public Skill(int[] s)
	{
		
		
		// should be a secure way of copying
		for (int i = 0; i < skillTypes; i++)
		{
			skill[i] = s[i];
		}
	}
	
	
	// parse a skill string
	public Skill(string source)
	{
		string[] s = source.TrimStart('{').TrimEnd('}').Split('|');
		
		if (s.Length == skillTypes)
		{
			foreach (string ss in s)
			{
				string[] sss = ss.Split(':');
				
				int key= 1, val=1;
				bool success;
				success = Int32.TryParse(sss[0],out key) && Int32.TryParse(sss[1],out val);
				
				
				
				if (success && key < skillTypes)
				{
					skill[key] = val;
				}
				
			}
		}
	}
	
	public static string GetSkillAsString(Skill k)
	{
		string returnMe = "{";
		for (int i = 0; i < skillTypes; i++)
		{
			returnMe += (i +":"+k.skill[i] );
			
			if (i < skillTypes-1)
				returnMe += "|";
		}
		returnMe += "}";
		
		return returnMe;
	}
	
}


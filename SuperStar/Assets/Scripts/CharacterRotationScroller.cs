using UnityEngine;
using System.Collections;

public class CharacterRotationScroller : MonoBehaviour
{
	public GameObject rotationTarget;
	UIDraggablePanel mainPanel;
	
	Vector3 targetAngles;
	
	//public float speed = 0.2f;
	
	// Use this for initialization
	void Start ()
	{
		mainPanel = transform.GetComponent<UIDraggablePanel>();
		mainPanel.onDragFinished += Normalize;
		rotationTarget = GameObject.Find("GirlParent");
	}
	
	// Update is called once per frame
	void Update ()
	{
		targetAngles = rotationTarget.transform.localEulerAngles;
		targetAngles.y = transform.localPosition.x * -1f;
		rotationTarget.transform.localEulerAngles = targetAngles;
	}

	public void MakeUndraggable()
	{
		transform.GetComponentInChildren<BoxCollider>().size = Vector3.zero;
	}
	
	void Normalize()
	{
		//Log.Debug("normalizing");
		
		Vector3 t;
		while (transform.localPosition.x > 360f)
		{
			t = transform.localPosition;
			t.x -= 360f;
			transform.localPosition = t;
		}
		
		while (transform.localPosition.x < -360f)
		{
			t = transform.localPosition;
			t.x += 360f;
			transform.localPosition = t;
		}
	}
}


using System;

public class SkillTier
{
	public enum STARCOLOR
	{
		BRONZE,
		SLIVER,
		GOLD,
		DIAMOND
	}
	
	public SkillTier(int pSkillTierId, int pSkillType, int pSkillAmount, STARCOLOR pColor, int pStars, string pSkillTierString)
	{
		this.skillTierId = pSkillTierId;
		this.skillType = pSkillType;
		this.skillAmount = pSkillAmount;
		this.skillColor = pColor;
		this.skillStars = pStars;
		this.skillTierString = pSkillTierString;
	}
	
	public int skillTierId;
	public int skillType;
	public int skillAmount;
	public STARCOLOR skillColor;
	public int skillStars;
	public string skillTierString;
}



using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class LocalNotification
{
#if UNITY_ANDROID
	public const string LocalNotificationBridgeClass = "muneris.unity.androidbridge.localNotification.LocalNotificationBridge";
#elif UNITY_IPHONE
	private static UnityEngine.LocalNotification iosNotification 	= new UnityEngine.LocalNotification();
#endif
	
	private static int notifyId 									= 1;
	private static ArrayList scheduleNotIdsList 					= new ArrayList();
	
	public ArrayList GetScheduledNotificationIds()
	{
		return scheduleNotIdsList;	
	}

	public static void SetTitle(string title)
	{
		Log.Debug("[LocalNotification] SetMessage " + title);
		#if UNITY_ANDROID
		new AndroidJavaClass(LocalNotificationBridgeClass).CallStatic("SetTitle", title);
		#endif
	}
	
	public static void SetFireDate(DateTime date)
	{
		Log.Debug("[LocalNotification] SetFireDate " + date.ToString());		
		
		string fireDateStr = date.ToString("yyyy-MM-dd HH:mm:ss");
		#if UNITY_ANDROID
			new AndroidJavaClass(LocalNotificationBridgeClass).CallStatic("SetFireDate", fireDateStr);
		#elif UNITY_IPHONE
			iosNotification.fireDate = date;
		#endif
	}
	
	public static void SetId(int id)
	{
		Log.Debug("[LocalNotification] SetId " + id);		
		notifyId = id;
		#if UNITY_ANDROID
			new AndroidJavaClass(LocalNotificationBridgeClass).CallStatic("SetId", id);
		#elif UNITY_IPHONE
			Dictionary<string, string> userInfo = new Dictionary<string, string>();
			userInfo["notifyId"] 				= id+"";
			iosNotification.userInfo 			= userInfo;
		#endif
	}
	
	public static void CancelNotification(int id)
	{
		Log.Debug("[LocalNotification] CancelNotification " + id);
		
		if (scheduleNotIdsList.Contains(id))
			scheduleNotIdsList.Remove(id);
		
		#if UNITY_ANDROID
			new AndroidJavaClass(LocalNotificationBridgeClass).CallStatic("CancelNotification", id);
		#elif UNITY_IPHONE
			UnityEngine.LocalNotification[] scheduledNotifications = NotificationServices.scheduledLocalNotifications;
			for (int i = 0; i < scheduledNotifications.Length; i++)
			{
				UnityEngine.LocalNotification notify 	= scheduledNotifications[i];
				Dictionary<string, string> userInfo		= (Dictionary<string, string>)notify.userInfo;
				int notifyId							= int.Parse(userInfo["notifyId"]);
				
				if (notifyId == id)
				{
					NotificationServices.CancelLocalNotification(notify);
					break;
				}
			}
		#endif
	}
	
	public static void SetMessage(string message)
	{
		Log.Debug("[LocalNotification] SetMessage " + message);		
		#if UNITY_ANDROID
			new AndroidJavaClass(LocalNotificationBridgeClass).CallStatic("SetMessage", message);
		#elif UNITY_IPHONE
			iosNotification.alertBody = message;
		#endif
	}
	
	public static void CancelAllNotification()
	{
		Log.Debug("[LocalNotification] CancelAllNotification");
		
		#if UNITY_ANDROID
			for (int i = 0; i < scheduleNotIdsList.Count; i++)
			{
				int scheduleId = (int)scheduleNotIdsList[i];
				
				new AndroidJavaClass(LocalNotificationBridgeClass).CallStatic("CancelNotification", scheduleId);
			}
		#elif UNITY_IPHONE
			NotificationServices.CancelAllLocalNotifications();
		#endif

		if (scheduleNotIdsList != null && scheduleNotIdsList.Count > 0)
			scheduleNotIdsList.Clear();
	}
	
	public static void ScheduleNotification()
	{
		Log.Debug("[LocalNotification] ScheduleNotification");
		if (!scheduleNotIdsList.Contains(notifyId))
			scheduleNotIdsList.Add(notifyId);
		
		#if UNITY_ANDROID
			new AndroidJavaClass(LocalNotificationBridgeClass).CallStatic("ScheduleNotification");
		#elif UNITY_IPHONE
			NotificationServices.ScheduleLocalNotification(iosNotification);
		#endif
	}
}

using System;
using UnityEngine;
using System.Collections.Generic;


public class DailyBonus
{
	public static readonly int cycleLength = 5;

	private static List<Reward> bonusList = new List<Reward>()
	{
		new Reward(999, -1, 1000,  0, 0, 0, "", new Skill(new int[]{0,0,0,0,0})),
		new Reward(999, -1, 2000,  0, 0, 0, "", new Skill(new int[]{0,0,0,0,0})),
		new Reward(999, -1, 3000,  0, 0, 0, "", new Skill(new int[]{0,0,0,0,0})),
		new Reward(999, -1, 4000,  0, 0, 0, "", new Skill(new int[]{0,0,0,0,0})),
		new Reward(999, -1,    0, 20, 0, 0, "", new Skill(new int[]{0,0,0,0,0}))
	};

	public static Reward getRewardOfDay(int d)
	{
		return bonusList[(d-1)%cycleLength];
	}
	

}
	
	



using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DLCPackage
{
	public int packageId;
	public string platform;
	public string hashValue;
	public string url;
	public int dlcId;

	public DLCPackage(int _packageId, string _platform, string _hashValue, string _url, int _dlcId)
	{
		this.packageId 	= _packageId;
		this.platform	= _platform;
		this.hashValue	= _hashValue;
		this.url		= _url;
		this.dlcId		= _dlcId;
	}

	public DLCPackage(int _packageId)
	{
		this.packageId 	= _packageId;
	}
}

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class Player
{
	// empty constructor for myself
	public Player()
	{
		playerId = -1;
		gameId = "";
		playerName = "Andy";
		profilePicURL = "";
	}
	
	// partial constructor for friends
	public Player(int pPlayerId, string pGameId, string pName, int pBfId, string pOwningItems, string pWearingItems, string pProfilePicUrl, Skill pSkill, int pTitleSkill)	
	{
		playerId = pPlayerId;
		gameId = pGameId;
		playerName = pName;
		currentBoyfriendId = pBfId;
		profilePicURL = pProfilePicUrl;
		setSkill(pSkill);
		titleSkill = pTitleSkill;
		
		owningItems = Items.stringToItemList(pOwningItems);
		wearingItems = Items.stringToItemList(pWearingItems);
	}
	
	public int playerId = -1;
	public int PlayerId
	{
		set{playerId = value; /*Save();*/}
		get{return playerId;}
	}

	private string playerName;
	public  string Name
	{
		set{playerName = value; /*Save();*/}
		get{return playerName;}
	}
	
	//private  int level;
	public int Level
	{
		set{//nth
		}
		get{return getLevelByExp();}
	}
	
	private  int exp;
	public  int Exp
	{
		set{exp = value;}
		get{return exp;}
	}
	
	private  int energy;
	public  int Energy
	{
		set{energy = value;}
		get{return energy;}
	}
	
	private  int maxEnergy;
	public  int MaxEnergy
	{
		set{maxEnergy = value;}
		get{return maxEnergy;}
	}
	
	private  int energyTimer;
	public  int EnergyTimer
	{
		set{energyTimer = value;}
		get{return energyTimer;}
	}
	
	private  int energyInterval;
	public  int EnergyInterval
	{
		set{energyInterval = value;}
		get{return energyInterval;}
	}
	
	
	private  int coins;
	public  int Coins
	{
		set{coins = value;}
		get{return coins;}
	}
	
	private  int gems;
	public  int Gems
	{
		set{gems = value;}
		get{return gems;}
	}
	
	private string gameId;
	public string GameId
	{
		set{gameId = value;}
		get{return gameId;}
	}
	
	private  string profilePicURL;
	public  string ProfilePicURL
	{
		set{profilePicURL = value;}
		get{return profilePicURL;}
	}
	
	private int currentBoyfriendId;
	public int CurrentBoyfriendId
	{
		set{currentBoyfriendId = value;}
		get{return currentBoyfriendId;}
	}
	
	private int streetPosition;
	public int StreetPosition
	{
		set{streetPosition = value;}
		get{return streetPosition;}
	}
	
	private Vector2 minimapPosition;
	public Vector2 MinimapPosition
	{
		set {minimapPosition = value;}
		get {return minimapPosition;}
	}
	
	private string lastSyncDate;
	public string LastSyncDate
	{
		set{lastSyncDate = value;}
		get{return lastSyncDate;}
	}
	
	private bool initializedPlayer = false;
	public bool InitializedPlayer
	{
		set {initializedPlayer = value;}
		get {return initializedPlayer;}
	}


	
	private int numOfSessions = 0;
	public int NumOfSessions
	{
		set {numOfSessions = value;}
		get {return numOfSessions;}
	}
	
	private int clothMatchTotalScore;
	public int ClothMatchTotalScore
	{
		set{clothMatchTotalScore = value;}
		get{return clothMatchTotalScore;}
	}
	
	private int clothMatchHighScore;
	public int ClothMatchHighScore
	{
		set{clothMatchHighScore = value;}
		get{return clothMatchHighScore;}
	}
	
	private int rougeAlpha = 127;
	public int RougeAlpha
	{
		set{rougeAlpha = value;}
		get{return rougeAlpha;}
	}
	
	private int eyeShadowAlpha = 127;
	public int EyeShadowAlpha
	{
		set{eyeShadowAlpha = value;}
		get{return eyeShadowAlpha;}
	}

	private int playerDLCVersion;
	public int PlayerDLCVersion
	{
		set{playerDLCVersion = value;}
		get{return playerDLCVersion;}
	}

	private DateTime dailyBonusStamp;
	public DateTime DailyBonusStamp
	{
		set{dailyBonusStamp = value;}
		get{return dailyBonusStamp;}
	}

	private int dailyBonusDay;
	public int DailyBonusDay
	{
		set{dailyBonusDay = value;}
		get{return dailyBonusDay;}
	}

	private bool scrolledQuest = false;
	public bool ScrolledQuest
	{
		set {scrolledQuest = value;}
		get {return scrolledQuest;}
	}

	private  Skill skillTable = new Skill(new int[] {1,0,0,0,0});
	public  Skill getSkill()
	{
		return skillTable;
	}
	
	public  void setSkill(Skill st)
	{
		skillTable = st;
	}
	public  void setSkill(int[] st)
	{
		for (int i = 0; i < st.Length; i++)
		skillTable.skill[i] = st[i];
	}
	
	private int titleSkill = 0;
	public int TitleSkill
	{
		get {return titleSkill;}
		set {titleSkill = value;}
	}
	
	
	public SocialElement facebookElement = new SocialElement(SocialElement.TYPE.FACEBOOK);
	public SocialElement googlePlusElement = new SocialElement(SocialElement.TYPE.GOOGLEPLUS);
	public SocialElement twitterElement = new SocialElement(SocialElement.TYPE.TWITTER);
	
	/*public string defaultOwningItems = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,19,20,52," +
		"21,22,23,24,25,26,27,28,29,30" + 
	    "54,55,56,57,58,59,60,61," +
		"32,34,36,38,40,41,43,44,45,47,48,49,50,"+
		"71,72,73,74,75,78,79,80," +//Eye ball
		"95,"+//dress
		"107,108,109,110,111,112,113,"+ // rouge
		"114,115,116,117,118," +// skin color
		"119,120,121,122,123,124,125,126," + // lips color	
		"127,128,129,130,131,132,133,134,135,136," +  // eyeshawdow color
		"137,138,139,140,141,142,143,144,145,146,147" +  // lips
		"148,149,150,151,152,153,154,155" +  // eye shape
		"156,157,158,159,160,161,162,163" +  // eyebrows
		""	; */
	//public string defaultWearingItems = "34,1,21,71,95,107,114,119,127, 137, 148,156";
	public List<Item> owningItems = new List<Item>();
	public List<Item> wearingItems = new List<Item>();
	
	//public string owningItemsString;
	//public string wearingItemsString;
	
	
	public int votingRank;
	public int votingVotes;
	
	public DateTime timeStamp = DateTime.UtcNow;
	
	public bool isWearingItem(Item item)
	{
		foreach (Item i in wearingItems)
		{
			if (i.itemId == item.itemId)
				return true;
		}
		return false;
	}
	
	public Item wearingItemOfSubcategory(ItemSubcategory subcat)
	{
		foreach (Item i in wearingItems)
		{
			if (i.subCategoryId == subcat.subcategoryid)
				return i;
		}
		
		return null;
	}
	
	public void wearItem(Item item)
	{
		// remove cloth of same subcategory first
		unwearItem(wearingItemOfSubcategory(ItemSubcategories.getSubcategory(item.subCategoryId)));
		
		if (item != null)
		{
			wearingItems.Add(item);
			
			// check conflict
			ItemConflicts.Attack(item,this);
		}
		
		//Save();
	}
	
	public void unwearItem(Item item)
	{
		if (item != null)		
		{
			List<Item> copyWearingItems = new List<Item>();
			
			foreach (Item i in wearingItems)
			{
				if (i.itemId != item.itemId)
				{
					copyWearingItems.Add(i);
				}
			}
			
			wearingItems = copyWearingItems;
		}
	}

	// ====================================================================================================
	
	public void Save()
	{
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		DatabaseManager.SharedManager.Save("UPDATE Players SET "+
			"  playerName = '"+ Name + "'" +
			", exp = " + Exp +
			", energy = " + Energy +
			", maxEnergy = " + MaxEnergy +
			", coins = " + Coins +
			", gems = " + Gems +
			", profilePicURL = '" + ProfilePicURL + "'" +
			", boyfriendId = " + CurrentBoyfriendId +
			", skills = '" + Skill.GetSkillAsString(skillTable) + "'" + 
			", gameId = '" + GameId + "'" +
			", titleSkill = " + TitleSkill +
			" WHERE playerId = " + PlayerId );
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ ((InitializedPlayer)? "1": "0") +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'initializedPlayer'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ NumOfSessions +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'numOfSessions'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ EnergyTimer+
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'energyTimer'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ EnergyInterval +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'energyInterval'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ StreetPosition +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'streetPosition'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+ (int)MinimapPosition.x + "|" + (int)MinimapPosition.y + "'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'minimapPosition'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ Globals.currentDLCVersion +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'dlcVersion'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ Globals.dlcProgress +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'dlcProgress'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+ Globals.startPlayTime +"'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'startPlayTime'");

		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
           "  strValue = '"+ Globals.isCracked.ToString() +"'" +
           " WHERE playerId = " + PlayerId +
		   " AND metaKey = 'isCracked'");

		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+ Globals.isIAP.ToString() +"'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'isIAP'");

		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
           "  strValue = '"+ Globals.hasFirstDLC.ToString() +"'" +
           " WHERE playerId = " + PlayerId +
		   " AND metaKey = 'hasFirstDLC'");

		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
		                                   "  strValue = '"+ ScrolledQuest.ToString() +"'" +
		                                   " WHERE playerId = " + PlayerId +
		                                   " AND metaKey = 'scrolledQuest'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+ Globals.iapRecord +"'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'iapRecord'");

		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
		   "  strValue = '"+ Globals.bundleVersion +"'" +
		   " WHERE playerId = " + PlayerId +
		   " AND metaKey = 'bundleVersion'");
		
		
		timeStamp = DateTime.UtcNow;
		DateTime now = DateTime.UtcNow;
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+ (now.Year+"-"+now.Month+"-"+now.Day+"-"+now.Hour+"-"+now.Minute+"-"+now.Second) + "'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'timeStamp'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+VisitRecord.GetRecordsAsString()+"'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'visitedGameIds'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ Globals.mainPlayer.ClothMatchTotalScore +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'clothMatchTotalScore'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ Globals.mainPlayer.ClothMatchHighScore +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'clothMatchHighScore'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ Globals.mainPlayer.RougeAlpha +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'rougeAlpha'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  intValue = "+ Globals.mainPlayer.EyeShadowAlpha +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'eyeShadowAlpha'");

		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
           "  intValue = "+ Globals.mainPlayer.PlayerDLCVersion +
           " WHERE playerId = " + PlayerId +
           " AND metaKey = 'playerDLCVersion'");
		
		
		string concat = "";
		foreach (Quest q in Quests.currentQuests)
		{
			if (!concat.Equals("")) concat += ",";
			
			concat += ""+q.activityId;
		}
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+  concat+"'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'currentQuestIds'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+ lastSyncDate + "'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'lastSyncDate'");

		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
		                                   "  strValue = '"+ (	DailyBonusStamp.Year+"-"+
		    													DailyBonusStamp.Month+"-"+
		    													DailyBonusStamp.Day+"-"+
		    													DailyBonusStamp.Hour+"-"+
		    													DailyBonusStamp.Minute+"-"+
		    													DailyBonusStamp.Second) + "'" +
		                                   " WHERE playerId = " + PlayerId +
		                                   " AND metaKey = 'dailyBonusStamp'");

		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
		                                   "  intValue = '"+ DailyBonusDay + "'" +
		                                   " WHERE playerId = " + PlayerId +
		                                   " AND metaKey = 'dailyBonusDay'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+ Globals.settings_bgm.ToString() + "'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'settings_bgm'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+ Globals.settings_sfx.ToString() + "'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'settings_sfx'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+ Globals.settings_notification.ToString() + "'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'settings_notification'");

		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
            "  strValue = '"+ Globals.canShowRatePopup + "'" +
            " WHERE playerId = " + PlayerId +
            " AND metaKey = 'canShowRatePopup'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
			"  strValue = '"+ Globals.langCode + "'" +
			" WHERE playerId = " + PlayerId +
			" AND metaKey = 'settings_langCode'");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayersToItems SET "+
			"  purchasedItems = '"+ Items.itemListToString(owningItems) + "'" +
			" ,wearingItems = '"+ Items.itemListToString(wearingItems) + "'" +
			" WHERE playerId = " + PlayerId);
		
		DatabaseManager.SharedManager.Save("UPDATE PlayersToSocials SET "+
			"  socialId = '" + facebookElement.socialId + "'" + 
			"  ,appearName = '"+ facebookElement.appearName + "'" +
			" WHERE playerId = " + PlayerId + " AND socialType = 'FACEBOOK' ");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayersToSocials SET "+
			"  socialId = '" + googlePlusElement.socialId + "'" + 
			"  ,appearName = '"+ googlePlusElement.appearName + "'" +
			" WHERE playerId = " + PlayerId + " AND socialType = 'GOOGLEPLUS' ");
		
		DatabaseManager.SharedManager.Save("UPDATE PlayersToSocials SET "+
			"  socialId = '" + twitterElement.socialId + "'" + 
			"  ,appearName = '"+ twitterElement.appearName + "'" +
			"  ,loginName = '" + twitterElement.loginName + "'" +
			"  ,loginPassword = '" + twitterElement.loginPassword + "'" +
			" WHERE playerId = " + PlayerId + " AND socialType = 'TWITTER' ");
		
	}
	
	public void Load()
	{
		SQLiteQuery qr;
		string querySelect =
			"SELECT * FROM Players WHERE playerId = " + playerId ;

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			// use private variables to prevent save again
			playerId = qr.GetInteger("playerId");
			playerName = qr.GetString("playerName");
			exp = qr.GetInteger("exp");
			energy = qr.GetInteger("energy");
			maxEnergy = qr.GetInteger("maxEnergy");
			coins = qr.GetInteger("coins");
			gems = qr.GetInteger("gems");
			currentBoyfriendId = qr.GetInteger("boyfriendId");
			gameId = qr.GetString("gameId");
			setSkill(new Skill(qr.GetString("skills")));
			profilePicURL = qr.GetString("profilePicURL");
			titleSkill = qr.GetInteger("titleSkill");

			DatabaseManager.SharedManager.cacher.setCoins(coins);
			DatabaseManager.SharedManager.cacher.setExp(exp);
			DatabaseManager.SharedManager.cacher.setEnergy(energy);
			DatabaseManager.SharedManager.cacher.setGems(gems);
			DatabaseManager.SharedManager.cacher.setMaxEnergy(maxEnergy);

		}
			
		qr.Release();
		
		// also load meta?
		querySelect =
			"SELECT * FROM PlayerMeta WHERE playerId = " + playerId ;

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			string metaKey = qr.GetString("metaKey");			
			
			if (metaKey.Equals("energyTimer"))
			{
				energyTimer = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("energyInterval"))
			{
				energyInterval = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("streetPosition"))
			{
				streetPosition = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("minimapPosition"))
			{
				minimapPosition = new Vector2(1.0f * Int32.Parse(qr.GetString("strValue").Split('|')[0]),
					1.0f * Int32.Parse(qr.GetString("strValue").Split('|')[1]));
			}
			else if (metaKey.Equals("dlcVersion"))
			{
				Globals.currentDLCVersion = qr.GetInteger("intValue");
				Globals.apparentDLCVersion = Globals.currentDLCVersion;
			}
			else if (metaKey.Equals("dlcProgress"))
			{
				Globals.dlcProgress = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("timeStamp"))
			{
				string[] lastStamp = qr.GetString("strValue").Split('-');
				timeStamp = new DateTime(
					Int32.Parse(lastStamp[0]),
					Int32.Parse(lastStamp[1]),
					Int32.Parse(lastStamp[2]),
					Int32.Parse(lastStamp[3]),
					Int32.Parse(lastStamp[4]),
					Int32.Parse(lastStamp[5])
				);
			}
			else if (metaKey.Equals("dailyBonusDay"))
			{
				dailyBonusDay = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("dailyBonusStamp"))
			{
				string[] lastStamp = qr.GetString("strValue").Split('-');
				dailyBonusStamp = new DateTime(
					Int32.Parse(lastStamp[0]),
					Int32.Parse(lastStamp[1]),
					Int32.Parse(lastStamp[2]),
					Int32.Parse(lastStamp[3]),
					Int32.Parse(lastStamp[4]),
					Int32.Parse(lastStamp[5])
					);
			}
			else if (metaKey.Equals("visitedGameIds"))
			{
				if (!qr.GetString("strValue").Equals(""))
				{
					// backward compability for visit game id (android)
					if (qr.GetString("strValue").Contains("-"))
					{

						Globals.visitedGameIds.Clear();
						string[] all = qr.GetString("strValue").Split('|');

						foreach (string single in all)
						{
							string[] singleArr = single.Split(',');
							string[] dateArr = singleArr[1].Split('-');

							Globals.visitedGameIds.Add(new VisitRecord(singleArr[0], new DateTime(
								Int32.Parse(dateArr[0]),
								Int32.Parse(dateArr[1]),
								Int32.Parse(dateArr[2]),
								Int32.Parse(dateArr[3]),
								Int32.Parse(dateArr[4]),
								Int32.Parse(dateArr[5]))));
						}
					}
					else
					{
						Globals.visitedGameIds = new List<VisitRecord>();
					}
				}
				else
					Globals.visitedGameIds = new List<VisitRecord>();
			}			
			else if (metaKey.Equals("currentQuestIds"))
			{
				if (!qr.GetString("strValue").Equals(""))
				{
					string[] ids = qr.GetString("strValue").Split(',');
					
					Quests.currentQuests = new List<Quest>();
					
					foreach (string s in ids)
					{
						Quests.currentQuests.Add(Quests.GetQuest(Int32.Parse(s)));
					}				
				}
				else
				{
					Log.Debug("quest error");
					Quests.RefreshQuestList();
					//Quests.currentQuests = new List<Quest>();
				}
			}			
			else if (metaKey.Equals("settings_bgm"))
			{
				Globals.settings_bgm = Boolean.Parse(qr.GetString("strValue"));
			}			
			else if (metaKey.Equals("settings_sfx"))
			{
				Globals.settings_sfx = Boolean.Parse(qr.GetString("strValue"));
			}
			else if (metaKey.Equals("settings_notification"))
			{
				Globals.settings_notification = Boolean.Parse(qr.GetString("strValue"));
			}
			else if (metaKey.Equals("canShowRatePopup"))
			{
				Globals.canShowRatePopup = Boolean.Parse(qr.GetString("strValue"));
			}			
			else if (metaKey.Equals("settings_langCode"))
			{
				Globals.langCode = qr.GetString("strValue");
			}
			else if (metaKey.Equals("lastSyncDate"))
			{
				lastSyncDate = qr.GetString("strValue");
			}
			else if (metaKey.Equals("initializedPlayer"))
			{
				initializedPlayer = (qr.GetInteger("intValue") == 1);
			}
			else if (metaKey.Equals("scrolledQuest"))
			{
				scrolledQuest = Boolean.Parse(qr.GetString("strValue"));
			}
			else if (metaKey.Equals("numOfSessions"))
			{
				numOfSessions = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("clothMatchTotalScore"))
			{
				clothMatchTotalScore = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("clothMatchHighScore"))
			{
				clothMatchHighScore = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("rougeAlpha"))
			{
				rougeAlpha = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("eyeShadowAlpha"))
			{
				eyeShadowAlpha = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("playerDLCVersion"))
			{
				playerDLCVersion = qr.GetInteger("intValue");
			}
			else if (metaKey.Equals("startPlayTime"))
			{
				Globals.startPlayTime = qr.GetString("strValue");
			}
			else if (metaKey.Equals("isCracked"))
			{
				Globals.isCracked = Boolean.Parse(qr.GetString("strValue"));
			}
			else if (metaKey.Equals("isIAP"))
			{
				Globals.isIAP = Boolean.Parse(qr.GetString("strValue"));
			}
			else if (metaKey.Equals("hasFirstDLC"))
			{
				Globals.hasFirstDLC = Boolean.Parse(qr.GetString("strValue"));
			}
			else if (metaKey.Equals("iapRecord"))
			{
				Globals.iapRecord = qr.GetString("strValue");	
			}
			else if (metaKey.Equals("bundleVersion"))
			{
				Globals.bundleVersion = qr.GetString("strValue");
			}
		}
			
		qr.Release();
		
		// also load items
		
		owningItems.Clear();
		wearingItems.Clear();
		querySelect =
			"SELECT * FROM PlayersToItems WHERE playerId = " + playerId ;

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
//			Log.Debug("loading of player Id: " + playerId + ", purchased items: "+qr.GetString("purchasedItems"));
			
			owningItems = Items.stringToItemList(qr.GetString("purchasedItems"));
			
			wearingItems = Items.stringToItemList(qr.GetString("wearingItems"));
			
		}
			
		qr.Release();
		
		querySelect =
			"SELECT * FROM PlayersToSocials WHERE playerId = " + playerId ;

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			if (qr.GetString("socialType").Equals("FACEBOOK"))
			{
				facebookElement.socialId = qr.GetString("socialId");
				facebookElement.appearName = qr.GetString("appearName");
			}
			if (qr.GetString("socialType").Equals("GOOGLEPLUS"))
			{
				googlePlusElement.socialId = qr.GetString("socialId");
				googlePlusElement.appearName = qr.GetString("appearName");
			}
			if (qr.GetString("socialType").Equals("TWITTER"))
			{
				twitterElement.socialId = qr.GetString("socialId");
				twitterElement.appearName = qr.GetString("appearName");
				
				try
				{
					twitterElement.loginName = qr.GetString("loginName");
					twitterElement.loginPassword = qr.GetString("loginPassword");
				}
				catch(Exception e)
				{
					Log.Debug("No field for loginName or loginPassword");	
				}
			}
			
		}
			
		qr.Release();
	}
	
		
	// ====================================================================================================
	
	
	
	public int getLevelByExp()
	{
		return getLevelByExp(Globals.mainPlayer.Exp);
	}
	
	public int getLevelByExp(int e)
	{
		for (int i = 1; i <= ExpTable.expTable.Count-2; i++)
		{
			if (e < ExpTable.expTable[i+1])
				return i;
		}
		
		return ExpTable.expTable.Count-1;
	}
	
	public void GainExp(int increment)
	{
		if (increment == 0) return;
		
		if (Globals.mainPlayer.Level >= ExpTable.expTable.Count-1) return;
		
		int previousLevel = Globals.mainPlayer.Level;


		if (!DatabaseManager.SharedManager.cacher.verifyExp(Globals.mainPlayer.Exp))
		{
			BanAction();
			return;
		}

		Globals.mainPlayer.Exp += increment;

		DatabaseManager.SharedManager.cacher.setExp(Globals.mainPlayer.Exp);
		
		if (Globals.mainPlayer.Level > previousLevel)
		{
			PopupManager.ShowLevelUpPopup(new Reward(0,1,0,10,0,0,"",new Skill(new int[]{0,0,0,0,0})), false);
			
			//Flurry Event
			Hashtable param = new Hashtable();
			param["Level"] 	= Globals.mainPlayer.Level;
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Current_Level, param);
		}		

		GameObject tmmGO = GameObject.Find("TopMenu");
		if (tmmGO != null)
		{
			TopMenuManager tmm = tmmGO.GetComponent<TopMenuManager>();
			if (tmm != null)
				tmm.GainExpAnimation(increment);
		}
		
		int currentLevel = Globals.mainPlayer.Level;
		if (currentLevel == 2)
			MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.ReachLevel2);
		else if (currentLevel == 3)
			MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.ReachLevel3);
	}
	
	public  void GainEnergy(int increment)
	{
		GainEnergy(increment, true);
	}
	
	public  void GainEnergy(int increment, bool effected)
	{
		if (increment == 0) return;
		
		//Log.Debug("energy/max/increment " + Globals.mainPlayer.Energy + "," + Globals.mainPlayer.MaxEnergy + "," + increment);
		
		if (Globals.mainPlayer.Energy == Globals.mainPlayer.MaxEnergy)
			Globals.mainPlayer.EnergyTimer = Globals.mainPlayer.EnergyInterval;
		
		// bounds energy amount to [0,MaxEnergy]
		int boundedAmount = Mathf.Min(Mathf.Max(0, Globals.mainPlayer.Energy + increment), Globals.mainPlayer.MaxEnergy);
		//Log.Debug("increment " + increment);
		
		
		
		
		if (!DatabaseManager.SharedManager.cacher.verifyEnergy(Globals.mainPlayer.Energy))
		{
			BanAction();
			return;
		}
		if (!DatabaseManager.SharedManager.cacher.verifyMaxEnergy(Globals.mainPlayer.MaxEnergy))
		{
			BanAction();
			return;
		}


		Globals.mainPlayer.Energy = boundedAmount;

		DatabaseManager.SharedManager.cacher.setEnergy(Globals.mainPlayer.energy);
		
		if (effected)
		{
			GameObject tmmGO = GameObject.Find("TopMenu");
			if (tmmGO != null)
			{
				TopMenuManager tmm = tmmGO.GetComponent<TopMenuManager>();
				if (tmm != null)
					tmm.GainEnergyAnimation(increment);
			}
		}
	}
	
	public  void GainCoins(int increment)
	{
		if (increment == 0) return;

		if (!DatabaseManager.SharedManager.cacher.verifyCoins(Globals.mainPlayer.Coins))
		{
			BanAction();
			return;
		}

		Globals.mainPlayer.Coins += increment;

		DatabaseManager.SharedManager.cacher.setCoins(Globals.mainPlayer.Coins);
		
		GameObject tmmGO = GameObject.Find("TopMenu");
		if (tmmGO != null)
		{
			TopMenuManager tmm = tmmGO.GetComponent<TopMenuManager>();
			if (tmm != null)
				tmm.GainCoinsAnimation(increment);
		}
	}
	
	public  void GainGems(int increment)
	{
		if (increment == 0) return;

		if (!DatabaseManager.SharedManager.cacher.verifyGems(Globals.mainPlayer.Gems))
		{
			BanAction();
			return;
		}

		Globals.mainPlayer.Gems += increment;

		DatabaseManager.SharedManager.cacher.setGems(Globals.mainPlayer.Gems);

		
		GameObject tmmGO = GameObject.Find("TopMenu");
		if (tmmGO != null)
		{
			TopMenuManager tmm = tmmGO.GetComponent<TopMenuManager>();
			if (tmm != null)
				tmm.GainGemsAnimation(increment);
		}
	}

	public void BanAction()
	{
		PopupManager.ShowInformationPopup("BAN 9 YOU!",false);
	}
	
	
}



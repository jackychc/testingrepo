using System;


public class SocialElement
{
	public enum TYPE
	{
		FACEBOOK,
		GOOGLEPLUS,
		TWITTER
	}
	public TYPE socialType;
	public string socialId;
	public string appearName;
	
	public string loginName;
	public string loginPassword;
	
	public SocialElement(SocialElement.TYPE t)
	{
		socialType = t;
	}
}



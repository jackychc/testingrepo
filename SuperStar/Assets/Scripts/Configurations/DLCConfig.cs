using System.Collections;
using UnityEngine;
using System;
using System.Collections.Generic;

public class DLCConfigs
{
	public static List <DLCConfig> allDlcConfigs = new List<DLCConfig>();
	
	public static void Save()
	{
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		//DatabaseManager.db.Open(Globals.dbSavePath);
		foreach (DLCConfig a in allDlcConfigs)
		{
			
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM Dlcs WHERE dlcId = '" + a.dlcId + "'",
				
				//update
				new List<string>(){
				"UPDATE Dlcs SET "+
				"  dlcVersion = " + a.dlcVersion + ","+
				"  dlcContent = '" + a.dlcContent.Replace("'", "DaSh0123456789") + "'" + "," +
				"  dlcLevel = " + a.dlcLevel + 
				" WHERE dlcId = " + a.dlcId
				},
			
				//insert
				new List<string>(){
					"INSERT INTO Dlcs (dlcId, dlcVersion, dlcContent, dlcLevel) VALUES ("+
					a.dlcId +
				" , '" + a.dlcVersion + "'"+
				" , '" + a.dlcContent.Replace("'", "DaSh0123456789") + "'"+
				" , " + a.dlcLevel +
				")"
				}
			);
		}
	}
	
	public static void Load()
	{
		allDlcConfigs.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM Dlcs";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			int dlcId				= int.Parse(qr.GetString("dlcId"));
			string dlcVersion		= qr.GetString("dlcVersion");
			string dlcContent 		= qr.GetString("dlcContent").Replace("DaSh0123456789", "'");
			int dlcLevel			= qr.GetInteger("dlcLevel");

			if (dlcContent != null && !dlcContent.Equals(""))
			{
				Hashtable dlcHT 	= (Hashtable) SuperstarFashionMiniJSON.JsonDecode(dlcContent);

				if (dlcHT != null)
				{
					if (dlcId > Globals.currentDLCVersion)
					{
						DLCConfig dlcc = new DLCConfig
						(
							Int32.Parse(dlcHT["dlcId"].ToString()),
							dlcHT["dlcVersion"].ToString(),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Items"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Rewards"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Jobs"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["ItemConflicts"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Shops"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Boyfriends"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Packages"])
						);		
						dlcc.isAppUpdate 	= (Int32.Parse(dlcHT["isAppUpdate"].ToString()) == 1);
						dlcc.dlcLevel		= dlcLevel;
						dlcc.dlcContent 	= dlcContent;
						allDlcConfigs.Add(dlcc);
					}
				}
			}
		}			
		qr.Release();
	}
		
	// TODO: app update dim suen?
	public static void ExecuteDLC()
	{
		//bool hasExecuteDLC = false;
		allDlcConfigs.Sort((x, y) => x.dlcId.CompareTo(y.dlcId));
		
		foreach (DLCConfig dlcc in allDlcConfigs)
		{
			if (dlcc.dlcId > Globals.currentDLCVersion)
			{
				
				if (!dlcc.isAppUpdate)
				{
					dlcc.ConvertToListInside();
				
					dlcc.Commit();
				}
				else
				{
					dlcc.CommitAppUpdate();
				}

				//hasExecuteDLC = true;
			}
		}

		//Globals.hasExecuteDLC = hasExecuteDLC;
	}

	public static void ExecuteDLC(int dlcId)
	{
		//bool hasExcuteDLC = false;
		for (int i = 0; i < allDlcConfigs.Count; i++)
		{
			DLCConfig dlcc = allDlcConfigs[i];

			if (dlcc.dlcId == dlcId)
			{
				Log.Debug("DLC Config ExecuteDLC with DLCId: " + dlcId);
				if (!dlcc.isAppUpdate)
				{
					dlcc.ConvertToListInside();
				
					dlcc.Commit();
				}
				else
				{
					dlcc.CommitAppUpdate();
				}

				//hasExcuteDLC = true;
			}
		}

		//Globals.hasExecuteDLC = hasExcuteDLC;
	}
}

public class DLCConfig
{
	public int dlcId 			= 0;
	public string dlcVersion 	= "";
	public string dlcContent 	= "";
	public bool isAppUpdate 	= false;
	public int dlcLevel 		= 0;
	public Hashtable itemsHT, rewardsHT, jobsHT, itemConflictsHT, shopsHT, boyfriendsHT, packagesHT;
	
	public List<Item> items 					= new List<Item>();
	public List<Reward> rewards 				= new List<Reward>();
	public List<Job> jobs 						= new List<Job>();
	public List<ItemConflictDB> itemConflictDBs = new List<ItemConflictDB>();
	public List<Shop> shops 					= new List<Shop>();
	public List<Boyfriend> boyfriends 			= new List<Boyfriend>();
	public List<DLCPackage> packages			= new List<DLCPackage>();
	
	int invalidCode = (int) -99999999;
	
	public DLCConfig(){}
	public DLCConfig (int pDlcId, string pDlcVersion, Hashtable pItemsHT, Hashtable pRewardsHT, Hashtable pJobsHT, Hashtable pItemConflictsHT, Hashtable pShopsHT, Hashtable pBoyfriendsHT, Hashtable pPackagesHT)
	{
		dlcId 			= pDlcId;
		dlcVersion 		= pDlcVersion;
		itemsHT 		= pItemsHT;
		rewardsHT 		= pRewardsHT;
		jobsHT 			= pJobsHT;
		itemConflictsHT = pItemConflictsHT;
		shopsHT 		= pShopsHT;
		boyfriendsHT 	= pBoyfriendsHT;
		packagesHT		= pPackagesHT;
	}
	
	public IEnumerator ConvertToList()
	{
		ConvertToListInside();
		
		yield return null;
	}
	
	public void ConvertToListInside()
	{
		
		if (itemsHT != null)
		{
			ArrayList itemsList =  new ArrayList(itemsHT.Values);
			
			foreach (Hashtable item in itemsList)
			{
				//Debug.Log("");
				
				Item itemObject = new Item(TryToParse(item["itemId"]));
				
				itemObject.itemName = item["itemName"] != null? item["itemName"].ToString() : "";
				itemObject.visibility = item["visibility"] != null? (item["visibility"].ToString().Equals("true")? true: false) : false;
				itemObject.subCategoryId = TryToParse(item["subcategoryId"]);
				itemObject.brandId = item["brandId"] != null? TryToParse(item["brandId"]) : 0;
				itemObject.price =  TryToParse( item["price"]);
				itemObject.gems =  TryToParse(item["gems"]);
				itemObject.requiredLevel = TryToParse(item["requiredLevel"]);
				itemObject.featureId =  (item["featureId"] != null? TryToParse(item["featureId"]) : 0);
					
				if (item["charisma"] != null && item["charisma"].ToString().Split(',').Length == 2)
				{
					itemObject.sexy = TryToParse((item["charisma"].ToString().Split(','))[0]);
					itemObject.lively = TryToParse((item["charisma"].ToString().Split(','))[1]);
				}
				else
				{
					itemObject.sexy = invalidCode;
					itemObject.lively = invalidCode;
				}
				
				itemObject.rewardId = -1;
				itemObject.dlcId =	TryToParse(item["dlcId"].ToString());
				itemObject.meta = item["meta"] != null? item["meta"].ToString() : "";
				itemObject.texMeta = item["texMeta"] != null? item["texMeta"].ToString() : "";
				itemObject.colorMeta = item["colorMeta"] != null? item["colorMeta"].ToString() : "";
				
				if (item["itemPackages"] != null)
				{
					ArrayList itemIsotopesList =  new ArrayList(((Hashtable) item["itemPackages"]).Values);
					foreach (Hashtable isotope in itemIsotopesList)
					{
						if (isotope["hashValue"] != null && isotope["url"] != null)
						{
							itemObject.md5Hashs.Add(isotope["hashValue"].ToString());
							itemObject.bundleUrls.Add(isotope["url"].ToString().Replace("\\", ""));
						}
					}
				}
				
				
				itemObject.iconUrl = item["iconURL"] != null? item["iconURL"].ToString() : "";
				itemObject.textureUrl = item["textureURL"] != null? item["textureURL"].ToString() : "";
				
				items.Add(itemObject);
				
				
			}
		}
		else
		{
			//Debug.LogError("whole item table is null!"+dlcId);
		}
		
		if (rewardsHT != null)
		{
			ArrayList rewardsList =  new ArrayList(rewardsHT.Values);
			
			foreach (Hashtable reward in rewardsList)
			{
				Reward rewardObject = new Reward(TryToParse(reward["rewardId"]));
				
				
				rewardObject.remainder = TryToParse(reward["remainder"]);
				rewardObject.coins = TryToParse(reward["coins"]);
				rewardObject.gems = TryToParse(reward["gems"]);
				rewardObject.exp = TryToParse(reward["exp"]);
				rewardObject.energy = TryToParse(reward["energy"]);
				rewardObject.itemIds = reward["itemIds"]!= null? reward["itemIds"].ToString() : "";
				rewardObject.skills = new Skill(reward["skills"].ToString());
				
				rewards.Add(rewardObject);
				
				
				
				
			}
		}
		if (jobsHT != null)
		{
			ArrayList jobsList =  new ArrayList(jobsHT.Values);
			
			foreach (Hashtable job in jobsList)
			{
				Job jobObject = new Job(TryToParse (job["jobId"]));
				
				
				jobObject.sortOrder = TryToParse (job["sortOrder"]);
				jobObject.levelRequired = TryToParse (job["levelRequired"]);
				jobObject.expireTime = TryToParse (job["expireTime"]);
				jobObject.visibleForSkill = job["visibleForSkill"] != null? job["visibleForSkill"].ToString() : "";
				jobObject.title =job["title"] != null? job["title"].ToString() : "";
				jobObject.description = job["description"] != null? job["description"].ToString() : "";
				jobObject.timeRequired = TryToParse (job["timeRequired"]);
				jobObject.timeLeft = TryToParse (job["timeLeft"]);
				jobObject.gemsToFinish = TryToParse (job["gemsToFinish"]);
				jobObject.rewardId = TryToParse (job["rewardId"]);
				jobObject.activityId = TryToParse (job["activityId"]);

				jobs.Add(jobObject);
			}
		}
		
		if (itemConflictsHT != null)
		{
			ArrayList itemConflictsList =  new ArrayList(itemConflictsHT.Values);
			
			foreach (Hashtable conflict in itemConflictsList)
			{
				int pAttacker = TryToParse(conflict["conflictId1"]);
				int pDefenser = TryToParse(conflict["conflictId2"]);
				
				// this array is supposed to have length 2
				ItemConflicts.ConflictType pType = ItemConflicts.ConflictType.NULL;
				ItemConflicts.ConflictAction pAction = ItemConflicts.ConflictAction.NULL;
				
				if (Enum.IsDefined(typeof(ItemConflicts.ConflictType), conflict["conflictType"].ToString()))
					pType = (ItemConflicts.ConflictType) Enum.Parse(typeof(ItemConflicts.ConflictType), conflict["conflictType"].ToString());
				
				if (Enum.IsDefined(typeof(ItemConflicts.ConflictAction), conflict["conflictAction"].ToString()))
					pAction = (ItemConflicts.ConflictAction) Enum.Parse(typeof(ItemConflicts.ConflictAction), conflict["conflictAction"].ToString());
				
				itemConflictDBs.Add(new ItemConflictDB(pAttacker, pDefenser, pType, pAction));
				
				
				
			}
		}
		
		if (shopsHT != null)
		{
			ArrayList shopsList =  new ArrayList(shopsHT.Values);
			
			foreach (Hashtable shop in shopsList)
			{
				Shop shopObject = new Shop(TryToParse (shop["shopId"]));
				
				
				if (shop["items"] != null)
				{
					List<int> itemIds = new List<int>();
					
					foreach (String s in shop["items"].ToString().Split(','))
					{
						itemIds.Add(TryToParse(s));
					}
					shopObject.itemIds = itemIds;
				}
				
				shops.Add(shopObject);
			}
		}
		
		if (boyfriendsHT != null)
		{
			ArrayList boyfriendsList =  new ArrayList(boyfriendsHT.Values);
			
			foreach (Hashtable boyfriend in boyfriendsList)
			{
				Boyfriend boyfriendObject = new Boyfriend(TryToParse (boyfriend["boyfriendId"]));
				
				boyfriendObject.boyfriendName = boyfriend["boyfriendName"] != null? boyfriend["boyfriendName"].ToString(): "";
				boyfriendObject.title = boyfriend["boyfriendTitle"] != null? boyfriend["boyfriendTitle"].ToString(): "";
				boyfriendObject.meta = boyfriend["meta"] != null? boyfriend["meta"].ToString(): "";
				boyfriendObject.flirtExp = TryToParse (boyfriend["flirtExp"]);
				boyfriendObject.maxFlirtExp = TryToParse (boyfriend["flirtExpMax"]);
				boyfriendObject.charismaRequired = TryToParse (boyfriend["charismaRequired"]);
				boyfriendObject.rewardTimer = TryToParse (boyfriend["rewardTimer"]);
				boyfriendObject.meshName = boyfriend["meshName"] != null? boyfriend["meshName"].ToString(): "";
				boyfriendObject.dlcId = TryToParse (boyfriend["dlcId"]);
				boyfriendObject.iconURL = boyfriend["iconURL"] != null? boyfriend["iconURL"].ToString(): "";
				
				if (boyfriend["boyfriendPackages"] != null)
				{
					ArrayList boyfriendIsotopesList =  new ArrayList(((Hashtable) boyfriend["boyfriendPackages"]).Values);
					foreach (Hashtable isotope in boyfriendIsotopesList)
					{
						if (isotope["hashValue"] != null && isotope["url"] != null)
						{
							boyfriendObject.md5Hash = isotope["hashValue"].ToString();
							boyfriendObject.bundleUrl = isotope["url"].ToString().Replace("\\", "");
						}
					}
				}
				
				
				boyfriendObject.flirtCost = new List<int>();
				if (boyfriend["flirtCost"] != null)
				{
					foreach(string s in boyfriend["flirtCost"].ToString().Split(','))
					{
						if (TryToParse(s) != invalidCode)
							boyfriendObject.flirtCost.Add(TryToParse(s));
					}
				}
				
				boyfriendObject.flirtRepeat = new List<int>();
				if (boyfriend["flirtRepeat"] != null)
				{
					foreach(string s in boyfriend["flirtRepeat"].ToString().Split(','))
					{
						if (TryToParse(s) != invalidCode)
							boyfriendObject.flirtRepeat.Add(TryToParse(s));
					}
				}
				
				boyfriendObject.instantGiftCost = new List<int>();
				if (boyfriend["instantGiftCost"] != null)
				{
					foreach(string s in boyfriend["instantGiftCost"].ToString().Split(','))
					{
						if (TryToParse(s) != invalidCode)
							boyfriendObject.instantGiftCost.Add(TryToParse(s));
					}
				}
				
				boyfriendObject.location = new List<int>();
				if (boyfriend["location"] != null)
				{
					foreach(string s in boyfriend["location"].ToString().Split(','))
					{
						if (TryToParse(s) != invalidCode)
							boyfriendObject.location.Add(TryToParse(s));
					}
				}
				
				boyfriendObject.rewardIds = new List<int>();
				if (boyfriend["rewardIds"] != null)
				{
					foreach(string s in boyfriend["rewardIds"].ToString().Split(','))
					{
						if (TryToParse(s) != invalidCode)
							boyfriendObject.rewardIds.Add(TryToParse(s));
					}
				}
				
				boyfriendObject.rewardClaimed = new List<int>();
				if (boyfriend["rewardClaimed"] != null)
				{
					foreach(string s in boyfriend["rewardClaimed"].ToString().Split(','))
					{
						if (TryToParse(s) != invalidCode)
							boyfriendObject.rewardClaimed.Add(TryToParse(s));
					}
				}
				
				
				boyfriends.Add(boyfriendObject);
			}
		}

		if (packagesHT != null)
		{
			//Log.Debug("[DLCConfigs][ConvertToListInside] The packages count: " + packagesHT.Count);
			ArrayList packagesList = new ArrayList(packagesHT.Values);

			foreach (Hashtable package in packagesList)
			{
				DLCPackage packageObj 	= new DLCPackage(TryToParse(package["itemPackageId"]));
				packageObj.platform		= package["platform"] != null? package["platform"].ToString(): "";
				packageObj.hashValue	= package["hashValue"] != null? package["hashValue"].ToString(): "";
				packageObj.url			= package["url"] != null? package["url"].ToString(): "";
				packageObj.dlcId		= TryToParse(package["dlcId"].ToString());

				packages.Add(packageObj);
			}

		}
	}
	
	bool IsValidItemId(int id)
	{
		if (id == invalidCode) return false;
		if (Items.getItem(id) != null) return true;
		
		// maybe it is a dlc item?
		foreach (Item item in items)
		{
			if (item.itemId == id)
			{
				return true;
			}
		}
		
		// or previous dlc items?
		foreach (DLCConfig c in DLCConfigs.allDlcConfigs)
		{
			foreach (Item item in c.items)
			{
				if (item.itemId == id)
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	bool IsValidRewardId(int id)
	{
		if (id == invalidCode) return false;
		if (Rewards.getReward(id) != null) return true;
		
		// maybe it is a dlc item?
		foreach (Reward reward in rewards)
		{
			if (reward.rewardId == id)
			{
				return true;
			}
		}
		
		// or previous dlc items?
		foreach (DLCConfig c in DLCConfigs.allDlcConfigs)
		{
			foreach (Reward reward in c.rewards)
			{
				if (reward.rewardId == id)
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	public bool CheckValidity()
	{
		Log.Debug("DLC Config: check Items");
		foreach (Item itemObject in items)
		{
			if (itemObject.itemId == invalidCode) return false;
			if (itemObject.itemName.Equals("")) return false;
			if (ItemSubcategories.getSubcategory(itemObject.subCategoryId) == null) return false;
			if (itemObject.price == invalidCode) return false;
			if (itemObject.gems == invalidCode) return false;
			if (itemObject.requiredLevel == invalidCode) return false;
			if (itemObject.featureId == invalidCode) return false;
			if (itemObject.sexy == invalidCode) return false;
			if (itemObject.lively == invalidCode) return false;	
			if (itemObject.dlcId == invalidCode) return false;
		}
		
		Log.Debug("DLC Config: check Rewards");
		foreach (Reward rewardObject in rewards)
		{
			if (rewardObject.rewardId == invalidCode) return false;
			if (rewardObject.remainder == invalidCode) return false;
			if (rewardObject.coins == invalidCode) return false;
			if (rewardObject.gems == invalidCode) return false;
			if (rewardObject.exp == invalidCode) return false;
			if (rewardObject.energy == invalidCode) return false;
			
			
			if (!rewardObject.itemIds.Equals(""))
			{
				string[] ids = rewardObject.itemIds.Split(',');
				
				foreach (string s in ids)
				{
					if (!IsValidItemId(TryToParse(s)))
						return false;
					
				}
			}
		}
		
		Log.Debug("DLC Config: check Jobs");
		foreach (Job jobObject in jobs)
		{
			if (jobObject.sortOrder == invalidCode) return false;
			if (jobObject.levelRequired == invalidCode) return false;
			if (jobObject.expireTime == invalidCode) return false;
			if (jobObject.visibleForSkill.Equals("")) return false;
				
			if (jobObject.timeRequired == invalidCode) return false;
			if (jobObject.timeLeft == invalidCode) return false;
			if (jobObject.gemsToFinish == invalidCode) return false;
			if (!IsValidRewardId(jobObject.rewardId)) return false;
			if (jobObject.activityId == invalidCode) return false;			
		}
		
		Log.Debug("DLC Config: check ItemConflicts");
		foreach (ItemConflictDB icdb in itemConflictDBs)
		{
			
			if (icdb.action.Equals(ItemConflicts.ConflictAction.NULL) || icdb.conflictType.Equals(ItemConflicts.ConflictType.NULL)) return false;
			
			if (icdb.conflictType.Equals(ItemConflicts.ConflictType.S2S))
			{
				if (ItemSubcategories.getSubcategory(icdb.attacker)	== null
					|| ItemSubcategories.getSubcategory(icdb.defenser)	== null)
				{
					Log.Debug("DLC Config subcat/subcat id is null: " + icdb.attacker + " " + icdb.defenser);
					return false;
				}
			}
			
			if (icdb.conflictType.Equals(ItemConflicts.ConflictType.S2I))
			{
				if (ItemSubcategories.getSubcategory(icdb.attacker)	== null
					|| !IsValidItemId(icdb.defenser))
				{
					Log.Debug("DLC Config subcat/itemId id is null: " + icdb.attacker + " " + icdb.defenser);
					return false;
				}
			}
			
			if (icdb.conflictType.Equals(ItemConflicts.ConflictType.I2S))
			{
				if (!IsValidItemId(icdb.attacker)
					|| ItemSubcategories.getSubcategory(icdb.defenser)	== null)
				{
					Log.Debug("DLC Config item/subcat id is null: " + icdb.attacker + " " + icdb.defenser);
					return false;
				}
			}
			
			if (icdb.conflictType.Equals(ItemConflicts.ConflictType.I2I))
			{
				if (!IsValidItemId(icdb.attacker)
					|| !IsValidItemId(icdb.defenser))
				{
					Log.Debug("DLC Config item id is null: " + icdb.attacker + " " + icdb.defenser);
					return false;
				}
			}
		}		
		
		Log.Debug("DLC Config: check Shops");
		foreach (Shop shop in shops)
		{
			if (shop.shopId == invalidCode) return false;
			
			if (shop.itemIds.Count > 0)
			{
				
				foreach (int s in shop.itemIds)
				{
					if (!IsValidItemId(s))
					{
						Log.Debug("DLC Config: shop's itemId is null: " + s);
						return false;
					}					
				}
			}
		}
		
		Log.Debug("DLC Config: check Boyfriends");
		foreach (Boyfriend boyfriend in boyfriends)
		{
			if (boyfriend.boyfriendId == invalidCode) return false;
			if (boyfriend.boyfriendName.Equals("")) return false;
			if (boyfriend.flirtExp == invalidCode) return false;
			if (boyfriend.maxFlirtExp == invalidCode) return false;
			if (boyfriend.charismaRequired == invalidCode) return false;
			if (boyfriend.rewardTimer == invalidCode) return false;
			if (boyfriend.meshName.Equals("")) return false;
			if (boyfriend.dlcId == invalidCode) return false;
			if (boyfriend.flirtCost.Count != 3) return false;
			if (boyfriend.flirtRepeat.Count != 3) return false;
			if (boyfriend.instantGiftCost.Count != 3) return false;
			if (boyfriend.location.Count <= 0) return false;
			if (boyfriend.rewardIds.Count != 3) return false;
			if (boyfriend.rewardIds.Count > 0)
			{				
				foreach (int s in boyfriend.rewardIds)
				{
					if (!IsValidRewardId(s))
					{
						Log.Debug("DLC Config boyfriend's reward is null: " + s);
						return false;
					}
				}
			}
			if (boyfriend.rewardClaimed.Count != 3) return false;
		}


		Log.Debug("DLC Config Check Packages");
		foreach(DLCPackage package in packages)
		{
			//Log.Debug("PackageId: " + package.packageId);
			//Log.Debug("Platform: " + package.platform);
			//Log.Debug("HashValue: " + package.hashValue);
			//Log.Debug("URL: " + package.url);
			//Log.Debug("DLCId: " + package.dlcId);
			     


			if (package.packageId == invalidCode) return false;
			if (package.platform.Equals("")) return false;
			if (package.hashValue.Equals("")) return false;
			if (package.url.Equals("")) return false;
			if (package.dlcId == invalidCode) return false;
		}
		
		return true;
	}
	
	public void CommitAppUpdate ()
	{
		string fileName = "dlc_queryfile_" + dlcId;
							
		// load file
		TextAsset theFile = Resources.Load("DLC Query/"+fileName) as TextAsset;
		
		if (theFile != null)
		{
		
			// split query by ;
			string inside = theFile.text;
			Log.Debug("what have i loaded?"+inside);
						
			if (inside != null && !inside.Equals(""))
			{
				string[] queries = inside.Split(';');
				bool isSuccess = false;
				SQLiteQuery qr = null;	
				try
				{
					qr = new SQLiteQuery(DatabaseManager.SharedManager.db,"BEGIN TRANSACTION");
					qr.Step();
					qr.Release();
					
					foreach(string s in queries)
					{
						Log.Debug("DLC queries: " + s);
						
						qr = new SQLiteQuery(DatabaseManager.SharedManager.db, s.Replace("\n", ""));
						qr.Step();
						qr.Release();
						
						//GainProgress(fraction*1f/queries.Length);
					}
					qr = new SQLiteQuery(DatabaseManager.SharedManager.db,"COMMIT");
					qr.Step();
					qr.Release();
					
					isSuccess = true;
				}
				catch
				{
					isSuccess = false;
					Log.Debug("ROLLLLLBACK");
					
					qr = new SQLiteQuery(DatabaseManager.SharedManager.db,"ROLLBACK");
					qr.Step();
					qr.Release();
					
					//yield break;
				}
				
				if (isSuccess)
				{
					string commitText = Language.Get("dlc_committing");
					
					Globals.currentDLCVersion 	= dlcId;
					Globals.apparentDLCVersion 	= dlcId;
					Globals.mainPlayer.Save();
					
					Log.Warning("AFTER APP UPDATE, NOW DLC IS : " + dlcId);
					//PopupManager.ShowInformationPopup("DLC finished! now version is: "+dlcId);
					
					
					
					//DatabaseManager.db.Close();
					
					//LogFlurry Event
					Hashtable param = new Hashtable();
					param["Success"] = "New_" + dlcId;
					MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.DLC_State, param);
				}
			}
			else
			{
				string commitText = Language.Get("dlc_committing");

				Globals.currentDLCVersion 	= dlcId;
				Globals.apparentDLCVersion 	= dlcId;
				Globals.mainPlayer.Save();

				Log.Warning("AFTER APP UPDATE, NOW DLC IS : " + dlcId);
				//PopupManager.ShowInformationPopup("DLC finished! now version is: "+dlcId);



				//DatabaseManager.db.Close();

				//LogFlurry Event
				Hashtable param = new Hashtable();
				param["Success"] = "New_" + dlcId;
				MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.DLC_State, param);
			}
		}
	}
	
	public void Commit ()
	{
	
		// all items in this DLC are downloaded, and metadata are set
		//DatabaseManager.db.Open(Globals.dbSavePath);

		if (items != null)
		{
			Log.Debug("DLCConfig - Commit: Start Save Items");
			foreach (Item item in items)
			{
				//Debug.Log("THE ITEM:" +item.itemId+","+item.itemName);
				
				Item existingItem = Items.getItem(item.itemId);
				bool exist = existingItem != null;
					
				if (exist)
				{
					Items.allItems.Remove(existingItem);
					Items.allItems.Add(item);
				}
				else
				{
					Items.allItems.Add(item);
				}
			}
			Items.Save();
			Log.Debug("DLCConfig - Commit: Saved Items");
		}

		if (rewards != null)
		{
			Log.Debug("DLCConfig - Commit: Start Save Rewards");
			foreach (Reward reward in rewards)
			{
				Reward existingReward = Rewards.getReward(reward.rewardId);
				bool exist = existingReward != null;
					
				if (exist)
				{
					Rewards.rewardsList.Remove(existingReward);
					Rewards.rewardsList.Add(reward);
				}
				else
				{
					Rewards.rewardsList.Add(reward);
				}
			}
			Rewards.Save();
			Log.Debug("DLCConfig - Commit: Saved Rewards");
		}

		if (jobs != null)
		{
			Log.Debug("DLCConfig - Commit: Start Save Jobs");
			foreach (Job job in jobs)
			{
				Job existingJob = Jobs.getJob(job.activityId);
				bool exist = existingJob != null;
					
				if (exist)
				{
					
					Jobs.jobsList.Remove(existingJob);
					Jobs.jobsList.Add(job);
				}
				else
				{
					Jobs.jobsList.Add(job);
				}
			}
			Jobs.Save();
			Log.Debug("DLCConfig - Commit: Saved Jobs");
		}

		if (itemConflictDBs != null)
		{
			Log.Debug("DLCConfig - Commit: Start Save ItemConfict");
			foreach (ItemConflictDB itemConflictDB in itemConflictDBs)
			{
				ItemConflictDB existingConflictDB = ItemConflicts.getConflictDB(itemConflictDB.attacker, itemConflictDB.defenser, itemConflictDB.conflictType);
				bool exist = existingConflictDB != null;
					
				if (exist)
				{
					ItemConflicts.allConflictDBs.Remove(existingConflictDB);
					ItemConflicts.allConflictDBs.Add(itemConflictDB);
				}
				else
				{
					ItemConflicts.allConflictDBs.Add(itemConflictDB);
				}
			}		
			ItemConflicts.Save();
			ItemConflicts.GenerateConflictsByDB();
			Log.Debug("DLCConfig - Commit: Saved ItemConficts");
		}

		if (shops != null)
		{
			Log.Debug("DLCConfig - Commit: Start Save Shops");		
			foreach (Shop shop in shops)
			{
				shop.items = new List<Item>();
				foreach (int i in shop.itemIds)
				{
					shop.items.Add(Items.getItem(i));
				}
				
				Shop existingShop = Shops.GetShopById(shop.shopId);
				bool exist = existingShop != null;
					
				if (exist)
				{
					shop.shopName		= existingShop.shopName;
					shop.bgmFilename 	= existingShop.bgmFilename;
					shop.iconName 		= existingShop.iconName;
					shop.hideSubcats 	= existingShop.hideSubcats;
					
					Shops.shopsList.Remove(existingShop);
					Shops.shopsList.Add(shop);
				}
				else
				{
					
					shop.bgmFilename = "";
					shop.iconName = "";
					shop.hideSubcats = "";
					
					Shops.shopsList.Add(shop);
				}
				
				
			}
			Shops.Save();
			Log.Debug("DLCConfig - Commit: Saved Shops");
		}
		

		if (boyfriends != null)
		{
			Log.Debug("DLCConfig - Commit: Start Save Boyfriends");
			foreach (Boyfriend bf in boyfriends)
			{
				Boyfriend existingBf = Boyfriends.GetBoyfriend(bf.boyfriendId);
				bool exist = existingBf != null;
					
				if (exist)
				{
					Boyfriends.boyfriendsList.Remove(existingBf);
					Boyfriends.boyfriendsList.Add(bf);
				}
				else
				{
					Boyfriends.boyfriendsList.Add(bf);
				}
			}
			Boyfriends.Save();
			Log.Debug("DLCConfig - Commit: Saved Boyfriends");
		}	
		
		
	//	DatabaseManager.SharedManager.Save("INSERT INTO Dlcs (dlcId, dlcVersion) VALUES ("+dlcId+", '"+ dlcVersion +"')");
		
		Globals.currentDLCVersion = dlcId;
		Globals.apparentDLCVersion = dlcId;
		Globals.mainPlayer.Save();
		
		Log.Warning("NOW DLC IS : " + dlcId);
		//PopupManager.ShowInformationPopup("DLC finished! now version is: "+dlcId);
		
		
		
		//DatabaseManager.db.Close();
		
		//LogFlurry Event
		Hashtable param = new Hashtable();
		param["Success"] = "New_" + dlcId;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.DLC_State, param);
	}
	
	int TryToParse(object v)
	{
		if (v != null)
			return TryToParse(v.ToString());
		else
			return invalidCode;
	}
	
	int TryToParse(string v)
	{
		int result = 0;
		
		if (Int32.TryParse(v, out result))
			return result;
		else return invalidCode;
	}
}



using System;
using UnityEngine;
using System.Collections.Generic;


public class Rewards
{
	public static List<Reward> rewardsList = new List<Reward>();
	
	public static Reward[] initialRewardsList = {
	    // jobs
		        // id,remainder,coin,gem, exp,energy, item, skill
		new Reward(1,    -1,     50,  0,   20,  0,      "", new Skill(new int[] {2,0,0,0,0})),
		new Reward(2,    -1,    230,  0,  100,  0,      "", new Skill(new int[] {10,0,0,0,0})),
		new Reward(3,    -1,    660,  0,  270,  0,      "", new Skill(new int[] {30,0,0,0,0})),
		new Reward(4,    -1,   1250,  0,  500,  0,      "", new Skill(new int[] {60,0,0,0,0})),
		new Reward(5,    -1,   3200,  0, 1350,  0,      "", new Skill(new int[] {180,0,0,0,0})),
		new Reward(6,    -1,   5800,  0, 2300,  0,      "", new Skill(new int[] {360,0,0,0,0})),
		new Reward(7,    -1,   9300,  0, 3700,  0,      "", new Skill(new int[] {600,0,0,0,0})),
		new Reward(8,    -1,  14000,  0, 5600,  0,      "", new Skill(new int[] {960,0,0,0,0})),
	
		new Reward(9,    -1,    120,  0,   50,  0,      "", new Skill(new int[] {0,5,0,0,0})),
		new Reward(10,   -1,    340,  0,  140,  0,      "", new Skill(new int[] {0,15,0,0,0})),
		new Reward(11,   -1,    980,  0,  400,  0,      "", new Skill(new int[] {0,45,0,0,0})),
		new Reward(12,   -1,   2400,  0,  960,  0,      "", new Skill(new int[] {0,120,0,0,0})),
		new Reward(13,   -1,   4000,  0, 1600,  0,      "", new Skill(new int[] {0,240,0,0,0})),
		new Reward(14,   -1,   7500,  0, 3000,  0,      "", new Skill(new int[] {0,480,0,0,0})),
		new Reward(15,   -1,  11000,  0, 4400,  0,      "", new Skill(new int[] {0,720,0,0,0})),
		new Reward(16,   -1,  17000,  0, 6200,  0,      "", new Skill(new int[] {0,1200,0,0,0})),
	
		new Reward(17,   -1,    340,  0,  150,  0,      "", new Skill(new int[] {0,0,0,0,15})),
		new Reward(18,   -1,    660,  0,  270,  0,      "", new Skill(new int[] {0,0,0,0,30})),
		new Reward(19,   -1,   1250,  0,  500,  0,      "", new Skill(new int[] {0,0,0,0,60})),
		new Reward(20,   -1,   2400,  0,  960,  0,      "", new Skill(new int[] {0,0,0,0,120})),
		new Reward(21,   -1,   4000,  0, 1600,  0,      "", new Skill(new int[] {0,0,0,0,240})),
		new Reward(22,   -1,   7500,  0, 3000,  0,      "", new Skill(new int[] {0,0,0,0,480})),
		new Reward(23,   -1,  11000,  0, 4400,  0,      "", new Skill(new int[] {0,0,0,0,720})),
		new Reward(24,   -1,  20000,  0, 7400,  0,      "", new Skill(new int[] {0,0,0,0,1440})),
	
	 	// boyfriend
		// id,remainder,coin,gem, exp,energy, item, skill
		
		//new Reward(25,    -1,     50,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(26,    -1,   1400,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		//new Reward(27,    -1,   2800,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		//new Reward(28,    -1,     50,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(29,    -1,   1800,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		//new Reward(30,    -1,   3600,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		//new Reward(31,    -1,     50,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(32,    -1,   2500,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		//new Reward(33,    -1,   5000,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(34,    -1,     50,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(35,    -1,   3000,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		//new Reward(36,    -1,   6000,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		
		new Reward(37,    -1,      0,  0,    0,  0,   "252", new Skill(new int[] {0,0,0,0,0})),
		new Reward(38,    -1,      0,  0,    0,  0,   "475", new Skill(new int[] {0,0,0,0,0})),
		new Reward(39,    -1,      0,  0,    0,  0,   "471", new Skill(new int[] {0,0,0,0,0})),
		new Reward(40,    -1,      0,  0,    0,  0,    "93", new Skill(new int[] {0,0,0,0,0})),
		new Reward(41,    -1,      0,  0,    0,  0,   "101", new Skill(new int[] {0,0,0,0,0})),
		new Reward(42,    -1,      0,  0,    0,  0,   "104", new Skill(new int[] {0,0,0,0,0})),
		new Reward(43,    -1,      0,  0,    0,  0,   "229", new Skill(new int[] {0,0,0,0,0})),
		new Reward(44,    -1,      0,  0,    0,  0,   "233", new Skill(new int[] {0,0,0,0,0})),
		new Reward(45,    -1,      0,  0,    0,  0,   "285", new Skill(new int[] {0,0,0,0,0})),
		new Reward(46,    -1,      0,  0,    0,  0,   "286", new Skill(new int[] {0,0,0,0,0})),
		new Reward(47,    -1,      0,  0,    0,  0,   "287", new Skill(new int[] {0,0,0,0,0})),
		new Reward(48,    -1,      0,  0,    0,  0,   "291", new Skill(new int[] {0,0,0,0,0})),
		new Reward(49,    -1,      0,  0,    0,  0,   "482", new Skill(new int[] {0,0,0,0,0})),
		new Reward(50,    -1,      0,  0,    0,  0,   "473", new Skill(new int[] {0,0,0,0,0})),
		new Reward(51,    -1,      0,  0,    0,  0,   "480", new Skill(new int[] {0,0,0,0,0})),
		new Reward(52,    -1,      0,  0,    0,  0,   "478", new Skill(new int[] {0,0,0,0,0})),
		new Reward(53,    -1,      0,  0,    0,  0,   "399", new Skill(new int[] {0,0,0,0,0})),
		new Reward(54,    -1,      0,  0,    0,  0,   "483", new Skill(new int[] {0,0,0,0,0})),
		new Reward(55,    -1,      0,  0,    0,  0,   "476", new Skill(new int[] {0,0,0,0,0})),
		new Reward(56,    -1,      0,  0,    0,  0,   "481", new Skill(new int[] {0,0,0,0,0})),
		new Reward(57,    -1,      0,  0,    0,  0,   "346", new Skill(new int[] {0,0,0,0,0})),
		new Reward(58,    -1,      0,  0,    0,  0,   "441", new Skill(new int[] {0,0,0,0,0})),
		new Reward(59,    -1,      0,  0,    0,  0,   "472", new Skill(new int[] {0,0,0,0,0})),
		new Reward(60,    -1,      0,  0,    0,  0,   "426", new Skill(new int[] {0,0,0,0,0})),
		new Reward(61,    -1,      0,  0,    0,  0,   "479", new Skill(new int[] {0,0,0,0,0})),
		new Reward(62,    -1,      0,  0,    0,  0,   "474", new Skill(new int[] {0,0,0,0,0})),
		new Reward(63,    -1,      0,  0,    0,  0,   "477", new Skill(new int[] {0,0,0,0,0})),
	
	    // quest
	
		new Reward(101,    -1,   500,  0,   10,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(102,    -1,   500,  0,   20,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(103,    -1,   500,  0,   30,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(104,    -1,   500,  0,   40,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(105,    -1,   500,  0,   40,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(106,    -1,  2000,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(107,    -1,  1000,  0,  300,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(108,    -1,  1000,  0,  600,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(109,    -1,  1000,  0, 1000,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(110,    -1,     0,  0,  200,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(111,    -1, 10000,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(112,    -1, 20000,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(113,    -1,     0,  0,  100,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(114,    -1,     0,  0,  200,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(115,    -1,     0,  0,  300,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(116,    -1,     0,  0,  400,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(117,    -1,     0,  0,  500,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(118,    -1,     0,  0,  100,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(119,    -1,     0,  0,  150,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(120,    -1,     0,  0,  200,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(121,    -1,     0,  0,  100,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(122,    -1,     0,  0,  200,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(123,    -1,     0,  0,  300,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(127,    -1,   500,  0, 1000,  0,      "", new Skill(new int[] {0,0,0,0,0})),

		new Reward(132,    -1,  2000,  0,   10,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		new Reward(133,    -1,  5000,  0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
		
		//Poster
		new Reward(168,    -1,   10,   0,    0,  0,      "", new Skill(new int[] {0,0,0,0,0})),
	};
	
	public static Reward getReward(int rId)
	{
		foreach(Reward r in rewardsList)
		{
			if (r.rewardId == rId)
				return r;
		}
		
		return null;
	}
	
	public static Reward CombineRewards(List<Reward> list)
	{
		Reward returnMe = new Reward(100000, 1, 0,0,0,0, "", new Skill(new int[]{0,0,0,0,0}));
		
		foreach (Reward r in list)
		{
			returnMe.coins += r.coins;
			returnMe.gems += r.gems;
			returnMe.exp += r.exp;
			returnMe.energy += r.energy;
			
			if (!r.itemIds.Equals(""))
			{
				if (!returnMe.itemIds.Equals(""))
					returnMe.itemIds += ",";
				
				returnMe.itemIds += r.itemIds;
			}
			
			for (int i = 0; i < Skill.skillTypes; i++)
				returnMe.skills.skill[i] += r.skills.skill[i];
		}
		
		
		return returnMe;
	}
	
	
	
	public static void Save()
	{
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		//DatabaseManager.db.Open(Globals.dbSavePath);
		foreach (Reward a in rewardsList)
		{
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM Rewards WHERE rewardId = '" + a.rewardId + "'",
				
				//update
				new List<string>(){
				"UPDATE Rewards SET "+
				"  remainder = " + a.remainder +
				" ,coins = " + a.coins + 
				" ,gems = " + a.gems +
				" ,exp = " + a.exp +
				" ,energy = " + a.energy +
				" ,itemIds = '" + a.itemIds + "'" +
				" ,skills = '" + Skill.GetSkillAsString(a.skills) + "'" +
				" WHERE rewardId = " + a.rewardId
				},
			
				//insert
				new List<string>(){
					"INSERT INTO Rewards (rewardId, remainder, coins, gems, exp, energy, itemIds, skills) VALUES( "+
				                  a.rewardId +
				"  , " + a.remainder +
				" , " + a.coins + 
				" , " + a.gems +
				" , " + a.exp +
				" , " + a.energy +
				" , '" + a.itemIds + "'" +
				" , '" + Skill.GetSkillAsString(a.skills) + "'" +
				")"
				}
			);
			
			/*SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM Rewards WHERE rewardId = '" + a.rewardId + "'" ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			if (hasRecord)
			{
				DatabaseManager.SharedManager.Save("UPDATE Rewards SET "+
				"  remainder = " + a.remainder +
				" ,coins = " + a.coins + 
				" ,gems = " + a.gems +
				" ,exp = " + a.exp +
				" ,energy = " + a.energy +
				" ,itemIds = '" + a.itemIds + "'" +
				" ,skills = '" + Skill.GetSkillAsString(a.skills) + "'" +
				" WHERE rewardId = " + a.rewardId);
			}
			else
			{
				DatabaseManager.SharedManager.Save("INSERT INTO Rewards (rewardId, remainder, coins, gems, exp, energy, itemIds, skills) VALUES( "+
				                  a.rewardId +
				"  , " + a.remainder +
				" , " + a.coins + 
				" , " + a.gems +
				" , " + a.exp +
				" , " + a.energy +
				" , '" + a.itemIds + "'" +
				" , '" + Skill.GetSkillAsString(a.skills) + "'" +
				")");
			}
			*/
			
		}
		//DatabaseManager.db.Close();
	}
	
	
	public static void Load()
	{
		rewardsList.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM Rewards";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			rewardsList.Add(new Reward(
				qr.GetInteger("rewardId"),
				qr.GetInteger("remainder"),
				qr.GetInteger("coins"),
				qr.GetInteger("gems"),
				qr.GetInteger("exp"),
				qr.GetInteger("energy"),
				qr.GetString("itemIds"),
				new Skill(qr.GetString("skills"))
				));
			
		}
			
		qr.Release();
	}
}



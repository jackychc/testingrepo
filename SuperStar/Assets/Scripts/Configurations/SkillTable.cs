using System;
using UnityEngine;
using System.Collections.Generic;


public class SkillTable
{
	public static List<SkillTier> allSkillTiers = new List<SkillTier>();
	
	public static List<SkillTier> initialSkillTiers = new List<SkillTier>()
	{
		new SkillTier(1, 0, 1, SkillTier.STARCOLOR.BRONZE, 1, "job0_color1"),
		new SkillTier(2, 0, 3, SkillTier.STARCOLOR.BRONZE, 2, "job0_color1"),
		new SkillTier(3, 0, 30, SkillTier.STARCOLOR.BRONZE, 3, "job0_color1"),
		new SkillTier(4, 0, 90, SkillTier.STARCOLOR.BRONZE, 4, "job0_color1"),
		new SkillTier(5, 0, 210, SkillTier.STARCOLOR.BRONZE, 5, "job0_color1"),
		new SkillTier(6, 0, 390, SkillTier.STARCOLOR.SLIVER, 1, "job0_color2"),
		new SkillTier(7, 0, 630, SkillTier.STARCOLOR.SLIVER, 2, "job0_color2"),
		new SkillTier(8, 0, 990, SkillTier.STARCOLOR.SLIVER, 3, "job0_color2"),
		new SkillTier(9, 0, 1350, SkillTier.STARCOLOR.SLIVER, 4, "job0_color2"),
		new SkillTier(10, 0, 1710, SkillTier.STARCOLOR.SLIVER, 5, "job0_color2"),
		new SkillTier(11, 0, 2070, SkillTier.STARCOLOR.GOLD, 1, "job0_color3"),
		new SkillTier(12, 0, 2790, SkillTier.STARCOLOR.GOLD, 2, "job0_color3"),
		new SkillTier(13, 0, 3510, SkillTier.STARCOLOR.GOLD, 3, "job0_color3"),
		new SkillTier(14, 0, 4230, SkillTier.STARCOLOR.GOLD, 4, "job0_color3"),
		new SkillTier(15, 0, 4950, SkillTier.STARCOLOR.GOLD, 5, "job0_color3"),
		new SkillTier(16, 0, 6030, SkillTier.STARCOLOR.DIAMOND, 1, "job0_color4"),
		new SkillTier(17, 0, 7110, SkillTier.STARCOLOR.DIAMOND, 2, "job0_color4"),
		new SkillTier(18, 0, 8190, SkillTier.STARCOLOR.DIAMOND, 3, "job0_color4"),
		new SkillTier(19, 0, 9270, SkillTier.STARCOLOR.DIAMOND, 4, "job0_color4"),
		new SkillTier(20, 0, 10710, SkillTier.STARCOLOR.DIAMOND, 5, "job0_color4"),
		
		new SkillTier(61, 1, 0, SkillTier.STARCOLOR.BRONZE, 0, "job1_color1"),
		new SkillTier(21, 1, 5, SkillTier.STARCOLOR.BRONZE, 1, "job1_color1"),
		new SkillTier(22, 1, 30, SkillTier.STARCOLOR.BRONZE, 2, "job1_color1"),
		new SkillTier(23, 1, 90, SkillTier.STARCOLOR.BRONZE, 3, "job1_color1"),
		new SkillTier(24, 1, 210, SkillTier.STARCOLOR.BRONZE, 4, "job1_color1"),
		new SkillTier(25, 1, 390, SkillTier.STARCOLOR.BRONZE, 5, "job1_color1"),
		new SkillTier(26, 1, 630, SkillTier.STARCOLOR.SLIVER, 1, "job1_color2"),
		new SkillTier(27, 1, 990, SkillTier.STARCOLOR.SLIVER, 2, "job1_color2"),
		new SkillTier(28, 1, 1350, SkillTier.STARCOLOR.SLIVER, 3, "job1_color2"),
		new SkillTier(29, 1, 1710, SkillTier.STARCOLOR.SLIVER, 4, "job1_color2"),
		new SkillTier(30, 1, 2070, SkillTier.STARCOLOR.SLIVER, 5, "job1_color2"),
		new SkillTier(31, 1, 2790, SkillTier.STARCOLOR.GOLD, 1, "job1_color3"),
		new SkillTier(32, 1, 3510, SkillTier.STARCOLOR.GOLD, 2, "job1_color3"),
		new SkillTier(33, 1, 4230, SkillTier.STARCOLOR.GOLD, 3, "job1_color3"),
		new SkillTier(34, 1, 4950, SkillTier.STARCOLOR.GOLD, 4, "job1_color3"),
		new SkillTier(35, 1, 6030, SkillTier.STARCOLOR.GOLD, 5, "job1_color3"),
		new SkillTier(36, 1, 7110, SkillTier.STARCOLOR.DIAMOND, 1, "job1_color4"),
		new SkillTier(37, 1, 8190, SkillTier.STARCOLOR.DIAMOND, 2, "job1_color4"),
		new SkillTier(38, 1, 9270, SkillTier.STARCOLOR.DIAMOND, 3, "job1_color4"),
		new SkillTier(39, 1, 10710, SkillTier.STARCOLOR.DIAMOND, 4, "job1_color4"),
		new SkillTier(40, 1, 12150, SkillTier.STARCOLOR.DIAMOND, 5, "job1_color4"),
		
		new SkillTier(62, 4, 0, SkillTier.STARCOLOR.BRONZE, 0, "job4_color1"),
		new SkillTier(41, 4, 15, SkillTier.STARCOLOR.BRONZE, 1, "job4_color1"),
		new SkillTier(42, 4, 30, SkillTier.STARCOLOR.BRONZE, 2, "job4_color1"),
		new SkillTier(43, 4, 90, SkillTier.STARCOLOR.BRONZE, 3, "job4_color1"),
		new SkillTier(44, 4, 210, SkillTier.STARCOLOR.BRONZE, 4, "job4_color1"),
		new SkillTier(45, 4, 390, SkillTier.STARCOLOR.BRONZE, 5, "job4_color1"),
		new SkillTier(46, 4, 630, SkillTier.STARCOLOR.SLIVER, 1, "job4_color2"),
		new SkillTier(47, 4, 990, SkillTier.STARCOLOR.SLIVER, 2, "job4_color2"),
		new SkillTier(48, 4, 1350, SkillTier.STARCOLOR.SLIVER, 3, "job4_color2"),
		new SkillTier(49, 4, 1710, SkillTier.STARCOLOR.SLIVER, 4, "job4_color2"),
		new SkillTier(50, 4, 2070, SkillTier.STARCOLOR.SLIVER, 5, "job4_color2"),
		new SkillTier(51, 4, 2790, SkillTier.STARCOLOR.GOLD, 1, "job4_color3"),
		new SkillTier(52, 4, 3510, SkillTier.STARCOLOR.GOLD, 2, "job4_color3"),
		new SkillTier(53, 4, 4230, SkillTier.STARCOLOR.GOLD, 3, "job4_color3"),
		new SkillTier(54, 4, 4950, SkillTier.STARCOLOR.GOLD, 4, "job4_color3"),
		new SkillTier(55, 4, 6030, SkillTier.STARCOLOR.GOLD, 5, "job4_color3"),
		new SkillTier(56, 4, 7110, SkillTier.STARCOLOR.DIAMOND, 1, "job4_color4"),
		new SkillTier(57, 4, 8190, SkillTier.STARCOLOR.DIAMOND, 2, "job4_color4"),
		new SkillTier(58, 4, 9270, SkillTier.STARCOLOR.DIAMOND, 3, "job4_color4"),
		new SkillTier(59, 4, 10710, SkillTier.STARCOLOR.DIAMOND, 4, "job4_color4"),
		new SkillTier(60, 4, 12150, SkillTier.STARCOLOR.DIAMOND, 5, "job4_color4")
		
	};
	
	
	public static SkillTier GetSkillTier(int id)
	{
		foreach (SkillTier t in allSkillTiers)
		{
			if (t.skillTierId == id)
				return t;
		}
		
		return null;
	}
	
	public static List<SkillTier> GetAllSkillTiersOfType(int pSkillType)
	{
		List<SkillTier> returnMe = new List<SkillTier>();
		
		foreach (SkillTier t in allSkillTiers)
		{
			if (t.skillType == pSkillType)
				returnMe.Add(t);
		}
		
		returnMe.Sort((x, y) => {return x.skillAmount.CompareTo(y.skillAmount);});
		
		return returnMe;
	}
	
	public static SkillTier GetSkillTier(int pSkillType, int pSkillAmount)
	{
		List<SkillTier> allTiersInType = GetAllSkillTiersOfType(pSkillType);
		
		
		for (int i = 0; i < allTiersInType.Count; i++)
		{
			if (pSkillAmount < allTiersInType[i].skillAmount)
			{
				if (i != 0)
					return allTiersInType[i-1];
				else
					return allTiersInType[0];
			}
		}
		
		return allTiersInType[allTiersInType.Count-1];
	}
	
	public static SkillTier GetNextSkillTier(int pSkillType, int pSkillAmount)
	{
		List<SkillTier> allTiersInType = GetAllSkillTiersOfType(pSkillType);
		
		
		for (int i = 0; i < allTiersInType.Count; i++)
		{
			if (pSkillAmount < allTiersInType[i].skillAmount)
			{
				return allTiersInType[i];
			}
		}
		
		return null;
	}
	
	public static void Save()
	{
	}
	
	public static void Load()
	{
		foreach (SkillTier t in initialSkillTiers)
		{
			allSkillTiers.Add(t);
		}
	}
	
}



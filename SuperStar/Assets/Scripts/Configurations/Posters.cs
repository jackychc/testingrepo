using System;
using UnityEngine;
using System.Collections.Generic;


public class Posters
{
	public static List<Poster> postersList = new List<Poster>();
	
	public static Poster[] initialPostersList = {
			// id, period, probability, scene, reward, url, location
			new Poster(1,"24/12/2013 - 25/12/2013", 1, "HOME", 1, "http://www.abc.com/", 1, 0)
		};
	
	
	public static void Save()
	{
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		//DatabaseManager.db.Open(Globals.dbSavePath);
		foreach (Poster a in postersList)
		{
			
			SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM Posters WHERE posterId = '" + a.posterId + "'" ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			if (hasRecord)
			{
				DatabaseManager.SharedManager.Save("UPDATE Posters SET "+
					"  posterPeriod = '" + a.period + "'"+
					" ,posterProbability = " + a.probability +
					" ,posterRedirectScene = '" + a.redirectScene + "'" +
					" ,rewardId = " + a.rewardId +
					" ,posterURL = '"+ a.posterURL + "'" +
					" ,posterLocation = " + a.posterLocation +
					" ,dlcId = " + a.dlcId +
					" WHERE posterId = " + a.posterId);
			}
			else
			{
				DatabaseManager.SharedManager.Save("INSERT INTO Posters (posterId, posterPeriod, posterProbability, posterRedirectScene, rewardId, posterURL, posterLocation, dlcId) VALUES ("+
					  a.posterId +
					" , '" + a.period + "'"+
					" , " + a.probability +
					" , '" + a.redirectScene + "'" +
					" , " + a.rewardId +
					" , '"+ a.posterURL + "'" +
					" , " + a.posterLocation + 
					" , " + a.dlcId + 
					")");
			}
		}
		//DatabaseManager.db.Close();
	}

	public static Poster GetPosterOfShop(Shop s)
	{
		Poster resultPoster = null;
		for (int i = 0; i < postersList.Count; i++)
		{
			Poster p = postersList[i];
			if (p.redirectScene.Trim().StartsWith("Shop"))
			{
				int shopNo = Int32.Parse(p.redirectScene.Trim().Remove(0,4));
				Shop posterShop = Shops.GetShopById(shopNo);

				if (posterShop.shopId == s.shopId)
				{
					resultPoster = p;
					break;
				}
			}
		}

		return resultPoster;
	}
	
	public static Poster GetPoster(int id)
	{
		foreach (Poster p  in postersList)
		{
			if (p.posterId == id)
				return p;
		}
		
		return null;
	}
	
	
	public static void Load()
	{
		postersList.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM Posters";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			postersList.Add(new Poster(
				qr.GetInteger("posterId"),
				qr.GetString("posterPeriod"),
				qr.GetInteger("posterProbability"),
				qr.GetString("posterRedirectScene"),
				qr.GetInteger("rewardId"),
				qr.GetString("posterURL"),
				qr.GetInteger("posterLocation"),
				qr.GetInteger("dlcId")
			));
			
		}
			
		qr.Release();
	}
}


public class Poster
{
	public int posterId;
	public string period;
	public int probability;
	public string redirectScene;
	public int rewardId;
	public string posterURL;
	public int posterLocation;
	public int dlcId;
	
	public Poster(int pPosterId, string pPeriod, int pProbability, string pRedirectScne, int pRewardId, string pURL, int pLocation, int pDlcId)
	{
		this.posterId = pPosterId;
		this.period = pPeriod;
		this.probability = pProbability;
		this.redirectScene = pRedirectScne;
		this.rewardId = pRewardId;
		this.posterURL = pURL;
		this.posterLocation = pLocation;
		this.dlcId = pDlcId;
	}
}
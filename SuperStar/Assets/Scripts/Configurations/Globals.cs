using System;
using System.IO;
using UnityEngine;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;

public class Globals
{
	/**
	 * Custom Defines:
	 * FGP - Fashion Girl Power
	 * SFG - Superstar Fashion Girl
	 * TC  - Top Celebrity
	 */
	
	//Settings - PlayerMeta Default Values
	public static readonly string			DEFAULT_canShowRatePopup		= "true";
	public static readonly string 			DEFAULT_settings_bgm 			= "true";
	public static readonly string 			DEFAULT_settings_sfx 			= "true";
	public static readonly string 			DEFAULT_settings_notification 	= "true";
	public static readonly string 			DEFAULT_settings_langCode 		= "EN";
	public static readonly string			DEFAULT_minimapPosition 		= "0|0";
	public static readonly string 			DEFAULT_visitedGameIds 			= "";
#if UNITY_IPHONE
	public static readonly int 				DEFAULT_dlcVersion 				= 7;
#else
	public static readonly int 				DEFAULT_dlcVersion 				= 0;
#endif
	public static readonly int				DEFAULT_energyInterval 			= 360;
	public static readonly int				DEFAULT_energyTimer 			= 360;
	public static readonly int				DEFAULT_streetPosition 			= 2637;
	public static readonly DateTime			DEFAULT_timeStamp 				= DateTime.UtcNow;
	public static readonly DateTime			DEFAULT_lastSyncDate 			= DateTime.UtcNow;
	public static readonly DateTime			DEFAULT_dailyBonusStamp 		= DateTime.UtcNow.AddDays(-1);
	public static readonly int				DEFAULT_dailyBonusDay 			= 0;		
	public static readonly int				DEFAULT_initializedPlayer 		= 0;
	public static readonly int				DEFAULT_numOfSessions			= 0;
	public static readonly int				DEFAULT_clothMatchTotalScore	= 0;
	public static readonly int				DEFAULT_clothMatchHighScore		= 0;
	public static readonly string			DEFAULT_startPlayTime			= "";
	public static readonly string			DEFAULT_isCracked				= "false";
	public static readonly string			DEFAULT_isIAP					= "false";
	public static readonly string			DEFAULT_hasFirstDLC				= "false";
	public static readonly string			DEFAULT_iapRecord				= "";
	public static readonly int 				DEFAULT_rougeAlpha				= 127;
	public static readonly int 				DEFAULT_eyeShadowAlpha			= 127;
	
	
	//Player Default Values
	public static readonly string			DEFAULT_playerName 				= "";
	public static readonly string			DEFAULT_profilePicURL 			= "http://www.yahoo.com.hk/logo.png";
	public static readonly int				DEFAULT_playerId				= -1;
	public static readonly int				DEFAULT_level 					= 1;
	public static readonly int				DEFAULT_exp 					= 0;
	public static readonly int				DEFAULT_energy 					= 20;
	public static readonly int				DEFAULT_maxEnergy 				= 20;
#if !UNITY_EDITOR
	public static readonly int				DEFAULT_coins 					= 15000;
	public static readonly int				DEFAULT_gems 					= 100;	
#else
	public static readonly int				DEFAULT_coins 					= 15000;
	public static readonly int				DEFAULT_gems 					= 100;
#endif
	
	public static readonly int				DEFAULT_boyfriendId				= -1;
	public static readonly Skill			DEFAULT_skills					= new Skill(new int[]{1,0,0,0,0});
	public static readonly int				DEFAULT_titleSkill				= 1;
	
	//Boyfriends
	public static readonly int				DEFAULT_flirtExp				= 0;
	public static readonly string			DEFAULT_rewardClaimed			= "0,0,0";
	public static readonly int				DEFAULT_rewardTimer				= -1;
	
	//PlayersToItems
	public static readonly string 			DEFAULT_wearingItems 			= 	"201,215";
	public static readonly string 			DEFAULT_purchasedItems 			=	"1,2,6,7,8,9,10,11,12,13," + //front hair
																				"21,26,27,28,29,55,56,57,58,59,60," + //back hair
																				"32,181,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,"+
																				"71,72,73,74,75,76,77,78," +//Eye ball
																				"180,105,"+//dress
																				"179,107,108,109,110,"+ // rouge
																				"114,178,115,116,118,171,172," +// skin color
																				"119,120,121,122,123,124,125,126,170," + // lips color	
																				"127,128,129,131,132,133,134,135,136," +  // eyeshawdow color
																				"137,138,139,140," +  // lips
																				"142,143,144,145,146,147,148,150," +  // eye shape
																				"156,158,159,160,162," +  // eyebrows
																				"201," +  //bra
																				//"184,"	+ //coat
																				"186,"	+ //handbag
																				"188,"	+ //hat
																				"190,319,"	+ //skirt
																				"192,278,"	+ //top
																					"215," +  //panties
																				"374,"	+ //socks
																				"414,417,427," +  //necklace
																				"339,"	+ //shoes
																				"381,"	+ //leggings
																				"665"   + //glasses
			/*"484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,513,514,515,516,536,537,538,560,561,562,563,582,602,603,623" +
			",667,668,669,670,671,672,674,675,677,678,680,681,682" + */
																				"";

	//testing for gift items
	//public static readonly string			DEFAULT_purchasedItems			= "93,101,104,229,230,231,233,441,464,394,399,401,405,417,424,426,427,336,345,346,285,286,287,288,291,299,300,303,252,254,268,277,313,320,321,393";
	//public static readonly string			DEFAULT_purchasedItems			= "471,472,473,474,475,476,477,478,479,480,481,482,483";



	//Quest
#if UNITY_ANDROID
	public static readonly string			DEFAULT_currentQuests   		= "101,132,133";
#elif UNITY_IPHONE
	public static readonly string			DEFAULT_currentQuests   		= "101,132,102";
#else
	public static readonly string			DEFAULT_currentQuests   		= "101,132,133";
#endif	
	public static readonly bool 			DEFAULT_scrolledQuest			= false;
	public static readonly int				IAP2XDuration					= 1;

	public static bool						hasChanagedClothesInCloset		= false;
	public static string					currentTimeStr					= "";
	public static string 					localTimeStr					= "";
	public static string 					iapRecord						= "";
	//public static bool						hasExecuteDLC					= false;


	public static bool						isCracked						= false;
	public static bool						hasFirstDLC						= false;
	public static bool						isIAP							= false;
	public static bool						canShowAds						= true;
	public static bool						refreshLocalNotification		= false;
	public static bool						IsDLCCompleted					= false;
	public static string					startPlayTime					= "";
	public static int 						currentDLCVersion 				= 0;
	public static int						apparentDLCVersion				= 0;
	public static string 					langCode 						= "EN";
	
#if FGP
	public static string 					fbPhotoAlbumName 				= "Fashion Girl Power Photos";
#elif SFG
	public static string 					fbPhotoAlbumName 				= "Superstar Fashion Girl Photos";
#elif TC
	public static string					fbPhotoAlbumName 				= "Top Celebrity Photos";
#endif


	//SNS 
#if UNITY_ANDROID
	
	#if SFG
		public static readonly string 			googlePlusClientId				= "574698034094-gpg8kpiekbtj82tplppra81kb7pbpeak.apps.googleusercontent.com";
	#elif FGP
		public static readonly string 			googlePlusClientId 				= "1060601407898-n5sakt1v7vnvdjtqurh8q5n3uuptvvlq.apps.googleusercontent.com";
	#endif	
	
#elif UNITY_IPHONE
	
	#if SFG
		public static readonly string			googlePlusClientId				= "574698034094-2b7nv5br9uva0td5bd3jfuckrf024n9d.apps.googleusercontent.com";
	#endif
	
#endif
	public static readonly string			twitterConsumerKey 				= "hYOqDXQXBbCGVpYtmEG8Fw";
	public static readonly string			twitterConsumerSecret 			= "Cd0E9j1eWifsqy97oRvowxaZHlDBJrJ9Yux0frkcVA";
	public static readonly string			facebookAppId					= "499957646757052";
	public static readonly string			facebookNameSpace				= "superstar_fashion_gl";
	public static readonly string			facebookFanPageId				= "476712692445516";


	// other things that no need to save
#if UNITY_ANDROID
	public static string					keyhash 						= "Ul+f7XGjj/jD62j0eTpT0rDxf84=";
	
	#if UNITY_AMAZON
		#if SFG
			//For Amazon
			public static string 					packageName						= "com.animoca.amazon.superstarfashiongirl";
			public static string					androidPrivacyURL				= "http://www.animoca.com/privacy_for_ac_android.html";
		#endif
	#else
		#if SFG
			//For Android
			public static string 					packageName						= "com.animocacollective.google.superstarFashionGirl";
			public static string					androidPrivacyURL				= "http://www.animoca.com/privacy_for_ac_android.html";
		#elif FGP
			public static string 					packageName						= "com.cybermind.google.superstarFashionGirl";
			public static string					androidPrivacyURL				= "http://cybermind.jp/privacy.html";
		#endif
	
		public static string					googlePlayURL					= "market://details?id=" + packageName;
	#endif
	
#elif UNITY_IPHONE
	
	public static string					bundleId						= "com.cybermind.itunes.superstarfashiongirl";
	public static string					iosPrivacyURL					= "http://cybermind.jp/privacy.html";
	public static string					appleAppId						= "778416978";
	public static string					itunesURL						= "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=" + appleAppId;
	
#endif
	
	//public static string					serverURL						= "http://stg-ac-superstarstory.outblaze.net/";
	public static string					serverURL						= "http://prod-ac-ssfashion.outblaze.net/";
	public static string					serverAPIURL					= serverURL + "API/";
	
#if SFG	
	public static string					appName							= "Superstar Fashion Girl";
#elif FGP
	public static string					appName							= "Fashion Girl Power";
#endif
	
	public static string 					dbName							= "SuperstarFashionGirl.db";
	public static string 					dbSavePath 						= Application.persistentDataPath + "/" + dbName;
	public static string 					dbKey                   		= "0x0102030405060708090a0b0c0d0e0f10";
	public static int						dbVersion						= 2;
	
#if UNITY_ANDROID
	public static string 					bundleVersion 					= "1.0.4.60";
#elif UNITY_IPHONE
	public static string 					bundleVersion 					= "1.0.3.55";
#endif
	
	//Debug Mode
	public static bool						isDebugMode						= true;
	public static bool						allowDebugLog					= true;
	
	public static GameObject globalCharacter;

	public static int miniGame_clothMatch_scoreForGift = 4000;

	public static List<Message> 			messages;
	public static Player 					mainPlayer 						= new Player();
	public static List<Item> 				wearingBackup					= new List<Item>();
	public static List<VisitRecord>			visitedGameIds					= new List<VisitRecord>();
	public static int						dlcProgress                     = 0;
	public static int 						takeoverShownCount 				= 0;
	public static bool 						shouldShowGameIdPopup 			= false;
	//public static bool						shouldShowDLCPopup				= false;
	public static bool 						settings_bgm 					= Boolean.Parse(DEFAULT_settings_bgm);
	public static bool 						settings_sfx 					= Boolean.Parse(DEFAULT_settings_sfx);
	public static bool 						settings_notification 			= Boolean.Parse(DEFAULT_settings_notification);
	public static bool						canShowRatePopup				= Boolean.Parse(DEFAULT_canShowRatePopup);
	

	//Topics
	public static Topic normalTopic;
	public static Topic specialTopic;
	public static Topic viewingTopic;
} 



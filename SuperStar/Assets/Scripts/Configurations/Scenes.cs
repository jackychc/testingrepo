using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum Scenes : int
{
	NONE 				= -1,
	INITDB 				= 0,
	TITLE 				= 1,
	HOME 				= 2,
	LOADING 			= 3,
	STREET 				= 4,
	JOB 				= 5,
	CLOSET 				= 6,
	PHOTOSHOT			= 7,
	//VOTING 				= 8,
	FRIENDSHOME			= 8,
	SHOP 				= 9,
	//VOTINGCLOSET 		= 11,
	//VOTINGLOBBY 		= 12,
	//VOTINGCAPTURE 		= 13,
	BOYFRIEND 			= 10,
	MINIGAME 	        = 11,
	MINIGAMECOINDROP 	= 12,
	MINIGAMECLOTHMATCH 	= 13,
	BOYFRIENDMAP        = 14,
	GROUPCHATSCENE		= 15,
	ZITEMCAPTURE        = 16
}



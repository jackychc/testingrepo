using System;
using UnityEngine;
using System.Collections.Generic;


public class Quests
{
	
	
	public static List<Quest> questsList = new List<Quest>();
	
	public static List<Quest> currentQuests = new List<Quest>();
	
	public static Quest[] initialQuestsList = {
			 // id, order,title,description,  running, complete,     new,   repeat,skip, action, scene.      time req.,time left, gem(skip), reward
		new Quest(101,1,"","quest_q101",      false,   false,  false, true, false,false, "Quest1", ""     ,         0,       -1,        2,       101),
		new Quest(102,2,"","quest_q102",      false,   false,  false, true, false, false, "Quest2", ""     ,         0,       -1,        2,       102),
		new Quest(103,3,"","quest_q103",      false,   false,  false, true, false, false, "Quest3", ""     ,         0,       -1,        2,       103),
		new Quest(104,4,"","quest_q104",      false,   false,  false, true, false, false, "Quest4", ""     ,         0,       -1,        2,       104),
		new Quest(105,5,"","quest_q105",      false,   false,  false, true, false, false, "Quest5", ""     ,         0,       -1,        2,       105),
		new Quest(106,6,"","quest_q106",      false,   false,  false, true, false, false, "Quest6", ""     ,         0,       -1,        2,       106),
		new Quest(107,7,"","quest_q107",      false,   false,  false, true, true, true, "Quest7", ""     ,         0,       -1,        2,       107),
		new Quest(108,8,"","quest_q107",      false,   false,  false, true, true, true, "Quest8", ""     ,         0,       -1,        2,       108),
		new Quest(109,9,"","quest_q107",      false,   false,  false, true, true, true, "Quest9", ""     ,         0,       -1,        2,       109),
		new Quest(110,10,"","quest_q110",     false,   false,  false, true, true, true, "Quest10", ""     ,         0,       -1,        2,       110),
		new Quest(111,11,"","quest_q111",     false,   false,  false, true, false, true, "Quest11", ""     ,         0,       -1,        2,       111),
		new Quest(112,12,"","quest_q111",     false,   false,  false, true, false, true, "Quest12", ""     ,         0,       -1,        2,       112),
		new Quest(113,13,"","quest_q113",     false,   false,  false, true, true, true, "Quest13", ""     ,         0,       -1,        2,       113),
		new Quest(114,14,"","quest_q114",     false,   false,  false, true, true, true, "Quest14", ""     ,         0,       -1,        2,       114),
		new Quest(115,15,"","quest_q114",     false,   false,  false, true, true, true, "Quest15", ""     ,         0,       -1,        2,       115),
		new Quest(116,16,"","quest_q114",     false,   false,  false, true,  true, true, "Quest16", ""     ,         0,       -1,        2,       116),
		new Quest(117,16,"","quest_q117",     false,   false,  false, true, true, true, "Quest17", ""     ,         0,       -1,        2,       117),
		new Quest(118,18,"","quest_q118",     false,   false,  false, true, true, true, "Quest18", ""     ,         0,       -1,        2,       118),
		new Quest(119,19,"","quest_q118",     false,   false,  false, true, true, true, "Quest19", ""     ,         0,       -1,        2,       119),
		new Quest(120,20,"","quest_q118",     false,   false,  false, true,  true, true, "Quest20", ""     ,         0,       -1,        2,       120),
		new Quest(121,21,"","quest_q121",     false,   false,  false, true, true, true, "Quest21", ""     ,         0,       -1,        2,       121),
		new Quest(122,22,"","quest_q121",     false,   false,  false, true, true, true, "Quest22", ""     ,         0,       -1,        2,       122),
		new Quest(123,23,"","quest_q121",     false,   false,  false, true,  true, true, "Quest23", ""     ,         0,       -1,        2,       123),
		new Quest(124,24,"","quest_q124",     false,   false,  false, true, true, true, "Quest24", ""     ,         0,       -1,        2,       124),
		new Quest(125,25,"","quest_q124",     false,   false,  false, true, true, true, "Quest25", ""     ,         0,       -1,        2,       125),
		new Quest(126,26,"","quest_q124",     false,   false,  false, true,  true, true, "Quest26", ""     ,         0,       -1,        2,       126),
		new Quest(127,27,"","quest_q127",     false,   false,  false, true,  true, true, "Quest27", ""     ,         0,       -1,        2,       127),

		new Quest(132,32,"","quest_q132",     false,   false,  false, true,  false, false, "Quest32", ""     ,         0,       -1,        2,       132),
		new Quest(133,33,"","quest_q133",     false,   false,  false, true,  false, false, "Quest33", ""     ,         0,       -1,        2,       133),

	};
	
	
	public static void Save()
	{
		// TODO :REWRITE
		
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;		
		
		foreach (Quest a in questsList)
		{
			
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM Quests WHERE questId = '" + a.activityId + "'",
				
				//update
				new List<string>(){
				"UPDATE Quests SET "+
				"  sortOrder = " + a.sortOrder +
				" ,isRunning = '" + a.isRunning.ToString() + "'" +
				" ,isCompleted = '" + a.isCompleted.ToString() +"'" +
				" ,completedOnce = '" + a.completedOnce.ToString() +"'" +
				" ,isNew = '" + a.isNew.ToString() +"'" +
				" ,canRepeat = '" + a.canRepeat.ToString() + "'" +
				" ,canSkip = '" + a.canSkip.ToString() +"'" +
				" ,actionClass = '" + a.stringAction + "'" +
				" ,goToSceneName = '" + a.goToSceneName + "'" +
				" WHERE questId = " + a.activityId
				,
				
				"UPDATE Activities SET "+
				"  title = '" + a.title + "'"+
				" ,description = '" + a.description + "'"+
				" ,timeRequired = " + a.timeRequired +
				" ,timeLeft = " + a.timeLeft +
				" ,gemsToFinish = " + a.gemsToFinish +
				" ,rewardId = " + a.rewardId +
				" WHERE activityId = " + a.activityId
				},
			
				//insert
				new List<string>(){
					"INSERT INTO Quests (questId, sortOrder, activityId, isRunning, isCompleted, completedOnce, isNew, canRepeat, canSkip, actionClass, goToSceneName) VALUES( "+
				       + a.activityId + 
					"  , " + a.sortOrder +
					"  , " + a.activityId +
					"  , '" + a.isRunning.ToString() + "'" +
					"  , '" + a.isCompleted.ToString() +"'" +
					"  , '" + a.completedOnce.ToString() +"'" +
					"  , '" + a.isNew.ToString() +"'" +
					"  , '" + a.canRepeat.ToString() + "'" +
					"  , '" + a.canSkip.ToString() +"'" +
					"  , '" + a.stringAction + "'" +
					"  , '" + a.goToSceneName + "'" +
				    ")"
					,
					"INSERT INTO Activities (activityId, title, description, timeRequired, timeLeft, gemsToFinish, rewardId) VALUES( "+
				                    a.activityId +
					", '" + a.title + "'"+
					" , '" + a.description + "'"+
					" , " + a.timeRequired +
					" , " + a.timeLeft +
					" , " + a.gemsToFinish +
					" , " + a.rewardId +
					" )"
				}
			);
			
			/*SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM Quests WHERE questId = '" + a.activityId + "'" ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			if (hasRecord)
			{
				// update
				DatabaseManager.SharedManager.Save("UPDATE Quests SET "+
				"  sortOrder = " + a.sortOrder +
				" ,isRunning = '" + a.isRunning.ToString() + "'" +
				" ,isCompleted = '" + a.isCompleted.ToString() +"'" +
				" ,completedOnce = '" + a.completedOnce.ToString() +"'" +
				" ,isNew = '" + a.isNew.ToString() +"'" +
				" ,canRepeat = '" + a.canRepeat.ToString() + "'" +
				" ,canSkip = '" + a.canSkip.ToString() +"'" +
				" ,actionClass = '" + a.stringAction + "'" +
				" ,goToSceneName = '" + a.goToSceneName + "'" +
				" WHERE questId = " + a.activityId);
								
				DatabaseManager.SharedManager.Save("UPDATE Activities SET "+
				"  title = '" + a.title + "'"+
				" ,description = '" + a.description + "'"+
				" ,timeRequired = " + a.timeRequired +
				" ,timeLeft = " + a.timeLeft +
				" ,gemsToFinish = " + a.gemsToFinish +
				" ,rewardId = " + a.rewardId +
				" WHERE activityId = " + a.activityId);
				
			}
			else
			{
				// insert
				DatabaseManager.SharedManager.Save("INSERT INTO Quests (questId, sortOrder, activityId, isRunning, isCompleted, completedOnce, isNew, canRepeat, canSkip, actionClass, goToSceneName) VALUES( "+
				       + a.activityId + 
				"  , " + a.sortOrder +
				"  , " + a.activityId +
				"  , '" + a.isRunning.ToString() + "'" +
				"  , '" + a.isCompleted.ToString() +"'" +
				"  , '" + a.completedOnce.ToString() +"'" +
				"  , '" + a.isNew.ToString() +"'" +
				"  , '" + a.canRepeat.ToString() + "'" +
				"  , '" + a.canSkip.ToString() +"'" +
				"  , '" + a.stringAction + "'" +
				"  , '" + a.goToSceneName + "'" +
			    ")");
			
				DatabaseManager.SharedManager.Save("INSERT INTO Activities (activityId, title, description, timeRequired, timeLeft, gemsToFinish, rewardId) VALUES( "+
				                    a.activityId +
					", '" + a.title + "'"+
				" , '" + a.description + "'"+
				" , " + a.timeRequired +
				" , " + a.timeLeft +
				" , " + a.gemsToFinish +
				" , " + a.rewardId +
				" )");
			}*/
		}
	}
	
	public static void Load()
	{
		// TODO :REWRITE
		
		questsList.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM Quests INNER JOIN Activities ON (Quests.activityId = Activities.activityId) ORDER BY sortOrder ASC";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			questsList.Add(new Quest(
				qr.GetInteger("activityId"),
				qr.GetInteger("sortOrder"),
				qr.GetString("title"),
				qr.GetString("description"),
				Boolean.Parse(qr.GetString("isRunning")),
				Boolean.Parse(qr.GetString("isCompleted")),
				Boolean.Parse(qr.GetString("completedOnce")),
				Boolean.Parse(qr.GetString("isNew")),
				Boolean.Parse(qr.GetString("canRepeat")),
				Boolean.Parse(qr.GetString("canSkip")),
				qr.GetString("actionClass"),
				"",
				
				qr.GetInteger("timeRequired"),
				qr.GetInteger("timeLeft"),
				qr.GetInteger("gemsToFinish"),
				qr.GetInteger("rewardId")
				));
			
		}
	
		
		qr.Release();
	}
	
	public static Quest GetQuest(int id)
	{	
		foreach (Quest q in questsList)
		{
			if (q.activityId == id)
				return q;
		}
		
		
		return null;
	}
	
	public static void RefreshQuestList()
	{
		List<Quest> setMe = new List<Quest>();
		
		foreach (Quest q in currentQuests)
		{
			if (q.isCompleted)
			{
				if (q.canRepeat)
				{
					q.isCompleted = false;
					q.isNew = true;
				}
			}
			else
			{
				setMe.Add(q);
			}
		}
		
		
		int countToAdd =  (3 - setMe.Count);
		
		//if (countToAdd > 0)
		//	StreetMenuManager.questExclaimation = true;
		
		for (int i = 0; i < countToAdd; i++)
		{
			
			
			Quest q = Quests.GetAQuest(setMe);
			
			if (q != null)
			{
				setMe.Add(q);
			}
		}
		
		
		
		currentQuests = setMe;
		
		foreach (Quest q in currentQuests)
		{
			q.ContinueQuest();
		}
	}
	
	public static void SkipQuest(Quest q)
	{
		for (int i =0; i < currentQuests.Count; i++)
		{
			if (currentQuests[i].activityId == q.activityId)
			{
				Quest t = Quests.GetAQuest(currentQuests);
				
				if (t != null)
				{
					currentQuests[i] = t;
					
					q.action.OnQuestStop();
					break;
				}
			}
		}
	}
	
	public static Quest GetAQuest(List<Quest> exclude)
	{
		System.Random rand = new System.Random();
		List<Quest> pool = Quests.ShuffleList<Quest>(questsList);
		
		Log.Debug("get a quest pool length" +pool.Count);
		
		foreach (Quest q in pool)
		{
			if (!exclude.Contains(q) &&
				q.action.AppearRequirement() &&
				!q.isCompleted)
			{
				return q;
			}
		}
		
		return null;
	}
	
	
	public static QuestAction getAction(string a)
	{
		return (QuestAction) Activator.CreateInstance(Type.GetType(a));
	}
	
	public static string intListToString(List<int> list)
	{

		string temp = "";
		foreach (int i in list)
		{
			if(!temp.Equals("")) temp += ",";
			temp += "" + i;
		}
		return temp;
	}
	public static List<int> stringToIntList(string st)
	{
		List<int> returnMe = new List<int>();
		
		if (st == null )return returnMe;
		
		if (!st.Equals(""))
		{
			string[] temp = st.Split(',');
				
			foreach (string s in temp)
			{
				
				try
				{
					returnMe.Add(Int32.Parse(s));
				}
				catch (Exception e)
				{
					Log.Debug(e);
				}
			}
		}
		
		return returnMe;
	}
	
	private static List<E> ShuffleList<E>(List<E> inputList)
	{
	     List<E> randomList = new List<E>();
		 List<E> copyList = new List<E>();
		foreach (E item in inputList)
		{
			copyList.Add(item);
		}
		
		// insert non-skippable quest first
		foreach (E item in copyList)
		{
			if (!((Quest) Convert.ChangeType(item, typeof(Quest))).canSkip)
				randomList.Add(item);
		}
	
	     System.Random r = new System.Random();
	     int randomIndex = 0;
	     while (copyList.Count > 0)
	     {
	          randomIndex = r.Next(0, copyList.Count); //Choose a random object in the list
			
			  if (!randomList.Contains(copyList[randomIndex]))
	         	 randomList.Add(copyList[randomIndex]); //add it to the new, random list
	          copyList.RemoveAt(randomIndex); //remove to avoid duplicates
	     }
		
	
	     return randomList; //return the new random list
	}
}



using System;
using UnityEngine;
using System.Collections.Generic;


public class Shops
{
	public static List<Shop> shopsList = new List<Shop>();
	
	public static List<Shop> initialShopsList = new List<Shop>() {
			// id, items
			new Shop(1, "I Love Sports" ,new List<int>(){/* //Dress
														 261,267//Top
														 //Skirt
														 //Trousers
														 //Shoes*/

														#if UNITY_ANDROID
															303,	//Skirt
															261,267,106,249,250,251,276,289,302,308,329,87,100,
															102,226,234,280,309,325,328,342,88,99,227,317,318,322,323,326,341
														#elif UNITY_IPHONE
															227,261,267,106,249,250,251,276,289,302,303,308,329,87,100,102,
															226,234,280,309,325,328,342,88,99,317,318,322,323,326,341
														#endif
														},
														"SuperstarStory_SportyStore", "ShopIcon1", ""), 
		
			new Shop(2,"Celebrity Style",new List<int>(){/*96,228,232, //Dress
														 193,255,275, //Top
														 301, //Skirt
														 314,324, //Trousers
														 330,332,335,349, //Shoes
														  //handbags
														  //Socks
													     381,382,383,384,385,386,387,388,389,397,398, //Leggings
													   	 415,421, //necklace
														 434,435,463,466//hat*/
														#if UNITY_ANDROID
															394,405,401,    //Legging
															254,277,        //Top
															321,299,		//Skirt
															345,			//Shoes
															96,193,228,232,255,275,301,314,324,330,332,335,349,382,383,384,385,386,387,388,389,397,398,415,421,434,435,
															463,466,83,97,103,194,306,315,331,350,390,391,392,395,396,400,402,403,406,418,432,442,451,458,461,465,448,81,
															82,84,85,86,89,191,253,256,257,258,259,260,264,281,282,292,294,295,304,305,337,338,347,348,356,359,361,375,376,
															377,378,379,380,404,407,408,409,411,412,413,425,431,443,445,447,449,452,453,457,459,460,462,469,90,91,262,263,265,
															266,269,270,271,272,273,274,279,283,284,290,293,296,297,307,310,311,312,316,333,352,410,437,444,450,454,470,
															//Glasses
															//667,668,669,670,671,672,674,675,677,678,680,681,682
														#elif UNITY_IPHONE
															96,193,228,232,254,255,275,277,299,301,314,321,324,330,332,335,345,349,382,383,384,385,386,387,388,389,394,397,
															398,401,405,415,421,434,435,463,466,83,97,103,194,306,315,331,350,390,391,392,395,396,400,402,403,406,418,432,442,
															448,451,458,461,465,81,82,84,85,86,89,191,253,256,257,258,259,260,264,281,282,292,294,295,304,305,337,338,347,348,
															356,359,361,375,376,377,378,379,380,404,407,408,409,411,412,413,425,431,443,445,447,449,452,453,457,459,460,462,
															469,90,91,262,263,265,266,269,270,271,272,273,274,279,283,284,290,293,296,297,307,310,311,312,316,333,352,410,437,
															444,450,454,470,484,486,490,514,536,538,560,563,582,603,623,667,668,669,670,671,672,674,675,677,678,680,681,682
														#endif
														}, 
														"SuperstarStory_SportyStore", "ShopIcon2", ""),
		
			new Shop(3,"Secret Garden"  ,new List<int>(){/*197,198,200,202, //Bra
													     214,221,222,223  //Panties*/
														#if UNITY_ANDROID
															197,198,200,202,214,221,222,223,183,195,196,204,210,211,212,218,199,203,205,206,207,208,213,216,217,220,224,225
														#elif UNITY_IPHONE
															197,198,199,200,202,214,221,222,223,183,195,196,204,210,211,212,218,203,205,206,207,208,213,216,217,220,224,225
														#endif
													    },
														"SuperstarStory_ElegantStore", "ShopIcon3", ""),
		
			new Shop(4,"Red Carpet"     ,new List<int>(){ 
														/* 92,98//Dress
														 //Shoes
														 //handbags
														 //Necklace
														//hat*/
														#if UNITY_ANDROID
															22,23,			//BackHair
															3,20,			//FrontHair
															92,98,334,343,416,420,422,423,428,430,94,95,235,237,238,239,240,241,242,243,245,246,247,248,340,344,357,360,364,369,419,
															429,436,438,439,440,446,455,467,468
														#elif UNITY_IPHONE
															3,20,22,23,92,98,334,343,416,420,422,423,428,430,94,95,235,237,238,239,240,241,242,243,245,246,247,248,340,344,357,360,364,369,419,429,436,438,439,440,446,455,
															467,468,491,494,495,496,497,499,561,562
														#endif
														},
														"SuperstarStory_ElegantStore", "ShopIcon4", ""),
		#if UNITY_IPHONE
			new Shop(5, "Shop 5"	, new List<int>(){
														487,488,489,515,602
													 	},
														"SuperstarStory_ElegantStore", "ShopIcon5", ""),

			new Shop(6, "Shop 6"	, new List<int>(){
														485,493,513,516,537
														},
														"SuperstarStory_ElegantStore", "ShopIcon6", "")
		#endif
		};
	
	
	public static void Save()
	{
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		//DatabaseManager.db.Open(Globals.dbSavePath);
		foreach (Shop a in shopsList)
		{
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM Shops WHERE shopId = '" + a.shopId + "'",
				
				//update
				new List<string>(){
				"UPDATE Shops SET "+
				"  shopName = '" + a.shopName + "',"+
				"  itemId = '" + Items.itemListToString(a.items) + "',"+
				"  bgmFilename = '" + a.bgmFilename + "'"+  ","+
				"  iconName = '" + a.iconName + "'"+  ","+
				"  hideSubcats = '" + a.hideSubcats + "'"+
				" WHERE shopId = " + a.shopId
				},
			
				//insert
				new List<string>(){
					"INSERT INTO Shops (shopid, shopName,itemId, bgmFilename, iconName, hideSubcats) VALUES ("+
					a.shopId +
				" , '" + a.shopName + "'"+
				" , '" + Items.itemListToString(a.items) + "'"+
				" , '" + a.bgmFilename + "'"+	
				" , '" + a.iconName + "'"+	
				" , '" + a.hideSubcats + "'"+	
				")"
				}
			);
			
			//Log.Debug("saving shops, item count: " + a.items.Count);
			/*SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM Shops WHERE shopId = '" + a.shopId + "'" ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			if (hasRecord)
			{
				DatabaseManager.SharedManager.Save("UPDATE Shops SET "+
				"  shopName = '" + a.shopName + "',"+
				"  itemId = '" + Items.itemListToString(a.items) + "',"+
				"  bgmFilename = '" + a.bgmFilename + "'"+  ","+
				"  iconName = '" + a.iconName + "'"+  ","+
				"  hideSubcats = '" + a.hideSubcats + "'"+
				" WHERE shopId = " + a.shopId);
			}
			else
			{
				DatabaseManager.SharedManager.Save("INSERT INTO Shops (shopid, shopName,itemId, bgmFilename, iconName, hideSubcats) VALUES ("+
					a.shopId +
				" , '" + a.shopName + "'"+
				" , '" + Items.itemListToString(a.items) + "'"+
				" , '" + a.bgmFilename + "'"+	
				" , '" + a.iconName + "'"+	
				" , '" + a.hideSubcats + "'"+	
				")");
			}*/
			
			
			
		}
		//DatabaseManager.db.Close();
	}
	
	
	public static void Load()
	{
		shopsList.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM Shops";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			shopsList.Add(new Shop(
				qr.GetInteger("shopId"),
				qr.GetString("shopName"),
				Items.stringToItemList(qr.GetString("itemId")),
				qr.GetString("bgmFilename"),
				qr.GetString("iconName"),
				qr.GetString("hideSubcats")
			));
			
		}
			
		qr.Release();
	}
	
	public static string itemListToString(List<int> list)
	{
		string temp = "";
		foreach (int i in list)
		{
			if(!temp.Equals("")) temp += ",";
			temp += "" + i;
		}
		
		return temp;
	}

	
	public static Shop GetShopById(int id)
	{
		foreach (Shop s in shopsList)
		{
			if (id == s.shopId)
				return s;
		}
		return null;
	}
	
	public static Shop getRandomShop()
	{
		List<Shop> pool = new List<Shop>();
		
		foreach(Shop s in shopsList)
		{
			//if ()
			{
				pool.Add(s);
			}
		}
		
		if (pool.Count > 0)
		{
		
			return pool[UnityEngine.Random.Range(0, pool.Count)];
		}
		
		// no match
		return null;
	}
	
	public static List<Shop> GetShopsWithSubcategories(List<ItemSubcategory> subcats)
	{
		List<Shop> returnMe = new List<Shop>();
		
		foreach (Shop s in Shops.shopsList)
		{
			if (s.shopId > 4) continue;

			if (!s.iconName.Equals("")) // do not include dlc shops
			{
				foreach (Item i in s.items)
				{
					if (subcats.Contains(ItemSubcategories.getSubcategory(i.subCategoryId)))
					{
						returnMe.Add(s);
						
						// next shop please
						break;
					}
				}
			}
		}
		
		return returnMe;
		
	}
}


public class Shop
{
	public int shopId;
	public string shopName;
	public List<int> itemIds;
	public List<Item> items;
	public string bgmFilename;
	public string iconName;
	public string hideSubcats;
	
	
	public Shop(int pShopId, string pShopName, List<Item> pItems, string pBgm, string pIcon, string pHideSubcats)
	{
		this.shopId = pShopId;
		this.shopName = pShopName;
		this.items = pItems;
		this.bgmFilename = pBgm;
		this.iconName = pIcon;
		this.hideSubcats = pHideSubcats;
	}
	public Shop(int pShopId, string pShopName, List<int> pItemIds, string pBgm, string pIcon, string pHideSubcats)
	{
		this.shopId = pShopId;
		this.shopName = pShopName;
		this.itemIds = pItemIds;
		this.items = new List<Item>();
		this.bgmFilename = pBgm;
		this.iconName = pIcon;
		this.hideSubcats = pHideSubcats;
		
		foreach (int i in pItemIds)
		{
			this.items.Add(Items.getItem(i));
		}
	}
	
	public Shop(int pShopId)
	{
		this.shopId =pShopId;
	}
}
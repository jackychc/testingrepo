using System;


public class VisitRecord
{
	public string gameId;
	public DateTime lastVisitTime;


	public VisitRecord (string pGameId, DateTime pLastVisitTime)
	{
		gameId = pGameId;
		lastVisitTime = pLastVisitTime;
	}

	public static VisitRecord FindRecordOfId(string pGameId)
	{
		foreach (VisitRecord r in Globals.visitedGameIds)
		{
			if (r.gameId.Equals(pGameId))
				return r;
		}

		return null;
	}

	public static string GetRecordsAsString()
	{
		string returnMe = "";

		foreach (VisitRecord r in Globals.visitedGameIds)
		{
			if (!returnMe.Equals("")) returnMe += "|";

			returnMe += r.gameId +","+
				r.lastVisitTime.Year + "-" +
				r.lastVisitTime.Month + "-"+
				r.lastVisitTime.Day + "-"+
				r.lastVisitTime.Hour + "-"+
				r.lastVisitTime.Minute + "-"+ 
				r.lastVisitTime.Second;
		}

		return returnMe;
	}
}



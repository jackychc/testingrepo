using System;
using System.Collections.Generic;


public class ItemCategories
{	
	public enum Id
	{
		
		TOP			= 1,
		DRESS		= 2,
		COAT		= 3,
		HAT         = 4,
		GLASSES     = 5,
		NECKLACE    = 6,
		HANDBAGS	= 7,
		BOTTOM		= 8,
		BRA      	= 9,
		PANTIES     = 10,
		SOCKS       = 11,
		LEGGINGS    = 12,
		SHOES		= 13,
		
		FRONTHAIR 	= 14,
		BACKHAIR 	= 15,
		SKINCOLOR	= 16,
		EYEBROWS	= 17,
		EYEMAKEUP	= 18,
		EYEBALLS	= 19,
		ROUGE		= 20,
		LIPS		= 21,
		PHOTOSHOTBG	= 22
	}
	
	
	public static List<ItemCategory> allCategories = new List<ItemCategory>()
	{
		//Character Assets
		new ItemCategory((int)Id.TOP, "Top", "Top", "closet_09_top_a",						"9", false),
		new ItemCategory((int)Id.BOTTOM, "Bottom", "Bottom", "closet_13_bottom_a",			"13", false),
		new ItemCategory((int)Id.DRESS, "Dress", "Dress", "closet_10_dress_a",				"10", false),
		new ItemCategory((int)Id.COAT, "Coat", "Coat", "closet_11_coat_a",					"11", false),
		new ItemCategory((int)Id.BRA, "Bra", "Bra", "closet_20_underwear_a",				"15", false),
		new ItemCategory((int)Id.PANTIES, "Panties", "Panties", "closet_21_underwearL_a",	"22", false),

		new ItemCategory((int)Id.HANDBAGS, "Hand Bags", "Hand Bags", "closet_17_handBags_a","17", false),
		new ItemCategory((int)Id.SOCKS, "Socks", "Socks", "closet_22_shortsock_a",			"16", false),
		new ItemCategory((int)Id.LEGGINGS, "Leggings", "Leggings", "closet_16_garter_a",	"27", false),
		new ItemCategory((int)Id.SHOES, "Shoes", "Shoes", "closet_14_shoes_a",				"14", false),

		new ItemCategory((int)Id.HAT, "Hat", "Hat", "closet_19_crown_a",					"23", true),
		new ItemCategory((int)Id.GLASSES, "Glasses", "Glasses", "closet_18_glasses_a",		"24", true),
		new ItemCategory((int)Id.NECKLACE, "Necklace", "Necklace","closet_12_accessories_a","25", true),
		
		new ItemCategory((int)Id.FRONTHAIR, "Front Hair", "Front Hair", "closet_01_frontHair_a",	"1", true),
		new ItemCategory((int)Id.BACKHAIR, "Back Hair", "Back Hair", "closet_02_backlHair_a",		"2", true),
		new ItemCategory((int)Id.SKINCOLOR, "Skin Color", "Skin Color", "closet_03_skinColor_a",	"3", true),
		new ItemCategory((int)Id.EYEBROWS, "Eye Brows", "Eye Brows", "closet_04_eyeBrows_a",		"4", true),
		new ItemCategory((int)Id.EYEMAKEUP, "Eye Makeup", "Eye Makeup", "closet_05_eyeMakeup_a",	"5", true),
		new ItemCategory((int)Id.EYEBALLS, "Eye Balls", "Eye Balls", "closet_06_eyesBall_a",		"6", true),
		new ItemCategory((int)Id.ROUGE, "Rouge", "Rouge", "closet_07_rouge_a",						"7", true),
		new ItemCategory((int)Id.LIPS, "Lips", "Lips", "closet_08_lips_a",							"8", true),
		
		//Non-Character Assets
		new ItemCategory((int)Id.PHOTOSHOTBG, "BG", "Backgound", "","21", false),
	};
	
	public static ItemCategory getCategory(int id)
	{
		foreach (ItemCategory ic in allCategories)
		{
			if (ic.categoryId == id)
			{
				return ic;
				
			}
		}
		
		return null;
	}
	
	public static ItemCategory getCategory(string s)
	{
		foreach (ItemCategory ic in allCategories)
		{
			if (ic.categoryName.Equals(s))
			{
				return ic;
				
			}
		}
		
		return null;
	}
	
	public static List<ItemSubcategory> getListOfSubcategoriesByCategory(int id)
	{
		foreach (ItemCategory ic in allCategories)
		{
			if (ic.categoryId == id)
			{
				return ic.listOfSubcategories;
				
			}
		}
		
		return null;
	}

}

public class ItemCategory
{
	
	public int categoryId;
	public string categoryName;
	public string categoryDescription;
	public string iconSpriteName;
	public List<ItemSubcategory> listOfSubcategories;
	public bool willZoom;
	
	public ItemCategory(int pCategoryId, string pCategoryName, string pCategoryDescription, string pIconSpriteName, string pListOfSubcategories, bool pWillZoom)
	{
		this.categoryId = pCategoryId;
		this.categoryName = pCategoryName;
		this.categoryDescription = pCategoryDescription;
		this.iconSpriteName = pIconSpriteName;
		this.willZoom = pWillZoom;
		this.listOfSubcategories = new List<ItemSubcategory>();

		foreach (String s in pListOfSubcategories.Split(','))
		{
			this.listOfSubcategories.Add(ItemSubcategories.getSubcategory(Int32.Parse(s)));
		}

	

	}
}



using System;
using UnityEngine;
using System.Collections.Generic;


public class Trainings
{
	public static List<Training> trainingsList = new List<Training>();
	
	public static Training[] initialTrainingsList = {
			
	};	
	
	public static void Save()
	{
		/*if (!DatabaseManager.SharedManager.IsInitialized)
			return;		
		
		foreach (Training a in trainingsList)
		{
			DatabaseManager.SharedManager.Save("UPDATE Trainings SET "+
				"  sortOrder = " + a.sortOrder +
				" WHERE trainingId = " + a.activityId);
			
			DatabaseManager.SharedManager.Save("UPDATE Activities SET "+
				"  title = '" + a.title + "'"+
				" ,description = '" + a.description + "'"+
				" ,timeRequired = " + a.timeRequired +
				" ,gemsToFinish = " + a.gemsToFinish +
				" ,rewardId = " + a.rewardId +
				" WHERE activityId = " + a.activityId);
		}*/
	}
	
	public static void Load()
	{
		trainingsList.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM Trainings INNER JOIN Activities ON (Trainings.activityId = Activities.activityId) ORDER BY sortOrder ASC";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			trainingsList.Add(new Training(
				qr.GetInteger("activityId"),
				qr.GetInteger("sortOrder"),
				qr.GetString("title"),
				qr.GetString("description"),
				qr.GetInteger("timeRequired"),
				qr.GetInteger("timeLeft"),
				qr.GetInteger("gemsToFinish"),
				qr.GetInteger("rewardId")
				));
			
		}
			
		qr.Release();
	}
}

public class Training: Activity
{
	

	public Training(int pTrainingId, int pSortOrder, string pTitle,
		string pDescription, int pTimeRequired, int pTimeLeft,
		int pGemsToFinish, int pRewardId)
	{
		this.activityId = pTrainingId;
		this.sortOrder = pSortOrder;
		this.title = pTitle;
		this.description = pDescription;
		this.timeRequired = pTimeRequired;
		this.timeLeft = pTimeLeft;
		this.gemsToFinish = pGemsToFinish;
		this.rewardId = pRewardId;
	}
}


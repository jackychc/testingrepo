using System;
using UnityEngine;
using System.Collections.Generic;


public class Features
{
	public static List<Feature> featuresList = new List<Feature>();
	
	public static void Save()
	{
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		//DatabaseManager.db.Open(Globals.dbSavePath);
		foreach (Feature a in featuresList)
		{
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM Features WHERE featureId = '" + a.featureId + "'",
				
				//update
				new List<string>(){
				"UPDATE Features SET "+
					"  featureName = \"" + a.featureName + "\""+
					" ,featureType = '" + a.featureType.ToString() + "'"+
					" ,coins = " + a.coins +
					" ,gems = " + a.gems +
					" ,discount = " + a.discount +
					" ,period = '" + a.period + "'" +
					" ,itemIds = '" + intListToString(a.itemIds) + "'" +
					" ,shopIds = '" + intListToString(a.shopIds) + "'" +
					" ,subcategoryIds = '" + intListToString(a.subcategoryIds) + "'" +			
					" ,iapPackageIds = '" + stringListToString(a.iapPackageIds) + "'" +
					" WHERE featureId = " + a.featureId
				},
			
				//insert
				new List<string>(){
					"INSERT INTO Features (featureId, featureName, featureType, coins, gems, discount, period, itemIds, shopIds, subcategoryIds, iapPackageIds) VALUES ("+
							a.featureId +
					" , \"" + a.featureName + "\""+
					" , '" + a.featureType.ToString() + "'"+
					" , " + a.coins +
					" , " + a.gems +
					" , " + a.discount +
					" , '" + a.period + "'" +
					" , '" + intListToString(a.itemIds) + "'" +
					" , '" + intListToString(a.shopIds) + "'" +
					" , '" + intListToString(a.subcategoryIds) + "'" +
					" , '" + stringListToString(a.iapPackageIds) + "'" +
					")"
				}
			);
			
			/*SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM Features WHERE featureId = '" + a.featureId + "'" ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			if (hasRecord)
			{
				DatabaseManager.SharedManager.Save("UPDATE Features SET "+
					"  featureName = \"" + a.featureName + "\""+
					" ,featureType = '" + a.featureType.ToString() + "'"+
					" ,coins = " + a.coins +
					" ,gems = " + a.gems +
					" ,discount = " + a.discount +
					" ,period = '" + a.period + "'" +
					" ,itemIds = '" + intListToString(a.itemIds) + "'" +
					" ,shopIds = '" + intListToString(a.shopIds) + "'" +
					" ,subcategoryIds = '" + intListToString(a.subcategoryIds) + "'" +			
					" ,iapPackageIds = '" + stringListToString(a.iapPackageIds) + "'" +
					" WHERE featureId = " + a.featureId);
			}
			else
			{
				DatabaseManager.SharedManager.Save("INSERT INTO Features (featureId, featureName, featureType, coins, gems, discount, period, itemIds, shopIds, subcategoryIds, iapPackageIds) VALUES ("+
							a.featureId +
					" , \"" + a.featureName + "\""+
					" , '" + a.featureType.ToString() + "'"+
					" , " + a.coins +
					" , " + a.gems +
					" , " + a.discount +
					" , '" + a.period + "'" +
					" , '" + intListToString(a.itemIds) + "'" +
					" , '" + intListToString(a.shopIds) + "'" +
					" , '" + intListToString(a.subcategoryIds) + "'" +
					" , '" + stringListToString(a.iapPackageIds) + "'" +
					")");
			}*/
		}
		//DatabaseManager.db.Close();
	}

	
	public static void Load()
	{
		featuresList.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM Features";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			featuresList.Add(new Feature(
				qr.GetInteger("featureId"),
				qr.GetString("featureName"),
				qr.GetString("featureType"),
				qr.GetInteger("coins"),
				qr.GetInteger("gems"),
				qr.GetInteger("discount"),
				qr.GetString("period"),
				stringToIntList(qr.GetString("itemIds")),
				stringToIntList(qr.GetString("shopIds")),
				stringToIntList(qr.GetString("subcategoryIds")),
				stringToStringList(qr.GetString("iapPackageIds"))
			));
			
		}
		
		Log.Warning("Feature List Count: " + featuresList.Count);
			
		qr.Release();
	}
	
	public static string intListToString(List<int> list)
	{

		string temp = "";
		foreach (int i in list)
		{
			if(!temp.Equals("")) temp += ",";
			temp += "" + i;
		}
		return temp;
	}
	
	public static List<int> stringToIntList(string st)
	{
		List<int> returnMe = new List<int>();
		
		if (st == null )return returnMe;
		
		if (!st.Equals(""))
		{
			string[] temp = st.Split(',');
				
			foreach (string s in temp)
			{
				
				try
				{
					returnMe.Add(Int32.Parse(s));
				}
				catch (Exception e)
				{
					Log.Warning(e);
				}
			}
		}
		
		return returnMe;
	}
	
	public static string stringListToString(List<string> list)
	{
		string temp = "";
		foreach (string s in list)
		{
			if(!temp.Equals("")) temp += ",";
			temp += s;
		}
		return temp;
	}
	
	public static List<string> stringToStringList(string st)
	{
		List<string> returnMe = new List<string>();
		
		if (st == null )return returnMe;
		
		if (!st.Equals(""))
		{
			string[] temp = st.Split(',');
				
			foreach (string s in temp)
			{				
				try
				{
					returnMe.Add(s);
				}
				catch (Exception e)
				{
					Log.Exception(e);
				}
			}
		}
		
		return returnMe;
	}
	
	public static Feature GetFeature(int id)
	{
		foreach (Feature p  in featuresList)
		{
			if (!p.isRunning()) continue;
			
			if (p.featureId == id)
				return p;
		}
		
		return null;
	}
	
	public static Feature GetFeatureOfShop(Shop shop)
	{
		foreach (Feature p in featuresList)
		{
			if (!p.isRunning()) 
				continue;
			
			if (!p.featureType.Equals(Feature.FEATURETYPE.SHOP))
				continue;
			
			if (p.shopIds != null && !p.shopIds.Equals(""))
			{
				if (p.shopIds.Contains(shop.shopId))
					return p;			
			}
		}
		
		return null;
	}
	
	public static Feature GetFeatureOfShopCat(Shop shop, ItemSubcategory subcat)
	{
		foreach	(Feature p in featuresList)
		{
			if (!p.isRunning()) 
				continue;	
			
			if (!p.featureType.Equals(Feature.FEATURETYPE.SHOPSUBCAT))
				continue;
			
			if (p.shopIds.Contains(shop.shopId) && p.subcategoryIds.Contains(subcat.subcategoryid))
				return p;
		}
		return null;
	}
	
	public static Feature GetFeatureOfItem(Item item)
	{
		foreach (Feature p in featuresList)
		{
			if (!p.isRunning()) continue;
			
			if (p.itemIds.Contains(item.itemId))
				return p;
			
			if (p.subcategoryIds.Contains(item.subCategoryId))
				return p;
		}
		
		return null;
	}
	
	public static Feature GetFeatureOfIAPPackageId(string packageId)
	{
		foreach (Feature f in featuresList)
		{
			if (!f.isRunning())
				continue;
			if (!f.featureType.Equals(Feature.FEATURETYPE.IAP))
				continue;
			
			if (f.iapPackageIds.Contains(packageId))
				return f;
		}
		
		return null;
	}
}


public class Feature
{
	public enum FEATURETYPE
	{
		ITEM,
		SHOP,
		SUBCATEGORY,
		SHOPSUBCAT,
		IAP
	}
	
	public bool isRunning()
	{
		
		string[] dates = period.Split('|');
		
		//string[] startDate = dates.Split('-');
		
		string[] startDateStr = dates[0].Split('-');
		string[] endDateStr = dates[1].Split('-');
		
		DateTime startDate = new DateTime(Int32.Parse(startDateStr[0]), Int32.Parse(startDateStr[1]), Int32.Parse(startDateStr[2]), 0,0,0);
		DateTime endDate = new DateTime(Int32.Parse(endDateStr[0]), Int32.Parse(endDateStr[1]), Int32.Parse(endDateStr[2]), 0,0,0);
		DateTime now = DateTime.UtcNow;
		//yield return StartCoroutine(GetSystemTime(result => now = result));
		TimeSpan spanA = now - startDate;
		TimeSpan spanB = endDate - now;
		//output((int) (span.TotalSeconds));
			
		
		return ((int)spanA.TotalSeconds > 0 && (int)spanB.TotalSeconds > 0);
	}
	
	public TimeSpan CalculateRemainSeconds()
	{
		
		string[] dates 		= period.Split('|');
		string endDateStr 	= dates[1];
		DateTime endDate 	= Convert.ToDateTime(endDateStr);
		DateTime now 		= DateTime.UtcNow;
		TimeSpan spanB 		= endDate - now;
		//output((int) (span.TotalSeconds));
			
		
		return spanB;
	}
	
	public DateTime StartDate
	{
		get
		{
			string[] dates = period.Split('|');		
			DateTime startTime = Convert.ToDateTime(dates[0]);
			
			//Log.Debug("Year: "+ startTime.Year + ", Month: " +startTime.Month + " , Day "+ startTime.Day);
			
			return startTime;
		}
	}
	
	public DateTime EndDate
	{
		get
		{
			string[] dates = period.Split('|');
			DateTime endTime = Convert.ToDateTime(dates[1]);
		
			return endTime;
		}
	}
	
	public int featureId;
	public string featureName;
	public FEATURETYPE featureType = FEATURETYPE.ITEM;
	public int coins;
	public int gems;
	public int discount;
	public string period;
	
	public List<int> itemIds;
	public List<int> shopIds;
	public List<int> subcategoryIds;
	public List<string> iapPackageIds;
	
	public Feature(int pFeatureId, string pFeatureName, string pType, int pCoins, int pGems, int pDiscount, string pPeriod, List<int> pItemIds, List<int> pShopIds, List<int> pSubcategoryIds)
	{
		this.featureId = pFeatureId;
		this.featureName = pFeatureName;
		
		if (pType.Equals("item"))
			this.featureType = FEATURETYPE.ITEM;
		else if (pType.Equals("shop"))
			this.featureType = FEATURETYPE.SHOP;
		else if (pType.Equals("subcategories"))
			this.featureType = FEATURETYPE.SUBCATEGORY;
		else if (pType.Equals("iap"))
			this.featureType = FEATURETYPE.IAP;
		else if (pType.Equals("shopsubcat"))
			this.featureType = FEATURETYPE.SHOPSUBCAT;
		
		
		this.coins = pCoins;
		this.gems = pGems;
		this.discount = pDiscount;
		this.period = pPeriod;
		this.itemIds = pItemIds;
		this.shopIds = pShopIds;
		this.subcategoryIds = pSubcategoryIds;
	}
	
	public Feature(int pFeatureId, string pFeatureName, string pType, int pCoins, int pGems, int pDiscount, string pPeriod, List<int> pItemIds, List<int> pShopIds, List<int> pSubcategoryIds, List<string> pIAPPackageIds)
		: this(pFeatureId, pFeatureName, pType, pCoins, pGems, pDiscount, pPeriod, pItemIds, pShopIds, pSubcategoryIds)
	{
		this.iapPackageIds = pIAPPackageIds;
	}
}
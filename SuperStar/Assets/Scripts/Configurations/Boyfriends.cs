

using System;
using UnityEngine;
using System.Collections.Generic;


public class Boyfriends
{
	public static List<int> checkPoints = new List<int>(){50400, 43200, 0};
	
	public static List<Boyfriend> boyfriendsList = new List<Boyfriend>();
	
	public static List<Boyfriend> initialBoyfriendsList = new List<Boyfriend>() {
			// id, name,         	 		title,		meta    ,mesh, 		exp ,maxexp,charisma,       location,               rewards,                       flirtCost,            flirtRepeat                 gemToFinish                rewardClaimed  timer dlc	
	//tier 1	
			new Boyfriend(1,"Will", 	"Tennis Player", "", "BF_tennis_mesh",  0 , 10000,    0,    new List<int>(){1,2}, new List<int>(){37,26,42}, new List<int>(){1,300,2}, new List<int>(){30,20,15}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 0),
			new Boyfriend(2,"Charlie",  "Student", "","BF_normal_mesh",			0 , 10000,    0,    new List<int>(){1,2}, new List<int>(){52,26,59}, new List<int>(){1,300,2}, new List<int>(){30,20,15}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 0),
			new Boyfriend(3,"Sam", 	  	"Captain", "","BF_pilot_mesh",  		0 , 10000,    0,    new List<int>(){3,4}, new List<int>(){50,26,61}, new List<int>(){1,300,2}, new List<int>(){30,20,15}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 0),
			new Boyfriend(8,"Rich",    	"Banker", "",    "BF_007_mesh",   		0 , 10000,    0,    new List<int>(){3,4}, new List<int>(){44,26,60}, new List<int>(){1,300,2}, new List<int>(){30,20,15}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 0),
	
		//tier 2
			new Boyfriend(7,"Jim",    	"Werewolf", "",  "BF_wolverine_mesh",  	0 , 10000,   40,    new List<int>(){1,2}, new List<int>(){51,29,56}, new List<int>(){1,300,2}, new List<int>(){50,34,25}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 3),
			new Boyfriend(11,"Toby",    	"Bear", "",    "BF_Ted_mesh",   	0 , 10000,   40,    new List<int>(){1,2}, new List<int>(){39,29,58}, new List<int>(){1,300,2}, new List<int>(){50,34,25}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 3),
			new Boyfriend(13,"Jammy",   "Chef", "",  "BF_jamie_mesh",    		0 , 10000,   40,    new List<int>(){1,2}, new List<int>(){38,29,55}, new List<int>(){1,300,2}, new List<int>(){50,34,25}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 3),
			new Boyfriend(18,"Carl", 	"Hero", "","BF_superman_mesh",    		0 , 10000,   40,    new List<int>(){3,4}, new List<int>(){45,29,46}, new List<int>(){1,300,2}, new List<int>(){50,34,25}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 3),
			
		
	//tier 3
			new Boyfriend(16,"Dave", 	"Football Star", "","BF_beckham_mesh",  0 , 10000,   70,    new List<int>(){3,4}, new List<int>(){41,32,47}, new List<int>(){2,600,4}, new List<int>(){42,27,20}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 3),
			new Boyfriend(17,"Baby", 	"Pop Star", "","BF_justin_mesh",    	0 , 10000,   70,    new List<int>(){1,2}, new List<int>(){40,32,48}, new List<int>(){2,600,4}, new List<int>(){42,27,20}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 3),
			//new Boyfriend(6,"Pys",   	"Kpop Star", "",    "BF_psy_mesh",   	0 , 10000,   70,    new List<int>(){3,4}, new List<int>(){43,32,57}, new List<int>(){2,600,4}, new List<int>(){42,27,20}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 0),
			new Boyfriend(19,"Thor",  "Lord of Storms", "", "BF_thor_mesh",  	0 , 10000,   70,    new List<int>(){3,4}, new List<int>(){49,32,54}, new List<int>(){2,600,4}, new List<int>(){42,27,20}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 3),
			new Boyfriend(20,"Toni",  "Iron Nutcracker", "", "BF_nut_mesh",  	0 , 10000,   70,    new List<int>(){1,2}, new List<int>(){62,35,63}, new List<int>(){2,600,4}, new List<int>(){42,27,20}, new List<int>(){0,79,479}, new List<int>(){0,0,0}, -1, 3)
	// tier 4


		
	};
	

	public static void Save()
	{
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		//DatabaseManager.db.Open(Globals.dbSavePath);
		foreach (Boyfriend a in boyfriendsList)
		{
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM Boyfriends WHERE boyfriendId = '" + a.boyfriendId + "'",
				
				//update
				new List<string>(){
				"UPDATE Boyfriends SET "+
					"  boyfriendName = '" + a.boyfriendName + "'"+
					" ,meshName = '" + a.meshName + "'"+
					" ,title = '" + a.title + "'"+
					" ,meta = '" + a.meta + "'"+
					" ,flirtExp = " + a.flirtExp +
					" ,flirtExpMax = " + a.maxFlirtExp +
					" ,charismaRequired = " + a.charismaRequired +
					" ,location = '"+ intListToString(a.location) + "'" +
					" ,rewardIds = '" + intListToString(a.rewardIds) + "'" +
					" ,flirtCost = '" + intListToString(a.flirtCost) + "'" +
					" ,flirtRepeat = '" + intListToString(a.flirtRepeat) + "'" +
					" ,instantGiftCost = '" + intListToString(a.instantGiftCost) + "'" +
					" ,rewardClaimed = '" + intListToString(a.rewardClaimed) + "'" +
					" ,rewardTimer = " + a.rewardTimer +
					" ,dlcId = " + a.dlcId +
					" WHERE boyfriendId = " + a.boyfriendId
				},
			
				//insert
				new List<string>(){
					"INSERT INTO Boyfriends (boyfriendId, boyfriendName, meshName, title, meta, flirtExp, flirtExpMax, charismaRequired, location, rewardIds, flirtCost, flirtRepeat, instantGiftCost, rewardClaimed, rewardTimer, dlcId) VALUES ("+
					  a.boyfriendId +
					" , '" + a.boyfriendName + "'"+
					" , '" + a.meshName + "'"+
					" , '" + a.title + "'"+
					" , '" + a.meta + "'"+
					" , " + a.flirtExp +
					" , " + a.maxFlirtExp +
					" , " + a.charismaRequired +
					" , '"+ intListToString(a.location) + "'" +
					" , '" + intListToString(a.rewardIds) + "'" +
					" , '" + intListToString(a.flirtCost) + "'" +
					" , '" + intListToString(a.flirtRepeat) + "'" +
					" , '" + intListToString(a.instantGiftCost) + "'" +
					" , '" + intListToString(a.rewardClaimed) + "'" +
					" , " + a.rewardTimer +
					" , " + a.dlcId +
					")"
				}
			);
			
			/*SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM Boyfriends WHERE boyfriendId = '" + a.boyfriendId + "'" ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			if (hasRecord)
			{
				DatabaseManager.SharedManager.Save("UPDATE Boyfriends SET "+
					"  boyfriendName = '" + a.boyfriendName + "'"+
					" ,meshName = '" + a.meshName + "'"+
					" ,title = '" + a.title + "'"+
					" ,meta = '" + a.meta + "'"+
					" ,flirtExp = " + a.flirtExp +
					" ,flirtExpMax = " + a.maxFlirtExp +
					" ,charismaRequired = " + a.charismaRequired +
					" ,location = '"+ intListToString(a.location) + "'" +
					" ,rewardIds = '" + intListToString(a.rewardIds) + "'" +
					" ,flirtCost = '" + intListToString(a.flirtCost) + "'" +
					" ,flirtRepeat = '" + intListToString(a.flirtRepeat) + "'" +
					" ,instantGiftCost = '" + intListToString(a.instantGiftCost) + "'" +
					" ,rewardClaimed = '" + intListToString(a.rewardClaimed) + "'" +
					" ,rewardTimer = " + a.rewardTimer +
					" ,dlcId = " + a.dlcId +
					" WHERE boyfriendId = " + a.boyfriendId);
			}
			else
			{
				DatabaseManager.SharedManager.Save("INSERT INTO Boyfriends (boyfriendId, boyfriendName, meshName, title, meta, flirtExp, flirtExpMax, charismaRequired, location, rewardIds, flirtCost, flirtRepeat, instantGiftCost, rewardClaimed, rewardTimer, dlcId) VALUES ("+
					  a.boyfriendId +
					" , '" + a.boyfriendName + "'"+
					" , '" + a.meshName + "'"+
					" , '" + a.title + "'"+
					" , '" + a.meta + "'"+
					" , " + a.flirtExp +
					" , " + a.maxFlirtExp +
					" , " + a.charismaRequired +
					" , '"+ intListToString(a.location) + "'" +
					" , '" + intListToString(a.rewardIds) + "'" +
					" , '" + intListToString(a.flirtCost) + "'" +
					" , '" + intListToString(a.flirtRepeat) + "'" +
					" , '" + intListToString(a.instantGiftCost) + "'" +
					" , '" + intListToString(a.rewardClaimed) + "'" +
					" , " + a.rewardTimer +
					" , " + a.dlcId +
					")");
			}*/
		}
		//DatabaseManager.db.Close();
	}

	
	public static void Load()
	{
		boyfriendsList.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM Boyfriends";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			boyfriendsList.Add(new Boyfriend(
				qr.GetInteger("boyfriendId"),
				qr.GetString("boyfriendName"),
				qr.GetString("title"),
				qr.GetString("meta"),
				qr.GetString("meshName"),
				qr.GetInteger("flirtExp"),
				qr.GetInteger("flirtExpMax"),
				qr.GetInteger("charismaRequired"),
				stringToIntList(qr.GetString("location")),
				stringToIntList(qr.GetString("rewardIds")),
				stringToIntList(qr.GetString("flirtCost")),
				stringToIntList(qr.GetString("flirtRepeat")),
				stringToIntList(qr.GetString("instantGiftCost")),
				stringToIntList(qr.GetString("rewardClaimed")),
				qr.GetInteger("rewardTimer"),
				qr.GetInteger("dlcId")
			));
			
		}
			
		qr.Release();
	}
	
	public static string intListToString(List<int> list)
	{

		string temp = "";
		foreach (int i in list)
		{
			if(!temp.Equals("")) temp += ",";
			temp += "" + i;
		}
		return temp;
	}
	public static List<int> stringToIntList(string st)
	{
		List<int> returnMe = new List<int>();
		
		if (st == null )return returnMe;
		
		if (!st.Equals(""))
		{
			string[] temp = st.Split(',');
				
			foreach (string s in temp)
			{
				
				try
				{
					returnMe.Add(Int32.Parse(s));
				}
				catch (Exception e)
				{
					Log.Debug(e);
				}
			}
		}
		
		return returnMe;
	}
	
	public static Boyfriend GetBoyfriend(int id)
	{
		foreach (Boyfriend p  in boyfriendsList)
		{
			if (p.boyfriendId == id)
				return p;
		}
		
		return null;
	}

	
	public static Boyfriend GetBoyfriendByCriteria(int loc, int charisma, List<Boyfriend> exclude)
	{
		
		
		
		List<Boyfriend> pool = new List<Boyfriend>();
		
		foreach (Boyfriend p in boyfriendsList)
		{
			if (p.location.Contains(loc) && charisma >= p.charismaRequired && p.rewardTimer == -1 && !exclude.Contains(p))
				pool.Add(p);
		}
		
		if (pool.Count > 0)
		{
		
			return pool[UnityEngine.Random.Range(0, pool.Count)];
		}
		
		// no match, random one
		
		for (int i = 0; i < 20; i++)
		{
			Boyfriend ran = boyfriendsList[UnityEngine.Random.Range(0, boyfriendsList.Count)];
			if (!exclude.Contains(ran) && ran.location.Contains(loc))
			{
				return ran;
			}
		}
		
		// still no match
			return null;
	}
	
	public static Boyfriend GetBoyfriendByCriteria(int charisma)
	{
		List<Boyfriend> pool = new List<Boyfriend>();
		
		foreach (Boyfriend p in boyfriendsList)
		{
			if (charisma >= p.charismaRequired && p.flirtExp < p.maxFlirtExp)
				pool.Add(p);
		}
		
		if (pool.Count > 0)
		{
		
			return pool[UnityEngine.Random.Range(0, pool.Count)];
		}
		
		// no match
		return null;
	}
	
	public static Texture2D GetIconOfBoyfriend(Boyfriend i)
	{
		Texture2D returnMe = null;
		returnMe =  (Texture2D) Resources.Load("Boyfriends/itemIcon/bf_"+i.boyfriendId+"_icon");
	
		return returnMe;
	}
}


public class Boyfriend
{
	public int boyfriendId;
	public string boyfriendName;
	public string meshName;
	public string title;
	public string meta;
	public int flirtExp;
	public int maxFlirtExp;
	public int charismaRequired;
	public List<int> location;
	public List<int> rewardIds;
	public List<int> flirtCost;
	public List<int> flirtRepeat;
	public List<int> instantGiftCost;
	public List<int> rewardClaimed; // 0/1
	public int rewardTimer;
	public int dlcId;
	
	// for dlc use
	public string iconURL;
	public string md5Hash;
	public string bundleUrl;
	
	
	public Boyfriend(int pBfId)
	{
		this.boyfriendId = pBfId;
	}
	
	public Boyfriend(int pBfId, string pBfName, string pTitle, string pMeta, string pMeshName, int pFlirtExp, int pMaxFlirtExp, int pCharismaReq, List<int> pLocation, List<int> pRewards, List<int> pFlirtCost, List<int> pFlirtRepeat, List<int> pInstantGiftCost, List<int> pRewardClaimed, int pTimer, int pDlc)
	{
		this.boyfriendId = pBfId;
		this.boyfriendName = pBfName;
		this.title = pTitle;
		this.meta = pMeta;
		this.meshName = pMeshName;
		this.flirtExp = pFlirtExp;
		this.maxFlirtExp = pMaxFlirtExp;
		this.charismaRequired = pCharismaReq;
		this.location = pLocation;
		this.rewardIds = pRewards;
		this.flirtCost = pFlirtCost;
		this.flirtRepeat = pFlirtRepeat;
		this.instantGiftCost = pInstantGiftCost;
		this.rewardClaimed = pRewardClaimed;
		this.rewardTimer = pTimer;
		this.dlcId = pDlc;
	}
}
using System;
using UnityEngine;
using System.Collections.Generic;


public class Jobs
{
	public static List<Job> jobsList = new List<Job>();
	
	public static Job[] initialJobsList = {
			//      id, order, title,                description, lv, time req, time left, expire, gem, reward, visible
			new Job(1,  1,    "Tennis Player Job 1", "do it!",    1,  120,       -1,       120+3600,  1,   1,      "0"),
			new Job(2,  2,    "Tennis Player Job 2", "do it!",    2,  600,       -1,       600+3600,  6,   2,      "0"),
			new Job(3,  3,    "Tennis Player Job 3", "do it!",    3,  1800,      -1,      1800+3600,  19,  3,      "0"),
			new Job(4,  4,    "Tennis Player Job 4", "do it!",    4,  3600,      -1,      3600+3600,  39,  4,      "0"),
		    new Job(5,  5,    "Tennis Player Job 5", "do it!",    5,  10800,     -1,     10800+5400,  119, 5,      "0"),
			new Job(6,  6,    "Tennis Player Job 6", "do it!",    6,  21600,     -1,    21600+10800,  239, 6,      "0"),
			new Job(7,  7,    "Tennis Player Job 7", "do it!",    7,  36000,     -1,    36000+18000,  399, 7,      "0"),
			new Job(8,  8,    "Tennis Player Job 8", "do it!",    8,  57600,     -1,    57600+28800,  639, 8,      "0"),
		
			new Job(9,   1,    "Dancer Job 1",       "do it!",    3,  300,       -1,       300+3600,  2,   9,      "1"),
			new Job(10,  2,    "Dancer Job 2", 		 "do it!",    3,  900,       -1,       900+3600,  9,   10,     "1"),
			new Job(11,  3,    "Dancer Job 3", 		 "do it!",    4,  1800,      -1,      1800+3600,  29,  11,     "1"),
			new Job(12,  4,    "Dancer Job 4", 		 "do it!",    5,  7200,      -1,      7200+3600,  79,  12,     "1"),
		    new Job(13,  5,    "Dancer Job 5", 		 "do it!",    6,  14400,     -1,     14400+7200,  159, 13,     "1"),
			new Job(14,  6,    "Dancer Job 6", 		 "do it!",    7,  28800,     -1,    28800+14400,  319, 14,     "1"),
			new Job(15,  7,    "Dancer Job 7", 		 "do it!",    8,  43200,     -1,    43200+21600,  479, 15,     "1"),
			new Job(16,  8,    "Dancer Job 8", 		 "do it!",    9,  72000,     -1,    72000+36000,  799, 16,     "1"),
		
			new Job(17,  1,    "Singer Job 1",       "do it!",    5,  900,       -1,       900+3600,  9,   17,     "4"),
			new Job(18,  2,    "Singer Job 2", 		 "do it!",    5,  1800,      -1,      1800+3600,  19,  18,     "4"),
			new Job(19,  3,    "Singer Job 3", 		 "do it!",    5,  3600,      -1,      3600+3600,  39,  19,     "4"),
			new Job(20,  4,    "Singer Job 4", 		 "do it!",    5,  7200,      -1,      7200+3600,  79,  20,     "4"),
		    new Job(21,  5,    "Singer Job 5", 		 "do it!",    6,  14400,     -1,     14400+7200,  159, 21,     "4"),
			new Job(22,  6,    "Singer Job 6", 		 "do it!",    7,  28800,     -1,    28800+14400,  319, 22,     "4"),
			new Job(23,  7,    "Singer Job 7", 		 "do it!",    8,  43200,     -1,    43200+21600,  479, 23,     "4"),
			new Job(24,  8,    "Singer Job 8", 		 "do it!",    9,  86400,     -1,    86400+43200,  959, 24,     "4"),
		
			/*//      id, order, title,         description, lv, time req, time left, expire, gem, reward, visible
			new Job(5,  1,    "Dancer Job 1", "do it!",    3,  300,       -1,       300+3600,  2,   5,      "1"),
			new Job(6,  2,    "Dancer Job 2", "do it!",    3,  900,       -1,       900+3600,  9,   6,      "1"),
			new Job(7,  3,    "Dancer Job 3", "do it!",    4,  2700,      -1,       2700+3600,  29,  7,      "1"),
			new Job(8,  4,    "Dancer Job 4", "do it!",    5,  7200,      -1,       7200+3600,  79,  8,      "1"),
		
			//      id, order, title,         description, lv, time req, time left, expire, gem, reward, visible
			new Job(9,   1,    "Model Job 1", "do it!",    5,  10800,       -1,       10800+5400,  119,   9,      "2"),
			new Job(10,  2,    "Model Job 2", "do it!",    6,  21600,       -1,       21600+10800,  239,   10,     "2"),
			new Job(11,  3,    "Model Job 3", "do it!",    7,  36000,       -1,       36000+18000,  339,   11,     "2"),
			new Job(12,  4,    "Model Job 4", "do it!",    8,  57600,       -1,       57600+28800,  639,   12,     "2"),
		
			//      id, order, title,         description, lv, time req, time left, expire, gem, reward, visible
			new Job(13,   1,   "Actress Job 1", "do it!",    6,  14400,       -1,       14400+7200,  159,   13,     "3"),
			new Job(14,   2,   "Actress Job 2", "do it!",    7,  28800,       -1,       28800+14400,  319,   14,     "3"),
			new Job(15,   3,   "Actress Job 3", "do it!",    8,  43200,       -1,       43200+21600,  479,   15,     "3"),
			new Job(16,   4,   "Actress Job 4", "do it!",    9,  72000,       -1,       72000+36000,  799,   16,     "3"),
		
			//      id, order, title,         description, lv, time req, time left, expire, gem, reward, visible
			new Job(17,   1,   "Singer Job 1", "do it!",    6,  14400,       -1,       14400+7200,  159,   17,     "4"),
			new Job(18,   2,   "Singer Job 2", "do it!",    7,  28800,       -1,       28800+14400,  319,   18,     "4"),
			new Job(19,   3,   "Singer Job 3", "do it!",    8,  43200,       -1,       43200+21600,  479,   19,     "4"),
			new Job(20,   4,   "Singer Job 4", "do it!",    9,  86400,       -1,       86400+43200,  959,   20,     "4"),*/
		};
	
	
	public static void Save()
	{
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		//DatabaseManager.db.Open(Globals.dbSavePath);
		foreach (Job a in jobsList)
		{
			
			
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM Jobs WHERE jobId = '" + a.activityId + "'",
				
				//update
				new List<string>(){
				"UPDATE Jobs SET "+
				"  sortOrder = " + a.sortOrder +
				" ,levelRequired = " + a.levelRequired + 
				" ,expireTime = " + a.expireTime +
				" ,visibleForSkill = '" + a.visibleForSkill + "'" +
				" WHERE jobId = " + a.activityId
				,
				
				"UPDATE Activities SET "+
				"  title = '" + a.title + "'"+
				" ,description = '" + a.description + "'"+
				" ,timeRequired = " + a.timeRequired +
				" ,timeLeft = " + a.timeLeft +
				" ,gemsToFinish = " + a.gemsToFinish +
				" ,rewardId = " + a.rewardId +
				" WHERE activityId = " + a.activityId
				},
			
				//insert
				new List<string>(){
					"INSERT INTO Jobs (jobId, sortOrder, activityId, levelRequired, expireTime, visibleForSkill) VALUES( "+
					                 + a.activityId + 
					"  , " + a.sortOrder +
					"  , " + a.activityId +
					" , " + a.levelRequired + 
					" , " + a.expireTime +
					" , '" + a.visibleForSkill + "'" +
				    ")"
					,
					"INSERT INTO Activities (activityId, title, description, timeRequired, timeLeft, gemsToFinish, rewardId) VALUES( "+
				                    a.activityId +
					", '" + a.title + "'"+
					" , '" + a.description + "'"+
					" , " + a.timeRequired +
					" , " + a.timeLeft +
					" , " + a.gemsToFinish +
					" , " + a.rewardId +
					" )"
				}
			);
			/*SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM Jobs WHERE jobId = '" + a.activityId + "'" ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			if (hasRecord)
			{
				// update
				DatabaseManager.SharedManager.Save("UPDATE Jobs SET "+
				"  sortOrder = " + a.sortOrder +
				" ,levelRequired = " + a.levelRequired + 
				" ,expireTime = " + a.expireTime +
				" ,visibleForSkill = '" + a.visibleForSkill + "'" +
				" WHERE jobId = " + a.activityId);
			
				DatabaseManager.SharedManager.Save("UPDATE Activities SET "+
				"  title = '" + a.title + "'"+
				" ,description = '" + a.description + "'"+
				" ,timeRequired = " + a.timeRequired +
				" ,timeLeft = " + a.timeLeft +
				" ,gemsToFinish = " + a.gemsToFinish +
				" ,rewardId = " + a.rewardId +
				" WHERE activityId = " + a.activityId);
				
			}
			else
			{
				// insert
				DatabaseManager.SharedManager.Save("INSERT INTO Jobs (jobId, sortOrder, activityId, levelRequired, expireTime, visibleForSkill) VALUES( "+
				                 + a.activityId + 
				"  , " + a.sortOrder +
				"  , " + a.activityId +
				" , " + a.levelRequired + 
				" , " + a.expireTime +
				" , '" + a.visibleForSkill + "'" +
			    ")");
			
				DatabaseManager.SharedManager.Save("INSERT INTO Activities (activityId, title, description, timeRequired, timeLeft, gemsToFinish, rewardId) VALUES( "+
				                    a.activityId +
					", '" + a.title + "'"+
				" , '" + a.description + "'"+
				" , " + a.timeRequired +
				" , " + a.timeLeft +
				" , " + a.gemsToFinish +
				" , " + a.rewardId +
				" )");
			}
			//Log.Debug("saving id = " + a.activityId +", timeleft = " + a.timeLeft);
			*/
			
		}
		//DatabaseManager.db.Close();
	}
	
	
	public static void Load()
	{
		jobsList.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM Jobs INNER JOIN Activities ON (Jobs.activityId = Activities.activityId) ORDER BY sortOrder ASC";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			jobsList.Add(new Job(
				qr.GetInteger("activityId"),
				qr.GetInteger("sortOrder"),
				qr.GetString("title"),
				qr.GetString("description"),
				qr.GetInteger("levelRequired"),
				qr.GetInteger("timeRequired"),
				qr.GetInteger("timeLeft"),
				qr.GetInteger("expireTime"),
				qr.GetInteger("gemsToFinish"),
				qr.GetInteger("rewardId"),
				qr.GetString("visibleForSkill")
				));
			
		}
			
		qr.Release();
	}
	
	public static List<Job> jobsOfSkill(int s)
	{
		List<Job> returnMe = new List<Job>();
		
		foreach(Job j in jobsList)
		{
			if (j.visibleForSkill.Contains(s.ToString()))
				returnMe.Add(j);
		}
		
		returnMe.Sort((x, y) => {return x.sortOrder.CompareTo(y.sortOrder);});
		
		return returnMe;
	}
	
	public static Job isDoingJobOfSkill(string skills)
	{
		string [] skill = skills.Split(',');
		foreach(Job j in jobsList)
		{
			foreach (string s in skill)
			{
				if (j.visibleForSkill.Contains(s) && isDoingJob(j))
					return j;
			}
		}
		
		return null;
	}
	
	public static int typesOfJobsDoing()
	{
		int count = 0;
		for (int i = 0 ; i < Skill.skillTypes; i++)
		{
			foreach(Job j in jobsList)
			{
				if (j.visibleForSkill.Contains(i+"") && isDoingJob(j))
				{
					count++;
					break;
				}
			}
		}
		return count;
	}
	
	public static Job isOnRewardOfSkill(string skills)
	{
		string [] skill = skills.Split(',');
		foreach(Job j in jobsList)
		{
			foreach (string s in skill)
			{
				if (j.visibleForSkill.Contains(s) && isOnReward(j))
					return j;
			}
		}
		
		return null;
	}
	
	public static float progressOfSkill(string skills)
	{
		Job j = isDoingJobOfSkill(skills);
		Job k = isOnRewardOfSkill(skills);
		if (j != null)
		{
			return Mathf.Min(1.0f, (j.expireTime - j.timeLeft)* 1.0f / j.timeRequired);
		}
		if (k != null)
			return 1.0f;
		
		return 0f;
	}
	
	public static bool isDoingJob(Job job)
	{
		return (((job.expireTime-job.timeRequired) <= job.timeLeft) && (job.timeLeft <= job.expireTime));
	}
	
	public static bool isOnReward(Job job)
	{
		return (job.timeLeft > 0 && (job.expireTime - job.timeLeft) > job.timeRequired);
	}

	public static bool isExpired(Job job)
	{
		return job.timeLeft == 0;
	}
	
	public static Job getJob(int id)
	{
		foreach(Job j in jobsList)
		{
			if (j.activityId == id)
				return j;
		}
		
		return null;
	}
	
	public static Job getRandomJob()
	{
		List<Job> pool = new List<Job>();
		
		foreach(Job j in jobsList)
		{
			if (j.levelRequired <= Globals.mainPlayer.Level &&
				!Jobs.isDoingJob(j) &&
				!Jobs.isOnReward(j))
			{
				pool.Add(j);
			}
		}
		
		if (pool.Count > 0)
		{
		
			return pool[UnityEngine.Random.Range(0, pool.Count)];
		}
		
		// no match
		return null;
	}
	
}


public class Job : Activity, IComparable<Job>
{
	public int levelRequired;
	public int expireTime;
	public string visibleForSkill;
	
	public Job(int pJobId, int pSortOrder, string pTitle,
		string pDescription, int pLevelRequired, int pTimeRequired, int pTimeLeft, int pExpireTime,
		int pGemsToFinish, int pRewardId, string pVisibleForSkill)
	{
		this.activityId = pJobId;
		this.sortOrder = pSortOrder;
		this.title = pTitle;
		this.description = pDescription;
		this.levelRequired = pLevelRequired;
		this.timeRequired = pTimeRequired;
		this.timeLeft = pTimeLeft;
		this.expireTime = pExpireTime;
		this.gemsToFinish = pGemsToFinish;
		this.rewardId = pRewardId;
		this.visibleForSkill = pVisibleForSkill;
	}
	
	public Job(int pJobId)
	{
		this.activityId = pJobId;
	}
	
	public void startJob()
	{
		this.timeLeft = this.expireTime;
	}
		
	public int CompareTo(Job that)
    {
        return this.timeRequired.CompareTo(that.timeRequired);
    }
			
}
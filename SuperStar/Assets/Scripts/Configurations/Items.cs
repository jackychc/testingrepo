using System;
using System.Collections.Generic;
using UnityEngine;


public class Items
{


	public static List<Item> allItems = new List<Item>();
	public static List<Item> initialItemsList = InitialItemsList.initialItemsList;
	
	
	// temp method
	public static void AllQuery()
	{
		string init = "INSERT INTO Items(itemId, itemName, visibility, subcategoryId, brandId, price, gems, requiredLevel, charisma, dlcId, meta, texMeta, colorMeta) VALUES";
		string q = init;
		
		foreach (Item a in initialItemsList)
		{
			if (a.itemId >= 484)
			{
				if (!q.Equals(init)) q += ",";
				
				Log.Debug("item id?"+a.itemId);
				
				q += "("
						+ a.itemId
						+ "," + "'" + a.itemName + "'"
						+ "," + "'" + a.visibility + "'"
						+ "," + a.subCategoryId
						+ "," + a.brandId
						+ "," + a.price
						+ "," + a.gems
						+ "," + a.requiredLevel
						+ "," + "'" + + a.sexy + "," + a.lively + "'"
						+ "," + a.dlcId
						+ "," + "'" + a.meta + "'"
						+ "," + "'" + a.texMeta + "'"
						+ "," + "'" + a.colorMeta + "'"

						 +")";


			}
		}
		
		Log.Warning("the Query:"+q);
	}

	public static void ConflictQuery()
	{
		string init = "INSERT INTO ItemConflicts(conflictId1, conflictId2, conflictType, conflictAction, dlcId) VALUES";
		string q = init;

		foreach (ItemConflictDB a in ItemConflicts.allConflictDBs)
		{

			{
				if (!q.Equals(init)) q += ",";


				q += "("
					+ a.attacker
						+ "," + a.defenser
						+ "," + "'" + a.conflictType.ToString() + "'"
						+ "," + "'" + a.action.ToString() + "'"
						+ "," + 7

						+")";


			}
		}

		Log.Warning("the Query:"+q);
	}
	
	public static void DebQuery()
	{
		string q = "";
		
		foreach (Item a in allItems)
		{
			
			
			if (a.requiredLevel >= 4)
			{
			
				q+= a.itemId+" | "+a.meta + " | "+a.texMeta + "\n";
			}
		}
		
		Log.Warning("the Query:"+q);
	}
	
	
	public static void Save()
	{
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		//DatabaseManager.db.Open(Globals.dbSavePath);
		foreach (Item a in allItems)
		{
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM Items WHERE itemId = '" + a.itemId + "'",
				
				//update
				new List<string>(){
				"UPDATE Items SET "+
				" itemName = '" + a.itemName + "'" + 
				" ,visibility = '" + a.visibility.ToString() + "'"+
				" ,subcategoryId = " + a.subCategoryId +
				" ,brandId = " + a.brandId +
				" ,price = " + a.price +
				" ,gems = " + a.gems +
				" ,requiredLevel = " + a.requiredLevel +
				" ,featureId = " + a.featureId +
				" ,charisma = '" + a.sexy + "," + a.lively + "'" +
				" ,rewardId = " + a.rewardId +
				" ,dlcId = " + a.dlcId +
				" ,meta = '" + a.meta + "'" +
				" ,texMeta = '" + a.texMeta + "'" +
				" ,colorMeta = '" + a.colorMeta + "'" +
				" WHERE itemId = " + a.itemId
				},
			
				//insert
				new List<string>(){
					"INSERT INTO Items  (itemId, itemName, visibility, subcategoryId, brandId, price, gems, requiredLevel, featureId, charisma, rewardId, dlcId, meta, texMeta, colorMeta) VALUES("+
				        a.itemId +
				" ,'" + a.itemName + "'" + 
				" , '" + a.visibility.ToString() + "'"+
				" , " + a.subCategoryId +
				" , " + a.brandId +
				" , " + a.price +
				" , " + a.gems +
				" , " + a.requiredLevel +
				" ," + a.featureId +
				" , '" + a.sexy + "," + a.lively + "'" +
				" , " + a.rewardId +
				" , " + a.dlcId +
				" , '" + a.meta + "'" +
				" , '" + a.texMeta + "'" +
				" , '" + a.colorMeta + "'" +
					")"
				}
			);
			
			/*SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM Items WHERE itemId = '" + a.itemId + "'" ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			if (hasRecord)
			{
				// update
				DatabaseManager.SharedManager.Save("UPDATE Items SET "+
				" itemName = '" + a.itemName + "'" + 
				" ,visibility = '" + a.visibility.ToString() + "'"+
				" ,subcategoryId = " + a.subCategoryId +
				" ,brandId = " + a.brandId +
				" ,price = " + a.price +
				" ,gems = " + a.gems +
				" ,requiredLevel = " + a.requiredLevel +
				" ,featureId = " + a.featureId +
				" ,charisma = '" + a.sexy + "," + a.lively + "'" +
				" ,rewardId = " + a.rewardId +
				" ,dlcId = " + a.dlcId +
				" ,meta = '" + a.meta + "'" +
				" ,texMeta = '" + a.texMeta + "'" +
				" ,colorMeta = '" + a.colorMeta + "'" +
				" WHERE itemId = " + a.itemId);
				
			}
			else
			{
				// insert
				DatabaseManager.SharedManager.Save("INSERT INTO Items  (itemId, itemName, visibility, subcategoryId, brandId, price, gems, requiredLevel, featureId, charisma, rewardId, dlcId, meta, texMeta, colorMeta) VALUES("+
				        a.itemId +
				" ,'" + a.itemName + "'" + 
				" , '" + a.visibility.ToString() + "'"+
				" , " + a.subCategoryId +
				" , " + a.brandId +
				" , " + a.price +
				" , " + a.gems +
				" , " + a.requiredLevel +
				" ," + a.featureId +
				" , '" + a.sexy + "," + a.lively + "'" +
				" , " + a.rewardId +
				" , " + a.dlcId +
				" , '" + a.meta + "'" +
				" , '" + a.texMeta + "'" +
				" , '" + a.colorMeta + "'" +
					")");
			}
			
			*/
			
		}
		//DatabaseManager.db.Close();
	}
	
	
	public static void Load()
	{
		allItems.Clear();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM Items";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			string[] indexParts = qr.GetString("charisma").Split(',');
			
			allItems.Add(new Item(
				qr.GetInteger("itemId"),
				qr.GetString("itemName"),
				Boolean.Parse(qr.GetString("visibility")),
				qr.GetInteger("subcategoryId"),
				qr.GetInteger("brandId"),
				qr.GetInteger("price"),
				qr.GetInteger("gems"),
				qr.GetInteger("requiredLevel"),
				qr.GetInteger("featureId"),
				int.Parse(indexParts[0]),
				int.Parse(indexParts[1]),
				qr.GetInteger("rewardId"),
				qr.GetInteger("dlcId"),
				qr.GetString("meta"),
				qr.GetString("texMeta"),
				qr.GetString("colorMeta")
				));
			
		}
			
		qr.Release();
	}
	
	public static Item getItem(int itemId)
	{
		foreach (Item i in allItems)
		{
			if (i.itemId == itemId)
				return i;
		}
		
		return null;
	}

	public static List<Item> getPurchasedItemsFromSubcategory(int subcatId, Player p)
	{
		if (p == null)
			return null;

		List<Item> itemList = new List<Item>();
		foreach (Item item in p.owningItems)
		{
			if (item.subCategoryId == subcatId)
				itemList.Add(item);
		}

		return itemList;
	}

	
	public static List<Item> getItemsFromSubcategory(int subcatId)
	{
		List<Item> itemList = new List<Item>();
		foreach (Item item in allItems)
		{
			if (item.subCategoryId == subcatId)
				itemList.Add(item);			
		}
		
		return itemList;
	}
	
	public static Item getRandomItemOfSubcategory(int subcatId)
	{
		List<Item> pool = new List<Item>();
		foreach (Item i in allItems)
		{
			if (i.subCategoryId == subcatId && !Items.isNoneItem(i))
				pool.Add(i);
		}
		
		if (pool.Count > 0)
			return pool[UnityEngine.Random.Range(0,pool.Count)];
		else
			return null;
	}
	
	public static Item getNoneItemOfSubcategory(int subcatId)
	{
		
		foreach (Item i in allItems)
		{
			if (i.subCategoryId == subcatId && i.meta.Equals("Null"))
			{
				return i;
			}
		}
		
		return null;
	}


	
	public static bool requireDLC(Item i)
	{
		return requireDLC(new List<Item>(){i});
	}
	
	public static bool requireDLC(List<Item> list)
	{
		if (list != null)
		{
			foreach (Item i in list)
			{
				if (i != null)
				{
					if (i.dlcId > Globals.currentDLCVersion)
					{
						Log.Debug("Item_requireDLC_i.dlcId: " + i.dlcId);
						Log.Debug("Item_requireDLC_Globals.currentDLCVersion: " + Globals.currentDLCVersion);
						return true;
					}
				}
				else
				{
					return true;
				}
			}
		}
		else
		{
			return true;
		}		
		
		return false;
	}
	
	public static string itemListToString(List<Item> list)
	{

		string temp = "";
		foreach (Item i in list)
		{
			if(!temp.Equals("")) temp += ",";
			temp += "" + i.itemId;
		}
		
//		Log.Debug("saving item list:" +temp);
		return temp;
		//return string.Join(",", list.ToArray());
	}
		
	public static List<Item> stringToItemList(string st)
	{
		List<Item> returnMe = new List<Item>();
		
		if (st != null && !st.Equals(""))
		{
			string[] temp = st.Split(',');
				
			foreach (string s in temp)
			{
				/*
				if (Items.getItem(int.Parse(s)) == null)
				{
					Item tp = new Item(int.Parse(s));
					tp.dlcId = int.MaxValue;
					allItems.Add(tp);
				}
				*/
				//Log.Debug("[Item][stringToItemList] s: " + s);
				Item item = Items.getItem(int.Parse(s));

				if (item == null)
					continue;
				
				try
				{
					/*//Log.Debug("Adding item:" + s);
					Item test = Items.getItem(Int32.Parse(s));
					
					if (test == null)
						Log.Error("null item");
					else
						Log.Error("item name: " + test.itemName);*/
					
					returnMe.Add(item);
				}
				catch (Exception e)
				{
					Log.Exception(e);
				}			
			}
		}
		
		return returnMe;
	}
	
	public static Vector2 sumCharismaForList(List<Item> list)
	{
		Vector2 returnMe = new Vector2(0f,0f);
		
		foreach (Item i in list)
		{
			returnMe.x += i.sexy;
			returnMe.y += i.lively;
		}
		
		return returnMe;
	}
	
	public static Texture2D GetIconOfItem(Item i)
	{
		Texture2D returnMe = null;
		returnMe =  (Texture2D) Resources.Load("CharacterAssets/Parts/"+ItemSubcategories.getSubcategory(i.subCategoryId).subcategoryName+"/itemIcon/"+i.itemId+"_icon");
	
		return returnMe;
	}
	
	public static bool isNoneItem(Item i)
	{
		return i.meta.Equals("Null");
	}
	
	
}


public class Item
{
	public int itemId;
	public string itemName;
	public bool visibility;
	public int subCategoryId;
	public int brandId;
	public int price;
	public int gems;
	public int requiredLevel;
	public int featureId;
	public int sexy;
	public int lively;
	public int rewardId;
	public int dlcId;
	public string meta;
	public string texMeta;
	public string colorMeta;
	
	// for dlc use only
	public List<string> md5Hashs = new List<string>();
	public List<string> bundleUrls = new List<string>();
	public string iconUrl = "";
	public string textureUrl = "";
	
	
	// i don't think this is possible
	//List<CharacterType> availableTypes = new List<CharacterType>();
	
	// get materials through mesh, not from here
	
	public Item (int pItemId, string pItemName, bool pVisibility, int pSubCategoryId, int pBrandId,
		int pPrice, int pGems, int pRequiredLevel, int pFeatureId, int pSexy, int pLively, int pRewardId, int pDlcId, string pMeta)
	:
		this(pItemId,  pItemName, pVisibility, pSubCategoryId, pBrandId,
		pPrice, pGems, pRequiredLevel, pFeatureId, pSexy, pLively, pRewardId, pDlcId, pMeta, "", "bb"){}
	
	
	public Item (int pItemId, string pItemName, bool pVisibility, int pSubCategoryId, int pBrandId,
		int pPrice, int pGems, int pRequiredLevel, int pFeatureId, int pSexy, int pLively, int pRewardId, int pDlcId, string pMeta, string pTexMeta, string pColorMeta)
	{
		this.itemId = pItemId;
		this.itemName = pItemName;
		this.visibility = pVisibility;
		this.subCategoryId = pSubCategoryId;
		this.brandId = pBrandId;
		this.price = pPrice;
		this.gems = pGems;
		this.requiredLevel = pRequiredLevel;
		this.featureId = pFeatureId;
		this.sexy= pSexy;
		this.lively = pLively;
		this.rewardId = pRewardId;
		this.dlcId = pDlcId;
		this.meta = pMeta;
		this.texMeta = pTexMeta;
		this.colorMeta = pColorMeta;
	}
	
	public Item (int pItemId)
	{
		this.itemId = pItemId;
	}
	
	
}
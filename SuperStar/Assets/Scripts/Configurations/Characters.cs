using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum CharacterType : int
{
	FEMALE = 0,
	MALE = 1
}

public class Characters
{	
	public static Character FEMALE = new Character(CharacterType.FEMALE,
		new List<ItemSubcategory>(){
		
		ItemSubcategories.getSubcategory(1),
		ItemSubcategories.getSubcategory(2),
		ItemSubcategories.getSubcategory(15),
		ItemSubcategories.getSubcategory(22),
		ItemSubcategories.getSubcategory(3),
		ItemSubcategories.getSubcategory(4),
		ItemSubcategories.getSubcategory(5),
		ItemSubcategories.getSubcategory(6),
		ItemSubcategories.getSubcategory(7),
		ItemSubcategories.getSubcategory(8),
		ItemSubcategories.getSubcategory(9),
		ItemSubcategories.getSubcategory(10),
	//	ItemSubcategories.getSubcategory(11),
		ItemSubcategories.getSubcategory(13),
		ItemSubcategories.getSubcategory(14),
		ItemSubcategories.getSubcategory(16),
		ItemSubcategories.getSubcategory(17),
		ItemSubcategories.getSubcategory(23),
		ItemSubcategories.getSubcategory(24),
		ItemSubcategories.getSubcategory(25),
		ItemSubcategories.getSubcategory(27)
		},
		"pose01");
	

}



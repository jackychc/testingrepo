using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScenesMap{
	
	private static ScenesNode startNode = null;
	
	public static bool isBuilt()
	{
		return (startNode != null);	
	}
	
	public static void Build()
	{
		if (startNode != null)
			return;
		
		//Construct the Title and InitDB Structure
		ScenesNode initDBNode 	= new ScenesNode(Scenes.INITDB);
		ScenesNode titleNode 	= new ScenesNode(Scenes.TITLE);
		
		
		//Construct the Home Structure
		ScenesNode photoShotNode 	= new ScenesNode(Scenes.PHOTOSHOT);
		ScenesNode closetNode 		= new ScenesNode(Scenes.CLOSET);
		ScenesNode homeNode 		= new ScenesNode(Scenes.HOME);
		ScenesNode friendsHomeNode 	= new ScenesNode(Scenes.FRIENDSHOME);
		homeNode.AddChild(friendsHomeNode);
		homeNode.AddChild(closetNode);
		homeNode.AddChild(photoShotNode);
		
		// sub-minigame scenes
		ScenesNode minigameNode     = new ScenesNode(Scenes.MINIGAME);
		ScenesNode miniClothNode	= new ScenesNode(Scenes.MINIGAMECLOTHMATCH);
		ScenesNode miniCoinNode		= new ScenesNode(Scenes.MINIGAMECOINDROP);
		minigameNode.AddChild(miniClothNode);
		minigameNode.AddChild(miniCoinNode);
		
		//Construct the Street Structure
		ScenesNode boyfriendNode 	= new ScenesNode(Scenes.BOYFRIEND);
		ScenesNode boyfriendMapNode = new ScenesNode(Scenes.BOYFRIENDMAP);		
		boyfriendNode.AddChild(boyfriendMapNode);
		
		//ScenesNode votingNode 		= new ScenesNode(Scenes.VOTING);
		ScenesNode jobNode 			= new ScenesNode(Scenes.JOB);
		ScenesNode shopNode			= new ScenesNode(Scenes.SHOP);
		ScenesNode streetNode 		= new ScenesNode(Scenes.STREET);
		streetNode.AddChild(homeNode);
		streetNode.AddChild(boyfriendNode);
		//streetNode.AddChild(votingNode);
		streetNode.AddChild(jobNode);
		streetNode.AddChild(minigameNode);
		streetNode.AddChild(shopNode);
		
		titleNode.AddChild(streetNode);
		initDBNode.AddChild(titleNode);	
		
		startNode = initDBNode;		
	}
	
	public static Scenes FindParentScenesWithScenes(Scenes currentScenes)
	{
		if (currentScenes.Equals(Scenes.NONE))
			return Scenes.NONE;
		
		List<ScenesNode> stack 	= new List<ScenesNode>();		
		stack.Add(startNode);		
		
		while(stack.Count > 0)
		{
			ScenesNode node = stack[0];
			if (node.values.Equals(currentScenes))
			{
				if (node.parent != null)
					return node.parent.values;
				else
					return Scenes.NONE;
			}
			else
			{
				if (node.hasChild())
				{
					//Stack it
					foreach(ScenesNode childNode in node.childNodes)
						stack.Add(childNode);
				}
			}
			stack.Remove(node);
		}
		
		return Scenes.NONE;
	}
}

using System;
using System.Collections.Generic;

/*
 *  none list:
 * 
 * 
rouge
top
dress
coat
skirt
trousers
socks
leggings
handbags
hat
glasses
necklace

 * 
 */

public class InitialItemsList
{
	public static List<Item> initialItemsList = new List<Item>(){
		// 		id, name,				visible,subcat,brand,price,gem,lv,feature,sexy,lively,reward, dlc, meta
		new Item(1, "Front Hair 1",   	true, 1, 0, 0, 0, 	1, 0, 0, 0, -1, 0, "fronthair_001"), 
		new Item(2, "Front Hair 2",   	true, 1, 0, 0, 0, 	1, 0, 0, 0, -1, 0, "fronthair_002"),
		new Item(3, "Front Hair 3",   	true, 1, 0, 0, 131, 1, 0, 0, 7, -1, 0, "fronthair_003"),
		new Item(6, "Front Hair 6",   	true, 1, 0, 0, 0, 	1, 0, 0, 0, -1, 0, "fronthair_006"),
		new Item(7, "Front Hair 7",   	true, 1, 0, 0, 0, 	1, 0, 0, 0, -1, 0, "fronthair_007"),
		new Item(8, "Front Hair 8",   	true, 1, 0, 0, 0, 	1, 0, 0, 0, -1, 0, "fronthair_008"),
		new Item(9, "Front Hair 9",   	true, 1, 0, 0, 0, 	1, 0, 0, 0, -1, 0, "fronthair_009"),
		new Item(10, "Front Hair 10", 	true, 1, 0, 0, 0, 	1, 0, 0, 0, -1, 0, "fronthair_010"),
		new Item(11, "Front Hair 11", 	true, 1, 0, 0, 0, 	1, 0, 0, 0, -1, 0, "fronthair_011"),
		new Item(12, "Front Hair 12", 	true, 1, 0, 0, 0, 	1, 0, 0, 0, -1, 0, "fronthair_012"),
		new Item(13, "Front Hair 13", 	true, 1, 0, 0, 0, 	1, 0, 0, 0, -1, 0, "fronthair_013"),	
		new Item(20, "Front Hair 20", 	true, 1, 0, 0, 198, 2, 0, 0, 11, -1, 0, "fronthair_020"),	
		
		// id,name,						visible,subcat,brand,price,gem,lv,feature,sexy, lively,reward,dlc,meta
		new Item(21, "Back Hair 1", 	true, 2, 0, 0, 0, 	1, 0, 0, 	0, -1, 0,  "backhair_001"),
		new Item(22, "Back Hair 2", 	true, 2, 0, 0, 131, 1, 0, 0, 	7, -1, 0,  "backhair_002"),
		new Item(23, "Back Hair 6", 	true, 2, 0, 0, 198, 2, 0, 0, 	11, -1, 0,  "backhair_006"),
		new Item(26, "Back Hair 7", 	true, 2, 0, 0, 0, 	1, 0, 0, 	0, -1, 0,  "backhair_007"),
		new Item(27, "Back Hair 8", 	true, 2, 0, 0, 0, 	1, 0, 0, 	0, -1, 0,  "backhair_008"),
		new Item(28, "Back Hair 9", 	true, 2, 0, 0, 0, 	1, 0, 0, 	0, -1, 0,  "backhair_009"),
		new Item(29, "Back Hair 10", 	true, 2, 0, 0, 0, 	1, 0, 0, 	0, -1, 0, "backhair_010"),
		new Item(55, "Back Hair 11", 	true, 2, 0, 0, 0, 	1, 0, 0, 	0, -1, 0, "backhair_011"),
		new Item(56, "Back Hair 12", 	true, 2, 0, 0, 0, 	1, 0, 0, 	0, -1, 0, "backhair_012"),
		new Item(57, "Back Hair 13", 	true, 2, 0, 0, 0, 	1, 0, 0, 	0, -1, 0, "backhair_013"),
		new Item(58, "Back Hair 14", 	true, 2, 0, 0, 0, 	1, 0, 0,	0, -1, 0, "backhair_014"),
		new Item(59, "Back Hair 15", 	true, 2, 0, 0, 0, 	1, 0, 0, 	0, -1, 0, "backhair_015"), 
		new Item(60, "Back Hair 16", 	true, 2, 0, 0, 0, 	1, 0, 0, 	0, -1, 0, "backhair_016"),
		
		
		//       id, name,             visible,subcat,brand,price,gem,lv,feature, sexy, lively,reward,dlc,meta
		new Item(32, "Pure-diamond", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"242_230_154_255"),
		new Item(181, "Hair Color 02", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"255_220_155_255"),
		new Item(33, "Sunflower Blonde", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"252_197_94_255"),
		new Item(34, "Caramel", true, 	18, 0, 0, 0, 1, 0, 0, 0, -1, 0,"178_133_99_255"),
		new Item(35, "Ruby Fusion", true, 	18, 0, 0, 0, 1, 0, 0, 0, -1, 0,"191_117_61_255"),
		new Item(36, "Auburn", true, 	18, 0, 0, 0, 1, 0, 0, 0, -1, 0,"177_54_63_255"),
		new Item(37, "Sparkling Amber", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"128_41_27_255"),
		new Item(38, "Chocolate Brown", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"83_25_16_255"),
		new Item(39, "Pink", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"243_164_145_255"),
		new Item(40, "Light Auburn", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"230_137_164_255"),
		new Item(41, "Hair Color 09", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"242_116_81_255"),
		new Item(42, "Hair Color 11", true, 	18, 0, 0, 0, 1, 0, 0, 0, -1, 0,"218_52_74_255"),
		new Item(43, "Deep Burgundy", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"96_27_110_255"),
		new Item(44, "Hair Color 13", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"151_111_170_255"),
		new Item(45, "Hair Color 16", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"142_185_185_255"),
		new Item(46, "Hair Color 14", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"68_102_179_255"),
		new Item(47, "Espresso", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"39_28_19_255"),
		new Item(48, "Grey", true, 	18, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"235_228_218_255"),
		//new Item(49, "62:35:18:255", true, 		18, 0, 0, 0, 1, 0, 5, 5,  -1, 0,"62_35_18_255"),
		//new Item(50, "27:18:12:255", true, 		18, 0, 0, 0, 1, 0, 5, 5,  -1, 0,"27_18_12_255"),
		
		
		new Item(71, "Black Eye", true, 	6, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"eyeball_001"),
		new Item(72, "Brown Eye", true, 	6, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"eyeball_002"),
		new Item(73, "Deep Blue Eye", true, 	6, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"eyeball_003"),
		new Item(74, "Gray Eye", true, 	6, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"eyeball_004"),
		new Item(75, "Grass Eye", true, 	6, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"eyeball_005"),
		new Item(76, "Pink Eye", true, 	6, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"eyeball_006"),
		new Item(77, "Purple Eye", true, 	6, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"eyeball_007"),
		new Item(78, "Red Eye", true, 	6, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"eyeball_008"),
		
		//       id, name,      					visible,subcat,brand,price,gem,lv,feature, sexy, lively,reward,dlc,meta
		new Item(180, "None",   						true, 	10, 0, 		0,    0, -1, 0,  0,  0,  -1, 0,"Null"),
		    new Item(81, "Pink Silk Mullet Dress", 			true, 	10, 0, 	    0,  392,  9, 0,  0, 21,  -1, 4, "dress_001", "dress_001_001", ""),       //dress_001
		    new Item(82, "Blue Denim Mullet Dress", 		true, 	10, 0, 		0,  368,  8, 0, 20,  0,  -1, 4, "dress_001", "dress_001_002", ""),
		    new Item(83, "Purple/White Shirt Dress", 		true, 	10, 0, 	    0,  214,  3, 0, 21,  0,  -1, 3, "dress_002", "dress_002_001", ""),
		    new Item(84, "Blue Floral A-Line Dress ", 		true, 	10, 0, 	    0,  343,  7, 0,  0, 19,  -1, 4, "dress_003", "dress_003_001", ""),
		    new Item(85, "Leopard A-Line Dress ", 			true, 	10, 0,      0,  368,  8, 0, 20,  0,  -1, 4, "dress_003", "dress_003_002", ""),
		    new Item(86, "Purple Floral A-Line Dress ", 	true, 	10, 0,  27000,    0,  8, 0,  0, 12,  -1, 4, "dress_003", "dress_003_003", ""),
		    new Item(87, "Striped Knitted Sweater Dress ", 	true, 	10, 0,  41000,    0,  9, 0,  0, 18,  -1, 4, "dress_004", "dress_004_001", ""),
		    new Item(88, "Pink Knitted Sweater Dress", 		true, 	10, 0,      0,  448, 12, 0,  0, 24,  -1, 5, "dress_004", "dress_004_002", ""),
		    new Item(89, "Pink/Mint Green High Waist Dress",true, 	10, 0,  33900,    0, 10, 0,  0, 15,  -1, 4, "dress_005", "dress_005_001", ""),
		    new Item(90, "Black/White Lace Shirt Dress",	true, 	10, 0,  	0,  448, 11, 0,  0, 24,  -1, 5, "dress_005", "dress_005_002", ""),
		
		    new Item(91, "Navy/White Lace Shirt Dress",		true, 	10, 0,  38600,    0, 12, 0,  0, 17,  -1, 5, "dress_005", "dress_005_003", ""),
		new Item(92, "Red Belted Minidre Ball Gown",		true, 	10, 0,  31600,    0,  3, 0, 14,  0,  -1, 0, "dress_006", "dress_006_001", ""),
		new Item(93, "White Mini Ball Gown",			true, 	10, 0,      0,    0,  1, 0,  0,  0,  -1, 0, "dress_006", "dress_006_002", ""),
		    new Item(94, "Gold Mini Ball Gown",				true, 	10, 0,      0,  468, 13, 0, 26,  0,  -1, 5, "dress_006", "dress_006_003", ""),
		    new Item(95, "Purple Mini Ball Gown",			true, 	10, 0,      0,  468, 13, 0, 26,  0,  -1, 5, "dress_006", "dress_006_004", ""),
		new Item(96, "Yellow Printed Shirt Dress",		true, 	10, 0,      0,  182,  2, 0,  0, 10,  -1, 0, "dress_007", "dress_007_003", ""),
		    new Item(97, "White Corset Long Skirt",			true, 	10, 0,  17800,    0,  4, 0,  8,  0,  -1, 3, "dress_011", "dress_011_001", ""),
		new Item(98, "Black Beaded Corset Long Skirt",	true, 	10, 0,  56000,    0,  3, 0, 25,  0,  -1, 0, "dress_011", "dress_011_002", ""),
		    new Item(99, "Gold Numbered Jersey Dress",		true, 	10, 0,      0,  448, 12, 0,  0, 24,  -1, 5, "dress_012", "dress_012_001", ""),
		    new Item(100, "Pink Jersey Dress",				true, 	10, 0,  29300,    0,  6, 0,  0, 13,  -1, 4, "dress_012", "dress_012_002", ""),
		
		new Item(101, "Blue Jersey Dress with Stripes",	true, 	10, 0,      0,    0,  1, 0,  0,  0,  -1, 0, "dress_012", "dress_012_003", ""),
		    new Item(102, "Bright Yellow Jersey Dress",		true, 	10, 0,  29300,    0,  6, 0,  0, 13,  -1, 4, "dress_012", "dress_012_004", ""),
		    new Item(103, "Navy Blue Long-Sleeved Dress",	true, 	10, 0,      0,  300,  5, 0,  0, 16,  -1, 3, "dress_014", "dress_014_002", ""),
		new Item(104, "White/GreyTennis Dress",			true, 	10, 0,      0,    0,  1, 0,  0,  0,  -1, 0, "dress_016", "dress_016_001", ""),
		new Item(105, "White/Pink Tennis Dress",		true, 	10, 0,      0,    0,  1, 0,  0,  0,  -1, 0, "dress_016", "dress_016_002", ""),
		    new Item(106, "Pink Tennis Dress",				true, 	10, 0,  17800,    0,  3, 0,  0,   8,  -1, 3, "dress_016", "dress_016_003", ""),
		
		    new Item(226, "Blue Striped Tennis Dress",		true, 	10, 0,  22400,    0,  6, 0,  0, 10,  -1, 4, "dress_016", "dress_016_004", ""),
		    new Item(227, "Union Jack T-Shirt Dress",		true, 	10, 0,      0,  448, 12, 0,  0, 24,  -1, 5, "dress_017", "dress_017_002", ""),
		new Item(228, "Zebra Striped Criss Cross Gown",	true, 	10, 0,   2500,    0,  1, 0,  0,  0,  -1, 0, "dress_019", "dress_019_001", ""),
		new Item(229, "Black Sweetheart Cocktail Dress",true, 	10, 0,      0,    0,  1, 0,  0,  0,  -1, 0, "dress_020", "dress_020_001", ""),
		new Item(230, "Blue and Black Spandex Mini Dress ",true,10, 0,      0,    0,  1, 0,  0,  0,  -1, 0, "dress_021", "dress_021_001", ""),
		
		new Item(231, "Black Speckled Mini Dress",		true,	10, 0,      0,    0,  1, 0,  0,  0,  -1, 0, "dress_021", "dress_021_002", ""),
		new Item(232, "White Belted Mini Dress",		true,	10, 0,      0,  149,  1, 0,  0,  8,  -1, 0, "dress_021", "dress_021_003", ""),
		new Item(233, "Black/White Bodycon Dress",		true,	10, 0,      0,    0,  1, 0,  0,  0,  -1, 0, "dress_022", "dress_022_001", ""),
		new Item(471, "Baby Pink Bear Costume", 		true, 	10, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "dress_022", "dress_022_002", ""),
		    new Item(234, "Sky Blue/White Tennis Dress",	true,	10, 0,  31600,    0,  6, 0,  0, 14,  -1, 4, "dress_023", "dress_023_001", ""),
		    new Item(235, "Pink Silk Gown",					true,	10, 0,      0,  742, 16, 0, 41,  0,  -1, 5, "dress_024", "dress_024_001", ""),
		//new Item(236, "Fluffy Pink Ballerina Dress",	true,	10, 0,  55300,    0, 18, 0,  0, 24,  -1, 0, "dress_025", "dress_025_001", ""),
		    new Item(237, "Baby Pink Strapless Slip Dress",	true,	10, 0,      0,  772, 17, 0, 42,  0,  -1, 5, "dress_027", "dress_027_001", ""),
		    new Item(238, "Fuschia Strapless Slip Dress",	true,	10, 0, 110700,    0, 17, 0, 49,  0,  -1, 5, "dress_027", "dress_027_002", ""),
		    new Item(239, "Off-White Strapless Ball Gown",	true,	10, 0,      0,  871, 20, 0, 48,  0,  -1, 5, "dress_028", "dress_028_001", ""),
		    new Item(240, "Sparkling Black and Blue Long Gown",	true,	10, 0,      0,  700, 15, 0, 38,  0,  -1, 5, "dress_029", "dress_029_001", ""),
		
		    new Item(241, "White Silk Princess Gown",		true,	10, 0,      0,  742, 16, 0, 41,  0,  -1, 5, "dress_030", "dress_030_001", ""),
		    new Item(242, "A-Line Gown",					true,	10, 0, 105500,    0, 16, 0, 47,  0,  -1, 5, "dress_031", "dress_031_001", ""),
		    new Item(243, "Orange Silk Wraparound Gown",	true,	10, 0,      0,  488, 14, 0, 27,  0,  -1, 5, "dress_032", "dress_032_001", ""),
		//new Item(244, "Green/White Ballerina Dress",	true,	10, 0,      0,  500, 17, 0,  0, 42,  -1, 0, "dress_034", "dress_034_001", ""),
		    new Item(245, "Black Quilted Leather Gown ",	true,	10, 0,      0,  700, 15, 0, 38,  0,  -1, 5, "dress_035", "dress_035_001", ""),
		    new Item(246, "White Strapless Gown",			true,	10, 0,      0,  841, 19, 0, 46,  0,  -1, 5, "dress_036", "dress_036_001", ""),
		    new Item(247, "Red Halter Gown",				true,	10, 0,      0,  841, 19, 0, 46,  0,  -1, 5, "dress_037", "dress_037_001", ""),
		new Item(482, "Goddess Gown", 					true, 	10, 0,      0,    0,  1, 0,  0,   0,  -1, 0, "dress_039", "dress_039_001", ""),
		    new Item(248, "Blue Silk Strapless Gown",		true,	10, 0,      0,  700, 15, 0, 38,  0,  -1, 5, "dress_040", "dress_040_001", ""),



		
		// rouge
		//       id, name,             visible,subcat,brand,price,gem,lv,feature, sexy, lively,reward,dlc,meta
		new Item(179, "None",          true, 	7, 0, 0, 0, -1, 0, 0, 0,  -1, 0,"0_0_0_0"),
		new Item(107, "Rouge Color 1", true, 	7, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"229_53_177_255"),
		new Item(108, "Rouge Color 2", true, 	7, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"229_53_95_255"),
		new Item(109, "Rouge Color 3", true, 	7, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"223_65_23_255"),
		new Item(110, "Rouge Color 4", true, 	7, 0, 0, 0, 1, 0, 0, 0,  -1, 0,"208_0_25_255"),
		//new Item(111, "Rouge Color 5", true, 	7, 0, 0, 0, 1, 0, 5, 5,  -1, 0,"231_99_69_255"),
		//new Item(112, "Rouge Color 6", true, 	7, 0, 0, 0, 1, 0, 5, 5,  -1, 0,"204_22_44_255"),
		
		// skin
		//       id, name,             visible,subcat,brand,price,gem,lv,feature, sexy, lively,reward,dlc,meta
		new Item(114, "Skin Color 1", true, 	3, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "224_192_177_255", "skinColor_001_001", ""),
		new Item(178, "Skin Color 2", true, 	3, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "221_196_168_255", "skinColor_002_001", ""),
		new Item(115, "Skin Color 3", true, 	3, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "225_189_157_255", "skinColor_003_001", ""),
		new Item(116, "Skin Color 4", true, 	3, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "219_169_134_255", "skinColor_004_001", ""),
		new Item(118, "Skin Color 6", true, 	3, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "196_136_86_255", "skinColor_006_001", ""),
		new Item(171, "Skin Color 7", true, 	3, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "166_108_61_255", "skinColor_007_001", ""),
		new Item(172, "Skin Color 8", true, 	3, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "141_78_55_255", "skinColor_008_001", ""),
		
		// lips color
		//       id, name,             visible,subcat,brand,price,gem,lv,feature, sexy, lively,reward,dlc,meta
		new Item(119, "Lips Color 1", true, 	19, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "212_66_66_255"),
		new Item(120, "Lips Color 2", true, 	19, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "231_87_53_255"),
		new Item(121, "Lips Color 3", true, 	19, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "239_64_136_255"),
		new Item(122, "Lips Color 4", true, 	19, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "198_65_164_255"),
		new Item(123, "Lips Color 5", true, 	19, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "149_69_149_255"),
		new Item(124, "Lips Color 6", true, 	19, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "204_22_44_255"),
		new Item(125, "Lips Color 7", true, 	19, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "164_0_35_255"),
		new Item(126, "Lips Color 8", true, 	19, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "93_0_87_255"),
		new Item(170, "Lips Color 9", true, 	19, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "2_15_125_255"),
		
		// eyeshadow
		//       id, name,             visible,subcat,brand,price,gem,lv,feature, sexy, lively,reward,dlc,meta
		new Item(127, "Eyeshadow Color 1", true,   20, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "213_105_169_255"),
		new Item(128, "Eyeshadow Color 2", true,   20, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "146_84_187_255"),
		new Item(129, "Eyeshadow Color 3", true,   20, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "128_115_195_255"),
		//new Item(130, "Eyeshadow Color 4", true,   20, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "106_68_134_255"),
		new Item(131, "Eyeshadow Color 5", true,   20, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "66_66_114_255"),
		new Item(132, "Eyeshadow Color 6", true,   20, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "115_145_170_255"),
		new Item(133, "Eyeshadow Color 7", true,   20, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "60_118_100_255"),
		new Item(134, "Eyeshadow Color 8", true,   20, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "138_114_86_255"),
		new Item(135, "Eyeshadow Color 9", true,   20, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "116_69_60_255"),
		new Item(136, "Eyeshadow Color 10", true,  20, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "33_33_33_255"),
		
		// lips
		//       id, name,             visible,subcat,brand,price,gem,lv,feature, sexy, lively,reward,dlc,meta
		new Item(137, "Lips 0", 		true, 	8, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_0"),
		new Item(138, "Lips 1", 		true, 	8, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "1_0_0_0"),
		new Item(139, "Lips 2", 		true, 	8, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_1_0_0"),
		new Item(140, "Lips 3", 		true, 	8, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_1_0"),
		//new Item(141, "Lips 4", 		true, 	8, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_1"),
		
		//eyeshape
		//       id, name,             visible,subcat,brand,price,gem,lv,feature, sexy, lively,reward,dlc,meta
		new Item(142, "Eyeshape 0", 	true, 	5, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_0_0_0_0_0", "eyeshape_000_001", "eyelash_00"),
		new Item(143, "Eyeshape 1", 	true, 	5, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "1_0_0_0_0_0_0_0", "eyeshape_001_001", "eyelash_01"),
		new Item(144, "Eyeshape 2", 	true, 	5, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_1_0_0_0_0_0_0", "eyeshape_002_001", "eyelash_00"),
		new Item(145, "Eyeshape 3", 	true, 	5, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_1_0_0_0_0_0", "eyeshape_003_001", "eyelash_03"),
		new Item(146, "Eyeshape 5", 	true, 	5, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_1_0_0_0_0", "eyeshape_005_001", "eyelash_00"),
		new Item(147, "Eyeshape 6", 	true, 	5, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_0_1_0_0_0", "eyeshape_006_001", "eyelash_00"),
		new Item(148, "Eyeshape 7", 	true, 	5, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_0_0_1_0_0", "eyeshape_007_001", "eyelash_00"),
		//new Item(149, "Eyeshape 1_o", 	true, 	5, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_0_0_0_1_0", "eyeshape_001_001", "eyelash_00"),
		new Item(150, "Eyeshape 2_o", 	true, 	5, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_0_0_0_0_1", "eyeshape_002_001", "eyelash_00"),
		
		
		//eyebrows
		//       id, name,             visible,subcat,brand,price,gem,lv,feature, sexy, lively,reward,dlc,meta
		new Item(156, "Eyebrows 0", 	true, 	4, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_0_0_0_0"),
		new Item(158, "Eyebrows 2", 	true, 	4, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_1_0_0_0_0_0"),
		new Item(159, "Eyebrows 3", 	true, 	4, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_1_0_0_0_0"),
		new Item(160, "Eyebrows 4", 	true, 	4, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_1_0_0_0"),
		new Item(162, "Eyebrows 6", 	true, 	4, 0, 0, 0, 1, 0, 0, 0,  -1, 0, "0_0_0_0_0_1_0"),

		new Item(167, "Photoshot BG4",	true,	21, 0, 0, 0, 0, 0, 0, 0, -1, 0, "camera_icon_04","{\"fileName\":\"camera_icon_04\",\"numOfPerson\":\"2\",\"onePerson\":{\"BKCamera\":\"-25,165,400,7.2,183,0.28,1,1,1\",\"transform\":\"-48,-1.23,14.9,0,3.76,0,1,1,1\"},\"twoPerson\":{\"BKCamera\":\"40,160,600,7.26,183.3,0.28,1,1,1\",\"0\":{\"transform\":\"0.07,-11.6,11.84,359.6,7.15,0.02,1,1,1\"}, \"1\":{\"transform\":\"-47.5,-22.6,3.93,359.6,20.75,-1.33e-08,1,1,1\"}}}", ""),
		new Item(168, "Photoshot BG5",	true,	21, 0, 0, 0, 0, 0, 0, 0, -1, 0, "camera_icon_05","{\"fileName\":\"camera_icon_05\",\"numOfPerson\":\"2\",\"onePerson\":{\"BKCamera\":\"-25,165,400,7.2,183,0.28,1,1,1\",\"transform\":\"-45.29,-1.7,18.1,359.6,7.15,0.02,1,1,1\"},\"twoPerson\":{\"BKCamera\":\"-25,165,400,7.2,183,0.28,1,1,1\",\"0\":{\"transform\":\"-27.1,-5.78,25.73,359.6,0.08,0.064,1,1,1\"}, \"1\":{\"transform\":\"-75.1,-13.56,15.21,359.6,9.69,0.075,1,1,1\"}}}", ""),
		    new Item(164, "Photoshot BG1",	true,	21, 0, 0, 0, 0, 0, 0, 0, -1, 3, "camera_icon_01", "{\"fileName\":\"camera_icon_01\",\"numOfPerson\":\"2\",\"onePerson\":{\"BKCamera\":\"40,160,600,7.2,183.3,0.28,1,1,1\",\"transform\":\"0,0,0,0,0,0,1,1,1\"},\"twoPerson\":{\"BKCamera\":\"40,160,600,7.26,183.3,0.28,1,1,1\",\"0\":{\"transform\":\"26.5,-12.8,14.7,0,352.3,0,1,1,1\"}, \"1\":{\"transform\":\"-25,-19.4,1.85,0,16.24,0,1,1,1\"}}}" , ""),
		    new Item(165, "Photoshot BG2",	true,	21, 0, 0, 0, 0, 0, 0, 0, -1, 3, "camera_icon_02","{\"fileName\":\"camera_icon_02\",\"numOfPerson\":\"2\",\"onePerson\":{\"BKCamera\":\"40,160,600,7.2,183.3,0.28,1,1,1\",\"transform\":\"0,0,0,0,0,0,1,1,1\"},\"twoPerson\":{\"BKCamera\":\"40,160,600,7.26,183.3,0.28,1,1,1\",\"0\":{\"transform\":\"23.9,-13,0.37,0,358,0,1,1,1\"}, \"1\":{\"transform\":\"-27.1,-19.4,0,0,9.68,0,1,1,1\"}}}", ""),
		    new Item(166, "Photoshot BG3",	true,	21, 0, 0, 0, 0, 0, 0, 0, -1, 3, "camera_icon_03","{\"fileName\":\"camera_icon_03\",\"numOfPerson\":\"1\",\"onePerson\":{\"BKCamera\":\"-25,165,400,7.2,183,0.28,1,1,1\",\"transform\":\"-48,-1.23,14.9,0,3.76,0,1,1,1\"}}", ""),

		//bra
	    //       id, name,   visible,subcat,brand,price,  gem,lv,feature, sexy, lively,reward,dlc,meta
		    new Item(183, "Lavender Demi Top", true, 15,    0,     0,   260,  4,   0,     0,     14,  -1, 3, "bra_001", "bra_001_001", ""),
		    new Item(195, "Sexy Red Demi Top", true, 15,    0, 17800,     0,  4,   0,     8,      0,  -1, 3, "bra_001", "bra_001_002", ""),
		    new Item(196, "Summer Floral Demi Top", true, 15,    0, 17800,     0,  4,   0,     0,      8,  -1, 3, "bra_001", "bra_001_003", ""),
		new Item(197, "Neon Yellow Sport Top", true, 15,    0,     0,   149,  1,   0,     0,      8,  -1, 0, "bra_002", "bra_001_001", ""),
		new Item(198, "Neon Pink Sport Top", true, 15,    0,     0,   149,  1,   0,     0,      8,  -1, 0, "bra_002", "bra_001_003", ""),
		    new Item(199, "Lacy Rouge Silk Top", true, 15,    0,     0,   368,  8,   0,    20,      0,  -1, 4, "bra_004", "bra_004_001", ""),
		new Item(200, "Classic Yellow Strapless Top", true, 15,    0,  8900,     0,  1,   0,     0,      4,  -1, 0, "bra_005", "bra_005_001", ""),
		new Item(201, "Basic White Strapless Top", true, 15,    0,     0,     0,  0,   0,     0,      0,  -1, 0, "bra_005", "bra_005_002", ""),
		new Item(202, "Basic Black Strapless Top", true, 15,    0,  8900,     0,  1,   0,     4,      0,  -1, 0, "bra_005", "bra_005_003", ""),
		    new Item(203, "Vintage Blue Strapless Top", true, 15,    0,     0,   371,  6,   0,     0,     20,  -1, 4, "bra_006", "bra_006_001", ""),
		    new Item(204, "Pink Ruffle Top", true, 15,    0,     0,   260,  4,   0,    14,      0,  -1, 3, "bra_006", "bra_006_002", ""),
		    new Item(205, "Navy White Striped Top", true, 15,    0, 22400,     0,  6,   0,     0,     10,  -1, 4, "bra_006", "bra_006_003", ""),
		    new Item(206, "Queen Lace Corset", true, 15,    0, 22400,     0,  6,   0,    10,      0,  -1, 4, "bra_007", "bra_007_001", ""),
		    new Item(207, "Flower Lady Corset", true, 15,    0, 27000,     0,  8,   0,    12,      0,  -1, 4, "bra_007", "bra_007_002", ""),
		    new Item(208, "Candy Girl Corset", true, 15,    0, 27000,     0,  8,   0,     0,     12,  -1, 4, "bra_007", "bra_007_003", ""),
		//new Item(209, "Lacy Noir Midriff Brassiere", true, 15,    0,     0,   368,  8,   0,    20,      0,  -1, 0, "bra_008_001", "bra_008_001", ""),
		
		
		// coat
	    //       id, name,            visible,subcat,brand,price,gem,lv,feature, sexy, lively,reward,dlc,meta
		//new Item(184, "None",   true, 	11, 0, 0, 0, -1, 0, 0, 0,  -1, 0,"Null"),
		//new Item(185, "Coat 1", true, 	11, 0, 100, 0, 1, 0, 30, 30,  -1, 0, "coat_001", "coat_001_001", ""),
		
		
		// handbag
		//       id, name,      						visible,	subcat,	brand,	price,	gem,	lv,	feature, sexy, lively,reward,dlc,meta
		new Item(186, "None",       					true, 		17, 	0, 		0, 		  0, 	-1, 	0, 		0, 		0,  -1, 0,		  "Null"),
		//new Item(187, "Pink Leather Bag with Lambskin", true, 		17, 	0, 		0, 		380, 	12,		0, 		21, 	0,  -1, 0, "handbag_001", "handbag_001_001", ""),
		//new Item(351, "Brown Leather Shoulder Bag", 	true, 		17, 	0, 	 6600, 		  0, 	 1,		0, 		0, 		0,  -1, 0, "handbag_002", "handbag_002_001", ""),
		    new Item(352, "Pink Mini Satchel", 				true, 		17, 	0, 		0, 		448, 	16,		0, 		0, 	   24,  -1, 5, "handbag_003", "handbag_003_001", ""),
		//new Item(353, "Pink Checkered Shoulder Bag", 	true, 		17, 	0, 		0, 		214, 	 4,		0, 		0, 	   11,  -1, 0, "handbag_004", "handbag_004_001", ""),
		//new Item(354, "Brown Checkered Satchel", 		true, 		17, 	0, 	13300, 		  0, 	 4,		0, 		0, 	    5,  -1, 0, "handbag_005", "handbag_005_001", ""),
		//new Item(355, "Small Leather Shoulder Bag", 	true, 		17, 	0, 	17800, 		  0, 	 6,		0, 		0, 	    8,  -1, 0, "handbag_006", "handbag_006_001", ""),
		    new Item(356, "Pink Shoulder Bag", 				true, 		17, 	0, 	22400, 		  0, 	 8,		0, 		0, 	   10,  -1, 4, "handbag_007", "handbag_007_001", ""),
		    new Item(357, "Leopard Print Round Bag", 		true, 		17, 	0, 	45700, 		  0, 	18,		0, 	   20, 	    0,  -1, 5, "handbag_008", "handbag_008_001", ""),
		//new Item(358, "handbag_009_001", 				true, 		17, 	0, 		0, 		  0, 	 0,		0, 	    0, 	    0,  -1, 0, "handbag_009", "handbag_009_001", ""),
		    new Item(359, "Yellow Sling Bag", 				true, 		17, 	0, 		0, 		260, 	 6,		0, 	    0, 	   14,  -1, 4, "handbag_010", "handbag_010_001", ""),
		    new Item(360, "Shimmering Shoulder Box Clutch", true, 		17, 	0, 		0, 		478, 	20,		0, 	    26,	    0,  -1, 5, "handbag_011", "handbag_011_001", ""),
		    new Item(361, "Orange and White Sling Bag", 	true, 		17, 	0, 		0, 		303, 	 8,		0, 	    0,	   16,  -1, 4, "handbag_012", "handbag_012_001", ""),
		//new Item(362, "Shpping Bag in Grained Calfskin",true, 		17, 	0, 		0, 		343, 	10,		0, 	    0,	   19,  -1, 0, "handbag_013", "handbag_013_001", ""),
		//new Item(363, "Pink Wirebag with Sequins",		true, 		17, 	0, 		0, 		415, 	14,		0, 	   23,	    0,  -1, 0, "handbag_013", "handbag_013_002", ""),
		    new Item(364, "Red Shoulder Clutch with Rivets",true, 		17, 	0, 	41000, 		  0, 	16,		0, 	   18,	    0,  -1, 5, "handbag_014", "handbag_014_001", ""),
		//new Item(365, "Pastel Sling Bag",				true, 		17, 	0, 	31600, 		  0, 	12,		0, 	   0,	   14,  -1, 0, "handbag_015", "handbag_015_001", ""),
		//new Item(366, "Monogram Shopping Bag",			true, 		17, 	0, 	27000, 		  0, 	10,		0, 	   0,	   12,  -1, 0, "handbag_016", "handbag_016_001", ""),
		//new Item(367, "Pastel Sling Bag",				true, 		17, 	0, 		0, 		114, 	 1,		0, 	   0,	    0,  -1, 0, "handbag_017", "handbag_017_001", ""),
		//new Item(368, "Gold Sequined Hobo Bag",			true, 		17, 	0, 	 6600, 		  0, 	 1,		0, 	   0,	    0,  -1, 0, "handbag_018", "handbag_018_001", ""),
		    new Item(369, "Heart Shape Evening Bag",		true, 		17, 	0, 	48100, 	  	  0, 	 20,	0, 	   21,	    0,  -1, 5, "handbag_019", "handbag_019_001", ""),
		//new Item(370, "Gray Prism Platinum Bag",		true, 		17, 	0, 	36300, 	  	  0, 	 14,	0, 	   16,	    0,  -1, 0, "handbag_020", "handbag_020_001", ""),
		//new Item(371, "Black Leather Handbag",			true, 		17, 	0, 	 	0, 		114, 	 1,		0, 	   0,	    0,  -1, 0, "handbag_021", "handbag_021_001", ""),		
		
		// hat
	    //       id, name,            				visible,subcat,brand,price,  gem,  lv,feature, sexy, lively,reward,dlc,meta
		new Item(188, "None",   						true, 	23, 0, 		  0,    0, -1, 0, 	    0, 	    0,  -1, 0,"Null"),
		new Item(434, "Black Satin Ribbon Headband", 	true, 	23, 0, 	   4400,    0,  1, 0, 	    0, 	    0,  -1, 0, "hat_001", "hat_001_001", ""),
		new Item(435, "Bejeweled Feather Headband", 	true, 	23, 0, 	   4400,    0,  1, 0, 	    0, 	    0,  -1, 0, "hat_002", "hat_002_001", ""),
		    new Item(436, "Floral Satin Ribbon Headband", 	true, 	23, 0, 	  20100,    0, 13, 0, 	    9, 	    0,  -1, 5, "hat_004", "hat_004_001", ""),
		    new Item(437, "Diamond Studded Red Bow", 		true, 	23, 0, 	      0,  245, 12, 0, 	    0, 	   13,  -1, 5, "hat_005", "hat_005_001", ""),
		    new Item(438, "Floral Headband", 				true, 	23, 0, 	      0,  275, 13, 0, 	   15, 	    0,  -1, 5, "hat_006", "hat_006_001", ""),
		    new Item(439, "Purple Butterfly Hairclips", 	true, 	23, 0, 	  22400,    0, 14, 0, 	   10, 	    0,  -1, 5, "hat_007", "hat_007_001", ""),
		    new Item(440, "Silver Tiara", 					true, 	23, 0, 	  20100,    0, 12, 0, 	    9, 	    0,  -1, 5, "hat_008", "hat_008_001", ""),
		new Item(441, "Orange Silk Bow", 				true, 	23, 0, 	      0,    0,  1, 0, 	    0, 	    0,  -1, 0, "hat_009", "hat_009_001", ""),
		    new Item(442, "Yellow and Pink Silk Bow", 		true, 	23, 0, 	   8900,    0,  5, 0, 	    0, 	    4,  -1, 3, "hat_010", "hat_010_001", ""),
		    new Item(443, "Fiery Fuchsia Mini Beret", 		true, 	23, 0, 	  13300,    0,  8, 0, 	    0, 	    5,  -1, 4, "hat_011", "hat_011_001", ""),
		    new Item(444, "Blonde Hair Bow", 				true, 	23, 0, 	  	  0,  245, 12, 0, 	    0, 	   13,  -1, 5, "hat_013", "hat_013_001", ""),
		    new Item(445, "Medallion Hairclip", 			true, 	23, 0, 	  13300,    0,  7, 0, 	    0, 	    5,  -1, 4, "hat_014", "hat_014_001", ""),
		    new Item(446, "Crystal Forehead Jewelry", 		true, 	23, 0, 	      0,  260, 12, 0, 	   14, 	    0,  -1, 5, "hat_015", "hat_015_001", ""),
		    new Item(447, "Gold and Pearl Headband", 		true, 	23, 0, 	      0,  230, 10, 0, 	   12, 	    0,  -1, 4, "hat_016", "hat_016_001", ""),
		    new Item(448, "Pink and Purple Derby Hat", 		true, 	23, 0, 	      0,  131,  4, 0, 	    0, 	    0,  -1, 3, "hat_017", "hat_017_001", ""),
		    new Item(449, "Glittery Heart Headband", 		true, 	23, 0, 	      0,  166,  6, 0, 	    0, 	    9,  -1, 4, "hat_018", "hat_018_001", ""),
		    new Item(450, "Lattice Shallow-Crowned Hat", 	true, 	23, 0, 	      0,  245, 11, 0, 	   13, 	    0,  -1, 5, "hat_019", "hat_019_001", ""),
		    new Item(451, "Purple Silk Bow", 				true, 	23, 0, 	      0,  149,  5, 0, 	    0, 	    8,  -1, 3, "hat_020", "hat_020_001", ""),
		    new Item(452, "Black Mini Top Hat", 			true, 	23, 0, 	  15600,    0,  9, 0, 	    0, 	    7,  -1, 4, "hat_021", "hat_021_001", ""),
		    new Item(453, "Tilted Decagon Hat", 			true, 	23, 0, 	      0,  198,  8, 0, 	    0, 	   11,  -1, 4, "hat_023", "hat_023_001", ""),
		    new Item(454, "Blue Rose Hairclip", 			true, 	23, 0, 	  17800,    0, 11, 0, 	    8, 	    0,  -1, 5, "hat_024", "hat_024_001", ""),
			new Item(455, "Pink Feather Headband", 			true, 	23, 0, 	  22400,    0, 14, 0, 	   10, 	    0,  -1, 5, "hat_025", "hat_025_001", ""),
		//new Item(456, "White Silk Headband", 			true, 	23, 0, 	  15600,    0, 10, 0, 	    7, 	    0,  -1, 0, "hat_026", "hat_026_001", ""),
		    new Item(457, "Off-White Floral Headband", 		true, 	23, 0, 	  15600,    0, 10, 0, 	    0, 	    7,  -1, 4, "hat_027", "hat_027_001", ""),
		    new Item(458, "Black and Gold Headband", 		true, 	23, 0, 	      0,  114,  3, 0, 	    0, 	    0,  -1, 3, "hat_028", "hat_028_001", ""),
		    new Item(459, "Lace Kitty Ears Headband", 		true, 	23, 0, 	  11100,    0,  6, 0, 	    0, 	    4,  -1, 4, "hat_029", "hat_029_001", ""),
		    new Item(460, "Scarlet Mini Hat", 				true, 	23, 0, 	      0,  214,  9, 0, 	    0, 	   11,  -1, 4, "hat_030", "hat_030_001", ""),
		    new Item(461, "Pink Spiked Headband", 			true, 	23, 0, 	   6600,    0,  3, 0, 	    0, 	    0,  -1, 3, "hat_031", "hat_031_001", ""),
		    new Item(462, "Blue and White Striped Bow", 	true, 	23, 0, 	      0,  182,  7, 0, 	    0, 	   10,  -1, 4, "hat_032", "hat_032_001", ""),
		new Item(463, "Beige Wide Brimmed Hat", 		true, 	23, 0, 	      0,   77,  1, 0, 	    0, 	    0,  -1, 0, "hat_034", "hat_034_001", ""),
		new Item(464, "Red Felt Fedora", 				true, 	23, 0, 	      0,    0,  1, 0, 	    0, 	    0,  -1, 0, "hat_035", "hat_035_001", ""),
		    new Item(465, "White Felt Fedora", 				true, 	23, 0, 	   8900,    0,  4, 0, 	    0, 	    4,  -1, 3, "hat_035", "hat_035_002", ""),
		new Item(466, "Black Felt Fedora", 				true, 	23, 0, 	      0,   77,  1, 0, 	    0, 	    0,  -1, 0, "hat_035", "hat_035_003", ""),
		    new Item(467, "Pink Vintage Floral Bonnet", 	true, 	23, 0, 	      0,  289, 14, 0, 	   16, 	    0,  -1, 5, "hat_036", "hat_036_001", ""),
		    new Item(468, "Bejeweled Feather Clip", 		true, 	23, 0, 	      0,  289, 14, 0, 	   16, 	    0,  -1, 5, "hat_037", "hat_037_001", ""),
		    new Item(469, "Gray Tie Headband", 				true, 	23, 0, 	      0,  230, 10, 0, 	    0, 	   12,  -1, 4, "hat_038", "hat_038_001", ""),


		
		// skirt
	    //       id, name,           			   visible,subcat,brand,  price,   gem,lv,feature,sexy, lively,reward,dlc,meta
		new Item(190, "None",   						true, 	13, 0,       0,     0, -1, 0,         0,   0,  -1, 0,"Null"),
		    new Item(191, "Neon Green Peplum Skirt", 		true, 	13, 0,   24700,     0,  7, 0,         0,  11,  -1, 4, "skirt_001", "skirt_001_001", ""),
		    new Item(281, "Neon Pink Peplum Skirt", 		true, 	13, 0,       0,   343,  7, 0,        19,   0,  -1, 4, "skirt_001", "skirt_001_002", ""),
		    new Item(282, "Off-White Long Skirt", 			true, 	13, 0,   31600,     0, 10, 0,        14,   0,  -1, 4, "skirt_002", "skirt_002_001", ""),
		    new Item(283, "Pink Long Skirt", 				true, 	13, 0,   33900,     0, 11, 0,        15,   0,  -1, 5, "skirt_002", "skirt_002_002", ""),
		    new Item(284, "Lavender Long Skirt", 			true, 	13, 0,   33900,     0, 11, 0,        15,   0,  -1, 5, "skirt_002", "skirt_002_003", ""),
		new Item(285, "Mint Green Long Skirt", 			true, 	13, 0,       0,     0,  1, 0,         0,   0,  -1, 0, "skirt_002", "skirt_002_004", ""),
		new Item(286, "Purple Floral Pleated Skirt", 	true, 	13, 0,       0,     0,  1, 0,         0,   0,  -1, 0, "skirt_003", "skirt_003_005", ""),
		new Item(472, "Wizard Student Pleated Skirt", 	true, 	13, 0,      0,    	0, 	1, 0,     	  0,   0,  -1, 0, "skirt_003", "skirt_003_006", ""),
		new Item(287, "Striped Mini Skirt", 			true, 	13, 0,       0,     0,  1, 0,         0,   0,  -1, 0, "skirt_004", "skirt_004_001", ""),
		new Item(288, "Multicolor Mini Skirt", 			true, 	13, 0,       0,     0,  1, 0,         0,   0,  -1, 0, "skirt_004", "skirt_004_002", ""),
		new Item(473, "Flight Attendant Skirt", 		true, 	13, 0,      0,    	0, 	1, 0,     	  0,   0,  -1, 0, "skirt_004", "skirt_004_005", ""),
		    new Item(289, "Hello Mini Skirt", 				true, 	13, 0,       0,   289,  5, 0,         0,  16,  -1, 3, "skirt_004", "skirt_004_003", ""),
		    new Item(290, "Sequin Mini Skirt", 				true, 	13, 0,       0,   557, 18, 0,        30,   0,  -1, 5, "skirt_004", "skirt_004_004", ""),
		new Item(291, "Yellow A-Line Skirt", 			true, 	13, 0,       0,     0,  1, 0,         0,   0,  -1, 0, "skirt_005", "skirt_005_008", ""),
		    new Item(292, "Orange A-Line Skirt", 			true, 	13, 0,   31600,     0, 10, 0,         0,  14,  -1, 4, "skirt_005", "skirt_005_009", ""),
		    new Item(293, "Leopard A-Line Skirt", 			true, 	13, 0,   41000,     0, 14, 0,        18,   0,  -1, 5, "skirt_005", "skirt_005_011", ""),
		    new Item(294, "Red Pleated A-Line Skirt", 		true, 	13, 0,       0,   392, 10, 0,        21,   0,  -1, 4, "skirt_005", "skirt_005_012", ""),
		    new Item(295, "Floral A-Line Skirt", 			true, 	13, 0,       0,   392, 10, 0,        21,   0,  -1, 4, "skirt_005", "skirt_005_013", ""),
		    new Item(296, "Black Leather Skirt with Rivets",true, 	13, 0,   54620,     0, 18, 0,        24,   0,  -1, 5, "skirt_005", "skirt_005_014", ""),
		new Item(474, "Blue Nutcracker Skirt", 			true, 	13, 0,      0,    	0, 	1, 0,     	  0,   0,  -1, 0, "skirt_005", "skirt_005_015", ""),
		    new Item(297, "Neon Yellow Wrap Around Skirt", 	true, 	13, 0,       0,   437, 14, 0,         0,  24,  -1, 5, "skirt_006", "skirt_006_001", ""),
		//new Item(298, "Peach Mini Layered Skirt", 		true, 	13, 0,   50500,     0, 16, 0,        22,   0,  -1, 0, "skirt_007", "skirt_007_001", ""),
		new Item(299, "Black and Red Pencil Skirt", 	true, 	13, 0,       0,   149,  1, 0,         8,   0,  -1, 0, "skirt_008", "skirt_008_001", ""),
		new Item(300, "Orange Pencil Skirt", 			true, 	13, 0,       0,     0,  1, 0,         0,   0,  -1, 0, "skirt_008", "skirt_008_002", ""),
		new Item(301, "Mocha Laced Pencil Skirt", 		true, 	13, 0,       0,   149,  1, 0,         8,   0,  -1, 0, "skirt_008", "skirt_008_003", ""),
		    new Item(302, "Orange Printed Pencil Skirt", 	true, 	13, 0,   17800,     0,  3, 0,         0,   8,  -1, 3, "skirt_008", "skirt_008_004", ""),
			new Item(303, "Blue Printed Pencil Skirt", 		true, 	13, 0,   17800,     0,  3, 0,         0,   8,  -1, 3, "skirt_008", "skirt_008_005", ""),
		    new Item(304, "Mint Green Straight Skirt", 		true, 	13, 0,   24700,     0,  7, 0,        11,   0,  -1, 4, "skirt_009", "skirt_009_001", ""),
		    new Item(305, "Pink Striped Straight Skirt", 	true, 	13, 0,       0,   343,  7, 0,         0,  19,  -1, 4, "skirt_009", "skirt_009_002", ""),
		    new Item(306, "Blue Straight Skirt", 			true, 	13, 0,   20100,     0,  5, 0,         9,   0,  -1, 3, "skirt_009", "skirt_009_003", ""),
		    new Item(307, "Pink Maxi Skirt", 				true, 	13, 0,   48100,     0, 15, 0,        21,   0,  -1, 5, "skirt_010", "skirt_010_001", ""),
		    new Item(308, "Blue and Pink Sports Skirt", 	true, 	13, 0,       0,   214,  3, 0,         0,  11,  -1, 3, "skirt_011", "skirt_011_001", ""),
		    new Item(309, "Yellow Sports Skirt", 			true, 	13, 0,       0,   289,  7, 0,         0,  16,  -1, 4, "skirt_011", "skirt_011_002", ""),
		new Item(475, "Ruffle Chef Skirt", 				true, 	13, 0,      0,    	0, 	1, 0,     	  0,   0,  -1, 0, "skirt_011", "skirt_011_003", ""),
		    new Item(310, "Pink Mini Layered Skirt", 		true, 	13, 0,   36300,     0, 12, 0,        16,   0,  -1, 5, "skirt_012", "skirt_012_001", ""),
		    new Item(311, "Blue Mini Layered Skirt", 		true, 	13, 0,   36300,     0, 12, 0,         0,  16,  -1, 5, "skirt_012", "skirt_012_002", ""),
		    new Item(312, "White Lacy Mini Layered Skirt", 	true, 	13, 0,       0,   437, 15, 0,        28,   0,  -1, 5, "skirt_012", "skirt_012_003", ""),



		//trousers
		//       id, name,           			   visible,subcat,brand,  price,   gem,lv,feature,sexy, lively,reward,dlc,meta
		new Item(313, "Neon Pink Slim-fit Jeans", 		true, 	13, 0,       0,      0,  1, 0,       0,   0,  -1, 0, "trouser_001", "trouser_001_001", ""),
		new Item(314, "Neon Yellow Slim-fit Jeans", 	true, 	13, 0,       0,    149,  1, 0,       0,   8,  -1, 0, "trouser_001", "trouser_001_002", ""),
		    new Item(315, "Orange Printed Slim-fit Jeans", 	true, 	13, 0,   13300,      0,  3, 0,       0,   5,  -1, 3, "trouser_001", "trouser_001_003", ""),
		    new Item(316, "Denim Shorts with Belt", 		true, 	13, 0,   33900,    	 0, 11, 0,       0,  15,  -1, 5, "trouser_003", "trouser_003_001", ""),
			new Item(317, "Gradient Shorts", 					true, 	13, 0,       0,    392, 11, 0,       0,  25,  -1, 5, "trouser_003", "trouser_003_002", ""),
			new Item(318, "Floral Shorts", 				true, 	13, 0,       0,    468, 13, 0,       0,  26,  -1, 5, "trouser_003", "trouser_003_003", ""),
		new Item(319, "Baby Pink Shorts with Belt", 	true, 	13, 0,       0,      0,  1, 0,       0,   0,  -1, 0, "trouser_003", "trouser_003_004", ""),
		new Item(320, "White Bootcut Pants", 			true, 	13, 0,       0,      0,  1, 0,       0,   0,  -1, 0, "trouser_004", "trouser_004_001", ""),
		new Item(321, "Black Bootcut Pants with Lace", 	true, 	13, 0,       8900,      0,  1, 0,       0,   4,  -1, 0, "trouser_004", "trouser_004_002", ""),
		    new Item(322, "Purple Sports Pants with Gold Print",true, 	13, 0,   	0,      488, 15, 0,       0,  27,  -1, 5, "trouser_008", "trouser_008_002", ""),
		    new Item(323, "Regal Sports Pants with Gold Print", true, 	13, 0,   	0,      488, 15, 0,       0,  27,  -1, 5, "trouser_008", "trouser_008_003", ""),
		new Item(324, "Bootcut Pants with Rose Print", 	true, 	13, 0,    8900,      0,  1, 0,       0,   4,  -1, 0, "trouser_008", "trouser_008_001", ""),
		    new Item(325, "Purple/Black Sports Shorts", 	true, 	13, 0,   29300,      0,  7, 0,       0,  13,  -1, 4, "trouser_010", "trouser_010_001", ""),
		    new Item(326, "Pink Sports Shorts", 			true, 	13, 0,   31600,      0, 11, 0,       0,  14,  -1, 5, "trouser_010", "trouser_010_002", ""),
		new Item(481, "Storm Lady Pants", 				true, 	13, 0,      0,    	0, 	1, 0,     	  0,   0,  -1, 0, "trouser_006", "trouser_006_001", ""),
		
		
		
		
		


		
		// top
	    //       id, name,  							visible,subcat,brand,price,gem,lv,feature,sexy,lively,reward,dlc,meta
		new Item(192, "None",   							true, 	9, 0,      0,    0,-1, 0,     0,     0,  -1, 0, "Null"),
		new Item(193, "Gold Lace Up Bustier Top", 			true, 	9, 0,      0,  149, 1, 0,     8,     0,  -1, 0, "top_001", "top_001_001", ""),
		    new Item(194, "Animal Print Midriff Top", 			true, 	9, 0,      0,  260, 4, 0,    14,     0,  -1, 3, "top_001", "top_001_002", ""),
		    new Item(249, "Blue Racerback Tank with Stripes", 	true, 	9, 0,  11000,    0, 3, 0,     0,    13,  -1, 3, "top_001", "top_001_003", ""),
		    new Item(250, "Pink Striped Sports Tank", 			true, 	9, 0,  11000,    0, 3, 0,     0,    13,  -1, 3, "top_002", "top_002_001", ""),
		    new Item(251, "Blue Striped Sports Tank", 			true, 	9, 0,  11000,    0, 3, 0,     0,    4,  -1, 3, "top_002", "top_002_002", ""),
		new Item(252, "Purple Polka Dot Tank Top", 			true, 	9, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "top_002", "top_002_003", ""),
		    new Item(253, "Brown Lace Sleeveless Blouse", 		true, 	9, 0,  31600,    0, 9, 0,    14,     0,  -1, 4, "top_003", "top_003_001", ""),
		new Item(254, "Red Striped Sleeveless Blouse", 		true, 	9, 0,   8900,    0, 1, 0,     0,     4,  -1, 0, "top_003", "top_003_002", ""),
		new Item(255, "Blue Striped Sleeveless Blouse", 	true, 	9, 0,   8900,    0, 1, 0,     0,     4,  -1, 0, "top_003", "top_003_003", ""),
		    new Item(256, "Stone Washed Denim Blouse", 			true, 	9, 0,  24700,    0, 7, 0,     0,    22,  -1, 4, "top_006", "top_006_001", ""),
		    new Item(257, "Blue Blouse with Pink Detail", 		true, 	9, 0,  24700,    0, 7, 0,     0,    22,  -1, 4, "top_006", "top_006_002", ""),
		    new Item(258, "Stars and Stripes Midriff Top", 		true, 	9, 0,      0,  415,10, 0,    23,     0,  -1, 4, "top_007", "top_007_001", ""),
		    new Item(259, "Red Striped Corset Top ", 			true, 	9, 0,      0,  317, 6, 0,    17,     0,  -1, 4, "top_007", "top_007_002", ""),
		    new Item(260, "Powder Blue Floral Corset Top", 		true, 	9, 0,      0,  415,10, 0,    23,     0,  -1, 4, "top_007", "top_007_003", ""),
		new Item(476, "Ruffle Chef Shirt", 					true, 	9, 0,      0,    	0, 	1, 0,     	  0,   0,  -1, 0, "top_007", "top_007_004", ""),
		new Item(477, "Blue Nutcracker Top", 				true, 	9, 0,      0,    	0, 	1, 0,     	  0,   0,  -1, 0, "top_007", "top_007_005", ""),
		new Item(261, "Pink Sports Top", 					true, 	9, 0,      0,     149,  1, 0,     	  0,   8,  -1, 0, "top_008", "top_008_001", ""),

		    new Item(262, "Bear Tank Top", 						true, 	9, 0, 48800,    0,15, 0,    21,     0,  -1, 5, "top_008", "top_008_002", ""),

		    new Item(263, "Hazard Tape Bandage Tube Top", 		true, 	9, 0,      0,  466,12, 0,    25,     0,  -1, 5, "top_010", "top_010_001", ""),
		    new Item(470, "Super Woman Corset Top", 			true, 	9, 0,      0,  466,12, 0,     0,     25,  -1, 5, "top_010", "top_010_002", ""),
		
		    new Item(264, "Geometric Print Chiffon Blouse", 	true, 	9, 0,  24700,    0, 7, 0,    0,     22,  -1, 4, "top_011", "top_011_001", ""),
		new Item(478, "Wizard Student Shirt", 				true, 	9, 0,      0,    	0, 	1, 0,     	  0,   0,  -1, 0, "top_011", "top_011_002", ""),
		    new Item(265, "Pink Sheer Long Sleeved Blouse", 	true, 	9, 0,      0,  478,13, 0,     0,    26,  -1, 5, "top_013", "top_013_001", ""),
		    new Item(266, "Bejeweled Sheer Silk Blouse", 		true, 	9, 0,      0,  437,11, 0,    24,     0,  -1, 5, "top_013", "top_013_002", ""),
		new Item(267, "Purple Ladies Polo Shirt", 			true, 	9, 0,   8900,    0, 1, 0,     0,     4,  -1, 0, "top_014", "top_014_001", ""),
		new Item(268, "Black/Blue Printed Shirt", 			true, 	9, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "top_015", "top_015_001", ""),
		    new Item(269, "White Shirt with Corset Design", 	true, 	9, 0,  35000,    0,11, 0,    15,     0,  -1, 5, "top_015", "top_015_002", ""),
		    new Item(270, "Ivory Lace Dress Top", 				true, 	9, 0,  45700,    0,14, 0,    20,     0,  -1, 5, "top_018", "top_018_003", ""),
		    new Item(271, "Pink Strapless Dress Top", 			true, 	9, 0,      0,  485,14, 0,    26,     0,  -1, 5, "top_019", "top_019_001", ""),
		    new Item(272, "Mint Green Strapless Dress Top", 	true, 	9, 0,  38600,    0,12, 0,    17,     0,  -1, 5, "top_019", "top_019_002", ""),
		    new Item(273, "White/Gold Strapless Dress Top", 	true, 	9, 0,  41000,    0,13, 0,    18,     0,  -1, 5, "top_019", "top_019_003", ""),
		    new Item(274, "Blue Sleeveless Tie Blouse", 		true, 	9, 0,  35000,    0,11, 0,     0,    15,  -1, 5, "top_020", "top_020_001", ""),
		new Item(275, "Orange Fitted Blouse", 				true, 	9, 0,   8900,    0, 1, 0,     0,     4,  -1, 0, "top_021", "top_021_001", ""),
		    new Item(276, "Pink Pow Tee", 						true, 	9, 0,  29300,    0, 5, 0,     0,    13,  -1, 3, "top_024", "top_024_001", ""),
		new Item(277, "Old Rose Printed Tee", 				true, 	9, 0,      0,  149, 1, 0,     0,     8,  -1, 0, "top_024", "top_024_002", ""),
		new Item(278, "Black Tee with Lips Print", 			true, 	9, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "top_024", "top_024_003", ""),
		    new Item(279, "Leopard Print Tee", 					true, 	9, 0,  38600,    0,12, 0,    17,     0,  -1, 5, "top_024", "top_024_004", ""),
		    new Item(280, "White Cat Tee", 						true, 	9, 0,      0,  392, 6, 0,     0,    25,  -1, 4, "top_024", "top_024_005", ""),
		new Item(479, "Flight Attendant Shirt", 			true, 	9, 0,      0,    	0, 	1, 0,     	  0,   0,  -1, 0, "top_028", "top_028_001", ""),
		new Item(480, "Storm Lady Top", 					true, 	9, 0,      0,    	0, 	1, 0,     	  0,   0,  -1, 0, "top_030", "top_030_001", ""),


		
		//panties
	    //       id, name,   							visible,subcat,brand,price,  gem,lv,feature, sexy, lively,reward,dlc,meta
		    new Item(210, "Lavender Ruched Bottom", 		true, 	  22,    0,     0,   260,  4,   0,     0,     14,  -1, 3, "panties_001", "panties_001_001", ""),
		    new Item(211, "Summer Floral Ruched Bottom", 	true,     22,    0, 17800,     0,  4,   0,     0,      8,  -1, 3, "panties_001", "panties_001_003", ""),
		    new Item(220, "Queen Lace Brief", 				true, 	  22,    0, 22400,     0,  6,   0,    10,      0,  -1, 4, "panties_001", "panties_001_004", ""),
		new Item(221, "Classic Yellow Brief", 			true, 	  22,    0,  8900,     0,  1,   0,     0,      4,  -1, 0, "panties_001", "panties_001_005", ""),
		    new Item(212, "Sexy Red Bikini Bottom", 		true,     22,    0, 17800,     0,  4,   0,     8,      0,  -1, 3, "panties_002", "panties_002_001", ""),
		    new Item(213, "Navy White Striped Bottom", 		true, 	  22,    0, 22400,     0,  6,   0,     0,     10,  -1, 4, "panties_002", "panties_002_002", ""),
		new Item(214, "Basic Black Bikini Bottom", 		true, 	  22,    0,  8900,     0,  1,   0,     4,      0,  -1, 0, "panties_002", "panties_002_003", ""),
		new Item(215, "Basic White Bikini Bottom", 		true, 	  22,    0,     0,     0,  0,   0,     0,      0,  -1, 0, "panties_002", "panties_002_004", ""),
		    new Item(216, "Flower Lady Bikini Bottom", 		true, 	  22,    0, 27000,     0,  8,   0,    12,      0,  -1, 4, "panties_002", "panties_002_005", ""),
		    new Item(217, "Candy Girl Ruffle-trim Bottom", 	true, 	  22,    0, 27000,     0,  8,   0,     0,     12,  -1, 4, "panties_003", "panties_003_001", ""),
		    new Item(218, "Pink Ruffle Bottom", 			true, 	  22,    0,     0,   260,  4,   0,    14,      0,  -1, 3, "panties_003", "panties_003_002", ""),
		//new Item(219, "Panties_001_001", 				true, 	  22,    0,     0,     0,  1,   0,     0,      0,  -1, 0, "panties_004", "panties_004_001", ""),
		new Item(222, "Neon Yellow Hiphugger", 			true, 	  22,    0,     0,   149,  1,   0,     0,      8,  -1, 0, "panties_005", "panties_005_001", ""),
		new Item(223, "Neon Pink Hiphugger", 			true, 	  22,    0,     0,   149,  1,   0,     0,      8,  -1, 0, "panties_005", "panties_005_002", ""),
		    new Item(224, "Vintage Blue Hiphugger", 		true, 	  22,    0,     0,   371,  6,   0,     0,     20,  -1, 4, "panties_005", "panties_005_003", ""),
		    new Item(225, "Rouge String Bottom", 			true, 	  22,    0,     0,   368,  8,   0,    20,      0,  -1, 4, "panties_006", "panties_006_001", ""),
		
		// shoes
	    //       id, name,  							visible,subcat,brand,price,gem,lv,feature,sexy,lively,reward,dlc,meta
		//new Item(327, "None",   							true, 	14, 0,      0,    0,-1, 0,     0,     0,  -1, 0, "Null"),
		    new Item(328, "Pink Wedge Sneakers", 				true, 	14, 0,      0,  343,10, 0,     0,    19,  -1, 4, "shoes_001", "shoes_001_001", ""),
		    new Item(329, "All-Star Wedge Sneakers", 			true, 	14, 0,      0,  214, 4, 0,     0,    11,  -1, 3, "shoes_001", "shoes_001_002", ""),
		new Item(330, "Brown Gladiator High Heels", 		true, 	14, 0,   6600,    0, 1, 0,     0,     0,  -1, 0, "shoes_002", "shoes_002_001", ""),
		    new Item(331, "Blue Gladiator High Heels", 			true, 	14, 0,  15600,    0, 5, 0,     0,     7,  -1, 3, "shoes_002", "shoes_002_002", ""),
		new Item(332, "Black Leather Knee-High Boots", 		true, 	14, 0,      0,  114, 1, 0,     0,     0,  -1, 0, "shoes_003", "shoes_003_001", ""),
		    new Item(333, "Gold Laced Knee-High Boots", 		true, 	14, 0,      0,  368,11, 0,    20,     0,  -1, 5, "shoes_003", "shoes_003_002", ""),
		    new Item(334, "Red T-Strap Pumps", 					true, 	14, 0,      0,  260, 6, 0,    14,     0,  -1, 4, "shoes_004", "shoes_004_001", ""),
		new Item(335, "Pastel T-Strap Pumps", 				true, 	14, 0,      0,  149, 2, 0,     0,     8,  -1, 0, "shoes_004", "shoes_004_002", ""),
		new Item(336, "Orchid Ankle Strap Wedge", 			true, 	14, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "shoes_005", "shoes_005_001", ""),
		    new Item(337, "Orange Leopard Ankle Strap Wedge", 	true, 	14, 0,  20100,    0, 7, 0,     9,     0,  -1, 4, "shoes_005", "shoes_005_002", ""),
		    new Item(338, "Green Leopard Ankle Strap Wedge", 	true, 	14, 0,      0,  289, 7, 0,    16,     0,  -1, 4, "shoes_005", "shoes_005_003", ""),
		new Item(339, "Pink Patent Pumps", 					true, 	14, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "shoes_006", "shoes_006_002", ""),
		    new Item(340, "Pink Speckled Pumps", 				true, 	14, 0,  31600,    0,12, 0,    14,     0,  -1, 5, "shoes_006", "shoes_006_003", ""),
		    new Item(341, "Peach Strappy Wedge", 				true, 	14, 0,  29300,    0,11, 0,     0,    13,  -1, 5, "shoes_007", "shoes_007_001", ""),
		    new Item(342, "Blue Leopard Strappy Wedge", 		true, 	14, 0,  27000,    0,10, 0,     0,    12,  -1, 4, "shoes_007", "shoes_007_002", ""),
		    new Item(343, "Purple Ankle Strap Heels", 			true, 	14, 0,  17800,    0, 6, 0,     8,     0,  -1, 4, "shoes_008", "shoes_008_001", ""),
		    new Item(344, "Pink Ankle Strap Heels", 			true, 	14, 0,      0,  380,12, 0,    21,     0,  -1, 5, "shoes_008", "shoes_008_002", ""),
		new Item(345, "Gray Ankle Boots", 					true, 	14, 0,      0, 	149, 2, 0,     0,     8,  -1, 0, "shoes_009", "shoes_009_001", ""),
		new Item(346, "Mint Green Ankle Boots", 			true, 	14, 0,  	0,    0, 0, 0,     0,     0,  -1, 0, "shoes_009", "shoes_009_002", ""),
		    new Item(347, "Black Studded Ankle Boots", 			true, 	14, 0,      0,  303, 8, 0,    16,     0,  -1, 4, "shoes_009", "shoes_009_003", ""),
		    new Item(348, "Blue Pumps with Crystal Heels", 		true, 	14, 0,  22400,    0, 8, 0,    10,     0,  -1, 4, "shoes_010", "shoes_010_001", ""),
		new Item(349, "Black Carved Wedge Sandals", 		true, 	14, 0,   8900,    0, 2, 0,     0,     4,  -1, 0, "shoes_013", "shoes_013_001", ""),
		    new Item(350, "Lime Green Ankle Strap Wedge", 		true, 	14, 0,  13300,    0, 4, 0,     0,     5,  -1, 3, "shoes_014", "shoes_014_001", ""),




		// socks
	    //       id, name,  							visible,subcat,brand,price,gem,lv,feature,sexy,lively,reward,dlc,meta
		new Item(374, "None",   							true, 	16, 0,      0,    0,-1, 0,     0,     0,  -1, 0, "Null"),
		    new Item(375, "Black Spotted Lace Stockings", 		true, 	16, 0,  22400,    0,10, 0,    10,     0,  -1, 4, "socks_001", "sock_001_001", ""),
		    new Item(376, "Striped Sheer Ribbon Stockings", 	true, 	16, 0,      0,  289, 9, 0,    16,     0,  -1, 4, "socks_001", "sock_001_002", ""),
		    new Item(377, "Pink Lace Ribbon Stockings", 		true, 	16, 0,  22400,    0,10, 0,    10,     0,  -1, 4, "socks_001", "sock_001_003", ""),
		    new Item(378, "Pink Checkered Socks", 				true, 	16, 0,  15600,    0, 6, 0,     0,     7,  -1, 4, "socks_002", "sock_001_001", ""),
		    new Item(379, "Multicolor Striped Socks", 			true, 	16, 0,  15600,    0, 6, 0,     0,     7,  -1, 4, "socks_002", "sock_001_002", ""),
		    new Item(380, "Embroidered Heart White Socks", 		true, 	16, 0,      0,  230, 6, 0,     0,    12,  -1, 4, "socks_002", "sock_001_003", ""),



		// leggings
	    //       id, name,  							visible,subcat,brand,price,gem,lv,feature,sexy,lively,reward,dlc,meta
		new Item(381, "None",   							true, 	27, 0,      0,    0,-1, 0,     0,     0,  -1, 0, "Null"),
		new Item(382, "Neon Green Spandex Leggings", 		true, 	27, 0,   6600,    0, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_001", ""),
		new Item(383, "Pink Spandex Leggings", 				true, 	27, 0,   6600,    0, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_002", ""),
		new Item(384, "Hot Pink Spandex Leggings", 			true, 	27, 0,   6600,    0, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_003", ""),
		new Item(385, "Bright Orange Spandex Leggings", 	true, 	27, 0,   6600,    0, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_004", ""),
		new Item(386, "Bright Sky Blue Spandex Leggings", 	true, 	27, 0,   6600,    0, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_005", ""),
		new Item(387, "Electric Blue Spandex Leggings", 	true, 	27, 0,   6600,    0, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_006", ""),
		new Item(388, "Gray Matte Leggings", 				true, 	27, 0,   6600,    0, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_007", ""),
		new Item(389, "Purple Spandex Leggings", 			true, 	27, 0,   6600,    0, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_008", ""),
		    new Item(390, "Blue Smiley Print Leggings", 		true, 	27, 0,      0,  214, 5, 0,     0,    11,  -1, 3, "legging_001", "legging_001_009", ""),
		    new Item(391, "Pink and Black Pattered Leggings", 	true, 	27, 0,   8900,    0, 3, 0,     0,     4,  -1, 3, "legging_001", "legging_001_010", ""),
		    new Item(392, "Star Print White Leggings", 			true, 	27, 0,   8900,    0, 3, 0,     0,     4,  -1, 3, "legging_001", "legging_001_011", ""),
		new Item(393, "Cross Print Black Leggings", 		true, 	27, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_012", ""),
		new Item(394, "Rainbow Leopard Print Leggings", 	true, 	27, 0,      0,  166, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_013", ""),
		    new Item(395, "Purple Zebra Print Leggings", 		true, 	27, 0,   8900,    0, 3, 0,     0,     4,  -1, 3, "legging_001", "legging_001_014", ""),
		    new Item(396, "Purple/Pink Zebra Print Leggings", 	true, 	27, 0,   8900,    0, 3, 0,     0,     4,  -1, 3, "legging_001", "legging_001_015", ""),
		new Item(397, "Giraffe Print Leggings", 			true, 	27, 0,      0,  166, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_016", ""),
		new Item(398, "Neon Striped Leggings", 				true, 	27, 0,      0,  166, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_017", ""),
		new Item(399, "Neon Checkered Leggings", 			true, 	27, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_018", ""),
		    new Item(400, "Black Kitty Leggings", 				true, 	27, 0,      0,  182, 4, 0,     0,    10,  -1, 3, "legging_001", "legging_001_019", ""),
		new Item(401, "Galactic Print Leggings", 			true, 	27, 0,      0,  166, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_020", ""),
		    new Item(402, "Beaded Embellished Leggings", 		true, 	27, 0,  11000,    0, 4, 0,     4,     0,  -1, 3, "legging_001", "legging_001_021", ""),
		    new Item(403, "Black Laced-Up Tights", 				true, 	27, 0,  11000,    0, 4, 0,     4,     0,  -1, 3, "legging_001", "legging_001_022", ""),
		    new Item(404, "Gold Dust Leggings", 				true, 	27, 0,      0,  260, 7, 0,     0,    14,  -1, 4, "legging_001", "legging_001_023", ""),
		new Item(405, "Black Side-Ripped Leggings", 		true, 	27, 0,      0,  166, 1, 0,     0,     0,  -1, 0, "legging_001", "legging_001_024", ""),
		    new Item(406, "Purple Leopard Print Leggings", 		true, 	27, 0,  13300,    0, 5, 0,     0,     5,  -1, 3, "legging_001", "legging_001_025", ""),
		    new Item(407, "Gold X Athletic Tights", 			true, 	27, 0,  17800,    0, 7, 0,     0,     8,  -1, 4, "legging_001", "legging_001_026", ""),
		    new Item(408, "Beige Printed Knitted Leggings", 	true, 	27, 0,      0,  317,10, 0,     0,    17,  -1, 4, "legging_002", "legging_002_001", ""),
		    new Item(409, "Eifel Tower Print Leggings", 		true, 	27, 0,      0,  289,10, 0,    16,     0,  -1, 4, "legging_002", "legging_002_002", ""),
		    new Item(410, "White Sheer Lace Leggings", 			true, 	27, 0,  24700,    0,11, 0,    11,     0,  -1, 5, "legging_002", "legging_002_003", ""),
		    new Item(411, "Black Sheer Lace Leggings", 			true, 	27, 0,  20100,    0, 8, 0,     9,     0,  -1, 4, "legging_002", "legging_002_004", ""),
		    new Item(412, "Black Rose Lace Leggings", 			true, 	27, 0,  22400,    0, 9, 0,    10,     0,  -1, 4, "legging_002", "legging_002_005", ""),
		    new Item(413, "Black Fishnet Leggings", 			true, 	27, 0,      0,  275, 8, 0,    15,     0,  -1, 4, "legging_002", "legging_002_006", ""),
		
		// necklace
	    //       id, name,  							visible,subcat,brand,price,gem,lv,feature,sexy,lively,reward,dlc,meta
		new Item(414, "None",   							true, 	25, 0,      0,    0,-1, 0,     0,     0,  -1, 0, "Null"),
		new Item(415, "Bejeweled Choker", 					true, 	25, 0,      0,   77, 1, 0,     0,     0,  -1, 0, "necklace_001", "necklace_001_001", ""),
		    new Item(416, "Gold Fringe Necklace", 				true, 	25, 0,  13300,    0, 8, 0,     5,     0,  -1, 4, "necklace_002", "necklace_002_001", ""),
		new Item(417, "Triple Pendant Necklace", 			true, 	25, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "necklace_003", "necklace_003_001", ""),
		    new Item(418, "Geometric Triangle Necklace", 		true, 	25, 0,   8900,    0, 4, 0,     0,     0,  -1, 3, "necklace_004", "necklace_004_001", ""),
		    new Item(419, "Layered Ribbon Necklace ", 			true, 	25, 0,  20100,    0,12, 0,     9,     0,  -1, 5, "necklace_005", "necklace_005_001", ""),
		    new Item(420, "Geometric Bib Necklace", 			true, 	25, 0,      0,  198, 8, 0,    11,     0,  -1, 4, "necklace_006", "necklace_006_001", ""),
		new Item(421, "Chunky Gold Bib Necklace", 			true, 	25, 0,   4400,    0, 1, 0,     0,     0,  -1, 0, "necklace_007", "necklace_007_001", ""),
		    new Item(422, "Silver V-Shaped Necklace", 			true, 	25, 0,  11100,    0, 6, 0,     4,     0,  -1, 4, "necklace_008", "necklace_008_001", ""),
		    new Item(423, "Shooting Star Necklace", 			true, 	25, 0,  15600,    0,10, 0,     7,     0,  -1, 4, "necklace_009", "necklace_009_001", ""),
		new Item(424, "Gold Multi Charms Necklace", 		true, 	25, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "necklace_010", "necklace_010_001", ""),
		    new Item(425, "Animal Print Collar Necklace", 		true, 	25, 0,      0,  230,10, 0,     0,    12,  -1, 4, "necklace_011", "necklace_011_001", ""),
		new Item(426, "Three Layered Gold Chain", 			true, 	25, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "necklace_012", "necklace_012_001", ""),
		new Item(427, "Arrowed Heart Pendant", 				true, 	25, 0,      0,    0, 1, 0,     0,     0,  -1, 0, "necklace_013", "necklace_013_001", ""),
		    new Item(428, "Sapphire Bib Necklace", 				true, 	25, 0,      0,  230,10, 0,     7,     0,  -1, 4, "necklace_014", "necklace_014_001", ""),
		    new Item(429, "Pink Rose Lace Choker", 				true, 	25, 0,      0,  245,12, 0,    13,     0,  -1, 5, "necklace_015", "necklace_015_001", ""),
		new Item(483, "Gold Sapphire Choker", 					true, 	25, 0,      0,    0,  1, 0,  0,   0,  -1, 0, "necklace_015", "necklace_015_002", ""),
		    new Item(430, "Diamond Studded Gold Chain", 		true, 	25, 0,      0,  166, 6, 0,     9,     0,  -1, 4, "necklace_018", "necklace_018_001", ""),
		    new Item(431, "Studded Collar Necklace", 			true, 	25, 0,  15600,    0,10, 0,     0,     7,  -1, 4, "necklace_019", "necklace_019_001", ""),
		    new Item(432, "Rose Gold Geometric Necklace", 		true, 	25, 0,      0,  131, 4, 0,     0,     0,  -1, 3, "necklace_020", "necklace_020_001", ""),

		new Item(665, "None", 							true, 	24, 0,      0,  0, 0, 0,     0,     0,  -1, 0, "Null"),
#if UNITY_IPHONE
		// winter - dress?
		//       id, name,  						visible,subcat,brand,price,gem,lv,feature,sexy,lively,reward,dlc,meta
		new Item(484, "Faux Fur Tube Dress",			true,	10, 0,  27000,    0,  8, 0,  0, 12,  -1, 7, "dress_014", "dress_014_003", ""),
		new Item(485, "Fairy Princess Ballerina Dress",	true,	10, 0,      0,  448, 12, 0, 24,  0,  -1, 7, "dress_034", "dress_034_002", ""),
		new Item(486, "Blue Fur-Trimmed Coat Dress",	true,	10, 0,      0,  392,  9, 0, 21,  0,  -1, 7, "dress_044", "dress_044_001", ""),
		new Item(487, "Snowman Puff Dress",				true,	10, 0,      0,  448, 12, 0,  0, 24,  -1, 7, "dress_045", "dress_045_001", ""),
		new Item(488, "Festive Tree Dress",				true,	10, 0,      0,  468, 13, 0,  0, 26,  -1, 7, "dress_047", "dress_047_001", ""),
		new Item(489, "Short Santa Jumpsuit",			true,	10, 0,      0,  488, 14, 0, 27,  0,  -1, 7, "dress_053", "dress_053_001", ""),
		new Item(490, "Gingerbread Jumpsuit",			true,	10, 0,  41000,    0,  9, 0,  0, 18,  -1, 7, "dress_054", "dress_054_001", ""),
		new Item(491, "Beige Embroidered Gown",			true,	10, 0,      0,  772, 17, 0, 42,  0,  -1, 7, "dress_060", "dress_060_001", ""),
		//new Item(492, "Dress 492",		true,	10, 0,      0,  700, 15, 0, 38,  0,  -1, 0, "dress_061", "dress_061_001", ""),
		new Item(493, "Snow Queen Silk Gown",			true,	10, 0,      0,  871, 20, 0, 48,  0,  -1, 7, "dress_062", "dress_062_001", ""),
		new Item(494, "Gold A-Line Tube Top",			true,	10, 0,      0,  468, 13, 0, 26,  0,  -1, 7, "dress_014", "dress_014_004", ""),
		new Item(495, "Blue Gem Embellished Dress",		true,	10, 0,      0,  468, 13, 0, 26,  0,  -1, 7, "dress_018", "dress_018_001", ""),
		new Item(496, "Patterned Strapless Ball Gown",	true,	10, 0,      0,  772, 17, 0, 42,  0,  -1, 7, "dress_028", "dress_028_002", ""),
		new Item(497, "Swirly Mermaid Gown",			true,	10, 0,      0,  488, 14, 0, 27,  0,  -1, 7, "dress_040", "dress_040_002", ""),
		new Item(499, "Royal Blue Silk Strapless Gown",	true,	10, 0,      0,  488, 14, 0, 27,  0,  -1, 0, "dress_027", "dress_027_003", ""),


		// winter - hat
		new Item(513, "Silk fairy Wings", 		true, 	23, 0, 	      0,  289, 14, 0, 	    0, 	   16,  -1, 7, "hat_003", "hat_003_001", ""),
		new Item(514, "Fuzzy Reindeer Headband", true, 	23, 0, 	  20100,    0, 12, 0, 	    9, 	    0,  -1, 7, "hat_040", "hat_040_001", ""),
		new Item(515, "Santa Hat", 				true, 	23, 0, 	  15600,    0, 10, 0, 	    7, 	    0,  -1, 7, "hat_042", "hat_042_001", ""),
		new Item(516, "Snow Queen Tiara", 		true, 	23, 0, 	      0,  289, 14, 0, 	    0, 	   16,  -1, 7, "hat_043", "hat_043_001", ""),

		// winter - skirt
		new Item(582, "Red Fur Trimmed Mini Skirt", true,13, 0,       0,   317, 6,  0,     17,      0,  -1, 7, "skirt_013", "skirt_013_001", ""),

		// winter - top
		new Item(623, "Tied Red Half Cardigan", 	true, 	9, 0,      0,  317, 6, 	0,     17,      0,  -1, 7, "top_027", "top_027_001", ""),

		// winter - shoes
		//new Item(558, "Shoe 558", 		true, 	14, 0,  13300,    0, 4, 0,     0,     5,  -1, 0, "shoes_015", "shoes_015_001", ""),
		//new Item(559, "Shoe 559", 		true, 	14, 0,  13300,    0, 4, 0,     0,     5,  -1, 0, "shoes_017", "shoes_017_001", ""),
		new Item(560, "Fur Trimmed Leather Boots", 	true, 	14, 0,      0,  343, 10, 0,     0,    19,  -1, 7, "shoes_019", "shoes_019_001", ""),
		new Item(561, "White Low Cut Boots", 		true, 	14, 0,  22400,    0,  8, 0,    10,     0,  -1, 7, "shoes_023", "shoes_023_001", ""),
		new Item(562, "Blue Glacier Wedge", 		true, 	14, 0,      0,  380, 12, 0,    21,     0,  -1, 7, "shoes_024", "shoes_024_001", ""),
		new Item(563, "Furry White Boots", 			true, 	14, 0,      0,  380, 12, 0,     0,    21,  -1, 7, "shoes_027", "shoes_027_001", ""),

		// winter -socks
		new Item(602, "Red and White Striped Stockings",true, 	16, 0, 20100,   0, 8, 0,     9,    0,  -1, 7, "socks_001", "sock_001_004", ""),
		new Item(603, "Red and White Striped Socks", 	true, 	16, 0, 17800,   0, 7, 0,     8,    0,  -1, 7, "socks_002", "sock_001_004", ""),

		// winter - necklace
		new Item(536, "Gold and Red Velvet Choker", true, 	25, 0, 15600,  0, 10, 0,     7,     0,  -1, 7, "necklace_021", "necklace_021_001", ""),
		new Item(537, "Snowflake Necklace", 		true, 	25, 0, 15600,  0, 10, 0,     7,     0,  -1, 7, "necklace_022", "necklace_022_001", ""),
		new Item(538, "Red Checkered Scarf", 		true, 	25, 0, 15600,  0, 10, 0,     7,     0,  -1, 7, "necklace_023", "necklace_023_001", ""),

		// winter - glasses

		
		//       id, name,  						visible,subcat,brand,price,gem,lv,feature,sexy,lively,reward,dlc,meta
		new Item(667, "Bejeweled Pinhole Glasses", 		true, 	24, 0,      0,  404,13, 0,     0,    22,  -1, 7, "glasses_002", "glasses_002_001", ""),
		new Item(668, "Brown Framed Glasses", 			true, 	24, 0,  22400,    0, 8, 0,     0,    10,  -1, 7, "glasses_003", "glasses_003_001", ""),
		new Item(669, "Black Framed Sunglasses", 		true, 	24, 0,      0,  330, 9, 0,    18,     0,  -1, 7, "glasses_004", "glasses_004_001", ""),
		new Item(670, "Floral Sunglasses", 				true, 	24, 0,      0,  303, 8, 0,    16,     0,  -1, 7, "glasses_005", "glasses_005_001", ""),
		new Item(671, "Bronze Vintage Sunglasses", 		true, 	24, 0,      0,  343, 9, 0,     0,    19,  -1, 7, "glasses_006", "glasses_006_001", ""),
		new Item(672, "Leopard Print Framed Glasses", 	true, 	24, 0,      0,  343, 9, 0,    19,     0,  -1, 7, "glasses_007", "glasses_007_001", ""),
		//new Item(673, "Glasses 673", 		true, 	24, 0,      0,  131, 1, 0,     0,     0,  -1, 0, "glasses_008", "glasses_008_001", ""),
		new Item(674, "Famous Mouse Sunglasses", 		true, 	24, 0,      0,  260, 6, 0,     0,    14,  -1, 7, "glasses_009", "glasses_009_001", ""),
		new Item(675, "Oversized Sunglasses", 			true, 	24, 0,      0,  330, 9, 0,    18,     0,  -1, 7, "glasses_010", "glasses_010_001", ""),
		//new Item(676, "Glasses 676", 		true, 	24, 0,      0,  131, 1, 0,     0,     0,  -1, 0, "glasses_011", "glasses_011_001", ""),
		new Item(677, "Sharp Edged Sunglasses", 		true, 	24, 0,      0,  260, 6, 0,     0,    14,  -1, 7, "glasses_012", "glasses_012_001", ""),
		new Item(678, "Pink Tinted Sunglasses", 		true, 	24, 0,  22400,    0, 8, 0,    10,     0,  -1, 7, "glasses_013", "glasses_013_001", ""),
		//new Item(679, "Glasses 679", 		true, 	24, 0,      0,  131, 1, 0,     0,     0,  -1, 0, "glasses_014", "glasses_014_001", ""),
		new Item(680, "Silver Framed Sunglasses", 		true, 	24, 0,  17800,    0, 6, 0,     0,     8,  -1, 7, "glasses_015", "glasses_015_001", ""),
		new Item(681, "Round Smoke Lens Sunglasses", 	true, 	24, 0,      0,  303, 8, 0,    16,     0,  -1, 7, "glasses_016", "glasses_016_001", ""),
		new Item(682, "Smoky Hooded Sunglasses", 		true, 	24, 0,  17800,    0, 6, 0,     0,     8,  -1, 7, "glasses_017", "glasses_017_001", ""),
#endif
	};
}


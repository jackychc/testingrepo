using System;
using System.Collections.Generic;


public class Topic
{
	public int topicId;
	public string topicName;
	public bool isSpecial;
	public string endDate;
	
	public bool isEnded;
	
	public int votes;
	public int rank;

	public List<Tier> tiers = new List<Tier>();

	
	public Topic (bool pIsSpecial)
	{
		isSpecial = pIsSpecial;
	}
	
	public class Tier
	{
		public int tierId;
		public int requirement;
		public int rank;
		public List<Reward> rewards = new List<Reward>();
		public bool claimed;
	}
	
	public List<int> getCountTiers()
	{
		//int[] returnMe = {10000,10000,10000};
		List<int> temp = new List<int>();
		
		foreach (Tier t in tiers)
		{
			if (t.requirement > 0)
			{
				temp.Add(t.requirement);
			}
		}
		
		temp.Sort();
		
		return temp;
		
		// suppose temp length = 3;
		
		/*for (int i = 0; i < 3; i++)
		{
			returnMe[i] = temp[temp.Count-1];
		}
		
		for (int i = 0; i < temp.Count; i++)
		{
			returnMe[i] = temp[i];
		}
		
		return returnMe;*/
		
	}
	
	public Tier getTier(int id)
	{
		return tiers.Find(delegate(Tier t) { return (t.tierId == id);});
	}
	// pos = 0,1,2
	public Tier getTierAt(int pos)
	{
		return tiers.Find(delegate(Tier t) { return (t.requirement == getCountTiers()[pos]);});
	}
	
	/*
	
	public int getTierIdAt(int pos)
	{
		return tiers.Find(delegate(Tier t) { return (t.requirement == getCountTiers()[pos]);}).tierId;
	}*/
	
	public bool isClaimedRankReward()
	{
		foreach (Tier t in tiers)
		{
			if (t.rank > 0 && t.claimed)
			{
				return true;
			}
		}
		return false;
	}
	
	
	void ClaimRewardAtTier(int tier)
	{
		
		// for later poster use la!
		/*int sumOfProbability = 0;
		List<int> rangeOfProbability = new List<int>();
		rangeOfProbability.Add(0);
		
		for (int i = 0; i < countRewards[tier].Count; i++)
		{
			Reward r = countRewards[tier][i];
			
			rangeOfProbability.Add(sumOfProbability + r.probability);
			sumOfProbability += r.probability;
			
		}
		
		int result = UnityEngine.Random.Range(1, sumOfProbability+1);
		
		for (int i = 1; i < countRewards[tier].Count+1; i++)
		{
			if (rangeOfProbability[i-1] < result && result <= rangeOfProbability[i])	
			{
				countRewards[tier][i-1].claim();
				return;
			}
		}*/
		
		
		// download a reward and claim.....
		
		
		// emergency exit
		//countRewards[tier][0].claim();
		
	}
}



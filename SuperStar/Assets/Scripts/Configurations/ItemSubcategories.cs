using System;
using System.Collections.Generic;


public class ItemSubcategories
{
	public enum Id
	{
		FRONTHAIR 		= 1,
		BACKHAIR 		= 2,
		SKINCOLOR		= 3,
		EYEBROWS		= 4,
		EYESHAPE		= 5,
		EYEBALLS		= 6,
		ROUGE			= 7,
		LIPS			= 8,
		TOP				= 9,
		DRESS			= 10,
		COAT			= 11,
		ACCESSORIES 	= 12,
		SKIRT			= 13,
		TROUSERS        = 26,
		SHOES			= 14,
		BRA		        = 15,
		PANTIES		    = 22,
		HAT 	        = 23,
		GLASSES         = 24,
		NECKLACE        = 25,
		SOCKS			= 16,
		LEGGINGS        = 27, //max
		HANDBAGS		= 17,
		HAIRCOLOR		= 18,
		LIPSCOLOR		= 19,
		EYESHAPECOLOR	= 20,
		PHOTOSHOTBG		= 21,
	}
	
	public static List<ItemSubcategory> allSubcategories = new List<ItemSubcategory>()
	{
		new ItemSubcategory((int)Id.FRONTHAIR, "FRONTHAIR", "Front Hair", false, ""),
		new ItemSubcategory((int)Id.BACKHAIR, "BACKHAIR", "Back Hair", false, ""),
		new ItemSubcategory((int)Id.SKINCOLOR, "SKINCOLOR", "Skin Color", false, ""),
		new ItemSubcategory((int)Id.EYEBROWS, "EYEBROWS", "Eye Brows", false, ""),
		new ItemSubcategory((int)Id.EYESHAPE, "EYESHAPE", "Eye Shape", false, ""),
		new ItemSubcategory((int)Id.EYEBALLS, "EYEBALL", "Eye Balls", false, ""),
		new ItemSubcategory((int)Id.ROUGE, "ROUGE", "Rouge", false, ""),
		new ItemSubcategory((int)Id.LIPS, "LIPS", "Lips", false, ""),
		new ItemSubcategory((int)Id.TOP, "TOP", "Top", true, ""),
		new ItemSubcategory((int)Id.DRESS, "DRESS", "One piece dress" , true, ""),
		new ItemSubcategory((int)Id.COAT, "COAT", "Coat", true, ""),
		new ItemSubcategory((int)Id.ACCESSORIES, "Accessories", "Accessories", true, ""),
		new ItemSubcategory((int)Id.SKIRT, "SKIRT", "Skirt/Pants", true, ""),
		new ItemSubcategory((int)Id.TROUSERS, "TROUSERS", "Trousers", true, ""),
		new ItemSubcategory((int)Id.SHOES, "SHOES", "Shoes", true, ""),
		new ItemSubcategory((int)Id.BRA, "BRA", "Bra", true, "9,10,11,13"),
		new ItemSubcategory((int)Id.PANTIES, "PANTIES", "Panty", true, "9,10,11,13,27"),
		new ItemSubcategory((int)Id.HAT, "HAT", "Hair accessory", true, ""),
		new ItemSubcategory((int)Id.GLASSES, "GLASSES", "Glasses", true, ""),
		new ItemSubcategory((int)Id.NECKLACE, "NECKLACE", "Necklace", true, ""),
		new ItemSubcategory((int)Id.SOCKS, "SOCKS", "Socks", true, ""),
		new ItemSubcategory((int)Id.LEGGINGS, "LEGGINGS", "Leggings", true, ""),
		new ItemSubcategory((int)Id.HANDBAGS, "HANDBAG", "Handbag", true, ""),
		new ItemSubcategory((int)Id.HAIRCOLOR, "HAIRCOLOR", "Hair Color", false, ""),
		new ItemSubcategory((int)Id.LIPSCOLOR, "LIPSCOLOR", "Lips Color", false, ""),
		new ItemSubcategory((int)Id.EYESHAPECOLOR, "EYESHAPECOLOR", "Eye Shape Color", false, ""),
		
		//Non-Character Assets Subcat
		new ItemSubcategory((int)Id.PHOTOSHOTBG, "PHOTOSHOTBG", "Photoshot Background", false, ""),
		
		/*
		new ItemSubcategory(1, "Skin", "Body Skin"),
		new ItemSubcategory(2, "FrontHair", "Front Hair"),
		new ItemSubcategory(3, "BackHair", "Back Hair"),
		new ItemSubcategory(4, "Eyeball", "Eyeball"),
		new ItemSubcategory(5, "Eyebrow", "Eyebrow"),
		new ItemSubcategory(6, "Lips", "Lips"),
		new ItemSubcategory(7, "Eyeshadow", "Eyeshadow"),
		new ItemSubcategory(8, "Eyelashes", "Eyelashes"),
		new ItemSubcategory(9, "Rouge", "Rouge"),
		new ItemSubcategory(10, "Bra", "Bra"),
		new ItemSubcategory(11, "Panties", "Panties"),
		new ItemSubcategory(12, "Front Hair", "Front Hair"),
		new ItemSubcategory(13, "Skin", "Body Skin"),
		new ItemSubcategory(14, "Front Hair", "Front Hair"),
		new ItemSubcategory(15, "Skin", "Body Skin"),
		new ItemSubcategory(16, "Front Hair", "Front Hair"),
		new ItemSubcategory(17, "Skin", "Body Skin"),
		new ItemSubcategory(18, "Front Hair", "Front Hair"),
		new ItemSubcategory(19, "Skin", "Body Skin"),
		new ItemSubcategory(20, "Front Hair", "Front Hair"),
		new ItemSubcategory(21, "Skin", "Body Skin"),
		new ItemSubcategory(22, "Front Hair", "Front Hair"),
		new ItemSubcategory(23, "Skin", "Body Skin")*/
	};
	
	public static ItemSubcategory getSubcategory(int id)
	{
		foreach(ItemSubcategory subcat in allSubcategories)
		{
			if (subcat.subcategoryid == id)
				return subcat;
		}
		
		return null;
	}
	
	public static ItemSubcategory getSubcategory(string name)
	{
		foreach(ItemSubcategory subcat in allSubcategories)
		{
			if (subcat.subcategoryName.ToLowerInvariant().Equals(name.ToLowerInvariant()))
				return subcat;
		}
		
		return null;
	}
}

public class ItemSubcategory
{
	public int subcategoryid;
	public string subcategoryName;
	public string subcategoryDescription;
	public bool shouldShowItemName; // in closet/shop
	public string subcatsToHide;  // specify which subcats to hide when item of this subcat is clicked
	
	public ItemSubcategory(int pSubcategoryId, string pSubcategoryName, string pSubcategoryDescription, bool pShowItemName, string pHide)
	{
		this.subcategoryid = pSubcategoryId;
		this.subcategoryName = pSubcategoryName;
		this.subcategoryDescription = pSubcategoryDescription;
		this.shouldShowItemName = pShowItemName;
		
		this.subcatsToHide = pHide;

	}
}



using System;
using System.Collections.Generic;
using UnityEngine;


public class ItemConflicts
{
	public enum ConflictType :int
	{
		NULL = 0,
		S2S = 1,
		S2I = 2,
		I2S = 3,
		I2I = 4
	}
	
	public enum ConflictAction
	{
		NULL,
		DESTROY,
		ALTERNATIVE,
		NOTHING            
	}
	
	public static List<ItemConflictDB> initialConflictDBs = new List<ItemConflictDB>()
	{
		new ItemConflictDB(9,10, ConflictType.S2S, ConflictAction.DESTROY),
		new ItemConflictDB(10,9, ConflictType.S2S, ConflictAction.DESTROY),
		new ItemConflictDB(10,13, ConflictType.S2S, ConflictAction.DESTROY),
		new ItemConflictDB(13,10, ConflictType.S2S, ConflictAction.DESTROY),

		new ItemConflictDB(12,434, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,434, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(12,435, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,435, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(12,464, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,464, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(28,463, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(56,463, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(58,463, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(59,463, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(28,464, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(56,464, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(58,464, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(59,464, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(28,466, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(56,466, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(58,466, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(59,466, ConflictType.I2I, ConflictAction.ALTERNATIVE),	

#if UNITY_IPHONE
		new ItemConflictDB(12,436, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,438, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,439, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,440, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,447, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,449, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,455, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,457, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,458, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,459, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,461, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,464, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(12,468, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(13,436, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,438, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,439, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,440, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,447, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,449, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,455, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,457, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,458, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,459, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,461, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,468, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(17,245, ConflictType.S2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(56,450, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(28,450, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(56,465, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(28,465, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(59,450, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(59,465, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		// initial batch dlc		
		new ItemConflictDB(9,199, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(10,199, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(11,199, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		
		new ItemConflictDB(10,217, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,217, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(26,217, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		
		new ItemConflictDB(10,218, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,218, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(26,218, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		
		new ItemConflictDB(10,225, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,225, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(26,225, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(27,225, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		
		new ItemConflictDB(13,256, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,257, ConflictType.S2I, ConflictAction.ALTERNATIVE),		
		
		new ItemConflictDB(253,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(255,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(256,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(257,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(261,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(262,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(265,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(266,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(270,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(271,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(272,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(273,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(275,225, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		
		new ItemConflictDB(191,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(281,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(291,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(292,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(293,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(294,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(295,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(296,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(310,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(311,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(312,270, ConflictType.I2I, ConflictAction.ALTERNATIVE),
			
		new ItemConflictDB(191,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(281,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(291,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(292,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(293,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(294,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(295,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(296,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(310,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(311,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(312,265, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		
		new ItemConflictDB(191,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(281,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(291,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(292,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(293,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(294,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(295,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(296,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(310,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(311,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(312,266, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		
		new ItemConflictDB(191,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(281,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(291,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(292,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(293,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(294,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(295,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(296,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(310,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(311,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(312,271, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		
		new ItemConflictDB(191,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(281,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(291,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(292,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(293,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(294,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(295,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(296,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(310,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(311,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(312,272, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		
		new ItemConflictDB(191,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(281,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(291,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(292,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(293,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(294,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(295,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(296,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(310,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(311,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(312,273, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		
		new ItemConflictDB(191,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(281,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(291,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(292,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(293,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(294,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(295,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(296,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(310,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(311,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(312,274, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		// end of initial dlc

		// winter - conflicts
		//new ItemConflictDB(17,485, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		//new ItemConflictDB(25,486, ConflictType.S2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(17,487, ConflictType.S2I, ConflictAction.ALTERNATIVE),	
		new ItemConflictDB(17,488, ConflictType.S2I, ConflictAction.ALTERNATIVE),	
		new ItemConflictDB(25,488, ConflictType.S2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(12,514, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,514, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(12,516, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(13,516, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(28,515, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(56,515, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(58,515, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(59,515, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(81,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(82,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(97,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(98,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(227,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(235,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(237,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(238,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(239,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(240,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(241,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(242,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(243,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(245,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(246,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(247,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(248,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(228,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(304,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(305,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(306,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(307,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(320,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(321,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(322,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(323,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(324,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(490,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(491,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(495,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(496,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(497,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(499,560, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(81,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(82,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(97,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(98,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(227,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(235,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(237,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(238,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(239,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(240,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(241,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(242,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(243,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(245,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(246,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(247,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(248,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(228,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(304,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(305,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(306,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(307,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(320,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(321,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(322,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(323,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(324,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(490,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(491,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(495,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(496,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(497,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(499,563, ConflictType.I2I, ConflictAction.ALTERNATIVE),

		new ItemConflictDB(28,448, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(56,448, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(58,448, ConflictType.I2I, ConflictAction.ALTERNATIVE),
		new ItemConflictDB(59,448, ConflictType.I2I, ConflictAction.ALTERNATIVE)


	#endif

	};
	
	public static List<ItemConflict> allConflicts = new List<ItemConflict>();
	public static List<ItemConflictDB> allConflictDBs = new List<ItemConflictDB>();
	
	/*public static void AllQuery()
	{
		string q = 
		"INSERT INTO ItemConflicts  (conflictId1, conflictId2, conflictType, conflictAction) VALUES ";
				        
			
		foreach (ItemConflictDB a in allConflictDBs)
		{
			if (!q.Equals("INSERT INTO ItemConflicts  (conflictId1, conflictId2, conflictType, conflictAction) VALUES ")) q += ",";
			
			q += "("+
				       a.attacker +
				" , " + a.defenser +
				" , '" + a.conflictType.ToString() + "'" +
				" , '" + a.action.ToString() + "'" +
					")";
		}
		
		Log.Warning("the Query:"+q);
	}*/
	
	public static void Save()
	{
		//return;
		
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		//DatabaseManager.db.Open(Globals.dbSavePath);
		foreach (ItemConflictDB a in allConflictDBs)
		{
		
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM ItemConflicts WHERE item1Id = " + a.attacker + " AND item2Id = " + a.defenser,
				
				//update
				new List<string>(){
				"UPDATE ItemConflicts SET "+
					" conflictType = '" + a.conflictType.ToString() + "'" + 
					",conflictAction = '" + a.action.ToString() + "'" + 
					" WHERE item1Id = " + a.attacker + " AND item2Id = " + a.defenser
				},
			
				//insert
				new List<string>(){
					"INSERT INTO ItemConflicts  (item1Id, item2Id, conflictType, conflictAction) VALUES("+
				        a.attacker +
				" , " + a.defenser +
				" , '" + a.conflictType.ToString() + "'" +
				" , '" + a.action.ToString() + "'" +
					")"
				}
			);
			
			/*SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM ItemConflicts WHERE item1Id = " + a.attacker + " AND item2Id = " + a.defenser ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			
			
			if (hasRecord)
			{
				
				// mainly is update the action
				
				{
					DatabaseManager.SharedManager.Save("UPDATE ItemConflicts SET "+
					" conflictType = '" + a.conflictType.ToString() + "'" + 
					",conflictAction = '" + a.action.ToString() + "'" + 
					" WHERE item1Id = " + a.attacker + " AND item2Id = " + a.defenser);
				}
				
			}
			else
			{
				// insert
				DatabaseManager.SharedManager.Save("INSERT INTO ItemConflicts  (item1Id, item2Id, conflictType, conflictAction) VALUES("+
				        a.attacker +
				" , " + a.defenser +
				" , '" + a.conflictType.ToString() + "'" +
				" , '" + a.action.ToString() + "'" +
					")");
			}
			*/
			
			
		}
		//DatabaseManager.db.Close();
	}
	
	
	public static void Load()
	{
		//return;
		
		allConflictDBs.Clear();
		
		// 1. generate a conflict list base on preset values
		//ItemConflicts.InitialConflicts();
		
		SQLiteQuery qr;
		string querySelect = "SELECT * FROM ItemConflicts";

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			
			allConflictDBs.Add(new ItemConflictDB(
			qr.GetInteger("item1Id"),
			qr.GetInteger("item2Id"),
			(ItemConflicts.ConflictType) Enum.Parse(typeof(ItemConflicts.ConflictType), qr.GetString("conflictType")),
			(ItemConflicts.ConflictAction) Enum.Parse(typeof(ItemConflicts.ConflictAction), qr.GetString("conflictAction"))
			));
			
			
		}
			
		qr.Release();
		
		GenerateConflictsByDB();
		
		
	}
	
	public static void GenerateConflictsByDB()
	{
		allConflicts.Clear();
		
		allConflictDBs.Sort((x, y) => ((int)x.conflictType).CompareTo((int) y.conflictType));
		
		foreach(ItemConflictDB icdb in allConflictDBs)
		{
			if (icdb.conflictType.Equals(ConflictType.I2I))
			{
				//Debug.LogWarning("attacker/defenser:"+icdb.attacker+","+icdb.defenser);
				AddConflict(Items.getItem(icdb.attacker), Items.getItem(icdb.defenser),icdb.action);
			}
			
			else if (icdb.conflictType.Equals(ConflictType.I2S))
				AddConflict(Items.getItem(icdb.attacker), ItemSubcategories.getSubcategory(icdb.defenser), icdb.action);
			
			else if (icdb.conflictType.Equals(ConflictType.S2I))
				AddConflict(ItemSubcategories.getSubcategory(icdb.attacker), Items.getItem(icdb.defenser),icdb.action);
			
			else if (icdb.conflictType.Equals(ConflictType.S2S))
				AddConflict(ItemSubcategories.getSubcategory(icdb.attacker), ItemSubcategories.getSubcategory(icdb.defenser),icdb.action);
		}
	}
	
	
	public static void AddConflict(Item pAttacker, Item pDefenser, ConflictAction pAction)
	{
		
		
		if (!Items.isNoneItem(pAttacker) && !Items.isNoneItem(pDefenser)) //DO NOT include None items
		{
			if (getConflict(pAttacker.itemId, pDefenser.itemId) != null)
			{
				allConflicts.Remove(getConflict(pAttacker.itemId, pDefenser.itemId));	
			}
			
			allConflicts.Add(new ItemConflict(pAttacker.itemId, pDefenser.itemId, pAction));
		}
	}
	
	public static void AddConflict(Item pAttacker, ItemSubcategory pDefenser, ConflictAction pAction)
	{
		foreach (Item i in Items.getItemsFromSubcategory(pDefenser.subcategoryid))
		{
			if (!Items.isNoneItem(pAttacker) && !Items.isNoneItem(i))
			{
				if (getConflict(pAttacker.itemId, i.itemId) != null)
				{
					allConflicts.Remove(getConflict(pAttacker.itemId, i.itemId));	
				}
				
				allConflicts.Add(new ItemConflict(pAttacker.itemId, i.itemId, pAction));
				
			}
		}
	}
	
	public static void AddConflict(ItemSubcategory pAttacker, Item pDefenser, ConflictAction pAction)
	{
		foreach (Item i in Items.getItemsFromSubcategory(pAttacker.subcategoryid))
		{
			if (!Items.isNoneItem(i) && !Items.isNoneItem(pDefenser))
			{
				if (getConflict(i.itemId, pDefenser.itemId) != null)
				{
					allConflicts.Remove(getConflict(i.itemId, pDefenser.itemId));	
				}
				
				allConflicts.Add(new ItemConflict(i.itemId, pDefenser.itemId, pAction));
			}
		}
	}
	
	public static void AddConflict(ItemSubcategory pAttacker, ItemSubcategory pDefenser, ConflictAction pAction)
	{
		foreach (Item i in Items.getItemsFromSubcategory(pAttacker.subcategoryid))
		{
			foreach (Item j in Items.getItemsFromSubcategory(pDefenser.subcategoryid))
			{	
				if (!Items.isNoneItem(i) && !Items.isNoneItem(j))
				{
					if (getConflict(i.itemId, j.itemId) != null)
					{
						allConflicts.Remove(getConflict(i.itemId, j.itemId));	
					}
					
					allConflicts.Add(new ItemConflict(i.itemId, j.itemId, pAction));
				}
			}
		}
	}
	
	public static void Attack(Item i, Player p)
	{
		
		foreach (ItemConflict conflict in allConflicts)
		{
			if (conflict.attacker == i.itemId && p.isWearingItem(Items.getItem(conflict.defenser)))	
			{
				if (conflict.action.Equals(ItemConflicts.ConflictAction.DESTROY))
				{
					// make the defenser catagory to None item
					p.wearItem(Items.getNoneItemOfSubcategory(Items.getItem(conflict.defenser).subCategoryId));
				}
			}
		}
	}
	
	public static bool NeedToAlternative(Item i, Player p)
	{
		foreach (ItemConflict conflict in allConflicts)
		{
			if (conflict.action.Equals(ItemConflicts.ConflictAction.ALTERNATIVE))
				if (conflict.defenser == i.itemId && p.isWearingItem(Items.getItem(conflict.attacker)))	
				{
					return true;
				}
		}
			
			
		return false;
	}
	
	public static ItemConflict getConflict(int pAttacker, int pDefenser)
	{
		foreach (ItemConflict conflict in allConflicts)
		{
			if (conflict.attacker == pAttacker && conflict.defenser == pDefenser)
			{
				return conflict;
			}
		}
		
		return null;
	}
	
	public static ItemConflictDB getConflictDB(int pAttacker, int pDefenser, ConflictType pType)
	{
		foreach (ItemConflictDB conflictDB in allConflictDBs)
		{
			if (conflictDB.attacker == pAttacker && conflictDB.defenser == pDefenser && conflictDB.conflictType.Equals(pType))
			{
				return conflictDB;
			}
		}
		
		return null;
	}
	
	
}

// the object that for DB and generate
public class ItemConflictDB
{
	// itemId OR subcatId
	public int attacker, defenser;
	public ItemConflicts.ConflictType conflictType;
	public ItemConflicts.ConflictAction action;
	
	public ItemConflictDB (int pAttacker, int pDefenser, ItemConflicts.ConflictType pType, ItemConflicts.ConflictAction pAction)
	{
		this.attacker = pAttacker;
		this.defenser = pDefenser;
		this.conflictType = pType;
		this.action = pAction;
	}
}

// the object for real time checking
public class ItemConflict
{
	// itemIds ONLY
	public int attacker, defenser;
	public ItemConflicts.ConflictAction action;
	
	public ItemConflict (int pAttacker, int pDefenser, ItemConflicts.ConflictAction pAction)
	{
		this.attacker = pAttacker;
		this.defenser = pDefenser;
		this.action = pAction;
	}
}



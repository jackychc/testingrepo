using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScenesNode {

	public Scenes values;
	public ScenesNode parent;
	public List<ScenesNode> childNodes;
	
	private ScenesNode(){}
	public ScenesNode(Scenes index)
	{
		this.values 	= index;
		this.childNodes = new List<ScenesNode>();
	}
	
	public void AddChild(ScenesNode child)
	{
		child.parent = this;
		childNodes.Add(child);
	}
	
	public void removeChild(ScenesNode child)
	{
		foreach(ScenesNode node in childNodes)
		{
			if (node.values.Equals(child.values))
			{
				childNodes.Remove(node);
				break;	
			}
		}
	}
	
	public bool hasChild()
	{
		if (childNodes != null && childNodes.Count > 0)
			return true;
		return false;
	}
}


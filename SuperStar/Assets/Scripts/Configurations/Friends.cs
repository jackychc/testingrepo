using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;


public class Friends
{
	public static List<Player> friends = new List<Player>();

	// ====================================================================================================
	
	public static void Save()
	{
		if (!DatabaseManager.SharedManager.IsInitialized)
			return;
		
		//DatabaseManager.db.Open(Globals.dbSavePath);
		foreach (Player fd in friends)
		{
			DatabaseManager.SharedManager.UpdateOrInsert(
				//judge
				"SELECT * FROM Players WHERE gameId = '" + fd.GameId + "'",
				
				//update
				new List<string>(){
				"UPDATE Players SET " +
				" gameId = '" + fd.GameId + "'," +
				" playerName = '" + fd.Name + "'," +
				" boyfriendId = '" + fd.CurrentBoyfriendId + "'," +
				" profilePicURL = '" + fd.ProfilePicURL + "'," +
				" skills = '"+ Skill.GetSkillAsString(fd.getSkill())+"',"+
				" titleSkill = '" + fd.TitleSkill + "'" +
				" WHERE playerId = " + fd.playerId
				,
				
				"UPDATE PlayersToItems SET " +
				" purchasedItems = '" + Items.itemListToString(fd.owningItems) + "'," +
				" wearingItems = '" + Items.itemListToString(fd.wearingItems) + "'" +
				" WHERE playerId = " + fd.playerId
				,
				
				"UPDATE PlayerMeta SET "+
				"  intValue = "+ fd.RougeAlpha +
				" WHERE playerId = " + fd.playerId +
				" AND metaKey = 'rougeAlpha'"
				,
						
				"UPDATE PlayerMeta SET "+
				"  intValue = "+ fd.EyeShadowAlpha +
				" WHERE playerId = " + fd.playerId +
				" AND metaKey = 'eyeShadowAlpha'"
				,

				"UPDATE PlayerMeta SET "+
				"  intValue = "+ fd.PlayerDLCVersion +
				" WHERE playerId = " + fd.playerId +
				" AND metaKey = 'playerDLCVersion'"
				,
						
				"UPDATE PlayersToSocials SET "+
				"  socialId = '" + fd.facebookElement.socialId + "'" + 
				" ,appearName = '"+ fd.facebookElement.appearName + "'" +
				" WHERE playerId = " + fd.playerId + " AND socialType = 'FACEBOOK' "
				,
						
				"UPDATE PlayersToSocials SET "+
				"  socialId = '" + fd.googlePlusElement.socialId + "'" + 
				" ,appearName = '"+ fd.googlePlusElement.appearName + "'" +
				" WHERE playerId = " + fd.playerId + " AND socialType = 'GOOGLEPLUS' "
				,
						
				"UPDATE PlayersToSocials SET "+
				"  socialId = '" + fd.twitterElement.socialId + "'" + 
				" ,appearName = '"+ fd.twitterElement.appearName + "'" +
				" ,loginName = '" + fd.twitterElement.loginName + "'" +
				" ,loginPassword = '" + fd.twitterElement.loginPassword + "'" +
				" WHERE playerId = " + fd.playerId + " AND socialType = 'TWITTER' "
				
				},
			
				//insert
				new List<string>(){
					"INSERT INTO Players (playerId, gameId, playerName, boyfriendId, profilePicURL, skills, titleSkill) VALUES ( " +
			 		  fd.playerId + "," +
				"'" + fd.GameId + "'," +
				"'" + fd.Name + "'," +
					  fd.CurrentBoyfriendId + "," +
				"'" + fd.ProfilePicURL + "'," +
				"'" + Skill.GetSkillAsString(fd.getSkill()) + "'," +
					  fd.TitleSkill +
					")"
					,
					"INSERT INTO PlayersToItems (playerId, purchasedItems, wearingItems) VALUES ( " +
			 		  fd.playerId + "," +
				"'" + Items.itemListToString(fd.owningItems) + "'," +
				"'" + Items.itemListToString(fd.wearingItems) + "')"
					,
					"INSERT INTO PlayerMeta (playerId, intValue, strValue, metaKey) VALUES ( " +
			 		  fd.playerId + "," +
				"'" + fd.RougeAlpha + "'," +
				"'" + "'," +
					  "'rougeAlpha'" +
					")"
					,
					"INSERT INTO PlayerMeta (playerId, intValue, strValue, metaKey) VALUES ( " +
			 		  fd.playerId + "," +
				"'" + fd.EyeShadowAlpha + "'," +
				"'" + "'," +
					  "'eyeShadowAlpha'" +
					")"
					,
					"INSERT INTO PlayerMeta (playerId, intValue, strValue, metaKey) VALUES ( " +
					fd.playerId + "," +
					"'" + fd.PlayerDLCVersion + "'," +
					"'" + "'," +
					"'playerDLCVersion'" +
					")"
					,
					"INSERT INTO PlayersToSocials (playerId, socialType, socialId, appearName) VALUES ( " +
			 		  fd.playerId + "," +
				"'"	+ "FACEBOOK" + "'," +
				"'" + fd.facebookElement.socialId + "'," +
				"'" + fd.facebookElement.appearName + "')"
					,
					"INSERT INTO PlayersToSocials (playerId, socialType, socialId, appearName) VALUES ( " +
			 		  fd.playerId + "," +
				"'"	+ "GOOGLEPLUS" + "'," +
				"'" + fd.googlePlusElement.socialId + "'," +
				"'" + fd.googlePlusElement.appearName + "')"
					,
					"INSERT INTO PlayersToSocials (playerId, socialType, socialId, appearName, loginName, loginPassword) VALUES ( " +
			 		  fd.playerId + "," +
				"'"	+ "TWITTER" + "'," +
				"'" + fd.twitterElement.socialId + "'," +
				"'" + fd.twitterElement.appearName + "'," +
				"'" + fd.twitterElement.loginName + "'," +
				"'" + fd.twitterElement.loginPassword + "')"
				}
			);
			
			/*SQLiteQuery qr;
			string querySelect =
			"SELECT * FROM Players WHERE gameId = '" + fd.GameId + "'" ;

			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			bool hasRecord = qr.Step();
			qr.Release();
			
			if (hasRecord)
			{
				// update
				DatabaseManager.SharedManager.Save("UPDATE Players SET " +
				" gameId = '" + fd.GameId + "'," +
				" playerName = '" + fd.Name + "'," +
				" boyfriendId = '" + fd.CurrentBoyfriendId + "'," +
				" profilePicURL = '" + fd.ProfilePicURL + "'," +
				" skills = '"+ Skill.GetSkillAsString(fd.getSkill())+"',"+
				" titleSkill = '" + fd.TitleSkill + "'" +
				" WHERE playerId = " + fd.playerId);
				
				DatabaseManager.SharedManager.Save("UPDATE PlayersToItems SET " +
				" purchasedItems = '" + Items.itemListToString(fd.owningItems) + "'," +
				" wearingItems = '" + Items.itemListToString(fd.wearingItems) + "'" +
				" WHERE playerId = " + fd.playerId);
				
				DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
				"  intValue = "+ fd.RougeAlpha +
				" WHERE playerId = " + fd.playerId +
				" AND metaKey = 'rougeAlpha'");
			
				DatabaseManager.SharedManager.Save("UPDATE PlayerMeta SET "+
				"  intValue = "+ fd.EyeShadowAlpha +
				" WHERE playerId = " + fd.playerId +
				" AND metaKey = 'eyeShadowAlpha'");
				
				
				DatabaseManager.SharedManager.Save("UPDATE PlayersToSocials SET "+
				"  socialId = '" + fd.facebookElement.socialId + "'" + 
				" ,appearName = '"+ fd.facebookElement.appearName + "'" +
				" WHERE playerId = " + fd.playerId + " AND socialType = 'FACEBOOK' ");
			
				DatabaseManager.SharedManager.Save("UPDATE PlayersToSocials SET "+
				"  socialId = '" + fd.googlePlusElement.socialId + "'" + 
				" ,appearName = '"+ fd.googlePlusElement.appearName + "'" +
				" WHERE playerId = " + fd.playerId + " AND socialType = 'GOOGLEPLUS' ");
			
				DatabaseManager.SharedManager.Save("UPDATE PlayersToSocials SET "+
				"  socialId = '" + fd.twitterElement.socialId + "'" + 
				" ,appearName = '"+ fd.twitterElement.appearName + "'" +
				" ,loginName = '" + fd.twitterElement.loginName + "'" +
				" ,loginPassword = '" + fd.twitterElement.loginPassword + "'" +
				" WHERE playerId = " + fd.playerId + " AND socialType = 'TWITTER' ");
			}
			else
			{
				// insert
				DatabaseManager.SharedManager.Save("INSERT INTO Players (playerId, gameId, playerName, boyfriendId, profilePicURL, skills, titleSkill) VALUES ( " +
			 		  fd.playerId + "," +
				"'" + fd.GameId + "'," +
				"'" + fd.Name + "'," +
					  fd.CurrentBoyfriendId + "," +
				"'" + fd.ProfilePicURL + "'," +
				"'" + Skill.GetSkillAsString(fd.getSkill()) + "'," +
					  fd.TitleSkill +
					")");
			
				DatabaseManager.SharedManager.Save("INSERT INTO PlayersToItems (playerId, purchasedItems, wearingItems) VALUES ( " +
			 		  fd.playerId + "," +
				"'" + Items.itemListToString(fd.owningItems) + "'," +
				"'" + Items.itemListToString(fd.wearingItems) + "')");
				
				DatabaseManager.SharedManager.Save("INSERT INTO PlayerMeta (playerId, intValue, strValue, metaKey) VALUES ( " +
			 		  fd.playerId + "," +
				"'" + fd.RougeAlpha + "'," +
				"'" + "'," +
					  "'rougeAlpha'" +
					")");
				
				DatabaseManager.SharedManager.Save("INSERT INTO PlayerMeta (playerId, intValue, strValue, metaKey) VALUES ( " +
			 		  fd.playerId + "," +
				"'" + fd.EyeShadowAlpha + "'," +
				"'" + "'," +
					  "'eyeShadowAlpha'" +
					")");
				
				DatabaseManager.SharedManager.Save("INSERT INTO PlayersToSocials (playerId, socialType, socialId, appearName) VALUES ( " +
			 		  fd.playerId + "," +
				"'"	+ "FACEBOOK" + "'," +
				"'" + fd.facebookElement.socialId + "'," +
				"'" + fd.facebookElement.appearName + "')");
				
				DatabaseManager.SharedManager.Save("INSERT INTO PlayersToSocials (playerId, socialType, socialId, appearName) VALUES ( " +
			 		  fd.playerId + "," +
				"'"	+ "GOOGLEPLUS" + "'," +
				"'" + fd.googlePlusElement.socialId + "'," +
				"'" + fd.googlePlusElement.appearName + "')");
				
				DatabaseManager.SharedManager.Save("INSERT INTO PlayersToSocials (playerId, socialType, socialId, appearName, loginName, loginPassword) VALUES ( " +
			 		  fd.playerId + "," +
				"'"	+ "TWITTER" + "'," +
				"'" + fd.twitterElement.socialId + "'," +
				"'" + fd.twitterElement.appearName + "'," +
				"'" + fd.twitterElement.loginName + "'," +
				"'" + fd.twitterElement.loginPassword + "')");
				
			}
			*/
			
		
		
		}
		//DatabaseManager.db.Close();
		
	}
	
	public static void Load()
	{
		friends.Clear();
		
		SQLiteQuery qr;
		string querySelect =
			"SELECT * FROM Players WHERE gameId != '" + Globals.mainPlayer.GameId + "'" ;

		qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
		while(qr.Step())
		{
			Log.Debug("loading a fd");
			Player fd = new Player(
				 qr.GetInteger("playerId")
				,qr.GetString("gameId")
				,qr.GetString("playerName")
				,qr.GetInteger("boyfriendId")
				,""
				,""
				,qr.GetString("profilePicURL")
				,new Skill(qr.GetString("skills"))
				,qr.GetInteger("titleSkill")
				);
			
			
			friends.Add(fd);
		}
			
		qr.Release();
		
		//Log.Debug("here, friends count:"+friends.Count);
		// also load items
		
		foreach (Player fd in friends)
		{
			fd.owningItems.Clear();
			fd.wearingItems.Clear();
			querySelect =
				"SELECT * FROM PlayersToItems WHERE playerId = " + fd.playerId ;
	
			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			while(qr.Step())
			{			
				fd.owningItems 	= Items.stringToItemList(qr.GetString("purchasedItems"));
				fd.wearingItems = Items.stringToItemList(qr.GetString("wearingItems"));
			}				
			qr.Release();
			
			
			querySelect =
				"SELECT * FROM PlayerMeta WHERE playerId = " + fd.playerId ;
	
			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			while(qr.Step())
			{			
				
				
				if (qr.GetString("metaKey").Equals("rougeAlpha"))
				{
					fd.RougeAlpha = qr.GetInteger("intValue");
				}
				if (qr.GetString("metaKey").Equals("eyeShadowAlpha"))
				{
					fd.RougeAlpha = qr.GetInteger("intValue");
				}
				else if (qr.GetString("metaKey").Equals("playerDLCVersion"))
				{
					fd.PlayerDLCVersion = qr.GetInteger("intValue");
				}
				
			}
			
			
			
			querySelect =
				"SELECT * FROM PlayersToSocials WHERE playerId = " + fd.playerId ;
	
			qr = new SQLiteQuery(DatabaseManager.SharedManager.db, querySelect);
			while(qr.Step())
			{
				
				
				
				if (qr.GetString("socialType").Equals("FACEBOOK"))
				{
					fd.facebookElement.socialId = qr.GetString("socialId");
					fd.facebookElement.appearName = qr.GetString("appearName");
				}
				if (qr.GetString("socialType").Equals("GOOGLEPLUS"))
				{
					fd.googlePlusElement.socialId = qr.GetString("socialId");
					fd.googlePlusElement.appearName = qr.GetString("appearName");
				}
				if (qr.GetString("socialType").Equals("TWITTER"))
				{
					fd.twitterElement.socialId = qr.GetString("socialId");
					fd.twitterElement.appearName = qr.GetString("appearName");
					fd.twitterElement.loginName = qr.GetString("loginName");
					fd.twitterElement.loginPassword = qr.GetString("loginPassword");
				}
			}
				
			qr.Release();
			
			
		}
	}
	
	// =========================================================================================
	
	/*public static void InsertRow(Hashtable fh)
	{
		Log.Debug("inserting: "+Int32.Parse(fh["playerId"].ToString())+", "+fh["gameId"].ToString());
		
		DatabaseManager.db.Open(Globals.dbSavePath);
		
		SQLiteQuery qr;
		string querySelect =
			"INSERT INTO Players (playerId, gameId) VALUES (?,?);" ;

		qr = new SQLiteQuery(DatabaseManager.db, querySelect);
		qr.Bind(Int32.Parse(fh["playerId"].ToString()));
		qr.Bind(fh["gameId"].ToString());
		qr.Step();
			
		qr.Release();
		
		
		querySelect =
			"INSERT INTO PlayersToItems (playerId) VALUES (?);" ;

		qr = new SQLiteQuery(DatabaseManager.db, querySelect);
		qr.Bind(Int32.Parse(fh["playerId"].ToString()));
		qr.Step();
			
		qr.Release();
		
		DatabaseManager.db.Close();
		
		//DatabaseManager.Save("INSERT INTO PlayersToItems (playerId) VALUES ("+ Int32.Parse(fh["playerId"].ToString())+");");
	}*/
	
	public static Player GetFriend(string gId)
	{
		foreach (Player fd in friends)
		{
			if (fd.GameId.Equals(gId))
			{
				return fd;
			}
		}
		
		return null;
	}
	
	public static void AddFriend(Player fd)
	{
		// remove a duplicated friend
		friends.RemoveAll(dup => dup.playerId.Equals(fd.playerId));
		
		friends.Add(fd);
		
		Save ();
		
	}
	
	public static void AddFriend(Hashtable fh)
	{	
		Player fd = new Player(
						Int32.Parse(fh["playerId"].ToString()),
						fh["gameId"].ToString(),
						fh["playerName"] != null? fh["playerName"].ToString() : "N"+fh["gameId"].ToString(),
						fh["boyfriendId"] != null? Int32.Parse(fh["boyfriendId"].ToString()) : -1 ,
						fh["purchasedItems"] != null? fh["purchasedItems"].ToString() : Globals.DEFAULT_purchasedItems,
						fh["wearingItems"] != null? fh["wearingItems"].ToString() : Globals.DEFAULT_wearingItems,
						" ",
						fh["skills"] != null? new Skill(fh["skills"].ToString()): new Skill(new int[]{1,0,0,0,0}),
						fh["skillTitle"] != null? Int32.Parse(fh["skillTitle"].ToString()) : 0
						);
		
		if (fh.ContainsKey("playerMeta"))
		{
			Hashtable playerMetaHT	= (Hashtable)fh["playerMeta"];
			if (playerMetaHT != null)
			{
				for (int i = 0; i < playerMetaHT.Count; i++)
				{
					Hashtable playerMeta 	= (Hashtable)playerMetaHT["" + i];
					string metaKey			= (string)playerMeta["metaKey"];
					if (metaKey != null && !metaKey.Equals(""))
					{
						if (metaKey.Equals("rougeAlpha"))
						{
							fd.RougeAlpha = (playerMeta["intValue"] != null)? int.Parse((string)playerMeta["intValue"]): Globals.DEFAULT_rougeAlpha;
						}
						else if (metaKey.Equals("eyeShadowAlpha"))
						{
							fd.EyeShadowAlpha = (playerMeta["intValue"] != null)? int.Parse((string)playerMeta["intValue"]): Globals.DEFAULT_eyeShadowAlpha;
						}
						else if (metaKey.Equals("dlcVersion"))
						{
							fd.PlayerDLCVersion = (playerMeta["intValue"] != null)? int.Parse((string)playerMeta["intValue"]): Globals.DEFAULT_dlcVersion;
						}
					}
				}
			}
		}
		
		//Add the social Features
		if (fh.ContainsKey("playerSocial"))
		{
			Hashtable socialFeatureHT = (Hashtable)fh["playerSocial"];
			if (socialFeatureHT != null)
			{
				for (int i = 0; i < socialFeatureHT.Count; i++)
				{
					Hashtable sns = (Hashtable)socialFeatureHT["" + i];
					string socialNetworkId = (string)sns["socialNetworkId"];
					string socialNetworkType = (string)sns["socialNetworkType"];
					
					if (socialNetworkType.Equals("facebook"))
						fd.facebookElement.socialId = socialNetworkId;
					else if (socialNetworkType.Equals("google"))
						fd.googlePlusElement.socialId = socialNetworkId;
					else if (socialNetworkType.Equals("twitter"))
						fd.twitterElement.socialId = socialNetworkId;
				}
			}
		}
		
		
		//fd.facebookElement.socialId = fh["facebookId"] != null? fh["facebookId"].ToString() :"";
		//fd.googlePlusElement.socialId = fh["googlePlusId"] != null? fh["googlePlusId"].ToString() :"";
		//fd.twitterElement.socialId = fh["twitterId"] != null? fh["twitterId"].ToString() :"";
		AddFriend(fd);
	}

	
	
}



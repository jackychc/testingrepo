using UnityEngine;
using System.Collections;

public class CharacterRotation : MonoBehaviour {
	
	Transform target;
	bool isPressing = false;
	int direction = 1;
	public float speed = 1f;
	public bool fadeInSpeed = true;
	public bool fadeOutSpeed = true;
	float mSpeed = 0.0f;
	// Use this for initialization
	void Start () {
		target = GameObject.Find("FEMALE").transform;
		
		InvokeRepeating("Move", 0f, 0.015f);
	}
	
	void Move()
	{
		if (mSpeed <= 0.0f && !isPressing) return;
		
		if (!isPressing)
			mSpeed = Mathf.Max(0.0f, fadeOutSpeed? mSpeed - 0.10f : 0.0f);
		else
			mSpeed = Mathf.Max(0.0f, fadeInSpeed? Mathf.Min(mSpeed + 0.10f, speed) : speed);
		
		
		
		target.RotateAroundLocal(new Vector3(0f,1f,0f), direction * 0.1f * mSpeed);
		
	}
	
	public void RotateClockwise()
	{
		StartPressing();
		direction = 1;
		
	}
	
	public void RotateAnticlockwise()
	{
		StartPressing();
		direction = -1;
	}
	
	public void StartPressing()
	{
		isPressing = true;
	}
	
	public void FinishPressing()
	{
		isPressing = false;
	}
	
}

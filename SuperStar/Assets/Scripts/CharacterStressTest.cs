using UnityEngine;
using System.Collections;

public class CharacterStressTest : MonoBehaviour {
	
	int loop = 1;
	int chars = 0;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void Generate()
	{
		
		int t = UnityEngine.Random.Range(4,5);
		if (t == 1)
		{
		
			GameObject newCharacter = (GameObject) Instantiate((Resources.Load("CharacterAssets/FEMALE") as GameObject));
			
			
			
			newCharacter.name = "FEMALE";
			newCharacter.AddComponent<CharacterManager>();
			newCharacter.GetComponent<CharacterManager>().GenerateCharacter(Globals.mainPlayer);
			newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"+(loop+1)));
			
			//newCharacter.GetComponent<CharacterManager>().ChangeAnimation("Take 001");
		}
		if (t == 2)
		{
		
			GameObject newCharacter = (GameObject) Instantiate((Resources.Load("CharacterAssets/MALE") as GameObject));		
			
			newCharacter.name = "MALE";
			newCharacter.AddComponent<CharacterManager>();
			newCharacter.GetComponent<CharacterManager>().GenerateCharacter(Globals.mainPlayer);
			newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"+(loop+1)));
			
			//newCharacter.GetComponent<CharacterManager>().ChangeAnimation("Take 001");
		}
		if (t == 3)
		{
			GameObject newCharacter = (GameObject) Instantiate((Resources.Load("CharacterAssets/THIRD") as GameObject));
			
			newCharacter.name = "THIRD";
			newCharacter.AddComponent<CharacterManager>();
		}
		
		if (t == 4)
		{
			
		}
		
		if (t == 6)
		{
			
			return;
		}

		
		loop = (loop+1) % 4;
		chars++;
		GameObject.Find("cCount").GetComponent<UILabel>().text = "Characters: "+chars;
	}
}

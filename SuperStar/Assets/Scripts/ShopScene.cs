using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ShopScene : MonoBehaviour {
	
	Shop shop;
	GameObject newCharacter;
	GameObject girlParent;
	bool isMagnifying;
	
	public UITexture background;
	public GameObject itemsNameGroupObj;
	public GameObject leftArrow, rightArrow, scrollPanelGroup;
	public GameObject zoomButton;
	
	public static Player fakePlayer;
	public static int callCount;
	
	
	
	public IndexBlock indexBlock;
	
	IEnumerator Start () {
		shop = VisitShop.shop;

		Hashtable param = new Hashtable();
		param["Shops"] = shop.shopName;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Shop_EnterShop, param);
		
		//background.atlas = (Resources.Load("bgH"+shop.shopId) as GameObject).GetComponent<UIAtlas>();
		//background.spriteName = "bgH"+shop.shopId;
		background.mainTexture = (Texture2D) (Resources.Load("Backgrounds/Shop"+shop.shopId) as Texture2D);
		
		fakePlayer = new Player();
		fakePlayer.playerId = -2;
		fakePlayer.wearingItems = Items.stringToItemList(Items.itemListToString(Globals.mainPlayer.wearingItems));
		
		indexBlock.target = fakePlayer;
		newCharacter = GameObject.Find("FEMALE");
		
		/*if (newCharacter == null)
		{
			newCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			newCharacter.name = "FEMALE";
			newCharacter.AddComponent<CharacterManager>();			
		}
		
		newCharacter.transform.parent = GameObject.Find("GirlParent").transform;*/
		newCharacter = Globals.globalCharacter;
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
		yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(fakePlayer));
		girlParent = GameObject.Find("GirlParent");
		
		
		if (!VisitShop.shop.bgmFilename.Equals(""))
		{
			SoundManager.SharedManager.ChangeMusic(VisitShop.shop.bgmFilename);
		}
		else
		{
			SoundManager.SharedManager.ChangeMusic("SuperstarStory_ElegantStore");
		}
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.SHOP);
		
		StartCoroutine(DelayedStart());
	}
	
	IEnumerator DelayedStart()
	{
		yield return new WaitForSeconds(0.5f);
		
		if (QuestFlags.GetFlag(-105).intValue == 0)
		{
			QuestFlags.SetFlag(-105, 1);
			PopupManager.ShowTutorialPopup(5, false);
		}
	}
	
	void magnify()
	{
		isMagnifying = !isMagnifying;
		
		if (isMagnifying)
		{
			scrollPanelGroup.GetComponent<ShopScrollListManager>().zoomOut();
			girlParent.GetComponent<PositionTweener>().Forward();
			
			leftArrow.GetComponent<PositionTweener>().Forward();
			rightArrow.GetComponent<PositionTweener>().Forward();
			scrollPanelGroup.GetComponent<AnchorTweener>().Forward();
			scrollPanelGroup.GetComponent<ShopScrollListManager>().HideAllPanels();
			
			GameObject.Find("ItemNamePanel").GetComponent<UIPanelAlphaTweener>().Backward();
			
			if (indexBlock != null)
			indexBlock.GetComponent<PositionTweener>().Forward();
			
			//zoomButton.SetActive(false);
			
			//Load itemsName
			StartCoroutine(ShowItemNames());			
			if (itemsNameGroupObj != null)
				itemsNameGroupObj.SetActive(true);
		}
		else
		{
			
			girlParent.GetComponent<PositionTweener>().Backward();
			
			leftArrow.GetComponent<PositionTweener>().Backward();
			rightArrow.GetComponent<PositionTweener>().Backward();
			scrollPanelGroup.GetComponent<AnchorTweener>().Backward();
			scrollPanelGroup.GetComponent<ShopScrollListManager>().LoadZoom();
			scrollPanelGroup.GetComponent<ShopScrollListManager>().LoadRotate();
			scrollPanelGroup.GetComponent<ShopScrollListManager>().LoadPanels();
			
			if (indexBlock != null)
			indexBlock.GetComponent<PositionTweener>().Backward();
			
			//zoomButton.SetActive(true);
			ResetItemNames();
			if (itemsNameGroupObj != null)
				itemsNameGroupObj.SetActive(false);		
		}
	}
	
	private void ResetItemNames()
	{
		for (int i = 0; i < itemsNameGroupObj.transform.childCount; i++)
		{
			GameObject childObj = itemsNameGroupObj.transform.GetChild(i).gameObject;
			
			TweenAlpha ta = childObj.GetComponent<TweenAlpha>();
			if (ta != null)
				Destroy(ta);
			UIPanel childObjPanel = childObj.GetComponent<UIPanel>();
			if (childObjPanel != null)
				childObjPanel.alpha = 0.0f;
		}
	}
	
	private IEnumerator ShowItemNames()
	{
		yield return new WaitForSeconds(0.5f);		
		
		for (int i = 0; i < itemsNameGroupObj.transform.childCount; i++)
		{
			GameObject childObj = itemsNameGroupObj.transform.GetChild(i).gameObject;
			string subcatName = childObj.name;
			
			bool isActiveObj = false;
			ItemSubcategory itemSubcat = ItemSubcategories.getSubcategory((int)Enum.Parse(typeof(ItemSubcategories.Id), subcatName));
			if (itemSubcat != null)
			{
				Item item = ShopScene.fakePlayer.wearingItemOfSubcategory(itemSubcat);
				if (item != null)
				{
					if (!item.itemName.Equals("None"))
					{
						UILabel itemLabel 	= childObj.transform.GetChild(0).GetComponent<UILabel>();
						itemLabel.text 		= item.itemName;
						isActiveObj 		= true;
					}
				}
			}			
			
			if (isActiveObj)
			{
				TweenAlpha ta = childObj.GetComponent<TweenAlpha>();
				if (ta != null)
					Destroy(ta);
				
				ta = childObj.AddComponent<TweenAlpha>();
				ta.from 	= 0.0f;
				ta.to 		= 1.0f;
				ta.delay	= i * 0.05f;
				ta.style 	= UITweener.Style.Once;
				ta.enabled	= true;
			}
			
			childObj.SetActive(isActiveObj);
		}
	}
	
	
	void toggleZoom()
	{
		if (isMagnifying)
		{
			magnify();
			return;
		}
		
		if (scrollPanelGroup.GetComponent<ShopScrollListManager>().isZooming())
			scrollPanelGroup.GetComponent<ShopScrollListManager>().zoomOut();
		else
			scrollPanelGroup.GetComponent<ShopScrollListManager>().zoomIn();
	}
	
	public void GoToStreetScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.STREET, true);	
	}
	
	public void GoToClosetScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.CLOSET, true);		
	}
}

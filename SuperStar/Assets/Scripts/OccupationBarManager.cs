using UnityEngine;
using System.Collections;

public class OccupationBarManager : MonoBehaviour {
	
	public GameObject title;
	public UISprite[] occupationLogos = new UISprite[5];
	public UISprite[] buttons = new UISprite[5];
	public UIFilledSprite[] progressBars = new UIFilledSprite[5];
	UISprite currentSelected;
	public JobScrollListManager jobScrollListManager;
	
	private bool canUpdateBars = true;
	
	
	void Start()
	{	
		currentSelected = occupationLogos[0];
		changeTitle1();
		
		StartCoroutine(FirstUpdate());
	}
	
	public void changeTitle(string t)
	{
		title.GetComponent<TextMesh>().text = t;
	}
	
	public void changeTitle1()
	{
		//changeTitle("Singer");
		buttonMatters(1);
		
	}
	public void changeTitle2()
	{
		//changeTitle("Dancer");
		buttonMatters(2);
	}
	public void changeTitle3()
	{
		//changeTitle("Tennis Player");
		buttonMatters(3);
	}
	public void changeTitle4()
	{
		//changeTitle("Actress");
		buttonMatters(4);
	}
	public void changeTitle5()
	{
		//changeTitle("Model");
		buttonMatters(5);
	}
	
	void buttonMatters(int i)
	{
		currentSelected.spriteName = currentSelected.spriteName.Trim('1') + "2";
		
		currentSelected = occupationLogos[i-1];
		currentSelected.spriteName = currentSelected.spriteName.Trim('2') + "1";
		
		UpdateWithoutLoop();
	}
	
	/*void Update()
	{
		for (int i = 0; i < progressBars.Length; i++)
		{
			progressBars[i].fillAmount = Jobs.progressOfSkill(i + "");
		}
		jobScrollListManager.updateEverySecond();
	}*/
	
	
	
	void UpdateWithoutLoop()
	{
		
		for (int i = 0; i < progressBars.Length; i++)
		{
			progressBars[i].fillAmount = Jobs.progressOfSkill(i + "");
			//Log.Debug ("UP!" + i + "," + progressBars[i].fillAmount);
		}
		jobScrollListManager.updateEverySecond();
	}
	
	IEnumerator FirstUpdate()
	{
		yield return new WaitForSeconds(0.002f);
		UpdateWithoutLoop();
		StartCoroutine(UpdateBars());
	}
	IEnumerator UpdateBars()
	{
		while(canUpdateBars)
		{
			UpdateWithoutLoop();
		
			yield return new WaitForSeconds(1.0f);
		}
		//StartCoroutine(UpdateBars());
	}
	
	public void StopUpdateBars()
	{
		canUpdateBars = false;
		StopCoroutine("UpdateBars");
	}
}

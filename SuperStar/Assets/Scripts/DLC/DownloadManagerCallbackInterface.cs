using UnityEngine;
using System.Collections;

public interface DownloadManagerCallbackInterface{
	void OnDownloadManagerDLCCompleted();
}

using UnityEngine;
using System.Collections;
using System.IO;
using System;

public delegate void DownloadCompleteListener();

public class AssetBundleDownloader : MonoBehaviour {
	
	private string bundleURL;
	private string bundleStoragePath;
	
	private const int maxRetry = 3;
	private int numOfRetry = 0;
	
	private WWW www;
	private string origMD5Hash;
	private bool isFinished;
	private bool isStarted;
	private bool hasError;
	
	//Time control download bundles
	private int timeoutInterval;
	private float currentTime;
	
	//Callback to indicate complete
	private DownloadCompleteListener listener;	
	
	public STATE state = STATE.DOWNLOADING;
		
	public enum STATE 
	{
		DOWNLOADING,
		SUCCESS,
		FAILED
	}
	
	void Start()
	{
		isFinished = false;
		isStarted = false;
		hasError = false;
			
		//Set up Storage Path
		bundleStoragePath = Application.persistentDataPath + Path.DirectorySeparatorChar;
		
		//Initialize Download
		//StartDownload("57f52376d83a5ebf2ecd8b05569cf9cf", "http://localhost/SuperStarStory/Radar.dlc");
		
		
		//Record current Time		
		//currentTime = System.DateTime.Now;
		//Inialize	
	}
	
	void Update()
	{
		if (!isFinished && isStarted && !www.isDone)
		{
			//Log.Debug(www.progress);	
		}
	}
	
	public void RegisterCompleteListener(DownloadCompleteListener listener)
	{
		this.listener = listener;	
	}
	
	public void StartDownload(string MD5Hash, string bundleURL)
	{
		this.bundleURL = bundleURL;
		this.origMD5Hash = MD5Hash;
		state = STATE.DOWNLOADING;

		StartCoroutine(DownloadBundle());
		
		//success(returnMe);
	}
	
	public IEnumerator WaitForResult(Action<bool> isSuccess)
	{
		while (state.Equals(STATE.DOWNLOADING))
		{
			//Log.Debug("loopong");
			yield return null;
		}
		
		isSuccess(state.Equals(STATE.SUCCESS));
	}
	
	IEnumerator DownloadBundle()
	{		
		//Start the download
		isStarted = true;
		
		www = new WWW(bundleURL);
		yield return www;
		
		isFinished = true;
		if (www.error != null)
		{
			Log.Error("There is some error occurs when downloading the asset bundle file");
			retry();
			yield break;
		}
		
		Log.Debug("Now Download Bundle From Path: " + bundleURL);
		
		//Extract the asset bundle
		AssetBundle bundle = www.assetBundle;
		UnityEngine.Object o = bundle.mainAsset;
		
		string md5Hash = MD5Hash.get(www.bytes);
		Log.Debug("Download md5 hash code: " + md5Hash);
		
		//Check the bundle corrupted
		if (md5Hash.Equals(origMD5Hash))
		{
			Log.Debug("Data Path:" + bundleStoragePath + o.name + ".dlc");
			File.WriteAllBytes(bundleStoragePath + o.name + ".dlc", www.bytes);
			
			Log.Debug("Download Complete");
			
			state = STATE.SUCCESS;
						
			if (listener != null)
			{
				Log.Debug("Listener is not null");
				
				listener();	
			}
		}
		else
		{
			Log.Debug("The downloaded bundle MD5 Hash is not equal!");
			Log.Debug("Check the bundle corrupted or not!");
			//unload the bundle
			bundle.Unload(false);
			retry();
			yield break;
		}
		
		bundle.Unload(false);
		www.Dispose();	
		hasError = false;
	}
	
	private void retry()
	{
		if (numOfRetry < maxRetry)
		{
			hasError = true;
			numOfRetry++;
			Log.Debug("Retry times: "+ numOfRetry +" download now!");
			www.Dispose();
			
			//Reset the www state
			isStarted = false;
			isFinished = false;
			StartCoroutine(DownloadBundle());
		}
		else
		{
			
			Log.Debug("Seems there are some network issues. Retries 5 times still failed download");
			state = STATE.FAILED;
			if (listener != null)
			{
				Log.Debug("Listener is not null");
				
				listener();	
			}
		}
	}
}

using UnityEngine;
using System.Collections;
using System.IO;

public class FileDownloader : MonoBehaviour {
	public enum State 
	{
		DOWNLOADING,
		SUCCESS,
		FAILED,
		CORRUPTED,
		NORMAL
	}

	public State state 	= State.NORMAL;
	private int numOfRetry 	= 0;
	private int maxRetry	= 3;
	
	WWW www = null;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public IEnumerator StartDownload(string url, string hashValue, string savePath)
	{
		Log.Debug("FileDownloader Start Downloading the file: " + url);
		www 				= null;
		string fileName 		= Path.GetFileName(url);
		numOfRetry				= 0;

		while (numOfRetry <= maxRetry)
		{
			state = State.DOWNLOADING;
			www = new WWW(url);
			yield return www;

			if (www.error != null)
			{
				state = State.FAILED;
				Log.Warning("[FileDownloader][StartDownload] Error! Cannot download the file: " + fileName);
				numOfRetry++;
			}
			else
			{
				if (www.bytes != null)
				{
					string dlHashValue = MD5Hash.get(www.bytes);
					if (dlHashValue.Equals(hashValue))
					{
						state = State.SUCCESS;
						File.WriteAllBytes(savePath, www.bytes);
						Log.Debug("FileDownloader Download Completed!! The file is " + fileName);
						numOfRetry = maxRetry + 1;
					}
					else
					{
						state = State.CORRUPTED;
						Log.Warning("[FileDownloader][StartDownload] Error! The hash value is not equal!! " + fileName);
						numOfRetry++;
					}
				}
				else
				{
					state = State.FAILED;
					Log.Warning("[FileDownloader][StartDownload] Error! The file is empty: " + fileName);
					numOfRetry++;
				}
			}

			www.Dispose();
			www = null;
		}
	}
	
	public float GetWWWProgress()
	{
		if (www != null)
			return www.progress;
		
		return 0f;
	}


}

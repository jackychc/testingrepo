using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class DownloadManager : MonoBehaviour, DLCAskPopupCallbackInterface, DLCBetweenPopupCallbackInterface, DLCCompletePopupCallbackInterface
{
	public FileDownloader fileDownloader;


	private bool dlcIsBoundedByLevel = false;
	private int dlcToDlcId			= -1;
	private int dlcMaxSize			= 0;



	private ArrayList callbackList = new ArrayList();
	public bool haveDLC;
	public bool isDownloading;

	public float apparentProgress = 0f;
	public float progress = 0f;
	public float pinchProgress = 0f;
	
	private static DownloadManager instance = null;
	public static DownloadManager SharedManager
	{
		get
		{
			return instance;	
		}
	}
	
	void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
			Destroy(this);
	}

	// Use this for initialization
	void Start () 
	{
		//StartCoroutine(ReallyStart());
	}
	
	public void SetCallback(DownloadManagerCallbackInterface callback)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(callback))
			{
				callbackList.Remove(callback);
			}
		}
		
		callbackList.Add(callback);
	}
	
	public void RemoveCallback(DownloadManagerCallbackInterface callback)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(callback))
				callbackList.Remove(callback);
		}
	}
	
	
	public int getLatestLocalDLCId()
	{
		//return Globals.currentDLCVersion;
		return Globals.apparentDLCVersion;
		
	}
	
	public IEnumerator CheckDLC(bool isBoundedByLevel, int toDlcId, int maxSize, Action<bool> have)
	{		
		Log.Debug("DLM Check dLC!!");
		
		haveDLC = false;
		
		int localDlcId = getLatestLocalDLCId();
		Log.Debug("DLC currentDLCVersion: " + localDlcId);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("dlcId", localDlcId.ToString());
		kvp.Add("maxSize", maxSize.ToString());

		if (Globals.isDebugMode)
			kvp.Add("isDebugMode", "1");

		if (isBoundedByLevel)
			kvp.Add("dlcLevel", Globals.mainPlayer.Level.ToString());

		#if UNITY_IPHONE
				kvp.Add("platform", "ios");
		#elif UNITY_ANDROID
				kvp.Add("platform", "android");
		#else
				kvp.Add("platform", "ios");
		#endif
		
		string url = Globals.serverAPIURL + "GetDLCs.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{					
					if (al[1] != null)
					{
						haveDLC = true;
						have(true);
					}
					else
					{
						// no dlc
						have(false);
					}
				}
				else
				{
					// no dlc
					have(false);
				}
			}
			else
				have(false);
		}
		
		//yield return StartCoroutine(HTTPPost.Post("GetDLCs", kvp, result => response = result));			
	}
	
	public void StartDLC(bool isBondedByLevel, int toDlcId, int maxSize)
	{
		InvokeRepeating("Move", 0f, 0.03f);
		Log.Debug("start DLC!!");
		StartCoroutine(EnumStartDLC(isBondedByLevel, toDlcId, maxSize));
	}

	public IEnumerator EnumStartDLC(bool isBoundedByLevel, int toDlcId, int maxSize)
	{
		// "resume" download
		if (Globals.dlcProgress >= 100)
		{
			Globals.dlcProgress = 0;
		}

		progress = Globals.dlcProgress/100f;
		apparentProgress = progress;
		
		isDownloading 		= true;
		bool emergencyStop 	= false;
		int localDlcId 		= getLatestLocalDLCId();
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("dlcId", localDlcId.ToString());
		kvp.Add("maxSize", maxSize.ToString());

		if (Globals.isDebugMode)
			kvp.Add("isDebugMode", "1");

		if (isBoundedByLevel)
			kvp.Add("dlcLevel", Globals.mainPlayer.Level.ToString());

		if (toDlcId != -1)
			kvp.Add("toDlcId", toDlcId.ToString());

#if UNITY_IPHONE
		kvp.Add("platform", "ios");
#elif UNITY_ANDROID
		kvp.Add("platform", "android");
#endif
		string url = Globals.serverAPIURL + "GetDLCs.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (response != null)
		{
			ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
			if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
			{				
				if (al[1] != null)
				{
					MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.DLC_Download);

					List<DLCConfig> setMe = new List<DLCConfig>();					
					
					ArrayList dlcList =  new ArrayList(((Hashtable) al[1]).Values);				
					foreach (Hashtable dlcHT in dlcList)
					{
						// this is an good example to get nested data. hahaha.....
						int dlcId			= Int32.Parse(dlcHT["dlcId"].ToString());
						string dlcVersion	= dlcHT["dlcVersion"].ToString();
						int isAppUpdate		= Int32.Parse(dlcHT["isAppUpdate"].ToString());
						int dlcLevel	 	= Int32.Parse(dlcHT["dlcLevel"].ToString());

						if (isBoundedByLevel && Globals.mainPlayer.Level < dlcLevel)
							continue;
						
						Debug.LogWarning("getting dlc content:"+SuperstarFashionMiniJSON.JsonEncode(dlcHT));
						
						DLCConfig dlcc = new DLCConfig
						(
							dlcId,
							dlcVersion,
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Items"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Rewards"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Jobs"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["ItemConflicts"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Shops"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Boyfriends"]),
							(Hashtable) (((Hashtable)dlcHT["dlcItems"])["Packages"])
						);						
						dlcc.isAppUpdate 	= (isAppUpdate == 1);
						dlcc.dlcLevel 		= dlcLevel;
						
						string successTest = "";
						
						do
						{
							successTest =  SuperstarFashionMiniJSON.JsonEncode(dlcHT);
						} while (successTest == null);
						
						
						dlcc.dlcContent = successTest;
						setMe.Add(dlcc);
					}
					Log.Debug("Total num of DLC : " + dlcList.Count);
					
					setMe.Sort((x,y) => {return x.dlcId.CompareTo(y.dlcId);});
					
					// "resume" download
					float fraction = ((1f - progress)/setMe.Count);
					Log.Debug("DLC Fraction: " + fraction);
					
					// we need to update DLC by sequence
					foreach (DLCConfig dlcc in setMe)
					{						
						if (dlcc.isAppUpdate)
						{
							string fileName = "dlc_queryfile_" + dlcc.dlcId;

							// load file
							TextAsset theFile = Resources.Load("DLC Query/"+fileName) as TextAsset;

							if (theFile == null)
							{
								Popup p = PopupManager.GetLatestPopup();
								if (p != null && p.gameObject.name.Equals("DLCBetweenPopup"))
								{
									p.Hide();
								}

								PopupManager.ShowNewAppVersionPopup(null, false);

								//LogErrorFlurryEvent(dlcc.dlcId);
								isDownloading = false;

								yield break;		
							}
							else
							{
								DLCConfigs.allDlcConfigs.Add(dlcc);
								DLCConfigs.Save();
								Globals.apparentDLCVersion = dlcc.dlcId;

								Log.Debug("DLC The DLC is valid and saved to DB: " + dlcc.dlcId);

								GainProgress(fraction);
							}
						}
						else	
						{
							yield return StartCoroutine(dlcc.ConvertToList());		


							if (dlcc.packages.Count > 0)
							{
								foreach (DLCPackage p in dlcc.packages)
								{
									WWW www = null;
									
									if ((p.hashValue != null && !p.hashValue.Equals("")) &&
									    (p.url != null && !p.url.Equals("")))
									{
										//Download the zip files from server
										string fileName		= Path.GetFileName(p.url);
										string filePath 	= Application.persistentDataPath + Path.DirectorySeparatorChar + fileName;
										FileInfo fileInfo 	= new FileInfo(filePath);
										if (fileInfo != null && fileInfo.Exists)
										{
											www = new WWW("file://" + filePath);
											www.threadPriority = ThreadPriority.Low;
											yield return www;

											if (www.error != null)
											{
												Log.Debug("DLC Cannot download the file: " + filePath);
												if (!Globals.isDebugMode)
												{
													emergencyStop = true;
													//break;
													StartCoroutine(FailRoutine(dlcc.dlcId));
													www.Dispose();
													www = null;
													yield break;
												}
											}
											else
											{
												if (www.bytes != null)
												{
													string hashValue = MD5Hash.get(www.bytes);

													if (hashValue.Equals(p.hashValue))
													{
														Log.Debug("DLC The zip file equal " + fileName +  " and will be skipped" );
														//GainProgress((fraction/2.5f)/dlcc.items.Count);
														www.Dispose();
														www = null;

														GainProgress(fraction * 0.8f / dlcc.packages.Count);
														continue;
													}
												}
												else
												{
													Log.Debug("DLC Cannot read the zip file: " + fileName);
													if (!Globals.isDebugMode)
													{
														emergencyStop = true;
														//break;
														StartCoroutine(FailRoutine(dlcc.dlcId));
														www.Dispose();
														www = null;
														yield break;
													}
												}
											}

											www.Dispose();
											www = null;
										}
										
										PinchProgress();
										StartCoroutine(fileDownloader.StartDownload(p.url, p.hashValue, filePath));
										do 
										{
											yield return new WaitForSeconds(0.5f);
											
											GainProgressFromPinch((fraction * 0.8f / dlcc.packages.Count) * fileDownloader.GetWWWProgress());
											
										} while(fileDownloader.state.Equals(FileDownloader.State.NORMAL) || fileDownloader.state.Equals(FileDownloader.State.DOWNLOADING));										
										
										
										FileDownloader.State downloadState = fileDownloader.state;
										if (downloadState.Equals(FileDownloader.State.FAILED) || downloadState.Equals(FileDownloader.State.CORRUPTED))
										{
											Log.Debug("DLC The Download State is failed or corrupted!!");
											emergencyStop = true;

											StartCoroutine(FailRoutine(dlcc.dlcId));
											yield break;
										}
										
										// ensure the percentage is correct even if www.progress not work
										GainProgressFromPinch(fraction * 0.8f / dlcc.packages.Count);
									}
									else
									{
										GainProgress(fraction * 0.8f / dlcc.packages.Count);
									}
								}
							}
							else
							{
								Log.Debug("DLC The DlC Packages count is 0");
							}



							if (!emergencyStop)
							{
								if (!dlcc.CheckValidity())
								{
									Log.Debug("the DLC format is not valid!!");
									emergencyStop = true;
									//break;
									StartCoroutine(FailRoutine(dlcc.dlcId));
									yield break;
								}

								//string commitText = Language.Get("dlc_committing");
								//PopupManager.ShowLoadingFlower(commitText);
								
								//yield return new WaitForSeconds(0.5f);
								
								//dlcc.Commit();
								
								//testing code: save to db, commit when restart
								DLCConfigs.allDlcConfigs.Add(dlcc);
								DLCConfigs.Save();
								Globals.apparentDLCVersion = dlcc.dlcId;

								Log.Debug("DLC The DLC is valid and saved to DB: " + dlcc.dlcId);

								//yield return new WaitForSeconds(0.5f);
								//PopupManager.HideLoadingFlower();
								// metadata: 20%
								GainProgress(fraction/5f);
							}
							else
							{								
								//break;
								StartCoroutine(FailRoutine(dlcc.dlcId));
								yield break;
							}
						}					
						
					} // end of for loop
					
					
					/*if (emergencyStop)
					{
						ResetProgress();
						//show popup?
						
						//PopupManager.HideAllPopup();
						Popup p = PopupManager.GetLatestPopup();
						if (p != null && p.gameObject.name.Equals("DLCBetweenPopup"))
						{
							p.Hide();
						}
						PopupManager.ShowInformationPopup(Language.Get("dlc_downloadFail"), true);
						
						StartCoroutine(FailRoutine(dlcc.dlcId));
						yield break;
					}*/
				}
				
			}
		}
		
		isDownloading = false;
	}
	
	private IEnumerator FailRoutine(int id)
	{
		LogErrorFlurryEvent(id);
		isDownloading = false;
		ResetProgress();
						//show popup?
		
		// still commit previous success dlcs immediately
		//DLCConfigs.ExecuteDLC();
						
		//PopupManager.HideAllPopup();
		Popup p = PopupManager.GetLatestPopup();
		if (p != null && p.gameObject.name.Equals("DLCBetweenPopup"))
		{
			p.Hide();
		}
		PopupManager.ShowInformationPopup(Language.Get("dlc_downloadFail"), true);
		
		yield return null;
	}
	
	private void LogErrorFlurryEvent(int updateToDlcId)
	{
		//LogFlurry Event
		Hashtable param = new Hashtable();
		param["Failed"] = "New_" + updateToDlcId;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.DLC_State, param);
	}
	
	void Move()
	{
		if (apparentProgress < progress)
		{
			apparentProgress += 0.005f;
			
			if( apparentProgress >= 1.0f)
			{
				apparentProgress = 1f;
				if (callbackList != null)
				{
					foreach(DownloadManagerCallbackInterface c in callbackList)
					{
						c.OnDownloadManagerDLCCompleted();
					}
				}
				Globals.IsDLCCompleted = true;

				PopupManager.ShowDLCCompletePopup(this, true);
				
				/*GameObject t = GameObject.Find("StreetMenu");
				if (t != null)
				{
					t.GetComponent<StreetMenuManager>().dlcButton.SetActive(false);
				}*/
				
				CancelInvoke("Move");
				//NotificationManager.AddNotificationOfType(NotificationManager.TYPE.DLC_FINISHED);
				// TODO : change to notification
				//PopupManager.ShowInformationPopup("DLC finished! now version is: "+Globals.currentDLCVersion);
			}
		}
		
		
	}
	
	public float GetApparentProgress()
	{
		return apparentProgress;
	}
	
	public float GetProgress()
	{
		return progress;
	}
	
	public void ResetProgress()
	{
		apparentProgress = 0f;
		progress = 0f;
	}
	
	public void PinchProgress()
	{
		pinchProgress = progress;
	}
	
	public void GainProgressFromPinch(float p)
	{
		progress = pinchProgress + p;
	}
	
	public void GainProgress(float p)
	{
		progress += p;
		
		Globals.dlcProgress = (int) (progress * 100);
	}
	
	public string getSavePath(string fileName)
	{
		return Application.persistentDataPath + Path.DirectorySeparatorChar + (fileName.Replace(" ","")) +".png";
	}

	#region Popup
	public void AskForDLCUpgrade(bool isBoundedByLevel, int toDlcId, int maxSize, int type, bool canCancelQuit)
	{
		dlcIsBoundedByLevel 	= isBoundedByLevel;
		dlcToDlcId				= toDlcId;
		dlcMaxSize				= maxSize;
		PopupManager.ShowDLCAskPopup(this, canCancelQuit, type, false);
	}

	public void AskForDLCUpgrade(bool isBoundedByLevel, int toDlcId, int maxSize)
	{
		AskForDLCUpgrade(isBoundedByLevel, toDlcId, maxSize, 2, true);
	}
	
	public void AskForDLCProgress(bool isBoundedByLevel, int toDlcId, int maxSize)
	{
		dlcIsBoundedByLevel	= isBoundedByLevel;
		dlcToDlcId			= toDlcId;
		dlcMaxSize			= maxSize;

		PopupManager.ShowDLCBetweenPopup(this, isBoundedByLevel, toDlcId, maxSize, false);
	}
	#endregion
	
	#region DLCAskPopup Callback
	public void OnDLCAskPopupYes()
	{
		PopupManager.ShowDLCBetweenPopup(this, dlcIsBoundedByLevel, dlcToDlcId, dlcMaxSize, false);
	}
	
	public void OnDLCAskPopupNo()
	{
		Scenes currScene = SceneManager.SharedManager.CurrScene;
		if (currScene.Equals(Scenes.FRIENDSHOME))
			SceneManager.SharedManager.LoadScene(Scenes.STREET, true);
		else if (currScene.Equals(Scenes.BOYFRIEND))
			SceneManager.SharedManager.LoadScene(Scenes.STREET, true);
	}
	#endregion
	
	#region DLCBetweenPopup Callback
	public void OnDLCBetweenPopupClicked()
	{
		Scenes currScene = SceneManager.SharedManager.CurrScene;
		if (currScene.Equals(Scenes.FRIENDSHOME))
			SceneManager.SharedManager.LoadScene(Scenes.STREET, true);
		else if (currScene.Equals(Scenes.BOYFRIEND))
			SceneManager.SharedManager.LoadScene(Scenes.STREET, true);
	}
	#endregion
	
	#region DLCCompletePopup Callback
	public void OnDLCCompletePopupOKClicked()
	{
		Time.timeScale = 0f;
		Application.Quit();
		//SceneManager.SharedManager.LoadScene(Scenes.TITLE, true);
	}
	
	public void OnDLCCompletePopupCancelClicked()
	{
		Scenes currScene = SceneManager.SharedManager.CurrScene;
		if (currScene.Equals(Scenes.BOYFRIEND))
		{
			SceneManager.SharedManager.LoadScene(Scenes.STREET, true);
		}
	}
	#endregion
}

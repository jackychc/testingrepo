using UnityEngine;
using System.Collections;

public class ParallexBackground : MonoBehaviour {
	
	public UIDraggablePanel mainPanel;
	public UIDraggablePanel[] subPanels;
	
	// Use this for initialization
	void Start () {
		
		// ADD SCROLL BARS AT RUNTIME WILL NOT WORK!!!
	}
	
	// Update is called once per frame
	void Update () {
		
		if (mainPanel == null) return;
		
		// test on looping
		
		
		foreach (UIDraggablePanel subPanel in subPanels)
		{
			subPanel.horizontalScrollBar.scrollValue = mainPanel.horizontalScrollBar.scrollValue;
		}
	}
}

using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class DealItem : MonoBehaviour {

	public Item item;
	public int sortOrder;
	public UISprite currencyCoin, currencyGem;
	public UILabel amount;
	public UILabel reallyAmount;
	public UISysFontLabel expireLabel;
	public GameObject buyButton;
	public GameObject priceBlock;
	public GameObject block;
	public GameObject background;
	public GameObject lockSprite;
	public GameObject levelLabel;
	public GameObject newBlock;
	public GameObject discountCross;
	public GameObject icon;
	public GameObject wearButton;
	
	[HideInInspector] public int discount;
	[HideInInspector] public int offerCoins;
	[HideInInspector] public int offerGems;
	public Feature feature 	= null;
	public Poster poster	= null;

	public void Start()
	{
		StartCoroutine(ReallyStart());
	}
	
	public IEnumerator ReallyStart()
	{
		//Log.Debug("item name:"+item.itemName);
		UITexture iconTexture = icon.GetComponent<UITexture>();
		Texture2D tex = Items.GetIconOfItem(item);
		if(tex == null)
		{
			yield return StartCoroutine(LoadIconFromPersistent(item, result => tex = result));
		}
		
		string shaderName 	= "UnlitTransparentColored";
		Shader shader 		= Resources.Load("Shaders/" + shaderName) as Shader;
		if (shader != null)
			iconTexture.material = new Material(shader);
		else
			iconTexture.material = new Material(Shader.Find ("Unlit/Transparent Colored"));
        iconTexture.material.mainTexture = tex;
        icon.transform.localScale = new Vector3(128f,128f,1f);
		
		SetBought();
		SetCurrencyAndAmount();
		SetLevelLock();
		
		Destroy(transform.GetComponent<UIPanel>());
		
		InvokeRepeating("UpdateExpireTime", 0f, 1f);
	}
	
	
	public IEnumerator LoadIconFromPersistent(Item i, Action<Texture2D> returnMe)
	{
		WWW loader = new WWW("file://" + Application.persistentDataPath + Path.DirectorySeparatorChar + i.itemId + "_icon.png");
		yield return loader;
		
		returnMe((Texture2D)loader.texture);
	}
	
	public void SetBought()
	{
		if (Globals.mainPlayer.owningItems.Contains(item))
		{
			wearButton.SetActive(true);
			buyButton.SetActive(false);
			expireLabel.gameObject.SetActive(true);
			block.SetActive(true);
		}
	}
	
	public void SetCurrencyAndAmount()
	{
		if (item.requiredLevel < Globals.mainPlayer.Level)
		{
			if (item.gems > 0)
			{
				currencyGem.gameObject.SetActive(true);
				currencyCoin.gameObject.SetActive(false);
				
				
				if (offerGems < item.gems)
				{
					amount.text = /*offerGems + ""; //*/item.gems + "";
					discountCross.SetActive(true);
					reallyAmount.text = ""+offerGems;
					
				}
				else
				{
					amount.text = "";
					discountCross.SetActive(false);
					reallyAmount.text = ""+ offerGems;
				}
			}
			else
			{
				currencyGem.gameObject.SetActive(false);
				currencyCoin.gameObject.SetActive(true);
				
				if (offerCoins < item.price)
				{
					amount.text = /*offerCoins + "";//*/item.price + "";
					discountCross.SetActive(true);
					reallyAmount.text = ""+offerCoins;
					
				}
				else
				{
					amount.text = "";
					discountCross.SetActive(false);
					reallyAmount.text = ""+offerCoins;
				}
			}
		}
	}
	
	public void SetLevelLock()
	{
		if (item.requiredLevel > Globals.mainPlayer.Level)
		{
			block.SetActive(true);
			lockSprite.SetActive(true);
			levelLabel.GetComponent<UILabel>().text = "Lv"+item.requiredLevel;
			levelLabel.SetActive(true);
			expireLabel.gameObject.SetActive(true);
			
			currencyGem.gameObject.SetActive(false);
			currencyCoin.gameObject.SetActive(false);
			amount.text = "";
			reallyAmount.text = "";
		}
	}
	
	
	
	public void ClickItem()
	{
		StartCoroutine(ClickItemRoutine());
	}
	
	public IEnumerator ClickItemRoutine()
	{
		yield return null;
		
		// restore all item's "buy" status
		foreach (ShopItem si in transform.parent.GetComponentsInChildren<ShopItem>())
		{
			si.buyButton.SetActive(false);
			si.background.GetComponent<UISprite>().spriteName = "items_bg";
		}
		background.GetComponent<UISprite>().spriteName = "item_choose";
		
		
		
		
		if (item.requiredLevel > Globals.mainPlayer.Level)
		{
			// not enoguh level popup?
			buyButton.SetActive(false);
		}
		else
			buyButton.SetActive(true);
				
		
		SpringPanel.Begin(GameObject.Find("ScrollPanelT"), new Vector3(0f,-transform.localPosition.y ,0f), 5f);
		
		
		
		
	}
	
	public void WearClicked()
	{
		Globals.mainPlayer.wearItem(item);
		
		GameObject girl = GameObject.Find("FEMALE");
		
		if (girl != null)
			girl.GetComponent<CharacterManager>().GenerateCharacter(Globals.mainPlayer);
	}
	
	public void BuyItem()
	{
		if (item.requiredLevel > Globals.mainPlayer.Level)
			return;
		
		// check if enough $$
		if (!checkEnoughCurrency())
		{
			if (offerGems > 0)
			{
				PopupManager.ShowNotEnoughCurrencyPopup("Gems", offerGems - Globals.mainPlayer.Gems, false);				
			}
			else
			{
				PopupManager.ShowNotEnoughCurrencyPopup("Coins", offerCoins - Globals.mainPlayer.Coins, false);
					
			}			
			return;
		}
		
		
		
		Globals.mainPlayer.owningItems.Add(item);
		
		buyButton.SetActive(false);
		wearButton.SetActive(true);
		//priceBlock.SetActive(false);
		newBlock.SetActive(false);
		block.SetActive(true);
		//expireLabel.gameObject.SetActive(true);
		background.GetComponent<UISprite>().spriteName = "items_bg";
		
		if (item.gems > 0)
		{
			Globals.mainPlayer.GainGems(-offerGems);
		}
		else
		{
			Globals.mainPlayer.GainCoins(-offerCoins);
		}
		
		
		PopupManager.ShowItemBroughtPopup(true);
		
		//PPA
		MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.BuyAnItemAnyShop);
		
		//Flurry Event
		Hashtable param = new Hashtable();
		param["itemName"] 	= item.itemName;
		param["price"] 		= item.price;
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Purchased_Item, param);
	}
	
	public bool checkEnoughCurrency()
	{
		if (offerGems > 0)
		{
			if (Globals.mainPlayer.Gems < offerGems)
				return false;
		}
		else
		{
			if (Globals.mainPlayer.Coins < offerCoins)
				return false;
		}
		
		return true;
	}
	
	public void UpdateExpireTime()
	{
		if (feature != null && poster == null)
		{
			TimeSpan remainingTime = feature.CalculateRemainSeconds();
			int sec = (int)remainingTime.TotalSeconds;
			
			if (sec <= 0)
			{
				expireLabel.Text = "";
			}
			else
			{
				string dateString = string.Format("{0:00}:{1:00}:{2:00}", remainingTime.Hours, remainingTime.Minutes, remainingTime.Seconds);
				if (remainingTime.Days > 0)
				{
					dateString = remainingTime.Days + " " + Language.Get("deal_days") + " " + dateString;
				}				
				expireLabel.Text = Language.Get("msg_dealexpires") + " " + dateString;
			}
		}
		else if (poster != null && feature == null)
		{
			Log.Debug("Feature is null");
			string[] dates = poster.period.Split('|');

			if (dates.Length == 2)
			{
				DateTime now 		= DateTime.UtcNow;
				DateTime startTime 	= DateTime.Parse(dates[0]);
				DateTime endTime	= DateTime.Parse(dates[1]);

				startTime	= new DateTime(startTime.Year, startTime.Month, startTime.Day, 23, 59, 59);
				endTime		= new DateTime(endTime.Year, endTime.Month, endTime.Day, 23, 59, 59);
				if ((now - startTime).TotalSeconds > 0 && (endTime - now).TotalSeconds >= 0)
				{
					TimeSpan remainingTime = endTime - now;
					int sec = (int)remainingTime.TotalSeconds;
					if (sec <= 0)
					{
						expireLabel.Text = "";
					}
					else
					{
						string dateString = string.Format("{0:00}:{1:00}:{2:00}", remainingTime.Hours, remainingTime.Minutes, remainingTime.Seconds);
						if (remainingTime.Days > 0)
						{
							dateString = remainingTime.Days + " " + Language.Get("deal_days") + " " + dateString;
						}				
						expireLabel.Text = Language.Get("msg_dealexpires") + " " + dateString;
					}
				}
			}
		}
		else if (feature != null && poster != null)
		{
			TimeSpan featureRemainingTime = feature.CalculateRemainSeconds();

			//Fetch the poster date
			string[] dates = poster.period.Split('|');
			DateTime now 		= DateTime.UtcNow;
			DateTime endTime	= DateTime.Parse(dates[1]);

			endTime		= new DateTime(endTime.Year, endTime.Month, endTime.Day, 23, 59, 59);
			TimeSpan posterRemainingTime = endTime - now;
			TimeSpan remainingTime = TimeSpan.MinValue;

			//Compare the feature and the poster time
			if (featureRemainingTime.TotalSeconds < posterRemainingTime.TotalSeconds)
			{
				remainingTime = featureRemainingTime;
			}
			else if (posterRemainingTime.TotalSeconds < featureRemainingTime.TotalSeconds)
			{
				remainingTime = posterRemainingTime;
			}

			int sec = (int)remainingTime.TotalSeconds;
			if (sec <= 0)
			{
				expireLabel.Text = "";
			}
			else
			{
				string dateString = string.Format("{0:00}:{1:00}:{2:00}", remainingTime.Hours, remainingTime.Minutes, remainingTime.Seconds);
				if (remainingTime.Days > 0)
				{
					dateString = remainingTime.Days + " " + Language.Get("deal_days") + " " + dateString;
				}				
				expireLabel.Text = Language.Get("msg_dealexpires") + " " + dateString;
			}
		}
		else
		{
			expireLabel.Text = "";
		}
	}
	/*
	string twoDigit(int i)
	{
		return (i < 10)? ("0"+i):(i+"");
	}
	*/

}

using System;


public class SingleMesh
{
	int id;
	string filename;
	string[] materials;
	
	public SingleMesh (int pId, string pFilename, string[] pMaterials)
	{
		this.id = pId;
		this.filename = pFilename;
		this.materials = pMaterials;
	}
}



using UnityEngine;
using System.Collections;

public class RewardRow : MonoBehaviour
{
	
	public GameObject iconGO,iconSpriteGO,xGO,amountGO, itemBlock;
	
	Vector3 fixPos = new Vector3(0f,0f,-3f);
	
	public TYPE type;
	public int amount;
	
	public int rowSpan = 1;
	
	public enum TYPE
	{
		COINS,
		GEMS,
		EXP,
		ENERGY,
		ITEM,
		SKILL0,
		SKILL1,
		SKILL2,
		SKILL3,
		SKILL4
	}
	
	public void setRow(TYPE t, int a)
	{
		type = t;
		amount = a;
		
		
		
		rowSpan = (type.Equals(TYPE.ITEM)? 3:1);
		
		float mul = 2f;
		
		if (t != TYPE.ITEM)
		{
			itemBlock.SetActive(false);
				
			amountGO.GetComponent<UILabel>().text = amount + "";
			
			UISprite iconSprite = iconGO.GetComponent<UISprite>();
			//set icon
			switch (t)
			{
				case TYPE.COINS: iconSprite.spriteName = "top_menu_coin"; break;
				case TYPE.GEMS: iconSprite.spriteName = "top_menu_gem"; break;
				case TYPE.EXP: iconSprite.spriteName = "top_menu_lv"; mul = 1.5f; break;
				case TYPE.ENERGY: iconSprite.spriteName = "top_menu_energy"; mul = 1.6f; break;
				/*case TYPE.SKILL0: iconSprite.spriteName = "jobs_icon_tennis"; break;
				case TYPE.SKILL1: iconSprite.spriteName = "jobs_icon_dancer"; break;
				case TYPE.SKILL2: iconSprite.spriteName = "jobs_icon_model"; break;
				case TYPE.SKILL3: iconSprite.spriteName = "jobs_icon_actress"; break;
				case TYPE.SKILL4: iconSprite.spriteName = "jobs_icon_singer"; break;*/
				case TYPE.ITEM:  break;
			}
			
			iconSprite.MakePixelPerfect();
			
			iconSprite.transform.localScale =  new Vector3(iconSprite.transform.localScale.x * mul,
															iconSprite.transform.localScale.y *mul,
															iconSprite.transform.localScale.z *mul);
			
		}
		
		else
		{
			iconGO.SetActive(false);
			xGO.SetActive(false);
			amountGO.SetActive(false);
			itemBlock.SetActive(true);
			
			Item it = Items.getItem(a);
			UITexture iconTexture = iconSpriteGO.GetComponent<UITexture>();
			
			Texture2D tex = Items.GetIconOfItem(it);
			iconTexture.material = new Material (Shader.Find ("Unlit/Transparent"));
        	iconTexture.material.mainTexture = tex;
        	iconSpriteGO.transform.localScale = new Vector3(256f,256f,1f);
			
			InvokeRepeating("Move", 0f, 0.015f);
		}
	}
	
	
	
	void Move()
	{
		iconSpriteGO.transform.localPosition = fixPos;
	}
}


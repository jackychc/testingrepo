using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class StreetAnimationManager : MonoBehaviour, InputManagerCallback
{
	public static bool animationLock;
	public static bool breakTheLoop, breakTheEnd;
	
	GameObject newCharacter;
	public GameObject girlParent;
	public GameObject atmHintsObj;
	public UIScrollBar streetScrollBar;
	public UIPanel streetPanel;
	public Transform[] friendPositions = new Transform[3];
	bool[] haveFriendInPosition = new bool[3];
	//bool[] greetedForPosition = new bool[3];
	public GameObject[] visitBubbles = new GameObject[3];
	Player[] friendInPosition = new Player[3];
	public UISysFontLabel[] friendName = new UISysFontLabel[3];
	
	int seed;
	int tapStack;
	
	private DateTime idleTime;
	private int moveDirection = 0;
	
	
	float previousScrollValue = -100f;
	bool isScrolling;
	Vector3 charVector;
	public static float charRotation = 0f, currentCharRotation = 0f;
	
	//SceneManager sm;
	
	//string targetScene = "HOME";
	//int goToShop = 1;

	void Start()
	{
		charRotation 		= 0f;
		currentCharRotation = 0f;


		//newCharacter = null;
		//previousScrollValue = streetScrollBar.scrollValue;
		for(int i =0;i < 3; i++)
		{
			haveFriendInPosition[i] = false;
		}
		breakTheLoop = false;
		breakTheEnd = false;
		StartCoroutine(HiOnce());
		StartCoroutine(LoadSomeFriends());
		charVector = new Vector3(0f,0f,0f);
		//InvokeRepeating("RotateRoutine", 0f, 0.02f);
		seed = 1;
		
		
		InputManager.SharedManager.SetCallback(this);		
		idleTime = DateTime.Now;
		
		if (!Globals.isIAP)
			atmHintsObj.SetActive(true);
		
	#if !UNITY_EDITOR
		InvokeRepeating("CheckIdle", 0.0f, 0.1f);
	#endif
	}
	
	void OnDestroy()
	{
		InputManager.SharedManager.RemoveCallback(this);	
	}
	
	#region ATM Hints
	public void SetATMHintsObjEnable(bool enable)
	{
		if (atmHintsObj != null)
			atmHintsObj.SetActive(enable);
	}
	#endregion
	
	private void CheckIdle()
	{
		DateTime currTime = DateTime.Now;
		TimeSpan timeDiff = currTime - idleTime;
		if (timeDiff.TotalSeconds > 30.0f)
		{			
			CancelInvoke("CheckIdle");
			InvokeRepeating("SlowScroll", 0.0f, 0.01f);
		}
	}
	
	private void SlowScroll()
	{
		if (moveDirection == 0)
		{
			if (streetScrollBar.scrollValue + 0.0002f >= 0.999f)
			{
				moveDirection = 1;
				CheckRotation();
				return;
			}
			
			streetScrollBar.scrollValue += 0.0002f;
			streetPanel.clipRange = new Vector4(streetPanel.clipRange.x, 0, streetPanel.clipRange.z, streetPanel.clipRange.w);
		}
		else if (moveDirection == 1)
		{
			if (streetScrollBar.scrollValue - 0.0002f <= 0.01f)
			{
				moveDirection = 0;
				CheckRotation();
				return;
			}
			streetScrollBar.scrollValue -= 0.0002f;
			streetPanel.clipRange = new Vector4(streetPanel.clipRange.x, 0, streetPanel.clipRange.z, streetPanel.clipRange.w);
		}
	}
	
	IEnumerator UpdateAtIntervals()
	{
		while(true)
		{
			if (!Mathf.Approximately(previousScrollValue, -100f))
			{
				bool moved = (Mathf.Abs(previousScrollValue -streetScrollBar.scrollValue) >= 0.0001f);			
				
				if (!isScrolling && moved)
				{
					//Start Scrolling
					StopCoroutine("EndScrolling");
					StartCoroutine(StartScrolling(streetScrollBar.scrollValue - previousScrollValue) );
					//StartScrolling(streetScrollBar.scrollValue - previousScrollValue);
					isScrolling = true;
				}
				/*else if (isScrolling && moved)
				{
					CheckRotation(streetScrollBar.scrollValue - previousScrollValue);
					isScrolling = true;
				}*/
				else if (isScrolling && !moved)
				{
					isScrolling = false;
					StartCoroutine("EndScrolling");
				}
			}
			
			previousScrollValue = streetScrollBar.scrollValue;
			yield return new WaitForSeconds(isScrolling? 0.015f : 0.015f);
		}
	}
	
	IEnumerator RotateAtIntervals()
	{
		while(true)
		{
			currentCharRotation += (charRotation - currentCharRotation) * 0.08f;
			
			charVector.y = currentCharRotation;
			
			girlParent.transform.eulerAngles = charVector;
			
			yield return new WaitForSeconds(0.015f);
		}

		//StartCoroutine(RotateAtIntervals());
	}
	
	void CheckRotation()
	{
		if (moveDirection == 0)
		{
			charRotation = -60f;
		}
		else
		{
			charRotation = 60f;	
		}
	}
	
	IEnumerator StartScrolling(float delta)
	{
		
		// disable all visit bubbles
		for (int i = 0; i < friendPositions.Length; i++)
		{
			visitBubbles[i].SetActive(false);
		}
		
		if (delta > 0)
		{
			// scrolling forward
			//Log.Debug(">> scrolling");
			charRotation = -60f;
			
			
		}
		else
		{
			// scrolling backward
			//Log.Debug("<< scrolling");
			charRotation = 60f;
		}
		
		//if (!breakTheLoop)
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("street_walk");
		
		breakTheLoop = true;
		breakTheEnd = false;
		
		yield return null;
		
	}
	
	IEnumerator EndScrolling()
	{
		// ?
		if (!breakTheEnd)
		{
			//Log.Warning("breakTheEnd:"+breakTheEnd.ToString());

			yield return new WaitForSeconds(0.5f);
			
			//Log.Debug("end scrolling");
			charRotation = 0f;
			
			yield return new WaitForSeconds(0.1f);
			
			if (!breakTheEnd)
			{
				newCharacter.GetComponent<CharacterManager>().InsertAnimation("pose0"+UnityEngine.Random.Range(1,2+1));
				RestartLoop();
			}
		}	
	}
	
	public void CancelEndWait()
	{
		StopCoroutine("EndScrolling");
	}
	
	
	void ResetRotation()
	{
		charRotation = 0f;
	}
	

	IEnumerator HiOnce()
	{
		//while (newCharacter == null)
		{
			yield return new WaitForSeconds(0.005f);
			//if (newCharacter == null)
			newCharacter = Globals.globalCharacter;
			girlParent = GameObject.Find("GirlParent");
			
		}
		
		
		yield return new WaitForSeconds(1.1f);
		
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("street_hi", false);
		
		newCharacter.GetComponent<CharacterManager>().QueueAnimation("pose0"+UnityEngine.Random.Range(1,2+1), true);

		StartCoroutine(UpdateAtIntervals());
		StartCoroutine(LoopAnimations());
		StartCoroutine(RotateAtIntervals());
	}
	
	IEnumerator HiYourFriend(int pos)
	{
		//if (!breakTheLoop)
		{
			breakTheLoop = true;
			friendPositions[pos].GetComponentInChildren<CharacterManager>().InsertAnimation("street_hi", false);
		
			friendPositions[pos].GetComponentInChildren<CharacterManager>().QueueAnimation("pose01", true);
			newCharacter.GetComponent<CharacterManager>().InsertAnimation("street_greet",false);
			//yield return StartCoroutine(PlayAnimation("street_greet", 2.367f));
			RestartLoop();
		}
		
		yield return null;
	}
	

	public IEnumerator LoopAnimations()
	{
		while(true)
		{			
			if (!breakTheLoop && newCharacter.GetComponent<CharacterManager>().animationQueue.Count <= 3)
			{
				switch(seed)
				{
					
					case 1:
						newCharacter.GetComponent<CharacterManager>().QueueAnimation("street_rotate", false);
					break;
					case 2:
						newCharacter.GetComponent<CharacterManager>().QueueAnimation("street_head", false);
					break;
					default:
						newCharacter.GetComponent<CharacterManager>().QueueAnimation("pose0"+UnityEngine.Random.Range(1,2+1), true);
					break;
					
					
				}
				
				seed = UnityEngine.Random.Range(1,10+1);
			}		
			yield return new WaitForSeconds(1.0f);			
		}
	}
	
	
	public void RestartLoop()
	{
		breakTheLoop = false;
	}
	
	
	
	IEnumerator LoadSomeFriends()
	{
		List<Player> targets = new List<Player>();
		
		for (int i = 0; i < Friends.friends.Count/*friendPositions.Length*/; i++)
		{
			Player p = getFriendNotInList(targets);
			if (p != null)
			{
				if (p.wearingItems != null)
				{
					//if (!(Items.requireDLC(VisitFriend.friend.wearingItems)))

					if (Globals.currentDLCVersion >= p.PlayerDLCVersion)
					{
						targets.Add(p);

						if (targets.Count >= friendPositions.Length)
							break;
					}
				}
			}
		}
		
		for (int i = 0; i < Mathf.Min(friendPositions.Length, targets.Count); i++)
		{
			GameObject fdGo = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			fdGo.transform.localPosition = new Vector3(-200000f,-200000f,-200000f);
			fdGo.gameObject.name = "FRIEND";
			fdGo.AddComponent<CharacterManager>();
			yield return StartCoroutine(fdGo.GetComponent<CharacterManager>().EnumGenerateCharacter(targets[i]));
			fdGo.transform.parent = friendPositions[i];
			fdGo.transform.localPosition = Vector3.zero;
			fdGo.transform.localRotation = friendPositions[i].FindChild("Scaler").localRotation;
			fdGo.transform.localScale = friendPositions[i].FindChild("Scaler").localScale;
			
			haveFriendInPosition[i] = true;
			friendInPosition[i] = targets[i];
			friendName[i].Text = targets[i].Name;
		}
		
		yield return null;
	}
	
	Player getFriendNotInList(List<Player> exclude)
	{
		if (Friends.friends.Count > 0)
		{
			Player p = Friends.friends[UnityEngine.Random.Range(0, Friends.friends.Count)];
			if (p != null)
			{
				if(!exclude.Contains(p))
				{
					return p;
				}
			}
		}
		
		return null;
	}
	
	
	public IEnumerator Tapped()
	{
		//Log.Debug("tapped!!!!!!!!!!");
		
		//StopCoroutine("EndScrolling");
		//isScrolling = false;
		
		ResetRotation();
		breakTheLoop = true;
		breakTheEnd = true;
		
		int testAni = UnityEngine.Random.Range(0,3);
		switch(testAni)
		{	
			case 0:
				newCharacter.GetComponent<CharacterManager>().InsertAnimation("street_hi", false);
				break;
			case 1:
				newCharacter.GetComponent<CharacterManager>().InsertAnimation("street_rotate", false);
				break;
			case 2:
				newCharacter.GetComponent<CharacterManager>().InsertAnimation("street_head", false);
			break;
		}
		
		RestartLoop();
		
		yield return null;
		
	}
	
	
	
	public void FriendTapped1()
	{
		FriendTapped(1);
	}
	public void FriendTapped2()
	{
		FriendTapped(2);
	}
	public void FriendTapped3()
	{
		FriendTapped(3);
	}
	
	public void FriendTapped(int pos)
	{
		if (haveFriendInPosition[pos-1])
		{
			//LogFlurry Event
			MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Street_Click_Friends);		
			
			//isScrolling = false;
			breakTheLoop = true;
			breakTheEnd = true;
			ResetRotation();
			
			for (int i = 0; i < friendPositions.Length; i++)
			{
				visitBubbles[i].SetActive(false);
			}
			visitBubbles[pos-1].SetActive(true);
			
			
			StartCoroutine(HiYourFriend(pos-1));
		}
	}
	
	public void VisitClicked1()
	{
		VisitClicked(1);
	}
	public void VisitClicked2()
	{
		VisitClicked(2);
	}
	public void VisitClicked3()
	{
		VisitClicked(3);
	}
	public void VisitClicked(int pos)
	{		
		//Hide ();
		VisitFriend.Visit(friendInPosition[pos-1], "STREET");
		SceneManager.SharedManager.LoadScene(Scenes.FRIENDSHOME, true);
	}
	
	#region InputManagerCallback	
	public void OnInputManagerTouchBegan(Touch touch){}
	
	
	public void OnInputManagerTouchEnded(Touch touch)
	{
		CancelInvoke("SlowScroll");
		idleTime = DateTime.Now;
		InvokeRepeating("CheckIdle", 0.0f, 0.1f);
	}
	
	public void OnInputManagerFemaleCharacterTouched(){}
	public void OnInputManagerBoyfriendChacaterTouched(){}
	public void OnInputManagerBackPressed(){}
	#if UNITY_EDITOR
	public void OnInputManagerMouseClicked(Vector3 clickedPos){}
	#endif
	
	#endregion	

}


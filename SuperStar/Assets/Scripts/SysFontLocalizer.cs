using UnityEngine;
using System.Collections;

public class SysFontLocalizer : MonoBehaviour
{
	public string identifier;

	// Use this for initialization
	void Awake()
	{
		UISysFontLabel sysFontLabel = transform.GetComponent<UISysFontLabel>();
		if (sysFontLabel != null)
		{
			sysFontLabel.Text = Language.Get(identifier);
		}
	}


	void Start ()
	{
		UISysFontLabel sysFontLabel = transform.GetComponent<UISysFontLabel>();
		if (sysFontLabel != null)
		{
			string newText = Language.Get(identifier);
			if (!sysFontLabel.Text.Equals(newText))
				sysFontLabel.Text = newText;
		}
	}
	
	public void ReloadText()
	{
		transform.GetComponent<UISysFontLabel>().Text = Language.Get(identifier);
	}
}


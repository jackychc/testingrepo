using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RewardRowsPositioner : MonoBehaviour
{
	public readonly int oneRowHeight = 55;
	
	public Reward reward;
	
	List<GameObject> rows = new List<GameObject>();
	
	List<int> yPosList = new List<int>();
	
	public void Position()
	{
		
		
		InitializeRows();
		
		yPosList = CalculateYPos();
		
		for (int i = 0; i < rows.Count; i++)
		{
			rows[i].transform.parent = transform;
			rows[i].transform.localPosition = new Vector3(0f, yPosList[i] * 1f,-1f);
			rows[i].transform.localRotation = Quaternion.identity;
			rows[i].transform.localScale = new Vector3(0.5f,0.5f,0.5f);
		}
	}
	
	public void InitializeRows()
	{
		
		GameObject rr;
		
		if (reward.coins > 0)
		{
			rr = (GameObject) Instantiate(Resources.Load("RewardRow") as GameObject);
		
			rr.GetComponent<RewardRow>().setRow(RewardRow.TYPE.COINS, reward.coins);
			rows.Add(rr);
		}
		
		if (reward.gems > 0)
		{
			rr = (GameObject) Instantiate(Resources.Load("RewardRow") as GameObject);
			rr.GetComponent<RewardRow>().setRow(RewardRow.TYPE.GEMS, reward.gems);
			rows.Add(rr);
		}
		
		if (reward.exp > 0)
		{
			rr = (GameObject) Instantiate(Resources.Load("RewardRow") as GameObject);
			rr.GetComponent<RewardRow>().setRow(RewardRow.TYPE.EXP, reward.exp);
			rows.Add(rr);
		}
		
		if (reward.energy > 0)
		{
			rr = (GameObject) Instantiate(Resources.Load("RewardRow") as GameObject);
			rr.GetComponent<RewardRow>().setRow(RewardRow.TYPE.ENERGY, reward.energy);
			rows.Add(rr);
		}
		
		if (!reward.itemIds.Equals(""))
		{
			string[] itemIds = reward.itemIds.Split(',');
			foreach (string s in itemIds)
			{
				rr = (GameObject) Instantiate(Resources.Load("RewardRow") as GameObject);
				rr.GetComponent<RewardRow>().setRow(RewardRow.TYPE.ITEM, Int32.Parse(s));
				rows.Add(rr);
			}
		}
	}
	
	
	List<int> CalculateYPos()
	{
		List<int> returnMe = new List<int>();
		
		int totalRows = rows.Count;
		int totalHeight = 0;
		for (int i = 0; i < rows.Count; i++)
		{
			totalHeight += oneRowHeight * rows[i].GetComponent<RewardRow>().rowSpan;
		}
		
		int yPos = totalHeight/2 - (oneRowHeight/2);
		
		for (int i = 0; i < rows.Count; i++)
		{
			returnMe.Add(yPos);
			
			yPos -= oneRowHeight * rows[i].GetComponent<RewardRow>().rowSpan;
		}
		
		return returnMe;
		
		
	}
}


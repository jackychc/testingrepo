using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class RankScrollListManager : MonoBehaviour {
	
	GameObject[] rankPool = new GameObject[41];
	public VotingLobbyScene lobby;
	
	float xpos = 0f;
	Vector3 resetBoxCollider;
	Vector4 resetClipRange;
	
	public int minRank = 1, maxRank = Int32.MaxValue, headCount = Int32.MaxValue;
	
	int previousCenterRank = 15;
	int centerRank = 15;
	
	public List<Player> players;
	
	Mode refreshMode = Mode.INDEPENDENT;
	
	// Use this for initialization
	void Start () {
		
		//StartCoroutine(retreiveHeadCount());
		
		transform.GetComponent<UIDraggablePanel>().onDragFinished += onDragFinished;
		int width = ((int) Math.Max(3,(Mathf.Log(centerRank+5,10)+1))) * 20;
		
		for (int i = 0; i < 41; i++)
		{
				rankPool[i] = (GameObject) Instantiate(Resources.Load ("RankItem") as GameObject);
			
				rankPool[i].name = "Rank" + i;
				rankPool[i].transform.parent = transform;
				
				//rankPool[i].GetComponent<RankItem>().rankLabel.text = (centerRank+i-20)+"";
				
				rankPool[i].transform.localPosition = new Vector3((i-20) * width, 0f,0f);
				rankPool[i].transform.localRotation = Quaternion.identity;
				rankPool[i].transform.localScale = new Vector3(1f, 1f, 1f);
			
				xpos = xpos + 100f;
			
				rankPool[i].SetActive(false);
		}		
	}
	
	public void RefreshWithPlayers(List<Player> p)
	{
		//transform.gameObject.SetActive(true);
		transform.parent.GetComponent<AnchorTweener>().Forward();
		players = p;
		setRankAndRange(1,1,p.Count);
		refreshMode = Mode.INDEPENDENT;
		Refresh();
	}
	
	
	public void RefreshWithRank(int r)
	{
		//transform.gameObject.SetActive(false);
		setRankAndRange(1,r,headCount);
		refreshMode = Mode.GLOBAL;
		Refresh();
	}
	
	public void Refresh()
	{		
		lobby.ClearBoardData();
		int width = ((int) Math.Max(3,(Mathf.Log(centerRank+5,10)+1))) * 20;
		resetBoxCollider = new Vector3(width * 1f,50f,-1f);
		
		xpos = transform.GetComponent<UICenterOnChild>().centeredObject.transform.localPosition.x;
		
		for (int i = 0; i < 41; i++)
		{
			rankPool[i].GetComponent<BoxCollider>().size = resetBoxCollider;
			rankPool[i].transform.localPosition = new Vector3(xpos + (i-20) * width, 0f,0f);
			
			if ((centerRank - 20 + i) < minRank || (centerRank - 20 + i) > maxRank)
			{
				rankPool[i].SetActive(false);
				continue;
			}
			rankPool[i].SetActive(true);
			rankPool[i].GetComponent<RankItem>().rankLabel.text = (centerRank - 20 + i)+"";
			

		}
		
		previousCenterRank = centerRank;
		
		if (refreshMode.Equals(Mode.INDEPENDENT))
		{
			transform.parent.GetComponent<AnchorTweener>().Forward();
			StartCoroutine(retriveDataOfPlayer(players[centerRank-1]));
		}
		else
			StartCoroutine(retriveDataOfRank(centerRank));
	}
	
	IEnumerator retriveDataOfPlayer(Player p)
	{
		lobby.setRankText(p.votingRank+"");	
		lobby.setVotesText(p.votingVotes+"");
		lobby.setPlayerName(p.Name);
		lobby.MattersOfGameId(p.GameId);
		// TODO: gameId, player name
		
		if (Items.requireDLC(p.wearingItems))
		{
			PopupManager.ShowInformationPopup("The contestant is wearing new clothes that requires DLC.", false); 
		}
		else	
		{
			GameObject femaleObj = GameObject.Find("FEMALE");
			yield return StartCoroutine(femaleObj.GetComponent<CharacterManager>().EnumGenerateCharacter(p));
		}
		
		yield return null;
	}
	
	IEnumerator retriveDataOfRank(int r)
	{
		string rank 	= r.ToString();
		string topicId 	= Globals.viewingTopic.topicId.ToString();
		
		string checkSumData = topicId + rank;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("SearchPlayerWithTopicsRank", checkSumData);	
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("topicId", topicId);
		kvp.Add("rank", rank);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "SearchPlayerWithTopicsRank.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response 			= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			//yield return StartCoroutine(HTTPPost.Post("GetFriends", kvp, result => response = result));
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					// have rank already
					if (al[1] != null)
					{
						Player p = new Player();
						p.playerId = -2;
						lobby.setRankText(((Hashtable)al[1])["rank"] != null? ((Hashtable)al[1])["rank"].ToString() : "--");
						lobby.setVotesText(((Hashtable)al[1])["votes"] != null? ((Hashtable)al[1])["votes"].ToString() : "--");
						p.GameId = ((Hashtable)al[1])["gameId"].ToString();
						p.Name = Globals.mainPlayer.Name;
						lobby.setPlayerName(p.Name);
						p.wearingItems = Items.stringToItemList(((Hashtable)al[1])["voteItems"].ToString());
						lobby.MattersOfGameId(p.GameId);
						GameObject.Find("FEMALE").GetComponent<CharacterManager>().GenerateCharacter(p);
						
					}
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("SearchPlayerWithTopicsRank", kvp, result => response = result));		
	}
	
	public void setRankAndRange(int min, int center, int max)
	{
		minRank = min;
		centerRank = center;
		maxRank = max;
	}
	

	
	
	/*
	IEnumerator retreiveHeadCount()
	{
		string response = "";
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("topicId", Globals.topicId.ToString());
		
		yield return StartCoroutine(HTTPPost.Post("GetTopicsHeadCount", kvp, result => response = result));
		
		ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
		if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
		{
			// have rank already
			if (al[1] != null)
			{
				headCount = Int32.Parse(al[1].ToString());
				
			}
		}
	}*/
	
	void onDragFinished()
	{
		centerRank = transform.GetComponent<UICenterOnChild>().centeredObject.GetComponent<RankItem>().getRank();
		Refresh();
	}
	
	public enum Mode
	{
		INDEPENDENT,
		GLOBAL
	}

}

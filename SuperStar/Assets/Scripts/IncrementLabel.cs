using UnityEngine;
using System.Collections;

public class IncrementLabel : MonoBehaviour
{
	public string text = "+1";
	UILabel self;
	Vector3 pos;
	
	// Use this for initialization
	void Start ()
	{
		self = transform.GetComponent<UILabel>();
		self.text = text;
		InvokeRepeating("Move", 0f, 0.018f);
	}
	
	// Update is called once per frame
	void Move ()
	{
		self.alpha -= 0.0075f;
		
		if (self.alpha <= 0f)
		{
			GameObject.Destroy(transform.gameObject);
			return;
		}
		
		pos = transform.localPosition;
		pos.y -= 0.1f;
		transform.localPosition = pos;
	}
}


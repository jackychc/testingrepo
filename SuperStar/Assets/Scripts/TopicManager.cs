using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TopicManager : MonoBehaviour
{
	
	public static bool haveTopic = false;
	
	// Use this for initialization
	void Start ()
	{
		Globals.normalTopic = null;
		Globals.specialTopic = null;
		//DontDestroyOnLoad(transform.gameObject);
		StartCoroutine(ReallyStart());
		
	}
	
	IEnumerator ReallyStart()
	{
		yield return StartCoroutine(GetTopicFromServer());
	}
	

	
	IEnumerator GetTopicFromServer()
	{
		bool t = false;
		
		//for (int i = 0; i < 2; i++)
		{
			GameID gameId = new GameID(Globals.mainPlayer.GameId);
			int playerId = gameId.toInt();
			string checkSumData = gameId.ID + playerId;
			string checkSum = MD5Hash.GetServerCheckSumWithString("GetLatestTopics", checkSumData);
			
			Dictionary<string, string> kvp = new Dictionary<string, string>();
			kvp.Add("gameId",Globals.mainPlayer.GameId);
			kvp.Add("checkSum", checkSum);
			
			string url = Globals.serverAPIURL + "GetLatestTopics.php";
			yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
			
			string response				= NetworkManager.SharedManager.Response;
			NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
			
			if (state.Equals(NetworkManager.State.Success))
			{
				if (response != null && !response.Equals(""))
				{
					ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
					if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
					{
						
						ArrayList alt = new ArrayList(((Hashtable) al[1]).Values);
						
						for (int i = 0; i < alt.Count; i++)
						{
							Hashtable innerHt = (Hashtable) alt[i];
							
							// have topic
							if (innerHt["topicIsEnd"] != null)
							{
								//int isEnd = Int32.Parse(innerHt["topicIsEnd"].ToString());
								
								//if (isEnd == 0)
								{
									if (Int32.Parse(innerHt["topicType"].ToString()) == 0)
									{	
										Globals.normalTopic = new Topic(false);
										Globals.normalTopic.topicId = Int32.Parse(innerHt["topicId"].ToString());
										Globals.normalTopic.topicName = innerHt["topicName"].ToString();
										Globals.normalTopic.endDate = innerHt["topicPeriod"].ToString().Split('|')[1];
										Globals.normalTopic.isEnded = (Int32.Parse(innerHt["topicIsEnd"].ToString()) == 1);
									}
									else if (Int32.Parse(innerHt["topicType"].ToString()) == 1)
									{
										Globals.specialTopic = new Topic(true);
										Globals.specialTopic.topicId = Int32.Parse(innerHt["topicId"].ToString());
										Globals.specialTopic.topicName = innerHt["topicName"].ToString();
										Globals.specialTopic.endDate = innerHt["topicPeriod"].ToString().Split('|')[1];
										Globals.specialTopic.isEnded = (Int32.Parse(innerHt["topicIsEnd"].ToString()) == 1);
									}
									
									
									t = true;
									
								}
							}
						}
						
						if (t)
						{
							haveTopic = true;
							
						}
					}
				}
			}
			
			//yield return StartCoroutine(HTTPPost.Post("GetLatestTopics",kvp, result => response = result));
		}
	}
}


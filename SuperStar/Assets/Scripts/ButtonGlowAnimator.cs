using UnityEngine;
using System.Collections;

public class ButtonGlowAnimator : MonoBehaviour {
	public UISprite target;
	public string[] spriteNames = new string[4];
	public float period = 0.2f;
	public float delay = 2f;
	 int spriteNo = 0;

	// Use this for initialization
	void Start ()
	{
	}
	
	void OnEnable()
	{
		//Log.Debug("ENABLE");
		StopCoroutine("UpdateAtInterval");
		StartCoroutine(UpdateAtInterval());
	}
	
	void OnDisable()
	{
		StopCoroutine("UpdateAtInterval");	
	}
	
	IEnumerator UpdateAtInterval()
	{		
		while(true)
		{			
			spriteNo = ((spriteNo + 1) >= spriteNames.Length)? -1: spriteNo + 1;

			for (int i = 0; i < spriteNames.Length; i++)
			{
				target.spriteName = spriteNames[i];

				yield return new WaitForSeconds(period);
			}

			if (delay > 0f)
			{
				target.spriteName = "$Y%#%#%#";

				yield return new WaitForSeconds(delay);
			}
		}
	}


}

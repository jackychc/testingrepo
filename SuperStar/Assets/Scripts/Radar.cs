using UnityEngine;
using System.Collections;

public class Radar : MonoBehaviour {
	
	public GameObject[] icons = new GameObject[5];
	public GameObject occupationText;
	
	// Use this for initialization
	void Start () {
		Refresh();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Refresh()
	{
		int max = findMax();
		
		for (int i = 0; i < 5; i++)
		{
			icons[i].GetComponent<UISprite>().spriteName = icons[i].GetComponent<UISprite>().spriteName.TrimEnd('b').TrimEnd('_');
			
			if (i == max)
				icons[i].GetComponent<UISprite>().spriteName = icons[i].GetComponent<UISprite>().spriteName + "_b";
		}
		
		string occupation = "";
		
		switch (max)
		{
			case 4: occupation = "Singer"; break;
			case 1: occupation = "Dancer"; break;
			case 0: occupation = "Tennis Player"; break;
			case 3: occupation = "Actress"; break;
			case 2: occupation = "Model"; break;
			default: occupation = "Singer";  break;
		}
		occupationText.GetComponent<UISysFontLabel>().Text = occupation;
		
		GameObject.Find("Graph").GetComponent<RadarChartDrawer>().updatePolygon();
	}
	
	int findMax()
	{
		int index = 0;
		float max = 0f;
		for (int i = 0; i < 5; i++)
		{
			if (Globals.mainPlayer.getSkill().skill[i] > max)
			{
				max = Globals.mainPlayer.getSkill().skill[i];
				index = i;
			}
		}
		
		return index;
	}
}

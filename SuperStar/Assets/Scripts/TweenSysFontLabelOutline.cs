using UnityEngine;
using System.Collections;

public class TweenSysFontLabelOutline : UITweener {
	
	public int from;
	public int to;
	
	public int Quality
	{
		set{
			if (sysFontLabel != null)
				sysFontLabel.EffectQuality = value;
		}
		
		get
		{
			if (sysFontLabel != null)
				return sysFontLabel.EffectQuality;
			else
				return 0;
		}
	}
	
	private UISysFontLabel sysFontLabel;
	
	void Awake()
	{
		sysFontLabel = this.gameObject.GetComponent<UISysFontLabel>();
	}
	
	//public Transform cachedTransform { get { if (mTrans == null) mTrans = transform; return mTrans; } }
	//public Vector3 position { get { return cachedTransform.localPosition; } set { cachedTransform.localPosition = value; } }
	
	override protected void OnUpdate (float factor, bool isFinished) {  Quality = (int)(from * (1f - factor) + to * factor);}

	/// <summary>
	/// Start the tweening operation.
	/// </summary>
	/// 
	
	static public TweenSysFontLabelOutline Begin (GameObject go, float duration, int quality)
	{
		TweenSysFontLabelOutline con = UITweener.Begin<TweenSysFontLabelOutline>(go,duration);
		con.from = con.Quality;
		con.to = quality;
		
		if (duration <= 0f)
		{
			con.Sample(1f, true);
			con.enabled = true;
		}	
		
		return con;
	}
}

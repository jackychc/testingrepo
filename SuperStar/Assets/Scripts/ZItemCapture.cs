using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZItemCapture : MonoBehaviour
{
	public GameObject whitedoll;
	public string gFileName;
	GameObject target;
	
	int subcat = 16;
	bool singleCap = false;

	// automated capture
	List<int> desiredSubcat = new List<int>(){10,13,9,14,16,25,24};
	int leastItemId = 484;

	public GameObject[] cameras = new GameObject[28];
	
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(ReallyStart());

	}

	IEnumerator ReallyStart()
	{
		if (GameObject.Find("FEMALE") != null)
			Destroy(GameObject.Find("FEMALE"));
		
		yield return new WaitForEndOfFrame();

		if (singleCap)
		{
			StartCoroutine(SingleCapture());
		}
		
		else
		{
			whitedoll = GameObject.Find("whitedoll");
			StartCoroutine(AutoCapture());
		}
	}
	
	public IEnumerator SingleCapture()
	{		
		yield return new WaitForEndOfFrame();
		
		string fileName = gFileName;
		Log.Debug("filename?"+fileName);

		Texture2D tex = new Texture2D(512,512, TextureFormat.ARGB32, false);
    	tex.ReadPixels(new Rect(0,0,512,512), 0, 0);
    	tex.Apply();
    	byte[] imageBytes = tex.EncodeToPNG();
		System.IO.File.WriteAllBytes(fileName+".png", imageBytes);
	}


	public IEnumerator AutoCapture()
	{		
		yield return new WaitForEndOfFrame();


		//List<Item> targetItems = new List<Item>();

		foreach (int s in desiredSubcat)
		{

			for (int c = 0; c < cameras.Length; c++)
			{
				if (cameras[c] != null)
					cameras[c].SetActive(false);
			}

			if (cameras[s] != null)
				cameras[s].SetActive(true);

			yield return new WaitForEndOfFrame();

			List<Item> targetItems = new List<Item>();

			List<Item> sieve =  Items.getItemsFromSubcategory(s);

			foreach (Item i in sieve)
			{
				if (i.itemId >= leastItemId)
					targetItems.Add(i);

			}


		

			for (int i = 0; i < targetItems.Count; i++)
			{
				if (Items.isNoneItem(targetItems[i])) continue;

				//if (i > 1)
				if (target != null)
				{
					Destroy (target);
					target = null;
					//	Destroy(GameObject.Find("hairT_back"+((i-1) < 10? "00":"0")+""+(i-1) +"(Clone)"));
				}

				yield return new WaitForEndOfFrame();

				Object obj = Resources.Load("CharacterAssets/Parts/"+ItemSubcategories.getSubcategory(targetItems[i].subCategoryId).subcategoryName+"/"+targetItems[i].meta);

				if (obj == null)
					continue;

				target = Instantiate(obj) as GameObject;
				target.transform.parent = whitedoll.transform;
				target.animation.Stop();


				//

				yield return new WaitForEndOfFrame();


				// load tex also
				SkinnedMeshRenderer smr = target.GetComponentInChildren<SkinnedMeshRenderer>();
				Material[] mList = smr.sharedMaterials;

				if (subcat != (int)ItemSubcategories.Id.BACKHAIR && subcat != (int)ItemSubcategories.Id.FRONTHAIR) 
				{
					Texture2D texture= (Texture2D) Resources.Load("CharacterAssets/Parts/"+ItemSubcategories.getSubcategory(targetItems[i].subCategoryId).subcategoryName+"/tex/" + targetItems[i].texMeta);	
					mList[0].SetTexture("_MainTex", texture);
				}
				else
					mList[0].color = new Color(217f/255f,215f/255f,83f/255f,1f);
				smr.sharedMaterials = mList;


				yield return new WaitForEndOfFrame();



				string fileName = targetItems[i].itemId + "_icon";
				Log.Debug("filename?"+fileName);






				Texture2D tex = new Texture2D(512,512, TextureFormat.ARGB32, false);
				tex.ReadPixels(new Rect(1, 0, 512,512), 0, 0);
				tex.Apply();
				byte[] imageBytes = tex.EncodeToPNG();
				System.IO.File.WriteAllBytes(fileName+".png", imageBytes);

			}

		}

	}

	/*
	public IEnumerator Capture()
	{		
		yield return new WaitForEndOfFrame();
		
		List<Item> targetItems = Items.getItemsFromSubcategory(subcat );
		
		for (int i = 0; i < targetItems.Count; i++)
		{
			if (Items.isNoneItem(targetItems[i])) continue;
			
			//if (i > 1)
			if (target != null)
			{
				Destroy (target);
				target = null;
			//	Destroy(GameObject.Find("hairT_back"+((i-1) < 10? "00":"0")+""+(i-1) +"(Clone)"));
			}
			
			yield return new WaitForEndOfFrame();

			Object obj = Resources.Load("CharacterAssets/Parts/"+ItemSubcategories.getSubcategory(subcat).subcategoryName+"/"+targetItems[i].meta);

			if (obj == null)
				continue;

			target = Instantiate(obj) as GameObject;
			target.transform.parent = whitedoll.transform;
			target.animation.Stop();
			
		
			//
			
			yield return new WaitForEndOfFrame();
			
			
			// load tex also
			SkinnedMeshRenderer smr = target.GetComponentInChildren<SkinnedMeshRenderer>();
			Material[] mList = smr.sharedMaterials;
			
			if (subcat != (int)ItemSubcategories.Id.BACKHAIR && subcat != (int)ItemSubcategories.Id.FRONTHAIR) 
			{
				Texture2D texture= (Texture2D) Resources.Load("CharacterAssets/Parts/"+ItemSubcategories.getSubcategory(subcat).subcategoryName+"/tex/" + targetItems[i].texMeta);	
				mList[0].SetTexture("_MainTex", texture);
			}
			else
				mList[0].color = new Color(217f/255f,215f/255f,83f/255f,1f);
			smr.sharedMaterials = mList;
			
		
			yield return new WaitForEndOfFrame();
			
			
			
			string fileName = targetItems[i].itemId + "_icon";
			Log.Debug("filename?"+fileName);
			
			
			
		
			
			
			Texture2D tex = new Texture2D(512,512, TextureFormat.ARGB32, false);
        	tex.ReadPixels(new Rect(0, 0, 512,512), 0, 0);
        	tex.Apply();
        	byte[] imageBytes = tex.EncodeToPNG();
			System.IO.File.WriteAllBytes(fileName+".png", imageBytes);
			
		}
		
		
	}*/
	
	public static Vector4 getDimensions(float proportionX, float proportionY, float offsetX, float offsetY )
	{
		Vector4 returnMe = Vector4.zero;
		
		//Log.Debug("screen width height" + Screen.width +","+Screen.height);
		
		float screenRatio = Screen.height * 1f / Screen.width;
		
		// result picture's pixel dimension
		returnMe.y = Screen.width *       proportionX * Mathf.Min(1f, (screenRatio/1.5f));
		returnMe.z = Screen.height *      proportionY * Mathf.Min(1f, (1.5f/screenRatio));
		
		Log.Debug("final dimension: " + returnMe.y +","+ returnMe.z);
			
		
		returnMe.w = (Screen.width/2) - (returnMe.y/2) + (offsetX * Screen.width);
		if (screenRatio < 1.5f) 
			returnMe.w = (Screen.width/2) - (returnMe.y/2) + (offsetX * (Screen.width * screenRatio/1.5f));
		
		returnMe.x = (Screen.height/2) - (returnMe.z/2) + (offsetY * Screen.height);
		if (screenRatio > 1.5f) 
			returnMe.x = (Screen.height/2) - (returnMe.z/2) + (offsetY * (Screen.height * 1.5f/screenRatio));
		
		Log.Debug("final offset: " + returnMe.w + "," + returnMe.x);
		
		
		return returnMe;
	}
}


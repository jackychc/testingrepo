using UnityEngine;
using System.Collections;

public class FerrisWheelRotation : MonoBehaviour {
	
	public GameObject[] cars = new GameObject[16];
	Transform[] carTrans = new Transform[16];
	
	Vector3[] pos = new Vector3[16];
	Vector3 euler = new Vector3(0f,0f,0f);
	public GameObject wheelCenter;
	Transform wheelCenterTrans;
	
	public float radius = 275f;
	public float speed = 1f;
	float angle = 0f;
	
	// Use this for initialization
	void Start ()
	{
	
		//float pAngle = 0f;
		for (int i = 0; i < 16; i++)
		{
			carTrans[i] = cars[i].transform;
			carTrans[i].localPosition = new Vector3(
				radius * CosineApproximation(( i*360f/16)),
				radius * SineApproximation(( i*360f/16)),
				0f);
			pos[i] = carTrans[i].localPosition;
			
			//pAngle += 360f/16;
		}
		
		wheelCenterTrans = wheelCenter.transform;
		euler = wheelCenterTrans.eulerAngles;
		
		InvokeRepeating("Move", 0f, 0.03f);
	}
	
	// Update is called once per frame
	void Move () {
		for (int i = 0; i < 16; i++)
		{
			pos[i].x = radius * CosineApproximation(angle + i*360f/16);
			pos[i].y = radius * SineApproximation(angle + i*360f/16);
			
			carTrans[i].localPosition = pos[i];
			
		}
		
		euler.z = 10f + angle;
		wheelCenterTrans.eulerAngles = euler;
		
		angle += speed * 1f/12;
			
		while (angle > 360f)
			angle -= 360f;
	}
	
	float CosineApproximation(float degree)
	{
		return SineApproximation(90 - degree);
	}
	
	float SineApproximation(float deg)
	{
		//float deg = degree;
		int dir = 1;
		
		// make sure degree is between [0,360]
		while (deg < 0f)
			deg= 360f + deg;
		
		while (deg > 360f)
			deg = deg - 360f;
		
		if (deg > 180f)
		{
			dir = -1;
			deg = 360f - deg;
		}
		
		return dir * (4f*deg*(180f-deg)) * 1f/(40500f-deg*(180f-deg));
	}
}

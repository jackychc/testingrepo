using UnityEngine;
using System.Collections;

public class GrowEffectController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OnGrowEffectFinished()
	{		
		TweenPosition tp 	= this.gameObject.GetComponent<TweenPosition>();
		TweenAlpha ta 		= this.gameObject.GetComponent<TweenAlpha>();
		
		Vector3 fromVector 	= tp.from;
		Vector3 toVector	= tp.to; 
		
		if (ta != null)
			Destroy(ta);
		if (tp != null)
			Destroy(tp);		
		
		tp = this.gameObject.AddComponent<TweenPosition>();
		tp.style			= UITweener.Style.Once;
		tp.delay			= 2.0f;
		tp.duration			= 0.5f;
		tp.from				= fromVector;
		tp.to				= toVector;
		tp.eventReceiver	= this.gameObject;
		tp.callWhenFinished = "OnGrowEffectFinished";
		tp.enabled 			= true;
		
		ta = this.gameObject.AddComponent<TweenAlpha>();
		ta.style			= UITweener.Style.Once;
		ta.delay			= 2.0f;
		ta.duration 		= 0.2f;
		ta.from				= 0;
		ta.to				= 1;		
		ta.enabled 			= true;
		
		this.gameObject.GetComponent<UISprite>().alpha = 0.0f;
	}
}

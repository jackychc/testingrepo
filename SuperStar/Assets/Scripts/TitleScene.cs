using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TitleScene : MonoBehaviour
{
	public GameObject titleObject;
	public GameObject dollObject;
	public GameObject tapToStart;
	public GameObject buildVerObj;
	public GameObject girlParentObj;
	public PositionTweener posTweener;



	private volatile bool uiLock = false;

	IEnumerator Start()
	{
		uiLock = true;

		//Check APK Signature
		#if UNITY_ANDROID && !UNITY_EDITOR && !UNITY_AMAZON
		string keyHash = new AndroidJavaClass("muneris.unity.androidbridge.utils.TestLog").CallStatic<string>("getHash");
		if (!Globals.keyhash.Equals(keyHash))
		{
			Log.Debug("APK Signautre is not valid!!");
			PopupManager.ShowSystemErrorPopup(Language.Get("msg_apkError"), false);
			yield break;
		}
		else if (Application.genuineCheckAvailable)
		{
			if (!Application.genuine)
			{
				Log.Debug("APK Signautre is not valid!!");
				PopupManager.ShowSystemErrorPopup(Language.Get("msg_apkError"), false);
				yield break;
			}
		}
		#endif

		#if !UNITY_EDITOR
		//Check Cracked
		if (Globals.iapRecord == null || Globals.iapRecord.Replace(" ", "").Equals(""))
		{
			int gemPackage6 	= (int)IAPPopup.IAPPackageIds.gemPackage6;
			int coinPackage6 	= (int)IAPPopup.IAPPackageIds.coinPackage6;

			if (Globals.mainPlayer.Coins > coinPackage6 * 10)
			{
				Globals.isCracked = true;
			}
			else if (Globals.mainPlayer.Gems > gemPackage6 * 10)
			{
				Globals.isCracked = true;
			}
		}

		if (Globals.isCracked)
		{
			PopupManager.ShowSystemErrorPopup(Language.Get("msg_apkError"), true);
			yield break;
		}
		#endif
		
		initTitleSprite();
		CheckNewAppVersion();

		yield return StartCoroutine(DelayedStart());
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.TITLE);

		uiLock = false;

		//Application.LoadLevel(15);
	}	
	
	private void CheckNewAppVersion()
	{
		//Check for New App Version
		Muneris.AppVersionCheck.NewAppVersion v = MunerisManager.SharedManager.GetNewAppVersion();
		if (v != null)
		{
			PopupManager.ShowNewAppVersionPopup(v, false);
		}
	}
	
	private void initTitleSprite()
	{
		//Change the title sprite depends on the language
		if (titleObject != null)
		{
			UISprite titleSprite = titleObject.GetComponent<UISprite>();
			if (titleSprite != null)
			{
				LanguageCode currLangCode = (LanguageCode)Enum.Parse(typeof(LanguageCode), Globals.langCode);			
#if FGP
				if (currLangCode.Equals(LanguageCode.JA))
				{
					titleSprite.spriteName = "FGP_titleScreen_title_ja";
				}
				else if (currLangCode.Equals(LanguageCode.KO))
				{
					titleSprite.spriteName = "FGP_titleScreen_title_ko";
				}
				else if (currLangCode.Equals(LanguageCode.ES))
				{
					titleSprite.spriteName = "FGP_titleScreen_title_es";
				}
				else
				{
					titleSprite.spriteName = "FGP_titleScreen_title_en";
				}
#elif SFG
				titleSprite.spriteName = "SFG_titleScreen_title";
#endif
			}
			
			titleSprite.MakePixelPerfect();
		}
	}
		
	IEnumerator DelayedStart()
	{
		/*
		if (Globals.shouldShowGameIdPopup)
		{
			Globals.shouldShowGameIdPopup =  false;
			
			PopupManager.ShowGameIDGetPopup(Globals.mainPlayer.GameId, false);
		}
		*/

		//StartCoroutine(StartTimer());		
		//buildVerObj.GetComponent<UISysFontLabel>().Text = "Ver: "+ Globals.buildVerString;
		
		SoundManager.SharedManager.ChangeMusic("SuperstarStory_title");

		Globals.globalCharacter.transform.localPosition = new Vector3(999, 999, 999);
		yield return StartCoroutine(Globals.globalCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));

		if (Globals.globalCharacter.transform.parent == null)
			Globals.globalCharacter.transform.parent 		= GameObject.Find("GirlParent").transform;
		Globals.globalCharacter.transform.localPosition 		= Vector3.zero;
		Globals.globalCharacter.transform.parent.localPosition	= Vector3.zero;
		
		if (Globals.mainPlayer.InitializedPlayer)
		{
			//Log Flurry Event
			Hashtable param = new Hashtable();
			param["Language"] = Globals.langCode;
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Language_Used, param);
			
			param = new Hashtable();
			param["Level"] = Globals.mainPlayer.Level;
			MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Current_Level, param);			
		}
		
		//Increase the NumOfSessions
		if (SceneManager.SharedManager.PrevScene.Equals(Scenes.INITDB) || SceneManager.SharedManager.PrevScene.Equals(Scenes.NONE))
		{
			Globals.mainPlayer.NumOfSessions++;
			Log.Debug("Num of Sessions: " + Globals.mainPlayer.NumOfSessions);
		}
		
		/*
		#if UNITY_ANDROID && !UNITY_EDITOR
		MunerisManager.SharedManager.TestNewVersion();
		#endif
		*/
		//PopupManager.ShowNewAppVersionPopup(new Muneris.AppVersionCheck.NewAppVersion("ABC", false), false);
		
		DontDestroyOnLoad(GameObject.Find("GirlParent"));
		Globals.globalCharacter.GetComponent<CharacterManager>().InsertAnimation("street_walk");
		posTweener.Forward();

		//yield return new WaitForSeconds(0.5f);
		//dollObject.GetComponent<AnchorTweener>().Forward();

		do
		{
			yield return new WaitForSeconds(0.1f);
		} while (posTweener.proportion < 0.95f);

		tapToStart.SetActive(true);
		Globals.globalCharacter.GetComponent<CharacterManager>().InsertAnimation("pose01");

	}
	
	/*
	IEnumerator StartTimer()
	{
		GameObject timer = GameObject.Find("Timer");
		
		if (timer == null)
		{
			timer = (GameObject) Instantiate(Resources.Load("Timer") as GameObject);
			
			DontDestroyOnLoad(timer);
		}		
		yield return null;	
	}*/
	
	public void EnterItemCaptureMode()
	{
#if UNITY_EDITOR
		//Application.LoadLevel((int)Scenes.ZITEMCAPTURE);
		//SceneManager.SharedManager.LoadScene(Scenes.ZITEMCAPTURE, false);
#else
		//Globals.mainPlayer.GainCoins(999999);
		//Globals.mainPlayer.GainGems(999999);
#endif
	}
	
	public void WashDB()
	{
		StartCoroutine(DatabaseManager.SharedManager.WashDB());
	}
	
	public void GoToStreetScene()
	{
		if (uiLock)
			return;

		/*
		Destroy(Globals.globalCharacter);
		Application.LoadLevel((int)Scenes.ZITEMCAPTURE);
		return;
		*/

		if (Globals.mainPlayer.InitializedPlayer)
			SceneManager.SharedManager.LoadScene(Scenes.STREET, true);
		else
			SceneManager.SharedManager.LoadScene(Scenes.CLOSET, true);
	}
	
	
	private void OnPrivacyButtonClicked()
	{
		if (uiLock)
			return;

#if UNITY_ANDROID
		Application.OpenURL(Globals.androidPrivacyURL);
#elif UNITY_IPHONE
		Application.OpenURL(Globals.iosPrivacyURL);	
#endif
	}

	private void OnFanPageClicked()
	{
		if (uiLock)
			return;

		FacebookManager.SharedManager.ShowFanPage(Globals.facebookFanPageId);
	}

}
;
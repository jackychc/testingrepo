 using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class HomeScene : MonoBehaviour, InputManagerCallback {
	public ParticleSystem kissHeartParticleSystem;
	public GameObject giftThinkingObj;
	public GameObject getGiftButtonObj;
	public GameIdAsker friendPopupAsker;
	
	int animationAlgorithm = 1;
	
	GameObject newCharacter;
	int chars = 0;
	
	public GameObject[] objectsToHideForPhoto = new GameObject[3];
	
	
	//public GameObject rewardTrigger;
	
	public UIInput inputField;
	public UISysFontLabel playerNameLabel;
	
	public AnchorTweener radarTweener;
	public UISprite occupationIcon;
	public UISysFontLabel occupationText;
	
	GameObject peripheralItem, peripheralChild, boyfriendModel;
	
	bool shouldLoopAnimation;
	
	bool giftUILock;
	
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(ReallyStart());
		inputField.text = Globals.mainPlayer.Name;
		SetOccupation();
		
		InvokeRepeating("Move", 0f, 0.03f);
		
		SoundManager.SharedManager.ChangeMusic("SuperstarStory_home_remix0");
		InputManager.SharedManager.SetCallback(this);
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.HOME);
		
		StartCoroutine(DelayedStart());
	}
	
	IEnumerator DelayedStart()
	{
		yield return new WaitForSeconds(0.5f);
		
		
		
		if (Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId) != null && QuestFlags.GetFlag(-102).intValue == 0)
		{
			QuestFlags.SetFlag(-102, 1);
			PopupManager.ShowTutorialPopup(2, false);
		}
	}
	
	
	void OnDestroy()
	{
		InputManager.SharedManager.RemoveCallback(this);
	}
	
	public void SubmitNewName()
	{
		Log.Debug("name commited");
		
		if (inputField.text.Equals(""))
			inputField.text = "P"+Globals.mainPlayer.GameId;
		
		Globals.mainPlayer.Name = inputField.text;
	}

	public void RegenCharacters()
	{
		StartCoroutine(ReallyStart());
	}	

	public IEnumerator ReallyStart()
	{
		GameObject gp = GameObject.Find("GirlParent");
		gp.transform.localPosition = Vector3.zero;
		gp.transform.localRotation = Quaternion.identity;
		gp.transform.localScale = Vector3.one;
		
		if (boyfriendModel != null)
		{
			shouldLoopAnimation = false;
			StopCoroutine("LoopAnimations");

			if (peripheralItem != null)
			{
				DestroyImmediate(peripheralItem);
				peripheralItem = null;
			}

			DestroyImmediate(boyfriendModel);
			boyfriendModel = null;
		}

		Boyfriend myBf = Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId);
		if (myBf != null)
		{
			// if have a current bf that is 100%, generate him also
			animationAlgorithm = 2;
			
			// load YOU
			/*if (GameObject.Find("FEMALE") == null)
			{
				newCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
				newCharacter.name = "FEMALE";
				newCharacter.AddComponent<CharacterManager>();*/
				newCharacter = Globals.globalCharacter;
				newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference2A"));
				yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
				//DontDestroyOnLoad(newCharacter);
			//}
			
			/*else
			{
				newCharacter = GameObject.Find("FEMALE");
				newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference2A"));
				yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
			}*/
			
			newCharacter.GetComponent<CharacterManager>().InsertAnimation(RandomIdleAnimationString("g"));

			Log.Debug("Start GetBoyfriend Mesh");
			yield return StartCoroutine(GetBoyfriendMesh(myBf));
			boyfriendModel.transform.localPosition = Vector3.zero;
			Log.Debug("Finish GetBoyfriend Mesh");
			boyfriendModel.name = "BOYFRIEND";
			CharacterManager cm = boyfriendModel.AddComponent<CharacterManager>();
			cm.InsertShadow();
			cm.InsertAnimation(RandomIdleAnimationString("b"));
			cm.SetPRSbyReference(GameObject.Find("CharacterReference2B"));
			CheckBoyfriendReward();
		}			
		else
		{
			/*if (GameObject.Find("FEMALE") == null)
			{
			
				newCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
				newCharacter.name = "FEMALE";
				newCharacter.AddComponent<CharacterManager>();*/
			Debug.Log("after throwing...");
				newCharacter = Globals.globalCharacter;
				newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
				yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
				
				//DontDestroyOnLoad(newCharacter);
			//}			
			/*else
			{
				newCharacter = GameObject.Find("FEMALE");
				newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
				yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
			}*/
			
			animationAlgorithm = 1;
			newCharacter.GetComponent<CharacterManager>().InsertAnimation(RandomIdleAnimationString("g"));
			
			shouldLoopAnimation = true;
			//shouldLoopAnimation = true;
			
			StartCoroutine(LoopAnimations());
			
			peripheralItem = null;
			peripheralChild = null;
		}
		
		//CheckBoyfriendReward();
	}
	
	public bool ShouldShowGift()
	{
		Boyfriend myBf = Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId);
		if(myBf == null) { return false;}
		
		if (myBf.rewardTimer >= 0)
		{
			List<int> checkPoints = Boyfriends.checkPoints;
			for (int i = 0 ; i < checkPoints.Count; i++)
			{
				int checkPointTime = checkPoints[i];
				if (myBf.rewardTimer <= checkPointTime && myBf.rewardClaimed[i] == 0)
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	private bool HaveGiftClaim()
	{
		Boyfriend myBf = Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId);
		if(myBf == null)
			return false;
		
		List<int> checkPoints = Boyfriends.checkPoints;
		for (int i = 0 ; i < checkPoints.Count; i++)
		{
			if (myBf.rewardClaimed[i] == 0)
				return true;
		}
		
		return false;
	}
	
	public void CheckBoyfriendReward()
	{
		if (ShouldShowGift())
		{
			if (peripheralItem == null)
			{
				peripheralItem = (GameObject) Instantiate((Resources.Load("CharacterAssets/Peripheral/giftbox_obj") as GameObject));
				//peripheralItem = tempPeri.transform.FindChild("DM_giftbox_ALL").gameObject;
			}
			
			peripheralChild = peripheralItem.transform.FindChild("DM_giftbox_ALL").gameObject;
			peripheralChild.transform.parent = boyfriendModel.transform;
			peripheralItem.animation.AddClip(Resources.Load("CharacterAssets/Animation/visitR_gift") as AnimationClip,"visitR_gift");
			
			peripheralItem.SetActive(true);
			peripheralChild.SetActive(true);
			
			newCharacter.GetComponent<CharacterManager>().InsertAnimation("visitL_gift");
			boyfriendModel.GetComponent<CharacterManager>().InsertAnimation("BF_gift");
			//if (boyfriendModel.animation.GetClip("BF_gift") == null)
			//	boyfriendModel.animation.AddClip(Resources.Load("CharacterAssets/Animation/BF_gift") as AnimationClip,"BF_gift");
			//boyfriendModel.animation.Play("BF_gift");
			peripheralItem.animation.Play("visitR_gift");
			shouldLoopAnimation = false;
			getGiftButtonObj.SetActive(true);
		}		
		else
		{
			newCharacter.GetComponent<CharacterManager>().InsertAnimation("BF_idle_g");
			//boyfriendModel.animation.Play("BF_idle_b");
			boyfriendModel.GetComponent<CharacterManager>().InsertAnimation("BF_idle_b");
			
			if (peripheralItem != null)
				peripheralItem.SetActive(false);
			if (peripheralChild != null)
				peripheralChild.SetActive(false);
			if (HaveGiftClaim())
			{
				if (giftThinkingObj != null)
					giftThinkingObj.SetActive(true);
			}
			else
			{
				if (giftThinkingObj != null)
					giftThinkingObj.SetActive(false);
			}
			
			getGiftButtonObj.SetActive(false);
			
			shouldLoopAnimation = true;
			StartCoroutine(LoopAnimations());
		}
		//rewardTrigger.transform.localScale = Vector3.zero;
		
	}
	
	public void GiftTapped()
	{
		if (!giftUILock && Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId) != null)
		{
			giftUILock = true;
			StartCoroutine(GetGiftRoutine());
		}
	}
	
	public IEnumerator GetGiftRoutine()
	{
		// open box....
		if (ClaimableRewards() > 0)
		{
			yield return StartCoroutine(GetGiftAnimation());
		}
		
		PopupManager.ShowBoyfriendRewardPopup(Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId), false);
		giftUILock = false;
	}
	
	public void Ask()
	{
		if (giftUILock) return;
		
		friendPopupAsker.Ask();
	}
	
	public IEnumerator GetGiftAnimation()
	{
		giftUILock = true;
		
		if (HaveGiftClaim())
		{
			if (giftThinkingObj.activeSelf)
				giftThinkingObj.SetActive(false);
		}
		
		if (peripheralItem == null)
		{			
			peripheralItem = (GameObject) Instantiate((Resources.Load("CharacterAssets/Peripheral/giftbox_obj") as GameObject));
			//peripheralItem = tempPeri.transform.FindChild("DM_giftbox_ALL").gameObject;
			peripheralChild = peripheralItem.transform.FindChild("DM_giftbox_ALL").gameObject;
			
			peripheralChild.transform.parent = boyfriendModel.transform;
			peripheralItem.animation.AddClip(Resources.Load("CharacterAssets/Animation/visitR_gift") as AnimationClip,"visitR_gift");
		}
		
		peripheralItem.SetActive(true);
		peripheralChild.SetActive(true);
		
		
		peripheralItem.animation.AddClip(Resources.Load("CharacterAssets/Animation/visitR_gift") as AnimationClip,"visitR_giftopen");
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("visitL_giftopen",false);
		boyfriendModel.GetComponent<CharacterManager>().InsertAnimation("BF_giftopen",false);
		peripheralItem.animation.Play("visitR_giftopen");
		
		
		// create local notification of next gift
			
		yield return new WaitForSeconds(1.4f);
		
		SoundManager.SharedManager.PlaySFX("gift_open");
		
		yield return new WaitForSeconds(1.4f);
		giftUILock = false;
	}
	
	int ClaimableRewards()
	{
		int returnMe = 0;
		
		if (Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId) == null)
			return 0;
		
		if (Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId).rewardTimer >= 0)
		{	
			
			for (int i = 0 ; i < Boyfriends.checkPoints.Count; i++)
			{
				if (Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId).rewardTimer <= Boyfriends.checkPoints[i])
				{
					
					
					if (Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId).rewardClaimed[i] == 0)
					{
						// not claimed
						returnMe++;
					}
				}
			}
		}
		return returnMe;
	}
	
	/*public void TriggerClicked()
	{
		// show that bf popup
		PopupManager.ShowBoyfriendRewardPopup(Boyfriends.GetBoyfriend(Globals.mainPlayer.CurrentBoyfriendId));
	}*/
	
	public void CapturePhoto()
	{	
		if (giftUILock) return;
		
		SceneManager.SharedManager.LoadScene(Scenes.PHOTOSHOT, false);
		//StartCoroutine(CapturePhotoRoutine());
	}
	
	public IEnumerator CapturePhotoRoutine()
	{
		bool[] originalStatus = new bool[objectsToHideForPhoto.Length];
		for(int i = 0; i < objectsToHideForPhoto.Length; i++)
		{
			originalStatus[i] = objectsToHideForPhoto[i].activeSelf;
			objectsToHideForPhoto[i].SetActive(false);
		}
		
		yield return new WaitForEndOfFrame();
		Vector4 dimensions = ScreenCapturer.getDimensions(1.0f, 1.0f, 0.0f, 0.0f);
		
		int startX = (int) dimensions.w;
		int startY = (int) dimensions.x;
        int width = (int) dimensions.y;
        int height = (int) dimensions.z;
		
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(startX, startY, width, height), 0, 0);
        tex.Apply();
        byte[] imageBytes = tex.EncodeToPNG();
		
		for(int i = 0; i < objectsToHideForPhoto.Length; i++)
		{
			objectsToHideForPhoto[i].SetActive(originalStatus[i]);
		}
	}
	
	public void ShowBoyfriendList()
	{
		PopupManager.ShowBoyfriendListPopup(false);
	}
	
	public void PenClicked()
	{
		Log.Debug("pen clicked, start typing your name");
		inputField.selected = true;
	}
	
	void Move()
	{
		playerNameLabel.Text = inputField.text;
	}
	
	public void GoToStreetScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.STREET, true);	
	}

	public void GroupChat()
	{
		SceneManager.SharedManager.LoadScene(Scenes.GROUPCHATSCENE, true);
	}
	
	public void GoToClosetScene()
	{
		if (giftUILock) return;	

		SceneManager.SharedManager.LoadScene(Scenes.CLOSET, false);
	}
	
	public void SetOccupation()
	{
		int index = 0;
		float max = 0f;
		for (int i = 0; i < 5; i++)
		{
			if (Globals.mainPlayer.getSkill().skill[i] > max)
			{
				max = Globals.mainPlayer.getSkill().skill[i];
				index = i;
			}
		}
		
		switch (index)
		{
			case 0: occupationText.Text = "Tennis Player"; occupationIcon.spriteName = "titleName_tennis"; occupationIcon.MakePixelPerfect(); break;
			case 1: occupationText.Text = "Dancer"; occupationIcon.spriteName = "titleName_dancer"; occupationIcon.MakePixelPerfect();break;
			case 2: occupationText.Text = "Model"; occupationIcon.spriteName = "titleName_model"; occupationIcon.MakePixelPerfect();break;
			case 3: occupationText.Text = "Actress";occupationIcon.spriteName = "titleName_actress"; occupationIcon.MakePixelPerfect();break;
			case 4: occupationText.Text = "Singer";occupationIcon.spriteName = "titleName_singer"; occupationIcon.MakePixelPerfect();break;
			default: occupationText.Text = "Singer";occupationIcon.spriteName = "titleName_singer"; occupationIcon.MakePixelPerfect(); break;
		}
	}

	public void TitleShowerClicked()
	{
		if (!giftUILock)
		{
			PopupManager.ShowAchievementListPopup(SkillTable.GetSkillTier(Globals.mainPlayer.TitleSkill).skillType+1, false);
		}
	}
	
	public IEnumerator LoopAnimations()
	{
		while(shouldLoopAnimation)
		{			
			if (newCharacter != null)
			{
				CharacterManager cm = newCharacter.GetComponent<CharacterManager>();
				if (cm != null && cm.animationQueue.Count <= 3)
					cm.QueueAnimation(RandomIdleAnimationString("g"), false);	
			}	
			if (boyfriendModel != null)
			{
				CharacterManager cm = boyfriendModel.GetComponent<CharacterManager>();
				if (cm != null && cm.animationQueue.Count <= 3)
				{
					cm.QueueAnimation(RandomIdleAnimationString("b"), false);	
				}
			}
			yield return new WaitForSeconds(2.0f);			
		}
	}
	
	
	string RandomIdleAnimationString(string t)
	{
		if (animationAlgorithm == 1)
		{
			int ran = UnityEngine.Random.Range(1,2+1);
		
			switch(ran)
			{
				//abcde
				
				case 1: return "pose01";
				case 2: return "pose02";
				
			}
		}
		if (animationAlgorithm == 2)
		{
			int ran = UnityEngine.Random.Range(1,4+1);
			
			if (t.Equals("b") && Globals.mainPlayer.CurrentBoyfriendId == 7) return (ran%2 == 0? "BF_pose02_b":"BF_idle_b");
			
			switch(ran)
			{
				//abcde
				
				case 1: return "BF_pose01_"+t;
				case 2: return "BF_pose02_"+t;
				default: return "BF_idle_"+t; 
				
			}
		}
		
		return "BF_idle_"+t;
	}
	
	#region InpputManager Callback
	public void OnInputManagerFemaleCharacterTouched()
	{
		if (PopupManager.PopupCount() == 0)
		{
			if (!giftUILock && !ShouldShowGift() && !HaveGiftClaim())
			{
				if (boyfriendModel != null)
				{
					OnInputManagerBoyfriendChacaterTouched();
					return;
				}
				
				Log.Debug("InputManagerFemaleCharacterTouched Received");
				string currAniName = newCharacter.GetComponent<CharacterManager>().CurrAnimationName();
				Log.Debug("Current Animation Name: " + currAniName);			
					
				int tryCount = 0;
				string changeAniName = "";
				while(tryCount < 3)
				{
					int randAni = UnityEngine.Random.Range(0,3);
					switch(randAni)
					{
						case 0:
							changeAniName = "street_hi";
							break;
						case 1:
							changeAniName = "street_rotate";
							break;
						case 2:
							changeAniName = "street_head";
							break;
					}
					
					if (!currAniName.Equals(changeAniName))
						break;
					
					tryCount++;
				}
				
				newCharacter.GetComponent<CharacterManager>().InsertAnimation(changeAniName, false);			
				newCharacter.GetComponent<CharacterManager>().QueueAnimation(RandomIdleAnimationString("g"));
			}
		}
	}
	
	public void OnInputManagerBoyfriendChacaterTouched()
	{
		if (!giftUILock && !ShouldShowGift() && !HaveGiftClaim())
		{
			Log.Debug("InputManagerBoyfriendCharacterTouched Received");
			
			newCharacter.GetComponent<CharacterManager>().InsertAnimation("BF_kiss_g",false);
			newCharacter.GetComponent<CharacterManager>().QueueAnimation(RandomIdleAnimationString("g"));
			
			boyfriendModel.GetComponent<CharacterManager>().InsertAnimation("BF_kiss_b",false);
			boyfriendModel.GetComponent<CharacterManager>().QueueAnimation(RandomIdleAnimationString("b"));
			
			kissHeartParticleSystem.Play();
		}
	}

#if UNITY_EDITOR
	public void OnInputManagerMouseClicked(Vector3 clickedPos){}
#endif
	public void OnInputManagerTouchBegan(Touch touch){}
	public void OnInputManagerTouchEnded(Touch touch){}
	public void OnInputManagerBackPressed(){}
	#endregion	
	
	
	private IEnumerator GetBoyfriendMesh(Boyfriend bf)
	{
		GameObject returnMe = null;
		
		UnityEngine.Object obj = Resources.Load("Boyfriends/"+bf.meshName);			
		if (obj == null)
		{
			// try load from streaming assets folder instead
			string filePath 	= Application.persistentDataPath + "/"  + bf.meshName +".dlc";
			FileInfo fileInfo 	= new FileInfo(filePath);			
		    if (fileInfo != null && fileInfo.Exists)
		    {
		   
				WWW bundle = new WWW("file://" + filePath);
				yield return bundle;
				
				//Log.Debug("am i here?" +item);
				if (bundle.error != null)
				{
					Log.Debug("Home Cannot load the boyfriend "+ bf.meshName + " from path: " + filePath);
				}
				else
				{
					if (bundle.assetBundle != null)
					{
						Log.Debug("Home Load boyfriend from Assetbundle");
						returnMe = (GameObject) Instantiate(bundle.assetBundle.mainAsset as GameObject);					
						bundle.assetBundle.Unload(false);
						
						//Re-assign the shaders to the boyfriend model						
						SkinnedMeshRenderer[] smrs = returnMe.GetComponentsInChildren<SkinnedMeshRenderer>();
						for (int i = 0; i < smrs.Length; i++)
						{
							SkinnedMeshRenderer smr = smrs[i];
							Material[] materials 	= smr.materials;
							for (int j = 0; j < materials.Length; j++)
							{
								Material material 	= materials[j];
								string shaderName	= material.shader.name.Replace(" ", "").Replace("/", "").Replace("-", "");
								Shader shader 		= Resources.Load("Shaders/" + shaderName) as Shader;
								if (shader != null)
									material.shader	= shader;
								else
									material.shader = Shader.Find(material.shader.name);
							}
						}
						Log.Debug("Home Assigned shader");
					}
					else
					{
						Log.Debug("Home the boyfriend "+ bf.meshName + " asset bundle is null from path: " + filePath);
					}
				}		
		    }
			else
			{
				Log.Warning("PATH FAIL:"+ bf.meshName);
			}
			
		}
		else
		{
			Log.Warning("Load From Local Path: Boyfriend: " + bf.meshName);
			returnMe = (GameObject) Instantiate(obj as GameObject);
		}
		
		boyfriendModel = returnMe;
		boyfriendModel.transform.localPosition = new Vector3(-200000f,-200000f,-200000f);

		yield return null;
	}
	

}

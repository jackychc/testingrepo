using UnityEngine;
using System.Collections;

public class PositionTweener : MonoBehaviour {
	
	public enum TWEENMODE
	{
		PASSIVE,
		LOOP,
		BACK_AND_FORTH
	}
	
	public enum CURVETYPE
	{
		STRAIGHT,
		ASCEND,
		DESCEND
	}
	
	public int id = 1;
	public float proportion = 0f;
	float sineProportion = 0f;
	bool commanded;
	bool isForward = true;
	
	public float speed = 1f;
	float fromXValue;
	public float xDisplacement;
	float fromYValue;
	public float yDisplacement;
	float fromZValue;
	public float zDisplacement;
	
	Vector3 fromVector = Vector3.zero, toVector = Vector3.zero;
	
	public TWEENMODE mode = TWEENMODE.PASSIVE;
	public CURVETYPE curveType = CURVETYPE.DESCEND;
	
	Transform mTrans;
	public Transform cachedTransform { get { if (mTrans == null) mTrans = transform; return mTrans; } }
	
	
	// Use this for initialization
	void Start () {
		fromXValue = cachedTransform.localPosition.x;
		fromYValue = cachedTransform.localPosition.y;
		fromZValue = cachedTransform.localPosition.z;
		
		fromVector = new Vector3(fromXValue, fromYValue, fromZValue);
		toVector = new Vector3(fromXValue+xDisplacement, fromYValue+yDisplacement,  fromZValue+zDisplacement);
	}
	
	public void setProportion(float v)
	{
		proportion = v;
	}
	
	public void Forward()
	{
		commanded = true;
		isForward = true;
	}
	
	public void Backward()
	{
		commanded = true;
		isForward = false;
	}
	
	void Update()
	{
		if (mode.Equals(TWEENMODE.PASSIVE) && !commanded)
			return;
			//yield break;
		
		proportion = Mathf.Max(0.0f, Mathf.Min(proportion, 1.0f));
		
		
		proportion = proportion + (isForward? 1 : -1) * speed * (1f/30);
		
		sineProportion = proportion;
		
		if (curveType == CURVETYPE.DESCEND) sineProportion = 1 - (proportion-1)*(proportion-1);
		if (curveType == CURVETYPE.ASCEND) sineProportion = (proportion)*(proportion);
		//float sineProportion = 0.00286454f + (-0.599857f)*proportion + 5.7043684f*proportion*proportion + -4.266974f*proportion*proportion*proportion;
		
		
		cachedTransform.localPosition = Vector3.Lerp(fromVector, toVector, sineProportion);
			//new Vector3( Mathf.Lerp(fromXValue, fromXValue+xDisplacement, sineProportion),
			//									Mathf.Lerp(fromYValue, fromYValue+yDisplacement, sineProportion),
			//									Mathf.Lerp(fromZValue, fromZValue+zDisplacement, sineProportion));
		
		if (mode.Equals(TWEENMODE.LOOP))
		{
			if (proportion >= 1.0f)
				proportion = 0f;
			
		}
		
		if (mode.Equals(TWEENMODE.BACK_AND_FORTH))
		{
			if (proportion <= 0f || proportion >= 1.0f)
				isForward = !isForward;
		}
		
		else if (mode.Equals(TWEENMODE.PASSIVE))
		{
			if (proportion <= 0f || proportion >= 1.0f)
				commanded = false;
		}
	}
}

using UnityEngine;
using System;
using System.Collections;

public interface TwitterManagerCallbackInterface {
	
	void OnTwitterManagerGetUserProfile(ArrayList userInfos);
	void OnTwitterManagerGetFriendIds(ArrayList friendIds);
	void OnTwitterManagerLoginFailed();
	void OnTwitterManagerLogin(string userId);
	void OnTwitterManagerLogout();
	void OnTwitterManagerError(Exception e);
	void OnTwitterManagerUploadPhoto();
	void OnTwitterManagerUploadPhotoError(string error);
	
	//Checking for the differet facebook account logged in
	void OnTwitterManagerNewUserError(Hashtable socialInfo);
	void OnTwitterManagerExistingUserError(Hashtable existingUserInfo);
}

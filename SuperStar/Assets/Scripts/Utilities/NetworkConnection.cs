using UnityEngine;
using System.Collections;
using System.Net;

public class NetworkConnection :MonoBehaviour
{
	void Start()
	{
		/*
		if (NetworkConnection.isURLReachable("http://stg-ac-api.so1.outblaze.net.obi"))
		{
			Log.Debug("Online");
		}
		else
			Log.Debug("Offline");*/
		
		State s = checkNetworkState();
		if (s == State.Fast)
			Log.Debug("Fast");
		else if (s == State.Normal)
			Log.Debug("Normal");
		else if (s == State.Slow)
			Log.Debug("Slow");
		else
			Log.Debug("Offline");
	}
	
	private static string ServerURL = "http://prod-ac-superstarstory.outblaze.net";
	private static float NormalTime = 2.0f;
	private static float FastTime = 1.0f;
	
	
	public enum State{Fast, Normal, Slow, Offline};
	
	public static bool isURLReachable(string url)
	{
		WebRequest request = HttpWebRequest.Create(url);
		request.Method = "HEAD";
		WebResponse resp = null;
		try
		{
			resp = request.GetResponse();
		}
		catch
		{
			resp = null;
		}
		
		return resp != null;
	}
	
	public static State checkNetworkState()
	{
		float currentTime = Time.realtimeSinceStartup;
		bool reachable = isURLReachable(ServerURL);
		if (reachable)
		{
			float finishTime = Time.realtimeSinceStartup;
			float timeDiff = finishTime - currentTime;
			Log.Debug("Time Diff:" + timeDiff);
			if (timeDiff <= FastTime)
				return State.Fast;
			else if (timeDiff <= NormalTime)
				return State.Normal;
			return State.Slow;			
		}
		else
		{
			return State.Offline;	
		}
	}
}

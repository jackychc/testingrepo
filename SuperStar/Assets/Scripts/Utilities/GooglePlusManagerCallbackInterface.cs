using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public interface GooglePlusManagerCallbackInterface {

	void OnGooglePlusManagerSignIn(string userId);
	void OnGooglePlusManagerSignInCancelled();
	void OnGooglePlusManagerSignInFailed(Hashtable errorObj);
	void OnGooglePlusManagerSignOut(Hashtable errorObj);
	void OnGooglePlusManagerGetFriends(ArrayList friendList);
	void OnGooglePlusManagerSharePhoto();
	void OnGooglePlusManagerSharePhotoFailed();
	void OnGooglePlusManagerError(Exception e);
	
	//Checking for the differet facebook account logged in
	void OnGooglePlusManagerNewUserError(Hashtable socialInfo);
	void OnGooglePlusManagerExistingUserError(Hashtable existingUserInfo);
}

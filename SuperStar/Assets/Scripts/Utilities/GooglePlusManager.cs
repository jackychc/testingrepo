using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class GooglePlusManager : MonoBehaviour {
	
#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _Native_GooglePlusManager_SignIn(string clientId);
	[DllImport ("__Internal")]
	private static extern void _Native_GooglePlusManager_SignOut();
	[DllImport ("__Internal")]
	private static extern void _Native_GooglePlusManager_GetProfile();
	[DllImport ("__Internal")]
	private static extern void _Native_GooglePlusManager_GetFriends();
	[DllImport ("__Internal")]
	private static extern bool _Native_GooglePlusManager_IsSignedIn();
	[DllImport ("__Internal")]
	private static extern void _Native_GooglePlusManager_SilentSignIn(string clientId);
#endif
	
	private enum Action
	{
		NONE,
		SilentSignIn,
		GetFriends,
		SharePhoto,
	}	
	private Action currentAction;
	private Hashtable stack		= null;
	private string userId 		= null;
	private bool isSilentLogout = false;
	private List<GooglePlusManagerCallbackInterface> callbackList = new List<GooglePlusManagerCallbackInterface>();
	
	public const string GooglePlusBridgeClass 	= "muneris.unity.androidbridge.google.GooglePlusBridge";
	
	private static GooglePlusManager instance = null;
	public static GooglePlusManager SharedManager{
		get{
			return instance;	
		}
	}
	
	void Awake(){
		if (instance == null)
		{
			instance = this;
			stack = new Hashtable();
			currentAction = Action.NONE;
			
			//Restore the DB's GooglePlusId
			string dbGooglePlusId = Globals.mainPlayer.googlePlusElement.socialId;
			if (dbGooglePlusId != null && !dbGooglePlusId.Equals(""))
				userId = dbGooglePlusId;
			DontDestroyOnLoad(this.transform.gameObject);
		}
		else
			Destroy(this.transform.gameObject);
	}
	// Use this for initialization
	void Start () 
	{
		if (userId != null && !userId.Equals(""))
		{
			currentAction = Action.SilentSignIn;
			SilentSignIn();
		}
	}
	
	public void Destroy()
	{
		Destroy(this.transform.gameObject);	
	}
	
	public void SetCallback(GooglePlusManagerCallbackInterface c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
			{
				callbackList.Remove(c);
			}
		}
		
		callbackList.Add(c);
	}
	
	public void RemoveCallback(GooglePlusManagerCallbackInterface c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
				callbackList.Remove(c);
		}
	}
	
	public void ReloadUserId()
	{
		string dbGooglePlusId = Globals.mainPlayer.googlePlusElement.socialId;
		if (dbGooglePlusId != null && !dbGooglePlusId.Equals(""))
			userId = dbGooglePlusId;
		else
			userId = null;
	}
	
	private IEnumerator LogFlurryEvent()
	{
		Hashtable param = new Hashtable();
		param["Connect"] = "Google+";
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Connect_SNS, param);
		
		yield return null;
	}
	
	public static string GetProfilePicURL(string googlePlusId, int width)
	{
		return "https://plus.google.com/s2/photos/profile/"+googlePlusId+"?sz="+width;
	}
	
	public void SignIn()
	{
		#if UNITY_IPHONE
			_Native_GooglePlusManager_SignIn(Globals.googlePlusClientId);	
		#elif UNITY_ANDROID
			new AndroidJavaClass(GooglePlusBridgeClass).CallStatic("signIn");
		#endif
	}
	
	public void SignOut()
	{
		#if UNITY_IPHONE
			_Native_GooglePlusManager_SignOut();
		#elif UNITY_ANDROID
			new AndroidJavaClass(GooglePlusBridgeClass).CallStatic("signOut");
		#endif
	}
	
	public bool IsSignedIn()
	{
		#if UNITY_IPHONE
			return _Native_GooglePlusManager_IsSignedIn();
		#elif UNITY_ANDROID
			return new AndroidJavaClass(GooglePlusBridgeClass).CallStatic<bool>("isSignedIn");
		#else
			return false;
		#endif
	}
	
	//#if UNITY_IPHONE
	public void SilentSignIn()
	{		
		Log.Debug("Silent SignIn");

		#if UNITY_IPHONE
			_Native_GooglePlusManager_SilentSignIn(Globals.googlePlusClientId);			
		#elif UNITY_ANDROID
			SignIn();
		#endif
	}
	//#endif
	
	public void SilentSignOut()
	{
		Log.Debug("Silent Sign out");
		isSilentLogout = true;
		SignOut();
	}
	
	public void GetProfile()
	{
		#if UNITY_IPHONE
			_Native_GooglePlusManager_GetProfile();	
		#elif UNITY_ANDROID
			new AndroidJavaClass(GooglePlusBridgeClass).CallStatic("getProfile");
		#endif
	}
	
	public void GetFriendsList()
	{
		currentAction = Action.GetFriends;
		if (!IsSignedIn())
		{
			Log.Debug("Not Signed In");
			SignIn();
			return;
		}
		
		#if UNITY_IPHONE
			_Native_GooglePlusManager_GetFriends();
		#elif UNITY_ANDROID
			new AndroidJavaClass(GooglePlusBridgeClass).CallStatic("getFriends");
		#endif
	}
	
	public void SharePhoto(string fileURL)
	{
		currentAction = Action.SharePhoto;
		if (!IsSignedIn())
		{
			stack["fileURL"] = fileURL;
			Log.Debug("Not Signed In");
			SignIn();
			return;
		}
		#if UNITY_IPHONE
				
		#elif UNITY_ANDROID
			new AndroidJavaClass(GooglePlusBridgeClass).CallStatic("sharePhotos", fileURL);
		#endif		
	}
	

	#region Callback
	//Callback From Native Code
	void onGooglePlusSignIn(string userIdJson)
	{	
		if (currentAction != Action.SilentSignIn)
		{
			GetProfile();
		}
		else
		{
			Log.Debug("Silent SignIn completed");
			//Silent Sign in
			//Reset it to NONE
			currentAction = Action.NONE;
		}
	}
	
	void onGooglePlusSignInCancelled()
	{
		if (callbackList != null)
		{
			for (int i = 0;i < callbackList.Count; i++)
			{
				GooglePlusManagerCallbackInterface c = callbackList[i];
				c.OnGooglePlusManagerSignInCancelled();
			}
		}
	}
	
	void onGooglePlusSignInFailed(string errorJson)
	{
		Hashtable errorJsonObj = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(errorJson);
		if (errorJsonObj != null)
		{
			Log.Debug("Sign in error!! " + (string)errorJsonObj["description"]);
		}
		
		if (callbackList != null)
		{
			Log.Debug("Num of GooglePlus callback: " + callbackList.Count);
			for (int i = 0;i < callbackList.Count; i++)
			{
				GooglePlusManagerCallbackInterface c = callbackList[i];
				if (errorJsonObj != null)
					c.OnGooglePlusManagerSignInFailed(errorJsonObj);
				else
					c.OnGooglePlusManagerSignInFailed(null);
			}
		}
	}
	
	void onGooglePlusSignOut(string errorJson)
	{
		Hashtable errorJsonObj = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(errorJson);
		if (errorJsonObj != null)
		{
			Log.Debug("Sign Out error!! " + (string)errorJsonObj["description"]);
		}
		
		if (!isSilentLogout)
		{
			if (callbackList != null)
			{
				for (int i = 0;i < callbackList.Count; i++)
				{
					GooglePlusManagerCallbackInterface c = callbackList[i];
					c.OnGooglePlusManagerSignOut(errorJsonObj);
				}
			}
		}
		else
			isSilentLogout = false;
	}
	
	void onGooglePlusGetProfile(string jsonObj)
	{
		Log.Debug(jsonObj);
		Hashtable userProfile = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(jsonObj);
		string currentUserId = "" + userProfile["id"];		
		
		if (userId != null && !userId.Equals(""))
		{
			if (currentUserId != null && !currentUserId.Equals(""))
			{
				if (currentUserId.Equals(userId))
				{
					if (callbackList != null)
					{
						for (int i = 0;i < callbackList.Count; i++)
						{
							GooglePlusManagerCallbackInterface c = callbackList[i];
							c.OnGooglePlusManagerSignIn(userId);
						}
					}
					
					if (currentAction != Action.NONE)
					{
						switch(currentAction)
						{
							case Action.GetFriends:
								GetFriendsList();
								break;
							case Action.SharePhoto:
								string fileURL = (string)stack["fileURL"];
								SharePhoto(fileURL);
								stack.Remove(fileURL);
								break;
							default:
								break;
						}
					}
					return;
				}
			}
		}		
		
		StartCoroutine(CheckUserAccountWithId(currentUserId, userProfile));
	}
	
	void onGooglePlusGetFriends(string jsonObj)
	{
		Log.Debug("GooglePlus jsonObj: " + jsonObj);
		Hashtable responseHT = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(jsonObj);
		ArrayList friendList = (ArrayList)responseHT["friendList"];
		
		//ArrayList friendList = (ArrayList)MiniJSON.JsonDecode(jsonObj);
		
		if (callbackList != null)
		{
			for (int i = 0;i < callbackList.Count; i++)
			{
				GooglePlusManagerCallbackInterface c = callbackList[i];
				c.OnGooglePlusManagerGetFriends(friendList);
			}
		}
	}
	
	void onGooglePlusSharePhoto()
	{
		if (callbackList != null)
		{
			for (int i = 0;i < callbackList.Count; i++)
			{
				GooglePlusManagerCallbackInterface c = callbackList[i];
				c.OnGooglePlusManagerSharePhoto();
			}
		}
	}
	
	void onGooglePlusSharePhotoFailed()
	{
		if (callbackList != null)
		{
			for (int i = 0;i < callbackList.Count; i++)
			{
				GooglePlusManagerCallbackInterface c = callbackList[i];
				c.OnGooglePlusManagerSharePhotoFailed();
			}
		}
	}
	
	#endregion
	
	#region Server API
	private IEnumerator CheckUserAccountWithId(string socialNetworkId, Hashtable userInfo)
	{
		//Prepare CheckSum
		string checkSumData = socialNetworkId + "google";
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("SearchGameSave", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("socialNetworkId", socialNetworkId);
		kvp.Add("socialNetworkType", "google");
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "SearchGameSave.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				try
				{
					Log.Debug("check response "+response);
					ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
					bool hasResponse = Boolean.Parse(((Hashtable)al[0])["response"].ToString());
					if (hasResponse)
					{
						bool newSave = false;
						bool existingSave = false;
						Hashtable serverUserInfo = (Hashtable)al[1];
						if (serverUserInfo != null)
						{
							serverUserInfo["PlayerSocialGP"] = userInfo;
							
							if (userId != null && !userId.Equals(""))
							{
								if (!socialNetworkId.Equals(userId))
									existingSave = true;
							}
							else
								existingSave = true;
						}
						else
						{
							if (userId != null && !userId.Equals(""))
							{
								if (!socialNetworkId.Equals(userId))
									newSave = true;
							}
						}
						
						if (existingSave)
						{
							//Get save Load Save With Data
							if (callbackList != null)
							{
								for (int i = 0;i < callbackList.Count; i++)
								{
									GooglePlusManagerCallbackInterface c = callbackList[i];
									c.OnGooglePlusManagerExistingUserError(serverUserInfo);
								}
							}
							
							//SilentSignOut();
						}
						else if (newSave)
						{
							//No Save
							if (callbackList != null)
							{
								for (int i = 0;i < callbackList.Count; i++)
								{
									GooglePlusManagerCallbackInterface c = callbackList[i];
									c.OnGooglePlusManagerNewUserError(userInfo);
								}
							}
							
							//SilentSignOut();
						}
						else
						{
							if (userId == null || userId.Equals(""))
								StartCoroutine(UploadUserIdToServer(socialNetworkId));
							userId = socialNetworkId;
							
							if (callbackList != null)
							{
								for (int i = 0;i < callbackList.Count; i++)
								{
									GooglePlusManagerCallbackInterface c = callbackList[i];
									c.OnGooglePlusManagerSignIn(userId);
								}
							}
							
							StartCoroutine(LogFlurryEvent());
							
							if (currentAction != Action.NONE)
							{
								switch(currentAction)
								{
									case Action.GetFriends:
										GetFriendsList();
										break;
									case Action.SharePhoto:
										string fileURL = (string)stack["fileURL"];
										SharePhoto(fileURL);
										stack.Remove(fileURL);
										break;
									default:
										break;
								}
							}
						}
					}
					else
					{
						Log.Debug("Error on the response message");
					}
				}
				catch (Exception e)
				{
					if (callbackList != null)
					{
						for (int i = 0;i < callbackList.Count; i++)
						{
							GooglePlusManagerCallbackInterface c = callbackList[i];
							c.OnGooglePlusManagerError(e);
						}
					}
				}
			}
		}
		
		
		//yield return StartCoroutine(HTTPPost.Post("SearchGameSave", kvp, result => response = result));		
	}
	
	private IEnumerator UploadUserIdToServer(string uid)
	{
		Log.Debug("GooglePlus Uploading User Id to Server");
		string socialNetworkType = "google";		
		
		GameID gameId = new GameID(Globals.mainPlayer.GameId);
		int playerId = gameId.toInt();
		string checkSumData = gameId.ID + playerId + socialNetworkType + uid;
		string checkSum = MD5Hash.GetServerCheckSumWithString("UpdatePlayerSocialFeature", checkSumData);		
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("socialNetworkType", socialNetworkType);
		kvp.Add("socialNetworkId", uid);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "UpdatePlayerSocialFeature.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al = new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				bool responseTag = Boolean.Parse(((Hashtable)al[0])["response"].ToString());
				if (responseTag)
				{
					Log.Debug("GooglePlus Uploaded the User to the Server");
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("UpdatePlayerSocialFeature", kvp, result => response = result));
	}
	#endregion
}

using UnityEngine;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Net;
using System.IO;

public class TwitterManager : MonoBehaviour {
	private enum Action
	{
		NONE,
		Login,
		GetUserProfile,
		GetFriendIds,
		PostStatus,
		PostStatusWithImage,
	}

	private string oauth_access_token 			= null;
	private string oauth_access_token_secret 	= null;
	private string userName 					= null;
	private string password 					= null;
	private string userId 						= null;
	private Hashtable userInfo 					= null;	
	private bool isSilentLogout 				= false;
	
	private Action currentAction 								= Action.NONE;
	private Dictionary<string, object> paramList 				= new Dictionary<string, object>();	
	private List<TwitterManagerCallbackInterface> callbackList 	= new List<TwitterManagerCallbackInterface>();
	
	private static TwitterManager instance = null;
	public static TwitterManager SharedManager
	{
		get{
			return instance;	
		}
	}
	
	public string UserID
	{
		get{
			return userId;	
		}
	}
	
	public string AccessToken
	{
		get{
			return oauth_access_token;	
		}
	}
	
	public string AccessTokenSecret
	{
		get{
			return oauth_access_token_secret;	
		}
	}
	
	public string UserName
	{
		set{
			userName = value;	
		}
		
		get{
			return userName;	
		}
	}
	
	public string Password
	{
		set{
			password = value;	
		}
		
		get{
			return password;	
		}
	}
	
	public void SetCallback(TwitterManagerCallbackInterface c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
			{
				callbackList.Remove(c);
			}
			
			callbackList.Add(c);
		}
	}
	
	public void RemoveCallback(TwitterManagerCallbackInterface c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
				callbackList.Remove(c);
		}
	}
	
	private void ResetCredentials()
	{
		userName = null;
		password = null;
	}
	
	void Awake()
	{
		if (instance == null)
		{
			instance = this;
			
			//Restore the DB's TwitterId
			string dbTwitterId = Globals.mainPlayer.twitterElement.socialId;
			if (dbTwitterId != null && !dbTwitterId.Equals(""))
				userId = dbTwitterId;
			
			string dbTwitterLoginName = Globals.mainPlayer.twitterElement.loginName;
			if (dbTwitterLoginName != null && !dbTwitterLoginName.Equals(""))
				userName = dbTwitterLoginName;
			
			string dbTwitterPassword = Globals.mainPlayer.twitterElement.loginPassword;
			if (dbTwitterPassword != null && !dbTwitterPassword.Equals(""))
				password = dbTwitterPassword;
			
			DontDestroyOnLoad(this.transform.gameObject);
			Log.Debug("TwitterManager init");
		}
		else
			Destroy(this.transform.gameObject);
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	public void Destroy()
	{
		Destroy(this.transform.gameObject);	
	}
	
	void OnApplicationQuit()
	{
		instance = null;
	}
	
	private IEnumerator LogFlurryEvent()
	{
		Hashtable param = new Hashtable();
		param["Connect"] = "Twitter";
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Connect_SNS, param);
		
		yield return null;
	}
	
	public void SilentLogout()
	{
		isSilentLogout = true;
		Logout();
		isSilentLogout = false;
	}
	
	public void Logout()
	{
		oauth_access_token = null;
		oauth_access_token_secret = null;
		userName = null;
		password = null;

		if (!isSilentLogout)
		{
			if (callbackList != null)
			{
				for (int i = 0;i < callbackList.Count; i++)
				{
					TwitterManagerCallbackInterface c = callbackList[i];
					c.OnTwitterManagerLogout();
				}
			}
		}
	}
	
	public bool IsLoggedInAndValidate()
	{
		return (userName != null && password != null && oauth_access_token != null && oauth_access_token_secret != null);
	}
	
	public bool IsLoggedIn()
	{
		return (userName != null && password != null);
	}
	
	public void Login()
	{
		StartCoroutine(ObtainAccessToken());
	}
	
	public void ReloadUserId()
	{
		//Restore the DB's TwitterId
		string dbTwitterId 			= Globals.mainPlayer.twitterElement.socialId;
		if (dbTwitterId != null && !dbTwitterId.Equals(""))
			userId = dbTwitterId;
		else
			userId = null;
		
		Globals.mainPlayer.twitterElement.loginName		= userName;
		Globals.mainPlayer.twitterElement.loginPassword = password;
	}
	/*
	public IEnumerator EnumLoginIn()
	{
		
		
		
		if ((oauth_access_token == null || oauth_access_token.Equals("")) || (oauth_access_token_secret == null || oauth_access_token_secret.Equals("")))
			yield return StartCoroutine(ObtainAccessToken());
		else
		{
			bool valid = false;
			yield return StartCoroutine(CheckAccessToken(isValid => valid = isValid));
			if (!valid)
				yield return StartCoroutine(ObtainAccessToken());
			else
			{
				//The token is valid
				//Success Sign in
				if (callback != null)
					callback.OnTwitterManagerLogin(userId);
			}
		}
	}*/
	
	private string CreateAuthorization(string method, string url, string oauthToken, string callback, bool withOauthAccessTokenSecret, bool isMultipart, Dictionary<string, string> param)
	{
		string oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
		string oauth_signature = "";
		string oauth_signature_method = "HMAC-SHA1";
		TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0);
		string oauth_timestamp = Convert.ToInt64(ts.TotalSeconds).ToString();
		string oauth_version = "1.0";
		
		SortedDictionary<string, string> sd = new SortedDictionary<string, string>();
		if (callback != null)
			sd.Add("oauth_callback", callback);
		sd.Add("oauth_consumer_key", Globals.twitterConsumerKey);
		sd.Add("oauth_nonce", oauth_nonce);
		sd.Add("oauth_signature_method", oauth_signature_method);
		sd.Add("oauth_timestamp", oauth_timestamp);
		if (oauthToken != null)
			sd.Add("oauth_token", oauthToken);
		sd.Add("oauth_version", oauth_version);
		//if (status != null && !isMultipart)
			//sd.Add("status", Uri.EscapeDataString(status));
		
		//Create the base string
		string baseString = String.Empty;
		baseString += method + "&";
		baseString += Uri.EscapeDataString(url) + "&";		
		foreach (KeyValuePair<string, string> entry in sd)
		{
			baseString += Uri.EscapeDataString(entry.Key + "=" + entry.Value + "&");
		}
		
		//Params
		if (!isMultipart)
		{
			if (param != null && param.Count > 0)
			{
				foreach (string key in param.Keys)
				{
					string values = Uri.EscapeDataString("" + param[key]);
					baseString += Uri.EscapeDataString(key + "=" + values + "&");
				}				
			}
		}		
		
		baseString = baseString.Substring(0, baseString.Length - 3);
		Log.Debug(baseString);
		
		string signingKey = Uri.EscapeDataString(Globals.twitterConsumerSecret) + "&" + ((withOauthAccessTokenSecret)? Uri.EscapeDataString(oauth_access_token_secret):"");
		HMACSHA1 hmac = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));
		oauth_signature = Convert.ToBase64String(hmac.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));		
		string authorization = "OAuth " +
									   "oauth_nonce=\"" +Uri.EscapeDataString(oauth_nonce)+ "\"," + 
				((callback != null)?"oauth_callback=\"" +Uri.EscapeDataString(callback)+ "\",": "")+
							"oauth_signature_method=\"" +Uri.EscapeDataString(oauth_signature_method)+ "\"," +
								   "oauth_timestamp=\"" +Uri.EscapeDataString(oauth_timestamp)+ "\"," +
								"oauth_consumer_key=\"" +Uri.EscapeDataString(Globals.twitterConsumerKey)+ "\"," +	
				((oauthToken != null)? "oauth_token=\"" + Uri.EscapeDataString(oauthToken)+ "\",": "")+
								   "oauth_signature=\"" +Uri.EscapeDataString(oauth_signature)+ "\"," +
								"oauth_version=\"" +oauth_version+ "\"";
		
		Log.Debug(authorization);
		return authorization;		
	}
	
	
	private void ResumeCurrentAction()
	{
		switch(currentAction)
		{
			case Action.GetFriendIds:
			{
				GetFriendsId();
				break;
			}
			case Action.GetUserProfile:
			{
				ArrayList userIds = (ArrayList)paramList["userIds"];
				GetUserProfile(userIds);
				break;
			}
			case Action.PostStatus:
			{
				string status = (string)paramList["status"];
				PostStatus(status);
				break;
			}
			case Action.PostStatusWithImage:
			{
				string status = (string)paramList["status"];
				string filePath = (string)paramList["filePath"];
				PostStatusWithImage(status, filePath);
				break;
			}
			default:
			{
				Log.Debug("Default Resume action");
				break;
			}
		}		
	}
		
	private void ResetCurrentAction()
	{
		currentAction = Action.NONE;
	}
	
	#region Login
	/*
	private IEnumerator CheckAccessToken(Action<bool> isValid)
	{
		string verifyCredentialsURL = "https://api.twitter.com/1.1/account/verify_credentials.json";
		string authorization = CreateAuthorization("GET", verifyCredentialsURL, oauth_access_token, null, true, null, false);
		var request = new HTTP.Request("GET", verifyCredentialsURL);
		request.headers.Set("Authorization", authorization);
		request.headers.Set("Content-Type", "application/x-www-form-urlencoded");
		request.Send();
		
		while(!request.isDone)
			yield return null;
		
		int statusCode = request.response.status;
		bool isOk = request.response.message.Equals("OK");
		if (statusCode == 200 && isOk){
			string responseText = request.response.Text;
			Hashtable responseHT = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(responseText);
			if (responseHT.ContainsKey("id"))
			{
				//Check userId when login
				string currentUserId = "" + responseHT["id"];				
				if (userId != null && !userId.Equals(""))
				{
					if (currentUserId != null && !currentUserId.Equals(""))
					{
						if (!currentUserId.Equals(userId))
						{
							Log.Debug("New User Detected");
							Log.Debug("Check the userId have game save or not");
					
							StartCoroutine(CheckUserAccountWithId(currentUserId));
							isValid(true);
							yield break;
						}
					}
					else
						Log.Debug("Twitter Manager CheckAccessToken the TwitterId is NULL!!!!");
				}
				
				if (userId == null || userId.Equals(""))
					StartCoroutine(UploadUserIdToServer(currentUserId));
				userId = "" + responseHT["id"];				
				if (callback != null)
					callback.OnTwitterManagerLogin(userId);
			}
			isValid(true);
		}
		else{
			isValid(false);
		}
		
	}
	*/
	
	private IEnumerator ObtainRequestToken()
	{
		string requestURL = "https://api.twitter.com/oauth/request_token";
		
		//Prepare the signature then generate the authorization
		string authorization = CreateAuthorization("POST", requestURL, null, "oob", false, false, null);		
		Hashtable headers = new Hashtable();
		headers.Add("Authorization", authorization);
		headers.Add("Content-Type", "application/x-www-form-urlencoded");		
		WWW www = new WWW(requestURL, new byte[]{ (byte) 0}, headers);
		yield return www;
		
		if (www.error != null)
		{
			Log.Debug("Cannot retrieve request token from Twitter!");
			Log.Debug(www.error);
			ResetCredentials();
			
			if (callbackList != null)
			{
				for (int i = 0;i < callbackList.Count; i++)
				{
					TwitterManagerCallbackInterface c = callbackList[i];
					c.OnTwitterManagerLoginFailed();
				}
			}
		}
		else
		{
			//Get the request Token
			Hashtable responseKeyValues = new Hashtable();
			string response = www.text;
			string[] responseRows = response.Split('&');
			foreach (string row in responseRows)
			{
				string[] keyValues = row.Split('=');
				responseKeyValues.Add(keyValues[0], keyValues[1]);
			}
			
			if (responseKeyValues.ContainsKey("oauth_token") && responseKeyValues.ContainsKey("oauth_token_secret"))
			{
				string oauth_token = (string)responseKeyValues["oauth_token"];
				
				StartCoroutine(AuthenticateRequestToken(oauth_token));
			}
			else
			{
				Log.Debug("The response Text does not contain oauth_token!!");	
			}			
		}			
		yield return null;
	}
	
	private IEnumerator AuthenticateRequestToken(string oauth_token)
	{
		string authenticateURL = "https://api.twitter.com/oauth/authenticate?oauth_token=" + oauth_token;
		
		Log.Debug(authenticateURL);
		WWW www = new WWW(authenticateURL);
		yield return www;
		
		if (www.error != null)
		{
			Log.Debug("Cannot show the authentication");
			Log.Debug(www.error);
			ResetCredentials();
			
			if (callbackList != null)
			{
				for (int i = 0;i < callbackList.Count; i++)
				{
					TwitterManagerCallbackInterface c = callbackList[i];
					c.OnTwitterManagerLoginFailed();
				}
			}
		}
		else
		{
			Log.Debug(www.text);
			string content = www.text;
			//Extract the possible info from the login page then send submit the form request to get the pin
			string authenticity_token = Regex.Match(content, "twttr\\.form_authenticity_token = '([^']+)';", RegexOptions.IgnoreCase).Groups[1].Value;					
			Log.Debug(authenticity_token);
			
			WWWForm form = new WWWForm();
			form.AddField("authenticity_token", authenticity_token);
			form.AddField("oauth_token", oauth_token);
			form.AddField("session[username_or_email]", userName);
			form.AddField("session[password]", password);
			form.AddField("Allow", "allow");
			
			www = new WWW(authenticateURL, form);
			yield return www;
			
			if (www.error != null)
			{
				Log.Debug("Cannot send the form");
				Log.Debug(www.error);
				ResetCredentials();
				
				if (callbackList != null)
				{
					for (int i = 0;i < callbackList.Count; i++)
					{
						TwitterManagerCallbackInterface c = callbackList[i];
						c.OnTwitterManagerLoginFailed();
					}
				}
			}
			else 
			{
				Log.Debug(www.text);
				content = www.text;
				
				Match m = Regex.Match(content, "<code>\\s*([0-9]+)", RegexOptions.IgnoreCase);
				if (m != null)
				{
					//pin is here
					string pin = m.Groups[1].Value;
					Log.Debug(pin);
					
					StartCoroutine(AuthenticateAccessToken(oauth_token ,pin));					
				}
				else
					Log.Debug("Cannot retrieve the pin code!!!");
				
			}		
		}
	}
	
	private IEnumerator AuthenticateAccessToken(string oauth_token, string pin)
	{
		string accessTokenURL = "https://api.twitter.com/oauth/access_token";
		
		string authorization = CreateAuthorization("POST", accessTokenURL, oauth_token, null, false, false, null);				
		Hashtable headers = new Hashtable();
		headers.Add("Authorization", authorization);
		headers.Add("Content-Type", "application/x-www-form-urlencoded");							
		
		WWWForm form = new WWWForm();
		form.AddField("oauth_verifier", pin);
		
		WWW www = new WWW(accessTokenURL, form.data, headers);
		yield return www;
		
		if (www.error != null)
		{
			Log.Debug("Cannot verify the pin");
			Log.Debug(www.error);
			ResetCredentials();
			
			if (callbackList != null)
			{
				for (int i = 0;i < callbackList.Count; i++)
				{
					TwitterManagerCallbackInterface c = callbackList[i];
					c.OnTwitterManagerLoginFailed();
				}
			}
		}
		else
		{
			Log.Debug(www.text);
			Hashtable responseKeyValues = new Hashtable();
			string response = www.text;
			string[] responseRows = response.Split('&');
			foreach (string row in responseRows)
			{
				string[] keyValues = row.Split('=');
				responseKeyValues.Add(keyValues[0], keyValues[1]);
			}
			
			if (responseKeyValues.ContainsKey("oauth_token") && responseKeyValues.ContainsKey("oauth_token_secret") && responseKeyValues.ContainsKey("user_id"))
			{
				string currentUserId = (string)responseKeyValues["user_id"];
				oauth_access_token = (string)responseKeyValues["oauth_token"];
				oauth_access_token_secret = (string)responseKeyValues["oauth_token_secret"];				
				Log.Debug(oauth_access_token);
				
				//Get the userInfo
				StartCoroutine(OnTwitterLogin(currentUserId));
			}
			else
			{
				Log.Debug("Cannot obtain the oauth_token and oauth_token_secret");
				ResetCredentials();
				
				if (callbackList != null)
				{
					for (int i = 0;i < callbackList.Count; i++)
					{
						TwitterManagerCallbackInterface c = callbackList[i];
						c.OnTwitterManagerLoginFailed();
					}
				}
			}
		}		
	}
	
	private IEnumerator ObtainAccessToken()
	{
		if (userName == null || userName.Equals("") || password == null || password.Equals(""))
			yield break;
		
		StartCoroutine(ObtainRequestToken());
	}
	#endregion
	
	#region API
	public void GetFriendsId()
	{
		currentAction = Action.GetFriendIds;
		if (!IsLoggedInAndValidate())
		{
			Login();
			return;
		}
		
		StartCoroutine(EnumGetFriendIds());	
	}
	
	public IEnumerator EnumGetFriendIds()
	{		
		Log.Debug("Get the friendsIds");
		
		ArrayList friendList = new ArrayList();
		string getFriendIdsURL = "https://api.twitter.com/1.1/friends/ids.json";
		string authorization = CreateAuthorization("GET", getFriendIdsURL, oauth_access_token, null, true, false, null);
		var request = new HTTP.Request("GET", getFriendIdsURL);
		request.headers.Set("Authorization", authorization);
		request.headers.Set("Content-Type", "application/x-www-form-urlencoded");
		request.Send();
		while(!request.isDone)
			yield return null;
		
		int statusCode = request.response.status;
		bool isOk = request.response.message.Equals("OK");		
		if (statusCode == 200 && isOk)
		{
			if (request.response.Bytes.Length > 0)
			{
				//Extract the ids
				Log.Debug(request.response.Text);
				Hashtable friendsHT = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(request.response.Text);
				if (friendsHT != null)
				{
					ArrayList ids = (ArrayList)friendsHT["ids"];
					if (ids != null)
						friendList = ids;
				}
				
				
				//Return ids 
				
				/*
				for (int i = 0; i < ids.Count; i++)
				{
					string id = "" + ids[i];
					Log.Debug(id);
				}
				*/
				//Log.Debug(request.response.Text);
				//Log.Debug(text);
			}
		}
		
		if (callbackList != null)
		{
			for (int i = 0;i < callbackList.Count; i++)
			{
				TwitterManagerCallbackInterface c = callbackList[i];
				c.OnTwitterManagerGetFriendIds(friendList);
			}
		}
		
		ResetCurrentAction();
	}
	
	public void GetUserProfile(ArrayList userIds)
	{
		currentAction = Action.GetUserProfile;
		if (!IsLoggedInAndValidate())
		{
			paramList["userIds"] = userIds;
			Login();
			return;
		}
		
		StartCoroutine(EnumGetUserProfile(userIds, false));
	}
	
	public IEnumerator EnumGetUserProfile(ArrayList userIds, bool isLoginCheck)
	{	
		string userIdStr = "";
		for (int i = 0; i < userIds.Count; i++)
		{
			string id = (string)userIds[i];
			if (i != userIds.Count - 1)
				userIdStr += id + ",";
			else
				userIdStr += id;
		}
		
		Log.Debug(oauth_access_token);
		Log.Debug(oauth_access_token_secret);
		Log.Debug(userIdStr);
			
		string getUserProfileURL = "https://api.twitter.com/1.1/users/lookup.json";
		
		Dictionary<string, string> paramDict = new Dictionary<string, string>();
		paramDict.Add("user_id", userIdStr);
		string authorization = CreateAuthorization("GET", getUserProfileURL, oauth_access_token, null, true, false, paramDict);
		
		//The get params is not included in the authorization
		getUserProfileURL += "?user_id=" + userIdStr;
		
		Log.Debug(getUserProfileURL);
		
		var request = new HTTP.Request("GET", getUserProfileURL);
		request.headers.Set("Authorization", authorization);
		request.headers.Set("Content-Type", "application/x-www-form-urlencoded");
		request.Send();	
		while(!request.isDone)
			yield return null;
		
		int statusCode = request.response.status;
		bool isOk = request.response.message.Equals("OK");
		if (statusCode == 200 && isOk)
		{
			Log.Debug(request.response.Text);
			ArrayList respArr = (ArrayList)SuperstarFashionMiniJSON.JsonDecode(request.response.Text);
			if (respArr != null && respArr.Count > 0)
			{
				if (!isLoginCheck)
				{
					if (callbackList != null)
					{
						for (int i = 0;i < callbackList.Count; i++)
						{
							TwitterManagerCallbackInterface c = callbackList[i];
							c.OnTwitterManagerGetUserProfile(respArr);
						}
					}
				}
				else
				{
					if (respArr != null)
					{
						userInfo = (Hashtable)respArr[0];
					}
					else
						Log.Debug("Cannot fetch the userInfo!!!!");
				}
			}
		}
		else
		{
			Log.Debug(request.response.message);
			Log.Debug(request.response.status);
		}		
		
		if (currentAction == Action.GetUserProfile)
			ResetCurrentAction();
	}
	
	public void PostStatus(string status)
	{
		currentAction = Action.PostStatus;
		if (!IsLoggedInAndValidate())
		{
			paramList["status"] = status;
			Login();
			return;
		}
		
		StartCoroutine(EnumPostStatus(status));	
	}
	
	public IEnumerator EnumPostStatus(string status)
	{		
		Log.Debug("Update Status");
				
		string updateStatusURL = "http://api.twitter.com/1.1/statuses/update.json";
		Dictionary<string, string> paramDict = new Dictionary<string, string>();
		paramDict.Add("status", status);
		string authorization = CreateAuthorization("POST", updateStatusURL, oauth_access_token, null, true, false, paramDict);
		
		Hashtable headers = new Hashtable();
		headers.Add("Authorization", authorization);
		headers.Add("Content-Type", "application/x-www-form-urlencoded");
		
		//updateStatusURL += "?status=" + escapedCaption;
		
		WWWForm form = new WWWForm();
		form.AddField("status", status);
		
		WWW www = new WWW(updateStatusURL, form.data, headers);
		yield return www;
		
		if (www.error != null)
		{
			Log.Debug("Error!!!: " + www.error);
		}
		else
		{		
			string response = www.text;
			Log.Debug("Success!");
			Log.Debug(response);
		}
		
		ResetCurrentAction();
	}
	
	public void PostStatusWithImage(string status, string filePath)
	{
		currentAction = Action.PostStatusWithImage;
		if (!IsLoggedInAndValidate())
		{
			Log.Debug("Post Status with image not logged in");
			paramList["status"] 	= status;
			paramList["filePath"] 	= filePath;
			Login();
			return;
		}
		
		StartCoroutine(EnumPostStatusWithImage(status, filePath));	
	}
	
	public IEnumerator EnumPostStatusWithImage(string status, string filePath)
	{		
		Log.Debug("Update Status with image");
		
		var data = System.IO.File.ReadAllBytes(filePath);		
		string updateStatusURL = "http://api.twitter.com/1.1/statuses/update_with_media.json";
		Dictionary<string, string> paramDict = new Dictionary<string, string>();
		paramDict.Add("status", status);
		string authorization = CreateAuthorization("POST", updateStatusURL, oauth_access_token, null, true, true, paramDict);

		Log.Debug(authorization);		
		
		string date = System.DateTime.UtcNow.ToUniversalTime().ToString("r");
		WWWForm form = new WWWForm();
		form.AddField("status", status);
		form.AddBinaryData("media[]", data, "2.png", "image/png");		
		
		string dataString = Encoding.UTF8.GetString(form.data);
		//dataString = dataString.Replace("Content-Type: text/plain; charset=\"utf-8\"\r\n", "");
		Log.Debug(dataString);
		byte[] formData = Encoding.UTF8.GetBytes(dataString);
		
		Hashtable ret = new Hashtable();		
		foreach (string key in form.headers.Keys)
		{
			ret[key] = form.headers[key];	
		}
		ret["Authorization"] = authorization;
		//ret["Connection"] = "Keep-Alive";
		//ret["Expect"] = "";
		//ret["Accept-Encoding"] = "gzip,deflate,sdch";
		//ret["Accept-Language"] = "en-US,en;q=0.8";
		//ret["Content-Length"] = "" + formData.Length;
		ret["Date"] = date;
		//ret["Accept"] = "*/*";
		//ret["Proxy-Connection"] = "Keep-Alive";
		//ret["User-Agent"] = "Http Client";
		//ret["Cache-Control"] = "no-cache";
		
		
		Log.Debug(ret.toJson());
		
		
		WWW www = new WWW(updateStatusURL, form.data, ret);		
		yield return www;
		if (www.error != null)
		{
			Log.Debug("TwitterManager Error!!!: " + www.error);
			
			if (callbackList != null)
			{
				for (int i = 0;i < callbackList.Count; i++)
				{
					TwitterManagerCallbackInterface c = callbackList[i];
					c.OnTwitterManagerUploadPhotoError(www.error);
				}
			}
		}
		else
		{
			string response = www.text;
			Log.Debug(response);
			Hashtable respHash = (Hashtable)SuperstarFashionMiniJSON.JsonDecode(response);
			if (respHash != null)
			{
				Log.Debug(respHash);
				string createdAt = (string)respHash["created_at"];
				if (createdAt != null)
				{
					if (callbackList != null)
					{
						for (int i = 0;i < callbackList.Count; i++)
						{
							TwitterManagerCallbackInterface c = callbackList[i];
							c.OnTwitterManagerUploadPhoto();
						}
					}
					
					Log.Debug("Upload Twitter Success!!!");
				}
			}
			//Log.Debug(response);
		}
		
		
		/*
		var request = new HTTP.Request("POST", updateStatusURL, formData);
		//Hashtable ret = new Hashtable();
		
		foreach (string key in ret.Keys)
		{
			request.headers.Add(key, (string)ret[key]);
		}
		request.Send();	
		
		while(!request.isDone)
			yield return null;
		
		int statusCode = request.response.status;
		bool isOk = request.response.message.Equals("OK");
		if (statusCode == 200 && isOk){
			Log.Debug(request.response.Text);
		}
		else{
			Log.Debug(request.response.message);
			Log.Debug(request.response.status);
		}	
		*/
		
		ResetCurrentAction();
	}
	#endregion
	
	#region callback
	private IEnumerator OnTwitterLogin(string currentUserId)
	{		
		Log.Debug("Start Get User Profile");
		yield return StartCoroutine(EnumGetUserProfile(new ArrayList(){currentUserId}, true));
		
		Log.Debug("Get User Profile Done");
		if (userInfo != null)
		{
			if (userId != null && !userId.Equals(""))
			{
				if (currentUserId != null && !currentUserId.Equals(""))
				{
					if (currentUserId.Equals(userId))
					{
						Log.Debug("Twitter Ids are equal!!");
						Log.Debug("No need to check with server account");
						userId = currentUserId;
						
						//Save the twitter userName + password
						Globals.mainPlayer.twitterElement.loginName 	= userName;
						Globals.mainPlayer.twitterElement.loginPassword = password;
						
						if (!currentAction.Equals(Action.NONE) && !currentAction.Equals(Action.Login))
						{
							Log.Debug("OnTwitterLogin currentAction: " + currentAction);
							ResumeCurrentAction();
						}
						else
						{
							Log.Debug("OnTwitterLogin currentAction: " + currentAction);
						
							if (callbackList != null)
							{
								for (int i = 0;i < callbackList.Count; i++)
								{
									TwitterManagerCallbackInterface c = callbackList[i];
									c.OnTwitterManagerLogin(userId);
								}
							}
						}
						
						yield break;
					}
				}
				else
					Log.Debug("Twitter Manager CheckAccessToken the TwitterId is NULL!!!!");
			}
			
			StartCoroutine(CheckUserAccountWithId(currentUserId));			
		}
		else
			Log.Debug("Cannot fetch the userInfo!!!!");		
	}	
	#endregion
	
	
	#region Server API
	private IEnumerator CheckUserAccountWithId(string socialNetworkId)
	{
		Log.Debug("CheckUserAccount With Id: " + socialNetworkId);
		
		
		//Prepare CheckSum
		string checkSumData = socialNetworkId + "twitter";
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("SearchGameSave", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("socialNetworkId", socialNetworkId);
		kvp.Add("socialNetworkType", "twitter");
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "SearchGameSave.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				try
				{
					Log.Debug("check response "+response);
					ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
					bool hasResponse = Boolean.Parse(((Hashtable)al[0])["response"].ToString());
					if (hasResponse)
					{
						bool newSave = false;
						bool existingSave = false;
						Hashtable serverUserInfo = (Hashtable)al[1];
						if (serverUserInfo != null)
						{
							Log.Debug("Get account from Server");
							serverUserInfo["PlayerSocialTW"] = userInfo;
							
							if (userId != null && userId.Equals(""))
							{
								if (!socialNetworkId.Equals(userId))
									existingSave = true;
							}
							else
								existingSave = true;
						}
						else
						{
							if (userId != null && !userId.Equals(""))
							{
								if (!socialNetworkId.Equals(userId))
									newSave = true;
							}						
						}
						
						if (newSave)
						{
							if (callbackList != null)
							{
								for (int i = 0;i < callbackList.Count; i++)
								{
									TwitterManagerCallbackInterface c = callbackList[i];
									c.OnTwitterManagerNewUserError(userInfo);
								}
							}
							
							//SilentLogout();
						}
						else if (existingSave)
						{
							if (callbackList != null)
							{
								for (int i = 0;i < callbackList.Count; i++)
								{
									TwitterManagerCallbackInterface c = callbackList[i];
									c.OnTwitterManagerExistingUserError(serverUserInfo);
								}
							}
							
							//SilentLogout();
						}
						else
						{
							if (userId == null || userId.Equals(""))
								StartCoroutine(UploadUserIdToServer(socialNetworkId));
							
							StartCoroutine(LogFlurryEvent());
							
							userId = socialNetworkId;
							
							//Save the twitter userName + password
							Globals.mainPlayer.twitterElement.loginName 	= userName;
							Globals.mainPlayer.twitterElement.loginPassword = password;
							
							if (!currentAction.Equals(Action.NONE) && !currentAction.Equals(Action.Login))
							{
								ResumeCurrentAction();
							}
							else
							{							
								if (callbackList != null)
								{
									for (int i = 0;i < callbackList.Count; i++)
									{
										TwitterManagerCallbackInterface c = callbackList[i];
										c.OnTwitterManagerLogin(userId);
									}
								}
							}
						}
					}
					else
					{
						Log.Debug("Error on the response message");
					}
				}
				catch (Exception e)
				{
					ResetCredentials();
					if (callbackList != null)
					{
						for (int i = 0;i < callbackList.Count; i++)
						{
							TwitterManagerCallbackInterface c = callbackList[i];
							
							if (currentAction.Equals(Action.Login))
								c.OnTwitterManagerLoginFailed();
							else
								c.OnTwitterManagerError(e);
						}
					}
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("SearchGameSave", kvp, result => response = result));
	}
	
	private IEnumerator UploadUserIdToServer(string uid)
	{
		Log.Debug("Twitter Uploading User Id to Server");
		string socialNetworkType = "twitter";		
		
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + socialNetworkType + uid;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("UpdatePlayerSocialFeature", checkSumData);		
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("socialNetworkType", socialNetworkType);
		kvp.Add("socialNetworkId", uid);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "UpdatePlayerSocialFeature.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al = new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				bool responseTag = Boolean.Parse(((Hashtable)al[0])["response"].ToString());
				if (responseTag)
				{
					Log.Debug("Twitter Uploaded the User to the Server");
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("UpdatePlayerSocialFeature", kvp, result => response = result));			
	}
	#endregion
}

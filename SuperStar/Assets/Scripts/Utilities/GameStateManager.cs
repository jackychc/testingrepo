using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameStateManager : MonoBehaviour{

	// Use this for initialization
	
	private static GameStateManager instance;
	public static GameStateManager SharedManager 
	{
		get{
			return instance;	
		}
	}
	
	void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.transform.gameObject);
		}
		else
			Destroy(this);
	}
	
	void Start()
	{
		//Init DC Banner System
		EventSystemController.OnQueryCompleted 				+= HandleOnQueryCompleted;
		EventSystemController.OnEventBannerLoadCompleted 	+= HandleOnEventBannerLoadCompleted;
		EventSystemController.OnEventBannerLoadFailed 		+= HandleOnEventBannerLoadFailed;
	}
	
	// Update is called once per frame
	void Update() 
	{
	}
	
	void OnDestroy()
	{
		EventSystemController.OnQueryCompleted 				-= HandleOnQueryCompleted;
		EventSystemController.OnEventBannerLoadCompleted 	-= HandleOnEventBannerLoadCompleted;
		EventSystemController.OnEventBannerLoadFailed 		-= HandleOnEventBannerLoadFailed;		
	}
	
	public bool CheckToShowinterstitial(Scenes s)
	{
		#if UNITY_EDITOR
			return false;
		#endif
		
		if (Globals.canShowAds)
		{
			bool enabledDCBanner = GetCargoBoolForKey("ads_enable_dc_banner_system", false);
			if (enabledDCBanner)
			{
				string proportionStr = GetCargoStringForKey("ads_featured_dc_banner_random_ratio");
				
				if (proportionStr != null && !proportionStr.Equals(""))
				{
					string[] proportion = proportionStr.Split(':');
					if (proportion != null && proportion.Length > 0)
					{
						//Calculate the total proportion
						int featuredProportion = int.Parse(proportion[0]);
						int dcBannerProportion = int.Parse(proportion[1]);
						int totalProportion = featuredProportion + dcBannerProportion;
						int randNum = UnityEngine.Random.Range(1, totalProportion);
						
						if (randNum > 0 && randNum <= featuredProportion)
						{
							ShowInterstitial(s);
						}
						else
						{
							ShowDCBanner(s);
						}
					}
				}
			}
			else
			{
				ShowInterstitial(s);
			}					
			Globals.takeoverShownCount++;
			
			return true;
		}
		return false;
	}
	
	private void ShowInterstitial(Scenes s)
	{
		int showInsCount	= GetCargoIntForKey("ads_show_featured_count_per_sesssion", -1);
		string scenesIdsStr = GetCargoStringForKey("ads_show_featured_location");
		bool isAbleToShow 	= s.Equals(Scenes.STREET);
		
		if (scenesIdsStr != null && !scenesIdsStr.Equals(""))
		{
			List<string> scenesIds = new List<string>(scenesIdsStr.Split(','));
			if (scenesIds.Contains("" + (int)s))
			{
				isAbleToShow = true;
			}
		}
		else
		{
			if (s.Equals(Scenes.STREET))
			{
				isAbleToShow = true;
			}
		}
		
		if (isAbleToShow)
		{
			if (showInsCount == -1 || Globals.takeoverShownCount < showInsCount)
			{
				MunerisManager.SharedManager.ShowFeatured();
			}
		}
	}
	
	private void ShowDCBanner(Scenes s)
	{
		int showInsCount	= GetCargoIntForKey("ads_show_featured_count_per_sesssion", -1);
		string scenesIdsStr = GetCargoStringForKey("ads_show_dcbanner_location");
		bool isAbleToShow 	= false;
		
		if (scenesIdsStr != null && !scenesIdsStr.Equals(""))
		{
			List<string> scenesIds = new List<string>(scenesIdsStr.Split(','));
			if (scenesIds.Contains("" + (int)s))
			{
				isAbleToShow = true;
			}
		}
		else
		{
			if (s.Equals(Scenes.STREET))
			{
				isAbleToShow = true;
			}
		}
		
		if (isAbleToShow)
		{
			if (showInsCount == -1 || Globals.takeoverShownCount < showInsCount)
			{
				#if UNITY_ANDROID
				EventSystemController.GetInstance().appBundleID		= Globals.packageName;
				#elif UNITY_IPHONE
				EventSystemController.GetInstance().appBundleID		= Globals.bundleId;
				#endif
		
				string appVersion = UtilsHelper.GetAppVersionFromBundleVersion(Globals.bundleVersion);
				EventSystemController.GetInstance().appVersion = appVersion;
				EventSystemController.GetInstance().start();
			}
		}
	}
	
	#region EventSystem Event Handler
	public void HandleOnEventBannerLoadCompleted (Texture2D texture, string bannerLink){
		if (texture != null){
			Debug.Log("Banner loaded!!");
			PopupManager.ShowDCBannerPopup(bannerLink, texture, true);
		}
	}

	public void HandleOnEventBannerLoadFailed(string error){
		Debug.Log("Load banner failed! The error is: " + error);
	}

	public void HandleOnQueryCompleted(){
		Log.Debug("Handle On Query Completeted!!!");
		EventSystemController.GetInstance().loadEventBanner();
	}
	#endregion
	
	
	#region Get Cargo Data
	public object GetCargoValueForKey(string key)
	{
		object ret 			= null;
		Hashtable cargoHT 	= MunerisManager.SharedManager.GetCargo();
			
		if (cargoHT != null)
		{
			try
			{
				ret = cargoHT[key];
			}
			catch(Exception e)
			{
				Debug.Log("Cargo didnt contain the key: " + key);
				Debug.Log("Exception: " + e.ToString());
			}
		}
		return ret;
	}
	
	public int GetCargoIntForKey(string key, int defaultValue)
	{
		int ret 			= defaultValue;
		Hashtable cargoHT 	= MunerisManager.SharedManager.GetCargo();
			
		if (cargoHT != null)
		{
			try
			{
				ret = Convert.ToInt32(cargoHT[key]);
			}
			catch(Exception e)
			{
				Debug.Log("Cargo didnt contain the key: " + key);
				Debug.Log("Exception: " + e.ToString());
			}
		}
		
		return ret;
	}
	
	public string GetCargoStringForKey(string key)
	{
		string ret 			= null;
		Hashtable cargoHT 	= MunerisManager.SharedManager.GetCargo();
		
		if (cargoHT != null)
		{
			try
			{
				ret = "" + cargoHT[key];				
			}
			catch(Exception e)
			{
				Debug.Log("Cargo didnt contain the key: " + key);
				Debug.Log("Exception: " + e.ToString());
			}
		}
		return ret;
	}
	
	public bool GetCargoBoolForKey(string key, bool defaultValue)
	{
		int intValue = Convert.ToInt32(defaultValue);
		return Convert.ToBoolean(this.GetCargoIntForKey(key, intValue));
	}
	
	public float GetCargoFloatForKey(string key, float defaultValue)
	{
		float ret 			= defaultValue;
		Hashtable cargoHT 	= MunerisManager.SharedManager.GetCargo();
		
		if (cargoHT != null)
		{
			try
			{
				ret = float.Parse("" + cargoHT[key]);
			}
			catch(Exception e)
			{
				Debug.Log("Cargo didnt contain the key: " + key);
				Debug.Log("Exception: " + e.ToString());
			}
		}
		return ret;
	}	
	#endregion
}

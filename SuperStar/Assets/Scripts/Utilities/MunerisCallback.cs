using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Muneris.Takeover;
using Muneris.CrashReport;
using Muneris.AppState;
using Muneris.VirtualStore;
using Muneris.BannerAd;
using Muneris.PushNotification;
using Muneris.AppVersionCheck;
using Muneris;


public class MunerisCallback : ITakeoverCallback, IPurchaseStatusCallback, IPushNotificationCallback, ITextMessageCallback, IProductMessageCallback, IBannerAdCallback, IAppVersionCheckCallback, IPushMessageCallback
{
	public BannerAdResponse BannerAdResponse {get; private set;}
		
	#region Takeover
	public void OnTakeoverRequestStart (TakeoverEvent takeoverEvent)
	{
		Log.Debug("The TakeOver started");
	}

	public void OnTakeoverLoaded (TakeoverEvent takeoverEvent)
	{
		Log.Debug("The TakeOver loaded");
	}

	public void OnTakeoverDismiss (TakeoverEvent takeoverEvent)
	{
		Log.Debug("The TakeOver closed");
		PopupManager.HideLoadingFlower();
	}

	public void OnTakeoverFail (TakeoverEvent takeoverEvent, TakeoverException exception)
	{
		Log.Debug("The TakeOver load failed");
	}

	public void OnTakeoveRequestEnd (TakeoverEvent takeoverEvent)
	{
		Log.Debug("The TakeOver is ending");
	}
	#endregion
	
	#region IAP IPurchaseStatusCallback
	
	public void OnProductPackagePurchase (ProductPackage productPackage, VirtualStoreException exception) {
	}
	
	public void OnProductPackageDefer (ProductPackage productPackage, VirtualStoreException exception) {
	}
	
	public void OnProductPackageRestore (List<ProductPackage> productPackages, VirtualStoreException exception) {
	}

	// deprecated on Muneris 4.4.2
//	public void OnProductPurchaseComplete (ProductPackage productPackage)
//	{
//		Log.Debug("Success Purchase package: "+ productPackage.PackageId);
//		
//		//Log Flurry Event
//		Hashtable param = new Hashtable();
//		param["Package"] = productPackage.PackageId;
//		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.In_App_Purchase_Success, param);
//	}
//
//	public void OnProductPurchaseFail (ProductPackage productPackage, VirtualStoreException e)
//	{
//		Log.Debug("Failed to purchase package: " + e.Message.ToString());
//		PopupManager.HideLoadingFlower();
//		
//		Hashtable param = new Hashtable();
//		param["Package"] = productPackage.PackageId;
//		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.In_App_Purchase_Failed, param);
//	}
//
//	public void OnProductPurchaseCancel (ProductPackage productPackage)
//	{
//		Log.Debug("Cancelled to purchase package: " + productPackage.PackageId);
//		PopupManager.HideLoadingFlower();
//		
//		Hashtable param = new Hashtable();
//		param["Package"] = productPackage.PackageId;
//		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.In_App_Purchase_Cancelled, param);
//	}
//
//	public void OnProductRestoreComplete (ArrayList productPackages)
//	{
//		Log.Debug("Restored the purchase packages");
//	}
//
//	public void OnProductRestoreFail (VirtualStoreException e)
//	{
//		Log.Debug("Restore failed " + e.Message.ToString());
//	}
	#endregion
	
	#region Banner Ads
	public void OnBannerAdInit (BannerAdEvent bannerAdEvent)
	{
		Log.Debug("Init Ad Dismiss");
		//this.BannerAdResponse = null;
	}

	public void OnBannerAdLoad (BannerAdEvent bannerAdEvent,BannerAdResponse bannerAdResponse)
	{
		Log.Debug("Loaded Banner Ad");
		this.BannerAdResponse = bannerAdResponse;
		
		// !?
		//MunerisManager.SharedManager.HideBannerAds();
	}

	public void OnBannerAdDismiss (BannerAdEvent bannerAdEvent)
	{
		Log.Debug("Banner Ad Dismiss");
		this.BannerAdResponse = null;
	}

	public void OnBannerAdFail (BannerAdEvent bannerAdEvent, BannerAdException e)
	{
		Log.Debug("Show Banner Ad fail");
		this.BannerAdResponse = null;
	}	
	#endregion
	
	#region New App Version Update
	public void OnNewVersionAvailable (NewAppVersion newAppVersion)
	{
		Log.Debug("OnNewVersionAvailable New Version received");
		//Show New Version
		if (newAppVersion != null)
		{
			//Assign the new app version
			//Used a buffer to allow title scene to get it back
			Log.Debug("OnNewVersionAvailable Add the new app version to MunerisManager");
			MunerisManager.SharedManager.SetNewAppVersion(newAppVersion);
		}
	}
	#endregion
	
	#region Push Notification

	public void OnGooglePushRegister(string registrationId, GooglePushRegistrationException e)
	{
		if (registrationId != null)
		{
			//Checking for uploaded registration ID;
			Log.Debug("Get Registration ID!!!!!");
			Log.Debug(registrationId);
			Log.Debug("Get Registration ID!!!!!");	
			
			MunerisManager.SharedManager.UploadPushToken("android", registrationId, Globals.langCode);
		}
	}
	
	public void OnGooglePushUnregister(string registrationId, GooglePushRegistrationException e)
	{
		
	}

	public void OnApplePushRegister (string registrationId, ApplePushRegistrationException e)
	{

	}

	// deprecated in Muneris 4.4.2
	//	public void OnPushRegister(string registrationId)
	//	{
	//		if (registrationId != null)
	//		{
	//			//Checking for uploaded Device Token
	//			Log.Debug("DEVICE TOKEN !!!!!!!!!!!!!!!!!!!!!!!! From callback" + registrationId);
	//			MunerisManager.SharedManager.UploadPushToken("iOS", registrationId, Globals.langCode);
	//		}
	//	}

	#endregion
	
	#region Push Message
	public void OnPushMessageReceive(PushMessage pushMessage)
	{
		Log.Debug("Push Message Receive!!!!: " + pushMessage.ToString());
	}
	#endregion
	
	#region Text Message
	public void OnTextMessageReceive (TextMessage textMessage)
	{
		Log.Debug("Text Message Receive !!!!: " + textMessage.ToString());
	}
	#endregion
	
	#region Product Message

	public void OnProductMessageReceive(ProductMessage productMessage)
	{		
		if (productMessage.ProductPackageBundles != null) {
			foreach (ProductPackageBundle bundle in productMessage.ProductPackageBundles) {
				Product product = bundle.Product;
   
				Log.Debug("OnProductMessageReceive Qty: " + bundle.Quantity + " ToString: " + product.ToString());

				string source 		= productMessage.Source;
				int qty 			= (int)bundle.Quantity;
				string productId 	= product.ProductId;
				string message		= product.Description;
				string packageId	= Enum.GetName(typeof(IAPPopup.IAPPackageIds), qty);
				
				if (source.Equals("iap"))
				{
					if (!Globals.isIAP)
					{
						//Double the quantity when first time IAP
						qty *= 2;
					}
					else
					{
						Feature f = Features.GetFeatureOfIAPPackageId(packageId);
						if (f != null)
						{
							qty = (int)(qty * (1 + f.discount * 0.01));
						}	
					}
					
					if (Globals.iapRecord == null || Globals.iapRecord.Equals(""))
						Globals.iapRecord = packageId;
					else
						Globals.iapRecord += "," + packageId;
					
					if (!Globals.isIAP)
					{
						Globals.isIAP = true;
						Globals.mainPlayer.Save();
					}
				}
				
				//Check receive type
				Reward reward = new Reward(999, -1, 0, 0, 0, 0, "", new Skill(new int[]{0,0,0,0,0}));
				
				if (productId.ToLowerInvariant().Equals("gem"))
				{
					reward.gems = qty;
				}
				else if (productId.ToLowerInvariant().Equals("coin"))
				{
					reward.coins = qty;
				}
				else if (productId.ToLowerInvariant().Equals("level"))
				{
					int exp = ExpTable.expTable[Globals.mainPlayer.Level + qty] - Globals.mainPlayer.Exp;
					reward.exp = exp;
				}		
				
				if (reward != null)
				{
					int initDBSceneNum = (int)Scenes.INITDB;
					if (Application.loadedLevel == initDBSceneNum)
					{
						Log.Debug("Receive Product Message when initDB");
						reward.claim();
					}
					else
					{
						PopupManager.HideLoadingFlower();
						//PopupManager.HideAllPopup();
						
						if (message != null && !message.Equals(""))
						{
							PopupManager.ShowRewardPopup(reward, message, true);
						}
						else
						{
							PopupManager.ShowRewardPopup(reward, true);
						}
					}
				}
			
//				switch (product.ProductType) {
//					
//				case ProductType.Consumable:
//					//Increment total item quantity
//					totalQuantity = originalQuantity + bundle.Quantity;
//					break;
//					
//				case ProductType.NonConsumable:
//					break;
//					
//				case ProductType.Subscription:
//					break;
//					
//				default:
//					break;        
//				}
			}
		}
	}
	#endregion
}

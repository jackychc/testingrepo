using UnityEngine;
using System.Collections;

public sealed class Log {
	
	public static void Debug(object message)
	{
		#if UNITY_EDITOR
			UnityEngine.Debug.Log(message);
		#else
			if (Globals.allowDebugLog)
				UnityEngine.Debug.Log(message);
		#endif
	}
	
	public static void Debug(object message, Object context)
	{
		#if UNITY_EDITOR
			UnityEngine.Debug.Log(message, context);	
		#else
			if (Globals.allowDebugLog)
				UnityEngine.Debug.Log(message);
		#endif
	}
	
	public static void Warning(object message)
	{
		#if UNITY_EDITOR
			UnityEngine.Debug.LogWarning(message);	
		#else
			if (Globals.allowDebugLog)
				UnityEngine.Debug.LogWarning(message);
		#endif
	}
	
	public static void Warning(object message, Object context)
	{
		#if UNITY_IPHONE
			UnityEngine.Debug.LogWarning(message, context);	
		#else 
			if (Globals.allowDebugLog)
				UnityEngine.Debug.LogWarning(message);
		#endif
	}
	
	public static void Exception(System.Exception exception)
	{
		#if UNITY_EDITOR
			UnityEngine.Debug.LogException(exception);	
		#else
			if (Globals.allowDebugLog)
				UnityEngine.Debug.LogException(exception);
		#endif
	}
	
	public static void Exception(System.Exception exception, Object context)
	{
		#if UNITY_EDITOR
			UnityEngine.Debug.LogException(exception, context);
		#else
			if (Globals.allowDebugLog)
				UnityEngine.Debug.LogException(exception, context);
		#endif
	}
	
	public static void Error(object message)
	{
		#if UNITY_EDITOR
			UnityEngine.Debug.LogError(message);
		#else
			if (Globals.allowDebugLog)
				UnityEngine.Debug.LogError(message);
		#endif
	}	
	
	public static void Error(object message, Object context)
	{
		#if UNITY_EDITOR
			UnityEngine.Debug.LogError(message, context);
		#else
			if (Globals.allowDebugLog)
				UnityEngine.Debug.LogError(message, context);
		#endif
	}
}

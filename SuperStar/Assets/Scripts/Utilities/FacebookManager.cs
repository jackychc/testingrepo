using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Muneris.Facebook;

public class FacebookManager : MonoBehaviour, IFacebookSessionCallback, IFacebookApiCallback
{	
	private enum GraphAction
	{
		NONE = 0,
		Login,
		Logout,
		GetFacebookIdForChecking,
		GetFriends,
		PublishSendVoteOpenGraph,
		PublishReceiveGiftOpenGraph,
		PublishWinAPrizeOpenGraph,
		PublishRankingOpenGraph,
		UploadPhoto
	}	
	
	private Dictionary<string, string> paramList 	= new Dictionary<string, string>();
	private GraphAction currentAction 				= GraphAction.NONE;
	private GraphAction loginAction 				= GraphAction.NONE;
	private bool isCheckedId 						= false;
	private bool isSilentLogout 					= false;
	private string userId 							= null;
	
	private List<FacebookManagerCallbackInterface> callbackList = new List<FacebookManagerCallbackInterface>();
	
	private static FacebookManager instance = null;
	public static FacebookManager SharedManager {
		get{
			return instance;
		}
	}
	
	void Awake(){
		if (instance == null)
		{
			instance = this;
			Muneris.Muneris.AddCallback(instance);
			
			loginAction = GraphAction.NONE;
			currentAction = GraphAction.NONE;
			isCheckedId = false;
			
			//Restore the DB's FacebookId
			string dbFacebookId = Globals.mainPlayer.facebookElement.socialId;
			if (dbFacebookId != null && !dbFacebookId.Equals(""))
				userId = dbFacebookId;
			
			DontDestroyOnLoad(this.transform.gameObject);
			Log.Debug("FacebookManager init");
		}
		else
			Destroy(this.transform.gameObject);
	}	
	
	// Use this for initialization
	void Start () {
		//PublishWinAPrizeOpenGraph();
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnApplicationQuit()
	{
		instance = null;	
	}
	
	public void Destroy()
	{
		Destroy(this.transform.gameObject);	
	}
	
	public void SetCallback(FacebookManagerCallbackInterface c)
	{			
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
			{
				Log.Debug("FacebookManager the callback is duplicate");
				Log.Debug("Now try to remove the callback");
				callbackList.Remove(c);

				Log.Debug("The callback List num: " + callbackList.Count);
			}

			callbackList.Add(c);
		}
	}
	
	public void RemoveCallback(FacebookManagerCallbackInterface c)
	{
		if (callbackList != null)
		{
			if (callbackList.Contains(c))
				callbackList.Remove(c);
		}
	}
	
	public void ReloadUserId()
	{
		string dbFacebookId = Globals.mainPlayer.facebookElement.socialId;
		if (dbFacebookId != null && !dbFacebookId.Equals(""))
			userId = dbFacebookId;
		else
			userId = null;
	}
	
	private IEnumerator LogFlurryEvent()
	{
		Hashtable param = new Hashtable();
		param["Connect"] = "Facebook";
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Connect_SNS, param);
		
		yield return null;
	}
	
	#region Facebook OpenGraph Action	
	public static string GetProfilePicURL(string facebookId, int width, int height)
	{
		return "http://graph.facebook.com/"+facebookId+"/picture?width="+width+"&height="+height;		
	}
	
	private void GetFacebookIdForChecking()
	{
		Log.Debug("GetFacebookIdForChecking");
		isCheckedId = true;
		
		currentAction = GraphAction.GetFacebookIdForChecking;
		string graphPath = "/me";
		ExecuteGraphApi(graphPath, new Hashtable(), new List<string>(), HttpMethod.GET, null);
	}
	
	public void GetFriendsOpenGraph()
	{
		Log.Debug("Get Friends OpenGraph");
		currentAction = GraphAction.GetFriends;
		if (!isCheckedId)
		{
			Log.Debug("Is not logged In");
			Login();
			return;	
		}
		string sqlQuery = "SELECT uid FROM user WHERE is_app_user AND uid IN (SELECT uid2 FROM friend WHERE uid1 = me())";
		var param = new Hashtable();
		param.Add("q", sqlQuery);
		
		string graphPath = "/fql";
		ExecuteGraphApi(graphPath, param, new List<string>(), HttpMethod.GET, null);
	}
	
	public void PublishSendVoteOpenGraph(string playerFBName, string playerFBID)
	{
		currentAction = GraphAction.PublishSendVoteOpenGraph;
		if (!isCheckedId)
		{
			paramList["playerFBName"] 	= playerFBName;
			paramList["playerFBID"] 	= playerFBID;
			Login();
			return;	
		}
		string graphPath = "/me/"+ Globals.facebookNameSpace +":vote";
		var param = new Hashtable();
		param.Add("friend", Globals.serverURL + "Facebook/FacebookOGSendVote.php?fb:app_id=" + Globals.facebookAppId +
							"&og:type="+ Globals.facebookNameSpace +":friend" +
							"&og:title="+ playerFBName +
		          			"&og:image=https://superstarstory.s3.amazonaws.com/Topics/Topics6/00LFT8.png" + //imagePath
							"&og:description=I voted!!" +
							"&playerid=" + playerFBID);
		List<string> permissions = new List<string>();
		permissions.Add("publish_actions");
//		var permissions = new ArrayList (new string[] { "publish_actions" });
		ExecuteGraphApi(graphPath, param, permissions, HttpMethod.POST, new Hashtable());
	}	
	
	public void PublishReceiveGiftOpenGraph(string boyfriendName)
	{
		currentAction = GraphAction.PublishReceiveGiftOpenGraph;
		if (!isCheckedId)
		{
			paramList["boyfriendName"] = boyfriendName;
			Login();
			return;	
		}
		string graphPath = "/me/"+ Globals.facebookNameSpace +":receive";
		var param = new Hashtable();
		param.Add("boyfriend", Globals.serverURL + "Facebook/FacebookOGReceiveGift.php?fb:app_id=" + Globals.facebookAppId +
							   "&og:type="+ Globals.facebookNameSpace +":boyfriend" +
							   "&og:title=" + boyfriendName +
		          			   "&og:image=https://superstarstory.s3.amazonaws.com/Topics/Topics6/00LFT8.png" + //gift image
							   "&og:description=I like it!!");
//		var permissions = new ArrayList (new string[] { "publish_actions" });
		List<string> permissions = new List<string>();
		permissions.Add("publish_actions");
		ExecuteGraphApi(graphPath, param, permissions, HttpMethod.POST, new Hashtable());
	}
	
	public void PublishWinAPrizeOpenGraph()
	{
		currentAction = GraphAction.PublishWinAPrizeOpenGraph;
		if (!isCheckedId)
		{
			Login();
			return;	
		}
		string graphPath = "/me/"+ Globals.facebookNameSpace +":win";
		var param = new Hashtable();
		param.Add("prize", Globals.serverURL + "Facebook/FacebookOGWinAPrize.php?fb:app_id=" + Globals.facebookAppId +
						   "&og:type="+ Globals.facebookNameSpace +":prize" +
						   "&og:title=Prize" +
		          		   "&og:image=https://superstarstory.s3.amazonaws.com/Topics/Topics6/00LFT8.png" + //Prize image
						   "&og:description=I like it!!");
//		var permissions = new ArrayList (new string[] { "publish_actions" });
		List<string> permissions = new List<string>();
		permissions.Add("publish_actions");
		ExecuteGraphApi(graphPath, param, permissions, HttpMethod.POST, new Hashtable());	
	}
	
	public void PublishRankingOpenGraph(string rankString)
	{
		currentAction = GraphAction.PublishRankingOpenGraph;
		if (!isCheckedId)
		{
			paramList["rankString"] = rankString;
			Login();
			return;	
		}
		string graphPath = "/me/"+ Globals.facebookNameSpace +":rank";
		var param = new Hashtable();
		param.Add("result", Globals.serverURL + "Facebook/FacebookOGWinAPrize.php?fb:app_id=" + Globals.facebookAppId +
						    "&og:type="+ Globals.facebookNameSpace +":result" +
						    "&og:title=" + rankString +
		          			"&og:image=https://superstarstory.s3.amazonaws.com/Topics/Topics6/00LFT8.png" + //Prize image
						    "&og:description=I like it!!");
		//		var permissions = new ArrayList (new string[] { "publish_actions" });
		List<string> permissions = new List<string>();
		permissions.Add("publish_actions");
		ExecuteGraphApi(graphPath, param, permissions, HttpMethod.POST, new Hashtable());
	}
	
	public void UploadPhoto(string caption, string imagePath)
	{
		Log.Debug("Facebook UploadPhoto");
		currentAction = GraphAction.UploadPhoto;
		if (!isCheckedId)
		{
			paramList["caption"] 	= caption;
			paramList["imagePath"]	= imagePath;
			Login();
			return;	
		}
		
		Hashtable param = new Hashtable();
		param.Add("message", caption);
		
		Facebook.ExecuteUploadPhotoApi(imagePath, param, new Hashtable());	
	}
	
	private void ResumeCurrentAction()
	{
		switch(currentAction)
		{
			case GraphAction.GetFriends:
				GetFriendsOpenGraph();
				break;
			case GraphAction.PublishRankingOpenGraph:
				string rankString = paramList["rankString"];
				PublishRankingOpenGraph(rankString);
				break;
			case GraphAction.PublishReceiveGiftOpenGraph:
				string boyfriendName = paramList["boyfriendName"];
				PublishReceiveGiftOpenGraph(boyfriendName);
				break;
			case GraphAction.PublishSendVoteOpenGraph:
				string playerFBName = paramList["playerFBName"];
				string playerFBID = paramList["playerFBID"];
				PublishSendVoteOpenGraph(playerFBName, playerFBID);
				break;
			case GraphAction.PublishWinAPrizeOpenGraph:
				PublishWinAPrizeOpenGraph();
				break;
			case GraphAction.UploadPhoto:
				string caption = paramList["caption"];
				string imagePath = paramList["imagePath"];
				UploadPhoto(caption, imagePath);
				break;
			default:
				break;			
		}	
	}
	#endregion
	
	#region Facebook Function
	public void ShowFanPage(string fanPageId)
	{

//		Facebook.ShowFanPage(fanPageId);
	}

	public bool IsLoggedIn()
	{
		return Facebook.IsLoggedIn();	
	}
	
	public void Login()
	{
		//Check if there is existing action
		if (!currentAction.Equals(GraphAction.NONE))
		{
			Log.Debug(currentAction);
			Log.Debug("Record Login Action");
			loginAction = currentAction;
		}
		else
			currentAction = GraphAction.Login;
		
		/*
		if (!IsLoggedIn())
		{
			currentAction = GraphAction.Login;
			//Facebook.LoginForRead();
		}
		*/
		
		//{
		GetFacebookIdForChecking();
		//}
	}
	
	public void SilentLogout()
	{
		isSilentLogout = true;
		Logout();
	}
	
	public void Logout()
	{
		currentAction = GraphAction.Logout;
		Facebook.Logout();	
	}
//	ExecuteApi (string graphPath, Hashtable @params, List<string> permissions, HttpMethod httpMethod, Hashtable cargo)
	public void ExecuteGraphApi(string graphPath, Hashtable param, List<string> permissions, HttpMethod method, Hashtable cargo)
	{
		Facebook.ExecuteApi(graphPath, param, permissions, method, cargo);
	}
	#endregion
	
	#region FacebookSessionCallback

	public void OnFacebookLogin(FacebookSessionException exception)
	{
		Log.Debug("on Facebook Login");
		
		if (!isCheckedId)
			GetFacebookIdForChecking();
	}
	
	public void OnFacebookLogout(FacebookSessionException exception)
	{
		currentAction = GraphAction.NONE;

		//Remove the facebookId in the server
		Log.Debug("on Facebook Logout");
		if (!isSilentLogout)
		{
			if (callbackList != null)
			{
				for (int i = 0;i < callbackList.Count; i++)
				{
					FacebookManagerCallbackInterface c = callbackList[i];
					c.OnFacebookManagerLogout();
				}
			}
		}
		else
			isSilentLogout = false;
	}
	
	public void OnFacebookCancelLogin()
	{
		Log.Debug("on Facebook Cancel Login");
		
		if (callbackList != null)
		{
			for (int i = 0;i < callbackList.Count; i++)
			{
				FacebookManagerCallbackInterface c = callbackList[i];
				c.OnFacebookManagerLoginCancel();
			}
		}
	}
	
	public void OnFacebookSessionFail(Exception e)
	{
		isCheckedId = false;
		Log.Debug("on Facebook Session Failed!");
		
		if (callbackList != null)
		{
			for (int i = 0;i < callbackList.Count; i++)
			{
				FacebookManagerCallbackInterface c = callbackList[i];
				c.OnFacebookManagerError(e);
			}
		}
	}
	#endregion
	
	#region FacebookApiCallback
	
	public void OnFacebookApiRespond(Hashtable graphObject, Hashtable cargo, FacebookApiException exception)
	{
		Log.Debug("Facebook API Respond");
		
		switch(currentAction)
		{
			case GraphAction.GetFacebookIdForChecking:
			{
				Log.Debug("Facebook API Response GetFacebookIdForChecking");
				string currentUserId = (string)graphObject["id"];
				
				if (userId != null && !userId.Equals(""))
				{
					if (currentUserId.Equals(userId))
					{
						if (callbackList != null)
						{
							for (int i = 0;i < callbackList.Count; i++)
							{
								FacebookManagerCallbackInterface c = callbackList[i];
								c.OnFacebookManagerLogin(userId);
							}
						}
						
						MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.ConnectToFacebook);
						StartCoroutine(LogFlurryEvent());
					
						//Restore loginAction
						Log.Debug("Restore Login action");
						if (!loginAction.Equals(GraphAction.NONE))
						{
							Log.Debug("Facebook restored");
							currentAction 	= loginAction;
							loginAction 	= GraphAction.NONE;
							ResumeCurrentAction();
						}
						else
							currentAction = GraphAction.NONE;
						return;
					}
				}
			
				Log.Debug("Facebook userId retrieved. Now check the Id in Server");
				StartCoroutine(CheckUserAccountWithId(currentUserId, graphObject));
				
				return;
			}
			case GraphAction.GetFriends:
			{
				//Extract the FbFriends from the arrayList
				ArrayList dataObject = (ArrayList)graphObject["data"];
				ArrayList fbFriends = new ArrayList();
				for (int i = 0; i < dataObject.Count; i++)
				{
					Hashtable userObject = (Hashtable)dataObject[i];
					string uid = userObject["uid"].ToString();
					fbFriends.Add(uid);
				}
			
				if (callbackList != null)
				{
					for (int i = 0;i < callbackList.Count; i++)
					{
						FacebookManagerCallbackInterface c = callbackList[i];
						c.OnFacebookManagerGetFriends(fbFriends);
					}
				}		
				break;
			}
			case GraphAction.UploadPhoto:
			{
				if (callbackList != null)
				{
					for (int i = 0;i < callbackList.Count; i++)
					{
						FacebookManagerCallbackInterface c = callbackList[i];
						c.OnFacebookManagerUploadPhoto();
					}
				}
			
				MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.UploadPhotoToFacebook);
				break;
			}
			case GraphAction.PublishRankingOpenGraph:
				break;
			case GraphAction.PublishReceiveGiftOpenGraph:
				break;
			case GraphAction.PublishSendVoteOpenGraph:
				break;
			case GraphAction.PublishWinAPrizeOpenGraph:
				break;
			default:
				break;
		}
		
		currentAction = GraphAction.NONE;
		isCheckedId = false;
	}

	// Deleted callback in Muneris 4.4.2
//	public void OnFacebookApiFail (FacebookApiException e, System.Collections.Hashtable cargo)
//	{
//		Log.Debug("Facebook API Fail");
//		Log.Debug(e.ToString());
//		currentAction = GraphAction.NONE;
//		isCheckedId = false;
//	}
	#endregion
			
	#region Server API
	private IEnumerator CheckUserAccountWithId(string socialNetworkId, Hashtable graphObject)
	{
		//Prepare CheckSum
		string checkSumData = socialNetworkId + "facebook";
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("SearchGameSave", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("socialNetworkId", socialNetworkId);
		kvp.Add("socialNetworkType", "facebook");
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "SearchGameSave.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				try
				{
					Log.Debug("check response "+response);
					ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
					bool hasResponse = Boolean.Parse(((Hashtable)al[0])["response"].ToString());
					if (hasResponse)
					{					
						bool newSave 		= false;
						bool existingSave 	= false;
						Hashtable userInfo 	= (Hashtable)al[1];
						if (userInfo != null)
						{
							userInfo["PlayerSocialFB"] = graphObject;				
							//Get save Load Save With Data
							
							//Check the userId
							if (userId != null && !userId.Equals(""))
							{
								if (!socialNetworkId.Equals(userId))
									existingSave = true;
							}
							else
								existingSave = true;
						}
						else
						{
							//No Save
							if (userId != null && !userId.Equals(""))
							{
								newSave = true;
							}
						}
						
						if (existingSave)
						{
							Log.Debug("FacebookManager: Existing SAve");
							if (callbackList != null)
							{
								for (int i = 0;i < callbackList.Count; i++)
								{
									FacebookManagerCallbackInterface c = callbackList[i];
									c.OnFacebookManagerExistingUserError(userInfo);
								}
							}
							
							//SilentLogout();
						}
						else if (newSave)
						{
							Log.Debug("FacebookManager: New Save");
							if (callbackList != null)
							{
								for (int i = 0;i < callbackList.Count; i++)
								{
									FacebookManagerCallbackInterface c = callbackList[i];
									c.OnFacebookManagerNewUserError(graphObject);
								}
							}
							
							//SilentLogout();
						}
						else
						{
							Log.Debug("Assign the userId");
							if (userId == null || userId.Equals(""))
							{
								StartCoroutine(UploadFacebookIdToServer(socialNetworkId));
								userId = socialNetworkId;
							}
							if (callbackList != null)
							{
								for (int i = 0; i < callbackList.Count; i++)
								{
									FacebookManagerCallbackInterface c = callbackList[i];
									c.OnFacebookManagerLogin(userId);
								}							
							}
							
							MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.ConnectToFacebook);
							StartCoroutine(LogFlurryEvent());
						
							//Restore loginAction
							Log.Debug("Restore Login action");
							if (!loginAction.Equals(GraphAction.NONE))
							{
								Log.Debug("restored");
								currentAction = loginAction;
								loginAction = GraphAction.NONE;
								ResumeCurrentAction();
							}
							else
								currentAction = GraphAction.NONE;
						}

					}
					else
					{
						Log.Debug("Error on the response message");
					}
				}
				catch (Exception e)
				{
					if (callbackList != null)
					{
						for (int i = 0;i < callbackList.Count; i++)
						{
							FacebookManagerCallbackInterface c = callbackList[i];
							c.OnFacebookManagerError(e);
						}
					}
				}
				finally
				{
					isCheckedId = false;
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("SearchGameSave", kvp, result => response = result));		
	}
	
	private IEnumerator UploadFacebookIdToServer(string uid)
	{
		Log.Debug("Facebook Uploading facebook Id to Server");
		string socialNetworkType = "facebook";		
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + socialNetworkType + uid;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("UpdatePlayerSocialFeature", checkSumData);		
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("socialNetworkType", socialNetworkType);
		kvp.Add("socialNetworkId", uid);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "UpdatePlayerSocialFeature.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al = new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				bool responseTag = Boolean.Parse(((Hashtable)al[0])["response"].ToString());
				if (responseTag)
				{
					Log.Debug("[FacebookManager] Uploaded the facebookId to the Server");
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("UpdatePlayerSocialFeature", kvp, result => response = result));			
	}
	#endregion
}

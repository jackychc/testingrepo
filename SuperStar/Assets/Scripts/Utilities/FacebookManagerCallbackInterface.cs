using UnityEngine;
using System.Collections;
using System;

public interface FacebookManagerCallbackInterface 
{
	void OnFacebookManagerGetFriends(ArrayList friendList);
	void OnFacebookManagerLogin(string userId);
	void OnFacebookManagerLogout();
	void OnFacebookManagerLoginCancel();
	void OnFacebookManagerError(Exception e);
	
	void OnFacebookManagerUploadPhoto();
	
	
	//Checking for the differet facebook account logged in
	void OnFacebookManagerNewUserError(Hashtable userInfo);
	void OnFacebookManagerExistingUserError(Hashtable existingUserInfo);
}

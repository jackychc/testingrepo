using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameID
{
	private string gameId;

	private static string defaultID = "TTTTTT";	
	private static Dictionary<string, int> 	IDLettersToValue 	= null;
	private static ArrayList 				IDValueToLetters 	= null;	
	private static int 						length 				= 6;
	private static int 						IDBase 				= 32;
	
	public string ID{
		get{
			return this.gameId;	
		}
	}
	
	private void init()
	{
		IDLettersToValue = new Dictionary<string, int>(){
			{"T", 0}, {"X", 1}, {"R", 2}, {"2", 3}, {"A", 4}, 
			{"6", 5}, {"Q", 6}, {"7", 7}, {"9", 8}, {"W", 9}, 
			{"V", 10}, {"U", 11}, {"Z", 12}, {"Y", 13}, {"5", 14}, 
			{"F", 15}, {"K", 16}, {"G", 17}, {"J", 18}, {"3", 19}, 
			{"P", 20}, {"E", 21}, {"N", 22}, {"8", 23}, {"H", 24}, 
			{"C", 25}, {"D", 26}, {"S", 27}, {"M", 28}, {"4", 29},
			{"L", 30}, {"B", 31}
		};
		
		
		IDValueToLetters = new ArrayList(){
			"T", "X", "R", "2", "A", 
			"6", "Q", "7", "9", "W", 
			"V", "U", "Z", "Y", "5", 
			"F", "K", "G", "J", "3", 
			"P", "E", "N", "8", "H", 
			"C", "D", "S", "M", "4", 
			"L", "B"
		};		
	}		
	
	public GameID(string ID)
	{
		this.init();
		if (ID != null && !ID.Equals(""))
			this.gameId = ID;
		else
			this.gameId = defaultID;
	}
	
	public void add(int index)
	{
		int intId = toInt() + index;
		this.gameId = GameID.parseInt(intId);
	}
	
	public void minus(int index)
	{
		int intId = toInt() - index;
		this.gameId = GameID.parseInt(intId);
	}
	
	public int toInt()
	{
		if (this.gameId.Length < length)
			return -1;
		
		int totalNum = 0;
		for (int i = 0; i < length; i++)
		{
			string letter = "" + this.gameId[i];
			int letterValue = IDLettersToValue[letter];
			
			totalNum += letterValue * (int)Math.Pow(IDBase, (length - 1) - i);	
		}
		
		return totalNum;
	}
	
	public static string parseInt(int intId)
	{
		string tempGameId = "";
		while(intId >= IDBase)
		{
			int result = intId % IDBase;
			tempGameId = IDValueToLetters[result] + tempGameId;
			intId = (int) intId/ IDBase;
		}		
		tempGameId = IDValueToLetters[intId] + tempGameId;	
		
		if (tempGameId.Length < length)
		{
			int diff = length - tempGameId.Length;
			for (int i = 0; i < diff; i++)
				tempGameId = IDValueToLetters[0] + tempGameId;
		}

		return tempGameId;
	}
}

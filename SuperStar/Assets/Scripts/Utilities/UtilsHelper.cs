using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class UtilsHelper {
	
#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern string _Native_Utils_GetDocumentDirectory();
	[DllImport ("__Internal")]
	private static extern void _Native_Utils_SaveImageToCameraRoll(string filePath);
	[DllImport ("__Internal")]
	private static extern string _Native_Utils_getDeviceLanguage();
	[DllImport ("__Internal")]
	private static extern void _Native_Utils_UnzipFile(string zipFile, string location, string callbackObj);
#elif UNITY_ANDROID
	public const string UtilsBridgeClass = "muneris.unity.androidbridge.utils.UtilsBridge";
#endif
	
	public static string GetWriteableDirectory()
	{
#if UNITY_EDITOR
		return Application.persistentDataPath;		
#elif UNITY_ANDROID
		return new AndroidJavaClass (UtilsBridgeClass).CallStatic<string>("getExternalStoragePath");
#elif UNITY_IPHONE
		return _Native_Utils_GetDocumentDirectory();
#endif
	}
	
	public static void RefreshDirectoryWithFileName(string fileName)
	{
#if UNITY_EDITOR
#elif UNITY_ANDROID
		new AndroidJavaClass ("muneris.unity.androidbridge.utils.UtilsBridge").CallStatic ("scanImages", fileName);
#elif UNITY_IPHONE
		_Native_Utils_SaveImageToCameraRoll(fileName);
#endif		
	}

	public static void UnzipFile(string zipFile, string location, string callbackObj)
	{
#if UNITY_EDITOR
#elif UNITY_ANDROID
		new AndroidJavaClass ("muneris.unity.androidbridge.utils.UtilsBridge").CallStatic ("UnzipFile", zipFile, location, callbackObj);
#elif UNITY_IPHONE
		_Native_Utils_UnzipFile(zipFile, location, callbackObj);
#endif
	}
	
	public static LanguageCode GetDeviceLanguage()
	{
		string langCode = "";
#if UNITY_EDITOR
		langCode = Application.systemLanguage.ToString();
#elif UNITY_ANDROID
		langCode = new AndroidJavaClass ("muneris.unity.androidbridge.utils.UtilsBridge").CallStatic<string>("getDeviceLanguage");
#elif UNITY_IPHONE
		langCode = _Native_Utils_getDeviceLanguage();
#endif
		Log.Debug("Device Language is: " + langCode);
		
		langCode = langCode.ToLowerInvariant();		
		if (langCode.Equals("ch") || langCode.Equals("zh-Hans"))
		{
			return LanguageCode.CN;
		}
		else if (langCode.Equals("zh") || langCode.Equals("zh-Hant"))
		{
			return LanguageCode.ZH;
		}
		else if (langCode.Equals("ja"))
		{
			return LanguageCode.JA;
		}
		else if (langCode.Equals("ko"))
		{
			return LanguageCode.KO;
		}
		else if (langCode.Equals("it"))
		{
			return LanguageCode.IT;
		}
		else if (langCode.Equals("fr"))
		{
			return LanguageCode.FR;
		}
		else if (langCode.Equals("pt"))
		{
			return LanguageCode.PT;
		}
		else if (langCode.Equals("de"))
		{
			return LanguageCode.DE;
		}
		else if (langCode.Equals("es"))
		{
			return LanguageCode.ES;		
		}
		
		return LanguageCode.EN;
	}
	
	public static Vector2 GetNGUIAspectRatioFromAspectRatio(Vector2 aspectRatio)
	{
		Vector2 nguiAspectRatio = new Vector2(0, 0);
		
		GameObject uiRootObj = GameObject.Find("UI Root (2D)");
		if (uiRootObj != null)
		{
			AspectRatioAdjustment adj = uiRootObj.GetComponent<AspectRatioAdjustment>();
			float adjWidth = adj.manualWidth;
			
			float origAspectRatio = aspectRatio.x/aspectRatio.y;
			float adjHeight = adjWidth/origAspectRatio;
			
			nguiAspectRatio = new Vector2(adjWidth, adjHeight);
		}		
		return nguiAspectRatio;
	}

	public static string GetAppVersionFromBundleVersion(string bundleVersion)
	{
		string appVersion 			= "";	
		string[] versionStringArray = bundleVersion.Split('.');

		for (int i = 0;i < versionStringArray.Length; i++)
		{
			if (i == versionStringArray.Length - 1)
				continue;

			appVersion += versionStringArray[i] + ".";
		}

		appVersion = appVersion.Remove(appVersion.Length - 1);

		return appVersion;
	}

	public static string GetVersionCodeFromBundleVersion(string bundleVersion)
	{
		string versionCode = "";
		string[] versionStringArray = bundleVersion.Split('.');

		for (int i = 0; i < versionStringArray.Length; i++)
		{
			if (i != versionStringArray.Length - 1)
				continue;

			versionCode = versionStringArray[i];
		}

		return versionCode;
	}
}

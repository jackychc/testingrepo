using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Muneris;
using Muneris.VirtualStore;
using Muneris.Takeover;
using Muneris.BannerAd;
using Muneris.AppEvent;
using Muneris.AppVersionCheck;

public class MunerisManager : MonoBehaviour
{	
	public enum PPA
	{
		PlayOn2ndDay,
		PlayOn3rdDay,
		PlayOn4thDay,
		ConnectToFacebook,
		UploadPhotoToFacebook,
		FinishFirstJobInJobCenter,
		PlaySuperstarRunway,
		BuyAnItemAnyShop,
		DateFirstBF,
		ReachLevel2,
		ReachLevel3,
		Match4CoinsInSuperstarCoins		
	}
	
	public enum FlurryEvent
	{
		Purchased_Item,
		Closet_Changed_Clothes,
		Current_Level,
		BF_Gift_Receive_Method,
		BF_Flirt_Method,
		BF_Flirt_State,
		Job_State,
		Shop_EnterShop,
		In_App_Purchase_Success,
		In_App_Purchase_Failed,
		In_App_Purchase_Requested,
		In_App_Purchase_Cancelled,
		In_App_Purchase_Unique_User,
		Recover_Energy_Gem_Other,
		Recover_Energy_Gem_BF_Scene,
		Recover_Energy_Gem_DropCoin_Scene,
		Recover_Energy_Gem_Runway_Scene,
		Camera_TakePhoto,
		Connect_SNS,
		Add_Friends,
		Visit_Friends,
		Street_Click_Friends,
		Street_Click_Posters,
		Street_Click_Quest,
		Street_Click_DLC,
		Quest_State,
		Runway_Played_times,
		Runway_Result,
		DropCoin_Payout_ChainLength,
		DropCoin_Fail_ChainLength,
		DropCoin_Total_Count,
		Reset_Game,
		Language_Used,
		DLC_Download,
		DLC_State,
	}
	
	private NewAppVersion newAppVersion = null;

	private const string enterTitleScreen	= "enterTitleScreen";
	private const string featured 			= "featured";
	private const string offers				= "offers";
	private const string adZone 			= "adZone";
	private const string moreapps 			= "moreapps";
	private const string customerSupport 	= "customersupport";
	
	private MunerisCallback callback = null;
	private static MunerisManager instance = null;	
	public static MunerisManager SharedManager {
		get{
			return instance;
		}
	}
	
	void Awake()
	{
		if (instance == null)
		{
			instance = this;
			callback = new MunerisCallback();
			Muneris.Muneris.AddCallback(callback);
			Takeovers.ShowBuiltinUI = true;
			Log.Debug("Muneris Manager");
			Log.Debug("Device Id: " + Muneris.Muneris.GetDeviceId());

			EnterTitleScreen();
		}
	}
	
	void Start()
	{
//		BannerAds.SetDefaultBannerAd(BannerPosition.TOP_CENTER, BannerSize.Size320x50, true);
//      BannerAds.RegisterBannerAd(adZone, BannerPosition.BOTTOM_CENTER, BannerSize.Size320x50, true);	
		BannerAds.RegisterBannerAd(adZone, BannerSize.Size320x50);
		
		//StartCoroutine(EnumGetBannerAd());
		StartCoroutine(EnumGetPushToken());
	}
	
	void Update()
	{
		
	}

	//Buffer for New App Version
	public void SetNewAppVersion(NewAppVersion v)
	{
		newAppVersion = v;
	}

	public NewAppVersion GetNewAppVersion()
	{
		return newAppVersion;
	}
	
	public void TestNewVersion()
	{
		#if UNITY_ANDROID
		new AndroidJavaClass("muneris.unity.androidbridge.Test").CallStatic("onNewAppVersionAvailable");
		#endif
	}
	
	/*private IEnumerator EnumGetBannerAd()
	{
		yield return new WaitForEndOfFrame();
		
		
	}*/
	
	private IEnumerator EnumGetPushToken()
	{
		yield return new WaitForEndOfFrame();
		
		GetPushToken();
	}
	
	private void GetPushToken()
	{
#if UNITY_IPHONE
		int types = (int)(RemoteNotificationType.Alert | RemoteNotificationType.Badge | RemoteNotificationType.Sound);
		Muneris.Muneris.RegisterForRemoteNotificationTypes(types);
#endif
#if UNITY_ANDROID
		var registrationId = new AndroidJavaClass("muneris.android.pushnotification.PushNotification").CallStatic<string>("getRegistrationId");
		if (registrationId != null)
		{
			//Upload the registration Id to the server
			Log.Debug("Get Registration ID!!!!!");
			Log.Debug(registrationId);
			Log.Debug("Get Registration ID!!!!!");
			
			MunerisManager.SharedManager.UploadPushToken("android", registrationId, Globals.langCode);
		}		
#endif	
	}
	
	public void UploadPushToken(string type, string token, string lang)
	{
		StartCoroutine(UploadPushTokenToServer(type, token, lang));	
	}
	
	private IEnumerator UploadPushTokenToServer(string type, string token, string lang)
	{
		Log.Debug("Uploading push token to Server");
		
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + type + token + lang;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("UpdatePlayerDevice", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("deviceType", type);
		kvp.Add("devicePushId", token);
		kvp.Add("deviceLang", lang);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "UpdatePlayerDevice.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al = new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (al != null)
				{
					bool responseTag = Boolean.Parse(((Hashtable)al[0])["response"].ToString());
					if (responseTag)
					{
						Log.Debug("Uploaded the Push Token to the Server");
					}		
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("UpdatePlayerDevice", kvp, result => response = result));	
	}
	
	public bool IsMunerisReady()
	{
		return Muneris.Muneris.IsReady;
	}
	
	public Hashtable GetCargo()
	{
		Hashtable cargoHT = Muneris.Muneris.GetCargo();
		return cargoHT;
	}
	
	public String GetDeviceId()
	{
		//DeviceId deviceId = Muneris.Muneris.Instance.GetDeviceId();
		//return deviceId.InstallId;
		//For Debug Testing
#if UNITY_EDITOR
	return "123";	
#else
		DeviceId deviceId = Muneris.Muneris.Instance.getDeviceId();
	return deviceId.InstallId;
#endif
	}
	
	#region Products + Packages
	/*
	public ArrayList QueryProducts()
	{
		ArrayList products = VirtualStore.QueryProducts();
		if (products.Count != 0){
			foreach(Product p in products)
			{
				Log.Debug(p.Id);
				Log.Debug(p.Name);
				Log.Debug(p.Description);
			}
			return products;
		}
		return null;
	}
	
	public ArrayList QueryPackages()
	{
		ArrayList packages = VirtualStore.QueryProductPackages();
		if (packages.Count != 0){
			return packages;
		}
		return null;
	}
	
	public ArrayList QueryPackageWithProductId(string prodId)
	{
		ArrayList products = VirtualStore.QueryProducts(prodId);
		if (products.Count != 0){			
			ArrayList packages = VirtualStore.QueryProductPackages(prodId);
			if (packages.Count != 0){
				return packages;
			}
		}
		
		return null;
	}
	*/
	
	public void MakePurchase(string packageId)
	{
		VirtualStore.Purchase(packageId);	
	}
	
	public void RestorePurchase()
	{
		VirtualStore.Restore();
	}
	#endregion
	
	#region App Events
	public void EnterTitleScreen()
	{
		AppEvents.Report(enterTitleScreen);
	}

	public void ShowFeatured()
	{
		AppEvents.Report(featured);
	}
	
	public bool HasMoreApps()
	{
		return true;
//		return Takeovers.CheckAvailability(moreapps).AvailableCount > 0;	
	}
	
	public void ShowMoreApps()
	{
		AppEvents.Report(moreapps);
	}
	
	public bool HasBannersAds()
	{
		return true;
//		return Takeovers.CheckAvailability(adZone).AvailableCount > 0;	
	}
	
	
	public void ShowBannerAds()
	{	
		if (callback.BannerAdResponse != null)
		{
			Log.Debug("banner response is HERE!");
			callback.BannerAdResponse.BannerAdView.Show(NativeUIView.UIPosition.TOP_CENTER);
		}
		else
		{
			Log.Debug("banner response is null...");
			AppEvents.Report(adZone);
		}
	}
	
	public void HideBannerAds()
	{
		if (callback.BannerAdResponse != null)
			callback.BannerAdResponse.BannerAdView.Hide();
	}
	
	public void RemoveBannerAds()
	{
		if (callback.BannerAdResponse != null)
			callback.BannerAdResponse.BannerAdView.Remove();
	}
	
	public bool HasOffers()
	{
		return true;
//		return Takeovers.HasTakeover(offers);
	}
	
	public void ShowOffers()
	{
		AppEvents.Report(offers);
	}
	
	public void ShowCustomerSupport()
	{
		AppEvents.Report(customerSupport);	
	}
	
	public void CompletePPA(PPA ppa)
	{
#if !UNITY_EDITOR
		string ppaName = Enum.GetName(typeof(PPA), ppa);
		
		Log.Debug("Completed PPA: " + ppaName);
		AppEvents.Report(ppaName);
#endif
	}
	
	public void LogFlurryEvent(FlurryEvent e)
	{
		LogFlurryEventWithParams(e, null);
	}
	
	public void LogFlurryEventWithParams(FlurryEvent e, Hashtable param)
	{
		#if !UNITY_EDITOR		
		string eventName = Enum.GetName(typeof(FlurryEvent), e);		
		Log.Debug("Log Flurry Event : " + eventName);
		if (param != null)
		{
			Log.Debug("Log Flurry Event with params: " + param.toJson());
			AppEvents.Report(eventName, param);
		}
		else
			AppEvents.Report(eventName);
		#endif
	}
	#endregion
}

using UnityEngine;
using System.Collections;

public class ClothShopScene : MonoBehaviour {
	
	GameObject newCharacter;
	
	// Use this for initialization
	IEnumerator Start ()
	{
		newCharacter = (GameObject) Instantiate((Resources.Load("CharacterAssets/FEMALE") as GameObject));
		newCharacter.name = "FEMALE";
		newCharacter.AddComponent<CharacterManager>();
		
		yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
		//newCharacter.GetComponent<CharacterManager>().ChangeAnimation("Take 001");
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
		//StartCoroutine(testChange());
		
	}
	
	IEnumerator testChange()
	{
		yield return new WaitForSeconds(2);
		
		//newCharacter.GetComponent<CharacterManager>().ChangeCharacter(Characters.MALE);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class FriendsHomeScene : MonoBehaviour, PhotoshotPreviewPopupCallbackInterface, PopupManagerCallbackInterface
{
	GameObject newCharacter, friendFemale;
	GameObject boyfriendModel;
	
	public UISysFontLabel photoshotLabel;
	public GameObject flashPanel;
	public GameObject topMenu;
	public GameObject[] page1;
	public GameObject page2;
	public GameObject backButton;
	public UISysFontLabel friendName;
	public TitleShower titleShower;
	int screenShotNumber;
	
	GameObject peripheralItem, peripheralChild;
	bool shouldGenFriend = true;
	bool haveBf = false;
	bool givingGift = false;
	
	
	private void ValidiateFriendsWearingItems()
	{
		if (VisitFriend.friend.wearingItems != null)
		{			
			//Check for Facial Items and bra + panties
			List<ItemSubcategory> missingSubcats = CharacterManager.CheckForMissingStandardSubcategory(VisitFriend.friend.wearingItems);
			if (missingSubcats != null && missingSubcats.Count > 0)
			{
				Debug.Log("missing subcats: "+missingSubcats.Count);
				
				for (int i = 0;i < missingSubcats.Count; i++)
				{
					ItemSubcategory missingSubcat = missingSubcats[i];
					Item item = CharacterManager.GenerateRandomDefaultWearingItemForSubcategory(missingSubcat);
					VisitFriend.friend.wearingItems.Add(item);
				}
			}
		}
	}

	IEnumerator Start()
	{
		ValidiateFriendsWearingItems();
		//VisitFriend.friend.wearingItems

		if (VisitFriend.friend.PlayerDLCVersion > Globals.currentDLCVersion)
		{
			DownloadManager.SharedManager.AskForDLCUpgrade(false, VisitFriend.friend.PlayerDLCVersion, 0, 2, true);
			//PopupManager.ShowInformationPopup(Language.Get("msg_needdlc"), false);
			shouldGenFriend = false;
		}
		
		if (Boyfriends.GetBoyfriend(VisitFriend.friend.CurrentBoyfriendId) != null)
		{
			if (Boyfriends.GetBoyfriend(VisitFriend.friend.CurrentBoyfriendId).dlcId <= Globals.currentDLCVersion)
			{
				haveBf = true;
			}
			else
			{
				Log.Debug("the bf need dlc");
			}
		}
		
		// load YOU
		/*if (GameObject.Find("FEMALE") == null)
		{
			newCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			newCharacter.name = "FEMALE";
			newCharacter.AddComponent<CharacterManager>();
			newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference" + (haveBf?"3A":"1")));
			yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
			//DontDestroyOnLoad(newCharacter);
		}		
		else
		{
			newCharacter = GameObject.Find("FEMALE");
			newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"+ (haveBf?"3A":"1")));
			yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
		}*/
		newCharacter = Globals.globalCharacter;
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"+ (haveBf?"3A":"1")));
		yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
	
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("visitL_idle");
		
		
		if (shouldGenFriend)
		{
			friendFemale = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			friendFemale.transform.localPosition = new Vector3(200000f,200000f,200000f);
			friendFemale.name = "FRIENDFEMALE";
			friendFemale.AddComponent<CharacterManager>();
			friendFemale.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference" + (haveBf?"3B":"2")));
			yield return StartCoroutine(friendFemale.GetComponent<CharacterManager>().EnumGenerateCharacter(VisitFriend.friend));
			
			//StartCoroutine(LoopAnimations(friendFemale));		
			
			
			if (haveBf)
			{
				yield return StartCoroutine(GetBoyfriendMesh(Boyfriends.GetBoyfriend(VisitFriend.friend.CurrentBoyfriendId)));
				if (boyfriendModel == null)
				{
					//boyfriend need DLC
					haveBf = false;
					DownloadManager.SharedManager.AskForDLCUpgrade(false, VisitFriend.friend.CurrentBoyfriendId, 0);
				}
				else
				{	
					boyfriendModel.transform.localPosition = new Vector3(200000f,200000f,200000f);		
					boyfriendModel.name = "FRIENDBF";

					boyfriendModel.AddComponent<CharacterManager>();
					boyfriendModel.GetComponent<CharacterManager>().InsertShadow();
					boyfriendModel.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference3C"));
					boyfriendModel.GetComponent<CharacterManager>().InsertAnimation(RandomIdleAnimationString("b")+"_b");
					
					/*if (boyfriendModel.animation.GetClip("BF_idle_b") == null)
						boyfriendModel.animation.AddClip(Resources.Load("CharacterAssets/Animation/BF_idle_b") as AnimationClip,"BF_idle_b");
					
					boyfriendModel.animation.wrapMode = WrapMode.Loop;
					boyfriendModel.animation.Play("BF_idle_b");*/			
				}
			}			
			
			//Log Flurry Event
			MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Visit_Friends);
		}
		
		
		if (VisitFriend.friend != null)
		{
			friendName.Text =  Language.Get("friend_shome").Replace("%1", VisitFriend.friend.Name);//VisitFriend.friend.GameId +"'s Home"; // TODO: player name
			titleShower.Refresh(VisitFriend.friend);
		}
		
		//Change the BackButton function
		if (VisitFriend.fromScene.Equals("STREET"))
		{
			backButton.GetComponent<UIButtonMessage>().functionName = "GoToHomeScene";
		}
		else if (VisitFriend.fromScene.Equals("FRIENDS"))
		{
			backButton.GetComponent<UIButtonMessage>().functionName = "GoToFriendScene";	
		}
		//backButton.GetComponent<SceneManager>().goToScene = 

		GoToPage1();
		
		SoundManager.SharedManager.ChangeMusic("SuperstarStory_home_remix0");
		
		//debug
		//PopupManager.ShowDLCAskPopup(this, false);
		PopupManager.SetCallback(this);
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.FRIENDSHOME);
	}
	
	void OnDestroy()
	{
		PopupManager.RemoveCallback(this);
	}
	
	public void ChangePose()
	{
		Log.Debug("Change Pose");
		
		//ShootPhoto();
	}
	
	public void ShootPhoto()
	{
		SoundManager.SharedManager.PlaySFX("take_photo");

		TweenAlpha ta = flashPanel.GetComponent<TweenAlpha>();
		ta.enabled = true;

		QuestFlags.SetFlag(6, true);
	}
	
	public void OnFlashed()
	{
		Log.Debug("On Flash Finished");
		TweenAlpha ta = flashPanel.GetComponent<TweenAlpha>();
		Destroy(ta);
		
		TweenAlpha newTa = flashPanel.AddComponent<TweenAlpha>();
		newTa.delay = 0.0f;
		newTa.duration = 0.3f;
		newTa.from = 1;
		newTa.to = 0;
		newTa.enabled = true;
		newTa.eventReceiver = this.gameObject;
		newTa.callWhenFinished = "OnFlashFinished";
	}
	
	public void OnFlashFinished()
	{
		//Clear the exist TweenAlpha
		TweenAlpha ta = flashPanel.GetComponent<TweenAlpha>();
		Destroy(ta);
		
		//Restore to normal TweenAlpha
		TweenAlpha newTa = flashPanel.AddComponent<TweenAlpha>();
		newTa.delay = 0.05f;
		newTa.duration = 0.25f;
		newTa.from = 0;
		newTa.to = 1;
		newTa.enabled = false;
		newTa.eventReceiver = this.gameObject;
		newTa.callWhenFinished = "OnFlashed";
		
		//PopupManager.ShowLoadingFlower();
		StartCoroutine(ShootPhotoRoutine());		
	}
	
	private void SetShowPhotoshotUI(bool show)
	{
		topMenu.SetActive(show);
		for (int i = 0; i < page2.transform.childCount; i++)
		{
			Transform t = page2.transform.GetChild(i);
			if (!show)
			{
				if (t.gameObject.name.Equals(photoshotLabel.transform.parent.name))
					continue;
			}
			else
			{
				if (t.gameObject.name.Equals("Change Pose Button"))
					continue;
			}
			
			t.gameObject.SetActive(show);
		}
	}
	
	
	
	public IEnumerator ShootPhotoRoutine()
	{
		SetShowPhotoshotUI(false);
		//Log.Debug("Shoot Photo");
		
		yield return new WaitForEndOfFrame();
		
        Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        tex.Apply();
        
		PopupManager.ShowPhotoshotPreviewPopup(this, tex, false);
		
		/*
		screenShotNumber = UnityEngine.Random.Range(1,1000000);
		
		//string filePath = Application.persistentDataPath + "/SuperstarStory/";
		//string fileName = filePath + System.DateTime.Now.Second + ".png";
		
		
		//string[] fileNames = Directory.GetFiles(Application.persistentDataPath + "/SuperstarStory/");
		
		string storagePath 	= UtilsHelper.GetWriteableDirectory();
		string filePath 	= storagePath + Path.DirectorySeparatorChar + Globals.appName + Path.DirectorySeparatorChar;
		string fileName 	= filePath + System.DateTime.Now.Ticks + ".png";
		
		if (!Directory.Exists(filePath))
			Directory.CreateDirectory(filePath);
		File.WriteAllBytes(fileName, imageBytes);
		UtilsHelper.RefreshDirectoryWithFileName(fileName);
		
		//Log.Debug("Shoot id: "+screenShotNumber);
		//Application.CaptureScreenshot("Screenshot"+screenShotNumber+".png");
		
		//StartCoroutine("CheckScreenshotFinished");
		//QuestFlags.var6_shootPhoto = true;
		QuestFlags.SetFlag(6,true);
		
		topMenu.SetActive(true);
		
		GoToPage2();*/
	}
	
	public void GoToPage1()
	{
		/*foreach (Transform child in page1.GetComponentsInChildren<Transform>())
			child.gameObject.SetActive(true);
			*/
		foreach (GameObject go in page1)
		{
			go.SetActive(true);
		}
		
		page2.SetActive(false);
		/*
		foreach (Transform child in page2.GetComponentsInChildren<Transform>())
			child.gameObject.SetActive(false);*/
		
		
		
		
		if (shouldGenFriend)
		{
			bool haveGift = false;

			VisitRecord r = VisitRecord.FindRecordOfId(VisitFriend.friend.GameId);

			if (r != null)
			{
				if ((int) ((TimeSpan)(DateTime.UtcNow -  r.lastVisitTime)).TotalDays >= 1)
				{
					haveGift = true;
				}
			}
			else
				haveGift = true;

			if (haveGift)
			//if (!Globals.visitedGameIds.Contains(VisitFriend.friend.GameId))
			{
				//here we choose to change immediately, without crossfade
				newCharacter.GetComponent<CharacterManager>().PlayAnimation("visitL_gift");
				friendFemale.GetComponent<CharacterManager>().PlayAnimation("visitR_gift");
				//newCharacter.GetComponent<CharacterManager>().InsertAnimation("visitL_gift");
				//friendFemale.GetComponent<CharacterManager>().InsertAnimation("visitR_gift");
				
				if (peripheralItem == null)
				{
					peripheralItem = (GameObject) Instantiate((Resources.Load("CharacterAssets/Peripheral/giftbox_obj") as GameObject));
					//peripheralItem = tempPeri.transform.FindChild("DM_giftbox_ALL").gameObject;
		
					peripheralChild = peripheralItem.transform.FindChild("DM_giftbox_ALL").gameObject;
					
					peripheralChild.transform.parent = friendFemale.transform;
					//peripheralItem.transform.parent = friendFemale.transform;
					peripheralItem.animation.AddClip(Resources.Load("CharacterAssets/Animation/visitR_gift") as AnimationClip,"visitR_gift");
					
				}
				
				peripheralItem.SetActive(true);
				peripheralChild.SetActive(true);
				peripheralItem.animation.playAutomatically = false;
				peripheralItem.animation.Play("visitR_gift");
				
				
				//StartCoroutine(DelayedShowGift());
				//
				//Destroy(tempPeri);
				//tempPeri = null;
			}
			else
			{
				newCharacter.GetComponent<CharacterManager>().InsertAnimation("visitL_idle");
				friendFemale.GetComponent<CharacterManager>().InsertAnimation("visitR_idle");
				
				
			}
			
			friendFemale.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference" + (haveBf?"3B":"2")));
		
			if (haveBf)
				boyfriendModel.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference3C"));
		}
		
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference" + (haveBf?"3A":"1")));
		
		Log.Debug("go to page 1");
	}
	
	public void GoToPage2()
	{
		if (givingGift) return;
		if (!shouldGenFriend) return;
		
		foreach (GameObject go in page1)
		{
			go.SetActive(false);
		}
		page2.SetActive(true);
		
		if (peripheralItem != null)
			peripheralItem.SetActive(false);
		if (peripheralChild != null)
			peripheralChild.SetActive(false);
		
		if (VisitFriend.friend != null && photoshotLabel != null)
		{
			string myName 		= Globals.mainPlayer.Name;
			string friendName 	= VisitFriend.friend.Name;
			Log.Debug("My Name: " + myName);
			Log.Debug("Friends Name: " + friendName);
			photoshotLabel.Text = myName + " & " + friendName;
		}
		
		/*if (peripheralItem != null)
		{
			Destroy(peripheralItem);
			peripheralItem = null;
		}*/
		
		/*foreach (Transform child in page1.GetComponentsInChildren<Transform>())
			child.gameObject.SetActive(false);

		foreach (Transform child in page2.GetComponentsInChildren<Transform>())
			child.gameObject.SetActive(true);*/
		
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("visitL_photo");
		friendFemale.GetComponent<CharacterManager>().InsertAnimation("visitR_photo");
		
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference4A"));
		friendFemale.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference4B"));
		
		if(haveBf)
			boyfriendModel.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference4C"));
		Log.Debug("go to page 2");
	}
	
	IEnumerator DelayedShowGift()
	{
		yield return new WaitForSeconds(0.618f);
		peripheralItem.SetActive(true);
		peripheralChild.SetActive(true);
	}
	
	/*
	IEnumerator CheckScreenshotFinished()
	{
		float timeout = 0.0f;
		float timeoutMax = 4.0f;
		
		
		
		while (timeout < timeoutMax)
		{
			string filePath 	= Path.Combine(Application.persistentDataPath, "Screenshot"+screenShotNumber+".png");
			FileInfo fileInfo 	= new FileInfo(filePath);
			if (fileInfo != null && fileInfo.Exists)
			{
				Log.Debug("has photo 1!!");
				
				
				
				topMenu.SetActive(true);
				GoToPage2();
				
				PopupManager.ShowInformationPopup("Screenshot Taken!", false);
				
				break;
			}
			else if (System.IO.File.Exists("Screenshot"+screenShotNumber+".png"))
			{
				Log.Debug("has photo 2!!");
				
				
				
				topMenu.SetActive(true);
				GoToPage2();
				
				PopupManager.ShowInformationPopup("Screenshot Taken!", false);
				
				break;
			}
			else if  (System.IO.File.Exists(Path.Combine( Application.persistentDataPath, Path.Combine( Application.persistentDataPath,"Screenshot"+screenShotNumber+".png"))))
			{
				Log.Debug("has photo 3!!");
				
				
				
				topMenu.SetActive(true);
				GoToPage2();
				
				PopupManager.ShowInformationPopup("Screenshot Taken!", false);
				
				break;
			}
			else
			{
				yield return new WaitForSeconds(0.2f);
				timeout += 0.2f;
				
				if (timeout >= timeoutMax)
				{
					Log.Debug("save photo fail");
					
					topMenu.SetActive(true);
					GoToPage2();
					break;
				}
			}
		}
		
	}
	*/
	
	public void GiftTapped()
	{
		if (shouldGenFriend)
		{
			//Globals.visitedGameIds.Add(VisitFriend.friend.GameId);
			VisitRecord vr = VisitRecord.FindRecordOfId(VisitFriend.friend.GameId);

			if (vr != null)
			{
				if ((int) ((TimeSpan)(DateTime.UtcNow -  vr.lastVisitTime)).TotalDays >= 1)
				{
					vr.lastVisitTime = DateTime.UtcNow;
					StartCoroutine(GetGiftRoutine());
				}
			}
			else
			{
				Globals.visitedGameIds.Add(new VisitRecord(VisitFriend.friend.GameId, 
				                                           DateTime.UtcNow));

				StartCoroutine(GetGiftRoutine());
			}


		}
	}
	
	public IEnumerator GetGiftRoutine()
	{
		// open box....
		givingGift = true;
		
		peripheralItem.animation.AddClip(Resources.Load("CharacterAssets/Animation/visitR_gift") as AnimationClip,"visitR_giftopen");
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("visitL_giftopen",false);
		friendFemale.GetComponent<CharacterManager>().InsertAnimation("visitR_giftopen",false);
		peripheralItem.animation.Play("visitR_giftopen");
		
		yield return new WaitForSeconds(1.4f);
		
		SoundManager.SharedManager.PlaySFX("gift_open");
		
		yield return new WaitForSeconds(1.4f);
		
		
		Reward r = null;
			
		if (Globals.visitedGameIds.Count <= 25)
		{
			r = new Reward(1, 1, 0, 2, 0, 0, "", new Skill(new int[]{0,0,0,0,0}));
		}
		else
		{
			r = new Reward(1,1,50,0,0,0,"", new Skill(new int[]{0,0,0,0,0}));
		}
		PopupManager.ShowRewardPopup(r, false);


		
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("visitL_idle");
		friendFemale.GetComponent<CharacterManager>().InsertAnimation("visitR_idle");
		peripheralItem.SetActive(false);
		peripheralChild.SetActive(false);
		
		givingGift = false;
	}
	
	public void TitleShowerClicked()
	{
		if (!givingGift)
		{
			PopupManager.ShowAchievementListPopup(SkillTable.GetSkillTier(VisitFriend.friend.TitleSkill).skillType+1,VisitFriend.friend, false);
		}
	}
	
	/*IEnumerator PlayAnimation(GameObject target, string name, float duration)
	{
		//target.GetComponent<CharacterManager>().ChangeAnimation(name, false);
		yield return new WaitForSeconds(duration);
	}*/

	/*public IEnumerator LoopAnimations(GameObject target)
	{
		int seed = UnityEngine.Random.Range(1,10+1);
		switch(seed)
		{	
			case 1:
				yield return StartCoroutine(PlayAnimation(target,"street_rotate", 2.667f));
			break;
			case 2:
				yield return StartCoroutine(PlayAnimation(target,"street_head", 1.667f));
			break;
			default:
				yield return StartCoroutine(PlayAnimation(target,"pose0"+UnityEngine.Random.Range(1,2+1), 6.0f));
			break;
		}
		StartCoroutine(LoopAnimations(target));
	}*/
	
	public void GoToHomeScene()
	{
		if (givingGift) return;
		SceneManager.SharedManager.LoadScene(Scenes.STREET, true);			
	}
	
	public void GoToFriendScene()
	{
		if (givingGift) return;
		
		SceneManager.SharedManager.LoadScene(Scenes.HOME, true);
		//SceneManager.SharedManager.LoadScene(Scenes.FRIENDS, true);	
	}
	
	#region PopupManager Callback
	public void OnPopupManagerHidePopup(GameObject popup)
	{
		if (popup.name.Equals("PhotoshotPreviewPopup"))
		{
			SetShowPhotoshotUI(true);
		}
	}
	
	public void OnPopupManagerShowPopup(GameObject popup){}
	#endregion
	
	#region PhotoshotPopup Listener
	public void OnPhotoshotPreviewPopupSave()
	{
		Hashtable param = new Hashtable();
		param["Save"] = "Friends";
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Camera_TakePhoto, param);
		
		Log.Debug("PhotoshotPopup Listener save");
		//SetShowPhotoshotUI(true);
		
		///PopupManager.ShowInformationPopup(Language.Get("msg_photosaved"), true);
	}
	
	public void OnPhotoshotPreviewPopupDiscard()
	{		
		Log.Debug("PhotoshotPopup Listener Discard");
		//SetShowPhotoshotUI(true);
	}
	
	public void OnPhotoshotPreviewPopupShare()
	{		
		Log.Debug("PhotoshotPopup Listener Share");
	}
	#endregion
	
	private IEnumerator GetBoyfriendMesh(Boyfriend bf)
	{
		GameObject returnMe = null;
		
		UnityEngine.Object obj = Resources.Load("Boyfriends/"+bf.meshName);
			
		if (obj == null)
		{
			// try load from streaming assets folder instead
			string filePath 	= Application.persistentDataPath + "/"  + bf.meshName +".dlc";
			FileInfo fileInfo 	= new FileInfo(filePath);
		    if (fileInfo != null && fileInfo.Exists)
		    {
		   
				WWW bundle = new WWW("file://" + filePath);
				yield return bundle;
				
				//Log.Debug("am i here?" +item);
				if (bundle.error != null)
				{
					Log.Debug("FriendsHome Cannot load the boyfriend "+ bf.meshName + " from path: " + filePath);
				}
				else
				{
					if (bundle.assetBundle != null)
					{
						returnMe = (GameObject) Instantiate(bundle.assetBundle.mainAsset as GameObject);				
						bundle.assetBundle.Unload(false);					
						
						//Re-assign the shaders to the boyfriend model						
						SkinnedMeshRenderer[] smrs = returnMe.GetComponentsInChildren<SkinnedMeshRenderer>();
						for (int i = 0; i < smrs.Length; i++)
						{
							SkinnedMeshRenderer smr = smrs[i];
							Material[] materials 		= smr.materials;
							for (int j = 0; j < materials.Length; j++)
							{
								Material material 	= materials[j];
								string shaderName	= material.shader.name.Replace(" ", "").Replace("/", "").Replace("-", "");
								Shader shader 		= Resources.Load("Shaders/" + shaderName) as Shader;
								if (shader != null)
									material.shader	= shader;
								else
									material.shader = Shader.Find(material.shader.name);
							}						
						}	
					}
					else
					{
						Log.Debug("FriendsHome the boyfriend "+ bf.meshName + " asset bundle is null from path: " + filePath);	
					}
				}
		    }
			else
			{
				Log.Warning("PATH FAIL:"+ bf.meshName);
			}
			
		}
		else
		{
			returnMe = (GameObject) Instantiate(obj as GameObject);
		}
		
		boyfriendModel = returnMe;
	}
	
	string RandomIdleAnimationString(string t)
	{
		int ran = UnityEngine.Random.Range(1,5+1);
		
		if (t.Equals("b") && VisitFriend.friend.CurrentBoyfriendId == 7) return (ran%2 == 0? "BF_pose02":"BF_idle");
		
		switch(ran)
		{
			//abcde
			
			case 1: return "BF_pose01";
			case 2: return "BF_pose02";
			default: return "BF_idle"; 
			
		}
		
		return "BF_idle";
	}
}



using UnityEngine;
using System.Collections;

public class FlirtTriggerAnimator : MonoBehaviour
{
	public GameObject[] sprites = new GameObject[3];
	public float period = 0.6f;
	
	int spriteNo;
	float perPeriod;

	// Use this for initialization
	void Start ()
	{
		perPeriod = period / 3f;
		StartCoroutine(UpdateAtInterval());
	}
	
	IEnumerator UpdateAtInterval()
	{		
		while(true)
		{
			spriteNo = (spriteNo+1) % 3;
		
			sprites[1].SetActive(spriteNo >= 1);
			sprites[2].SetActive(spriteNo == 2);
			
			
			yield return new WaitForSeconds(perPeriod);
		}
	}
	
}


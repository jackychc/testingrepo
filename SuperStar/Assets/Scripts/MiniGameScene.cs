using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MiniGameScene : MonoBehaviour
{
	public GameObject moreappsObject;
	
	
	// Use this for initialization
	void Start ()
	{
		//Check moreapps enabled
		if (!MunerisManager.SharedManager.HasMoreApps())
		{
			moreappsObject.SetActive(false);
		}
			
		
		Log.Debug(SoundManager.SharedManager.BGMVolume);
		//if (SoundManager.SharedManager.CurrBGMState.Equals(SoundManager.BGMState.FADEOUT) || SoundManager.SharedManager.CurrBGMState.Equals(SoundManager.BGMState.STOP))
		{
			SoundManager.SharedManager.ChangeMusic("SuperstarStory_home_remix0");
		}
		
		//Log.Debug("Has banner?"+MunerisManager.SharedManager.HasBannersAds());
		//MunerisManager.SharedManager.ShowBannerAds();
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.MINIGAME);	
	}
	
	public void MoreGamesClicked()
	{
		Log.Debug("more apps clicked! has more?" + MunerisManager.SharedManager.HasMoreApps());
		MunerisManager.SharedManager.ShowMoreApps();
	}
	
	public void GoToCoinDropScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAMECOINDROP, false);	
	}
	
	public void GoToClothMatchScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.MINIGAMECLOTHMATCH, false);	
	}
	
	public void GoToStreetScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.STREET, true);	
	}
}


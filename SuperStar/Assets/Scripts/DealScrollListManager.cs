using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class DealScrollListManager : MonoBehaviour {
	
	public GameObject scrollPanelT;
	
	
	
	
	int previousSort = 0;
	float yPos = 0f;

	Feature shopFeature = null;
	Poster poster = null;
	
	//List<DealItem> items;
	
	
	// Use this for initialization
	void Start () {

		GetPoster();
		GetShopFeatureId();
		LoadItems();
		
	}

	public void GetPoster()
	{
		poster = Posters.GetPosterOfShop(VisitShop.shop);
	}

	public void GetShopFeatureId()
	{
		shopFeature = Features.GetFeatureOfShop(VisitShop.shop);
	}	

	
	public void LoadItems()
	{
		
		
		
		
		Transform itemPanel = scrollPanelT.transform;
		

		yPos = 122f;
		
		// loop through all player items, add!
		for (int i =0 ; i < VisitShop.shop.items.Count; i++)
		{
			Item item = VisitShop.shop.items[i];
			
			//if (Globals.mainPlayer.owningItems.Contains(item)) continue;
			
			//items.Add(go.GetComponent<DealItem>());
			
			Feature f = null;
			// do the feature stuff
			if (shopFeature != null)
			{
				f = shopFeature;
			}
			else if (Features.GetFeatureOfItem(item) != null)
			{
				f = Features.GetFeatureOfItem(item);	
			}
			
			GameObject go = (GameObject) Instantiate(Resources.Load ("DealItem") as GameObject);

			go.name = item.itemName;
			go.transform.parent = itemPanel;

			go.GetComponent<DealItem>().item = item;
			go.GetComponent<DealItem>().sortOrder = i+1;
			go.transform.localPosition = new Vector3(0, yPos,0f);
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = new Vector3(1f,1f, 1f);

			go.GetComponent<DealItem>().offerCoins = item.price;
			go.GetComponent<DealItem>().offerGems = item.gems;



			if (f != null)
			{			
				go.GetComponent<DealItem>().feature = f;
				
				if (f.discount > 0)
				{
					go.GetComponent<DealItem>().discount = f.discount;
					go.GetComponent<DealItem>().offerCoins = (int) (item.price * ((100-f.discount)/100f));
					go.GetComponent<DealItem>().offerGems = (int) (item.gems * ((100-f.discount)/100f));
				}
				else if (f.gems > 0)
				{
					go.GetComponent<DealItem>().offerGems = f.gems;
				}
				else if (f.coins > 0)
				{
					go.GetComponent<DealItem>().offerCoins = f.coins;
				}
			}

			if (poster != null)
			{
				go.GetComponent<DealItem>().poster = poster;
			}

			if (i != VisitShop.shop.items.Count-1)
				yPos -= 173f;

			/*else
			{
				if (f.gems > 0)
				{
					go.GetComponent<DealItem>().offerGems = item.gems;
				}
				else if (f.coins > 0)
				{
					go.GetComponent<DealItem>().offerCoins = item.price;
				}
			}*/

			
		}
		
		
		//itemPanel.GetComponent<UIGrid>().repositionNow = true;
		
	}
	
	
	public void goUp()
	{
		Log.Debug("going up");
		relativeItem(-5);
	}
	public void goDown()
	{
		Log.Debug("going down");
		relativeItem(5);
	}
	
	public void relativeItem(int position)
	{
		/*itemCenter.Recenter(); // get the center

		float springAmount = -itemCenter.centeredObject.transform.localPosition.y + (204f* position);
		Log.Debug("spring? = "+springAmount);
		springAmount = Mathf.Min(Mathf.Max(0f, springAmount), -yPos);
		Log.Debug("spring after? = "+springAmount);
		SpringPanel.Begin(scrollPanelT, new Vector3(0f,springAmount,0f), 5f);*/
		 
	}
	
	
	/*public void UpdateExpireTimer()
	{
		foreach (DealItem i in items)
		{
			if (i.feature != null)
			{
				if (i.feature.isRunning())
				{
					i.UpdateExpireTime(i.feature.CalculateRemainSeconds());
				}
				else
				{
					i.UpdateExpireTime(0);
				}
			}
		}
	}
	*/
	
}

using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class BoyfriendListItem : MonoBehaviour {
	
	public BoyfriendScrollListManager bsm;
	public Boyfriend boyfriend;
	
	public GameObject lockBlock;
	public GameObject inviteButton;
	public GameObject avatar;
	public GameObject leaveButton;
	public UILabel bfName;
	public UILabel bfTitle;
	public UISprite bg;
	public UITexture icon;
	public UISprite[] gift = new UISprite[3];
	
	
	// Use this for initialization
	void Start ()
	{
		//a very important line of code
		Destroy(transform.GetComponent<UIPanel>());
		
		SetInfo();
		
		
		//friendNameLabel.GetComponent<UISysFontLabel>().Text = friendName;
	}
	
	public void SetBgType(int type)
	{
		if (type == 1)
		{
			bg.spriteName = "bfList_roll_a";
		}
		else
		{
			bg.spriteName = "bfList_roll_b";
		}
	}
	
	public void SetInfo()
	{
		StartCoroutine(SetInfoRoutine());
	}
	
	public IEnumerator SetInfoRoutine()
	{
		if (boyfriend.rewardTimer == -1)
		{
			// not yet unlocked the bf
			leaveButton.SetActive(false);
			inviteButton.SetActive(false);
			lockBlock.SetActive(true);
			
			avatar.SetActive(true);
			//icon.gameObject.SetActive(false);
			bfName.text = "???";
			bfTitle.text = "???";
		}
		else
		{
			leaveButton.SetActive(false);
			inviteButton.SetActive(true);
			lockBlock.SetActive(false);
			
			avatar.SetActive(false);
			bfName.text = boyfriend.boyfriendName;
			bfTitle.text = boyfriend.title;
			
			
		}
		
		icon.mainTexture= Boyfriends.GetIconOfBoyfriend(boyfriend);//(Texture2D) Resources.Load("Boyfriends/itemIcon/"+boyfriend.boyfriendName);
		
		if (icon.mainTexture == null)
		{
			yield return StartCoroutine(LoadIconFromPersistent(boyfriend, result => icon.mainTexture = result));
		}
		
		if (Globals.mainPlayer.CurrentBoyfriendId == boyfriend.boyfriendId)
		{
			leaveButton.SetActive(true);
			inviteButton.SetActive(false);
			lockBlock.SetActive(false);
		}
		
		for (int i = 0; i < 3; i++)
		{
			if (boyfriend.rewardClaimed[i] == 1)
			{
				// sprite change..
				gift[i].spriteName = "bf_gift_unlock";
			}
		}
		
		yield return null;
	}
	
	
	public void InviteClicked()
	{
		Log.Debug("invite clicked, boyfriend id " + boyfriend.boyfriendId);
		//TODO: set the boyfriend model

		Log.Debug("BF List Item Boyfriend invited: " + boyfriend.boyfriendId);
		Globals.mainPlayer.CurrentBoyfriendId = boyfriend.boyfriendId;
		
		GameObject homeInit = GameObject.Find("Init");
		if (homeInit != null)
		{
			HomeScene homeScene = homeInit.GetComponent<HomeScene>();
			if (homeScene != null)
			{
				homeScene.giftThinkingObj.SetActive(false);
				homeScene.RegenCharacters();
			}
		}
		
		
		PopupManager.HideLatestPopup();
	}
	
	public void LeaveClicked()
	{
		Log.Warning("Leave Clicked");
		
		/*Globals.mainPlayer.CurrentBoyfriendId = -1; // !?
		
		GameObject.Destroy(GameObject.Find("BOYFRIEND"));
		
		GameObject homeInit = GameObject.Find("Init");
		if (homeInit != null)
		{
			if (homeInit.GetComponent<HomeScene>() != null)
				homeInit.GetComponent<HomeScene>().ReallyStart();
		}
		
		PopupManager.HideLatestPopup();*/
		
		PopupManager.ShowThrowBoyfriendPopup(false);
		PopupManager.HideLatestPopup();
	}
	
	public IEnumerator LoadIconFromPersistent(Boyfriend i, Action<Texture2D> returnMe)
	{
		WWW loader = new WWW("file://" + Application.persistentDataPath + Path.DirectorySeparatorChar + "bf_" + i.boyfriendId + "_icon.png");
		yield return loader;
		
		if (loader.error != null)
		{
			Log.Debug("BF List Item Cannot open the icon file " + i.boyfriendId + " icon");
		}
		else
		{
			if (loader.texture != null)
			{
				returnMe((Texture2D)loader.texture);
			}
			else
			{
				Log.Debug("BF List Item Cannot load the texture from assetbundle " + i.boyfriendId + " icon");
			}
		}
	}
}

using UnityEngine;
using System.Collections;

public class LittleLampLooper : MonoBehaviour
{
	public int lamps = 25;
	public int lampSeparation = 2;
	GameObject[] lampsGO;
	public float speed = 0.3f;
	
	int loopIndex;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine(DelayedStart());
	}
	
	IEnumerator DelayedStart()
	{
		yield return new WaitForSeconds(0.015f);
		
		lampsGO = new GameObject[lamps];
		
		for (int i = 1; i <= lamps; i++)
		{
			lampsGO[i-1] = transform.FindChild("Lamp"+i).gameObject;
		}
		
		InvokeRepeating("LampsLoop", 0f, speed);
	}
	
	void LampsLoop()
	{
		
		
		loopIndex = (loopIndex+1)%lampSeparation;
		for (int i = 1; i <= lamps; i++)
		{
			lampsGO[i-1].SetActive((i%lampSeparation) == loopIndex);
		}
		
	}
	
	
}


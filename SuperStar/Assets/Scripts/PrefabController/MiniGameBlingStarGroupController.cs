using UnityEngine;
using System.Collections;

public class MiniGameBlingStarGroupController : MonoBehaviour {
	
	public GameObject BlingStarGroup;
	public GameObject clothMathButton;
	public UISprite clothMathButtonSprite;
	
	public GameObject dropCoinButton;
	public UISprite dropCoinButtonSprite;
	
	public float startDuration;
	public float endDuration;
	public float startDelay;
	public float endDelay;
	
	public int NumOfStars;
	
	private ArrayList blingStarDropCoinQueue = new ArrayList();
	private ArrayList blingStarClothMathQueue = new ArrayList();
	
	private object blingStarDropCoinTicket = new object();
	private object blingStarClothMatchTicket = new object();
	
	// Use this for initialization
	void Start () {
		for (int i = 0; i < NumOfStars; i++)
		{
			GameObject star = CreateClothMathBlingStar();		
			blingStarClothMathQueue.Add(star);
		}
		
		for (int i = 0; i < NumOfStars; i++)
		{
			GameObject star = CreateDropCoinBlingStar();		
			blingStarDropCoinQueue.Add(star);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}	
	
	#region Create Bling Star
	private GameObject CreateDropCoinBlingStar()
	{
		//Random Position
		GameObject star = Instantiate(Resources.Load("Prefab/BlingStar")) as GameObject;
		if (star != null)
		{
			TweenScale tweenScale 		= star.GetComponent<TweenScale>();
			tweenScale.eventReceiver 	= this.gameObject;
			tweenScale.callWhenFinished = "OnBlingStarScaleFinishedClothMath";
			tweenScale.duration 		= Random.Range(startDuration, endDuration);
			tweenScale.delay			= Random.Range(startDelay, endDelay);
			
			float spriteWidth = star.transform.localScale.x;
			float spriteHeight = star.transform.localScale.y;			
			float newSpriteWidth = Random.Range(spriteWidth, (float)(spriteWidth * 0.5));
			float newSpriteHeight = newSpriteWidth / (spriteWidth/spriteHeight);
			
			tweenScale.from = new Vector3(newSpriteWidth, newSpriteHeight, 1.0f);
			//tweenRotate.duration = 
			
			float width = dropCoinButtonSprite.transform.localScale.x;
			float height = dropCoinButtonSprite.transform.localScale.y;
			
			Vector2 startPosition = new Vector2((float)(dropCoinButton.transform.localPosition.x - width * 0.5), (float)(dropCoinButton.transform.localPosition.y + height * 0.25));
			Vector2 endPosition = new Vector2((float)(dropCoinButton.transform.localPosition.x + width * 0.5), (float)(dropCoinButton.transform.localPosition.y - height * 0.5));
			
			float positionX = Random.Range(startPosition.x, endPosition.x);
			float positionY = Random.Range(startPosition.y, endPosition.y);
			
			star.transform.parent 			= BlingStarGroup.transform;
			star.transform.localPosition 	= new Vector3(positionX, positionY, 0);
			star.transform.localEulerAngles = new Vector3(0, 0, 0);
			star.transform.localScale 		= new Vector3(1, 1, 1);
		}
		
		return star;
	}
	
	private GameObject CreateClothMathBlingStar()
	{
		GameObject star = Instantiate(Resources.Load("Prefab/BlingStar")) as GameObject;
		if (star != null)
		{
			TweenScale tweenScale 		= star.GetComponent<TweenScale>();
			tweenScale.eventReceiver 	= this.gameObject;
			tweenScale.callWhenFinished = "OnBlingStarScaleFinishedDropCoin";
			tweenScale.duration 		= Random.Range(startDuration, endDuration);
			tweenScale.delay			= Random.Range(startDelay, endDelay);
			
			float width = clothMathButtonSprite.transform.localScale.x;
			float height = clothMathButtonSprite.transform.localScale.y;
			
			Vector2 startPosition = new Vector2((float)(clothMathButton.transform.localPosition.x - width * 0.5), (float)(clothMathButton.transform.localPosition.y + height * 0.25));
			Vector2 endPosition = new Vector2((float)(clothMathButton.transform.localPosition.x + width * 0.5), (float)(clothMathButton.transform.localPosition.y - height * 0.5));
			
			float positionX = Random.Range(startPosition.x, endPosition.x);
			float positionY = Random.Range(startPosition.y, endPosition.y);
			
			star.transform.parent 			= BlingStarGroup.transform;
			star.transform.localPosition 	= new Vector3(positionX, positionY, 0);
			star.transform.localEulerAngles = new Vector3(0, 0, 0);
			star.transform.localScale 		= new Vector3(1, 1, 1);
		}
		
		return star;
	}
	#endregion
	
	#region OnScaleFinished
	private void OnBlingStarScaleFinishedClothMath()
	{
		lock(blingStarClothMatchTicket)
		{
			if (blingStarClothMathQueue.Count > 0)
			{
				GameObject oldStar = (GameObject)blingStarClothMathQueue[0];
				Destroy(oldStar);
				blingStarClothMathQueue.RemoveAt(0);
			}
			
			GameObject star = CreateClothMathBlingStar();
			blingStarClothMathQueue.Add(star);
		}
	}
	
	private void OnBlingStarScaleFinishedDropCoin()
	{
		lock(blingStarDropCoinTicket)
		{
			if (blingStarDropCoinQueue.Count > 0)
			{
				GameObject oldStar = (GameObject)blingStarDropCoinQueue[0];
				Destroy(oldStar);
				blingStarDropCoinQueue.RemoveAt(0);
			}
			
			GameObject star = CreateDropCoinBlingStar();
			blingStarDropCoinQueue.Add(star);
		}
	}
	#endregion
}

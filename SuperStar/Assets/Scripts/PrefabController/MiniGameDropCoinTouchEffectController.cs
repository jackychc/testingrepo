using UnityEngine;
using System.Collections;

public class MiniGameDropCoinTouchEffectController : MonoBehaviour {
	
	public void OnTouchEffectFadedOut()
	{
		Destroy(this.gameObject);
	}
}

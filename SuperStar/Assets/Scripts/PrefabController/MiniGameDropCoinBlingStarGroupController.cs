using UnityEngine;
using System.Collections;

public class MiniGameDropCoinBlingStarGroupController : MonoBehaviour {
	public GameObject FlashArea;
	public GameObject BlingStarGroup;
	
	public float startDuration;
	public float endDuration;
	public float startDelay;
	public float endDelay;
	
	public int NumOfStars;
	
	private ArrayList blingStarQueue = new ArrayList();
	private object blingStarTicket = new object();
	
	// Use this for initialization
	void Start () {
		for (int i = 0; i < NumOfStars; i++)
		{
			GameObject star = CreateBlingStar();		
			blingStarQueue.Add(star);	
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	private GameObject CreateBlingStar()
	{
		GameObject star = Instantiate(Resources.Load("Prefab/BlingStar")) as GameObject;
		if (star != null)
		{
			TweenScale tweenScale 		= star.GetComponent<TweenScale>();
			tweenScale.eventReceiver 	= this.gameObject;
			tweenScale.callWhenFinished = "OnBlingStarScaleFinished";
			tweenScale.duration 		= Random.Range(startDuration, endDuration);
			tweenScale.delay			= Random.Range(startDelay, endDelay);
			
			float spriteWidth = star.transform.localScale.x;
			float spriteHeight = star.transform.localScale.y;
			
			float newSpriteWidth = Random.Range(spriteWidth, (float)(spriteWidth * 0.5));
			float newSpriteHeight = newSpriteWidth / (spriteWidth/spriteHeight);
			
			tweenScale.from = new Vector3(newSpriteWidth, newSpriteHeight, 1.0f);
			
			float width 	= FlashArea.transform.localScale.x;
			float height 	= FlashArea.transform.localScale.y;
			Vector2 startPosition 	= new Vector2((float)(FlashArea.transform.localPosition.x - width * 0.5), (float)(FlashArea.transform.localPosition.y + height * 0.5));
			Vector2 endPosition 	= new Vector2((float)(FlashArea.transform.localPosition.x + width * 0.5), (float)(FlashArea.transform.localPosition.y - height * 0.5));
			
			
			float positionX = Random.Range(startPosition.x, endPosition.x);
			float positionY = Random.Range(startPosition.y, endPosition.y);
			
			star.transform.parent 			= BlingStarGroup.transform;
			star.transform.localPosition 	= new Vector3(positionX, positionY, 0);
			star.transform.localEulerAngles = new Vector3(0, 0, 0);
			star.transform.localScale 		= new Vector3(1, 1, 1);
		}
		return star;
	}

	
	private void OnBlingStarScaleFinished()
	{
		lock(blingStarTicket)
		{
			if (blingStarQueue.Count > 0)
			{
				GameObject oldStar = (GameObject)blingStarQueue[0];
				Destroy(oldStar);
				blingStarQueue.RemoveAt(0);
			}
			
			GameObject star = CreateBlingStar();		
			blingStarQueue.Add(star);
		}
	}
}

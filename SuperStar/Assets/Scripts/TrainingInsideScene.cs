using System;
using UnityEngine;



public class TrainingInsideScene : MonoBehaviour
{
	GameObject newCharacter;
	
	void Start()		
	{
		newCharacter = (GameObject) Instantiate((Resources.Load("CharacterAssets/FEMALE") as GameObject));
		newCharacter.name = "FEMALE";
		newCharacter.AddComponent<CharacterManager>();
		newCharacter.GetComponent<CharacterManager>().GenerateCharacter(Globals.mainPlayer);
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
	}
		
	void Update()
	{
	}
}



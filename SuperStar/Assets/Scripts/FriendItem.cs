using UnityEngine;
using System.Collections;
using System;

public class FriendItem : MonoBehaviour, TwitterManagerCallbackInterface {
	
	public Player friend;
	public GameObject friendName;
	public GameObject friendIdValue;
	public UITexture downloadIcon;
	
	// Use this for initialization
	void Start ()
	{
		//a very important line of code
		Destroy(transform.GetComponent<UIPanel>());
	}
	
	void OnDestroy()
	{
		TwitterManager.SharedManager.RemoveCallback(this);	
	}
	
	public void SetFriend(Player f)
	{
		friend = f;		
		
		friendName.GetComponent<UISysFontLabel>().Text 	= friend.Name;
		friendIdValue.GetComponent<UILabel>().text 		= "ID: "+friend.GameId;
		
		StartCoroutine(SetIcon());
		//StartCoroutine(SetFacebookIcon());
	}
	
	public IEnumerator SetIcon()
	{
		string fbId 		= friend.facebookElement.socialId;
		string twitterId 	= friend.twitterElement.socialId;
		string googlePlusId = friend.googlePlusElement.socialId;
		
		if (fbId != null && !fbId.Equals(""))
		{
			Log.Debug("FriendList Item Friend Type: Facebook");
			string url = FacebookManager.GetProfilePicURL(friend.facebookElement.socialId, 80, 80);
			
			yield return StartCoroutine(SetIcon(url));
		}
		else if (googlePlusId != null && !googlePlusId.Equals(""))
		{
			Log.Debug("FriendList Item Friend Type: GooglePlus");
			
			string url = GooglePlusManager.GetProfilePicURL(friend.googlePlusElement.socialId, 80);
			
			yield return StartCoroutine(SetIcon(url));
			
		}
		else if (twitterId != null && !twitterId.Equals(""))
		{
			Log.Debug("FriendList Item Friend Type: Twitter");
			string url = "";
			
			if (TwitterManager.SharedManager.IsLoggedIn())
			{
				TwitterManager.SharedManager.SetCallback(this);
				TwitterManager.SharedManager.GetUserProfile(new ArrayList(){friend.twitterElement.socialId});
			}
		}
	}

	
	public IEnumerator SetIcon(string url)
	{
		Log.Debug(url);
		WWW www = new WWW(url);
		yield return www;
		
		if (www.error == null)
		{
			Texture2D texture = new Texture2D(80,80);
			texture.LoadImage(www.bytes);
			
			if (texture != null)
			{				
				string shaderName 	= "UnlitTransparentColored";
				Shader shader 		= Resources.Load("Shaders/" + shaderName) as Shader;
				if (shader != null)
					downloadIcon.material = new Material(shader);
				else
					downloadIcon.material = new Material(Shader.Find ("Unlit/Transparent Colored"));
					
				downloadIcon.mainTexture = texture;
				downloadIcon.transform.localScale = new Vector3(80f,80f,1f);
			}
		}
		else
		{
			Log.Debug("Cannot fetch the image from the facebook url");	
		}
	}
	
	public void ClickFriend()
	{
		Log.Warning("friend clicked, to do here");
		//PopupManager.ShowFriendInformationPopup(friend);
	}
	
	public void VisitClicked()
	{		
		VisitFriend.Visit(friend, "FRIENDS");
		
		PopupManager.HideLatestPopup();
		
		SceneManager.SharedManager.LoadScene(Scenes.FRIENDSHOME, true);
	}
	
	#region TwitterManager Callback
	public void OnTwitterManagerGetUserProfile(ArrayList userInfos)
	{
		if (userInfos != null)
		{
			Hashtable userInfo 	= (Hashtable)userInfos[0];
			string url 			= "" + userInfo["profile_image_url"];
			StartCoroutine(SetIcon(url));
		}
	}
	
	public void OnTwitterManagerGetFriendIds(ArrayList friendIds){}
	public void OnTwitterManagerLoginFailed(){}
	public void OnTwitterManagerLogin(string userId){}
	public void OnTwitterManagerLogout(){}
	public void OnTwitterManagerError(Exception e){}
	public void OnTwitterManagerUploadPhoto(){}
	public void OnTwitterManagerUploadPhotoError(string error){}
	public void OnTwitterManagerNewUserError(Hashtable socialInfo){}
	public void OnTwitterManagerExistingUserError(Hashtable existingUserInfo){}
	#endregion
	
}

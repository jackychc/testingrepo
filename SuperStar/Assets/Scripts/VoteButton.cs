using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class VoteButton : MonoBehaviour {

	public string targetGameId;
	
	public void Vote()
	{
		StartCoroutine(VoteRoutine());
	}
	
	IEnumerator VoteRoutine()
	{
		string topicId = Globals.viewingTopic.topicId.ToString();
		
		GameID gameId 		= new GameID(Globals.mainPlayer.GameId);
		int playerId 		= gameId.toInt();
		string checkSumData = gameId.ID + playerId + topicId + targetGameId;
		string checkSum 	= MD5Hash.GetServerCheckSumWithString("VotePlayer", checkSumData);
		
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("topicId", topicId);
		kvp.Add("gameId", Globals.mainPlayer.GameId);
		kvp.Add("targetGameId", targetGameId);
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "VotePlayer.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					PopupManager.ShowInformationPopup("Thank you!", false);
				}
				else
				{
					PopupManager.ShowInformationPopup(Language.Get("vote_networkerror"), false);
				}
			}
		}
		
		//yield return StartCoroutine(HTTPPost.Post("VotePlayer", kvp, result => response = result));	
	}
}

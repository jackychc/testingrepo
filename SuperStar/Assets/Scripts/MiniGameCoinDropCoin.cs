using UnityEngine;
using System.Collections;

public class MiniGameCoinDropCoin : MonoBehaviour
{
	public int slotId = -99;
	
	public void FlashCoin()
	{
		UISprite sprite = this.transform.FindChild("bgGrow").GetComponent<UISprite>();
		sprite.enabled = true;
		
		TweenAlpha ta = this.transform.FindChild("bgGrow").GetComponent<TweenAlpha>();
		Destroy(ta);
		
		ta = this.transform.FindChild("bgGrow").gameObject.AddComponent<TweenAlpha>();
		ta.from 	= 0.0f;
		ta.to 		= 1.0f;
		ta.duration = 0.5f;
		ta.eventReceiver = this.gameObject;
		ta.callWhenFinished = "OnFlashedCoin";
		ta.enabled 	= true;
	}
	
	public void OnFlashedCoin()
	{		
		TweenAlpha ta = this.transform.FindChild("bgGrow").GetComponent<TweenAlpha>();
		Destroy(ta);
		
		ta = this.transform.FindChild("bgGrow").gameObject.AddComponent<TweenAlpha>();
		ta.delay 	= 0.05f;
		ta.from 	= 1.0f;
		ta.to 		= 0.0f;
		ta.duration = 0.5f;
		ta.eventReceiver = this.gameObject;
		ta.callWhenFinished = "OnFlashedRestoredCoin";
		ta.enabled 	= true;
	}
	
	public void OnFlashedRestoredCoin()
	{
		UISprite growSprite = this.transform.FindChild("bgGrow").GetComponent<UISprite>();
		growSprite.enabled = false;
	}
}


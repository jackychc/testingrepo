using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class FriendRequestScrollListManager : MonoBehaviour
{
	float yPos = 0f;
	public List<FriendRequestItem> friendRequests = new List<FriendRequestItem>();
	
	void Start()
	{
		//StartCoroutine(LoadFriends());
	}
	
	public void LoadFriends(List<Player> fds)
	{
		friendRequests.Clear();
		int childs = transform.childCount;

        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
		SpringPanel.Begin(transform.gameObject, new Vector3(0f,195f,0f), 195f);
		
		yPos = 0f;
		// get all fds from server first?
		for (int i =0 ; i < fds.Count; i++)
		{
			
			GameObject go = (GameObject) Instantiate(Resources.Load ("FriendRequestItem") as GameObject);
		
			go.name = "FriendRequestItem"+i;
			go.transform.parent = transform;
			
			go.transform.localPosition = new Vector3(0, yPos,0f);
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = new Vector3(1f,1f, 1f);
			
			yPos -= 76f;
			
			FriendRequestItem fri = go.GetComponent<FriendRequestItem>();
			
			fri.gameId = fds[i].GameId;
			fri.friendName = fds[i].Name;
			fri.SetBgType((i+1)%2);
			friendRequests.Add(fri);
			
			
		}
		
		//transform.GetComponent<UIGrid>().repositionNow = true;
		
		
	}
	
	
	
}



using UnityEngine;
using System.Collections;

public class QuestItem : MonoBehaviour
{
	public Quest quest;
	
	public UISysFontLabel contentLabel;
	public GameObject skipButton;
	public GameObject goButton;
	public GameObject doneButton;
	public UISysFontLabel orangeLabel;
	public UISysFontLabel greenLabel;
	public RewardRowsPositioner rrp;
	public GameObject newBlock;
	public GameObject getRewardButton;
	
	public GameObject progressBlock;
	public UISysFontLabel progressLabel;

	bool rewardClicked;
	
	
	// Use this for initialization
	void Start ()
	{
		Destroy(transform.GetComponent<UIPanel>());
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
	
	public void Refresh()
	{
		Log.Debug("writing description");
		contentLabel.Text = quest.action.GetQuestDescription();
		
		rrp.reward = quest.action.GetQuestReward();
		rrp.Position();
		
		foreach (RewardRow row in rrp.GetComponentsInChildren<RewardRow>())
		{
			Destroy(row.transform.GetComponent<UIPanel>());
		}
		
		if (quest.isRunning)
		{
			//goButton.SetActive(false);
			
		}
		
		if (!quest.canSkip)
		{
			skipButton.SetActive(false);
		}
		
		if (quest.action.FinishRequirement())
		{
			goButton.SetActive(false);
			skipButton.SetActive(false);
			//doneButton.SetActive(true);
			
			StartCoroutine(WaitFocusRoutine());
		}
		
		if (quest.action.GetQuestProgress().Equals("") || quest.action.FinishRequirement())
		{
			progressBlock.SetActive(false);
		}
		else
		{
			progressBlock.SetActive(true);
			progressLabel.Text = quest.action.GetQuestProgress();
		}
		
		
		if (quest.isNew)
		{
			newBlock.SetActive(true);

			// here!
		}
		
	}
	
	IEnumerator WaitFocusRoutine()
	{
		float delta = transform.parent.localPosition.x + transform.localPosition.x;

		if (-20 <= delta && delta <= 20)
		{
			doneButton.SetActive(true);
			
			yield return new WaitForSeconds(0.2f);
			doneButton.GetComponent<ScaleTweener>().Forward();
			
			yield return new WaitForSeconds(0.2f);
			SoundManager.SharedManager.PlaySFX("stamp");

			yield return new WaitForSeconds(0.75f);
			getRewardButton.SetActive(true);
		}
		
		else
		{
			yield return new WaitForSeconds(0.1f);
			StartCoroutine(WaitFocusRoutine());
		}
	}
	
	
	void OrangePressed()
	{
		Log.Debug("Orange Pressed");
		
		PopupManager.ShowQuestSkipPopup(quest, false);
		PopupManager.HideLatestPopup();
		
		//transform.parent.GetComponent<QuestScrollListManager>().LoadQuests();
	}
	
	void GreenPressed()
	{
		Log.Debug("Green Pressed");
		
		if (quest.action.FinishRequirement())
		{
			DonePressed();
		}
		
		else
		{
			if (!quest.isRunning)
				quest.StartQuest();
			
			PopupManager.HideAllPopup();
			
			quest.isNew = false;
			quest.action.OnQuestGo();
		}
		
	}
	
	void DonePressed()
	{
		if (rewardClicked) return;

		rewardClicked = true;

		//Log Flurry Event
		Hashtable param = new Hashtable();
		param[quest.activityId] = "Done";
		MunerisManager.SharedManager.LogFlurryEventWithParams(MunerisManager.FlurryEvent.Quest_State, param);
		
		quest.isRunning = false;
		quest.isCompleted = true;
		quest.completedOnce = true;
		
		PopupManager.ShowQuestCompletePopup(quest,false);
		PopupManager.HideLatestPopup();
	}
}


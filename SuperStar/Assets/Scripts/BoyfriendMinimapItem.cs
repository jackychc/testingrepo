using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class BoyfriendMinimapItem : MonoBehaviour
{
	public BoyfriendMinimapPopup map;
	public Boyfriend boyfriend;
	public int location;
	//public bool isSexy;
	public UILabel nameTag;
	
	public UISprite nameBg;
	public UITexture bfIcon;
	public GameObject lockShade;
	public GameObject dlcIcon;
	
	public GameObject sexyIcon, livelyIcon;
	
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(DelayedStart());
	}
	
	IEnumerator DelayedStart()
	{
		Destroy(transform.GetComponent<UIPanel>());
		
		bfIcon.mainTexture = Boyfriends.GetIconOfBoyfriend(boyfriend);//(Texture2D) Resources.Load("Boyfriends/itemIcon/"+boyfriend.boyfriendName);
		
		if (bfIcon.mainTexture == null)
		{
			yield return StartCoroutine(LoadIconFromPersistent(boyfriend, result => bfIcon.mainTexture = result));
		}
		
		// todo? set size
		
		nameTag.text = boyfriend.boyfriendName;
		
		DateOrHome();
		
		if (!shouldEnable())
		{
			dlcIcon.SetActive(false);

			//nameBg.gameObject.SetActive(false);
			if (map.isSexyPlace[location-1])
				sexyIcon.SetActive(true);
			
			else
				livelyIcon.SetActive(true);
			
			nameTag.text = "  " + boyfriend.charismaRequired;
			nameTag.color = Color.red;
			
			lockShade.SetActive(true);
			//nameBg.color = Color.gray;
		}

		else
		{
			if (boyfriend.dlcId > Globals.currentDLCVersion)
			{
				dlcIcon.SetActive(true);
				nameTag.text = "  ";
			}
			else
				dlcIcon.SetActive(false);
		}
		
	}
	
	public void DateOrHome()
	{
		
	}
	
	public void ItemClicked()
	{
		//map.HideAllMenus();
		
		// ??
		if (shouldEnable())
		{
			if (boyfriend.dlcId <= Globals.currentDLCVersion)
				ShowMenu();
			else
			{
				if (!DownloadManager.SharedManager.isDownloading)
					DownloadManager.SharedManager.AskForDLCUpgrade(false, boyfriend.dlcId, -1);
				else
					DownloadManager.SharedManager.AskForDLCProgress(false, boyfriend.dlcId, -1);
			}
		}
		else
		{
			PopupManager.ShowTutorialPopup(5, false);
		}
		
		
		//Snapping();
	}
	
	public void SimsimiClicked()
	{
		Log.Debug("SIMSIMI!");
	}
	
	public void VisitClicked()
	{
		
		// TODO : if max reached, go home instead
		
		Globals.mainPlayer.CurrentBoyfriendId = boyfriend.boyfriendId;
		
		Log.Debug("visit!");
		map.SaveMinimapPosition();
		
		
		VisitBoyfriend.Visit(boyfriend, "STREET", location, map.isSexyPlace[location-1]);
//		map.Hide();
		//transform.GetComponent<SceneManager>().goToScene = "HOME";
		//transform.GetComponent<SceneManager>().GoToScene();
		SceneManager.SharedManager.LoadScene(Scenes.HOME, false);
		
	}
	
	public void DateClicked()
	{
		
		// TODO : if max reached, go home instead


		Log.Debug("date!");
		map.SaveMinimapPosition();
		
		VisitBoyfriend.Visit(boyfriend, "STREET", location, map.isSexyPlace[location-1]);
//		map.Hide();
		
		SceneManager.SharedManager.LoadScene(Scenes.BOYFRIEND, false);
		//transform.GetComponent<SceneManager>().goToScene = "BOYFRIEND";
		//transform.GetComponent<SceneManager>().GoToScene();
		
	}
	
	public void HideMenu()
	{
		//menu.SetActive(false);
	}
	
	public void ShowMenu()
	{
		//menu.SetActive(true);
		PopupManager.ShowBoyfriendOptionsPopup(this, boyfriend, (Texture2D) bfIcon.mainTexture, true);
	}
	
	public void Snapping()
	{
		//SpringPanel.Begin(transform.parent.gameObject, new Vector3(Mathf.Min(345f, Mathf.Max(-330f, -(transform.localPosition.x))), Mathf.Min(60f, Mathf.Max(-60f, -(transform.localPosition.y+40f))),-1f), 10f);
	}
	
	public bool shouldEnable()
	{
		if (boyfriend.rewardTimer != -1)
			return true;

		Vector2 playersIndex = Items.sumCharismaForList(Globals.mainPlayer.wearingItems);
		if (map.isSexyPlace[location-1])
		{
			return playersIndex.x >= boyfriend.charismaRequired;
		}
		//if (map.isSexyPlace[location-1])
		else
		{
			return playersIndex.y >= boyfriend.charismaRequired;
		}
	}
	
	public void BoyfriendOptionClicked()
	{
		if (Boyfriends.GetBoyfriend(boyfriend.boyfriendId).flirtExp >= Boyfriends.GetBoyfriend(boyfriend.boyfriendId).maxFlirtExp)
		{
			VisitClicked();
		}
		else
		{
			DateClicked();
		}
	}
	
	public IEnumerator LoadIconFromPersistent(Boyfriend i, Action<Texture2D> returnMe)
	{
		WWW loader = new WWW("file://" + Application.persistentDataPath + Path.DirectorySeparatorChar + "bf_" + i.boyfriendId + "_icon.png");
		yield return loader;
		
		if (loader.error != null)
		{
			Log.Debug("BF MiniMap Item cannot laod the icon file boyfriendId: " + i.boyfriendId);
		}
		else
		{
			if (loader.texture != null)
			{
				returnMe((Texture2D)loader.texture);
			}
			else
			{
				Log.Debug("BF MiniMap Item cannot laod the texture file with boyfriendId: " + i.boyfriendId);
			}
		}
	}
}


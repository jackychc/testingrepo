using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/**
 *   AppRoutinesChecker used to run some routines jobs
 * 	 that the app needed
 * 
 **/
public class AppRoutinesChecker : MonoBehaviour {

	void Awake()
	{
		DontDestroyOnLoad(this.gameObject);	
	}
	
	void OnDestroy()
	{
		
	}
	
	// Use this for initialization
	void Start () {
		StartCoroutine(CheckAppRoutines());
	}
	
	private IEnumerator CheckAppRoutines()
	{	
		//CheckShouldShowGameIDPopup();
		
		yield return StartCoroutine(FetchSystemTime());
		
		CheckNumOfDaysPlayed();
		CheckIAP2XExpired();
		CheckCanShowAdsInGame();
		
		Destroy(this.gameObject);
	}
	
	private void CheckShouldShowGameIDPopup()
	{
		if (!Globals.mainPlayer.InitializedPlayer)
		{
			string gameId = Globals.mainPlayer.GameId;
			if (gameId != null && !gameId.Equals(""))
				Globals.shouldShowGameIdPopup = true;
		}
	}
	
	private IEnumerator FetchSystemTime()
	{
		string checkSum = MD5Hash.GetServerCheckSumWithString("SystemTime", null);	
		Dictionary<string, string> kvp = new Dictionary<string, string>();
		kvp.Add("checkSum", checkSum);
		
		string url = Globals.serverAPIURL + "SystemTime.php";
		yield return StartCoroutine(NetworkManager.SharedManager.ConnectURL(url, kvp));
		
		string response				= NetworkManager.SharedManager.Response;
		NetworkManager.State state 	= NetworkManager.SharedManager.NetworkState;
		
		if (state.Equals(NetworkManager.State.Success))
		{
			if (response != null && !response.Equals(""))
			{
				ArrayList al =  new ArrayList(((Hashtable) SuperstarFashionMiniJSON.JsonDecode(response)).Values);
				if (Boolean.Parse(((Hashtable)al[0])["response"].ToString()))
				{
					// have rank already
					if (al[1] != null)
					{					
						int year 	= Int32.Parse(((Hashtable)al[1])["year"].ToString());
						int month 	= Int32.Parse(((Hashtable)al[1])["month"].ToString());
						int day		= Int32.Parse(((Hashtable)al[1])["day"].ToString());
						int hour	= Int32.Parse(((Hashtable)al[1])["hour"].ToString());
						int minute	= Int32.Parse(((Hashtable)al[1])["minute"].ToString());
						int second	= Int32.Parse(((Hashtable)al[1])["second"].ToString());
						
						//nowTime = DateTime.Parse(currentTimeStr);					
						Globals.currentTimeStr 	= year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
						Globals.localTimeStr	= DateTime.UtcNow.ToString("yyyy-MM-dd hh:mm:ss");
					}
				}
			}
		}
	}
	
	private void CheckNumOfDaysPlayed()
	{
		string currentTimeStr = Globals.currentTimeStr;
		
		if (currentTimeStr != null && !currentTimeStr.Equals(""))
		{
			DateTime nowTime = DateTime.Parse(Globals.currentTimeStr);
			
			if (Globals.startPlayTime == null || Globals.startPlayTime.Equals(""))
			{
				Log.Debug("App Routine The last Play Time is null");
				Log.Debug("App Routine now assign the current server time : " + currentTimeStr + " to startPlayTime");
				Globals.startPlayTime = currentTimeStr;
				Globals.mainPlayer.Save();
			}
			else
			{
				DateTime dbTime = DateTime.Parse(Globals.startPlayTime);
				TimeSpan timeDiff = nowTime - dbTime;
				if (timeDiff.TotalDays > 1 && timeDiff.TotalDays <= 2)
				{
					Log.Debug("Complete PPA PlayOn2ndDay");
					MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.PlayOn2ndDay);
				}
				else if (timeDiff.TotalDays > 2 && timeDiff.TotalDays <= 3)
				{
					Log.Debug("Complete PPA PlayOn3rdDay");
					MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.PlayOn3rdDay);
				}
				else if (timeDiff.TotalDays > 3 && timeDiff.TotalDays <= 4)
				{
					Log.Debug("Complete PPA PlayOn4thDay");
					MunerisManager.SharedManager.CompletePPA(MunerisManager.PPA.PlayOn4thDay);
				}
			}
		}
	}
	
	private void CheckIAP2XExpired()
	{
		string startPlayTimeStr = Globals.startPlayTime;
		if (startPlayTimeStr != null && !startPlayTimeStr.Equals(""))
		{
			DateTime startPlayTime 	= DateTime.Parse(startPlayTimeStr);
			DateTime currTime 		= DateTime.UtcNow;
			TimeSpan timeDiff 		= currTime - startPlayTime;
			
			if (timeDiff.TotalDays > Globals.IAP2XDuration)
			{
				Globals.isIAP = true;	
			}
		}
	}
	
	private void CheckCanShowAdsInGame()
	{
		string iapRecordsStr = Globals.iapRecord;
		if (iapRecordsStr != null && !iapRecordsStr.Equals(""))
		{
			Log.Debug("App Routine iapRecordStr: " + iapRecordsStr);
			string[] iapRecordsArr	 	= iapRecordsStr.Split(',');
			List<string> iapRecordsList = new List<string>(iapRecordsArr);
			if (iapRecordsList.Contains(Enum.GetName(typeof(IAPPopup.IAPPackageIds), IAPPopup.IAPPackageIds.coinPackage4)) 	||
				iapRecordsList.Contains(Enum.GetName(typeof(IAPPopup.IAPPackageIds), IAPPopup.IAPPackageIds.coinPackage5)) 	||
				iapRecordsList.Contains(Enum.GetName(typeof(IAPPopup.IAPPackageIds), IAPPopup.IAPPackageIds.coinPackage6)) 	||
				iapRecordsList.Contains(Enum.GetName(typeof(IAPPopup.IAPPackageIds), IAPPopup.IAPPackageIds.gemPackage4)) 	||
				iapRecordsList.Contains(Enum.GetName(typeof(IAPPopup.IAPPackageIds), IAPPopup.IAPPackageIds.gemPackage5)) 	||
				iapRecordsList.Contains(Enum.GetName(typeof(IAPPopup.IAPPackageIds), IAPPopup.IAPPackageIds.gemPackage6))
				)
			{
				Globals.canShowAds = false;
			}
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}

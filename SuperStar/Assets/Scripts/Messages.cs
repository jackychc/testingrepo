using System;
using System.Collections.Generic;
using System.Collections;


public class Messages
{
	public static List<Message> GetMessagesFromHashtable(Hashtable ht)
	{
		List<Message> returnMe = new List<Message>();
		
		if (ht == null) return returnMe;

		for (int i = 0; i < 2; i++)
		{
			string partName = i == 0? "requestForVotes" : "getVotes";
			
			if (ht[partName] == null) continue;
			
			Hashtable htReq = (Hashtable) ht[partName];
			ArrayList alReq = new ArrayList((htReq).Values);
			
			foreach (Hashtable htR in (alReq))
			{
				ArrayList al2Req = new ArrayList(((Hashtable)htR["requests"]).Values);
				
				foreach (Hashtable htR2 in al2Req)
				{
					Message m = new Message(
							Int32.Parse(htR2["voteRequestId"].ToString()),
							Int32.Parse(htR2["topicId"].ToString()),
							htR2["gameId"].ToString(),
							htR2["playerName"] != null? htR2["playerName"].ToString() : htR2["gameId"].ToString(),
							Int32.Parse(htR2["accepted"].ToString())
						);
					
					returnMe.Add(m);
				}
			}
		}
		
		return returnMe;
	}
	
	public static int GetNoOfMessages()
	{
		if (Globals.messages == null)
			return 0;
		
		return Globals.messages.Count;
	}
}

public class Message
{
	public int messageId;
	public int topicId;
	public string oppositeGameId;
	public string oppositePlayerName;
	public int accepted;
	
	public Message(int pMessageid, int pTopicId, string pGameId, string pOppositeName, int pAccepted)
	{
		this.messageId = pMessageid;
		this.topicId = pTopicId;
		this.oppositeGameId = pGameId;
		this.oppositePlayerName = pOppositeName;
		this.accepted = pAccepted;
	}
}



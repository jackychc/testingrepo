using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class PhotoshotScene : MonoBehaviour, PhotoshotPreviewPopupCallbackInterface {
	
	public GameObject leftArrow;
	public GameObject rightArrow;
	public GameObject scrollPanelGroup;
	public GameObject uiPanel;
	public GameObject flashPanel;
	
	public UITexture bgTexture;
	public UISprite showPanelButton;
	
	private bool isShowPanel = true;
	private GameObject mainCharacter;
	private GameObject characterRef;
	
	// girl, arrow
	float tween = 0f;
	public float[] xDisplacement = new float[2]{30f, 120f}; 
	bool isMagnifying;
	int magnifyMode = 0; // 0,1,2,0,1,2,...
	
	// Use this for initialization
	IEnumerator Start () 
	{
		GameObject femaleObj = Globals.globalCharacter;
		GameObject charRefObj = GameObject.Find("CharacterReference");
		GameObject girlParentObj = GameObject.Find("GirlParent");		
		
		/*if (femaleObj == null)
		{
			mainCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			mainCharacter.name = "FEMALE";
			mainCharacter.AddComponent<CharacterManager>();	
		}		
		else
		{
			mainCharacter = femaleObj;
		}*/
		
		mainCharacter = femaleObj;
		mainCharacter.transform.parent = girlParentObj.transform;
		mainCharacter.GetComponent<CharacterManager>().SetPRSbyReference(charRefObj);
		yield return StartCoroutine(mainCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
		characterRef = charRefObj;
		
		Globals.wearingBackup.Clear();
		foreach( Item i in Globals.mainPlayer.wearingItems)
			Globals.wearingBackup.Add(i);
		
		SoundManager.SharedManager.ChangeMusic("SuperstarStory_home_remix0");
		
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.PHOTOSHOT);
	}
	
	void Update()
	{
	}
	
	
	#region Button Click
	public void BackButtonClicked()
	{
		SceneManager.SharedManager.LoadScene(Scenes.HOME, true);	
	}	
	
	public void ShowPanelButtonClicked()
	{
		isShowPanel = !isShowPanel;
		
		if (isShowPanel)
		{
			//showPanelButton.spriteName = "arrow_down";
			//showPanelButton.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 180));
			showPanelButton.transform.localRotation = Quaternion.identity;
			scrollPanelGroup.GetComponent<AnchorTweener>().Backward();
		}
		else
		{
			showPanelButton.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 180));
			//showPanelButton.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -180));
			scrollPanelGroup.GetComponent<AnchorTweener>().Forward();
		}
	}
	
	public void CameraButtonClicked()
	{
		SoundManager.SharedManager.PlaySFX("take_photo");
		TweenAlpha ta = flashPanel.GetComponent<TweenAlpha>();
		ta.enabled = true;
	}
	
	public void HidePanelClicked()
	{
		ShowPanelButtonClicked();
	}
	#endregion
	
	#region Screen Capture
	public void OnFlashed()
	{
		Log.Debug("On Flash Finished");
		TweenAlpha ta = flashPanel.GetComponent<TweenAlpha>();
		Destroy(ta);
		
		//ta.from = 1;
		//ta.to= 0;
		//ta.enabled = true;
		TweenAlpha newTa = flashPanel.AddComponent<TweenAlpha>();
		newTa.delay = 0.0f;
		newTa.duration = 0.3f;
		newTa.from = 1;
		newTa.to = 0;
		newTa.enabled = true;
		newTa.eventReceiver = this.gameObject;
		newTa.callWhenFinished = "OnFlashFinished";
	}
	
	public void OnFlashFinished()
	{
		//Clear the exist TweenAlpha
		TweenAlpha ta = flashPanel.GetComponent<TweenAlpha>();
		Destroy(ta);
		
		//Restore to normal TweenAlpha
		TweenAlpha newTa = flashPanel.AddComponent<TweenAlpha>();
		newTa.delay = 0.05f;
		newTa.duration = 0.25f;
		newTa.from = 0;
		newTa.to = 1;
		newTa.enabled = false;
		newTa.eventReceiver = this.gameObject;
		newTa.callWhenFinished = "OnFlashed";
		
		//PopupManager.ShowLoadingFlower();
		StartCoroutine(CapturePhoto());
	}
	
	public void SetShowUI(bool show)
	{
		scrollPanelGroup.SetActive(show);
		uiPanel.SetActive(show);
	}
	
	public IEnumerator CapturePhoto()
	{		
		//Disable all the UI
		SetShowUI(false);
		//PopupManager.HideLoadingFlower();
		
		yield return new WaitForEndOfFrame();
		
		
		
        Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        tex.Apply();
		
		/*
		string storagePath 	= UtilsHelper.GetWriteableDirectory();
		string filePath 	= storagePath + Path.DirectorySeparatorChar + Globals.appName + Path.DirectorySeparatorChar;
		string fileName 	= System.DateTime.Now.Ticks + ".png";
		string fileNamePath = filePath + fileName;
		
		if (!Directory.Exists(filePath))
			Directory.CreateDirectory(filePath);
		File.WriteAllBytes(fileNamePath, imageBytes);
		UtilsHelper.RefreshDirectoryWithFileName(fileNamePath);
		*/
		//SetShowUI(true);
		
		//PopupManager.ShowInformationPopup("HIHI");
		PopupManager.ShowPhotoshotPreviewPopup(this, tex, false);
		//Destroy(tex);
		//PopupManager.ShowInformationPopup("Screen Captured!!");
	}
	#endregion
	
	
	#region PhotoshotPopup Listener
	public void OnPhotoshotPreviewPopupSave()
	{
		Log.Debug("PhotoshotPopup Listener save");
		SetShowUI(true);
	}
	
	public void OnPhotoshotPreviewPopupDiscard()
	{
		Log.Debug("PhotoshotPopup Listener Discard");
		SetShowUI(true);
	}
	
	public void OnPhotoshotPreviewPopupShare()
	{
		Log.Debug("PhotoshotPopup Listener Share");
		SetShowUI(true);
	}
	#endregion
}

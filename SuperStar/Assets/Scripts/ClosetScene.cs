using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ClosetScene : MonoBehaviour, InputNamePopupCallbackInterface, PopupManagerCallbackInterface {
	
	public GameObject itemsNameGroupObj;
	public GameObject closetMenu, topMenu;
	public GameObject leftArrow, rightArrow, scrollPanelGroup;
	public GameObject indexBlock;
	public GameObject zoomButton;
	public CharacterRotationScroller rotationScroller;

	private bool canShowItemNames 	= false;
	private volatile bool uiLock	= false;
	
	// girl, arrow
	float tween = 0f;
	public float[] xDisplacement = new float[2]{30f, 120f}; 
	
	GameObject newCharacter;
	bool isMagnifying;
	int magnifyMode = 0; // 0,1,2,0,1,2,...
	
	
	
	// Use this for initialization
	IEnumerator Start () 
	{		
		if (GameObject.Find("FEMALE") == null)
		{
			newCharacter = (GameObject) Instantiate((Resources.Load("girl3_hair2") as GameObject));
			newCharacter.name = "FEMALE";
			newCharacter.AddComponent<CharacterManager>();			
		}		
		else
		{
			newCharacter = GameObject.Find("FEMALE");
		}
		
		newCharacter.transform.parent 			= GameObject.Find("GirlParent").transform;
		newCharacter.transform.localPosition 	= Vector3.zero;
		newCharacter.GetComponent<CharacterManager>().SetPRSbyReference(GameObject.Find("CharacterReference"));
		yield return StartCoroutine(newCharacter.GetComponent<CharacterManager>().EnumGenerateCharacter(Globals.mainPlayer));
		
		
		Globals.wearingBackup.Clear();
		foreach( Item i in Globals.mainPlayer.wearingItems)
		{
			Globals.wearingBackup.Add(i);
		}
		
		
		if (!Globals.mainPlayer.InitializedPlayer)
		{
			canShowItemNames = false;
			StartCoroutine(EnumInitPlayer());
		}
		else
			canShowItemNames = true;
		
		SoundManager.SharedManager.ChangeMusic("SuperstarStory_home_remix0");
		PopupManager.SetCallback(this);
		
		StartCoroutine(LoopAnimations());
		
		GameStateManager.SharedManager.CheckToShowinterstitial(Scenes.CLOSET);
	}
	
	void OnDestroy()
	{
		PopupManager.RemoveCallback(this);	
	}
	
	IEnumerator EnumInitPlayer()
	{
		yield return new WaitForEndOfFrame();
		
		//Adjust the scene for startup
		magnify();
		leftArrow.SetActive(false);
		rightArrow.SetActive(false);
		topMenu.SetActive(false);
		closetMenu.SetActive(false);
		itemsNameGroupObj.SetActive(false);
		//topMenu.GetComponent<AnchorTweener>().Backward();
		//closetMenu.GetComponent<AnchorTweener>().Backward();


		//DownloadManager.SharedManager.AskForDLCUpgrade(true, -1, 1);
		PopupManager.ShowInputNamePopup(this, false);
	}
	
	void toggleZoom()
	{
		if (uiLock)
			return;

		Log.Debug("Zooming");
		if (isMagnifying) 
		{
			magnify();
			return;
		}
		
		if (scrollPanelGroup.GetComponent<ClosetScrollListManager>().isZooming())
			scrollPanelGroup.GetComponent<ClosetScrollListManager>().zoomOut();
		else
			scrollPanelGroup.GetComponent<ClosetScrollListManager>().zoomIn();
	}
	
	void magnify()
	{
		if (uiLock)
			return;

		Log.Debug("Magnifying");
		isMagnifying = !isMagnifying;
		
		//magnifyMode = (magnifyMode+1)%3;
		
		// 2
		if (/*magnifyMode == 2)*/isMagnifying)
		{
			//Log.Debug("mag!");
			
			scrollPanelGroup.GetComponent<ClosetScrollListManager>().zoomOut();
			GameObject.Find("GirlParent").GetComponent<PositionTweener>().Forward();
			
			leftArrow.GetComponent<PositionTweener>().Forward();
			rightArrow.GetComponent<PositionTweener>().Forward();
			
			if (indexBlock != null)
				indexBlock.GetComponent<PositionTweener>().Forward();
			
			
			scrollPanelGroup.GetComponent<AnchorTweener>().Forward();
			scrollPanelGroup.GetComponent<ClosetScrollListManager>().HideAllPanels();
			GameObject.Find("ItemNamePanel").GetComponent<UIPanelAlphaTweener>().Backward();
			
			//zoomButton.SetActive(false);
			
			if (canShowItemNames)
			{
				if (itemsNameGroupObj != null)
					itemsNameGroupObj.SetActive(true);
				
				//Load itemsName
				StartCoroutine(ShowItemNames());
			}
		}
		else
		{
			
			GameObject.Find("GirlParent").GetComponent<PositionTweener>().Backward();
			
			leftArrow.GetComponent<PositionTweener>().Backward();
			rightArrow.GetComponent<PositionTweener>().Backward();
			
			if (indexBlock != null)
			indexBlock.GetComponent<PositionTweener>().Backward();
			
			scrollPanelGroup.GetComponent<AnchorTweener>().Backward();
			
			/*if (magnifyMode == 1)
				scrollPanelGroup.GetComponent<ClosetScrollListManager>().zoomIn();
			else
				scrollPanelGroup.GetComponent<ClosetScrollListManager>().zoomOut();*/
			scrollPanelGroup.GetComponent<ClosetScrollListManager>().LoadZoom();
			
			
			scrollPanelGroup.GetComponent<ClosetScrollListManager>().LoadRotate();
			scrollPanelGroup.GetComponent<ClosetScrollListManager>().LoadPanels();
			
			//zoomButton.SetActive(true);
			
			if (canShowItemNames)
			{
				if (itemsNameGroupObj != null)
					itemsNameGroupObj.SetActive(false);
				
				ResetItemNames();			
			}			
		}
	}	
	
	private void ResetItemNames()
	{
		for (int i = 0; i < itemsNameGroupObj.transform.childCount; i++)
		{
			GameObject childObj = itemsNameGroupObj.transform.GetChild(i).gameObject;
			
			TweenAlpha ta = childObj.GetComponent<TweenAlpha>();
			if (ta != null)
				Destroy(ta);
			UIPanel childObjPanel = childObj.GetComponent<UIPanel>();
			if (childObjPanel != null)
				childObjPanel.alpha = 0.0f;
		}
	}
	
	private IEnumerator ShowItemNames()
	{
		//yield return new WaitForSeconds(0.1f);
		
		for (int i = 0; i < itemsNameGroupObj.transform.childCount; i++)
		{
			GameObject childObj = itemsNameGroupObj.transform.GetChild(i).gameObject;
			string subcatName = childObj.name;
			
			bool isActiveObj = false;
			ItemSubcategory itemSubcat = ItemSubcategories.getSubcategory((int)Enum.Parse(typeof(ItemSubcategories.Id), subcatName));
			if (itemSubcat != null)
			{
				Item item = Globals.mainPlayer.wearingItemOfSubcategory(itemSubcat);
				if (item != null)
				{
					if (!item.itemName.Equals("None"))
					{
						UILabel itemLabel 	= childObj.transform.GetChild(0).GetComponent<UILabel>();
						itemLabel.text 		= item.itemName;
						isActiveObj 		= true;
					}
				}
			}
			
			if (isActiveObj)
			{
				TweenAlpha ta = childObj.GetComponent<TweenAlpha>();
				if (ta != null)
					Destroy(ta);
				
				ta = childObj.AddComponent<TweenAlpha>();
				ta.from 	= 0.0f;
				ta.to 		= 1.0f;
				ta.delay	= i * 0.05f;
				ta.style 	= UITweener.Style.Once;
				ta.enabled	= true;
			}
			
			childObj.SetActive(isActiveObj);
		}
		
		yield return null;
	}
	
	void resetClothes()
	{
		Globals.mainPlayer.wearingItems = Globals.wearingBackup;
		
		Log.Debug("reset");
		
		GameObject.Find("FEMALE").GetComponent<CharacterManager>().GenerateCharacter(Globals.mainPlayer);
		
		scrollPanelGroup.GetComponentInChildren<ClosetScrollListManager>().refreshTicks();
		scrollPanelGroup.GetComponentInChildren<ClosetScrollListManager>().focusToWearingItem();
	}
	
	public IEnumerator LoopAnimations()
	{
		while(true)
		{		
			
			if (newCharacter.GetComponent<CharacterManager>().animationQueue.Count <= 3)
			{
				newCharacter.GetComponent<CharacterManager>().QueueAnimation(RandomIdleAnimationString(), false);	
			}	
			
			yield return new WaitForSeconds(2.0f);			
		}
	}
	
	string RandomIdleAnimationString()
	{
		int ran = UnityEngine.Random.Range(1,2+1);
		
		
		switch(ran)
		{
			//abcde
			
			case 1: return "pose01";
			case 2: return "pose02";
			
		}
		
		return "pose01";
	}
	
	public void ShowVotingReadyPopup()
	{
		PopupManager.ShowVotingReadyPopup(false);
	}
	
	public void ClosetFinish()
	{
		if (uiLock)
			return;

		uiLock = true;
		StartCoroutine(ClosetFinishRoutine());
	}
	
	public IEnumerator ClosetFinishRoutine()
	{
		
		scrollPanelGroup.GetComponentInChildren<ClosetScrollListManager>().HideAllPanels();
		//Reset the character rotation
		newCharacter.transform.parent.rotation = Quaternion.identity;
		newCharacter.transform.rotation = Quaternion.identity;


		
		// should reset the direction before doing animation
		//newCharacter.transform.localEulerAngles = GameObject.Find("CharacterReference").transform.localEulerAngles;
		newCharacter.GetComponent<CharacterManager>().InsertAnimation("closet_finish");
		rotationScroller.transform.localPosition = Vector3.zero;
		rotationScroller.MakeUndraggable();

		//Destroy(rotationScroller);
		yield return new WaitForSeconds(2.8f);
		
		SceneManager.SharedManager.LoadScene(Scenes.HOME, true);

		if (Globals.hasChanagedClothesInCloset)
		{
			MunerisManager.SharedManager.LogFlurryEvent(MunerisManager.FlurryEvent.Closet_Changed_Clothes);

			Globals.hasChanagedClothesInCloset = false;
		}

		uiLock = false;
	}
	
	public void GoToInitDBScene()
	{
		SceneManager.SharedManager.LoadScene(Scenes.INITDB, true);	
	}
	
	#region PopupManager Callback
	public void OnPopupManagerHidePopup(GameObject popup)
	{
		/*
		if (popup.name.Equals("GameIDGetPopup"))
		{
			magnify();
			topMenu.SetActive(true);
			closetMenu.SetActive(true);
			canShowItemNames = true;
		}
		*/
	}
	
	public void OnPopupManagerShowPopup(GameObject popup){}
	#endregion

	public void OnInputNamePopupOKButtonClicked()
	{
		/*
		if (Globals.shouldShowGameIdPopup)
		{
			Globals.shouldShowGameIdPopup =  false;
			
			//PopupManager.ShowGameIDGetPopup(Globals.mainPlayer.GameId, false);
		}
		else
		{
			magnify();
			topMenu.SetActive(true);
			closetMenu.SetActive(true);
			canShowItemNames = true;	
		}
		*/

		//if (Globals.shouldShowDLCPopup)
		//{
			//DownloadManager.SharedManager.AskForDLCUpgrade(true, -1, 1, 2, false);
			//Globals.shouldShowDLCPopup = false;
		//}

		magnify();
		topMenu.SetActive(true);
		closetMenu.SetActive(true);
		canShowItemNames = true;
	}
}

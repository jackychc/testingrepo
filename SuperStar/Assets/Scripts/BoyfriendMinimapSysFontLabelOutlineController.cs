using UnityEngine;
using System.Collections;

public class BoyfriendMinimapSysFontLabelOutlineController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OnTweenOutlineFadeInFinished()
	{
		TweenSysFontLabelOutline to = this.gameObject.GetComponent<TweenSysFontLabelOutline>();
		if (to != null)
			Destroy(to);
		
		to = this.gameObject.AddComponent<TweenSysFontLabelOutline>();
		to.style				= UITweener.Style.Once;
		to.delay 				= 0.0f;
		to.duration 			= 0.25f;
		to.callWhenFinished 	= "OnTweenOutlineFinished";
		to.eventReceiver		= this.gameObject;
		to.from					= 100;
		to.to					= 5;
		to.enabled				= true;	
	}
	
	
	public void OnTweenOutlineFinished()
	{
		TweenSysFontLabelOutline to = this.gameObject.GetComponent<TweenSysFontLabelOutline>();
		if (to != null)
			Destroy(to);
		
		to = this.gameObject.AddComponent<TweenSysFontLabelOutline>();
		to.style				= UITweener.Style.Once;
		to.delay 				= 2.0f;
		to.duration 			= 0.25f;
		to.callWhenFinished 	= "OnTweenOutlineFadeInFinished";
		to.eventReceiver		= this.gameObject;
		to.from					= 5;
		to.to					= 100;
		to.enabled				= true;
		
		this.gameObject.GetComponent<UISysFontLabel>().EffectQuality = 5;
	}
}

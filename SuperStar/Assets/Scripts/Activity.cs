using UnityEngine;
using System.Collections;

public class Activity
{
	public int activityId = 1;
	public int sortOrder;
	public string title = "";
	public string description = "";
	public int timeRequired = 0;
	public int timeLeft = 0;
	public int gemsToFinish = 2;
	public int rewardId = -1;
	
	
}


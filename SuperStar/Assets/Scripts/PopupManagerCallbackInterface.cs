using UnityEngine;
using System.Collections;

public interface PopupManagerCallbackInterface{
	void OnPopupManagerHidePopup(GameObject popup);
	void OnPopupManagerShowPopup(GameObject popup);
}

# **Welcome to SuperStar!** #

## **Pre-requisite** ##
Check if the following is installed in yr machine.

- Python 2.7.5

- Git

- Git-fat

- rsync


## **1. Configuration in Unity** ##

Enable External option in **Unity → Preferences → Packages → Repository**. (Skip this step in Unity v4.5 and up)

Switch to Visible Meta Files in **Edit → Project Settings → Editor → Version Control Mode**.

Switch to Force Text in **Edit → Project Settings → Editor → Asset Serialization Mode**.

**Save** the scene and project from File menu.